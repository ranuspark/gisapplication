package com.example.bhumicloud.gisapplications.model.field.attachment.audio;

import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;

import java.util.Map;

public class AudioItemValue extends MediaItemValue {
    private static final String JSON_AUDIO_ID = "audio_id";

    public AudioItemValue(Attachment attachment) {
        super(attachment);
    }

    public AudioItemValue(Map attributes) {
        super(attributes);
    }

    protected String getIDFieldTitle() {
        return JSON_AUDIO_ID;
    }

    protected boolean canEqual(Object object) {
        return object instanceof AudioItemValue;
    }
}
