package com.example.bhumicloud.gisapplications.model.field.attachment.audio;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;

import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment.Type;
import com.example.bhumicloud.gisapplications.util.MimeUtils;

import java.io.File;
import java.util.List;

public class Audio extends Attachment {
    public static final Creator<Audio> CREATOR = new C11781();
    public static final String RECORDING_MIME_TYPE = "audio/m4a";

    static class C11781 implements Creator<Audio> {
        C11781() {
        }

        public Audio createFromParcel(Parcel source) {
            return new Audio(source);
        }

        public Audio[] newArray(int size) {
            return new Audio[size];
        }
    }

    public static Uri generateAudioURI(Context context, String key, String contentType) {
        return Uri.fromFile(generateAudioFile(context, key, contentType));
    }

    public static File generateAudioFile(Context context, String key, String contentType) {
        return new File(Attachment.getStorageDirectory(context), "audio-" + key + "." + MimeUtils.getExtension(contentType));
    }

    public static List<Attachment> getAudios(Account account, Record record, boolean includeUploaded) {
        return Attachment.getAttachments(account, record, includeUploaded, Attachment.TYPE_AUDIO);
    }

    public Audio(String key) {
        super(key);
    }

    public Audio(Cursor cursor) {
        super(cursor);
    }

    private Audio(Parcel parcel) {
        super(parcel);
    }

    public File generateFile(Context context, String contentType) {
        return generateAudioFile(context, getRemoteID(), contentType);
    }

    public Type getType() {
        return Type.AUDIO;
    }

    public boolean canEqual(Object other) {
        return other instanceof Audio;
    }
}
