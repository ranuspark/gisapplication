package com.example.bhumicloud.gisapplications.util;

import android.content.Context;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.InflaterInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class CompressionUtils {
    public static File zipFiles(Context context, File... files) throws Throwable {
        Throwable e;
        Throwable th;
        if (FileUtils.isExternalStorageAvailable()) {
            File cache = context.getExternalCacheDir();
            if (FileUtils.isWritable(cache)) {
                File destination = new File (cache, System.currentTimeMillis() + ".zip");
                boolean success = true;
                FileOutputStream fos = null;
                BufferedOutputStream bos = null;
                ZipOutputStream zos = null;
                try {
                    FileOutputStream fos2 = new FileOutputStream (destination);
                    try {
                        BufferedOutputStream bos2 = new BufferedOutputStream (fos2);
                        try {
                            ZipOutputStream zos2 = new ZipOutputStream (bos2);
                            try {
                                for (File file : files) {
                                    if (file != null && file.exists()) {
                                        addToZip(file, zos2);
                                    }
                                }
                                if (zos2 != null) {
                                    try {
                                        zos2.close();
                                    } catch (Throwable e2) {
                                        GISLogger.log(e2);
                                        success = false;
                                    }
                                }
                                if (bos2 != null) {
                                    try {
                                        bos2.close();
                                    } catch (Throwable e22) {
                                        GISLogger.log(e22);
                                        success = false;
                                    }
                                }
                                if (fos2 != null) {
                                    try {
                                        fos2.close();
                                        zos = zos2;
                                        bos = bos2;
                                        fos = fos2;
                                    } catch (Throwable e222) {
                                        GISLogger.log(e222);
                                        success = false;
                                        zos = zos2;
                                        bos = bos2;
                                        fos = fos2;
                                    }
                                } else {
                                    bos = bos2;
                                    fos = fos2;
                                }
                            } catch (IOException e3) {
                                IOException e222 = e3;
                                zos = zos2;
                                bos = bos2;
                                fos = fos2;
                                try {
                                    GISLogger.log(e222);
                                    success = false;
                                    if (zos != null) {
                                        try {
                                            zos.close();
                                        } catch (Throwable e2222) {
                                            GISLogger.log(e2222);
                                            success = false;
                                        }
                                    }
                                    if (bos != null) {
                                        try {
                                            bos.close();
                                        } catch (Throwable e22222) {
                                            GISLogger.log(e22222);
                                            success = false;
                                        }
                                    }
                                    if (fos != null) {
                                        try {
                                            fos.close();
                                        } catch (Throwable e222222) {
                                            GISLogger.log(e222222);
                                            success = false;
                                        }
                                    }
                                    if (success) {
                                        return destination;
                                    }
                                    destination.delete();
                                    return null;
                                } catch (Throwable th2) {
                                    th = th2;
                                    if (zos != null) {
                                        try {
                                            zos.close();
                                        } catch (Throwable e2222222) {
                                            GISLogger.log(e2222222);
                                        }
                                    }
                                    if (bos != null) {
                                        try {
                                            bos.close();
                                        } catch (Throwable e22222222) {
                                            GISLogger.log(e22222222);
                                        }
                                    }
                                    if (fos != null) {
                                        try {
                                            fos.close();
                                        } catch (Throwable e222222222) {
                                            GISLogger.log(e222222222);
                                        }
                                    }
                                    throw th;
                                }
                            } catch (Throwable th3) {
                                th = th3;
                                zos = zos2;
                                bos = bos2;
                                fos = fos2;
                                if (zos != null) {
                                    zos.close();
                                }
                                if (bos != null) {
                                    bos.close();
                                }
                                if (fos != null) {
                                    fos.close();
                                }
                                throw th;
                            }
                        } catch (IOException e4) {
                            IOException e222222222 = e4;
                            bos = bos2;
                            fos = fos2;
                            GISLogger.log(e222222222);
                            success = false;
                            if (zos != null) {
                                zos.close();
                            }
                            if (bos != null) {
                                bos.close();
                            }
                            if (fos != null) {
                                fos.close();
                            }
                            if (success) {
                                return destination;
                            }
                            destination.delete();
                            return null;
                        } catch (Throwable th4) {
                            th = th4;
                            bos = bos2;
                            fos = fos2;
                            if (zos != null) {
                                zos.close();
                            }
                            if (bos != null) {
                                bos.close();
                            }
                            if (fos != null) {
                                fos.close();
                            }
                            throw th;
                        }
                    } catch (IOException e5) {
                        IOException e222222222 = e5;
                        fos = fos2;
                        GISLogger.log(e222222222);
                        success = false;
                        if (zos != null) {
                            zos.close();
                        }
                        if (bos != null) {
                            bos.close();
                        }
                        if (fos != null) {
                            fos.close();
                        }
                        if (success) {
                            return destination;
                        }
                        destination.delete();
                        return null;
                    } catch (Throwable th5) {
                        th = th5;
                        fos = fos2;
                        if (zos != null) {
                            zos.close();
                        }
                        if (bos != null) {
                            bos.close();
                        }
                        if (fos != null) {
                            fos.close();
                        }
                        throw th;
                    }
                } catch (IOException e6) {
                    IOException e222222222 = e6;
                    GISLogger.log(e222222222);
                    success = false;
                    if (zos != null) {
                        zos.close();
                    }
                    if (bos != null) {
                        bos.close();
                    }
                    if (fos != null) {
                        fos.close();
                    }
                    if (success) {
                        return destination;
                    }
                    destination.delete();
                    return null;
                }
                if (success) {
                    return destination;
                }
                destination.delete();
            }
        }
        return null;
    }

    public static void addToZip(File source, ZipOutputStream zos) throws IOException {
        if (FileUtils.isReadable(source)) {
            zos.putNextEntry(new ZipEntry (source.getName()));
            BufferedInputStream in = new BufferedInputStream (new FileInputStream (source));
            byte[] buffer = new byte[1024];
            while (true) {
                int length = in.read(buffer);
                if (length >= 0) {
                    zos.write(buffer, 0, length);
                } else {
                    zos.closeEntry();
                    in.close();
                    return;
                }
            }
        }
    }

    public static byte[] inflate(byte[] data) throws IOException {
        ByteArrayInputStream input = new ByteArrayInputStream (data);
        ByteArrayOutputStream output = new ByteArrayOutputStream ();
        InflaterInputStream inflater = new InflaterInputStream (input);
        byte[] buffer = new byte[256];
        while (true) {
            int length = inflater.read(buffer);
            if (length < 0) {
                return output.toByteArray();
            }
            output.write(buffer, 0, length);
        }
    }
}
