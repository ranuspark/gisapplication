package com.example.bhumicloud.gisapplications.model.layer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.util.FileUtils;
import com.example.bhumicloud.gisapplications.util.FileUtils.GISDirectoryType;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

public class MBTilesMapLayer extends MapLayer {
    public static final Creator<MBTilesMapLayer> CREATOR = new C12191();
    public static final String TYPE = "mbtiles";
    private String mLocalURL;

    static class C12191 implements Creator<MBTilesMapLayer> {
        C12191() {
        }

        public MBTilesMapLayer createFromParcel(Parcel source) {
            return new MBTilesMapLayer(source);
        }

        public MBTilesMapLayer[] newArray(int size) {
            return new MBTilesMapLayer[size];
        }
    }

    public static File getStorageDirectory(Context context) {
        return FileUtils.getGisDataDir (context, FileUtils.GISDirectoryType.maps);
    }

    public static void removeTempFiles(Context context, ArrayList<String> layerIDs) {
        File storageDir = getStorageDirectory(context);
        if (storageDir != null) {
            File[] temps = storageDir.listFiles();
            if (temps != null && temps.length > 0) {
                for (File file : temps) {
                    if (file.getName().endsWith(".tmp") && !layerIDs.contains(FileUtils.getFilenameFromPath(file.getPath()))) {
                        file.delete();
                    }
                }
            }
        }
    }

    public MBTilesMapLayer(Cursor cursor) {
        super(cursor);
        this.mLocalURL = cursor.getString(cursor.getColumnIndex(MapLayer.COLUMN_LOCAL_URL));
    }

    public MBTilesMapLayer(Account account, File file) {
        this.mAccountID = account.getRowID();
        this.mName = file.getName();
        this.mDescription = file.getAbsolutePath();
        this.mLocalURL = file.getAbsolutePath();
        this.mFilesize = (double) file.length();
    }

    public MBTilesMapLayer(Account account, Map jsonRepresentation) {
        super(account, jsonRepresentation);
    }

    protected MBTilesMapLayer(Parcel parcel) {
        super(parcel);
        this.mLocalURL = parcel.readString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.mLocalURL);
    }

    public File getLocalFile() {
        if (TextUtils.isEmpty(this.mLocalURL)) {
            return null;
        }
        return new File (this.mLocalURL);
    }

    public void setLocalFile(File file) {
        if (file != null) {
            this.mLocalURL = file.getAbsolutePath();
        } else {
            this.mLocalURL = null;
        }
    }

    public boolean isAvailable() {
        File file = getLocalFile();
        return file != null && file.exists();
    }

    public void setAvailable(boolean available) {
    }

    public boolean isLocalOnly() {
        return TextUtils.isEmpty(this.mRemoteID);
    }

    public File generateStorageLocation(Context context, String remoteID) {
        return new File (getStorageDirectory(context), remoteID + ".mbtiles");
    }

    public String getType() {
        return TYPE;
    }

    protected ContentValues getContentValues() {
        ContentValues values = super.getContentValues();
        values.put(MapLayer.COLUMN_LOCAL_URL, this.mLocalURL);
        return values;
    }
}
