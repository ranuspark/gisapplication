package com.example.bhumicloud.gisapplications.model.validation;

import android.text.TextUtils;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.FormValueContainer;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.SectionElement;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Condition {
    public static final String CONTAINS = "contains";
    public static final String EQUAL_TO = "equal_to";
    public static final String GREATER_THAN = "greater_than";
    public static final String IS_EMPTY = "is_empty";
    public static final String IS_NOT_EMPTY = "is_not_empty";
    private static final String JSON_FIELD_KEY = "field_key";
    private static final String JSON_OPERATOR = "operator";
    private static final String JSON_VALUE = "value";
    public static final String LESS_THAN = "less_than";
    public static final String NOT_EQUAL_TO = "not_equal_to";
    public static final String STARTS_WITH = "starts_with";
    public static final String STATUS_ELEMENT_KEY = "@status";
    public static final String TYPE_ALL = "all";
    public static final String TYPE_ANY = "any";
    private final String mElementKey;
    private final String mOperator;
    private final String mValue;

    public static boolean shouldElementBeVisible(Element element, Record record, FormValueContainer values, HashMap<String, Boolean> cache) {
        if (cache != null && cache.containsKey(element.getKey())) {
            return ((Boolean) cache.get(element.getKey())).booleanValue();
        }
        if (cache == null) {
            cache = new HashMap();
        }
        boolean shouldBeVisible = shouldElementBeVisibleRecursive(element, record, values, cache);
        if (!(element instanceof SectionElement)) {
            return shouldBeVisible;
        }
        boolean hasVisibleChildren = false;
        Iterator it = ((SectionElement) element).getElements().iterator();
        while (it.hasNext()) {
            if (shouldElementBeVisibleRecursive((Element) it.next(), record, values, cache)) {
                hasVisibleChildren = true;
                break;
            }
        }
        shouldBeVisible = shouldBeVisible && hasVisibleChildren;
        return shouldBeVisible;
    }

    public static boolean shouldElementBeVisibleRecursive(Element element, Record record, FormValueContainer values, HashMap<String, Boolean> cache) {
        if (cache.containsKey(element.getKey())) {
            return ((Boolean) cache.get(element.getKey())).booleanValue();
        }
        cache.put(element.getKey(), Boolean.valueOf(true));
        if (element.getOverrideIsHidden() != null) {
            boolean z;
            String key = element.getKey();
            if (element.isHidden()) {
                z = false;
            } else {
                z = true;
            }
            cache.put(key, Boolean.valueOf(z));
            if (element.isHidden()) {
                return false;
            }
            return true;
        } else if (element.isHidden() || record.getForm().elementHasHiddenParent(element)) {
            cache.put(element.getKey(), Boolean.valueOf(false));
            return false;
        } else {
            boolean result;
            boolean shouldBeVisible = false;
            if (!element.hasVisibilityConditions()) {
                shouldBeVisible = true;
            } else if (TYPE_ANY.equals(element.getVisibleConditionsType())) {
                for (Condition condition : element.getVisibleConditions()) {
                    if (condition.isSatisfied(record, values, (HashMap) cache)) {
                        shouldBeVisible = true;
                        break;
                    }
                }
            } else if (TYPE_ALL.equals(element.getVisibleConditionsType())) {
                shouldBeVisible = true;
                for (Condition condition2 : element.getVisibleConditions()) {
                    if (!condition2.isSatisfied(record, values, (HashMap) cache)) {
                        shouldBeVisible = false;
                        break;
                    }
                }
            }
            boolean parentsVisible = true;
            Element iterator = record.getForm().getParentElement(element);
            while (iterator != null) {
                if (!shouldElementBeVisibleRecursive(iterator, record, values, cache)) {
                    parentsVisible = false;
                    break;
                }
                iterator = record.getForm().getParentElement(iterator);
            }
            if (parentsVisible && shouldBeVisible) {
                result = true;
            } else {
                result = false;
            }
            cache.put(element.getKey(), Boolean.valueOf(result));
            if (parentsVisible && shouldBeVisible) {
                return true;
            }
            return false;
        }
    }

    public static boolean shouldElementBeRequired(Element element, Record record, FormValueContainer values) {
        if (!element.hasRequirementConditions() || element.getOverrideIsRequired() != null) {
            return element.isRequired();
        }
        HashMap cache = new HashMap();
        if (TYPE_ANY.equals(element.getRequiredConditionsType())) {
            for (Condition condition : element.getRequiredConditions()) {
                if (condition.isSatisfied(record, values, cache)) {
                    return true;
                }
            }
            return false;
        } else if (!TYPE_ALL.equals(element.getRequiredConditionsType())) {
            return false;
        } else {
            for (Condition condition2 : element.getRequiredConditions()) {
                if (!condition2.isSatisfied(record, values, cache)) {
                    return false;
                }
            }
            return true;
        }
    }

    private static FormValue getValueForCondition(Condition condition, FormValueContainer values, Record record) {
        if (STATUS_ELEMENT_KEY.equals(condition.mElementKey)) {
            return record.getStatusValue();
        }
        return values.getFormValue(condition.getElementKey());
    }

    private static Element getElementForCondition(Condition condition, Record record) {
        if (STATUS_ELEMENT_KEY.equals(condition.getElementKey())) {
            return record.getStatusValue().getElement();
        }
        return record.getForm().getElement(condition.getElementKey());
    }

    private boolean isSatisfied(Record record, FormValueContainer values, HashMap<String, Boolean> cache) {
        Element referencedElement = getElementForCondition(this, record);
        boolean isReferencedFieldSatisfied = true;
        if (referencedElement != null) {
            boolean skipElement = referencedElement.isHidden() || record.getForm().elementHasHiddenParent(referencedElement);
            if (!skipElement) {
                isReferencedFieldSatisfied = shouldElementBeVisibleRecursive(referencedElement, record, values, cache);
            }
        }
        return isSatisfied(record, values, isReferencedFieldSatisfied);
    }

    private boolean isSatisfied(Record record, FormValueContainer values, boolean referencedFieldsSatisfied) {
        boolean z = false;
        FormValue value = null;
        if (referencedFieldsSatisfied) {
            value = getValueForCondition(this, values, record);
        }
        if (EQUAL_TO.equals(getOperator())) {
            return isEqual(value, getValue());
        }
        if (NOT_EQUAL_TO.equals(getOperator())) {
            if (isEqual(value, getValue())) {
                return false;
            }
            return true;
        } else if (IS_EMPTY.equals(getOperator())) {
            return isEmpty(value);
        } else {
            if (IS_NOT_EMPTY.equals(getOperator())) {
                if (isEmpty(value)) {
                    return false;
                }
                return true;
            } else if (CONTAINS.equals(getOperator())) {
                return contains(value, getValue());
            } else {
                if (STARTS_WITH.equals(getOperator())) {
                    return startsWith(value, getValue());
                }
                if (GREATER_THAN.equals(getOperator())) {
                    return isGreaterThan(value, getValue());
                }
                if (!LESS_THAN.equals(getOperator()) || isLessThan(value, getValue())) {
                    z = true;
                }
                return z;
            }
        }
    }

    private static boolean isEqual(FormValue recordValue, String testValue) {
        if (recordValue == null) {
            return TextUtils.isEmpty(testValue);
        }
        return recordValue.isEqualForConditions(testValue);
    }

    private static boolean isEmpty(FormValue recordValue) {
        return recordValue == null || recordValue.isEmpty();
    }

    private static boolean contains(FormValue recordValue, String testValue) {
        if (recordValue == null) {
            return TextUtils.isEmpty(testValue);
        }
        return recordValue.contains(testValue);
    }

    private static boolean startsWith(FormValue recordValue, String testValue) {
        if (recordValue == null) {
            return TextUtils.isEmpty(testValue);
        }
        return recordValue.startsWith(testValue);
    }

    private static boolean isLessThan(FormValue recordValue, String testValue) {
        if (recordValue == null) {
            return TextUtils.isEmpty(testValue);
        }
        return recordValue.isLessThan(testValue);
    }

    private static boolean isGreaterThan(FormValue recordValue, String testValue) {
        if (recordValue == null) {
            return TextUtils.isEmpty(testValue);
        }
        return recordValue.isGreaterThan(testValue);
    }

    public Condition(Map json) {
        this.mValue = JSONUtils.getString(json, "value");
        this.mOperator = JSONUtils.getString(json, JSON_OPERATOR);
        this.mElementKey = JSONUtils.getString(json, JSON_FIELD_KEY);
    }

    public String getElementKey() {
        return this.mElementKey;
    }

    public String getOperator() {
        return this.mOperator;
    }

    public String getValue() {
        return this.mValue;
    }

    public HashMap<String, String> toJSON() {
        HashMap<String, String> json = new HashMap();
        json.put("value", this.mValue);
        json.put(JSON_OPERATOR, this.mOperator);
        json.put(JSON_FIELD_KEY, this.mElementKey);
        return json;
    }
}
