CREATE TABLE IF NOT EXISTS Accounts (
    _id INTEGER PRIMARY KEY AUTOINCREMENT,
    user_id TEXT,
    first_name TEXT,
    last_name TEXT,
    user_email TEXT,
    context_id TEXT,
    context_name TEXT,
    context_type TEXT,
    role TEXT,
    token TEXT
);
CREATE INDEX IF NOT EXISTS accounts_user_id_idx ON Accounts (user_id);
CREATE INDEX IF NOT EXISTS accounts_context_id_idx ON Accounts (context_id);