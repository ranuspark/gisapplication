package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import com.example.bhumicloud.gisapplications.model.field.temporal.time.TimeElement;

public class TimeElementView extends ElementView<TimeElement> {
    private TimeElementViewListener mListener;

    public interface TimeElementViewListener {
        void onShowTimeFieldDialog(TimeElement timeElement);
    }

    public TimeElementView(Context context, TimeElement element, boolean viewOnly) {
        super(context, element, viewOnly);
    }

    public void setListener(TimeElementViewListener listener) {
        this.mListener = listener;
    }

    protected void onFieldViewClicked() {
        super.onFieldViewClicked();
        if (this.mListener != null) {
            this.mListener.onShowTimeFieldDialog((TimeElement) getElement());
        }
    }
}
