package com.example.bhumicloud.gisapplications.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.bhumicloud.gisapplications.RepeatableItemCollectionViewListener;
import com.google.zxing.client.android.Intents.Scan;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.Activity.GISActivity.OnBackPressedListener;
import com.example.bhumicloud.gisapplications.model.DefaultValues;
import com.example.bhumicloud.gisapplications.model.Feature;
import com.example.bhumicloud.gisapplications.model.FormValueContainer;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableElement;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableItemValue;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableValue;
import com.example.bhumicloud.gisapplications.model.validation.error.FieldLengthValidationError;
import com.example.bhumicloud.gisapplications.util.FileBasedStateStorage;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.widget.SearchRelativeLayout;
import com.example.bhumicloud.gisapplications.widget.SearchRelativeLayout.OnSearchQueryChangedListener;
import com.journeyapps.barcodescanner.CaptureActivity;

import java.util.ArrayList;
import java.util.List;

public class RepeatableIndexFragment extends GISFragment implements OnBackPressedListener, RecordEditorFragmentListener, RepeatableItemCollectionViewListener {
    private static final String STATE_CONDITIONAL_VALUES = "STATE_CONDITIONAL_VALUES";
    private static final String STATE_FILE_SAVED_STORAGE = "STATE_FILE_SAVED_STORAGE";
    private static final String STATE_HAS_CHANGES = "STATE_HAS_CHANGES";
    private static final String STATE_MAP_VIEW_MODE = "STATE_MAP_VIEW_MODE";
    private static final String STATE_REPEATABLE_VALUE = "STATE_REPEATABLE_VALUE";
    private static final String STATE_SEARCH_QUERY = "STATE_SEARCH_QUERY";
    private static final String STATE_SEARCH_VISIBLE = "STATE_SEARCH_VISIBLE";
    private static final String STATE_VIEW_ONLY_MODE = "STATE_VIEW_ONLY_MODE";
    private RepeatableItemCollectionView mCollectionView;
    private MenuItem mDoneMenuItem;
    private MenuItem mEditModeMenuItem;
    private boolean mHasChanges;
    private boolean mMapViewMode;
    private FormValueContainer mParentConditionalValues;
    private RepeatableValue mRepeatableValue;
    private EditText mSearchEditText;
    private MenuItem mSearchMenuItem;
    private String mSearchQuery;
    private boolean mSearchVisible;
    private boolean mViewOnlyMode;
    private MenuItem mViewToggleMenuItem;

    public interface RepeatableIndexFragmentListener {
        void onRepeatableValueChanged(RepeatableValue repeatableValue);
    }

    class C11151 implements OnSearchQueryChangedListener {
        C11151() {
        }

        public void onSearchQueryChanged(String query) {
            RepeatableIndexFragment.this.mSearchQuery = query;
            RepeatableIndexFragment.this.reloadCollectionView();
        }
    }
    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        if (savedState != null) {
            this.mSearchQuery = savedState.getString(STATE_SEARCH_QUERY);
            this.mMapViewMode = savedState.getBoolean(STATE_MAP_VIEW_MODE, false);
            this.mSearchVisible = savedState.getBoolean(STATE_SEARCH_VISIBLE, false);
            this.mHasChanges = savedState.getBoolean(STATE_HAS_CHANGES, false);
            this.mViewOnlyMode = savedState.getBoolean(STATE_VIEW_ONLY_MODE, false);
            FileBasedStateStorage stateStorage = (FileBasedStateStorage) savedState.getParcelable(STATE_FILE_SAVED_STORAGE);
            if (stateStorage != null) {
                this.mRepeatableValue = (RepeatableValue) stateStorage.get(STATE_REPEATABLE_VALUE);
                this.mParentConditionalValues = (FormValueContainer) stateStorage.get(STATE_CONDITIONAL_VALUES);
            } else {
                GISLogger.log("FileBasedStateStorage was null");
            }
        }
        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repeatable_index, container, false);
        RepeatableElement element = getElement();
        if (element != null) {
            getActivity().setTitle(element.getLabel());
        }
        return view;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Fragment collectionView = getChildFragmentManager().findFragmentById(R.id.repeatable_fragment_container);
        if (collectionView instanceof RepeatableItemCollectionView) {
            this.mCollectionView = (RepeatableItemCollectionView) collectionView;
        }
        configureCollectionView();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_repeatable_index, menu);
        this.mSearchMenuItem = menu.findItem(R.id.menu_item_repeatable_search);
        this.mViewToggleMenuItem = menu.findItem(R.id.menu_item_view_toggle);
        this.mEditModeMenuItem = menu.findItem(R.id.menu_item_edit);
        this.mDoneMenuItem = menu.findItem(R.id.menu_item_done);
        final SearchRelativeLayout searchView = (SearchRelativeLayout) MenuItemCompat.getActionView(this.mSearchMenuItem);
        this.mSearchEditText = searchView.getSearchEditText();
        this.mSearchEditText.setHint(R.string.search_items);
        searchView.setOnSearchQueryChangedListener(new C11151());
        MenuItemCompat.setOnActionExpandListener(this.mSearchMenuItem, new OnActionExpandListener () {
            public boolean onMenuItemActionExpand(MenuItem item) {
                RepeatableIndexFragment.this.mSearchVisible = true;
                searchView.onMenuItemActionExpand(item);
                return true;
            }

            public boolean onMenuItemActionCollapse(MenuItem item) {
                RepeatableIndexFragment.this.mSearchVisible = false;
                searchView.onMenuItemActionCollapse(item);
                return true;
            }
        });
        this.mSearchEditText.setText(this.mSearchQuery);
        if (this.mSearchVisible) {
            this.mSearchMenuItem.expandActionView();
            if (this.mSearchQuery != null) {
                this.mSearchEditText.setSelection(this.mSearchQuery.length());
            }
        }
        if (!getElement().isGeometryEnabled()) {
            this.mViewToggleMenuItem.setVisible(false);
            this.mViewToggleMenuItem.setEnabled(false);
        }
        configureViewModeMenuItems();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_view_toggle:
                onToggleViewModeOptionSelected();
                return true;
            case R.id.menu_item_edit:
                switchToEditMode();
                return true;
            case R.id.menu_item_done:
                switchFromEditMode();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_SEARCH_QUERY, this.mSearchQuery);
        outState.putBoolean(STATE_MAP_VIEW_MODE, this.mMapViewMode);
        outState.putBoolean(STATE_SEARCH_VISIBLE, this.mSearchVisible);
        outState.putBoolean(STATE_HAS_CHANGES, this.mHasChanges);
        outState.putBoolean(STATE_VIEW_ONLY_MODE, this.mViewOnlyMode);
        FileBasedStateStorage stateStorage = new FileBasedStateStorage(getActivity());
        stateStorage.store(STATE_REPEATABLE_VALUE, this.mRepeatableValue);
        stateStorage.store(STATE_CONDITIONAL_VALUES, this.mParentConditionalValues);
        outState.putParcelable(STATE_FILE_SAVED_STORAGE, stateStorage);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case 106:
                    String contents = data.getStringExtra( Scan.RESULT);
                    if (contents != null) {
                        if (this.mSearchEditText == null) {
                            this.mSearchQuery = contents;
                            break;
                        }
                        this.mSearchEditText.setText(contents);
                        this.mSearchEditText.setSelection(contents.length());
                        break;
                    }
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public boolean onBackPressed() {
        if (this.mHasChanges) {
            onRepeatableValueChanged();
        }
        return true;
    }

    public void onFeatureEdited(Feature feature, boolean autoSave) {
        logDebugEvent("onFeatureEdited");
        if (feature instanceof RepeatableItemValue) {
            this.mRepeatableValue.insertItem((RepeatableItemValue) feature);
            this.mHasChanges = true;
            onRepeatableValueChanged();
        }
        reloadCollectionView();
    }

    public void onFeatureDeleted(Feature feature) {
        logDebugEvent("onFeatureDeleted");
        onDeleted((RepeatableItemValue) feature);
    }

    public void onDeleted(RepeatableItemValue value) {
        this.mHasChanges = true;
        this.mRepeatableValue.removeItem(value);
        onRepeatableValueChanged();
        reloadCollectionView();
    }

    public void onSelected(RepeatableItemValue itemValue) {
        RecordEditorFragment editor = createEditorFragmentForRepeatableItem(itemValue);
        editor.setViewOnlyMode(this.mViewOnlyMode);
        getRecordActivity().pushFragment(this, editor, getElement().getKey());
    }

    public void onAdded(RepeatableItemValue itemValue) {
        RecordEditorFragment editor = createEditorFragmentForRepeatableItem(itemValue);
        editor.setAddMode(true);
        getRecordActivity().pushFragment(this, editor, getElement().getKey());
    }

    public void onShowOnMap(RepeatableItemValue value) {
        if (!this.mMapViewMode) {
            onToggleViewModeOptionSelected();
        }
        ((MapViewFragment) this.mCollectionView).zoomToFeature((long) value.getIndex(), value.getLatLng());
    }

    public RecordEditorActivity getRecordActivity() {
        return (RecordEditorActivity) getActivity();
    }

    public void setRepeatableValue(RepeatableValue value) {
        this.mRepeatableValue = value;
    }

    public RepeatableElement getElement() {
        return this.mRepeatableValue.getElement();
    }

    public void setViewOnlyMode(boolean viewOnlyMode) {
        this.mViewOnlyMode = viewOnlyMode;
    }

    public void setParentValuesForConditions(FormValueContainer values) {
        this.mParentConditionalValues = values;
    }

    public void onAddNewItemOptionSelected() {
        int i = 0;
        RepeatableElement element = getElement();
        if (!element.hasMaxLength() || this.mRepeatableValue == null || this.mRepeatableValue.length() < element.getMaxLength()) {
            if (this.mRepeatableValue != null) {
                i = this.mRepeatableValue.length();
            }
            onAdded(new RepeatableItemValue(element, i));
            return;
        }
        Toast.makeText(getActivity(), new FieldLengthValidationError(element).getMessage(), Toast.LENGTH_SHORT).show();
    }

    private RecordEditorFragment createEditorFragmentForRepeatableItem(RepeatableItemValue itemValue) {
        RecordEditorFragment editor = new RecordEditorFragment();
        editor.setRootEditor(true);
        editor.setElementContainer(getElement());
        editor.setTargetFragment(this, 0);
        editor.setParentValuesForConditions(this.mParentConditionalValues);
        RepeatableItemValue value = new RepeatableItemValue(itemValue);
        editor.setFeatureStateBeforeEdit(value);
        DefaultValues.applyStickyDefaults(getRecordActivity().getRecord().getForm(), value);
        DefaultValues.applyDefaultValues(getElement().getElements(), value.getFormValues(), null);
        editor.setCurrentFeature(value);
        return editor;
    }

    private void onToggleViewModeOptionSelected() {
        this.mMapViewMode = !this.mMapViewMode;
        configureCollectionView();
        configureViewModeMenuItems();
    }

    private void configureViewModeMenuItems() {
        if (this.mMapViewMode) {
            this.mViewToggleMenuItem.setIcon(R.drawable.ic_action_list);
            this.mViewToggleMenuItem.setTitle(R.string.list_view);
            this.mEditModeMenuItem.setEnabled(false);
            return;
        }
        this.mViewToggleMenuItem.setIcon(R.drawable.ic_action_map);
        this.mViewToggleMenuItem.setTitle(R.string.map_view);
        this.mEditModeMenuItem.setEnabled(true);
    }

    private void configureCollectionView() {
        if (this.mMapViewMode) {
            showMapView(false);
        } else {
            showListView(false);
        }
        reloadCollectionView();
    }

    private void reloadCollectionView() {
        if (this.mCollectionView != null) {
            this.mCollectionView.setViewOnlyMode(this.mViewOnlyMode);
            List<RepeatableItemValue> items = this.mRepeatableValue.getSortedList();
            if (TextUtils.isEmpty(this.mSearchQuery)) {
                this.mCollectionView.setRepeatableItems(items);
                return;
            }
            List<RepeatableItemValue> filtered = new ArrayList ();
            for (RepeatableItemValue item : items) {
                if (item.getSearchableValue().toLowerCase().contains(this.mSearchQuery.toLowerCase())) {
                    filtered.add(item);
                }
            }
            this.mCollectionView.setRepeatableItems(filtered);
        }
    }

    private void showMapView(boolean async) {
        RepeatableMapFragment map = new RepeatableMapFragment();
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.repeatable_fragment_container, map);
        //ft.setCustomAnimations(17432576, 17432577);
        ft.commit();
        if (!async) {
            fm.executePendingTransactions();
        }
        this.mCollectionView = map;
    }

    private void showListView(boolean async) {
        RepeatableListFragment list = this.mCollectionView instanceof RepeatableListFragment ? (RepeatableListFragment) this.mCollectionView : new RepeatableListFragment();
        if (!list.isAdded()) {
            FragmentManager fm = getChildFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.repeatable_fragment_container, list);
           // ft.setCustomAnimations(17432576, 17432577);
            ft.commit();
            if (!async) {
                fm.executePendingTransactions();
            }
        }
        this.mCollectionView = list;
    }

    private void onRepeatableValueChanged() {
        if (this.mParentConditionalValues != null) {
            this.mParentConditionalValues.setFormValue(this.mRepeatableValue.getElement(), this.mRepeatableValue);
        }
        Fragment target = getTargetFragment();
        if (target != null && (target instanceof RepeatableIndexFragmentListener)) {
            ((RepeatableIndexFragmentListener) target).onRepeatableValueChanged(this.mRepeatableValue);
        }
    }

    private void switchToEditMode() {
        this.mViewToggleMenuItem.setVisible(false);
        this.mSearchMenuItem.collapseActionView();
        this.mSearchMenuItem.setVisible(false);
        this.mEditModeMenuItem.setEnabled(false);
        this.mDoneMenuItem.setVisible(true);
        this.mCollectionView.setEditMode(true);
    }

    private void switchFromEditMode() {
        this.mCollectionView.setEditMode(false);
        this.mViewToggleMenuItem.setVisible(true);
        this.mSearchMenuItem.setVisible(true);
        if (this.mEditModeMenuItem != null) {
            this.mEditModeMenuItem.setEnabled(true);
        }
        if (this.mDoneMenuItem != null) {
            this.mDoneMenuItem.setVisible(false);
        }
        List<RepeatableItemValue> items = this.mCollectionView.getItems();
        RepeatableValue.rebuildIndexes(items);
        if (RepeatableValue.isOrderDifferent(this.mRepeatableValue.getSortedList(), items)) {
            this.mRepeatableValue.setItems(this.mCollectionView.getItems());
            this.mHasChanges = true;
            onRepeatableValueChanged();
            reloadCollectionView();
        }
    }
}
