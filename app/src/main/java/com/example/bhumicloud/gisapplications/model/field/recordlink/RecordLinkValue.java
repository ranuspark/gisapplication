package com.example.bhumicloud.gisapplications.model.field.recordlink;

import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.MultipleValueItem;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class RecordLinkValue implements FormValue {
    private final ArrayList<RecordLinkItemValue> mItems = new ArrayList();
    private RecordLinkElement mRecordLinkElement;

    public RecordLinkValue(RecordLinkElement element) {
        this.mRecordLinkElement = element;
    }

    public RecordLinkValue(RecordLinkElement element, List json) {
        this.mRecordLinkElement = element;
        if (json != null && !json.isEmpty()) {
            for (int i = 0; i < json.size(); i++) {
                Map itemJSON = JSONUtils.getHashMap(json, i);
                if (itemJSON != null) {
                    this.mItems.add(new RecordLinkItemValue(itemJSON));
                }
            }
        }
    }

    public Element getElement() {
        return this.mRecordLinkElement;
    }

    public String getDisplayValue() {
        switch (this.mItems.size()) {
            case 0:
                return null;
            case 1:
                Record record = ((RecordLinkItemValue) this.mItems.get(0)).getRecord();
                if (record != null) {
                    String title = record.getTitle();
                    if (title != null) {
                        return title;
                    }
                }
                break;
        }
        return GIS.getInstance().getResources().getQuantityString(R.plurals.record_count, this.mItems.size(), new Object[]{Integer.valueOf(this.mItems.size())});
    }

    public String getSearchableValue() {
        if (this.mItems.isEmpty()) {
            return null;
        }
        ArrayList<String> titles = new ArrayList();
        Iterator it = this.mItems.iterator();
        while (it.hasNext()) {
            String title = ((RecordLinkItemValue) it.next()).getRecordTitle();
            if (!TextUtils.isEmpty(title)) {
                titles.add(title);
            }
        }
        return TextUtils.join(" ", titles);
    }

    public int length() {
        return this.mItems.size();
    }

    public ArrayList<HashMap<String, String>> toJSON() {
        if (isEmpty()) {
            return null;
        }
        ArrayList<HashMap<String, String>> array = new ArrayList();
        Iterator it = this.mItems.iterator();
        while (it.hasNext()) {
            array.add(((RecordLinkItemValue) it.next()).toJSON());
        }
        return array;
    }

    public boolean isEmpty() {
        return this.mItems.size() < 1;
    }

    public Object getColumnValue() {
        return null;
    }

    public ArrayList<MultipleValueItem> getMultipleValues() {
        ArrayList<MultipleValueItem> values = new ArrayList();
        Iterator it = this.mItems.iterator();
        while (it.hasNext()) {
            values.add(new MultipleValueItem(getElement(), ((RecordLinkItemValue) it.next()).getRecordIdentifier()));
        }
        return values;
    }

    public boolean isEqualForConditions(String testValue) {
        return false;
    }

    public boolean contains(String testValue) {
        return false;
    }

    public boolean startsWith(String testValue) {
        return false;
    }

    public boolean isLessThan(String testValue) {
        try {
            return ((double) this.mItems.size()) < Double.parseDouble(testValue);
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean isGreaterThan(String testValue) {
        try {
            return ((double) this.mItems.size()) > Double.parseDouble(testValue);
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public ArrayList<RecordLinkItemValue> getRecordLinks() {
        return this.mItems;
    }

    public RecordLinkItemValue getLastRecordLinkItemValue() {
        return isEmpty() ? null : (RecordLinkItemValue) this.mItems.get(this.mItems.size() - 1);
    }

    public ArrayList<String> getRecordLinkIdentifiers() {
        ArrayList<String> identifiers = new ArrayList();
        Iterator it = this.mItems.iterator();
        while (it.hasNext()) {
            identifiers.add(((RecordLinkItemValue) it.next()).getRecordIdentifier());
        }
        return identifiers;
    }

    public void addRecordLink(RecordLinkItemValue value) {
        this.mItems.add(value);
    }

    public void remove(RecordLinkItemValue value) {
        this.mItems.remove(value);
    }
}
