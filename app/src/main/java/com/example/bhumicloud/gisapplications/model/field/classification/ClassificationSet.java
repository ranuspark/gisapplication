package com.example.bhumicloud.gisapplications.model.field.classification;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.dossier.PersistentObject;
import com.example.bhumicloud.gisapplications.dossier.PersistentObject.QueryField;
import com.example.bhumicloud.gisapplications.dossier.PersistentStore;
import com.example.bhumicloud.gisapplications.dossier.RowCache;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.util.CursorUtils;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClassificationSet extends PersistentObject {
    @QueryField("account_id")
    public static final String COLUMN_ACCOUNT_ID = "account_id";
    @QueryField("description")
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_ITEMS = "items";
    @QueryField("name")
    public static final String COLUMN_NAME = "name";
    @QueryField("remote_id")
    public static final String COLUMN_REMOTE_ID = "remote_id";
    private static final ArrayList<String> QUERY_COLUMNS = PersistentObject.buildColumns(ClassificationSet.class);
    public static final String TABLE_NAME = "FMClassificationSets";

    private long mAccountID;
    private String mDescription;
    private ArrayList<ClassificationItem> mItems;
    private ArrayList<Object> mItemsJSON;
    private String mName;
    private String mRemoteID;

    private static RowCache<ClassificationSet> sRowCache = new RowCache<ClassificationSet>(50) {};

    public static synchronized ClassificationSet getClassificationSet(long id) {
        ClassificationSet classificationSet;
        synchronized (ClassificationSet.class) {
            classificationSet = (ClassificationSet) sRowCache.get(Long.valueOf(id));
        }
        return classificationSet;
    }

    public static synchronized void clearCache() {
        synchronized (ClassificationSet.class) {
            sRowCache.evictAll();
        }
    }

    public static ClassificationSet getClassificationSet(Account account, String remoteID) {
        ClassificationSet result = null;
        SQLiteDatabase db = GIS.getDatabase();
        List<String> predicates = new ArrayList();
        List<String> valuesList = new ArrayList();
        if (account != null) {
            predicates.add("account_id = ?");
            valuesList.add(String.valueOf(account.getRowID()));
        }
        if (!TextUtils.isEmpty(remoteID)) {
            predicates.add("remote_id = ?");
            valuesList.add(remoteID);
        }
        String[] columns = new String[]{"_id"};
        Cursor c = db.query(TABLE_NAME, columns, TextUtils.join(" AND ", predicates), (String[]) valuesList.toArray(new String[valuesList.size()]), null, null, null);
        if (c != null) {
            c.moveToFirst();
            if (!c.isAfterLast()) {
                result = getClassificationSet(c.getLong(0));
            }
            c.close();
        }
        return result;
    }

    public static void truncate(ArrayList<String> excludedRemoteIDs, Account account) {
        SQLiteDatabase db = PersistentStore.getInstance(GIS.getInstance()).getWritableDatabase();
        ArrayList<String> predicates = new ArrayList();
        ArrayList<String> paramsList = new ArrayList();
        if (account != null) {
            predicates.add("account_id = ?");
            paramsList.add(String.valueOf(account.getRowID()));
        }
        if (excludedRemoteIDs != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("remote_id NOT IN (");
            for (int i = 0; i < excludedRemoteIDs.size(); i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append("'").append((String) excludedRemoteIDs.get(i)).append("'");
            }
            sb.append(")");
            predicates.add(sb.toString());
        }
        db.delete(TABLE_NAME, TextUtils.join(" AND ", predicates), (String[]) paramsList.toArray(new String[paramsList.size()]));
    }

    public ClassificationSet(Map jsonRepresentation) {
        setAttributesFromJSON(jsonRepresentation);
    }

    public ClassificationSet(Cursor cursor, SQLiteDatabase db) {
        super(cursor);
        this.mRemoteID = cursor.getString(cursor.getColumnIndex("remote_id"));
        this.mAccountID = cursor.getLong(cursor.getColumnIndex("account_id"));
        this.mName = cursor.getString(cursor.getColumnIndex("name"));
        this.mDescription = cursor.getString(cursor.getColumnIndex("description"));
        try {
            this.mItemsJSON = JSONUtils.arrayFromJSON(CursorUtils.fetchLargeColumn(db, TABLE_NAME, cursor, COLUMN_ITEMS));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private ClassificationSet(Parcel source) {
        super(source);
        boolean isChoicesJSON = true;
        this.mRemoteID = source.readString();
        this.mAccountID = source.readLong();
        this.mName = source.readString();
        this.mDescription = source.readString();
        if (source.readInt() != 1) {
            isChoicesJSON = false;
        }
        if (isChoicesJSON) {
            this.mItemsJSON = new ArrayList();
            source.readList(this.mItemsJSON, null);
        }
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(getRemoteID());
        dest.writeLong(this.mAccountID);
        dest.writeString(getName());
        dest.writeString(getDescription());
        if (this.mItemsJSON != null) {
            dest.writeInt(1);
            dest.writeList(this.mItemsJSON);
            return;
        }
        dest.writeInt(0);
    }

    public String getRemoteID() {
        return this.mRemoteID;
    }

    public String getName() {
        return this.mName;
    }

    public String getDescription() {
        return this.mDescription;
    }

    public ArrayList<ClassificationItem> getItems() {
        if (this.mItems != null) {
            return this.mItems;
        }
        this.mItems = new ArrayList();
        if (this.mItemsJSON != null) {
            for (int i = 0; i < this.mItemsJSON.size(); i++) {
                Map itemJSON = JSONUtils.getHashMap(this.mItemsJSON, i);
                if (itemJSON != null) {
                    this.mItems.add(new ClassificationItem(null, itemJSON));
                }
            }
        }
        return this.mItems;
    }

    public void setAttributesFromJSON(Map json) {
        this.mName = JSONUtils.getString(json, "name");
        this.mDescription = JSONUtils.getString(json, "description");
        this.mItemsJSON = JSONUtils.getArrayList(json, COLUMN_ITEMS);
        this.mItems = null;
    }

    public void setAccount(Account account) {
        if (account != null) {
            this.mAccountID = account.getRowID();
        } else {
            this.mAccountID = 0;
        }
    }

    protected ContentValues getContentValues() {
        ContentValues values = super.getContentValues();
        values.put("remote_id", getRemoteID());
        values.put("account_id", Long.valueOf(this.mAccountID));
        values.put("name", getName());
        values.put("description", getDescription());
        values.put(COLUMN_ITEMS, JSONUtils.toJSONString(this.mItemsJSON));
        return values;
    }

    protected String getTableName() {
        return TABLE_NAME;
    }
}
