package com.example.bhumicloud.gisapplications.util;

import android.annotation.TargetApi;
import android.media.CamcorderProfile;

public class GISCamcorderProfile {
    private static final String QUALITY_STRING_1080p = "1080p";
    private static final String QUALITY_STRING_480p = "480p";
    private static final String QUALITY_STRING_720p = "720p";
    private static final String QUALITY_STRING_CIF = "CIF (352 x 288)";
    private static final String QUALITY_STRING_LOW = "Low Quality";
    private static final String QUALITY_STRING_QCIF = "QCIF (176 x 144)";
    private static final String QUALITY_STRING_QVGA = "QVGA (320 x 240)";
    private static final String QUALITY_STRING_UHD_4K = "UHD 4K";
    private final int mCamcorderProfile;
    private final String mUserFriendlyString;

    public GISCamcorderProfile() {
        this(getDefaultCamcorderProfile());
    }

    public GISCamcorderProfile(int camcorderProfile) {
        if (camcorderProfile == 0) {
            camcorderProfile = translateLowQualityCamcorderProfile();
        }
        this.mCamcorderProfile = camcorderProfile;
        this.mUserFriendlyString = getUserFriendlyString(this.mCamcorderProfile);
    }

    public String getUserFriendlyString() {
        return this.mUserFriendlyString;
    }

    public int getCamcorderProfile() {
        return this.mCamcorderProfile;
    }

    private static int getDefaultCamcorderProfile() {
        if (CamcorderProfile.hasProfile(5)) {
            return 5;
        }
        if (CamcorderProfile.hasProfile(4)) {
            return 4;
        }
        if (CamcorderProfile.hasProfile(3)) {
            return 3;
        }
        if (CamcorderProfile.hasProfile(2)) {
            return 2;
        }
        return 0;
    }

    @TargetApi(21)
    private static int translateLowQualityCamcorderProfile() {
        CamcorderProfile profile = CamcorderProfile.get(0);
        if (profile == null) {
            return 0;
        }
        int height = profile.videoFrameHeight;
        int width = profile.videoFrameWidth;
        if (height == 2160 && width == 3840 && DeviceInfo.isLollipop()) {
            return 8;
        }
        if (height == 240 && width == 320) {
            return 7;
        }
        if (height == 1080 && width == 1920) {
            return 6;
        }
        if (height == 720 && width == 1280) {
            return 5;
        }
        if (height == 480 && width == 720) {
            return 4;
        }
        if (height == 288 && width == 352) {
            return 3;
        }
        if (height == 144 && width == 176) {
            return 2;
        }
        return 0;
    }

    private static String getUserFriendlyString(int camcorderProfile) {
        switch (camcorderProfile) {
            case 2:
                return QUALITY_STRING_QCIF;
            case 3:
                return QUALITY_STRING_CIF;
            case 4:
                return QUALITY_STRING_480p;
            case 5:
                return QUALITY_STRING_720p;
            case 6:
                return QUALITY_STRING_1080p;
            case 7:
                return QUALITY_STRING_QVGA;
            case 8:
                return QUALITY_STRING_UHD_4K;
            default:
                return QUALITY_STRING_LOW;
        }
    }
}
