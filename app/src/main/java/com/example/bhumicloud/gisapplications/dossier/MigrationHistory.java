package com.example.bhumicloud.gisapplications.dossier;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.bhumicloud.gisapplications.util.GISLogger;

public class MigrationHistory {
    private static final String DEBUG_TAG = MigrationHistory.class.getSimpleName();
    private static final String PREFERENCES_NAME = "GIS.Migration.History";
    private static final String _001_RECORDS_SEARCH_INDEX_KEY = "RecordsSearchIndexKey";
    private static final String _002_ATTACHMENTS_LOCAL_URI_CONVERSION_KEY = "AttachmentsConvertLocalUriKey";
    private static final String _003_MAP_LAYERS_LOCAL_PATHS_KEY = "MapLayersLocalPathsKey";
    private static final String _004_ATTACHMENT_THUMBNAIL_PATHS_KEY = "AttachmentThumbnailPathsKey";
    private static final String _005_INTO_FORM_SCHEMA_TABLES_KEY = "IntoFormSchemaTablesKey";
    private static final String _006_APPLICATION_SHARED_PREFS_TO_PER_USER = "ApplicationSharedPrefsToPerUser";

    public static boolean _001_hasRecordsBeenAddedToSearchIndex(Context context) {
        return getMigrationHistory(context).getBoolean(_001_RECORDS_SEARCH_INDEX_KEY, false);
    }

    public static void _001_setRecordsAddedToSearchIndex(Context context, boolean completed) {
        GISLogger.log(DEBUG_TAG, "Setting Records Added to Search Index Migration: " + completed);
        getMigrationHistory(context).edit().putBoolean(_001_RECORDS_SEARCH_INDEX_KEY, completed).apply();
    }

    public static boolean _002_hasConvertedAttachmentLocalURIs(Context context) {
        return getMigrationHistory(context).getBoolean(_002_ATTACHMENTS_LOCAL_URI_CONVERSION_KEY, false);
    }

    public static void _002_setConvertedAttachmentLocalURIs(Context context, boolean completed) {
        GISLogger.log(DEBUG_TAG, "Setting Attachment Local URI Format Migration: " + completed);
        getMigrationHistory(context).edit().putBoolean(_002_ATTACHMENTS_LOCAL_URI_CONVERSION_KEY, completed).apply();
    }

    public static boolean _003_hasMigratedMapLayerLocalPaths(Context context) {
        return getMigrationHistory(context).getBoolean(_003_MAP_LAYERS_LOCAL_PATHS_KEY, false);
    }

    public static void _003_setMigratedMapLayerLocalPaths(Context context, boolean completed) {
        GISLogger.log(DEBUG_TAG, "Setting Map Layer Local Path Migration: " + completed);
        getMigrationHistory(context).edit().putBoolean(_003_MAP_LAYERS_LOCAL_PATHS_KEY, completed).apply();
    }

    public static boolean _004_hasMigratedAttachmentThumbnailPaths(Context context) {
        return getMigrationHistory(context).getBoolean(_004_ATTACHMENT_THUMBNAIL_PATHS_KEY, false);
    }

    public static void _004_setMigratedAttachmentThumbnailPaths(Context context, boolean completed) {
        GISLogger.log(DEBUG_TAG, "Setting Attachment Thumbnail Path Migration: " + completed);
        getMigrationHistory(context).edit().putBoolean(_004_ATTACHMENT_THUMBNAIL_PATHS_KEY, completed).apply();
    }

    public static boolean _005_hasCreatedAndSavedIntoFormSchemaTables(Context context) {
        return getMigrationHistory(context).getBoolean(_005_INTO_FORM_SCHEMA_TABLES_KEY, false);
    }

    public static void _005_setCreatedAndSavedIntoFormSchemaTables(Context context, boolean completed) {
        GISLogger.log(DEBUG_TAG, "Setting Form Schema Tables Migration: " + completed);
        getMigrationHistory(context).edit().putBoolean(_005_INTO_FORM_SCHEMA_TABLES_KEY, completed).apply();
    }

    public static boolean _006_hasCopiedApplicationSharedPrefsToPerUser(Context context) {
        return getMigrationHistory(context).getBoolean(_006_APPLICATION_SHARED_PREFS_TO_PER_USER, false);
    }

    public static void _006_setCopiedApplicationSharedPrefsToPerUser(Context context, boolean completed) {
        GISLogger.log(DEBUG_TAG, "Setting Copied Application Shared Prefs to Per User: " + completed);
        getMigrationHistory(context).edit().putBoolean(_006_APPLICATION_SHARED_PREFS_TO_PER_USER, completed).apply();
    }

    public static boolean isMigrationRequired(Context context) {
        if (_001_hasRecordsBeenAddedToSearchIndex(context) && _002_hasConvertedAttachmentLocalURIs(context) && _003_hasMigratedMapLayerLocalPaths(context) && _004_hasMigratedAttachmentThumbnailPaths(context) && _005_hasCreatedAndSavedIntoFormSchemaTables(context) && _006_hasCopiedApplicationSharedPrefsToPerUser(context)) {
            return false;
        }
        return true;
    }

    private static SharedPreferences getMigrationHistory(Context context) {
        return context.getApplicationContext().getSharedPreferences(PREFERENCES_NAME, 0);
    }
}
