package com.example.bhumicloud.gisapplications.View;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsIntent.Builder;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.customtabs.CustomTabsSession;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.textual.hyperlink.HyperlinkElement;
import com.example.bhumicloud.gisapplications.model.field.textual.hyperlink.HyperlinkValue;
import com.example.bhumicloud.gisapplications.util.IntentUtils;

public class HyperlinkElementView extends ElementView<HyperlinkElement> {
    private boolean mBound;
    private CustomTabsServiceConnection mConnection = new C12341();
    private CustomTabsClient mCustomTabsClient;
    private CustomTabsSession mCustomTabsSession;
    private HyperlinkElementViewListener mHyperlinkElementViewListener;
    private Uri mUri;

    public interface HyperlinkElementViewListener {
        void onClick(Element element);
    }

    class C12341 extends CustomTabsServiceConnection {
        C12341() {
        }

        public void onCustomTabsServiceConnected(ComponentName name, CustomTabsClient client) {
            HyperlinkElementView.this.mBound = true;
            HyperlinkElementView.this.mCustomTabsClient = client;
            HyperlinkElementView.this.mCustomTabsClient.warmup(0);
            if (HyperlinkElementView.this.mCustomTabsSession == null) {
                HyperlinkElementView.this.mCustomTabsSession = HyperlinkElementView.this.mCustomTabsClient.newSession(null);
            }
            if (HyperlinkElementView.this.mCustomTabsSession != null) {
                HyperlinkElementView.this.mCustomTabsSession.mayLaunchUrl(HyperlinkElementView.this.mUri, null, null);
            }
        }

        public void onServiceDisconnected(ComponentName name) {
            HyperlinkElementView.this.mBound = false;
            HyperlinkElementView.this.mCustomTabsClient = null;
            HyperlinkElementView.this.mCustomTabsSession = null;
        }
    }

    class C12352 implements OnClickListener {
        C12352() {
        }

        public void onClick(View v) {
            HyperlinkElementView.this.onHyperlinkClicked();
        }
    }

    public HyperlinkElementView(Context context, HyperlinkElement element, boolean viewOnly) {
        super(context, element, viewOnly);
        Button hyperlinkButton = (Button) findViewById(R.id.hyperlink_button);
        if (element.hasDescription()) {
            hyperlinkButton.setText(element.getDescription());
        } else {
            hyperlinkButton.setText(element.getLabel());
        }
        hyperlinkButton.setOnClickListener(new C12352());
    }

    public void setValue(FormValue value) {
        super.setValue(value);
        HyperlinkElement element = (HyperlinkElement) getElement();
        Uri link = null;
        if ((value instanceof HyperlinkValue) && !value.isEmpty()) {
            link = ((HyperlinkValue) value).getURL();
        } else if (element.hasDefaultURL()) {
            link = element.getDefaultURL();
        }
        Context context = getContext();
        if (link != null) {
            if (!link.isAbsolute()) {
                link = link.buildUpon().scheme("http").build();
            }
            if (IntentUtils.isIntentSafe(context, new Intent ("android.intent.action.VIEW", link))) {
                this.mUri = link;
                if (!IntentUtils.getNativeAppPackages(context, this.mUri).isEmpty()) {
                    if (this.mBound) {
                        context.unbindService(this.mConnection);
                    }
                    this.mCustomTabsSession = null;
                } else if (!this.mBound) {
                    this.mBound = CustomTabsClient.bindCustomTabsService(context, "com.android.chrome", this.mConnection);
                } else if (this.mCustomTabsSession == null && this.mCustomTabsClient != null) {
                    this.mCustomTabsSession = this.mCustomTabsClient.newSession(null);
                }
                if (this.mCustomTabsSession != null) {
                    this.mCustomTabsSession.mayLaunchUrl(this.mUri, null, null);
                    return;
                }
                return;
            }
        }
        if (this.mBound) {
            context.unbindService(this.mConnection);
        }
    }

    protected void onVisibilityChanged(@NonNull View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        if (visibility == 0 && this.mCustomTabsSession != null) {
            this.mCustomTabsSession.mayLaunchUrl(this.mUri, null, null);
        }
    }

    public void onDestroy(Context context) {
        super.onDestroy(context);
        if (this.mBound) {
            context.unbindService(this.mConnection);
        }
        this.mHyperlinkElementViewListener = null;
    }

    public void setHyperlinkElementViewListener(HyperlinkElementViewListener listener) {
        this.mHyperlinkElementViewListener = listener;
    }

    protected int getElementViewLayout() {
        return R.layout.view_hyperlink_element;
    }

    private void onHyperlinkClicked() {
        onFieldViewClicked();
        if (this.mHyperlinkElementViewListener != null) {
            this.mHyperlinkElementViewListener.onClick(getElement());
        }
        Context context = getContext();
        if (context != null && this.mUri != null) {
            if (this.mCustomTabsSession != null) {
                new Builder(this.mCustomTabsSession).build().launchUrl(context, this.mUri);
            } else {
                context.startActivity(new Intent ("android.intent.action.VIEW", this.mUri));
            }
        }
    }
}
