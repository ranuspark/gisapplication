package com.example.bhumicloud.gisapplications.model.field.attachment.video;

import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;

import java.util.Map;

public class VideoItemValue extends MediaItemValue {
    private static final String JSON_VIDEO_ID = "video_id";

    public VideoItemValue(Attachment attachment) {
        super(attachment);
    }

    public VideoItemValue(Map attributes) {
        super(attributes);
    }

    protected String getIDFieldTitle() {
        return JSON_VIDEO_ID;
    }

    protected boolean canEqual(Object object) {
        return object instanceof VideoItemValue;
    }
}
