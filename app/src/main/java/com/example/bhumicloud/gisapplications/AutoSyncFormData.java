package com.example.bhumicloud.gisapplications;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AutoSyncFormData {

    String response = null;
    NetworkInfo mobileCheck, wifiCheck;
    Context mcontext;
    JSONObject jsonObject, jsonobject;
    String jsonStr, chech_code, Mainprojectid;
    JSONArray dummydata, Elementdata, Imagedata, Parentdata, ParentSave, ParentCond, FormSave, FormCond, Formproject100;
    DatabaseHandler db;
    JSONObject details;
    String dirname;
    File direct;

    ArrayList<String> getimgepath = new ArrayList<>();
    StringBuilder IMGbuilder;
    String StoreServerimage;

    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    public AutoSyncFormData(Context mcontext) {
        this.mcontext = mcontext;
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public void SyncData() {


        ConnectivityManager connectionManager = (ConnectivityManager) mcontext.getSystemService(Context.CONNECTIVITY_SERVICE);
        wifiCheck = connectionManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        mobileCheck = connectionManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        sharedpreferences = mcontext.getSharedPreferences("LOGINCHECK", Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        Mainprojectid = sharedpreferences.getString("SaveProjectId", null);
        Log.d("Autosyncprojectid", Mainprojectid);
        if ((wifiCheck.isConnected() && wifiCheck.isAvailable()) || (mobileCheck.isConnected() && mobileCheck.isAvailable())) {
            new syncform().execute();
        } else {
            new MaterialDialog.Builder(mcontext)
                    .title("Alert !")
                    .content("Failed to sync data.Please make sure you are connected to wifi.")
                    .positiveText("Ok")
                    .show();
        }
    }

    void Download_Images_From_Server() {
        try {
            for (int i = 0; i < Imagedata.length(); i++) {
                details = Imagedata.getJSONObject(i);
                getimgepath.clear();
                Log.d("details.get", details.getString("Value"));
                if (details.getString("Value").equals("")) {
                    StoreServerimage = "";
                    db.addFormImages(new FormSaveDetails(Integer.parseInt(details.getString("Project_Id")), Integer.parseInt(details.getString("Form_Id")), Integer.parseInt(details.getString("User_id")), details.getString("Value"), StoreServerimage, details.getString("curr_loc")));
                } else {
                    String[] parts = details.getString("Value").split(",");
                    if (parts.length == 0) {
                        StoreServerimage = "";
                    } else {
                        for (int x = 0; x < parts.length; x++) {
                            File f = new File(direct, parts[x]);
                            Log.d("getBBbitmapbitmap", String.valueOf(f));
                            getimgepath.add(String.valueOf(f));
                            InputStream is = new URL("http://Bhumicloud.com/images/" + parts[x]).openStream();
                            OutputStream os = new FileOutputStream(f);
                            CopyStream(is, os);
                        }
                        IMGbuilder = new StringBuilder(128);
                        for (String value : getimgepath) {
                            if (IMGbuilder.length() > 0) {
                                IMGbuilder.append(",");
                            }
                            IMGbuilder.append(value);
                        }
                        StoreServerimage = IMGbuilder.toString();
                    }
                    db.addFormImages(new FormSaveDetails(Integer.parseInt(details.getString("Project_Id")), Integer.parseInt(details.getString("Form_Id")), Integer.parseInt(details.getString("User_id")), details.getString("Value"), StoreServerimage, details.getString("curr_loc")));
                }
            }
        } catch (Exception ex) {
        }
    }

    void StoreImages() {
        dirname = "/GISPROJECT" + Mainprojectid;
        Log.d("dirname", dirname);
        direct = new File(Environment.getExternalStorageDirectory() + dirname);
        Log.d("dirname", String.valueOf(direct));
        if (!direct.exists()) {
            direct.mkdir();
            Download_Images_From_Server();
        } else {
            if (direct.isDirectory()) {
                String[] previmages = direct.list();
                if (previmages.length != 0) {
                    for (int i = 0; i < previmages.length; i++) {
                        new File(direct, previmages[i]).delete();
                    }
                    Log.d("previmages", "data exist");
                } else {
                    Log.d("previmages", "No data exist");
                }
            }
            Download_Images_From_Server();
        }
    }

    class syncform extends AsyncTask<Void, Void, Void> {
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(mcontext);
            pDialog.setMessage("Please wait...\nSync in progress");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<>();
                nameValuePairs.add(new BasicNameValuePair("ProjectID", Mainprojectid));
                //  nameValuePairs.add(new BasicNameValuePair("email", getEmail));

                ServiceHandler sh = new ServiceHandler();
                jsonStr = sh.makeServiceCall(Constants.BASE_URL + Constants.FormSyncUrl, ServiceHandler.GET, nameValuePairs);
                Log.d("Login_Responseclass: ", "> " + jsonStr);

                jsonobject = new JSONObject(jsonStr);
                jsonObject = jsonobject.getJSONObject("response");
                chech_code = jsonObject.getString("code");
                dummydata = jsonObject.getJSONArray("dummy_data");
                Elementdata = jsonObject.getJSONArray("Element_data");
                Imagedata = jsonObject.getJSONArray("Images_data");
                Parentdata = jsonObject.getJSONArray("Parent_data");
                ParentSave = jsonObject.getJSONArray("Parent_save");
                ParentCond = jsonObject.getJSONArray("Parent_conditional");
                FormSave = jsonObject.getJSONArray("Form_save");
                FormCond = jsonObject.getJSONArray("Form_conditional");
                Formproject100 = jsonObject.getJSONArray("Form_project");

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (jsonStr == null) {
                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                new MaterialDialog.Builder(mcontext)
                        .title("Alert !")
                        .content("Internet is not working!")
                        .positiveText("Ok")
                        .show();
            } else if (chech_code.equals("1")) {
                db = new DatabaseHandler(mcontext);
                db.deleteFormdata();
                db.deleteFormElement();
                db.deleteFormImages();
                db.deleteFormParentdata();
                db.deleteFormParentSavedata();
                db.deleteFormParentConditional();
                db.deleteFormConditional();
                db.deleteFormSavedata();
                db.deleteFormFormproject();

                try {
                    if (dummydata.length() > 0) {
                        for (int i = 0; i < dummydata.length(); i++) {
                            details = dummydata.getJSONObject(i);
                            db.addFormdata(new FormSaveDetails(Integer.parseInt(details.getString("fieldId")), Integer.parseInt(details.getString("Project_Id")), Integer.parseInt(details.getString("Form_Id")), Integer.parseInt(details.getString("eleId")), Integer.parseInt(details.getString("Order_ID")), details.getString("Element_Name"), Integer.parseInt(details.getString("Group_id")), Integer.parseInt(details.getString("Drop_value_Order")), details.getString("Drop_value_Name"), Integer.parseInt(details.getString("show_in_search")), Integer.parseInt(details.getString("show_parent")), Integer.parseInt(details.getString("condition_logic"))));
                        }

                        List<FormSaveDetails> contacts = db.ShowformData();
                        for (FormSaveDetails cn : contacts) {
                            String log = "Id: " + cn.getSID() + "fieldId: " + cn.getfiledID() + " ,Project: " + cn.getProjectId() + " ,Form: " + cn.getFormId() + " ,eleld: " + cn.geteleId() + " ,Orderid: " + cn.getorderid() + " ,elementname: " + cn.getelename() + " ,groupid: " + cn.getgrpid() + " ,droporder: " + cn.getDroporder() + " ,dropname: " + cn.getDropname() + " ,showinsearch: " + cn.getshowinsearch() + " ,showparent: " + cn.getshowparent() + " ,condlogic: " + cn.getconlogic();
                            Log.d("fgjdfgjgjdgjdgj: ", log);
                        }
                    }


                    if (Elementdata.length() > 0) {
                        for (int i = 0; i < Elementdata.length(); i++) {
                            details = Elementdata.getJSONObject(i);
                            db.addFormElement(new FormSaveDetails(Integer.parseInt(details.getString("eleId")), details.getString("input_type")));
                        }
                    }

                    if (Imagedata.length() > 0) {
                        Log.d("Imagedatalength: ", String.valueOf(Imagedata));
                        StoreImages();
                    }

                    if (Parentdata.length() > 0) {
                        for (int i = 0; i < Parentdata.length(); i++) {
                            details = Parentdata.getJSONObject(i);
                            db.addFormPArentData(new FormSaveDetails(Integer.parseInt(details.getString("p_id")), Integer.parseInt(details.getString("Project_Id")), Integer.parseInt(details.getString("eleId")), Integer.parseInt(details.getString("Order_ID")), details.getString("Element_Name"), Integer.parseInt(details.getString("Group_id")), Integer.parseInt(details.getString("Drop_value_Order")), details.getString("Drop_value_Name"), Integer.parseInt(details.getString("condition_logic"))));
                        }
                    }

                    if (ParentSave.length() > 0) {
                        for (int i = 0; i < ParentSave.length(); i++) {
                            details = ParentSave.getJSONObject(i);
                            db.addFormPArentSaveData(new FormSaveDetails(Integer.parseInt(details.getString("User_id")), Integer.parseInt(details.getString("p_id")), details.getString("Value"), Integer.parseInt(details.getString("Project_Id")), Integer.parseInt(details.getString("Form_Id")), Integer.parseInt(details.getString("save_id")), Integer.parseInt(details.getString("group_id"))));
                        }
                    }


                    if (ParentCond.length() > 0) {
                        for (int i = 0; i < ParentCond.length(); i++) {
                            details = ParentCond.getJSONObject(i);
                            db.addFormPArentConditional(new FormSaveDetails(Integer.parseInt(details.getString("project_id")), Integer.parseInt(details.getString("Group_id")), Integer.parseInt(details.getString("fieldid")), details.getString("values1"), Integer.parseInt(details.getString("field_action")), Integer.parseInt(details.getString("flag"))));
                        }
                    }

                    if (FormSave.length() > 0) {

                        ArrayList _array = new ArrayList();
                        for (int i = 0; i < FormSave.length(); i++) {
                            details = FormSave.getJSONObject(i);
                            db.addFormSaveData(new FormSaveDetails(Integer.parseInt(details.getString("User_id")), Integer.parseInt(details.getString("fieldId")), details.getString("Value"), Integer.parseInt(details.getString("Project_Id")), Integer.parseInt(details.getString("Form_Id")), details.getString("Date")));
                        }
                    }


                    if (FormCond.length() > 0) {
                        for (int i = 0; i < FormCond.length(); i++) {
                            details = FormCond.getJSONObject(i);
                            db.addFormConditional(new FormSaveDetails(Integer.parseInt(details.getString("project_id")), Integer.parseInt(details.getString("fieldid")), details.getString("values1"), Integer.parseInt(details.getString("field_action")), Integer.parseInt(details.getString("flag"))));
                        }
                    }

                    if (Formproject100.length() > 0) {
                        for (int j = 0; j < Formproject100.length(); j++) {
                            details = Formproject100.getJSONObject(j);
                            //     db.addFormProject(new FormSaveDetails(Integer.parseInt(details.getString("id")),Integer.parseInt(details.getString("userid")), details.getString("name"), details.getString("description"), details.getString("date1"), Integer.parseInt(details.getString("status"))));
                            db.addFormProject(new FormSaveDetails(Integer.parseInt(details.getString("id")), details.getString("userid"), details.getString("name"), details.getString("description"), details.getString("date1"), Integer.parseInt(details.getString("status"))));
                        }
                    }

                    if (pDialog.isShowing()) {
                        pDialog.dismiss();
                    }

                    new MaterialDialog.Builder(mcontext)
                            .title("Alert !")
                            .content("Synchronization process completed.")
                            .positiveText("Ok")
                            .show();
                } catch (Exception e) {
                }
            } else {
                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                new MaterialDialog.Builder(mcontext)
                        .title("Alert !")
                        .content("Please check your internet connection!")
                        .positiveText("Ok")
                        .show();


            }

        }
    }
}
