package com.example.bhumicloud.gisapplications.model.field.attachment.signature;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;

import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment.Type;
import com.example.bhumicloud.gisapplications.util.MimeUtils;

import java.io.File;
import java.util.List;

public class Signature extends Attachment {
    public static final Creator<Signature> CREATOR = new C11821();

    static class C11821 implements Creator<Signature> {
        C11821() {
        }

        public Signature createFromParcel(Parcel source) {
            return new Signature(source);
        }

        public Signature[] newArray(int size) {
            return new Signature[size];
        }
    }

    public static File generateSignatureFile(Context context, String key, String contentType) {
        return new File(Attachment.getStorageDirectory(context), "signature-" + key + "." + MimeUtils.getExtension(contentType));
    }

    public static List<Attachment> getSignatures(Account account, Record record, boolean includeUploaded) {
        return Attachment.getAttachments(account, record, includeUploaded, Attachment.TYPE_SIGNATURE);
    }

    public Signature(String key) {
        super(key);
    }

    public Signature(Cursor cursor) {
        super(cursor);
    }

    private Signature(Parcel parcel) {
        super(parcel);
    }

    public Signature(String key, Uri localUri, String thumbnailPath) {
        super(key, localUri, thumbnailPath);
    }

    public File generateFile(Context context, String contentType) {
        return generateSignatureFile(context, getRemoteID(), contentType);
    }

    public Type getType() {
        return Type.SIGNATURE;
    }

    public boolean canEqual(Object other) {
        return other instanceof Signature;
    }
}
