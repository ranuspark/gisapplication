package com.example.bhumicloud.gisapplications.Activity;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.graphics.Paint;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.Activity.LocationInputDialog.LocationInputDialogListener;
import com.example.bhumicloud.gisapplications.maps.MBTilesOfflineTileProvider;
import com.example.bhumicloud.gisapplications.maps.OnlineTileProvider;
import com.example.bhumicloud.gisapplications.model.InteractivityData;
import com.example.bhumicloud.gisapplications.model.layer.MBTilesMapLayer;
import com.example.bhumicloud.gisapplications.model.layer.MapLayer;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.example.bhumicloud.gisapplications.util.DeviceInfo;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.LocationUtils;
import com.example.bhumicloud.gisapplications.util.PermissionsUtils;
import com.example.bhumicloud.gisapplications.util.StreamUtils;
import com.example.bhumicloud.gisapplications.widget.FloatingActionButton;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import io.fabric.sdk.android.services.network.HttpRequest;

public abstract class MapViewFragment extends GISFragment implements LocationListener {
    private static final int MAP_ANIMATION_LENGTH = 750;
    private static final String TAG_INTERACTIVITY_DATA = "TAG_INTERACTIVITY_DATA";
    private static final String TAG_INTERACTIVITY_DATA_BUNDLE = "TAG_INTERACTIVITY_BUNDLE";
    private static final String TAG_INTERACTIVITY_DATA_CONTENT_SHOWING = "TAG_INTERACTIVITY_DATA_SHOWING";
    private static final String TAG_LOCATION_FOLLOW_MODE = "TAG_LOCATION_FOLLOW_MODE";
    private static final String TAG_LOCATION_INPUT_DIALOG = "TAG_LOCATION_INPUT_DIALOG";
    private static String sInteractivityTemplate;
    private WebView mContentView;
    private TextView mCoordinateLabel;
    private ImageView mCrosshair;
    private boolean mCrosshairEnabled;
    private Location mCustomLocation;
    private boolean mDisplayCoordinates;
    protected FloatingActionButton mFloatingActionButton;
    private boolean mFloatingActionButtonAnimated;
    private float mFloatingActionButtonTranslation;
    private boolean mFollowMode;
    private float mFollowModeZoom;
    private GoogleMap mGoogleMap;
    protected GoogleMapsClickListener mGoogleMapsClickListener;
    private boolean mIgnoreCameraChange;
    private InteractivityData mInteractivityData;
    private boolean mInteractivityDataContentShowing;
    private View mInteractivityView;
    private boolean mIsCameraAnimating;
    private final List<OnMapReadyCallback> mList = Collections.synchronizedList ( new ArrayList () );
    private LocationInputDialogListener mLocationInputListener = new LocationInputDialogListener () {
        public void onLocationValueEntered(Location location) {
            MapViewFragment.this.moveToLocation ( location );
            MapViewFragment.this.setManualLocation ( location );
        }
    };
    private MapView mMapView;
    private float mMaximumZoomLevel;
    private android.support.design.widget.FloatingActionButton mMyLocationButton;
    private int mSelectedBasemap;
    private Marker mSelectedMarker;
    private WebView mSnippetView;
    private final ArrayList<TileOverlay> mTileOverlays = new ArrayList ();
    private final ArrayList<TileProvider> mTileProviders = new ArrayList ();
    private ImageView mToggleView;

    public interface GoogleMapsClickListener {
        void onMapClick();

        void onMarkerClick(Marker marker);
    }

    public void onAttach(Context context) {
        super.onAttach ( context );
        if (!(context instanceof LocationListenerActivity)) {
            throw new RuntimeException ( context.toString () + " must extend " + LocationListenerActivity.class.getSimpleName () );
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setHasOptionsMenu ( true );
        this.mFloatingActionButtonTranslation = (float) getResources ().getDimensionPixelSize ( R.dimen.fab_horizontal_translation );
        this.mGoogleMapsClickListener = new GoogleMapsClickListener () {
            public void onMarkerClick(Marker marker) {
                if (!MapViewFragment.this.mFloatingActionButtonAnimated) {
                    MapViewFragment.this.mFloatingActionButtonAnimated = true;
                    ObjectAnimator.ofFloat ( MapViewFragment.this.mFloatingActionButton, "translationX", new float[]{0.0f, -MapViewFragment.this.mFloatingActionButtonTranslation} ).start ();
                }
                MapViewFragment.this.mSelectedMarker = marker;
            }

            public void onMapClick() {
                MapViewFragment.this.resetFloatingActionButtonPosition ();
            }
        };
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate ( R.layout.fragment_map_view, container, false );
        this.mMapView = (MapView) view.findViewById ( R.id.map_view );
        this.mMapView.onCreate ( savedInstanceState );
        MapsInitializer.initialize ( getActivity () );
        this.mCrosshair = (ImageView) view.findViewById ( R.id.map_crosshair );
        setCrosshairEnabled ( this.mCrosshairEnabled );
        this.mCoordinateLabel = (TextView) view.findViewById ( R.id.coordinate_view );
        setCoordinatesEnabled ( this.mDisplayCoordinates );
        this.mInteractivityView = view.findViewById ( R.id.map_interactivity_view );
        this.mSnippetView = (WebView) view.findViewById ( R.id.map_interactivity_snippet_view );
        this.mContentView = (WebView) view.findViewById ( R.id.map_interactivity_content_view );
        this.mSnippetView.setBackgroundColor ( 0 );
        this.mContentView.setBackgroundColor ( 0 );
        this.mSnippetView.setLayerType ( 1, new Paint () );
        this.mContentView.setLayerType ( 1, new Paint () );
        ImageView closeView = (ImageView) view.findViewById ( R.id.map_interactivity_close );
        this.mToggleView = (ImageView) view.findViewById ( R.id.map_interactivity_toggle );
        closeView.setOnClickListener ( new OnClickListener () {
            public void onClick(View v) {
                MapViewFragment.this.hideInteractivityView ();
            }
        } );
        this.mToggleView.setOnClickListener ( new OnClickListener () {
            public void onClick(View v) {
                MapViewFragment.this.toggleInteractivityContentView ();
            }
        } );
        if (savedInstanceState != null) {
            Bundle bundle = savedInstanceState.getBundle ( TAG_INTERACTIVITY_DATA_BUNDLE );
            if (bundle != null) {
                this.mInteractivityData = (InteractivityData) bundle.getParcelable ( TAG_INTERACTIVITY_DATA );
                this.mInteractivityDataContentShowing = bundle.getBoolean ( TAG_INTERACTIVITY_DATA_CONTENT_SHOWING );
                this.mFollowMode = bundle.getBoolean ( TAG_LOCATION_FOLLOW_MODE );
            }
        }
        this.mFloatingActionButton = (FloatingActionButton) view.findViewById ( R.id.floating_action_button );
        this.mFloatingActionButton.setVisibility ( View.GONE );
        this.mMyLocationButton = (android.support.design.widget.FloatingActionButton) view.findViewById ( R.id.my_location_button );
        this.mMyLocationButton.setSelected ( this.mFollowMode );
        this.mMyLocationButton.setOnClickListener ( new OnClickListener () {
            public void onClick(View v) {
                MapViewFragment.this.toggleFollowMode ();
            }
        } );
        return view;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated ( view, savedInstanceState );
        this.mMapView.getMapAsync ( new OnMapReadyCallback () {
            public void onMapReady(GoogleMap googleMap) {
                MapViewFragment.this.mGoogleMap = googleMap;
                MapViewFragment.this.onConfigureMap ( googleMap );
                MapViewFragment.this.onMapShowing ( googleMap );
            }
        } );
        LocationInputDialog inputDialog = (LocationInputDialog) getChildFragmentManager ().findFragmentByTag ( TAG_LOCATION_INPUT_DIALOG );
        if (inputDialog != null) {
            inputDialog.setLocationInputDialogListener ( this.mLocationInputListener );
        }
    }

    public void onStart() {
        super.onStart ();
        if (PermissionsUtils.checkPermissionsAndRequest ( getActivity (), 64 )) {
            ((LocationListenerActivity) getActivity ()).startLocationUpdates ();
        }
    }

    public void onResume() {
        super.onResume ();
        if (this.mMapView != null) {
            this.mMapView.onResume ();
            GoogleMap googleMap = getMap ();
            if (googleMap != null) {
                onMapShowing ( googleMap );
            }
        }
    }

    public void onPause() {
        super.onPause ();
        if (this.mMapView != null) {
            this.mMapView.onPause ();
        }
    }

    public void onDestroy() {
        super.onDestroy ();
        if (this.mMapView != null) {
            this.mMapView.onDestroy ();
        }
        ((LocationListenerActivity) getActivity ()).stopLocationUpdates ();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState ( outState );
        if (this.mMapView != null) {
            this.mMapView.onSaveInstanceState ( outState );
        }
        if (this.mInteractivityView != null && this.mInteractivityView.getVisibility () == View.VISIBLE) {
            Bundle bundle = new Bundle ();
            bundle.putParcelable ( TAG_INTERACTIVITY_DATA, this.mInteractivityData );
            if (this.mContentView.getVisibility () == View.VISIBLE) {
                bundle.putBoolean ( TAG_INTERACTIVITY_DATA_CONTENT_SHOWING, true );
            } else {
                bundle.putBoolean ( TAG_INTERACTIVITY_DATA_CONTENT_SHOWING, false );
            }
            outState.putBundle ( TAG_INTERACTIVITY_DATA_BUNDLE, bundle );
        }
        outState.putBoolean ( TAG_LOCATION_FOLLOW_MODE, this.mFollowMode );
    }

    public void onLowMemory() {
        super.onLowMemory ();
        if (this.mMapView != null) {
            this.mMapView.onLowMemory ();
        }
    }

    public void onLocationChanged(Location location) {
        if (this.mFollowMode) {
            zoomToFollow ( location );
        }
    }

    public void addOfflineLayer(MBTilesMapLayer layer) {
        File file = layer.getLocalFile ();
        if (file != null && file.exists ()) {
            try {
                addTileOverlay ( new MBTilesOfflineTileProvider ( file ), layer.getDisplayOrder () );
            } catch (SQLiteDatabaseCorruptException e) {
                Context context = getActivity ();
                if (context != null) {
                    Builder b = new Builder ( context );
                    b.setTitle ( R.string.corrupt_mbtiles_title );
                    b.setMessage ( context.getString ( R.string.corrupt_mbtiles_message, new Object[]{layer.getName ()} ) );
                    b.show ();
                }
            }
        }
    }

    public void addTileOverlay(TileProvider provider, int zIndex) {
        this.mTileProviders.add ( provider );
        TileOverlayOptions opts = new TileOverlayOptions ();
        opts.tileProvider ( provider );
        opts.zIndex ( (float) zIndex );
        GoogleMap map = getMap ();
        if (map != null) {
            this.mTileOverlays.add ( map.addTileOverlay ( opts ) );
        }
    }

    public void clearTileOverlays() {
        Iterator it = this.mTileOverlays.iterator ();
        while (it.hasNext ()) {
            ((TileOverlay) it.next ()).remove ();
        }
        it = this.mTileProviders.iterator ();
        while (it.hasNext ()) {
            TileProvider provider = (TileProvider) it.next ();
            if (provider instanceof MBTilesOfflineTileProvider) {
                ((MBTilesOfflineTileProvider) provider).close ();
            }
        }
        this.mTileOverlays.clear ();
        this.mTileProviders.clear ();
    }

    public void reloadMapLayers(Context context) {
        clearTileOverlays ();
        for (Long longValue : PatronSettings.getActiveMapLayers ( context )) {
            MapLayer layer = MapLayer.getMapLayer ( longValue.longValue () );
            if (layer != null) {
                if (layer instanceof MBTilesMapLayer) {
                    addOfflineLayer ( (MBTilesMapLayer) layer );
                } else {
                    addTileOverlay ( new OnlineTileProvider ( layer ), layer.getDisplayOrder () );
                }
            }
        }
    }

    public void setBasemap(int basemap) {
        this.mSelectedBasemap = basemap;
        GoogleMap map = getMap ();
        if (map != null) {
            switch (basemap) {
                case PatronSettings.SYNC_MODE_ALWAYS /*0*/:
                    map.setMapType ( 1 );
                    break;
                case PermissionsUtils.REQUEST_01 /*1*/:
                    map.setMapType ( 2 );
                    break;
                case PermissionsUtils.REQUEST_02 /*2*/:
                    map.setMapType ( 4 );
                    break;
                case PatronSettings.SORT_MODE_UPDATED_AT /*3*/:
                    map.setMapType ( 3 );
                    break;
                default:
                    map.setMapType ( 0 );
                    break;
            }
            calculateZoomConstraints ();
        }
    }

    public LatLngBounds getVisibleBounds() {
        GoogleMap map = getMap ();
        if (map != null) {
            return map.getProjection ().getVisibleRegion ().latLngBounds;
        }
        return null;
    }

    public Location getCenterCoordinate() {
        if (this.mCustomLocation != null) {
            return this.mCustomLocation;
        }
        CameraPosition cameraPosition = getCurrentCameraPosition ();
        return cameraPosition == null ? null : LocationUtils.toLocation ( cameraPosition.target );
    }

    public CameraPosition getCurrentCameraPosition() {
        GoogleMap map = getMap ();
        return map == null ? null : map.getCameraPosition ();
    }

    public void moveToLocation(Location location) {
        GoogleMap map = getMap ();
        if (map != null && location != null) {
            if (this.mFollowMode) {
                toggleFollowMode ();
            }
            map.animateCamera ( CameraUpdateFactory.newLatLng ( LocationUtils.getLatLng ( location ) ), MAP_ANIMATION_LENGTH, null );
        }
    }

    public void moveToLatLngBounds(LatLngBounds bounds) {
        GoogleMap map = getMap ();
        if (map != null && bounds != null) {
            if (this.mFollowMode) {
                toggleFollowMode ();
            }
            map.animateCamera ( CameraUpdateFactory.newLatLngBounds ( bounds, (int) TypedValue.applyDimension ( 1, 50.0f, getResources ().getDisplayMetrics () ) ), MAP_ANIMATION_LENGTH, null );
        }
    }

    public void zoomToFeature(final long id, final LatLng latLng) {
        if (latLng != null) {
            GoogleMap map = getMap ();
            if (map != null) {
                if (this.mFollowMode) {
                    toggleFollowMode ();
                }
                CameraPosition pos = PatronSettings.getMapCameraPosition ( getActivity () );
                float zoom = Math.max ( pos == null ? 2.0f : pos.zoom, 16.0f );
                if (zoom > this.mMaximumZoomLevel) {
                    zoom = this.mMaximumZoomLevel;
                }
                map.animateCamera ( CameraUpdateFactory.newLatLngZoom ( latLng, zoom ), MAP_ANIMATION_LENGTH, null );
                return;
            }
            synchronized (this.mList) {
                this.mList.add ( new OnMapReadyCallback () {
                    public void onMapReady(GoogleMap googleMap) {
                        MapViewFragment.this.zoomToFeature ( id, latLng );
                    }
                } );
            }
        }
    }

    public void setCrosshairEnabled(boolean enabled) {
        this.mCrosshairEnabled = enabled;
        if (this.mCrosshair != null) {
            this.mCrosshair.setVisibility ( enabled ? View.VISIBLE : View.GONE );
            if (enabled) {
                this.mCrosshair.getViewTreeObserver ().addOnGlobalLayoutListener ( new OnGlobalLayoutListener () {
                    @TargetApi(16)
                    public void onGlobalLayout() {
                        CoordinatorLayout.LayoutParams lp = new CoordinatorLayout.LayoutParams ( MapViewFragment.this.mCrosshair.getLayoutParams () );
                        int x = (MapViewFragment.this.mMapView.getWidth () / 2) - (MapViewFragment.this.mCrosshair.getWidth () / 2);
                        int y = (MapViewFragment.this.mMapView.getHeight () / 2) - (MapViewFragment.this.mCrosshair.getHeight () / 2);
                        if (DeviceInfo.isRightToLeft ( MapViewFragment.this.getResources () )) {
                            lp.setMargins ( 0, y, x, 0 );
                        } else {
                            lp.setMargins ( x, y, 0, 0 );
                        }
                        MapViewFragment.this.mCrosshair.setLayoutParams ( lp );
                        if (DeviceInfo.isJellyBean ()) {
                            MapViewFragment.this.mCrosshair.getViewTreeObserver ().removeOnGlobalLayoutListener ( this );
                        } else {
                            MapViewFragment.this.mCrosshair.getViewTreeObserver ().removeGlobalOnLayoutListener ( this );
                        }
                    }
                } );
            }
        }
    }

    public void setMyLocationEnabled(GoogleMap map) {
        if (map == null) {
            map = getMap ();
        }
        if (map != null) {
            map.setMyLocationEnabled ( true );
        }
        this.mMyLocationButton.setVisibility(View.VISIBLE);
        ((LocationListenerActivity) getActivity()).startLocationUpdates();
    }

    public void setCoordinatesEnabled(boolean enabled) {
        this.mDisplayCoordinates = enabled;
        if (this.mCoordinateLabel != null) {
            this.mCoordinateLabel.setVisibility(enabled ? View.VISIBLE : View.GONE);
            if (enabled) {
                this.mCoordinateLabel.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        MapViewFragment.this.onCoordinateViewClicked();
                    }
                });
                GoogleMap map = getMap();
                if (map != null) {
                    map.getUiSettings().setMapToolbarEnabled(false);
                }
            }
        }
    }

    public void setInteractivityData(InteractivityData data) {
        if (data != null) {
            String template = getInteractivityTemplate();
            if (TextUtils.isEmpty(data.getSnippet())) {
                data.setSnippet("<em>No Snippet</em>");
            }
            if (TextUtils.isEmpty(data.getContent())) {
                data.setContent("<em>No Content</em>");
            }
            String snippet = String.format(template, new Object[]{data.getSnippet()});
            String content = String.format(template, new Object[]{data.getContent()});
            this.mSnippetView.loadDataWithBaseURL(null, snippet, "text/html", "UTF-8", null);
            this.mContentView.loadDataWithBaseURL(null, content, "text/html", "UTF-8", null);
            this.mInteractivityData = data;
            if (this.mInteractivityView.getVisibility() != View.VISIBLE) {
                this.mInteractivityView.setVisibility(View.VISIBLE);
                if (this.mFollowMode) {
                    toggleFollowMode();
                }
                this.mMyLocationButton.setVisibility(View.GONE);
            }
        }
    }

    public void hideInteractivityView() {
        this.mSnippetView.loadUrl("about:blank");
        this.mContentView.loadUrl("about:blank");
        if (this.mInteractivityView.getVisibility() != View.GONE) {
            this.mInteractivityView.setVisibility(View.GONE);
            if (PermissionsUtils.checkPermissionsAndRequest(getActivity(), 64)) {
                setMyLocationEnabled(null);
            }
        }
        this.mInteractivityData = null;
    }

    public void toggleInteractivityContentView() {
        if (this.mContentView.getVisibility() != View.VISIBLE) {
            this.mContentView.setVisibility(View.VISIBLE);
            this.mToggleView.setSelected(true);
            return;
        }
        this.mContentView.setVisibility(View.GONE);
        this.mToggleView.setSelected(false);
    }

    public void setManualLocation(Location location) {
        this.mIgnoreCameraChange = true;
        this.mCustomLocation = location;
        if (this.mCoordinateLabel != null && location != null) {
            this.mCoordinateLabel.setText(LocationUtils.toString(location, getResources()));
        }
    }

    @Nullable
    protected GoogleMap getMap() {
        return this.mGoogleMap;
    }

    protected void onMapReady() {
    }

    protected void onConfigureMap(final GoogleMap map) {
        map.setIndoorEnabled(false);
        map.setTrafficEnabled(false);
        map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            public void onCameraIdle() {
                MapViewFragment.this.mIsCameraAnimating = false;
                MapViewFragment.this.onCameraIdle(MapViewFragment.this.getCurrentCameraPosition());
            }
        });
        map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
            public void onInfoWindowClick(Marker marker) {
                MapViewFragment.this.onMarkerInfoWindowClicked(marker);
            }
        });
        map.setOnMarkerClickListener(new OnMarkerClickListener() {
            public boolean onMarkerClick(Marker marker) {
                if (MapViewFragment.this.mFollowMode) {
                    MapViewFragment.this.toggleFollowMode();
                }
                if (MapViewFragment.this.mGoogleMapsClickListener != null) {
                    MapViewFragment.this.mGoogleMapsClickListener.onMarkerClick(marker);
                }
                return false;
            }
        });
        map.setOnMapClickListener(new OnMapClickListener() {
            public void onMapClick(LatLng location) {
                if (MapViewFragment.this.mGoogleMapsClickListener != null) {
                    MapViewFragment.this.mGoogleMapsClickListener.onMapClick();
                }
                MapViewFragment.this.onUserClickedLocation(location);
            }
        });
        map.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener () {
            public void onCameraMoveStarted(int reason) {
                MapViewFragment.this.mIsCameraAnimating = true;
                if (MapViewFragment.this.mFollowMode && reason == 1) {
                    MapViewFragment.this.toggleFollowMode();
                }
            }
        });
        map.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener () {
            public void onCameraMove() {
                if (MapViewFragment.this.mSelectedMarker != null && MapViewFragment.this.mSelectedMarker.isInfoWindowShown()) {
                    LatLng selected = MapViewFragment.this.mSelectedMarker.getPosition();
                    if (selected != null && !map.getProjection().getVisibleRegion().latLngBounds.contains(selected)) {
                        MapViewFragment.this.resetFloatingActionButtonPosition();
                    }
                }
            }
        });
        map.getUiSettings().setMyLocationButtonEnabled(false);
        if (PermissionsUtils.checkPermissionsAndRequest(getActivity(), 64)) {
            setMyLocationEnabled(map);
        }
        setBasemap(this.mSelectedBasemap);
    }

    protected void onCameraIdle(CameraPosition cameraPosition) {
        calculateZoomConstraints();
        GoogleMap map = getMap();
        if (map != null) {
            onVisibleRegionChanged(map.getProjection().getVisibleRegion().latLngBounds);
            if (this.mInteractivityData != null && this.mInteractivityView.getVisibility() != View.VISIBLE) {
                setInteractivityData(this.mInteractivityData);
                if (this.mInteractivityDataContentShowing) {
                    toggleInteractivityContentView();
                }
                this.mInteractivityDataContentShowing = false;
            }
        }
    }

    protected void onVisibleRegionChanged(LatLngBounds region) {
        if (this.mSelectedMarker != null && this.mSelectedMarker.isInfoWindowShown()) {
            LatLng selected = this.mSelectedMarker.getPosition();
            if (!(selected == null || region.contains(selected))) {
                resetFloatingActionButtonPosition();
            }
        }
        if (!this.mIgnoreCameraChange) {
            this.mCustomLocation = null;
            if (isAdded()) {
                this.mCoordinateLabel.setText(LocationUtils.toString(getCenterCoordinate(), getResources()));
            }
        }
        this.mIgnoreCameraChange = false;
    }

    protected void onMarkerInfoWindowClicked(Marker marker) {
    }

    protected void onUserClickedLocation(LatLng location) {
        CameraPosition cameraPosition = getCurrentCameraPosition();
        if (cameraPosition != null) {
            int zoom = (int) cameraPosition.zoom;
            Iterator it = this.mTileProviders.iterator();
            while (it.hasNext()) {
                TileProvider provider = (TileProvider) it.next();
                if (provider instanceof MBTilesOfflineTileProvider) {
                    MBTilesOfflineTileProvider mbtiles = (MBTilesOfflineTileProvider) provider;
                    if (mbtiles.supportsInteractivity()) {
                        setInteractivityData(mbtiles.getInteractivityData(location, zoom));
                    }
                }
            }
        }
    }

    protected void onCoordinateViewClicked() {
        LocationInputDialog dialog = new LocationInputDialog();
        dialog.setLocation(getCenterCoordinate());
        dialog.setLocationInputDialogListener(this.mLocationInputListener);
        dialog.show(getChildFragmentManager(), TAG_LOCATION_INPUT_DIALOG);
    }

    protected void resetFloatingActionButtonPosition() {
        if (this.mFloatingActionButtonAnimated) {
            this.mFloatingActionButtonAnimated = false;
            ObjectAnimator.ofFloat(this.mFloatingActionButton, "translationX", new float[]{-this.mFloatingActionButtonTranslation, 0.0f}).start();
        }
    }

    private void onMapShowing(GoogleMap googleMap) {
        setBasemap(PatronSettings.getBasemap(getActivity()));
        reloadMapLayers(getActivity());
        calculateZoomConstraints();
        onMapReady();
        synchronized (this.mList) {
            for (OnMapReadyCallback onMapReadyCallback : this.mList) {
                onMapReadyCallback.onMapReady(googleMap);
            }
            this.mList.clear();
        }
    }

    private void calculateZoomConstraints() {
        float max = 0.0f;
        Iterator it = this.mTileProviders.iterator();
        while (it.hasNext()) {
            TileProvider provider = (TileProvider) it.next();
            if (provider instanceof MBTilesOfflineTileProvider) {
                max = Math.max(max, (float) ((MBTilesOfflineTileProvider) provider).getMaximumZoom());
            }
        }
        GoogleMap map = getMap();
        if (!(map == null || map.getMapType() == 0)) {
            max = Math.max(max, map.getMaxZoomLevel());
        }
        this.mMaximumZoomLevel = max;
    }

    private String getInteractivityTemplate() {
        if (sInteractivityTemplate != null) {
            return sInteractivityTemplate;
        }
        String template = "%s";
        try {
            template = StreamUtils.toString(getResources().openRawResource(R.raw.interactivity_template));
        } catch (Throwable e) {
            GISLogger.log(e);
        }
        sInteractivityTemplate = template;
        return template;
    }

    private void toggleFollowMode() {
        boolean newFollowMode = !this.mFollowMode;
        this.mMyLocationButton.setSelected(newFollowMode);
        if (newFollowMode) {
            zoomToFollow(((LocationListenerActivity) getActivity()).getLastLocation());
        }
        this.mFollowMode = newFollowMode;
    }

    private void zoomToFollow(Location location) {
        float f = 2.0f;
        GoogleMap map = getMap();
        if (map != null && location != null) {
            LatLng latLng = LocationUtils.getLatLng(location);
            if (!this.mIsCameraAnimating || this.mFollowModeZoom < 2.0f) {
                CameraPosition pos = map.getCameraPosition();
                if (pos != null) {
                    f = pos.zoom;
                }
                this.mFollowModeZoom = Math.max(f, 16.0f);
            }
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, this.mFollowModeZoom), MAP_ANIMATION_LENGTH, null);
        }
    }
}
