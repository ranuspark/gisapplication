package com.example.bhumicloud.gisapplications.model;

import com.fasterxml.jackson.annotation.JsonValue;
import java.util.HashMap;

public interface Jsonable {
    @JsonValue
    HashMap<String, Object> toJSON();
}
