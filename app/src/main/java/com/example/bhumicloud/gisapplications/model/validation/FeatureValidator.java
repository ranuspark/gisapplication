package com.example.bhumicloud.gisapplications.model.validation;

import com.example.bhumicloud.gisapplications.model.Feature;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.FormValueContainer;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.SectionElement;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableElement;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableItemValue;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableValue;
import com.example.bhumicloud.gisapplications.model.field.textual.text.TextElement;
import com.example.bhumicloud.gisapplications.model.field.textual.text.TextValue;
import com.example.bhumicloud.gisapplications.model.validation.error.FieldLengthValidationError;
import com.example.bhumicloud.gisapplications.model.validation.error.GeometryRequiredValidationError;
import com.example.bhumicloud.gisapplications.model.validation.error.NumericFormatValidationError;
import com.example.bhumicloud.gisapplications.model.validation.error.NumericRangeValidationError;
import com.example.bhumicloud.gisapplications.model.validation.error.RequiredFieldValidationError;
import com.example.bhumicloud.gisapplications.model.validation.error.TextPatternValidationError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FeatureValidator {
    public static List<FeatureValidationError> validateFeature(Feature feature, Record record, FormValueContainer valuesForConditions) {
        ArrayList<FeatureValidationError> errors = new ArrayList ();
        if (feature instanceof Record) {
            return validateRecord((Record) feature, valuesForConditions);
        }
        if (feature instanceof RepeatableItemValue) {
            return validateRepeatable((RepeatableItemValue) feature, record, valuesForConditions);
        }
        return errors;
    }

    public static ArrayList<FeatureValidationError> validateRecord(Record record, FormValueContainer valuesForConditions) {
        Form form = record.getForm();
        List<Element> elements = form.getElements();
        ArrayList<FeatureValidationError> errors = new ArrayList ();
        if (form.isGeometryRequired() && !record.hasLocation()) {
          //  errors.add(new GeometryRequiredValidationError());
        }
        validateFormValues(errors, elements, valuesForConditions, record, null);
        return errors;
    }

    public static ArrayList<FeatureValidationError> validateRepeatable(RepeatableItemValue repeatable, Record record, FormValueContainer valuesForConditions) {
        RepeatableElement element = repeatable.getElement();
        List<Element> elements = element.getElements();
        ArrayList<FeatureValidationError> errors = new ArrayList ();
        if (element.isGeometryRequired() && !repeatable.hasLocation()) {
        //    errors.add(new GeometryRequiredValidationError());
        }
        validateFormValues(errors, elements, valuesForConditions, record, null);
        return errors;
    }

    public static void validateFormValues(List<FeatureValidationError> errors, List<Element> elements, FormValueContainer values, Record record, HashMap<String, Boolean> cache) {
        if (cache == null) {
            cache = new HashMap ();
        }
        for (Element element : elements) {
            if (Condition.shouldElementBeVisible(element, record, values, cache)) {
                if (!(element instanceof SectionElement)) {
                    boolean required = Condition.shouldElementBeRequired(element, record, values);
                    boolean visible = Condition.shouldElementBeVisible(element, record, values, cache);
                    boolean disabled = element.isDisabled();
                    FormValue value = values.getFormValue(element);
                    boolean nullValue = value == null;
                    boolean emptyValue = nullValue || value.isEmpty();
                    if (required && visible && !disabled && (nullValue || emptyValue)) {
                        errors.add(new RequiredFieldValidationError(record, element));
                    }
                    if (visible && !disabled && (element instanceof TextElement)) {
                        TextElement textElement = (TextElement) element;
                        TextValue textValue = (TextValue) value;
                        FeatureValidationError error = null;
                        if (textElement.isNumeric()) {
                            error = validateNumericField(record, textElement, textValue);
                        } else if (!textElement.isPatternEmpty()) {
                            if (required && (nullValue || emptyValue)) {
                                errors.add(new TextPatternValidationError(record, textElement));
                            } else {
                                error = validateTextPattern(record, textElement, textValue);
                            }
                        }
                        if (error != null) {
                            errors.add(error);
                        }
                    }
                    if (!(nullValue || emptyValue)) {
                        if (element.hasMinLength() && element.hasMaxLength() && (element.getMinLength() > value.length() || element.getMaxLength() < value.length())) {
                            errors.add(new FieldLengthValidationError(record, element));
                        } else if (element.hasMinLength() && element.getMinLength() > value.length()) {
                            errors.add(new FieldLengthValidationError(record, element));
                        } else if (element.hasMaxLength() && element.getMaxLength() < value.length()) {
                            errors.add(new FieldLengthValidationError(record, element));
                        }
                    }
                    if (element instanceof RepeatableElement) {
                        Element repeatable = (RepeatableElement) element;
                        RepeatableValue repeatableValue = (RepeatableValue) values.getFormValue(repeatable);
                        if (repeatableValue != null) {
                            for (RepeatableItemValue item : repeatableValue.getItems()) {
                                FormValueContainer itemValues = item.getFormValues().copy();
                                itemValues.mergeValues(values);
                               // validateFormValues(errors, repeatable.getElements(), itemValues, record, null);
                            }
                        }
                    }
                } else if (Condition.shouldElementBeVisible(element, record, values, cache)) {
                    validateFormValues(errors, ((SectionElement) element).getElements(), values, record, cache);
                }
            }
        }
    }

    private static FeatureValidationError validateNumericField(Record record, TextElement element, TextValue value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        if (!value.isNumeric()) {
            return new NumericFormatValidationError(record, element);
        }
        Number number = value.getNumericValue();
        if (number == null) {
            return new NumericFormatValidationError(record, element);
        }
        if (element.isInteger()) {
            if (value.getValue().contains(".")) {
                return new NumericFormatValidationError(record, element);
            }
        }
      //  if (number.doubleValue() < element.getMinValue() || number.doubleValue() > element.getMaxValue()) {
      //      return new NumericRangeValidationError(element);
      //  }
        return null;
    }

    private static FeatureValidationError validateTextPattern(Record record, TextElement element, TextValue value) {
        if (value == null || value.isEmpty() || value.getValue().matches(element.getPattern())) {
            return null;
        }
        return new TextPatternValidationError(record, element);
    }
}
