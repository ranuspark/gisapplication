package com.example.bhumicloud.gisapplications.Activity;

import android.content.Context;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import org.droidparts.util.*;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.util.KeyboardUtils;

public abstract class ChoicePickerDialog extends ThemedDialogFragment implements ListAdapter, OnItemClickListener, TextWatcher {
    public static final int MODE_MULTIPLE_CHOICE = 2;
    public static final int MODE_SIMPLE_CHOICE = 0;
    public static final int MODE_SINGLE_CHOICE = 1;
    private static final String STATE_CHOICE_MODE = "STATE_CHOICE_MODE";
    private static final String STATE_SEARCH_ENABLED = "STATE_SEARCH_ENABLED";
    private int mChoiceMode = 0;
    private final DataSetObservable mDataSetObservers = new DataSetObservable ();
    private LayoutInflater mInflater;
    private ListView mListView;
    private boolean mSearchEnabled;
    private EditText mSearchView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            this.mChoiceMode = savedInstanceState.getInt(STATE_CHOICE_MODE);
            this.mSearchEnabled = savedInstanceState.getBoolean(STATE_SEARCH_ENABLED);
        }
    }

    public View onCreateContentView(LayoutInflater inflater, ViewGroup root, Bundle savedState) {
        View view = inflater.inflate(R.layout.dialog_choice_picker, root, false);
        this.mListView = (ListView) view.findViewById(R.id.list_view);
        this.mListView.setChoiceMode(this.mChoiceMode);
        this.mListView.setOnItemClickListener(this);
        this.mListView.setAdapter(this);
        this.mSearchView = (EditText) view.findViewById(R.id.search_view);
        this.mSearchView.addTextChangedListener(this);
        this.mSearchView.setContentDescription("Search");
        if (this.mSearchEnabled) {
            this.mSearchView.setVisibility(View.VISIBLE);
        } else {
            this.mSearchView.setVisibility(View.GONE);
        }
        return view;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_CHOICE_MODE, this.mChoiceMode);
        outState.putBoolean(STATE_SEARCH_ENABLED, this.mSearchEnabled);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        if (onChoiceItemClicked(this.mListView, position, this.mListView.isItemChecked(position))) {
            KeyboardUtils.hideDialogSoftKeyboard(getActivity(), getView());
            dismissAllowingStateLoss();
        }
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    public void afterTextChanged(Editable s) {
        onSearchQueryDidChange(s.toString());
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public int getItemViewType(int position) {
        return 0;
    }

    public View getView(int position, View reusable, ViewGroup parent) {
        View view;
        if (reusable == null) {
            view = getLayoutInflater().inflate(getItemViewLayout(), parent, false);
        } else {
            view = reusable;
        }
        onBindItemView(view, position);
        return view;
    }

    public int getViewTypeCount() {
        return 1;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean areAllItemsEnabled() {
        return true;
    }

    public boolean isEnabled(int position) {
        return true;
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        this.mDataSetObservers.registerObserver(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        this.mDataSetObservers.unregisterObserver(observer);
    }

    public int getChoiceMode() {
        return this.mChoiceMode;
    }

    public void setChoiceMode(int choiceMode) {
        this.mChoiceMode = choiceMode;
        if (this.mListView != null) {
            this.mListView.setChoiceMode(choiceMode);
        }
    }

    public void setSearchEnabled(boolean enabled) {
        this.mSearchEnabled = enabled;
        if (this.mSearchView != null) {
            this.mSearchView.setVisibility(enabled ? View.VISIBLE : View.GONE);
        }
    }

    public ListView getListView() {
        return this.mListView;
    }

    protected void notifyDataSetChanged() {
        this.mDataSetObservers.notifyChanged();
    }


/*
    protected LayoutInflater getLayoutInflater() {
        if (this.mInflater != null) {
            return this.mInflater;
        }
        Object svc = getActivity().getSystemService( Context.LAYOUT_INFLATER_SERVICE);
        if (svc instanceof LayoutInflater) {
            this.mInflater = (LayoutInflater) svc;
        }
        return this.mInflater;
    }
*/

    protected int getItemViewLayout() {
        switch (this.mChoiceMode) {
            case 1:
                return R.layout.list_item_single_choice;
            case 2:
                return R.layout.list_item_mulitple_choice;
            default:
                return R.layout.list_item_simple_choice;
        }
    }

    protected void onBindItemView(View view, int position) {
        TextView textView;
        //TODO: WRONG ASSIGNMENT
        View v = view.findViewById( R.id.text_field);
        if (v instanceof TextView) {
            textView = (TextView) v;
        } else {
            textView = (TextView) view;
        }
        Object item = getItem(position);
        textView.setContentDescription(item.toString());
        textView.setText(item.toString());
    }

    protected boolean onChoiceItemClicked(ListView listView, int position, boolean selected) {
        return this.mChoiceMode == 0;
    }

    protected void onSearchQueryDidChange(String query) {
    }
}
