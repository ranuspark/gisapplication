package com.example.bhumicloud.gisapplications.model.layer;

import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable.Creator;

import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.util.GISLogger;

import java.io.File;
import java.util.Map;

public class XYZMapLayer extends MapLayer {
    public static final Creator<XYZMapLayer> CREATOR = new C12201();
    public static final String TYPE = "xyz";
    private boolean mAvailable;

    static class C12201 implements Creator<XYZMapLayer> {
        C12201() {
        }

        public XYZMapLayer createFromParcel(Parcel source) {
            return new XYZMapLayer(source);
        }

        public XYZMapLayer[] newArray(int size) {
            return new XYZMapLayer[size];
        }
    }

    public XYZMapLayer(Cursor cursor) {
        super(cursor);
        this.mAvailable = checkURL();
    }

    public XYZMapLayer(Account account, Map jsonRepresentation) {
        super(account, jsonRepresentation);
        this.mAvailable = checkURL();
    }

    protected XYZMapLayer(Parcel parcel) {
        super(parcel);
        boolean z = true;
        if (parcel.readInt() != 1) {
            z = false;
        }
        this.mAvailable = z;
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(this.mAvailable ? 1 : 0);
    }

    public void setAttributesFromJSON(Map jsonRepresentation) {
        super.setAttributesFromJSON(jsonRepresentation);
        this.mAvailable = checkURL();
    }

    public File generateStorageLocation(Context context, String remoteID) {
        return null;
    }

    public boolean isAvailable() {
        return this.mAvailable;
    }

    public void setAvailable(boolean available) {
        this.mAvailable = available;
    }

    public String getType() {
        return TYPE;
    }

    private boolean checkURL() {
        try {
            String url = getRemoteURL().toString();
            if (url.contains("{x}") && url.contains("{y}") && url.contains("{z}")) {
                return true;
            }
            return false;
        } catch (Throwable e) {
            GISLogger.log(e);
            return false;
        }
    }
}
