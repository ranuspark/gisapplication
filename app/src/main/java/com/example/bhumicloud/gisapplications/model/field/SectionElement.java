package com.example.bhumicloud.gisapplications.model.field;

import android.os.Parcel;

import com.example.bhumicloud.gisapplications.util.ElementUtils;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SectionElement extends Element implements ElementContainer {
    public static final Creator<SectionElement> CREATOR = new C11751();
    public static final String DISPLAY_INLINE = "inline";
    private static final String JSON_DISPLAY_MODE = "display";
    private static final String JSON_ELEMENTS = "elements";
    private String mDisplayMode;
    private Map<String, Element> mElementIndex;
    private ArrayList<Element> mElements;

    static class C11751 implements Creator<SectionElement> {
        C11751() {
        }

        public SectionElement createFromParcel(Parcel source) {
            return new SectionElement(source);
        }

        public SectionElement[] newArray(int size) {
            return new SectionElement[size];
        }
    }

    public SectionElement(Element parent, Map json) {
        super(parent, json);
    }

    private SectionElement(Parcel parcel) {
        super(parcel);
    }

    public ArrayList<Element> getElements() {
        if (this.mElements == null) {
            this.mElements = new ArrayList();
        }
        return this.mElements;
    }

    public Element getElement(String key) {
        return (Element) getElementIndex().get(key);
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = super.toJSON();
        json.put("display", getDisplayMode());
        ArrayList<Object> elements = new ArrayList();
        Iterator it = getElements().iterator();
        while (it.hasNext()) {
            elements.add(((Element) it.next()).toJSON());
        }
        json.put("elements", elements);
        return json;
    }

    public String getType() {
        return Element.TYPE_SECTION;
    }

    public String getDisplayMode() {
        return this.mDisplayMode;
    }

    public boolean isInline() {
        return DISPLAY_INLINE.equals(this.mDisplayMode);
    }

    public void setJSONAttributes(Map json) {
        super.setJSONAttributes(json);
        this.mDisplayMode = JSONUtils.getString(json, "display", DISPLAY_INLINE);
        this.mElements = null;
        List elementsJSON = JSONUtils.getArrayList(json, "elements");
        if (elementsJSON != null && !elementsJSON.isEmpty()) {
            for (int i = 0; i < elementsJSON.size(); i++) {
                HashMap<String, Object> elementJSON = JSONUtils.getHashMap(elementsJSON, i);
                if (!(elementJSON == null || elementJSON.isEmpty())) {
                    Element element = ElementFactory.getInstance(this, elementJSON);
                    if (element != null) {
                        getElements().add(element);
                    }
                }
            }
        }
    }

    public String toString() {
        String base = super.toString();
        Iterator it = getElements().iterator();
        while (it.hasNext()) {
            base = base + "\n\t" + ((Element) it.next()).toString();
        }
        return base;
    }

    private Map<String, Element> getElementIndex() {
        if (this.mElementIndex != null) {
            return this.mElementIndex;
        }
        this.mElementIndex = ElementUtils.flattenElements(getElements());
        return this.mElementIndex;
    }
}
