package com.example.bhumicloud.gisapplications.model.field;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.example.bhumicloud.gisapplications.model.Jsonable;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.validation.Condition;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import com.example.bhumicloud.gisapplications.util.ParcelUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Element implements Parcelable, Jsonable {
    private static final int DEFAULT_MAX_LENGTH = -1;
    private static final int DEFAULT_MIN_LENGTH = -1;
    private static final String JSON_DATA_NAME = "data_name";
    private static final String JSON_DEFAULT_PREVIOUS_VALUE = "default_previous_value";
    private static final String JSON_DEFAULT_VALUE = "default_value";
    private static final String JSON_DESCRIPTION = "description";
    private static final String JSON_DISABLED = "disabled";
    private static final String JSON_HIDDEN = "hidden";
    private static final String JSON_KEY = "key";
    private static final String JSON_LABEL = "label";
    private static final String JSON_MAX_LENGTH = "max_length";
    private static final String JSON_MIN_LENGTH = "min_length";
    private static final String JSON_REQUIRED = "required";
    private static final String JSON_REQUIRED_CONDITIONS = "required_conditions";
    private static final String JSON_REQUIRED_CONDITIONS_TYPE = "required_conditions_type";
    private static final String JSON_TYPE = "type";
    private static final String JSON_VISIBLE_CONDITIONS = "visible_conditions";
    private static final String JSON_VISIBLE_CONDITIONS_BEHAVIOR = "visible_conditions_behavior";
    private static final String JSON_VISIBLE_CONDITIONS_BEHAVIOR_CLEAR = "clear";
    private static final String JSON_VISIBLE_CONDITIONS_BEHAVIOR_PRESERVE = "preserve";
    private static final String JSON_VISIBLE_CONDITIONS_TYPE = "visible_conditions_type";
    public static final String TYPE_ADDRESS = "AddressField";
    public static final String TYPE_AUDIO = "AudioField";
    public static final String TYPE_BARCODE = "BarcodeField";
    public static final String TYPE_CALCULATION = "CalculatedField";
    public static final String TYPE_CHOICE = "ChoiceField";
    public static final String TYPE_CLASSIFICATION = "ClassificationField";
    public static final String TYPE_DATE = "DateField";
    public static final String TYPE_DATE_TIME = "DateTimeField";
    public static final String TYPE_HYPERLINK = "HyperlinkField";
    public static final String TYPE_LABEL = "Label";
    public static final String TYPE_PHOTO = "PhotoField";
    public static final String TYPE_RECORD_LINK = "RecordLinkField";
    public static final String TYPE_REPEATABLE = "Repeatable";
    public static final String TYPE_SECTION = "Section";
    public static final String TYPE_SIGNATURE = "SignatureField";
    public static final String TYPE_STATUS = "StatusElement";
    public static final String TYPE_TEXT = "TextField";
    public static final String TYPE_TIME = "TimeField";
    public static final String TYPE_VIDEO = "VideoField";
    public static final String TYPE_YES_NO = "YesNoField";
    private String mDataName;
    private boolean mDefaultPreviousValue;
    private String mDefaultValue;
    private String mDescription;
    private boolean mDisabled;
    private boolean mHidden;
    private String mKey;
    private String mLabel;
    private int mMaxLength;
    private int mMinLength;
    private String mOverrideDescription;
    private Boolean mOverrideIsDisabled;
    private Boolean mOverrideIsHidden;
    private Boolean mOverrideIsRequired;
    private String mOverrideLabel;
    private Integer mOverrideMaxLength;
    private Integer mOverrideMinLength;
    private String mParentKey;
    private boolean mRequired;
    private final List<Condition> mRequiredConditions = new ArrayList();
    private String mRequiredConditionsType;
    private final List<Condition> mVisibleConditions = new ArrayList();
    private String mVisibleConditionsBehavior;
    private String mVisibleConditionsType;

    public abstract String getType();

    public Element(Element parent, Map json) {
        if (parent != null) {
            this.mParentKey = parent.getKey();
        }
        setJSONAttributes(json);
    }

    protected Element(Parcel parcel) {
        this.mParentKey = parcel.readString();
        try {
            setJSONAttributes(JSONUtils.objectFromJSON(parcel.readString()));
            this.mOverrideLabel = parcel.readString();
            this.mOverrideDescription = parcel.readString();
            this.mOverrideIsRequired = ParcelUtils.readBoolean(parcel);
            this.mOverrideIsHidden = ParcelUtils.readBoolean(parcel);
            this.mOverrideIsDisabled = ParcelUtils.readBoolean(parcel);
            this.mOverrideMinLength = ParcelUtils.readInteger(parcel);
            this.mOverrideMaxLength = ParcelUtils.readInteger(parcel);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mParentKey);
        dest.writeString(JSONUtils.toJSONString(toJSON()));
        dest.writeString(this.mOverrideLabel);
        dest.writeString(this.mOverrideDescription);
        dest.writeValue(this.mOverrideIsRequired);
        dest.writeValue(this.mOverrideIsHidden);
        dest.writeValue(this.mOverrideIsDisabled);
        dest.writeValue(this.mOverrideMinLength);
        dest.writeValue(this.mOverrideMaxLength);
    }

    public int describeContents() {
        return 0;
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = new HashMap();
        json.put("type", getType());
        json.put("key", getKey());
        json.put(JSON_LABEL, getLabel());
        json.put("description", getDescription());
        json.put(JSON_DEFAULT_VALUE, getDefaultValue());
        json.put(JSON_DATA_NAME, getDataName());
        json.put(JSON_DISABLED, Boolean.valueOf(isDisabled()));
        json.put(JSON_HIDDEN, Boolean.valueOf(isHidden()));
        json.put(JSON_REQUIRED, Boolean.valueOf(isRequired()));
        json.put(JSON_VISIBLE_CONDITIONS_TYPE, getVisibleConditionsType());
        json.put(JSON_VISIBLE_CONDITIONS_BEHAVIOR, getVisibleConditionsBehavior());
        json.put(JSON_REQUIRED_CONDITIONS_TYPE, getRequiredConditionsType());
        ArrayList<Object> visibleConditions = new ArrayList();
        for (Condition condition : getVisibleConditions()) {
            visibleConditions.add(condition.toJSON());
        }
        json.put(JSON_VISIBLE_CONDITIONS, visibleConditions);
        ArrayList<Object> requiredConditions = new ArrayList();
        for (Condition condition2 : getRequiredConditions()) {
            requiredConditions.add(condition2.toJSON());
        }
        json.put(JSON_REQUIRED_CONDITIONS, requiredConditions);
        if (this.mMinLength != -1) {
            json.put(JSON_MIN_LENGTH, Integer.valueOf(this.mMinLength));
        }
        if (this.mMaxLength != -1) {
            json.put(JSON_MAX_LENGTH, Integer.valueOf(this.mMaxLength));
        }
        return json;
    }

    public String getKey() {
        return this.mKey;
    }

    public String getLabel() {
        return this.mOverrideLabel != null ? this.mOverrideLabel : this.mLabel;
    }

    public String getAbsoluteLabel(Record record) {
        List<String> labels = new ArrayList();
        labels.add(getLabel());
        Element iterator = record.getForm().getParentElement(this);
        while (iterator != null) {
            labels.add(0, iterator.getLabel());
            iterator = record.getForm().getParentElement(iterator);
        }
        return TextUtils.join(" / ", labels);
    }

    public String getDescription() {
        return this.mOverrideDescription != null ? this.mOverrideDescription : this.mDescription;
    }

    public boolean hasDescription() {
        return !TextUtils.isEmpty(getDescription());
    }

    public String getDefaultValue() {
        return this.mDefaultValue;
    }

    public String getDataName() {
        return this.mDataName;
    }

    public boolean isDisabled() {
        return this.mOverrideIsDisabled != null ? this.mOverrideIsDisabled.booleanValue() : this.mDisabled;
    }

    public boolean isEnabled() {
        return !isDisabled();
    }

    public boolean isRequired() {
        return this.mOverrideIsRequired != null ? this.mOverrideIsRequired.booleanValue() : this.mRequired;
    }

    public boolean isHidden() {
        return this.mOverrideIsHidden != null ? this.mOverrideIsHidden.booleanValue() : this.mHidden;
    }

    public boolean preserveValueWhenConditionallyHidden() {
        return JSON_VISIBLE_CONDITIONS_BEHAVIOR_PRESERVE.equals(getVisibleConditionsBehavior());
    }

    public String getVisibleConditionsType() {
        return this.mVisibleConditionsType;
    }

    public String getVisibleConditionsBehavior() {
        return this.mVisibleConditionsBehavior;
    }

    public String getRequiredConditionsType() {
        return this.mRequiredConditionsType;
    }

    public boolean hasVisibilityConditions() {
        return !this.mVisibleConditions.isEmpty();
    }

    public boolean hasRequirementConditions() {
        return !this.mRequiredConditions.isEmpty();
    }

    public List<Condition> getVisibleConditions() {
        return this.mVisibleConditions;
    }

    public List<Condition> getRequiredConditions() {
        return this.mRequiredConditions;
    }

    public int getMinLength() {
        return this.mOverrideMinLength != null ? this.mOverrideMinLength.intValue() : this.mMinLength;
    }

    public boolean hasMinLength() {
        return getMinLength() > -1;
    }

    public int getMaxLength() {
        return this.mOverrideMaxLength != null ? this.mOverrideMaxLength.intValue() : this.mMaxLength;
    }

    public boolean hasMaxLength() {
        return getMaxLength() > -1;
    }

    public boolean isDefaultPreviousValueEnabled() {
        return this.mDefaultPreviousValue;
    }

    public String getParentKey() {
        return this.mParentKey;
    }

    public void setOverrideLabel(String label) {
        this.mOverrideLabel = label;
    }

    public void setOverrideDescription(String description) {
        this.mOverrideDescription = description;
    }

    public void setOverrideIsRequired(Boolean required) {
        this.mOverrideIsRequired = required;
    }

    public Boolean getOverrideIsRequired() {
        return this.mOverrideIsRequired;
    }

    public void setOverrideIsHidden(Boolean hidden) {
        this.mOverrideIsHidden = hidden;
    }

    public Boolean getOverrideIsHidden() {
        return this.mOverrideIsHidden;
    }

    protected Boolean getOverrideIsDisabled() {
        return this.mOverrideIsDisabled;
    }

    public void setOverrideIsDisabled(Boolean disabled) {
        this.mOverrideIsDisabled = disabled;
    }

    public void setOverrideMinLength(Integer minLength) {
        this.mOverrideMinLength = minLength;
    }

    public void setOverrideMaxLength(Integer maxLength) {
        this.mOverrideMaxLength = maxLength;
    }

    public void setJSONAttributes(Map json) {
        int i;
        HashMap<String, Object> conditionJSON;
        this.mKey = JSONUtils.getString(json, "key");
        this.mLabel = JSONUtils.getString(json, JSON_LABEL);
        this.mDescription = JSONUtils.getString(json, "description");
        this.mDefaultValue = JSONUtils.getString(json, JSON_DEFAULT_VALUE);
        this.mDataName = JSONUtils.getString(json, JSON_DATA_NAME);
        this.mDisabled = JSONUtils.getBoolean(json, JSON_DISABLED);
        this.mHidden = JSONUtils.getBoolean(json, JSON_HIDDEN);
        this.mRequired = JSONUtils.getBoolean(json, JSON_REQUIRED);
        this.mVisibleConditionsType = JSONUtils.getString(json, JSON_VISIBLE_CONDITIONS_TYPE);
        this.mVisibleConditionsBehavior = JSONUtils.getString(json, JSON_VISIBLE_CONDITIONS_BEHAVIOR, JSON_VISIBLE_CONDITIONS_BEHAVIOR_CLEAR);
        this.mRequiredConditionsType = JSONUtils.getString(json, JSON_REQUIRED_CONDITIONS_TYPE);
        List visibleConditionsJSON = JSONUtils.getArrayList(json, JSON_VISIBLE_CONDITIONS);
        if (!(visibleConditionsJSON == null || visibleConditionsJSON.isEmpty())) {
            for (i = 0; i < visibleConditionsJSON.size(); i++) {
                conditionJSON = JSONUtils.getHashMap(visibleConditionsJSON, i);
                if (conditionJSON != null) {
                    this.mVisibleConditions.add(new Condition(conditionJSON));
                }
            }
        }
        List requiredConditionsJSON = JSONUtils.getArrayList(json, JSON_REQUIRED_CONDITIONS);
        if (!(requiredConditionsJSON == null || requiredConditionsJSON.isEmpty())) {
            for (i = 0; i < requiredConditionsJSON.size(); i++) {
                conditionJSON = JSONUtils.getHashMap(requiredConditionsJSON, i);
                if (conditionJSON != null) {
                    this.mRequiredConditions.add(new Condition(conditionJSON));
                }
            }
        }
        this.mMinLength = JSONUtils.getInt(json, JSON_MIN_LENGTH, Integer.valueOf(-1)).intValue();
        this.mMaxLength = JSONUtils.getInt(json, JSON_MAX_LENGTH, Integer.valueOf(-1)).intValue();
        this.mDefaultPreviousValue = JSONUtils.getBoolean(json, JSON_DEFAULT_PREVIOUS_VALUE);
    }

    public String toString() {
        return getClass().getSimpleName() + "<" + getKey() + "> " + getLabel();
    }
}
