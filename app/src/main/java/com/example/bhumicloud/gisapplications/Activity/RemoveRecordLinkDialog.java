package com.example.bhumicloud.gisapplications.Activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkElement;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkItemValue;

public class RemoveRecordLinkDialog extends ThemedDialogFragment {
    private static final String STATE_RECORD_LINK_ELEMENT = "STATE_RECORD_LINK_ELEMENT";
    private static final String STATE_RECORD_LINK_ITEM_VALUE = "STATE_RECORD_LINK_ITEM_VALUE";
    private RecordLinkElement mElement;
    private RecordLinkItemValue mItem;
    private RemoveRecordLinkDialogListener mListener;

    public interface RemoveRecordLinkDialogListener {
        void onPositiveButtonClicked(RecordLinkElement recordLinkElement, RecordLinkItemValue recordLinkItemValue);
    }

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        setPositiveButtonText((int) R.string.remove);
        setNegativeButtonText((int) R.string.cancel);
        if (savedState != null) {
            this.mElement = (RecordLinkElement) savedState.getParcelable(STATE_RECORD_LINK_ELEMENT);
            this.mItem = (RecordLinkItemValue) savedState.getParcelable(STATE_RECORD_LINK_ITEM_VALUE);
        }
    }

    public View onCreateContentView(LayoutInflater inflater, ViewGroup root, Bundle savedState) {
        View view = inflater.inflate(R.layout.dialog_single_message_no_title, root, false);
        ((TextView) view.findViewById(R.id.dialog_message)).setText(R.string.remove_item_confirmation);
        return view;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(STATE_RECORD_LINK_ELEMENT, this.mElement);
        outState.putParcelable(STATE_RECORD_LINK_ITEM_VALUE, this.mItem);
    }

    public void setRemoveRecordLinkDialogListener(RemoveRecordLinkDialogListener listener) {
        this.mListener = listener;
    }

    public void setElementAndItem(RecordLinkElement element, RecordLinkItemValue item) {
        this.mElement = element;
        this.mItem = item;
    }

    protected void onPositiveButtonClicked() {
        super.onPositiveButtonClicked();
        this.mListener.onPositiveButtonClicked(this.mElement, this.mItem);
        dismiss();
    }

    protected void onNegativeButtonClicked() {
        super.onNegativeButtonClicked();
        dismiss();
    }
}
