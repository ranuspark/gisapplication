package com.example.bhumicloud.gisapplications.model.field.temporal.date;

import android.os.Parcel;
import android.os.Parcelable.Creator;

import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.temporal.TemporalElement;

import java.util.Map;

public class DateElement extends TemporalElement {
    public static final Creator<DateElement> CREATOR = new C12001();

    static class C12001 implements Creator<DateElement> {
        C12001() {
        }

        public DateElement createFromParcel(Parcel source) {
            return new DateElement(source);
        }

        public DateElement[] newArray(int size) {
            return new DateElement[size];
        }
    }

    public DateElement(Element parent, Map json) {
        super(parent, json);
    }

    private DateElement(Parcel parcel) {
        super(parcel);
    }

    public String getType() {
        return Element.TYPE_DATE;
    }
}
