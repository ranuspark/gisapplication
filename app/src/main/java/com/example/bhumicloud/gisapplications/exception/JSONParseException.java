package com.example.bhumicloud.gisapplications.exception;

public class JSONParseException extends Exception {
    public JSONParseException(Object details) {
        super("could not parse to JSON: " + details);
    }
}
