package com.example.bhumicloud.gisapplications.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.maps.model.LatLng;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.dossier.PersistentObject;
import com.example.bhumicloud.gisapplications.dossier.SearchIndex;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusValue;
import com.example.bhumicloud.gisapplications.util.CursorUtils;
import com.example.bhumicloud.gisapplications.util.DateUtils;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import com.example.bhumicloud.gisapplications.util.LocationUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Record extends PersistentObject implements Feature {
    public static final String COLUMN_ACCOUNT_ID = "account_id";
    public static final String COLUMN_ACCURACY = "accuracy";
    public static final String COLUMN_ALTITUDE = "altitude";
    public static final String COLUMN_ASSIGNED_TO = "assigned_to";
    public static final String COLUMN_BEARING = "bearing";
    public static final String COLUMN_CREATED_ALTITUDE = "created_altitude";
    public static final String COLUMN_CREATED_AT = "created_at";
    public static final String COLUMN_CREATED_DURATION = "created_duration";
    public static final String COLUMN_CREATED_HORIZONTAL_ACC = "created_horizontal_accuracy";
    public static final String COLUMN_CREATED_LATITUDE = "created_latitude";
    public static final String COLUMN_CREATED_LONGITUDE = "created_longitude";
    public static final String COLUMN_DRAFT = "is_draft";
    public static final String COLUMN_EDITED_DURATION = "edited_duration";
    public static final String COLUMN_FORM_ID = "form_id";
    public static final String COLUMN_FORM_VALUES = "form_values";
    public static final String COLUMN_IS_NEW = "is_new";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGITUDE = "longitude";
    public static final String COLUMN_PROJECT_ID = "project_id";
    public static final String COLUMN_REMOTE_ID = "remote_id";
    public static final String COLUMN_ROW_ID = "_id";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_SYNCHRONIZED = "is_synchronized";
    public static final String COLUMN_SYNCHRONIZED_AT = "synchronized_at";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_UNIQUE_ID = "unique_id";
    public static final String COLUMN_UPDATED_ALTITUDE = "updated_altitude";
    public static final String COLUMN_UPDATED_AT = "updated_at";
    public static final String COLUMN_UPDATED_DURATION = "updated_duration";
    public static final String COLUMN_UPDATED_HORIZONTAL_ACC = "updated_horizontal_accuracy";
    public static final String COLUMN_UPDATED_LATITUDE = "updated_latitude";
    public static final String COLUMN_UPDATED_LONGITUDE = "updated_longitude";
    public static final String COLUMN_VALUES_INDEX_ROW_ID = "values_index_row_id";
    public static final String COLUMN_VERSION = "version";
    public static final Creator<Record> CREATOR = new C11701();
    private static final String JSON_KEY_ALTITUDE = "altitude";
    private static final String JSON_KEY_ASSIGNED_TO_ID = "assigned_to_id";
    private static final String JSON_KEY_CLIENT_CREATED_AT = "client_created_at";
    private static final String JSON_KEY_CLIENT_UPDATED_AT = "client_updated_at";
    private static final String JSON_KEY_COURSE = "course";
    private static final String JSON_KEY_CREATED_DURATION = "created_duration";
    private static final String JSON_KEY_CREATED_LOCATION = "created_location";
    private static final String JSON_KEY_EDITED_DURATION = "edited_duration";
    private static final String JSON_KEY_FORM_ID = "form_id";
    private static final String JSON_KEY_FORM_VALUES = "form_values";
    private static final String JSON_KEY_HORIZONTAL_ACCURACY = "horizontal_accuracy";
    private static final String JSON_KEY_ID = "id";
    private static final String JSON_KEY_LATITUDE = "latitude";
    private static final String JSON_KEY_LONGITUDE = "longitude";
    private static final String JSON_KEY_PROJECT_ID = "project_id";
    private static final String JSON_KEY_STATUS = "status";
    private static final String JSON_KEY_UPDATED_DURATION = "updated_duration";
    private static final String JSON_KEY_UPDATED_LOCATION = "updated_location";
    private static final String JSON_KEY_VERSION = "version";
    public static final String TABLE_NAME = "Records";
    private Account mAccount;
    private long mAccountID;
    private String mAssignedTo;
    private Date mCreatedAt;
    private Integer mCreatedDuration;
    private Location mCreatedLocation;
    private boolean mDraft;
    private Integer mEditedDuration;
    private Form mForm;
    private long mFormID;
    private boolean mIsNew;
    private Location mLocation;
    private Project mProject;
    private long mProjectID;
    private String mRemoteID;
    private String mStatus;
    private boolean mSynchronized;
    private long mSynchronizedAt;
    private String mTitle;
    private String mUniqueID;
    private Date mUpdatedAt;
    private Integer mUpdatedDuration;
    private Location mUpdatedLocation;
    private FormValueContainer mValueContainer;
    private HashMap<String, Object> mValueContainerJSON;
    private long mValuesIndexRowID;
    private long mVersion;

    static class C11701 implements Creator<Record> {
        C11701() {
        }

        public Record createFromParcel(Parcel source) {
            return new Record(source);
        }

        public Record[] newArray(int size) {
            return new Record[size];
        }
    }

    public static Record getRecord(long id) {
        Record record = null;
        String[] subArgs = new String[]{String.valueOf(id)};
        Cursor c = GIS.getDatabase().query(TABLE_NAME, null, "_id = ?", subArgs, null, null, null);
        c.moveToFirst();
        if (!c.isAfterLast()) {
            record = new Record(c);
        }
        c.close();
        return record;
    }

    public static ArrayList<Record> getRecords(SQLiteDatabase db, long formID, int batchSize, int offset) {
        ArrayList<Record> records = new ArrayList();
        if (db == null) {
            db = GIS.getDatabase();
        }
        Cursor cursor = db.rawQuery("SELECT * FROM Records WHERE form_id = ? ORDER BY _id ASC LIMIT " + batchSize + " OFFSET " + offset, new String[]{String.valueOf(formID)});
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            records.add(new Record(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return records;
    }

    public static Record getRecordByRemoteID(Account account, String remoteID) {
        Record record = null;
        SQLiteDatabase db = GIS.getDatabase();
        ArrayList<String> predicates = new ArrayList();
        ArrayList<String> paramsList = new ArrayList();
        if (account != null) {
            predicates.add("account_id = ?");
            paramsList.add(String.valueOf(account.getRowID()));
        }
        if (!TextUtils.isEmpty(remoteID)) {
            predicates.add("remote_id = ?");
            paramsList.add(remoteID);
        }
        Cursor cursor = db.query(TABLE_NAME, null, TextUtils.join(" AND ", predicates), (String[]) paramsList.toArray(new String[paramsList.size()]), null, null, null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            record = new Record(cursor);
        }
        cursor.close();
        return record;
    }

    public static Record duplicate(long id, boolean location, boolean values) {
        Record master = getRecord(id);
        if (master == null) {
            return null;
        }
        Record duplicate = new Record(master);
        if (values) {
            duplicate.mValueContainer = master.getFormValues().copy(true);
        }
        if (!location) {
            return duplicate;
        }
        duplicate.setLocation(master.getLocation());
        return duplicate;
    }

    public Record(Form form) {
        setAccount(form.getAccount());
        setForm(form);
        this.mRemoteID = UUID.randomUUID().toString();
        this.mIsNew = true;
    }

    public Record(Account account, Form form, Map json) {
        setAccount(account);
        setForm(form);
        setJSONAttributes(account, form, json);
        this.mIsNew = false;
    }

    public Record(Cursor cursor) {
        super(cursor);
        this.mAccountID = cursor.getLong(cursor.getColumnIndex("account_id"));
        this.mProjectID = cursor.getLong(cursor.getColumnIndex("project_id"));
        this.mFormID = cursor.getLong(cursor.getColumnIndex("form_id"));
        int idxCreatedAt = cursor.getColumnIndex(COLUMN_CREATED_AT);
        if (!cursor.isNull(idxCreatedAt)) {
            this.mCreatedAt = new Date(cursor.getLong(idxCreatedAt));
        }
        int idxUpdatedAt = cursor.getColumnIndex(COLUMN_UPDATED_AT);
        if (!cursor.isNull(idxUpdatedAt)) {
            this.mUpdatedAt = new Date(cursor.getLong(idxUpdatedAt));
        }
        this.mRemoteID = cursor.getString(cursor.getColumnIndex("remote_id"));
        this.mUniqueID = cursor.getString(cursor.getColumnIndex(COLUMN_UNIQUE_ID));
        this.mTitle = cursor.getString(cursor.getColumnIndex("title"));
        this.mStatus = cursor.getString(cursor.getColumnIndex("status"));
        this.mLocation = loadLocation(cursor, "latitude", "longitude", "altitude", COLUMN_BEARING, COLUMN_ACCURACY);
        this.mSynchronized = cursor.getInt(cursor.getColumnIndex(COLUMN_SYNCHRONIZED)) == 1;
        this.mSynchronizedAt = cursor.getLong(cursor.getColumnIndex(COLUMN_SYNCHRONIZED_AT));
        this.mValueContainer = null;
        try {
            this.mValueContainerJSON = JSONUtils.objectFromJSON(cursor.getString(cursor.getColumnIndex("form_values")));
        } catch (Throwable e) {
            GISLogger.log(e);
            this.mValueContainerJSON = null;
        }
        this.mValuesIndexRowID = cursor.getLong(cursor.getColumnIndex(COLUMN_VALUES_INDEX_ROW_ID));
        this.mVersion = cursor.getLong(cursor.getColumnIndex("version"));
        this.mAssignedTo = cursor.getString(cursor.getColumnIndex(COLUMN_ASSIGNED_TO));
        this.mDraft = CursorUtils.getBoolean(cursor, COLUMN_DRAFT);
        this.mIsNew = CursorUtils.getBoolean(cursor, COLUMN_IS_NEW);
        this.mCreatedLocation = loadLocation(cursor, COLUMN_CREATED_LATITUDE, COLUMN_CREATED_LONGITUDE, COLUMN_CREATED_ALTITUDE, null, COLUMN_CREATED_HORIZONTAL_ACC);
        this.mUpdatedLocation = loadLocation(cursor, COLUMN_UPDATED_LATITUDE, COLUMN_UPDATED_LONGITUDE, COLUMN_UPDATED_ALTITUDE, null, COLUMN_UPDATED_HORIZONTAL_ACC);
        this.mCreatedDuration = CursorUtils.getIntegerOrNull(cursor, "created_duration");
        this.mUpdatedDuration = CursorUtils.getIntegerOrNull(cursor, "updated_duration");
        this.mEditedDuration = CursorUtils.getIntegerOrNull(cursor, "edited_duration");
    }

    private Record(Parcel parcel) {
        super(parcel);
        boolean z;
        boolean z2 = true;
        this.mAccountID = parcel.readLong();
        this.mProjectID = parcel.readLong();
        this.mFormID = parcel.readLong();
        long createdAt = parcel.readLong();
        if (createdAt != Long.MAX_VALUE) {
            this.mCreatedAt = new Date(createdAt);
        }
        long updatedAt = parcel.readLong();
        if (updatedAt != Long.MAX_VALUE) {
            this.mUpdatedAt = new Date(updatedAt);
        }
        this.mRemoteID = parcel.readString();
        this.mUniqueID = parcel.readString();
        this.mTitle = parcel.readString();
        this.mStatus = parcel.readString();
        this.mLocation = (Location) parcel.readParcelable(null);
        if (parcel.readInt() == 1) {
            z = true;
        } else {
            z = false;
        }
        this.mSynchronized = z;
        this.mSynchronizedAt = parcel.readLong();
        this.mValueContainer = null;
        try {
            this.mValueContainerJSON = JSONUtils.objectFromJSON(parcel.readString());
        } catch (Throwable e) {
            GISLogger.log(e);
            this.mValueContainerJSON = null;
        }
        this.mValuesIndexRowID = parcel.readLong();
        this.mVersion = parcel.readLong();
        this.mAssignedTo = parcel.readString();
        if (parcel.readInt() == 1) {
            z = true;
        } else {
            z = false;
        }
        this.mDraft = z;
        if (parcel.readInt() != 1) {
            z2 = false;
        }
        this.mIsNew = z2;
        this.mCreatedLocation = (Location) parcel.readParcelable(null);
        this.mUpdatedLocation = (Location) parcel.readParcelable(null);
        this.mCreatedDuration = (Integer) parcel.readValue(null);
        this.mUpdatedDuration = (Integer) parcel.readValue(null);
        this.mEditedDuration = (Integer) parcel.readValue(null);
    }

    private Record(Record master) {
        this.mAccountID = master.mAccountID;
        this.mProjectID = master.mProjectID;
        this.mFormID = master.mFormID;
        this.mAccount = master.mAccount;
        this.mForm = master.mForm;
        this.mRemoteID = UUID.randomUUID().toString();
        this.mStatus = master.mStatus;
        this.mDraft = master.mDraft;
        this.mIsNew = true;
    }

    public FormValueContainer getFormValues() {
        if (this.mValueContainer == null) {
            this.mValueContainer = new FormValueContainer(getForm().getElements(), this.mValueContainerJSON);
        }
        return this.mValueContainer;
    }

    public FormValue getFormValue(String elementKey) {
        return getFormValues().getFormValue(elementKey);
    }

    public void setFormValue(Element element, FormValue value) {
        getFormValues().setFormValue(element, value);
        this.mTitle = null;
    }

    public Location getLocation() {
        return this.mLocation;
    }

    public boolean hasLocation() {
        return this.mLocation != null;
    }

    public void setLocation(Location location) {
        this.mLocation = location;
    }

    public Date getCreatedAt() {
        return this.mCreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.mCreatedAt = createdAt;
    }

    public Date getUpdatedAt() {
        return this.mUpdatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.mUpdatedAt = updatedAt;
    }

    public Location getCreatedLocation() {
        return this.mCreatedLocation;
    }

    public void setCreatedLocation(Location location) {
        this.mCreatedLocation = location;
    }

    public Location getUpdatedLocation() {
        return this.mUpdatedLocation;
    }

    public void setUpdatedLocation(Location location) {
        this.mUpdatedLocation = location;
    }

    public Integer getCreatedDuration() {
        return this.mCreatedDuration;
    }

    public void setCreatedDuration(Integer seconds) {
        this.mCreatedDuration = seconds;
    }

    public Integer getUpdatedDuration() {
        return this.mUpdatedDuration;
    }

    public void setUpdatedDuration(Integer seconds) {
        this.mUpdatedDuration = seconds;
    }

    public Integer getEditedDuration() {
        return this.mEditedDuration;
    }

    public void setEditedDuration(Integer seconds) {
        this.mEditedDuration = seconds;
    }

    public LatLng getLatLng() {
        if (hasLocation()) {
            return LocationUtils.getLatLng(this.mLocation);
        }
        return null;
    }

    public String getIdentifier() {
        return getRemoteID();
    }

    public void loadFromFeatureState(FeatureEditState state) {
        this.mProjectID = state.getProjectID();
        this.mCreatedAt = state.getCreatedAt();
        this.mUpdatedAt = state.getUpdatedAt();
        this.mProject = null;
        this.mStatus = state.getStatus();
        this.mLocation = state.getLocation();
        this.mSynchronized = state.isSynchronized();
        this.mValueContainer = state.getFormValues();
        this.mDraft = state.isDraft();
        this.mCreatedLocation = state.getCreatedLocation();
        this.mUpdatedLocation = state.getUpdatedLocation();
        this.mCreatedDuration = state.getCreatedDuration();
        this.mUpdatedDuration = state.getUpdatedDuration();
        this.mEditedDuration = state.getEditedDuration();
    }

    public String getRemoteID() {
        return this.mRemoteID;
    }

    public String getUniqueID() {
        if (this.mUniqueID == null || this.mUniqueID.length() <= 0) {
            this.mUniqueID = UUID.randomUUID().toString();
        }
        return this.mUniqueID;
    }

    public String getRecordStatus() {
        return this.mStatus;
    }

    public void setRecordStatus(String status) {
        this.mStatus = status;
    }

    public StatusValue getStatusValue() {
        return new StatusValue(getForm().getStatusElement(), getRecordStatus());
    }

    public void setAccount(Account account) {
        if (account != null) {
            this.mAccountID = account.getRowID();
        } else {
            this.mAccountID = 0;
        }
        this.mAccount = account;
    }

    public Account getAccount() {
        if (this.mAccount != null) {
            return this.mAccount;
        }
        if (this.mAccountID != 0) {
            this.mAccount = Account.getAccount(this.mAccountID);
        }
        return this.mAccount;
    }

    public void setForm(Form form) {
        if (form != null) {
            this.mFormID = form.getRowID();
        } else {
            this.mFormID = 0;
        }
        this.mForm = form;
    }

    public Form getForm() {
        if (this.mForm != null) {
            return this.mForm;
        }
        if (this.mFormID != 0) {
            this.mForm = Form.getForm(this.mFormID, false);
        }
        return this.mForm;
    }

    public void setProject(Project project) {
        this.mProject = project;
        this.mProjectID = this.mProject == null ? -1 : this.mProject.getRowID();
    }

    public Project getProject() {
        if (this.mProject != null) {
            return this.mProject;
        }
        if (this.mProjectID > 0) {
            this.mProject = Project.findByID(this.mProjectID);
        }
        return this.mProject;
    }

    public void setJSONAttributes(Account account, Map json) {
        setJSONAttributes(account, null, json);
    }

    public void setJSONAttributes(Account account, Form form, Map json) {
        if (this.mAccountID == 0) {
            this.mAccountID = JSONUtils.getLong(json, "account_id");
        }
        String projectRemoteID = JSONUtils.getString(json, "project_id");
        if (TextUtils.isEmpty(projectRemoteID)) {
            this.mProjectID = -1;
        } else {
            Project project = Project.findByRemoteID(account, projectRemoteID);
            this.mProjectID = project == null ? -1 : project.getRowID();
        }
        String clientCreatedAt = JSONUtils.getString(json, JSON_KEY_CLIENT_CREATED_AT);
        if (!TextUtils.isEmpty(clientCreatedAt)) {
            Date created = DateUtils.parseTimestampString(clientCreatedAt);
            if (created != null) {
                this.mCreatedAt = created;
            }
        }
        String clientUpdatedAt = JSONUtils.getString(json, JSON_KEY_CLIENT_UPDATED_AT);
        if (!TextUtils.isEmpty(clientUpdatedAt)) {
            Date updated = DateUtils.parseTimestampString(clientUpdatedAt);
            if (updated != null) {
                this.mUpdatedAt = updated;
            }
        }
        String formRemoteID = JSONUtils.getString(json, "form_id");
        if (form == null || !TextUtils.equals(form.getRemoteID(), formRemoteID)) {
            form = Form.getForm(account, formRemoteID, false);
        }
        if (form != null) {
            setForm(form);
        }
        this.mRemoteID = JSONUtils.getString(json, "id");
        if (this.mUniqueID == null) {
            this.mUniqueID = JSONUtils.getString(json, COLUMN_UNIQUE_ID);
        }
        this.mTitle = null;
        this.mStatus = JSONUtils.getString(json, "status");
        this.mLocation = loadLocation(json);
        if (!isSynchronized()) {
            this.mSynchronized = JSONUtils.getBoolean(json, "synchronized");
        }
        if (isSynchronized() && this.mSynchronizedAt == 0) {
            this.mSynchronizedAt = JSONUtils.getLong(json, COLUMN_SYNCHRONIZED_AT);
        }
        this.mValueContainer = null;
        this.mValueContainerJSON = JSONUtils.getHashMap(json, "form_values");
        if (this.mValuesIndexRowID == 0) {
            JSONUtils.getLong(json, COLUMN_VALUES_INDEX_ROW_ID);
        }
        this.mVersion = JSONUtils.getLong(json, "version");
        this.mAssignedTo = JSONUtils.getString(json, JSON_KEY_ASSIGNED_TO_ID);
        if (!isDraft()) {
            setDraft(JSONUtils.getBoolean(json, COLUMN_DRAFT));
        }
        if (!isNew()) {
            setNew(JSONUtils.getBoolean(json, COLUMN_IS_NEW));
        }
        this.mCreatedLocation = loadLocation(JSONUtils.getHashMap(json, JSON_KEY_CREATED_LOCATION));
        this.mUpdatedLocation = loadLocation(JSONUtils.getHashMap(json, JSON_KEY_UPDATED_LOCATION));
        this.mCreatedDuration = JSONUtils.getInt(json, "created_duration", null);
        this.mUpdatedDuration = JSONUtils.getInt(json, "updated_duration", null);
        this.mEditedDuration = JSONUtils.getInt(json, "edited_duration", null);
    }

    public String getTitle() {
        if (TextUtils.isEmpty(this.mTitle)) {
            Form form = getForm();
            if (form != null) {
                Element[] titleElements = form.getTitleElements();
                if (titleElements.length > 0) {
                    boolean first = true;
                    for (Element titleElement : titleElements) {
                        if (titleElement != null) {
                            FormValue value = getFormValue(titleElement);
                            if (value != null) {
                                if (first) {
                                    first = false;
                                    this.mTitle = value.getDisplayValue();
                                } else {
                                    this.mTitle += ", " + value.getDisplayValue();
                                }
                            }
                        }
                    }
                }
            }
            if (TextUtils.isEmpty(this.mTitle)) {
                this.mTitle = "Untitled";
            }
        }
        return this.mTitle;
    }

    public String getSearchableValue() {
        return getFormValues().getSearchableValue();
    }

    public long getValuesIndexRowID() {
        return this.mValuesIndexRowID;
    }

    public void setSynchronized(boolean isSynchronized) {
        this.mSynchronized = isSynchronized;
        if (isSynchronized) {
            this.mSynchronizedAt = new Date().getTime();
        }
    }

    public boolean isSynchronized() {
        return this.mSynchronized;
    }

    public boolean isDraft() {
        return this.mDraft;
    }

    public void setDraft(boolean draft) {
        this.mDraft = draft;
    }

    public boolean isNew() {
        return this.mIsNew;
    }

    public void setNew(boolean isNew) {
        this.mIsNew = isNew;
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = new HashMap();
        Project project = getProject();
        json.put("project_id", project != null ? project.getRemoteID() : null);
        Form form = getForm();
        if (form != null) {
            json.put("form_id", form.getRemoteID());
        }
        if (this.mCreatedAt != null) {
            json.put(JSON_KEY_CLIENT_CREATED_AT, DateUtils.getTimestampString(getCreatedAt()));
        }
        if (this.mUpdatedAt != null) {
            json.put(JSON_KEY_CLIENT_UPDATED_AT, DateUtils.getTimestampString(getUpdatedAt()));
        }
        json.put("id", this.mRemoteID);
        json.put("status", this.mStatus);
        json.putAll(saveLocation(this.mLocation, true));
        json.put("form_values", getFormValuesJSON());
        json.put("version", Long.valueOf(this.mVersion));
        json.put(JSON_KEY_CREATED_LOCATION, saveLocation(this.mCreatedLocation, false));
        json.put(JSON_KEY_UPDATED_LOCATION, saveLocation(this.mUpdatedLocation, false));
        json.put("created_duration", this.mCreatedDuration);
        json.put("updated_duration", this.mUpdatedDuration);
        json.put("edited_duration", this.mEditedDuration);
        return json;
    }

    public HashMap<String, Object> toLocalJSON() {
        HashMap<String, Object> json = toJSON();
        json.put(COLUMN_UNIQUE_ID, this.mUniqueID);
        json.put("synchronized", Boolean.valueOf(isSynchronized()));
        json.put(COLUMN_SYNCHRONIZED_AT, Long.valueOf(this.mSynchronizedAt));
        json.put(COLUMN_VALUES_INDEX_ROW_ID, Long.valueOf(getValuesIndexRowID()));
        json.put(COLUMN_ASSIGNED_TO, this.mAssignedTo);
        json.put(COLUMN_DRAFT, Boolean.valueOf(isDraft()));
        json.put(COLUMN_IS_NEW, Boolean.valueOf(isNew()));
        return json;
    }

    protected ContentValues getContentValues() {
        ContentValues values = super.getContentValues();
        values.put("account_id", Long.valueOf(this.mAccountID));
        if (this.mProjectID < 1) {
            values.putNull("project_id");
        } else {
            values.put("project_id", Long.valueOf(this.mProjectID));
        }
        values.put("form_id", Long.valueOf(this.mFormID));
        if (this.mCreatedAt != null) {
            values.put(COLUMN_CREATED_AT, Long.valueOf(this.mCreatedAt.getTime()));
        } else {
            values.putNull(COLUMN_CREATED_AT);
        }
        if (this.mUpdatedAt != null) {
            values.put(COLUMN_UPDATED_AT, Long.valueOf(this.mUpdatedAt.getTime()));
        } else {
            values.putNull(COLUMN_UPDATED_AT);
        }
        values.put("remote_id", this.mRemoteID);
        values.put(COLUMN_UNIQUE_ID, getUniqueID());
        values.put("title", getTitle());
        values.put("status", this.mStatus);
        values = saveLocation(values, this.mLocation, "latitude", "longitude", "altitude", COLUMN_BEARING, COLUMN_ACCURACY);
        values.put(COLUMN_SYNCHRONIZED, Boolean.valueOf(this.mSynchronized));
        values.put(COLUMN_SYNCHRONIZED_AT, Long.valueOf(this.mSynchronizedAt));
        values.put("form_values", JSONUtils.toJSONString(getFormValuesJSON()));
        values.put(COLUMN_VALUES_INDEX_ROW_ID, Long.valueOf(this.mValuesIndexRowID));
        values.put("version", Long.valueOf(this.mVersion));
        values.put(COLUMN_ASSIGNED_TO, this.mAssignedTo);
        values.put(COLUMN_DRAFT, Boolean.valueOf(this.mDraft));
        values.put(COLUMN_IS_NEW, Boolean.valueOf(this.mIsNew));
        String str = null;
        values = saveLocation(saveLocation(values, this.mCreatedLocation, COLUMN_CREATED_LATITUDE, COLUMN_CREATED_LONGITUDE, COLUMN_CREATED_ALTITUDE, null, COLUMN_CREATED_HORIZONTAL_ACC), this.mUpdatedLocation, COLUMN_UPDATED_LATITUDE, COLUMN_UPDATED_LONGITUDE, COLUMN_UPDATED_ALTITUDE, str, COLUMN_UPDATED_HORIZONTAL_ACC);
        values.put("created_duration", this.mCreatedDuration);
        values.put("updated_duration", this.mUpdatedDuration);
        values.put("edited_duration", this.mEditedDuration);
        return values;
    }

    protected String getTableName() {
        return TABLE_NAME;
    }

    protected boolean save(SQLiteDatabase db) {
        return save(db, true);
    }

    public boolean save(SQLiteDatabase db, boolean updateRecordValuesTables) {
        SearchIndex index = GIS.getSearchIndex();
        if (index != null) {
            this.mValuesIndexRowID = index.updateRecordValuesIndex(this);
        }
        if (db == null) {
            db = GIS.getDatabase();
        }
        boolean result = super.save(db);
        if (result && updateRecordValuesTables) {
            RecordValues.updateForRecord(db, this, true);
        }
        return result;
    }

    public void save(boolean updateRecordValuesTables) {
        save(null, updateRecordValuesTables);
    }

    public boolean saveWithTransaction() {
        SQLiteDatabase db = GIS.getDatabase();
        boolean result = false;
        try {
            db.beginTransactionNonExclusive();
            result = save(db);
            db.setTransactionSuccessful();
            return result;
        } finally {
            db.endTransaction();
        }
    }

    protected void delete(SQLiteDatabase db, Bundle options) {
        SearchIndex index = GIS.getSearchIndex();
        if (index != null) {
            index.deleteRecordValuesIndex(this);
        }
        RecordValues.deleteForRecord(db, this);
        super.delete(db, options);
    }

    public void deleteWithTransaction() {
        SQLiteDatabase db = GIS.getDatabase();
        try {
            db.beginTransactionNonExclusive();
            delete(db, null);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    private Location loadLocation(Cursor cursor, String latitudeColumn, String longitudeColumn, String altitudeColumn, String bearingColumn, String accuracyColumn) {
        Location location = null;
        Double latitude = CursorUtils.getDoubleOrNull(cursor, latitudeColumn);
        Double longitude = CursorUtils.getDoubleOrNull(cursor, longitudeColumn);
        if (!(latitude == null || longitude == null)) {
            location = new Location("Unknown");
            location.setLatitude(latitude.doubleValue());
            location.setLongitude(longitude.doubleValue());
            Double altitude = CursorUtils.getDoubleOrNull(cursor, altitudeColumn);
            Float accuracy = CursorUtils.getFloatOrNull(cursor, accuracyColumn);
            if (altitude != null) {
                location.setAltitude(altitude.doubleValue());
            }
            if (bearingColumn != null) {
                Float bearing = CursorUtils.getFloatOrNull(cursor, bearingColumn);
                if (bearing != null) {
                    location.setBearing(bearing.floatValue());
                }
            }
            if (accuracy != null) {
                location.setAccuracy(accuracy.floatValue());
            }
        }
        return location;
    }

    public ContentValues saveLocation(ContentValues values, Location location, String latitudeColumn, String longitudeColumn, String altitudeColumn, String bearingColumn, String accuracyColumn) {
        Float f = null;
        if (location != null) {
            values.put(latitudeColumn, Double.valueOf(location.getLatitude()));
            values.put(longitudeColumn, Double.valueOf(location.getLongitude()));
            values.put(altitudeColumn, location.hasAltitude() ? Double.valueOf(location.getAltitude()) : null);
            if (bearingColumn != null) {
                Float valueOf;
                if (location.hasBearing()) {
                    valueOf = Float.valueOf(location.getBearing());
                } else {
                    valueOf = null;
                }
                values.put(bearingColumn, valueOf);
            }
            if (location.hasAccuracy()) {
                f = Float.valueOf(location.getAccuracy());
            }
            values.put(accuracyColumn, f);
        } else {
            values.putNull(latitudeColumn);
            values.putNull(longitudeColumn);
            values.putNull(altitudeColumn);
            if (bearingColumn != null) {
                values.putNull(bearingColumn);
            }
            values.putNull(accuracyColumn);
        }
        return values;
    }

    private Location loadLocation(Map json) {
        Location location = null;
        if (JSONUtils.hasKey(json, "latitude") && JSONUtils.hasKey(json, "longitude")) {
            location = new Location("Unknown");
            location.setLatitude(JSONUtils.getDouble(json, "latitude"));
            location.setLongitude(JSONUtils.getDouble(json, "longitude"));
            if (JSONUtils.hasKey(json, "altitude")) {
                location.setAltitude(JSONUtils.getDouble(json, "altitude"));
            }
            if (JSONUtils.hasKey(json, JSON_KEY_COURSE)) {
                location.setBearing((float) JSONUtils.getDouble(json, JSON_KEY_COURSE));
            }
            if (JSONUtils.hasKey(json, JSON_KEY_HORIZONTAL_ACCURACY)) {
                location.setAccuracy((float) JSONUtils.getDouble(json, JSON_KEY_HORIZONTAL_ACCURACY));
            }
        }
        return location;
    }

    private HashMap<String, Object> saveLocation(Location location, boolean recordLocation) {
        Object obj = null;
        HashMap<String, Object> json;
        if (location != null) {
            json = new HashMap();
            json.put("latitude", Double.valueOf(location.getLatitude()));
            json.put("longitude", Double.valueOf(location.getLongitude()));
            json.put("altitude", location.hasAltitude() ? Double.valueOf(location.getAltitude()) : null);
            if (recordLocation) {
                Object valueOf;
                String str = JSON_KEY_COURSE;
                if (location.hasBearing()) {
                    valueOf = Float.valueOf(location.getBearing());
                } else {
                    valueOf = null;
                }
                json.put(str, valueOf);
            }
            String str2 = JSON_KEY_HORIZONTAL_ACCURACY;
            if (location.hasAccuracy()) {
                obj = Float.valueOf(location.getAccuracy());
            }
            json.put(str2, obj);
            return json;
        } else if (!recordLocation) {
            return null;
        } else {
            json = new HashMap();
            json.put("latitude", null);
            json.put("longitude", null);
            json.put("altitude", null);
            json.put(JSON_KEY_COURSE, null);
            json.put(JSON_KEY_HORIZONTAL_ACCURACY, null);
            return json;
        }
    }

    private Map getFormValuesJSON() {
        return getFormValues().toJSON();
    }

    private FormValue getFormValue(Element element) {
        return getFormValues().getFormValue(element);
    }

    public void writeToParcel(Parcel dest, int flags) {
        int i;
        int i2 = 1;
        super.writeToParcel(dest, flags);
        dest.writeLong(this.mAccountID);
        dest.writeLong(this.mProjectID);
        dest.writeLong(this.mFormID);
        if (this.mCreatedAt != null) {
            dest.writeLong(this.mCreatedAt.getTime());
        } else {
            dest.writeLong(Long.MAX_VALUE);
        }
        if (this.mUpdatedAt != null) {
            dest.writeLong(this.mUpdatedAt.getTime());
        } else {
            dest.writeLong(Long.MAX_VALUE);
        }
        dest.writeString(this.mRemoteID);
        dest.writeString(this.mUniqueID);
        dest.writeString(this.mTitle);
        dest.writeString(this.mStatus);
        dest.writeParcelable(this.mLocation, 0);
        if (this.mSynchronized) {
            i = 1;
        } else {
            i = 0;
        }
        dest.writeInt(i);
        dest.writeLong(this.mSynchronizedAt);
        dest.writeString(JSONUtils.toJSONString(getFormValuesJSON()));
        dest.writeLong(this.mValuesIndexRowID);
        dest.writeLong(this.mVersion);
        dest.writeString(this.mAssignedTo);
        if (this.mDraft) {
            i = 1;
        } else {
            i = 0;
        }
        dest.writeInt(i);
        if (!this.mIsNew) {
            i2 = 0;
        }
        dest.writeInt(i2);
        dest.writeParcelable(this.mCreatedLocation, 0);
        dest.writeParcelable(this.mUpdatedLocation, 0);
        dest.writeValue(this.mCreatedDuration);
        dest.writeValue(this.mUpdatedDuration);
        dest.writeValue(this.mEditedDuration);
    }
}

