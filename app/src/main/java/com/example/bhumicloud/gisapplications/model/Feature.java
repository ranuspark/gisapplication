package com.example.bhumicloud.gisapplications.model;

import android.location.Location;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;
import com.example.bhumicloud.gisapplications.model.field.Element;
import java.util.Date;

public interface Feature extends Parcelable {
    Date getCreatedAt();

    Integer getCreatedDuration();

    Location getCreatedLocation();

    Integer getEditedDuration();

    FormValue getFormValue(String str);

    FormValueContainer getFormValues();

    String getIdentifier();

    LatLng getLatLng();

    Location getLocation();

    Date getUpdatedAt();

    Integer getUpdatedDuration();

    Location getUpdatedLocation();

    boolean hasLocation();

    void loadFromFeatureState(FeatureEditState featureEditState);

    void setCreatedAt(Date date);

    void setCreatedDuration(Integer num);

    void setCreatedLocation(Location location);

    void setEditedDuration(Integer num);

    void setFormValue(Element element, FormValue formValue);

    void setLocation(Location location);

    void setUpdatedAt(Date date);

    void setUpdatedDuration(Integer num);

    void setUpdatedLocation(Location location);
}

