package com.example.bhumicloud.gisapplications.Activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;

public class PredefinedListDialog extends ChoicePickerDialog {
    private static final String ARG_CHOICE_ITEMS_ID = "gis:arg:dialog_choice_items_id";
    private static final String ARG_SELECTED_VALUE = "gis:arg:dialog_selected_value";
    private static final String ARG_TITLE_RESOURCE_ID = "gis:arg:dialog_title_resource_id";
    private static final String STATE_CHOICE_ITEMS = "STATE_CHOICE_ITEMS";
    private String[] mChoiceItems;
    private OnChoiceItemClickedListener mListener;
    private int mSelectedItem = -1;

    public interface OnChoiceItemClickedListener {
        void onChoiceItemClicked(Object obj, int i, boolean z);
    }

    public static PredefinedListDialog getInstance(int titleResourceId, int choiceItemsId, String selectedValue) {
        Bundle args = new Bundle ();
        PredefinedListDialog fragment = new PredefinedListDialog();
        args.putInt(ARG_TITLE_RESOURCE_ID, titleResourceId);
        args.putInt(ARG_CHOICE_ITEMS_ID, choiceItemsId);
        args.putString(ARG_SELECTED_VALUE, selectedValue);
        fragment.setArguments(args);
        return fragment;
    }

    public static PredefinedListDialog getInstance(int titleResourceId, int choiceItemsId) {
        return getInstance(titleResourceId, choiceItemsId, null);
    }

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        Bundle data = getArguments();
        int titleResourceId = data.getInt(ARG_TITLE_RESOURCE_ID);
        int choiceItemsId = data.getInt(ARG_CHOICE_ITEMS_ID);
        String selectedValue = data.getString(ARG_SELECTED_VALUE);
        if (titleResourceId > 0) {
            setTitle(titleResourceId);
        }
        if (choiceItemsId != 0) {
            this.mChoiceItems = getResources().getStringArray(choiceItemsId);
        }
        if (savedState != null) {
            this.mChoiceItems = savedState.getStringArray(STATE_CHOICE_ITEMS);
        }
        if (!TextUtils.isEmpty(selectedValue)) {
            for (int index = 0; index < this.mChoiceItems.length; index++) {
                if (selectedValue.equals(this.mChoiceItems[index])) {
                    this.mSelectedItem = index;
                }
            }
        }
        setChoiceMode(1);
    }

    public void onViewCreated(View view, Bundle savedState) {
        super.onViewCreated(view, savedState);
        if (-1 < this.mSelectedItem && this.mSelectedItem < this.mChoiceItems.length) {
            getListView().setItemChecked(this.mSelectedItem, true);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArray(STATE_CHOICE_ITEMS, this.mChoiceItems);
    }

    public int getCount() {
        if (this.mChoiceItems != null) {
            return this.mChoiceItems.length;
        }
        return 0;
    }

    public Object getItem(int position) {
        return this.mChoiceItems[position];
    }

    public boolean isEmpty() {
        return this.mChoiceItems == null || this.mChoiceItems.length > 0;
    }

    protected boolean onChoiceItemClicked(ListView listView, int position, boolean selected) {
        if (selected) {
            dismissAllowingStateLoss();
        }
        if (this.mListener != null) {
            this.mListener.onChoiceItemClicked(listView.getItemAtPosition(position), position, selected);
        }
        return super.onChoiceItemClicked(listView, position, selected);
    }

    public void setOnChoiceItemClickedListener(OnChoiceItemClickedListener listener) {
        this.mListener = listener;
    }

    public void setSelectedItem(int itemIndex) {
        this.mSelectedItem = itemIndex;
    }
}
