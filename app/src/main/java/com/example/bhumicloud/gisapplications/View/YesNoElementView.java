package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import android.text.TextUtils;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ToggleButton;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.field.choice.ChoiceItem;
import com.example.bhumicloud.gisapplications.model.field.textual.yesno.YesNoElement;
import com.example.bhumicloud.gisapplications.model.field.textual.yesno.YesNoValue;

public class YesNoElementView extends ElementView<YesNoElement> implements OnCheckedChangeListener {
    private YesNoElementViewListener mListener;
    private ToggleButton mNegativeToggleButton;
    private String mNegativeValue;
    private ToggleButton mNeutralToggleButton;
    private String mNeutralValue;
    private ToggleButton mPositiveToggleButton;
    private String mPositiveValue;
    private int mUserEventLock;
    private String label;

    public interface YesNoElementViewListener {
        void onYesNoValueSelected(YesNoElement yesNoElement, String str);
    }

    public YesNoElementView(Context context, YesNoElement element, boolean viewOnly) {
        super(context, element, viewOnly);
    }

    public void onCheckedChanged(CompoundButton button, boolean isChecked) {
        switch (button.getId()) {
            case R.id.positive_toggle_button:
                onPositiveButtonToggled(isChecked);
                return;
            case R.id.negative_toggle_button:
                onNegativeButtonToggled(isChecked);
                return;
            case R.id.neutral_toggle_button:
                onNeutralButtonToggled(isChecked);
                return;
            default:
                return;
        }
    }

    public void setValue(FormValue value) {
        setUserEvent(false);
        resetButtons();
        if (value instanceof YesNoValue) {
            YesNoValue yesNo = (YesNoValue) value;
            if (!yesNo.isEmpty()) {
                String selected = yesNo.getValue();
                if (TextUtils.equals(selected, this.mPositiveValue)) {
                    this.mPositiveToggleButton.setChecked(true);
                } else if (TextUtils.equals(selected, this.mNegativeValue)) {
                    this.mNegativeToggleButton.setChecked(true);
                } else if (TextUtils.equals(selected, this.mNeutralValue)) {
                    this.mNeutralToggleButton.setChecked(true);
                }
            }
        }
        setUserEvent(true);
    }

    public void setListener(YesNoElementViewListener listener) {
        this.mListener = listener;
    }

    public void update() {
        boolean z;
        boolean z2 = true;
        super.update();
        ToggleButton toggleButton = this.mPositiveToggleButton;
        if (isReadOnly()) {
            z = false;
        } else {
            z = true;
        }
        toggleButton.setEnabled(z);
        toggleButton = this.mNegativeToggleButton;
        if (isReadOnly()) {
            z = false;
        } else {
            z = true;
        }
        toggleButton.setEnabled(z);
        ToggleButton toggleButton2 = this.mNeutralToggleButton;
        if (isReadOnly()) {
            z2 = false;
        }
        toggleButton2.setEnabled(z2);
    }

    protected void initialize() {
        super.initialize();
        this.mPositiveToggleButton = (ToggleButton) findViewById(R.id.positive_toggle_button);
        this.mNegativeToggleButton = (ToggleButton) findViewById(R.id.negative_toggle_button);
        this.mNeutralToggleButton = (ToggleButton) findViewById(R.id.neutral_toggle_button);
        this.mPositiveToggleButton.setTag(((YesNoElement) getElement()).getDataName() + ":positive");
        this.mNegativeToggleButton.setTag(((YesNoElement) getElement()).getDataName() + ":negative");
        this.mNeutralToggleButton.setTag(((YesNoElement) getElement()).getDataName() + ":neutral");
        this.mPositiveToggleButton.setSaveEnabled(false);
        this.mNegativeToggleButton.setSaveEnabled(false);
        this.mNeutralToggleButton.setSaveEnabled(false);
        this.mPositiveToggleButton.setOnCheckedChangeListener(this);
        this.mNegativeToggleButton.setOnCheckedChangeListener(this);
        this.mNeutralToggleButton.setOnCheckedChangeListener(this);
        ChoiceItem positive = ((YesNoElement) getElement()).getPositiveChoice();
        if (positive != null) {
            String label = positive.getLabel();
            this.mPositiveToggleButton.setTextOn(label);
            this.mPositiveToggleButton.setTextOff(label);
            this.mPositiveValue = positive.getValue();
        }
        ChoiceItem negative = ((YesNoElement) getElement()).getNegativeChoice();
        if (negative != null) {
            label = negative.getLabel();
            this.mNegativeToggleButton.setTextOn(label);
            this.mNegativeToggleButton.setTextOff(label);
            this.mNegativeValue = negative.getValue();
        }
        ChoiceItem neutral = ((YesNoElement) getElement()).getNeutralChoice();
        if (neutral != null) {
            label = neutral.getLabel();
            this.mNeutralToggleButton.setTextOn(label);
            this.mNeutralToggleButton.setTextOff(label);
            this.mNeutralValue = neutral.getValue();
        }
        if (((YesNoElement) getElement()).isNeutralChoiceEnabled()) {
            this.mNeutralToggleButton.setEnabled(true);
            this.mNeutralToggleButton.setVisibility(0);
            return;
        }
        this.mNeutralToggleButton.setEnabled(false);
        this.mNeutralToggleButton.setVisibility(8);
    }

    protected int getElementViewLayout() {
        return R.layout.view_yes_no_element;
    }

    private boolean isUserEvent() {
        return this.mUserEventLock == 0;
    }

    private void setUserEvent(boolean userEvent) {
        if (userEvent) {
            this.mUserEventLock--;
            if (this.mUserEventLock < 0) {
                this.mUserEventLock = 0;
                return;
            }
            return;
        }
        this.mUserEventLock++;
    }

    private void buttonClick() {
        onFieldViewClicked();
    }

    private void onPositiveButtonToggled(boolean isChecked) {
        if (isUserEvent()) {
            buttonClick();
            if (isChecked) {
                notifyValueSelected(this.mPositiveValue);
            } else {
                notifyValueSelected(null);
            }
        }
        if (isChecked) {
            setUserEvent(false);
            this.mNegativeToggleButton.setChecked(false);
            this.mNeutralToggleButton.setChecked(false);
            setUserEvent(true);
        }
    }

    private void onNegativeButtonToggled(boolean isChecked) {
        if (isUserEvent()) {
            buttonClick();
            if (isChecked) {
                notifyValueSelected(this.mNegativeValue);
            } else {
                notifyValueSelected(null);
            }
        }
        if (isChecked) {
            setUserEvent(false);
            this.mPositiveToggleButton.setChecked(false);
            this.mNeutralToggleButton.setChecked(false);
            setUserEvent(true);
        }
    }

    private void onNeutralButtonToggled(boolean isChecked) {
        if (isUserEvent()) {
            buttonClick();
            if (isChecked) {
                notifyValueSelected(this.mNeutralValue);
            } else {
                notifyValueSelected(null);
            }
        }
        if (isChecked) {
            setUserEvent(false);
            this.mPositiveToggleButton.setChecked(false);
            this.mNegativeToggleButton.setChecked(false);
            setUserEvent(true);
        }
    }

    private void resetButtons() {
        setUserEvent(false);
        this.mPositiveToggleButton.setChecked(false);
        this.mNegativeToggleButton.setChecked(false);
        this.mNeutralToggleButton.setChecked(false);
        setUserEvent(true);
    }

    private void notifyValueSelected(String value) {
        if (this.mListener != null) {
            this.mListener.onYesNoValueSelected((YesNoElement) getElement(), value);
        }
    }
}
