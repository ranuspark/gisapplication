package com.example.bhumicloud.gisapplications.model.appgallery;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.Map;

public abstract class Item implements Parcelable, Comparable<Item> {
    private String mName;
    private String mRemoteId;

    public Item(Map<String, Object> json) {
        this.mName = JSONUtils.getString((Map) json, "name");
    }

    protected Item(Parcel parcel) {
        this.mRemoteId = parcel.readString();
        this.mName = parcel.readString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mRemoteId);
        dest.writeString(this.mName);
    }

    public int describeContents() {
        return 0;
    }

    public int compareTo(@NonNull Item otherItem) {
        if (this.mName == null) {
            return otherItem.getName() == null ? 0 : -1;
        } else {
            return this.mName.compareToIgnoreCase(otherItem.getName());
        }
    }

    public String getRemoteId() {
        return this.mRemoteId;
    }

    public String getName() {
        return this.mName;
    }
}
