package com.example.bhumicloud.gisapplications.Activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.apiLayer.Client;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.PatronSettings;

public class ServerConfigurationDialog extends ThemedDialogFragment {
    private CheckBox mEnableHttps;
    private EditText mHostnameField;
    private boolean mUserInput;

    class C11311 implements OnCheckedChangeListener {
        C11311() {
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            ServerConfigurationDialog.this.onEnableHttpsValueChanged(isChecked);
        }
    }

    class C11322 implements TextWatcher {
        C11322() {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void afterTextChanged(Editable s) {
            ServerConfigurationDialog.this.onServerHostnameValueChanged(s.toString());
        }
    }

    class C11333 implements OnClickListener {
        C11333() {
        }

        public void onClick(View v) {
            ServerConfigurationDialog.this.onResetDefaultsButtonClicked();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle((int) R.string.server_configuration);
    }

    public View onCreateContentView(LayoutInflater inflater, ViewGroup root, Bundle savedState) {
        View view = inflater.inflate(R.layout.dialog_server_configuration, root, false);
        Button resetDefaults = (Button) view.findViewById(R.id.reset_defaults_btn);
        this.mEnableHttps = (CheckBox) view.findViewById(R.id.enable_https_checkbox);
        this.mHostnameField = (EditText) view.findViewById(R.id.server_hostname_field);
        this.mUserInput = false;
        this.mHostnameField.setText(PatronSettings.getHostname(getActivity()));
        this.mEnableHttps.setChecked( PatronSettings.isHttpsEnabled(getActivity()));
        this.mUserInput = true;
        this.mEnableHttps.setOnCheckedChangeListener(new C11311());
        this.mHostnameField.addTextChangedListener(new C11322());
        resetDefaults.setOnClickListener(new C11333());
        return view;
    }

    private void onEnableHttpsValueChanged(boolean enabled) {
        if (this.mUserInput) {
            PatronSettings.setHttpsEnabled(getActivity(), enabled);
        }
    }

    private void onServerHostnameValueChanged(String hostname) {
        if (this.mUserInput) {
            PatronSettings.setHostname(getActivity(), hostname);
        }
    }

    private void onResetDefaultsButtonClicked() {
        this.mEnableHttps.setChecked(true);
        this.mHostnameField.setText(Client.DEFAULT_HOSTNAME);
    }
}
