package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.field.LabelElement;

public class LabelElementView extends ElementView<LabelElement> {
    public LabelElementView(Context context, LabelElement element, boolean viewOnly) {
        super(context, element, viewOnly);
    }

    protected int getElementViewLayout() {
        return R.layout.view_label_element;
    }
}
