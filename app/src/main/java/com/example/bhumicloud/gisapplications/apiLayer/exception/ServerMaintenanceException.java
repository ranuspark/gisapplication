package com.example.bhumicloud.gisapplications.apiLayer.exception;

import android.content.Context;

import com.example.bhumicloud.gisapplications.R;

public class ServerMaintenanceException extends GISServiceException {
    public ServerMaintenanceException() {
        super("The server is currently undergoing maintenance.\\n\\nPlease try again later.");
    }

    public String getUserFriendlyMessage(Context context) {
        return context.getString(R.string.server_maintenance_message);
    }
}
