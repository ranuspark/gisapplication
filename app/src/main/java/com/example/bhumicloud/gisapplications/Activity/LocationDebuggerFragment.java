package com.example.bhumicloud.gisapplications.Activity;

import android.annotation.TargetApi;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.google.android.gms.location.LocationListener;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.example.bhumicloud.gisapplications.util.DeviceInfo;
import com.example.bhumicloud.gisapplications.util.NumberUtils;
import com.example.bhumicloud.gisapplications.util.PermissionsUtils;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LocationDebuggerFragment extends ListFragment implements ListAdapter, LocationListener {
    private static final float METERS_TO_FEET = 3.28084f;
    private static final int ROW_ACCURACY = 4;
    private static final int ROW_ALTITUDE = 5;
    private static final int ROW_BEARING = 6;
    private static final int ROW_EXTRAS = 8;
    private static final int ROW_LATITUDE = 2;
    private static final int ROW_LONGITUDE = 3;
    private static final int ROW_PROVIDER = 1;
    private static final int ROW_SPEED = 7;
    private static final int ROW_TIMESTAMP = 0;
    private DecimalFormat coord_df;
    private DecimalFormat df;
    private Context mContext;
    private Location mCurrentLocation;
    private final DataSetObservable mDataSetObservers = new DataSetObservable ();
    private final DateFormat mDateFormat = SimpleDateFormat.getDateTimeInstance();
    private LayoutInflater mInflater;
    private boolean mUSMeasurements = false;

    class C10731 implements OnClickListener {
        C10731() {
        }

        public void onClick(DialogInterface dialogInterface, int position) {
            PatronSettings.setLocationDebuggerUnits(LocationDebuggerFragment.this.mContext, position);
            LocationDebuggerFragment.this.setMeasurements(position);
            dialogInterface.dismiss();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContext = getActivity();
        this.mInflater = getActivity().getLayoutInflater();
        setListAdapter(this);
        setMeasurements(PatronSettings.getLocationDebuggerUnits(this.mContext));
        this.df = NumberUtils.getRegularFormat(getResources());
        this.coord_df = NumberUtils.getCoordinateFormat(getResources());
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.activity_location_debugger, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_units:
                showUnitsDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onStart() {
        super.onStart();
        if (PermissionsUtils.checkPermissionsAndRequest(getActivity(), 64)) {
            ((LocationDebuggerActivity) getActivity()).startLocationUpdates();
        }
    }

    public void onLocationChanged(Location location) {
        this.mCurrentLocation = location;
        this.mDataSetObservers.notifyChanged();
    }

    public int getCount() {
        return 9;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public boolean hasStableIds() {
        return false;
    }

    public int getItemViewType(int position) {
        return 0;
    }

    public int getViewTypeCount() {
        return 1;
    }

    public boolean isEmpty() {
        return false;
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    public boolean isEnabled(int position) {
        return false;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = this.mInflater.inflate(R.layout.list_item_title_value, parent, false);
        }
        TextView valueLabel = (TextView) view.findViewById(R.id.list_item_value);
        ((TextView) view.findViewById(R.id.list_item_title)).setText(getInfoTitle(position));
        valueLabel.setText(getInfoValue(position));
        return view;
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        this.mDataSetObservers.registerObserver(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        this.mDataSetObservers.unregisterObserver(observer);
    }

    private String getInfoTitle(int position) {
        switch (position) {
            case 0:
                return getString(R.string.timestamp);
            case 1:
                return getString(R.string.provider);
            case 2:
                return getString(R.string.latitude);
            case 3:
                return getString(R.string.longitude);
            case 4:
                return getString(R.string.accuracy);
            case 5:
                return getString(R.string.altitude);
            case 6:
                return getString(R.string.bearing);
            case 7:
                return getString(R.string.speed);
            case 8:
                return getString(R.string.extra_info);
            default:
                return getString(R.string.unknown);
        }
    }

    @TargetApi(18)
    private String getInfoValue(int position) {
        String unavailable = getString(R.string.unavailable);
        if (this.mCurrentLocation == null) {
            return unavailable;
        }
        Location loc = this.mCurrentLocation;
        switch (position) {
            case 0:
                return this.mDateFormat.format(new Date (loc.getTime()));
            case 1:
                String provider = loc.getProvider();
                if (TextUtils.isEmpty(provider)) {
                    provider = unavailable;
                }
                if (DeviceInfo.isJellyBeanMR2() && loc.isFromMockProvider()) {
                    provider = provider + " (mock)";
                }
                return String.format("%s", new Object[]{provider});
            case 2:
                return this.coord_df.format(loc.getLatitude());
            case 3:
                return this.coord_df.format(loc.getLongitude());
            case 4:
                if (!loc.hasAccuracy()) {
                    return unavailable;
                }
                if (this.mUSMeasurements) {
                    return this.df.format((double) (loc.getAccuracy() * METERS_TO_FEET)) + "ft";
                }
                return this.df.format((double) loc.getAccuracy()) + "m";
            case 5:
                if (!loc.hasAltitude()) {
                    return unavailable;
                }
                if (this.mUSMeasurements) {
                    return this.df.format(loc.getAltitude() * 3.2808399200439453d) + "ft";
                }
                return this.df.format(loc.getAltitude()) + "m";
            case 6:
                if (loc.hasBearing()) {
                    return this.df.format((double) loc.getBearing()) + "°";
                }
                return unavailable;
            case 7:
                if (loc.hasSpeed()) {
                    return convertSpeed(loc.getSpeed());
                }
                return unavailable + "\n" + unavailable;
            case 8:
                Bundle extras = loc.getExtras();
                if (extras != null) {
                    return extras.toString();
                }
                return unavailable;
            default:
                return unavailable;
        }
    }

    private void showUnitsDialog() {
        Builder builder = new Builder (this.mContext);
        builder.setTitle(R.string.menu_item_title_units);
        builder.setSingleChoiceItems(R.array.units, PatronSettings.getLocationDebuggerUnits(this.mContext), new C10731());
        builder.show();
    }

    private void setMeasurements(int position) {
        this.mUSMeasurements = position != 0;
    }

    private String convertSpeed(float speedMpS) {
        if (this.mUSMeasurements) {
            return this.df.format((double) (METERS_TO_FEET * speedMpS)) + " f/s\n" + this.df.format(((double) speedMpS) * 2.23694d) + " mph";
        }
        return this.df.format((double) speedMpS) + " m/s\n" + this.df.format(((double) speedMpS) * 3.6d) + " kph";
    }
}
