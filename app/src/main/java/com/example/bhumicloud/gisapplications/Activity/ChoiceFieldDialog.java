package com.example.bhumicloud.gisapplications.Activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.Activity.TextInputDialog.TextInputDialogListener;
import com.example.bhumicloud.gisapplications.model.field.choice.ChoiceElement;
import com.example.bhumicloud.gisapplications.model.field.choice.ChoiceItem;
import com.example.bhumicloud.gisapplications.model.field.choice.ChoiceValue;
import com.example.bhumicloud.gisapplications.model.validation.error.FieldLengthValidationError;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ChoiceFieldDialog extends ChoicePickerDialog {
    private static final String STATE_CHOICES = "STATE_CHOICES";
    private static final String STATE_OTHER_ENABLED = "STATE_OTHER_ENABLED";
    private static final String STATE_OTHER_SELECTED = "STATE_OTHER_SELECTED";
    private static final String STATE_OTHER_VALUE = "STATE_OTHER_VALUE";
    private static final String STATE_SELECTED_VALUES = "STATE_SELECTED_VALUES";
    private static final String TAG_OTHER_FIELD_DIALOG = "TAG_OTHER_FIELD_DIALOG";
    private ChoiceElement mChoiceElement;
    private List<ChoiceItem> mChoices;
    private OnChoiceFieldSelectionListener mListener;
    private List<ChoiceItem> mMatchingChoices;
    private boolean mOtherEnabled;
    private final TextInputDialogListener mOtherFieldDialogListener = new C10481();
    private boolean mOtherSelected;
    private String mOtherValue;
    private final Set<String> mSelectedValues = new HashSet ();

    class C10481 implements TextInputDialogListener {
        C10481() {
        }

        public void onTextValueEntered(String value) {
            ChoiceFieldDialog.this.onOtherFieldDialogFinished(value);
        }
    }

    public interface OnChoiceFieldSelectionListener {
        void onChoiceFieldSelection(List<String> list, String str);
    }

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        setSearchEnabled(true);
        setNegativeButtonText((int) R.string.clear);
        if (getChoiceMode() == 2) {
            setPositiveButtonText((int) R.string.done);
        }
        if (savedState != null) {
            this.mOtherValue = savedState.getString(STATE_OTHER_VALUE);
            this.mOtherEnabled = savedState.getBoolean(STATE_OTHER_ENABLED);
            this.mOtherSelected = savedState.getBoolean(STATE_OTHER_SELECTED);
            this.mChoices = new ArrayList ();
            String choicesString = savedState.getString(STATE_CHOICES);
            if (!TextUtils.isEmpty(choicesString)) {
                try {
                    List json = JSONUtils.arrayFromJSON(choicesString);
                    for (int i = 0; i < json.size(); i++) {
                        HashMap<String, Object> itemJSON = JSONUtils.getHashMap(json, i);
                        if (itemJSON != null) {
                            this.mChoices.add(new ChoiceItem(itemJSON));
                        }
                    }
                } catch (IOException e) {
                    throw new RuntimeException (e);
                }
            }
            String[] selected = savedState.getStringArray(STATE_SELECTED_VALUES);
            if (selected != null) {
                Collections.addAll(this.mSelectedValues, selected);
            }
        }
    }

    public void onViewCreated(View view, Bundle savedState) {
        super.onViewCreated(view, savedState);
        reloadSelection();
        ListView list = getListView();
        if (this.mOtherEnabled && !TextUtils.isEmpty(this.mOtherValue)) {
            this.mOtherSelected = true;
            list.setItemChecked(getCount() - 1, true);
        }
        TextInputDialog dialog = (TextInputDialog) getChildFragmentManager().findFragmentByTag(TAG_OTHER_FIELD_DIALOG);
        if (dialog != null) {
            dialog.setTextInputDialogListener(this.mOtherFieldDialogListener);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_OTHER_VALUE, this.mOtherValue);
        outState.putBoolean(STATE_OTHER_ENABLED, this.mOtherEnabled);
        outState.putBoolean(STATE_OTHER_SELECTED, this.mOtherSelected);
        if (this.mChoices != null) {
            ArrayList<Object> choices = new ArrayList ();
            for (ChoiceItem item : this.mChoices) {
                choices.add(item.toJSON());
            }
            outState.putString(STATE_CHOICES, JSONUtils.toJSONString(choices));
        }
        outState.putStringArray(STATE_SELECTED_VALUES, (String[]) this.mSelectedValues.toArray(new String[this.mSelectedValues.size()]));
    }

    public void onCancel(DialogInterface dialog) {
        commitSelection();
        super.onCancel(dialog);
    }

    public boolean isEmpty() {
        if (this.mMatchingChoices != null) {
            return this.mMatchingChoices.isEmpty();
        }
        return !this.mOtherEnabled && (this.mChoices == null || this.mChoices.isEmpty());
    }

    public int getCount() {
        if (this.mMatchingChoices != null) {
            return this.mMatchingChoices.size();
        }
        int count = 0;
        if (this.mOtherEnabled) {
            count = 0 + 1;
        }
        if (this.mChoices != null) {
            return count + this.mChoices.size();
        }
        return count;
    }

    public ChoiceItem getItem(int position) {
        if (this.mMatchingChoices != null) {
            return (ChoiceItem) this.mMatchingChoices.get(position);
        }
        if (!isOtherFieldItem(position)) {
            return (ChoiceItem) this.mChoices.get(position);
        }
        String label;
        if (TextUtils.isEmpty(this.mOtherValue)) {
            label = getString(R.string.other_choice_button);
        } else {
            label = this.mOtherValue;
        }
        return new ChoiceItem(label, label);
    }

    public void setChoiceValue(ChoiceValue value) {
        this.mChoiceElement = value.getElement();
        setTitle(this.mChoiceElement.getLabel());
        if (this.mChoiceElement.isMultipleChoice()) {
            setChoiceMode(2);
        } else {
            setChoiceMode(1);
        }
        this.mOtherEnabled = this.mChoiceElement.isOtherFieldAllowed();
        this.mChoices = this.mChoiceElement.getChoices();
        this.mSelectedValues.addAll(value.getSelectedValues());
        this.mOtherValue = value.getOtherValue();
        notifyDataSetChanged();
    }

    public void setOnChoiceFieldSelectionListener(OnChoiceFieldSelectionListener listener) {
        this.mListener = listener;
    }

    protected void onNegativeButtonClicked() {
        super.onNegativeButtonClicked();
        clearSelection();
        this.mOtherValue = null;
        commitSelection();
        dismissAllowingStateLoss();
    }

    protected void onPositiveButtonClicked() {
        super.onPositiveButtonClicked();
        commitSelection();
        dismissAllowingStateLoss();
    }

    protected void onSearchQueryDidChange(String query) {
        String trimmed = query.trim();
        if (TextUtils.isEmpty(trimmed)) {
            this.mMatchingChoices = null;
        } else {
            this.mMatchingChoices = new ArrayList ();
            for (ChoiceItem item : this.mChoices) {
                if (item.getLabel().toLowerCase().contains(trimmed.toLowerCase())) {
                    this.mMatchingChoices.add(item);
                }
            }
        }
        notifyDataSetChanged();
        reloadSelection();
    }

    protected boolean onChoiceItemClicked(ListView listView, int position, boolean selected) {
        if (getChoiceMode() == 1) {
            return onSingleChoiceItemClicked(position, selected);
        }
        if (getChoiceMode() == 2) {
            return onMultipleChoiceItemClicked(listView, position, selected);
        }
        return super.onChoiceItemClicked(listView, position, selected);
    }

    private boolean onSingleChoiceItemClicked(int position, boolean selected) {
        if (isOtherFieldItem(position) && selected) {
            showOtherFieldDialog();
            return false;
        }
        Set<String> values = this.mSelectedValues;
        values.clear();
        if (this.mOtherSelected) {
            this.mOtherValue = null;
            this.mOtherSelected = false;
        }
        if (selected) {
            values.add(getItem(position).getValue());
        } else {
            values.remove(getItem(position).getValue());
        }
        commitSelection();
        return true;
    }

    private boolean onMultipleChoiceItemClicked(ListView listView, int position, boolean selected) {
        Set<String> values = this.mSelectedValues;
        if (!selected) {
            values.remove(getItem(position).getValue());
            if (this.mOtherSelected) {
                this.mOtherValue = null;
                this.mOtherSelected = false;
            }
        } else if (this.mChoiceElement != null && this.mChoiceElement.hasMaxLength() && values.size() >= this.mChoiceElement.getMaxLength()) {
            Toast.makeText(getActivity(), new FieldLengthValidationError(this.mChoiceElement).getMessage(), Toast.LENGTH_SHORT).show();
            listView.setItemChecked(position, false);
        } else if (isOtherFieldItem(position)) {
            showOtherFieldDialog();
        } else {
            values.add(getItem(position).getValue());
        }
        return false;
    }

    private boolean isOtherFieldItem(int position) {
        return this.mMatchingChoices == null && this.mOtherEnabled && position == getCount() - 1;
    }

    private void clearSelection() {
        ListView list = getListView();
        for (int i = 0; i < getCount(); i++) {
            list.setItemChecked(i, false);
        }
        this.mSelectedValues.clear();
        this.mOtherValue = null;
    }

    private void reloadSelection() {
        ListView list = getListView();
        for (int i = 0; i < getCount(); i++) {
            list.setItemChecked(i, false);
        }
        for (String value : this.mSelectedValues) {
            int position = getIndexOfValue(value);
            if (position >= 0) {
                list.setItemChecked(position, true);
            }
        }
        if (this.mOtherEnabled && this.mMatchingChoices == null) {
            list.setItemChecked(getCount() - 1, this.mOtherSelected);
        }
    }

    private int getIndexOfValue(String value) {
        List<ChoiceItem> choices = this.mChoices;
        if (this.mMatchingChoices != null) {
            choices = this.mMatchingChoices;
        }
        for (int i = 0; i < choices.size(); i++) {
            if (value.equals(((ChoiceItem) choices.get(i)).getValue())) {
                return i;
            }
        }
        return -1;
    }

    private void showOtherFieldDialog() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle(getString(R.string.other_field_title));
        dialog.setTextValue(this.mOtherValue);
        dialog.setTextInputDialogListener(this.mOtherFieldDialogListener);
        dialog.show(getChildFragmentManager(), TAG_OTHER_FIELD_DIALOG);
    }

    private void onOtherFieldDialogFinished(String value) {
        this.mOtherValue = value;
        ListView list = getListView();
        int otherPosition = getCount() - 1;
        if (!TextUtils.isEmpty(value)) {
            this.mOtherSelected = true;
            list.setItemChecked(otherPosition, true);
            if (getChoiceMode() == 1) {
                this.mSelectedValues.clear();
                commitSelection();
                dismiss();
            }
        }
        notifyDataSetChanged();
    }

    private void commitSelection() {
        if (this.mListener != null) {
            if (this.mOtherEnabled && !this.mOtherSelected) {
                this.mOtherValue = null;
            }
            ArrayList<String> selections = new ArrayList (this.mSelectedValues);
            List<String> orderedValues = new ArrayList ();
            if (this.mChoices != null) {
                for (ChoiceItem item : this.mChoices) {
                    String value = item.getValue();
                    if (selections.contains(value)) {
                        orderedValues.add(value);
                        selections.remove(value);
                    }
                }
            }
            orderedValues.addAll(selections);
            this.mListener.onChoiceFieldSelection(orderedValues, this.mOtherValue);
        }
    }
}
