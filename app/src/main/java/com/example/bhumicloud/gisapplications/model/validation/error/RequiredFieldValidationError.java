package com.example.bhumicloud.gisapplications.model.validation.error;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.validation.FeatureValidationError;

public class RequiredFieldValidationError extends FeatureValidationError {
    public RequiredFieldValidationError(Record record, Element element) {
        String label = record == null ? element.getLabel() : element.getAbsoluteLabel(record);
        this.mMessage = String.format(GIS.getInstance().getResources().getString(R.string.required_field_validation_error), new Object[]{label});
    }
}
