package com.example.bhumicloud.gisapplications.Activity;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ResourceCursorAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bhumicloud.gisapplications.Activity.ContextualBottomSheetRecordMenu.ContextualBottomSheetRecordMenuListener;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.dossier.RecordCursorLoader;
import com.example.bhumicloud.gisapplications.dossier.RecordPredicate;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.Search;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement.StatusOption;
import com.example.bhumicloud.gisapplications.util.CursorUtils;
import com.example.bhumicloud.gisapplications.util.DateUtils;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.IntentUtils;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.example.bhumicloud.gisapplications.util.RecordFilterOptions;
import com.example.bhumicloud.gisapplications.widget.FloatingActionButton;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;

public class RecordListFragment1 extends GISListFragment implements RecordCollectionView, ContextualBottomSheetRecordMenuListener, LoaderCallbacks<Cursor> {
    private static final int LOADER_ID = 110;
    private static final String TAG_CONTEXTUAL_MENU_DIALOG = "TAG_CONTEXTUAL_MENU_DIALOG";
    private boolean mCreateRecordsEnabled = true;
    private TextView mDisplayedCount;
    private RecordFilterOptions mFilterOptions;
    private FloatingActionButton mFloatingActionButton;
    private Form mForm;
    private RecordCollectionViewListener mListener;
    private RecordPredicate mRecordConditions;
    private RecordCursorAdapter mRecordCursorAdapter;
    private String mRecordSearchQuery;
    private int mRowLayoutID;
    private ArrayList<String> mSelectedRecordIDs;
    private ArrayList<String> mNNSelectedRecordIDs=new ArrayList<>();

    static ArrayList<Search> re_list=new ArrayList<>();
    Cursor cursor12;

    class C11081 implements OnClickListener {
        C11081() {
        }

        public void onClick(View v) {
            RecordListFragment1.this.mListener.onNewRecordOptionSelected();
        }
    }

    class C11092 implements OnItemLongClickListener {
        C11092() {
        }

        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
            Bundle args = new Bundle ();
            args.putInt(ContextualBottomSheetRecordMenu.ARG_POSITION, position);
            args.putLong(ContextualBottomSheetRecordMenu.ARG_ID, id);
            CharSequence titleSeq = ((TextView) view.findViewById(R.id.record_title)).getText();
            args.putString(ContextualBottomSheetMenu.ARG_TITLE, titleSeq == null ? null : titleSeq.toString());
            ContextualBottomSheetRecordMenu menuDialog = new ContextualBottomSheetRecordMenu();
            menuDialog.setArguments(args);
            menuDialog.setListener(RecordListFragment1.this);
            menuDialog.show(RecordListFragment1.this.getChildFragmentManager(), RecordListFragment1.TAG_CONTEXTUAL_MENU_DIALOG);
            return true;
        }
    }

    private static class RecordCursorAdapter extends ResourceCursorAdapter {
        private Context mContext;
        private StatusElement mStatusElement;

        public RecordCursorAdapter(Context context, int layoutResourceID,Cursor cursor) {
            super(context, layoutResourceID, cursor, 0);
            this.mContext = context;
        }

        public void setStatusElement(StatusElement statusElement) {
            this.mStatusElement = statusElement;
        }

        public void bindView(View view, Context context, Cursor cursor) {
            StatusOption statusOption;
            SQLiteDatabase db = GIS.getDatabase();

           // Cursor cursor12 = null;


//            long long_id = cursor.getLong(cursor.getColumnIndex("_id"));
//            status = cursor.getString(cursor.getColumnIndex("status"));
//            Log.e("Value12", long_id + "  " + "status ");
//
//
//            view.setTag(long_id);

            view.setTag( Long.valueOf(CursorUtils.getLong(cursor, "_id")));
            String status = cursor.getString(cursor.getColumnIndex("status"));


            View statusFlag = view.findViewById(R.id.status_flag);
            if (this.mStatusElement == null || !this.mStatusElement.isEnabled() || TextUtils.isEmpty(status)) {
                statusFlag.setVisibility(View.GONE);
            } else {
                statusOption = this.mStatusElement.getOption(status);
                if (statusOption != null) {
                    statusFlag.setVisibility(View.VISIBLE);
                    int statusColor = ViewCompat.MEASURED_SIZE_MASK;
                    try {
                        statusColor = statusOption.getColor();
                    } catch (Throwable e) {
                        GISLogger.log(e);
                    }
                    statusFlag.setBackgroundColor(statusColor);
                } else {
                    statusFlag.setVisibility(View.GONE);
                }
            }
            TextView statusLabel = (TextView) view.findViewById(R.id.record_status);
            if (statusLabel != null) {
                if (this.mStatusElement == null || !this.mStatusElement.isEnabled() || TextUtils.isEmpty(status)) {
                    statusLabel.setVisibility(View.GONE);
                } else {
                    statusOption = this.mStatusElement.getOption(status);
                    if (statusOption != null) {
                        statusLabel.setVisibility(View.VISIBLE);
                        statusLabel.setText(statusOption.getLabel());
                    } else {
                        statusLabel.setVisibility(View.GONE);
                    }
                }
            }
            boolean isDraft = CursorUtils.getBoolean(cursor, Record.COLUMN_DRAFT);
            Log.e("isDraft", isDraft + "  " + "status ");

            boolean isSynchronized = CursorUtils.getBoolean(cursor, Record.COLUMN_SYNCHRONIZED);
            final int attachments = cursor.getInt(cursor.getColumnIndex("unsynced_attachments"));
            View syncStatus = view.findViewById(R.id.sync_status);
            if (syncStatus != null) {
                if (isDraft) {
                    syncStatus.setBackgroundResource(R.drawable.ic_record_draft);
                } else if (!isSynchronized) {
                    syncStatus.setBackgroundResource(R.drawable.ic_record_unsynced);
                } else if (attachments > 0) {
                    syncStatus.setBackgroundResource(R.drawable.ic_record_attachments);
                    syncStatus.setOnClickListener(new OnClickListener () {
                        public void onClick(View v) {
                            Context context = v.getContext();
                            Toast.makeText(context, context.getResources().getQuantityString(R.plurals.unsynced_attachments, attachments, new Object[]{attachments}), Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    syncStatus.setBackgroundResource(R.drawable.ic_record_synced);
                }
            }
            long updatedAt = -1;
            int idxUpdatedAt = cursor.getColumnIndex(Record.COLUMN_UPDATED_AT);
            if (!cursor.isNull(idxUpdatedAt)) {
                updatedAt = cursor.getLong(idxUpdatedAt);
            }
            TextView datesLabel = (TextView) view.findViewById(R.id.record_timestamps);
            if (datesLabel != null) {
                datesLabel.setText(DateUtils.getRelativeTimeSpanString(this.mContext, updatedAt));
            }
            String title = cursor.getString(cursor.getColumnIndex("title"));
            TextView titleLabel = (TextView) view.findViewById(R.id.record_title);
            if (TextUtils.isEmpty(title)) {
                titleLabel.setText(R.string.no_title);
            } else {
                titleLabel.setText(title);
            }
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RecordCollectionViewListener) {
            this.mListener = (RecordCollectionViewListener) context;
            return;
        }
        throw new RuntimeException (context.toString() + " must implement " + RecordCollectionViewListener.class.getSimpleName());
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Bundle args = getArguments();
        if (args == null) {
            this.mRowLayoutID = R.layout.list_item_record;
            this.mCreateRecordsEnabled = true;
            return;
        }
  /*      Bundle args1 = getArguments();
        re_list= (ArrayList<Search>) args1.getSerializable("record_list");
        mNNSelectedRecordIDs.add(re_list.get(0).getId());

        Log.e("GetList2", re_list.get(0).getId());
       //  cursor12 = null;
        for (int i=0;i<re_list.size();i++) {
            String id = re_list.get(i).getId();
          //  Log.e("Value11", id + "  " + "status ");

            SQLiteDatabase db = GIS.getDatabase();
            String query11 = "SELECT " + "*" + " FROM `" + "Records" + "` WHERE _id = '" + 5 + "';";
            cursor12 = db.rawQuery(query11, null);

            if (cursor12 != null && cursor12.getCount() > 0) {
                while (cursor12.moveToNext()) {
                    long long_id = cursor12.getLong(cursor12.getColumnIndex("_id"));
                //    status = cursor12.getString(cursor12.getColumnIndex("status"));
                  // Log.e("Value12", long_id + "  " + "status ");

                    //   ArrayList<Search> record_list1=new ArrayList<>();
//                        Search ss=new Search();
//                        ss.setIds(long_id);
//                        ss.setStatus(status);
//                        //  ss.setCursor(cursor1);
//                        record_list1.add(ss);
                  //  view.setTag(long_id);
                    //status = cursor.getString(cursor.getColumnIndex("status"));
                }
            }
        }
        this.mRecordCursorAdapter = new RecordCursorAdapter(getActivity(), this.mRowLayoutID,cursor12);

         onRecordCursorLoaded(cursor12); */
        this.mRowLayoutID = args.getInt(RecordCollectionView.EXTRA_LIST_ROW_LAYOUT_ID, R.layout.list_item_record);
        this.mCreateRecordsEnabled = args.getBoolean(RecordCollectionView.EXTRA_CREATE_RECORDS_ENABLED, true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_record_list, container, false);
        this.mDisplayedCount = (TextView) view.findViewById(R.id.displayed_records_count);
        this.mFloatingActionButton = (FloatingActionButton) view.findViewById(R.id.floating_action_button);
        if (this.mCreateRecordsEnabled) {
            this.mFloatingActionButton.setOnClickListener(new C11081());
        } else {
            this.mFloatingActionButton.setVisibility(View.GONE);
        }
        return view;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView listView = getListView();
        if (this.mFloatingActionButton.getVisibility() == View.VISIBLE) {
            this.mFloatingActionButton.attachToListView(listView);
        }
        listView.setOnItemLongClickListener(new C11092());
        ContextualBottomSheetRecordMenu contextMenuDialog = (ContextualBottomSheetRecordMenu) getChildFragmentManager().findFragmentByTag(TAG_CONTEXTUAL_MENU_DIALOG);
        if (contextMenuDialog != null) {
            contextMenuDialog.setListener(this);
        }
    }

    public void onStart() {
        super.onStart();
        if (this.mForm != null) {
            reloadData();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.mRecordCursorAdapter != null) {
            this.mRecordCursorAdapter.changeCursor(null);
            this.mRecordCursorAdapter = null;
        }
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        this.mListener.onRecordSelected(id);
    }

    public void onDuplicate(long id, boolean location, boolean values) {
        this.mListener.onDuplicateRecord(id, location, values);
    }

    public void onShowLocation(int adapterPosition) {
        onShowRecordOnMap(adapterPosition);
    }

    public void onDrivingDirections(int adapterPosition) {
        onShowDrivingDirections(adapterPosition);
    }

    public void onGenerate(long id) {
        this.mListener.onGenerateReport(id);
    }

    public void onDelete(long id) {
        this.mListener.onDeleteRecord(id);
    }

    public void setFormID(long formID) {
        this.mForm = Form.getForm(formID);
        if (getActivity() != null && this.mForm != null) {
            reloadData();
        }
    }

    public void setFilter(RecordFilterOptions options) {
        this.mFilterOptions = options;
        if (getActivity() != null && this.mForm != null) {
            reloadData();
        }
    }

    public void setRecordConditions(RecordPredicate conditions) {
        this.mRecordConditions = conditions;
    }

    public void setSelectedRecordLinkIDs(ArrayList<String> selectedRecordLinkIDs) {
        this.mSelectedRecordIDs = selectedRecordLinkIDs;
    }

    public void setSearchQuery(String query) {
        this.mRecordSearchQuery = query;
        reloadData();
    }

    public void reloadData() {
        if (this.mCreateRecordsEnabled && this.mFloatingActionButton != null) {
            this.mFloatingActionButton.show();
        }
        reloadRecords();
    }

    public void onSyncFinished() {
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Context activity = getActivity();
        LatLngBounds bounds = null;
        if (PatronSettings.isMapBoundsFilterEnabled(activity) && !(activity instanceof SelectRecordLinkActivity)) {
            bounds = RecordMapFragment.getLastVisibleBounds();
        }
        String attachments = String.format("(SELECT COUNT(*) FROM %s WHERE %s.%s = %s.%s AND %s.%s = 0) AS %s", new Object[]{Attachment.TABLE_NAME, Attachment.TABLE_NAME, Attachment.COLUMN_RECORD_ID, Record.TABLE_NAME, Record.COLUMN_UNIQUE_ID, Attachment.TABLE_NAME, Attachment.COLUMN_UPLOADED, "unsynced_attachments"});
        Log.e("Table_Attach...", attachments);
       // return new RecordCursorLoader(activity, Account.getActiveAccount(), this.mForm, this.mFilterOptions, this.mRecordConditions, this.mSelectedRecordIDs, this.mRecordSearchQuery, bounds, getSortBy(), "_id", "title", "status", Record.COLUMN_UPDATED_AT, Record.COLUMN_SYNCHRONIZED, Record.COLUMN_LATITUDE, Record.COLUMN_LONGITUDE, Record.COLUMN_DRAFT).addColumns(attachments);


        return new RecordCursorLoader(activity, Account.getActiveAccount(), this.mForm, this.mFilterOptions, this.mRecordConditions, this.mSelectedRecordIDs, null, bounds, getSortBy(), "_id", "title", "status", Record.COLUMN_UPDATED_AT, Record.COLUMN_SYNCHRONIZED, Record.COLUMN_LATITUDE, Record.COLUMN_LONGITUDE, Record.COLUMN_DRAFT).addColumns(attachments);


    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        onRecordCursorLoaded(data);
    }

    public void onLoaderReset(Loader<Cursor> loader) {
    }

    private void reloadRecords() {
        if (getActivity() != null) {
            getLoaderManager().restartLoader(110, null, this);
        }
    }

    private RecordCursorAdapter getRecordCursorAdapter() {
        if (this.mRecordCursorAdapter != null) {
            return this.mRecordCursorAdapter;
        }
        this.mRecordCursorAdapter = new RecordCursorAdapter(getActivity(), this.mRowLayoutID,cursor12);
        return this.mRecordCursorAdapter;
    }

    private void onRecordCursorLoaded(Cursor cursor) {
        int count;
        if (cursor == null || this.mForm == null) {
            setListAdapter(null);
            count = 0;
        } else {
            StatusElement statusElement = this.mForm.getStatusElement();
            RecordCursorAdapter adapter = getRecordCursorAdapter();
            adapter.changeCursor(cursor);
            adapter.setStatusElement(statusElement);
            setListAdapter(adapter);
            count = cursor.getCount();
        }
       // this.mDisplayedCount.setText(getResources().getQuantityString(R.plurals.record_count, count, new Object[]{Integer.valueOf(count)}));
    }

    private void onShowRecordOnMap(int position) {
        if (this.mRecordCursorAdapter != null) {
            Cursor c = this.mRecordCursorAdapter.getCursor();
            if (c != null && c.moveToPosition(position)) {
                this.mListener.onShowRecordLocation(CursorUtils.getLong(c, "_id"), CursorUtils.getDouble(c, Record.COLUMN_LATITUDE), CursorUtils.getDouble(c, Record.COLUMN_LONGITUDE));
            }
        }
    }

    private void onShowDrivingDirections(int position) {
        if (this.mRecordCursorAdapter != null) {
            Cursor c = this.mRecordCursorAdapter.getCursor();
            if (c != null && c.moveToPosition(position)) {
                startActivity(IntentUtils.getDrivingDirections(getActivity(), CursorUtils.getDouble(c, Record.COLUMN_LATITUDE), CursorUtils.getDouble(c, Record.COLUMN_LONGITUDE)));
            }
        }
    }

    private String getSortBy() {
        String sortBy;
        String orderBy;
        Activity activity = getActivity();
        if (!(activity instanceof SelectRecordLinkActivity)) {
            switch (PatronSettings.getSortingMode(activity)) {
                case 1:
                    sortBy = "Records.status";
                    break;
                case 2:
                    sortBy = "Records.created_at";
                    break;
                case 3:
                    sortBy = "Records.updated_at";
                    break;
                default:
                    sortBy = "Records.title COLLATE NOCASE";
                    break;
            }
        }
        sortBy = "Records.title COLLATE NOCASE";
        if (PatronSettings.isSortAscendingEnabled(activity)) {
            orderBy = "ASC";
        } else {
            orderBy = "DESC";
        }
        return sortBy + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + orderBy;
    }
}
