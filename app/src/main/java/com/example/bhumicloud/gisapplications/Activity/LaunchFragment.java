package com.example.bhumicloud.gisapplications.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.Activity.LoginFragment.LoginButtonListener;

public class LaunchFragment extends Fragment {
    private LaunchFragmentListener mListener;
    private Button mLoginButton;

    public interface LaunchFragmentListener extends LoginButtonListener {

    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LaunchFragmentListener) {
            this.mListener = (LaunchFragmentListener) context;
            return;
        }
        throw new RuntimeException (context.toString() + " must implement " + LaunchFragmentListener.class.getSimpleName());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_launch, container, false);
        this.mLoginButton = (Button) view.findViewById(R.id.login_btn);
        this.mLoginButton.setOnClickListener ( new OnClickListener () {
            @Override
            public void onClick(View v) {
                LaunchFragment.this.mListener.onLoginButtonClick();
            }
        } );
        return view;
    }

    public void enableButtons() {
        this.mLoginButton.setEnabled(true);
    }
}
