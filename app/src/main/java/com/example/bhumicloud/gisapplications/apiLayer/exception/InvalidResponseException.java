package com.example.bhumicloud.gisapplications.apiLayer.exception;

import android.content.Context;

import com.example.bhumicloud.gisapplications.R;

import java.io.IOException;
import java.net.HttpURLConnection;

import retrofit2.Response;

public class InvalidResponseException extends GISServiceException {
    public InvalidResponseException(HttpURLConnection connection) throws IOException {
        this(connection.getResponseCode(), connection.getResponseMessage());
    }

    public InvalidResponseException(Response response) {
        this(response.code(), response.message());
    }

    public InvalidResponseException(int status, String message) {
        super( String.format("(%d) %s", new Object[]{Integer.valueOf(status), message}));
    }

    public String getUserFriendlyMessage(Context context) {
        return context.getString(R.string.invalid_response_message, new Object[]{getMessage()});
    }
}
