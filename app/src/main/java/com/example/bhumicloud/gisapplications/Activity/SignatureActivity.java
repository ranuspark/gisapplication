package com.example.bhumicloud.gisapplications.Activity;

import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.field.attachment.signature.SignatureElement;
import com.example.bhumicloud.gisapplications.util.FileUtils;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.View.CanvasView;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;


public class SignatureActivity extends GISActivity {
    public static final String EXTRA_CLEARED = "GIS:extra:cleared";
    public static final String EXTRA_NOTES = "GIS:extra:notes";
    public static final String EXTRA_TIMESTAMP = "GIS:extra:timestamp";
    public static final String EXTRA_TITLE = "GIS:extra:title";
    public static final String EXTRA_URI = "GIS:extra:uri";
    private CanvasView mCanvasView;
    private Uri mStorageUri;

    class C11401 implements OnClickListener {
        C11401() {
        }

        public void onClick(View v) {
            SignatureActivity.this.onClearButtonPressed();
        }
    }

    class C11412 implements OnClickListener {
        C11412() {
        }

        public void onClick(View v) {
            SignatureActivity.this.onDoneButtonPressed();
        }
    }

    private static class BitmapSaveTask extends AsyncTask<Void, Void, File> {
        private final SignatureActivity mActivity;
        private final Bitmap mBitmap;
        private Exception mException;
        private ProgressDialog mProgressDialog;
        private final Date mStamp = new Date();
        private final Uri mUri;

        public BitmapSaveTask(SignatureActivity activity, Bitmap bitmap, Uri uri) {
            this.mActivity = activity;
            this.mUri = uri;
            this.mBitmap = bitmap;
            this.mProgressDialog = new ProgressDialog(this.mActivity);
            this.mProgressDialog.setTitle(R.string.signature_saving_dialog);
            this.mProgressDialog.setIndeterminate(true);
            this.mProgressDialog.setCancelable(false);
        }

        protected void onPreExecute() {
            super.onPreExecute();
            this.mProgressDialog.show();
        }

        protected File doInBackground(Void... params) {
            File output = new File(this.mUri.getPath());
            GISLogger.log("SignatureActivity", "Writing signature to " + output.getAbsolutePath());
            try {
                FileOutputStream writer = new FileOutputStream(output);
                this.mBitmap.compress(CompressFormat.PNG, 100, writer);
                writer.flush();
                writer.close();
                return output;
            } catch (Throwable e) {
                GISLogger.log(e);
                return null;
            }
        }

        protected void onPostExecute(File result) {
            super.onPostExecute(result);
            this.mProgressDialog.dismiss();
            if (result != null) {
                Intent i = new Intent();
                i.putExtra(SignatureActivity.EXTRA_TIMESTAMP, this.mStamp);
                this.mActivity.setResult(-1, i);
                this.mActivity.finish();
            } else if (this.mException != null) {
                Builder b = new Builder(this.mActivity);
                b.setTitle(R.string.error);
                b.setMessage(this.mException.getLocalizedMessage());
                b.show();
            }
        }
    }

    public static Intent getIntent(Context context, SignatureElement element) {
        Intent intent = new Intent(context, SignatureActivity.class);
        if (element != null) {
            intent.putExtra(EXTRA_TITLE, element.getLabel());
            intent.putExtra(EXTRA_NOTES, element.getAgreementText());
        }
        return intent;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_signature);
        Button doneButton = (Button) findViewById(R.id.done_btn);
        Button clearButton = (Button) findViewById(R.id.clear_btn);
        TextView notesLabel = (TextView) findViewById(R.id.notes);
        this.mCanvasView = (CanvasView) findViewById(R.id.canvas);
        clearButton.setOnClickListener(new C11401());
        doneButton.setOnClickListener(new C11412());
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String notes = extras.getString(EXTRA_NOTES);
            if (!TextUtils.isEmpty(notes)) {
                notesLabel.setText(notes);
            }
            this.mStorageUri = (Uri) extras.getParcelable(EXTRA_URI);
        }
    }

    private void onClearButtonPressed() {
        this.mCanvasView.clear();
    }

    private void onDoneButtonPressed() {
        if (this.mCanvasView.isBlank()) {
            Intent i = new Intent();
            i.putExtra(EXTRA_CLEARED, true);
            setResult(-1, i);
            finish();
        } else if (FileUtils.isExternalStorageAvailable()) {
            Bitmap bitmap = this.mCanvasView.getBitmap();
            if (bitmap != null) {
                new BitmapSaveTask(this, bitmap, this.mStorageUri).execute(new Void[0]);
            }
        } else {
            Builder b = new Builder(this);
            b.setTitle(R.string.error);
            b.setMessage(R.string.signature_external_storage_error);
            b.show();
        }
    }
}
