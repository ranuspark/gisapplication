package com.example.bhumicloud.gisapplications.model.field.attachment.photo;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaValue;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.List;
import java.util.Map;

public class PhotoValue extends MediaValue {
    private final PhotoElement mElement;

    public PhotoValue(PhotoElement element) {
        this.mElement = element;
    }

    public PhotoValue(PhotoElement element, List json) {
        this(element);
        if (json != null && !json.isEmpty()) {
            for (int i = 0; i < json.size(); i++) {
                Map photoJSON = JSONUtils.getHashMap(json, i);
                if (photoJSON != null) {
                    this.mMediaItems.add(new PhotoItemValue(photoJSON));
                }
            }
        }
    }

    protected int getDisplayValueResource() {
        return R.plurals.photo;
    }

    public PhotoElement getElement() {
        return this.mElement;
    }

    public void addPhoto(Photo photo) {
        if (photo != null) {
            this.mMediaItems.add(new PhotoItemValue((Attachment) photo));
        }
    }
}
