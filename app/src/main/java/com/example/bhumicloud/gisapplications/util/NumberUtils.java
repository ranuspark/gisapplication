package com.example.bhumicloud.gisapplications.util;

import android.content.res.Resources;
import android.text.TextUtils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Currency;
import java.util.Locale;

public class NumberUtils {
    private static final DecimalFormat COORDINATE_FORMAT = new DecimalFormat("0.000000");
    private static final DecimalFormat REGULAR_FORMAT = new DecimalFormat("#.##");
    private static final DecimalFormat RTL_COORDINATE_FORMAT = new DecimalFormat("0.000000;0.000000-");
    private static final DecimalFormat RTL_REGULAR_FORMAT = new DecimalFormat("#.##;#.##-");
    private static NumberFormat sLocalizedDecimalFormatter;
    private static NumberFormat sLocalizedIntegerFormatter;
    private static DecimalFormat sMachineDecimalFormatter;

    private static DecimalFormat getMachineDecimalFormatter() {
        if (sMachineDecimalFormatter == null) {
            sMachineDecimalFormatter = new DecimalFormat();
            sMachineDecimalFormatter.setDecimalSeparatorAlwaysShown(false);
            sMachineDecimalFormatter.setGroupingUsed(false);
            sMachineDecimalFormatter.setMinimumIntegerDigits(1);
            sMachineDecimalFormatter.setMinimumFractionDigits(0);
            sMachineDecimalFormatter.setMaximumFractionDigits(20);
            sMachineDecimalFormatter.setNegativePrefix("-");
            sMachineDecimalFormatter.setParseIntegerOnly(false);
            sMachineDecimalFormatter.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
        }
        return sMachineDecimalFormatter;
    }

    private static NumberFormat getLocalizedDecimalFormatter(boolean allowFloats) {
        if (allowFloats) {
            if (sLocalizedDecimalFormatter == null) {
                sLocalizedDecimalFormatter = NumberFormat.getNumberInstance();
                sLocalizedDecimalFormatter.setGroupingUsed(false);
                sLocalizedDecimalFormatter.setMaximumFractionDigits(20);
            }
            return sLocalizedDecimalFormatter;
        }
        if (sLocalizedIntegerFormatter == null) {
            sLocalizedIntegerFormatter = NumberFormat.getIntegerInstance();
            sLocalizedIntegerFormatter.setGroupingUsed(false);
            sLocalizedIntegerFormatter.setMaximumFractionDigits(20);
        }
        return sLocalizedIntegerFormatter;
    }

    private static Number parseNumberStrict(NumberFormat format, String numberString) {
        if (TextUtils.isEmpty(numberString)) {
            return null;
        }
        ParsePosition pos = new ParsePosition(0);
        Number result = format.parse(numberString, pos);
        if (pos.getIndex() == numberString.length() && pos.getErrorIndex() == -1) {
            return result;
        }
        return null;
    }

    private static String getLocalizedGroupingSeparator() {
        return String.valueOf(DecimalFormatSymbols.getInstance().getGroupingSeparator());
    }

    private static String formatLocalizedCurrencyStringFromNumberString(String value, String currencyCode, Integer minimumIntegerDigits, Integer minimumFractionDigits, Integer maximumFractionDigits, boolean useGroupingSeparator) {
        Number number = parseNumberFromMachineString(value);
        if (number == null) {
            return value;
        }
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        formatter.setGroupingUsed(useGroupingSeparator);
        if (minimumIntegerDigits != null) {
            formatter.setMinimumIntegerDigits(minimumIntegerDigits.intValue());
        }
        if (minimumFractionDigits != null) {
            formatter.setMinimumFractionDigits(minimumFractionDigits.intValue());
        }
        if (maximumFractionDigits != null) {
            formatter.setMaximumFractionDigits(maximumFractionDigits.intValue());
        }
        if (currencyCode != null) {
            formatter.setCurrency(Currency.getInstance(currencyCode));
        }
        return formatter.format(number.doubleValue());
    }

    public static String formatMachineStringFromNumber(double number) {
        return getMachineDecimalFormatter().format(number);
    }

    public static String formatLocalizedStringFromMachineString(String number, boolean allowFloats) {
        Number value = parseNumberFromMachineString(number);
        return value == null ? number : getLocalizedDecimalFormatter(allowFloats).format(value.doubleValue());
    }

    public static String formatMachineStringFromLocalizedString(String number) {
        Number value = parseNumberFromLocalizedString(number);
        return value == null ? number : formatMachineStringFromNumber(value.doubleValue());
    }

    public static String formatMachineStringFromLegacyString(String number) {
        if (number == null) {
            return null;
        }
        return number.replace(",", ".");
    }

    public static Number parseNumberFromMachineString(String numberString) {
        Number result = parseNumberStrict(getMachineDecimalFormatter(), numberString);
        if (result == null) {
            return parseNumberFromLocalizedString(numberString);
        }
        return result;
    }

    public static Number parseNumberFromLocalizedString(String numberString) {
        if (TextUtils.isEmpty(numberString)) {
            return null;
        }
        return parseNumberStrict(getLocalizedDecimalFormatter(true), numberString.replace(getLocalizedGroupingSeparator(), ""));
    }

    public static String formatLocalizedCurrencyStringFromNumberString(String value, String currencyCode) {
        return formatLocalizedCurrencyStringFromNumberString(value, currencyCode, null, null, null, true);
    }

    public static DecimalFormat getRegularFormat(Resources resources) {
        return DeviceInfo.isRightToLeft(resources) ? RTL_REGULAR_FORMAT : REGULAR_FORMAT;
    }

    public static DecimalFormat getCoordinateFormat(Resources resources) {
        return DeviceInfo.isRightToLeft(resources) ? RTL_COORDINATE_FORMAT : COORDINATE_FORMAT;
    }
}
