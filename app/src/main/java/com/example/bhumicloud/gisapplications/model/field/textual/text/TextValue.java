package com.example.bhumicloud.gisapplications.model.field.textual.text;

import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.example.bhumicloud.gisapplications.model.field.textual.TextualValue;
import com.example.bhumicloud.gisapplications.util.NumberUtils;

public class TextValue extends TextualValue {
    private final TextElement mElement;

    public TextValue(TextElement element, String aValue) {
        this.mElement = element;
        if (element.isNumeric()) {
            this.mValue = NumberUtils.formatMachineStringFromLegacyString(aValue);
        } else {
            this.mValue = aValue;
        }
    }

    public String getDisplayValue() {
        if (!getElement().isNumeric() || isEmpty()) {
            return this.mValue;
        }
        return NumberUtils.formatLocalizedStringFromMachineString(this.mValue, getElement().isDecimal());
    }

    public String getSearchableValue() {
        return getDisplayValue();
    }

    public Object getColumnValue() {
        return this.mElement.isNumeric() ? getNumericValue() : super.getColumnValue();
    }

    public TextElement getElement() {
        return this.mElement;
    }

    public Number getNumericValue() {
        try {
            return Double.valueOf(this.mValue);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public boolean isNumeric() {
        if (isEmpty() || NumberUtils.parseNumberFromMachineString(this.mValue) != null) {
            return true;
        }
        return false;
    }
}
