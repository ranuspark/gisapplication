CREATE TABLE IF NOT EXISTS ChoiceLists (
    _id INTEGER PRIMARY KEY AUTOINCREMENT,
    remote_id TEXT,
    account_id INTEGER,
    name TEXT,
    description TEXT,
    choices TEXT
);
CREATE INDEX IF NOT EXISTS choice_lists_account_id_idx ON ChoiceLists (account_id);
CREATE INDEX IF NOT EXISTS choice_lists_account_id_remote_id_idx ON ChoiceLists (account_id, remote_id);