package com.example.bhumicloud.gisapplications.model.field;

import com.example.bhumicloud.gisapplications.model.field.address.AddressElement;
import com.example.bhumicloud.gisapplications.model.field.attachment.audio.AudioElement;
import com.example.bhumicloud.gisapplications.model.field.attachment.photo.PhotoElement;
import com.example.bhumicloud.gisapplications.model.field.attachment.signature.SignatureElement;
import com.example.bhumicloud.gisapplications.model.field.attachment.video.VideoElement;
import com.example.bhumicloud.gisapplications.model.field.choice.ChoiceElement;
import com.example.bhumicloud.gisapplications.model.field.classification.ClassificationElement;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkElement;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableElement;
import com.example.bhumicloud.gisapplications.model.field.temporal.date.DateElement;
import com.example.bhumicloud.gisapplications.model.field.temporal.time.TimeElement;
import com.example.bhumicloud.gisapplications.model.field.textual.barcode.BarcodeElement;
import com.example.bhumicloud.gisapplications.model.field.textual.calculated.CalculatedElement;
import com.example.bhumicloud.gisapplications.model.field.textual.hyperlink.HyperlinkElement;
import com.example.bhumicloud.gisapplications.model.field.textual.text.TextElement;
import com.example.bhumicloud.gisapplications.model.field.textual.yesno.YesNoElement;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.Map;

public class ElementFactory {
    public static Element getInstance(Element parent, Map json) {
        String type = JSONUtils.getString(json, "type");
        if (type == null) {
            throw new RuntimeException("Can't create a new Element without a type!");
        } else if (Element.TYPE_SECTION.equals(type)) {
            return new SectionElement(parent, json);
        } else {
            if (Element.TYPE_TEXT.equals(type)) {
                return new TextElement(parent, json);
            }
            if (Element.TYPE_CLASSIFICATION.equals(type)) {
                return new ClassificationElement(parent, json);
            }
            if (Element.TYPE_CHOICE.equals(type)) {
                return new ChoiceElement(parent, json);
            }
            if (Element.TYPE_PHOTO.equals(type)) {
                return new PhotoElement(parent, json);
            }
            if (Element.TYPE_DATE.equals(type)) {
                return new DateElement(parent, json);
            }
            if (Element.TYPE_TIME.equals(type)) {
                return new TimeElement(parent, json);
            }
            if (Element.TYPE_DATE_TIME.equals(type)) {
                return new DateElement(parent, json);
            }
            if (Element.TYPE_LABEL.equals(type)) {
                return new LabelElement(parent, json);
            }
            if (Element.TYPE_ADDRESS.equals(type)) {
                return new AddressElement(parent, json);
            }
            if (Element.TYPE_SIGNATURE.equals(type)) {
                return new SignatureElement(parent, json);
            }
            if (Element.TYPE_REPEATABLE.equals(type)) {
                return new RepeatableElement(parent, json);
            }
            if (Element.TYPE_YES_NO.equals(type)) {
                return new YesNoElement(parent, json);
            }
            if (Element.TYPE_VIDEO.equals(type)) {
                return new VideoElement(parent, json);
            }
            if (Element.TYPE_AUDIO.equals(type)) {
                return new AudioElement(parent, json);
            }
            if (Element.TYPE_HYPERLINK.equals(type)) {
                return new HyperlinkElement(parent, json);
            }
            if (Element.TYPE_BARCODE.equals(type)) {
                return new BarcodeElement(parent, json);
            }
            if (Element.TYPE_CALCULATION.equals(type)) {
                return new CalculatedElement(parent, json);
            }
            if (Element.TYPE_RECORD_LINK.equals(type)) {
                return new RecordLinkElement(parent, json);
            }
            return null;
        }
    }
}
