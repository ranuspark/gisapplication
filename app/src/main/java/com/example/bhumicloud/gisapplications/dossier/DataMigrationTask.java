package com.example.bhumicloud.gisapplications.dossier;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.RecordValues;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.javascript.FormSchema;
import com.example.bhumicloud.gisapplications.model.layer.MapLayer;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.example.bhumicloud.gisapplications.util.CursorUtils;
import com.example.bhumicloud.gisapplications.util.FileUtils;
import com.example.bhumicloud.gisapplications.util.GISLogger;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class DataMigrationTask extends AsyncTask<Context, String, Void> {
    private static final String DEBUG_TAG = DataMigrationTask.class.getSimpleName();
    private static DataMigrationTask sSharedInstance;
    private Callback mCallback;
    private String mRecentMessage;

    public interface Callback {
        void onMigrationFinished();

        void onMigrationProgress(String str);
    }

    public static synchronized DataMigrationTask getInstance() {
        DataMigrationTask dataMigrationTask;
        synchronized (DataMigrationTask.class) {
            if (sSharedInstance != null) {
                dataMigrationTask = sSharedInstance;
            } else {
                sSharedInstance = new DataMigrationTask();
                dataMigrationTask = sSharedInstance;
            }
        }
        return dataMigrationTask;
    }

    private DataMigrationTask() {
    }

    public synchronized void setCallback(Callback callback) {
        this.mCallback = callback;
    }

    public synchronized void start(Context context) {
        if (getStatus() == Status.PENDING) {
            executeOnExecutor(THREAD_POOL_EXECUTOR, new Context[]{context});
        }
    }

    public boolean isRunning() {
        return getStatus() == Status.RUNNING;
    }

    public boolean isPending() {
        return getStatus() == Status.PENDING;
    }

    public String getRecentMessage() {
        return this.mRecentMessage;
    }

    protected Void doInBackground(Context... args) {
        GISLogger.log("DataMigrationTask", "Migrating Databases...");
        notifyProgress("Migrating Search Index");
        Context context = args[0];
        SearchIndex.getInstance(context).getWritableDatabase();
        notifyProgress(context.getResources().getString(R.string.upgrading_database));
        PersistentStore store = PersistentStore.getInstance(context);
        store.getWritableDatabase();
        if (!MigrationHistory._001_hasRecordsBeenAddedToSearchIndex(context)) {
            addExistingRecordsToSearchIndex(store.getWritableDatabase());
            MigrationHistory._001_setRecordsAddedToSearchIndex(context, true);
        }
        if (!MigrationHistory._002_hasConvertedAttachmentLocalURIs(context)) {
            convertAttachmentURIs(store.getWritableDatabase());
            MigrationHistory._002_setConvertedAttachmentLocalURIs(context, true);
        }
        if (!MigrationHistory._003_hasMigratedMapLayerLocalPaths(context)) {
            migrateMapLayerLocalPaths(context, store.getWritableDatabase());
            MigrationHistory._003_setMigratedMapLayerLocalPaths(context, true);
        }
        if (!MigrationHistory._004_hasMigratedAttachmentThumbnailPaths(context)) {
            migrateAttachmentThumbnailPaths(context, store.getWritableDatabase());
            MigrationHistory._004_setMigratedAttachmentThumbnailPaths(context, true);
        }
        if (!MigrationHistory._005_hasCreatedAndSavedIntoFormSchemaTables(context)) {
            createAndSaveIntoFormSchemaTables(context, store.getWritableDatabase());
            MigrationHistory._005_setCreatedAndSavedIntoFormSchemaTables(context, true);
        }
        if (!MigrationHistory._006_hasCopiedApplicationSharedPrefsToPerUser(context)) {
            copyApplicationSharedPrefsToPerUser(context, store.getWritableDatabase());
            MigrationHistory._006_setCopiedApplicationSharedPrefsToPerUser(context, true);
        }
        return null;
    }

    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        if (this.mCallback != null && values != null && values.length > 0) {
            this.mCallback.onMigrationProgress(values[0]);
        }
    }

    protected void onCancelled() {
        super.onCancelled();
        sSharedInstance = null;
    }

    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (this.mCallback != null) {
            this.mCallback.onMigrationFinished();
        }
        sSharedInstance = null;
    }

    private void addExistingRecordsToSearchIndex(SQLiteDatabase db) {
        GISLogger.log(DEBUG_TAG, "Adding records to values index...");
        notifyProgress("Indexing Records");
        String predicate = "form_id = ?";
        SQLiteDatabase sQLiteDatabase = db;
        Cursor forms = sQLiteDatabase.query(Form.TABLE_NAME, new String[]{"_id"}, null, null, null, null, null);
        if (forms != null) {
            forms.moveToFirst();
            while (!forms.isAfterLast()) {
                SQLiteDatabase sQLiteDatabase2 = db;
                Cursor records = sQLiteDatabase2.query(Record.TABLE_NAME, null, predicate, new String[]{String.valueOf(forms.getLong(0))}, null, null, null);
                if (records != null) {
                    records.moveToFirst();
                    while (!records.isAfterLast()) {
                        notifyProgress("Indexing Records", (long) (records.getPosition() + 1), (long) records.getCount());
                        new Record(records).save(false);
                        records.moveToNext();
                    }
                    records.close();
                }
                forms.moveToNext();
            }
            forms.close();
        }
    }

    private void convertAttachmentURIs(SQLiteDatabase db) {
        GISLogger.log(DEBUG_TAG, "Converting attachment local URIs...");
        notifyProgress("Converting Attachments");
        SQLiteDatabase sQLiteDatabase = db;
        Cursor c = sQLiteDatabase.query(Attachment.TABLE_NAME, new String[]{"_id", Attachment.COLUMN_LOCAL_URI}, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                String uriString = c.getString(1);
                if (!TextUtils.isEmpty(uriString)) {
                    String path = Uri.parse(uriString).getPath();
                    if (!TextUtils.isEmpty(path)) {
                        ContentValues cv = new ContentValues ();
                        cv.put(Attachment.COLUMN_LOCAL_URI, path);
                        db.update(Attachment.TABLE_NAME, cv, "_id = ?", new String[]{String.valueOf(c.getLong(0))});
                    }
                }
                c.moveToNext();
            }
            c.close();
        }
    }

    private void migrateMapLayerLocalPaths(Context context, SQLiteDatabase db) {
        GISLogger.log(DEBUG_TAG, "Migrating map layer local paths...");
        notifyProgress("Migrating Map Layer Paths");
        Cursor c = db.query(MapLayer.TABLE_NAME, null, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                String remote = CursorUtils.getString(c, "remote_id");
                if (!TextUtils.isEmpty(remote)) {
                    File file = MapLayer.getTypedInstance(c).generateStorageLocation(context, remote);
                    if (file != null) {
                        ContentValues cv = new ContentValues ();
                        cv.put(MapLayer.COLUMN_LOCAL_URL, file.getAbsolutePath());
                        db.update(MapLayer.TABLE_NAME, cv, "_id = ?", new String[]{String.valueOf(CursorUtils.getLong(c, "_id"))});
                    }
                }
                c.moveToNext();
            }
            c.close();
        }
    }

    private void migrateAttachmentThumbnailPaths(Context context, SQLiteDatabase db) {
        GISLogger.log(DEBUG_TAG, "Migrating attachment thumbnail paths...");
        notifyProgress("Migrating Attachment Thumbnail Paths");
        Cursor c = db.query(Attachment.TABLE_NAME, null, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                String filepath = CursorUtils.getString(c, Attachment.COLUMN_PATH_ONE);
                if (!TextUtils.isEmpty(filepath)) {
                    String thumbnailPath = FileUtils.generateThumbnailPath(context, filepath);
                    ContentValues cv = new ContentValues ();
                    if (thumbnailPath != null) {
                        cv.put(Attachment.COLUMN_THUMBNAIL_PATH, thumbnailPath);
                        db.update(Attachment.TABLE_NAME, cv, "_id = ?", new String[]{String.valueOf(CursorUtils.getLong(c, "_id"))});
                    }
                }
                c.moveToNext();
            }
            c.close();
        }
    }

    private void createAndSaveIntoFormSchemaTables(Context context, SQLiteDatabase db) {
        GISLogger.log(DEBUG_TAG, "Copying into and creating form schema tables...");
        notifyProgress("Copying Data Into New Schema Tables");
        Account originalActiveAccount = Account.getActiveAccount();
        FormSchema formSchema = new FormSchema(context, db);
        ArrayList<Account> accounts = new ArrayList ();
        Cursor cursor = db.query(Account.TABLE_NAME, null, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            accounts.add(new Account(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        Iterator it = accounts.iterator();
        while (it.hasNext()) {
            Account account = (Account) it.next();
            Account.setActiveAccount(account);
            Iterator it2 = Form.getForms(account).iterator();
            while (it2.hasNext()) {
                Form form = (Form) it2.next();
                notifyProgress( String.format("Indexing %s...", new Object[]{form.getName()}));
                try {
                    db.beginTransactionNonExclusive();
                    formSchema.deleteTablesAssociatedWithFormAndAccount(form.toJSON(), form.getRowID(), account);
                    formSchema.updateSchemaWithOldForm(null, form.toJSON(), form.getRowID(), account);
                    db.setTransactionSuccessful();
                    int count = 0;
                    db.beginTransactionNonExclusive();
                    ArrayList<Record> records;
                    do {
                        records = Record.getRecords(db, form.getRowID(), 500, count);
                        Iterator it3 = records.iterator();
                        while (it3.hasNext()) {
                            Record record = (Record) it3.next();
                            record.setForm(form);
                            record.setAccount(account);
                            RecordValues.updateForRecord(db, record, false);
                            count++;
                            if (count % 50 == 0) {
                                notifyProgress( String.format("Indexing %s... (%d)", new Object[]{form.getName(), Integer.valueOf(count)}));
                            }
                        }
                        try {
                        } catch (Throwable th) {
                            db.endTransaction();
                        }
                    } while (records.size() == 500);
                    db.setTransactionSuccessful();
                    db.endTransaction();
                } finally {
                    db.endTransaction();
                }
            }
        }
        formSchema.release();
        Account.setActiveAccount(originalActiveAccount);
    }

    private void copyApplicationSharedPrefsToPerUser(Context context, SQLiteDatabase db) {
        SharedPreferences applicationPrefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        ArrayList<Account> accounts = new ArrayList ();
        Cursor cursor = db.query(Account.TABLE_NAME, null, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            accounts.add(new Account(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        Map<String, ?> allAppPrefs = applicationPrefs.getAll();
        Iterator it = accounts.iterator();
        while (it.hasNext()) {
            Account account = (Account) it.next();
            Editor accountPrefsEditor = PatronSettings.getAccountPreferences(context, account).edit();
            for (Entry<String, ?> entry : allAppPrefs.entrySet()) {
                if (((String) entry.getKey()).equals("com.example.bhumicloud.gisapplications.preferences." + account.getRowID() + ".active_project")) {
                    accountPrefsEditor.putLong(PatronSettings.KEY_ACCOUNT_ACTIVE_PROJECT, ((Long) entry.getValue()).longValue());
                } else if (((String) entry.getKey()).equals("com.example.bhumicloud.gisapplications.preferences.map_camera_position")) {
                    accountPrefsEditor.putString(PatronSettings.KEY_ACCOUNT_MAP_CAMERA_POSITION, (String) entry.getValue());
                } else if (((String) entry.getKey()).equals("com.example.bhumicloud.gisapplications.preferences.active_view_mode")) {
                    accountPrefsEditor.putInt(PatronSettings.KEY_ACCOUNT_ACTIVE_VIEW_MODE, ((Integer) entry.getValue()).intValue());
                } else if (((String) entry.getKey()).equals("com.example.bhumicloud.gisapplications.preferences.active_form")) {
                    accountPrefsEditor.putLong(PatronSettings.KEY_ACCOUNT_ACTIVE_FORM, ((Long) entry.getValue()).longValue());
                } else if (((String) entry.getKey()).equals("GIS:sync:record_download_dates:" + account.getRowID())) {
                    accountPrefsEditor.putString(PatronSettings.KEY_SYNC_LAST_RECORD_DOWNLOAD_DATES, (String) entry.getValue());
                } else if (((String) entry.getKey()).equals("GIS:map:layers:" + account.getRowID())) {
                    accountPrefsEditor.putString(PatronSettings.KEY_MAP_LAYERS, (String) entry.getValue());
                } else if (((String) entry.getKey()).equals("GIS:map:basemap")) {
                    accountPrefsEditor.putInt(PatronSettings.KEY_MAP_BASEMAP, ((Integer) entry.getValue()).intValue());
                } else if (((String) entry.getKey()).equals("GIS:list:map_filter")) {
                    accountPrefsEditor.putBoolean(PatronSettings.KEY_LIST_MAP_FILTER, ((Boolean) entry.getValue()).booleanValue());
                } else if (((String) entry.getKey()).equals("GIS:list:sort_by")) {
                    accountPrefsEditor.putInt(PatronSettings.KEY_LIST_SORT_BY, ((Integer) entry.getValue()).intValue());
                } else if (((String) entry.getKey()).equals("GIS:list:ascending")) {
                    accountPrefsEditor.putBoolean(PatronSettings.KEY_LIST_ASCENDING, ((Boolean) entry.getValue()).booleanValue());
                } else if (((String) entry.getKey()).equals("GIS:sync:auto_sync_post_launch")) {
                    accountPrefsEditor.putBoolean(PatronSettings.KEY_SYNC_AUTO_POST_LAUNCH, ((Boolean) entry.getValue()).booleanValue());
                } else if (((String) entry.getKey()).equals("GIS:sync:record_edits")) {
                    accountPrefsEditor.putBoolean(PatronSettings.KEY_SYNC_AUTO_POST_RECORD_EDITS, ((Boolean) entry.getValue()).booleanValue());
                } else if (((String) entry.getKey()).equals("GIS:sync:photos")) {
                    accountPrefsEditor.putInt(PatronSettings.KEY_SYNC_PHOTOS, ((Integer) entry.getValue()).intValue());
                } else if (((String) entry.getKey()).equals("GIS:sync:videos")) {
                    accountPrefsEditor.putInt(PatronSettings.KEY_SYNC_VIDEOS, ((Integer) entry.getValue()).intValue());
                } else if (((String) entry.getKey()).equals("GIS:sync:audio_files")) {
                    accountPrefsEditor.putInt(PatronSettings.KEY_SYNC_AUDIO_FILES, ((Integer) entry.getValue()).intValue());
                } else if (((String) entry.getKey()).equals("GIS:sync:signatures")) {
                    accountPrefsEditor.putInt(PatronSettings.KEY_SYNC_SIGNATURES, ((Integer) entry.getValue()).intValue());
                } else if (((String) entry.getKey()).equals("GIS:photos:quality")) {
                    accountPrefsEditor.putInt(PatronSettings.KEY_PHOTOS_QUALITY, ((Integer) entry.getValue()).intValue());
                } else if (((String) entry.getKey()).equals("GIS:photos:save_to_gallery")) {
                    accountPrefsEditor.putBoolean(PatronSettings.KEY_PHOTOS_SAVE_TO_GALLERY, ((Boolean) entry.getValue()).booleanValue());
                } else if (((String) entry.getKey()).equals("GIS:videos:quality")) {
                    accountPrefsEditor.putInt(PatronSettings.KEY_VIDEOS_QUALITY, ((Integer) entry.getValue()).intValue());
                } else if (((String) entry.getKey()).equals("GIS:videos:save_to_gallery")) {
                    accountPrefsEditor.putBoolean(PatronSettings.KEY_VIDEOS_SAVE_TO_GALLERY, ((Boolean) entry.getValue()).booleanValue());
                } else if (((String) entry.getKey()).equals("GIS:locationdebugger:units:" + account.getRowID())) {
                    accountPrefsEditor.putInt(PatronSettings.KEY_LOCATION_DEBUGGER_UNITS, ((Integer) entry.getValue()).intValue());
                }
            }
            accountPrefsEditor.apply();
        }
        Editor applicationPrefsEditor = applicationPrefs.edit();
        applicationPrefsEditor.clear();
        applicationPrefsEditor.apply();
        if (allAppPrefs.containsKey("GIS:data:is_first_launch")) {
            applicationPrefsEditor.putLong(PatronSettings.KEY_APP_ACTIVE_ACCOUNT, -1);
        }
        if (allAppPrefs.containsKey("com.example.bhumicloud.gisapplications.preferences.active_account")) {
            applicationPrefsEditor.putLong(PatronSettings.KEY_APP_ACTIVE_ACCOUNT, ((Long) allAppPrefs.get("com.example.bhumicloud.gisapplications.preferences.active_account")).longValue());
        }
        if (allAppPrefs.containsKey("api_hostname")) {
            applicationPrefsEditor.putString(PatronSettings.KEY_API_HOSTNAME, (String) allAppPrefs.get("api_hostname"));
        }
        if (allAppPrefs.containsKey("api_https")) {
            applicationPrefsEditor.putBoolean(PatronSettings.KEY_API_HTTPS, ((Boolean) allAppPrefs.get("api_https")).booleanValue());
        }
        applicationPrefsEditor.apply();
    }

    private void notifyProgress(String message) {
        notifyProgress(message, -1, -1);
    }

    private void notifyProgress(String message, long current, long maximum) {
        StringBuilder sb = new StringBuilder (message);
        if (current >= 0 && maximum >= 0) {
            sb.append( String.format(" (%d/%d)", new Object[]{Long.valueOf(current), Long.valueOf(maximum)}));
        }
        this.mRecentMessage = sb.toString();
        publishProgress(new String[]{this.mRecentMessage});
    }
}
