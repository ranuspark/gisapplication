package com.example.bhumicloud.gisapplications.Activity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.AdapterDataObserver;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.apiLayer.Client;
import com.example.bhumicloud.gisapplications.Activity.GISActivity.OnBackPressedListener;
import com.example.bhumicloud.gisapplications.model.appgallery.App;
import com.example.bhumicloud.gisapplications.model.appgallery.Category;
import com.example.bhumicloud.gisapplications.model.appgallery.Item;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import com.example.bhumicloud.gisapplications.util.KeyboardUtils;
import com.example.bhumicloud.gisapplications.widget.OnItemClickListener;
import com.example.bhumicloud.gisapplications.widget.ParallaxHeader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class AppGalleryFragment extends GISFragment implements TextWatcher, LoaderCallbacks<ArrayList<Item>>, OnBackPressedListener, OnItemClickListener {
    private static final String ARG_APP_CATEGORY = "GIS:arg:app_category";
    private static final String ARG_SEARCH_FILTER = "GIS:arg:current_search_filter";
    private static final int LOADER_ID = 102;
    private static final String STATE_CATEGORY = "STATE_CATEGORY";
    private static final String STATE_SEARCH_QUERY = "STATE_SEARCH_QUERY";
    private AppGalleryAdapter mAdapter;
    private int mBackoffSeconds;
    private Category mCategory;
    private ImageView mCategoryBanner;
    private LinearLayoutManager mListLayoutManager;
    private AppGalleryFragmentListener mListener;
    private String mSearchText;
    private EditText mSearchView;
    private TextView mTitleView;

    public interface AppGalleryFragmentListener {
        ArrayList<Item> getAppGallery();

        void onAppSelected(App app);

        void setAppGallery(ArrayList<Item> arrayList);
    }

    private static class AppGalleryAdapter extends Adapter<ViewHolder> {
        private static final int APP = 8;
        private static final int CATEGORY = 4;
        private static final int FIRST_VIEW = 2;
        private static final int HEADER = 1;
        private static final int NONE = 0;
        private static final float SCROLL_MULTIPLIER = 0.5f;
        private View mEmptyView;

        private ParallaxHeader mHeader;
        private final ArrayList<Item> mItems = new ArrayList ();
        private OnItemClickListener mListener;
        private RecyclerView mRecyclerView;

        private final AdapterDataObserver mEmptyViewDataObserver = new AdapterDataObserver() {
            public void onChanged() {
                AppGalleryAdapter.this.checkIfEmpty();
            }

            public void onItemRangeInserted(int positionStart, int itemCount) {
                AppGalleryAdapter.this.checkIfEmpty();
            }

            public void onItemRangeRemoved(int positionStart, int itemCount) {
                AppGalleryAdapter.this.checkIfEmpty();
            }
        };

        class C10392 extends OnScrollListener {
            C10392() {
            }

            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                AppGalleryAdapter.this.translateHeader();
            }
        }

        private static class AppViewHolder extends ViewHolder {
            private final ImageView mImageView;
            private OnItemClickListener mListener;
            private final TextView mTaglineView;
            private final TextView mTitleView;

            public AppViewHolder(final View itemView, OnItemClickListener listener) {
                super(itemView);
                this.mListener = listener;
                this.mTitleView = (TextView) itemView.findViewById(R.id.app_gallery_app_title);
                this.mTaglineView = (TextView) itemView.findViewById(R.id.app_gallery_app_tagline);
                this.mImageView = (ImageView) itemView.findViewById(R.id.app_gallery_app_imageview);
                itemView.setOnClickListener(new OnClickListener () {
                    public void onClick(View v) {
                        AppViewHolder.this.mListener.onItemClick(itemView, AppViewHolder.this.getLayoutPosition());
                    }
                });
            }

            public TextView getTitleView() {
                return this.mTitleView;
            }

            public TextView getTaglineView() {
                return this.mTaglineView;
            }

            public ImageView getAppImageView() {
                return this.mImageView;
            }
        }

        private static class BasicViewHolder extends ViewHolder {
            public BasicViewHolder(View itemView) {
                super(itemView);
            }
        }

        private static class CategoryViewHolder extends ViewHolder {
            private final ImageView mImageView;
            private OnItemClickListener mListener;
            private final TextView mTitleView;

            public CategoryViewHolder(final View itemView, OnItemClickListener listener) {
                super(itemView);
                this.mListener = listener;
                this.mTitleView = (TextView) itemView.findViewById(R.id.app_gallery_category_title);
                this.mImageView = (ImageView) itemView.findViewById(R.id.app_gallery_category_imageview);
                itemView.setOnClickListener(new OnClickListener () {
                    public void onClick(View v) {
                        CategoryViewHolder.this.mListener.onItemClick(itemView, CategoryViewHolder.this.getLayoutPosition());
                    }
                });
            }

            public TextView getTitleView() {
                return this.mTitleView;
            }

            public ImageView getCategoryImageView() {
                return this.mImageView;
            }
        }

        public AppGalleryAdapter(Context context, RecyclerView recyclerView, ParallaxHeader header, View emptyView) {
            this.mRecyclerView = recyclerView;
            this.mEmptyView = emptyView;
            checkIfEmpty();
            this.mHeader = header;
        }

        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            recyclerView.addOnScrollListener(new C10392());
            registerAdapterDataObserver(this.mEmptyViewDataObserver);
            checkIfEmpty();
        }

        public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
            unregisterAdapterDataObserver(this.mEmptyViewDataObserver);
        }

        public int getItemCount() {
            return (this.mHeader == null ? 0 : 1) + this.mItems.size();
        }

        public int getItemViewType(int position) {
            if (this.mHeader != null) {
                if (position == 0) {
                    return 1;
                }
                position--;
            }
            int type = 0;
            if (position == 0) {
                type = 0 | 2;
            }
            if (this.mItems.get(position) instanceof Category) {
                return type | 4;
            }
            if (this.mItems.get(position) instanceof App) {
                return type | 8;
            }
            return type;
        }

        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
            if ((viewType & 1) == 1 && this.mHeader != null) {
                viewHolder = new BasicViewHolder(this.mHeader);
            } else if ((viewType & 4) == 4) {
                viewHolder = new CategoryViewHolder(inflater.inflate(R.layout.list_item_app_gallery_category, viewGroup, false), this.mListener);
            } else if ((viewType & 8) == 8) {
                viewHolder = new AppViewHolder(inflater.inflate(R.layout.list_item_app_gallery_app, viewGroup, false), this.mListener);
            }
            if ((viewType & 2) == 2) {
                translateHeader();
            }
            return viewHolder;
        }

        public void onBindViewHolder(ViewHolder viewHolder, int position) {
            if (this.mHeader != null) {
                position--;
            }
            if ((viewHolder.getItemViewType() & 4) == 4) {
                configureCategoryViewHolder((CategoryViewHolder) viewHolder, position);
            } else if ((viewHolder.getItemViewType() & 8) == 8) {
                configureAppViewHolder((AppViewHolder) viewHolder, position);
            }
        }

        private void configureAppViewHolder(AppViewHolder viewHolder, int position) {
            App app = (App) this.mItems.get(position);
            if (app != null) {
                viewHolder.getTitleView().setText(app.getName());
                viewHolder.getTaglineView().setText(app.getTagline());
            }
        }

        private void configureCategoryViewHolder(CategoryViewHolder viewHolder, int position) {
            Category category = (Category) this.mItems.get(position);
            if (category != null) {
                viewHolder.getTitleView().setText(category.getName());
            }
        }

        private void translateHeader() {
            if (this.mHeader != null) {
                float translationAmount = ((float) (-this.mHeader.getTop())) * SCROLL_MULTIPLIER;
                this.mHeader.setTranslationY(translationAmount);
                this.mHeader.setClipY( Math.round(translationAmount));
            }
        }

        private void checkIfEmpty() {
            int i = 8;
            if (this.mEmptyView != null) {
                boolean emptyViewVisible;
                int i2;
                if (this.mItems.size() == 0) {
                    emptyViewVisible = true;
                } else {
                    emptyViewVisible = false;
                }
                View view = this.mEmptyView;
                if (emptyViewVisible) {
                    i2 = 0;
                } else {
                    i2 = 8;
                }
                view.setVisibility(i2);
                RecyclerView recyclerView = this.mRecyclerView;
                if (!emptyViewVisible) {
                    i = 0;
                }
                recyclerView.setVisibility(i);
            }
        }

        public void setOnItemClickListener(OnItemClickListener listener) {
            this.mListener = listener;
        }

        public void clear() {
            this.mItems.clear();
            notifyDataSetChanged();
        }

        public Item getItemAtPosition(int position) {
            if (this.mHeader != null) {
                position--;
            }
            return (Item) this.mItems.get(position);
        }

        public void addAll(List<Item> items) {
            this.mItems.addAll(items);
            notifyDataSetChanged();
        }
    }

    private static class AppGalleryFilterLoader extends AsyncTaskLoader<ArrayList<Item>> {
        private Category mCategory;
        private final ArrayList<Item> mFullGallery;
        private String mSearchFilter;

        public AppGalleryFilterLoader(Context context, String searchFilter, Category category, ArrayList<Item> fullGallery) {
            super(context);
            if (!TextUtils.isEmpty(searchFilter)) {
                searchFilter = searchFilter.trim().toLowerCase();
            }
            this.mSearchFilter = searchFilter;
            this.mCategory = category;
            this.mFullGallery = fullGallery;
        }

        public ArrayList<Item> loadInBackground() {
            Item item;
            ArrayList<Item> filteredItems = new ArrayList ();
            Iterator<Item> r7;
            if (this.mCategory == null && !TextUtils.isEmpty(this.mSearchFilter)) {
                filteredItems.addAll(this.mFullGallery);
            } else if (this.mCategory == null) {
                r7 = this.mFullGallery.iterator();
                while (r7.hasNext()) {
                    item = (Item) r7.next();
                    if (item instanceof Category) {
                        filteredItems.add(item);
                    }
                }
            } else {
                String categoryRemoteId = this.mCategory.getRemoteId();
                r7 = this.mFullGallery.iterator();
                while (r7.hasNext()) {
                    item = (Item) r7.next();
                    if (item instanceof App) {
                        App app = (App) item;
                        if (app.getCategoryIds().contains(categoryRemoteId)) {
                            filteredItems.add(app);
                        }
                    }
                }
            }
            if (!TextUtils.isEmpty(this.mSearchFilter)) {
                Iterator<Item> iterator = filteredItems.iterator();
                while (iterator.hasNext()) {
                    item = (Item) iterator.next();
                    String name = item.getName();
                    String description = item instanceof App ? ((App) item).getDescription() : null;
                    if (name == null && description == null) {
                        iterator.remove();
                    } else if (description == null) {
                        if (!name.toLowerCase().trim().contains(this.mSearchFilter)) {
                            iterator.remove();
                        }
                    } else if (name != null) {
                        name = name.toLowerCase().trim();
                        description = description.toLowerCase().trim();
                        if (!(name.contains(this.mSearchFilter) || description.contains(this.mSearchFilter))) {
                            iterator.remove();
                        }
                    } else if (!description.toLowerCase().trim().contains(this.mSearchFilter)) {
                        iterator.remove();
                    }
                }
            }
            return filteredItems;
        }

        protected void onStartLoading() {
            forceLoad();
        }
    }

    private static class AppGalleryTask extends AsyncTask<Integer, Void, ArrayList<Item>> {
        private AppGalleryFragment mAppGalleryFragment;
        private final Client mClient;

        public AppGalleryTask(AppGalleryFragment fragment) {
            this.mAppGalleryFragment = fragment;
            this.mClient = new Client(fragment.getContext());
        }

        protected ArrayList<Item> doInBackground(Integer... args) {
            int backoffTime = -1;
            if (args.length > 0) {
                backoffTime = args[0].intValue();
            }
            ArrayList<Item> items = new ArrayList ();
            if (backoffTime > 0) {
                try {
                    GISLogger.log("sleeping for: " + backoffTime);
                    Thread.sleep((long) (backoffTime * 1000));
                } catch (InterruptedException e) {
                    GISLogger.log("Could not sleep for the backoff time: " + backoffTime);
                }
            }
            try {
                Iterator it;
                HashMap<String, Object> gallery = this.mClient.getAppGallery();
                ArrayList<Object> categories = JSONUtils.getArrayList(gallery, "categories");
                if (categories != null) {
                    it = categories.iterator();
                    while (it.hasNext()) {
                        items.add(new Category(JSONUtils.getHashMap(it.next())));
                    }
                }
                ArrayList<Object> apps = JSONUtils.getArrayList(gallery, "apps");
                if (apps != null) {
                    it = apps.iterator();
                    while (it.hasNext()) {
                        items.add(new App(JSONUtils.getHashMap(it.next())));
                    }
                }
                Collections.sort(items);
            } catch (Throwable e2) {
                GISLogger.log(e2);
            }
            return items;
        }

        protected void onPostExecute(ArrayList<Item> result) {
            super.onPostExecute(result);
            if (this.mAppGalleryFragment != null && this.mAppGalleryFragment.isAdded()) {
                this.mAppGalleryFragment.onAppGallery(result);
            }
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AppGalleryFragmentListener) {
            this.mListener = (AppGalleryFragmentListener) context;
            return;
        }
        throw new RuntimeException (context.toString() + " must implement " + AppGalleryFragmentListener.class.getSimpleName());
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            this.mSearchText = savedInstanceState.getString(STATE_SEARCH_QUERY);
            this.mCategory = (Category) savedInstanceState.getParcelable(STATE_CATEGORY);
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState) {
        View view = inflater.inflate(R.layout.fragment_app_gallery, container, false);
        this.mSearchView = (EditText) view.findViewById(R.id.search_view);
        if (!TextUtils.isEmpty(this.mSearchText)) {
            this.mSearchView.setText(this.mSearchText);
        }
        this.mSearchView.addTextChangedListener(this);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.app_gallery_recycler_view);
        this.mListLayoutManager = new LinearLayoutManager (getContext());
        this.mListLayoutManager.setOrientation(1);
        recyclerView.setLayoutManager(this.mListLayoutManager);
        ParallaxHeader header = (ParallaxHeader) inflater.inflate(R.layout.view_app_gallery_header, recyclerView, false);
        this.mCategoryBanner = (ImageView) header.findViewById(R.id.category_banner);
        this.mTitleView = (TextView) header.findViewById(R.id.gallery_title_view);
        this.mAdapter = new AppGalleryAdapter(getContext(), recyclerView, header, (TextView) view.findViewById(R.id.empty));
        this.mAdapter.setOnItemClickListener(this);
        recyclerView.setAdapter(this.mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration (getContext(), 1));
        return view;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshHeaderView();
        reloadGallery();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_SEARCH_QUERY, this.mSearchText);
        outState.putParcelable(STATE_CATEGORY, this.mCategory);
    }

    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }

    public boolean onBackPressed() {
        if (this.mCategory == null) {
            return true;
        }
        this.mCategory = null;
        this.mListLayoutManager.scrollToPosition(0);
        refreshHeaderView();
        reloadGallery();
        return false;
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    public void afterTextChanged(Editable searchBox) {
        this.mSearchText = searchBox != null ? searchBox.toString().toLowerCase() : null;
        reloadGallery();
    }

    public void onItemClick(View itemView, int position) {
        KeyboardUtils.hideDialogSoftKeyboard(getActivity(), itemView);
        Item item = this.mAdapter.getItemAtPosition(position);
        if (item instanceof App) {
            this.mListener.onAppSelected((App) item);
            return;
        }
        this.mCategory = (Category) item;
        refreshHeaderView();
        reloadGallery();
        this.mListLayoutManager.scrollToPosition(0);
    }

    public Loader<ArrayList<Item>> onCreateLoader(int id, Bundle args) {
        Category category = null;
        String searchFilter = args == null ? null : args.getString(ARG_SEARCH_FILTER);
        if (args != null) {
            category = (Category) args.getParcelable(ARG_APP_CATEGORY);
        }
        return new AppGalleryFilterLoader(getActivity(), searchFilter, category, this.mListener.getAppGallery());
    }

    public void onLoadFinished(Loader<ArrayList<Item>> loader, ArrayList<Item> data) {
        this.mAdapter.clear();
        this.mAdapter.addAll(data);
        this.mAdapter.notifyDataSetChanged();
        this.mSearchView.setVisibility(View.VISIBLE);
        if (TextUtils.isEmpty(this.mSearchText) || this.mCategory != null) {
            this.mTitleView.setVisibility(View.VISIBLE);
        } else {
            this.mTitleView.setVisibility(View.GONE);
        }
    }

    public void onLoaderReset(Loader<ArrayList<Item>> loader) {
        this.mAdapter.clear();
        this.mAdapter.notifyDataSetChanged();
    }

    public void onAppGallery(ArrayList<Item> fullGallery) {
        this.mListener.setAppGallery(fullGallery);
        reloadGallery();
    }

    private void refreshHeaderView() {
        if (this.mCategory == null) {
            this.mCategoryBanner.setImageDrawable(null);
            this.mCategoryBanner.setVisibility( View.GONE);
            this.mTitleView.setText(R.string.app_categories);
            return;
        }
        String categoryBannerUrl = this.mCategory.getBannerURL();
        if (!TextUtils.isEmpty(categoryBannerUrl)) {
            this.mCategoryBanner.setVisibility(View.VISIBLE);
        }
        this.mTitleView.setText(this.mCategory.getName());
    }

    private void reloadGallery() {
        ArrayList<Item> fullGallery = this.mListener.getAppGallery();
        if (fullGallery == null || fullGallery.size() <= 0) {
            new AppGalleryTask(this).execute(new Integer[]{Integer.valueOf(this.mBackoffSeconds)});
            this.mBackoffSeconds++;
            return;
        }
        Bundle bundle = new Bundle ();
        bundle.putString(ARG_SEARCH_FILTER, this.mSearchText);
        bundle.putParcelable(ARG_APP_CATEGORY, this.mCategory);
        this.mBackoffSeconds = 0;
        getLoaderManager().restartLoader(102, bundle, this);
    }
}
