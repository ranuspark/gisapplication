package com.example.bhumicloud.gisapplications.Activity;

import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.MarkerOptions;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.maps.GISMarker;
import com.example.bhumicloud.gisapplications.maps.GISMarker.Color;
import com.example.bhumicloud.gisapplications.util.LocationUtils;
import com.example.bhumicloud.gisapplications.util.StringUtils;

public class RecordPlacementMapFragment extends MapViewFragment {
    public static final String ARG_CAMERA_POSITION = "gis:arg:placement_camera_position";
    public static final String ARG_RECORD_LOCATION = "gis:arg:placement_record_location";
    private Location mLocation;
    private CameraPosition mPosition;

    public static RecordPlacementMapFragment getInstance(Location location, CameraPosition position) {
        Bundle args = new Bundle();
        RecordPlacementMapFragment fragment = new RecordPlacementMapFragment();
        args.putParcelable(ARG_RECORD_LOCATION, location);
        args.putParcelable(ARG_CAMERA_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        this.mLocation = (Location) args.getParcelable(ARG_RECORD_LOCATION);
        this.mPosition = (CameraPosition) args.getParcelable(ARG_CAMERA_POSITION);
    }

    protected void onConfigureMap(GoogleMap map) {
        super.onConfigureMap(map);
        map.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {
            public boolean onMyLocationButtonClick() {
                RecordPlacementMapFragment.this.setManualLocation(((LocationListenerActivity) RecordPlacementMapFragment.this.getActivity()).getLastLocation());
                return false;
            }
        });
        if (this.mLocation != null) {
            MarkerOptions opts = new MarkerOptions();
            opts.position(LocationUtils.getLatLng(this.mLocation));
            opts.title(StringUtils.correctRightToLeft(getResources(), getString(R.string.record_current_location_title)));
            opts.snippet(LocationUtils.toString(this.mLocation, getResources()));
            opts.draggable(false);
            opts.icon(GISMarker.getMarkerBitmapDescriptor(Color.CB0D0C));
            GISMarker.setAnchors(opts);
            map.addMarker(opts).showInfoWindow();
        }
        if (this.mPosition != null) {
            map.moveCamera(CameraUpdateFactory.newCameraPosition(this.mPosition));
        }
    }
}
