package com.example.bhumicloud.gisapplications.View.media.photo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import  com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import  com.example.bhumicloud.gisapplications.model.field.attachment.Attachment.Type;
import  com.example.bhumicloud.gisapplications.util.BitmapUtils;
import  com.example.bhumicloud.gisapplications.util.GISLogger;
import  com.example.bhumicloud.gisapplications.util.MimeUtils;
import  com.example.bhumicloud.gisapplications.View.media.MediaImageView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;

public class PhotoImageView extends MediaImageView {
    private Attachment mAttachment;
    private Target mTarget = new Target () {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, LoadedFrom from) {
            Exception e;
            PhotoImageView.this.setImageBitmap(bitmap);
            File thumbnailFile = PhotoImageView.this.mAttachment.getThumbnail();
            if (!thumbnailFile.exists()) {
                try {
                    thumbnailFile.createNewFile();
                    FileOutputStream fos = new FileOutputStream(thumbnailFile);
                    bitmap.compress(PhotoImageView.this.mThumbnailCompressFormat, 100, fos);
                    fos.close();
                    return;
                } catch (IOException e2) {
                    e = e2;
                } catch (NullPointerException e3) {
                    e = e3;
                }
            } else {
                return;
            }
            thumbnailFile.delete();
            GISLogger.log(e);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };
    private CompressFormat mThumbnailCompressFormat;

    public PhotoImageView(Context context) {
        super(context);
    }

    public PhotoImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PhotoImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setImage(Attachment anAttachment) {
        this.mAttachment = anAttachment;
        onRenderBackgroundImage();
    }

    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);
        onRenderBackgroundImage();
    }

    private void onRenderBackgroundImage() {
        int width = getWidth();
        int height = getHeight();
        if (this.mAttachment != null && this.mAttachment.getFileOne() != null && this.mAttachment.getThumbnail() != null && width > 0 && height > 0) {
            File thumbnailFile = this.mAttachment.getThumbnail();
            if (thumbnailFile.exists()) {
                Picasso.with(getContext()).load(thumbnailFile).into(this);
                return;
            }
            File file = this.mAttachment.getFileOne();
            this.mThumbnailCompressFormat = BitmapUtils.getCompressFormat(file.getPath(), MimeUtils.getMimeType(file, Type.PHOTO));
            Picasso.with(getContext()).load(file).resize(width, height).onlyScaleDown().centerCrop().into(this.mTarget);
        }
    }
}
