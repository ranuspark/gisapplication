package com.example.bhumicloud.gisapplications.util;

import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.ElementContainer;
import java.util.HashMap;
import java.util.List;

public class ElementUtils {
    public static HashMap<String, Element> flattenElements(List<Element> elements) {
        if (elements == null) {
            throw new IllegalArgumentException("The element list must not be null.");
        }
        HashMap<String, Element> flattened = new HashMap();
        for (Element element : elements) {
            flattened.put(element.getKey(), element);
            if (element instanceof ElementContainer) {
                flattened.putAll(flattenElements(((ElementContainer) element).getElements()));
            }
        }
        return flattened;
    }

    public static HashMap<String, Element> flattenElementsByDataName(List<Element> elements) {
        if (elements == null) {
            throw new IllegalArgumentException("The element list must not be null.");
        }
        HashMap<String, Element> flattened = new HashMap();
        for (Element element : elements) {
            flattened.put(element.getDataName(), element);
            if (element instanceof ElementContainer) {
                flattened.putAll(flattenElementsByDataName(((ElementContainer) element).getElements()));
            }
        }
        return flattened;
    }
}