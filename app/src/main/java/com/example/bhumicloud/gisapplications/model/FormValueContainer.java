package com.example.bhumicloud.gisapplications.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.SectionElement;
import com.example.bhumicloud.gisapplications.model.field.address.AddressElement;
import com.example.bhumicloud.gisapplications.model.field.address.AddressValue;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaValue;
import com.example.bhumicloud.gisapplications.model.field.attachment.audio.AudioElement;
import com.example.bhumicloud.gisapplications.model.field.attachment.audio.AudioValue;
import com.example.bhumicloud.gisapplications.model.field.attachment.photo.PhotoElement;
import com.example.bhumicloud.gisapplications.model.field.attachment.photo.PhotoValue;
import com.example.bhumicloud.gisapplications.model.field.attachment.signature.SignatureElement;
import com.example.bhumicloud.gisapplications.model.field.attachment.signature.SignatureValue;
import com.example.bhumicloud.gisapplications.model.field.attachment.video.VideoElement;
import com.example.bhumicloud.gisapplications.model.field.attachment.video.VideoValue;
import com.example.bhumicloud.gisapplications.model.field.choice.ChoiceElement;
import com.example.bhumicloud.gisapplications.model.field.choice.ChoiceValue;
import com.example.bhumicloud.gisapplications.model.field.classification.ClassificationElement;
import com.example.bhumicloud.gisapplications.model.field.classification.ClassificationValue;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkElement;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkValue;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableElement;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableItemValue;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableValue;
import com.example.bhumicloud.gisapplications.model.field.temporal.date.DateElement;
import com.example.bhumicloud.gisapplications.model.field.temporal.date.DateValue;
import com.example.bhumicloud.gisapplications.model.field.temporal.time.TimeElement;
import com.example.bhumicloud.gisapplications.model.field.temporal.time.TimeValue;
import com.example.bhumicloud.gisapplications.model.field.textual.TextualElement;
import com.example.bhumicloud.gisapplications.model.field.textual.TextualValue;
import com.example.bhumicloud.gisapplications.model.field.textual.barcode.BarcodeElement;
import com.example.bhumicloud.gisapplications.model.field.textual.barcode.BarcodeValue;
import com.example.bhumicloud.gisapplications.model.field.textual.calculated.CalculatedElement;
import com.example.bhumicloud.gisapplications.model.field.textual.calculated.CalculatedValue;
import com.example.bhumicloud.gisapplications.model.field.textual.hyperlink.HyperlinkElement;
import com.example.bhumicloud.gisapplications.model.field.textual.hyperlink.HyperlinkValue;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusValue;
import com.example.bhumicloud.gisapplications.model.field.textual.text.TextElement;
import com.example.bhumicloud.gisapplications.model.field.textual.text.TextValue;
import com.example.bhumicloud.gisapplications.model.field.textual.yesno.YesNoElement;
import com.example.bhumicloud.gisapplications.model.field.textual.yesno.YesNoValue;
import com.example.bhumicloud.gisapplications.model.validation.Condition;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class FormValueContainer implements Parcelable {

    private final List<Element> mElements;
    private final SortedMap<String, FormValue> mValues;

    public static final Creator<FormValueContainer> CREATOR = new Creator<FormValueContainer>() {
        public FormValueContainer createFromParcel(Parcel source) {
            return new FormValueContainer(source);
        }

        public FormValueContainer[] newArray(int size) {
            return new FormValueContainer[size];
        }
    };

    public static FormValue convertToNewElementType(Element element, FormValue sourceFormValue) {
        if (sourceFormValue == null) {
            return null;
        }
        boolean destinationIsTextual = element instanceof TextualElement;
        boolean sourceIsTextual = sourceFormValue.getElement() instanceof TextualElement;
        TextualValue textualValue;
        if (destinationIsTextual && sourceIsTextual) {
            textualValue = (TextualValue) sourceFormValue;
            String value = textualValue.toJSON();
            if ((sourceFormValue.getElement() instanceof CalculatedElement) && (element instanceof TextElement) && !((TextElement) element).isNumeric()) {
                value = textualValue.getDisplayValue();
            }
            return inflateFormValue(element, value);
        } else if (destinationIsTextual) {
            if (!(sourceFormValue instanceof ChoiceValue) && !(sourceFormValue instanceof ClassificationValue)) {
                return null;
            }
            String displayValue = sourceFormValue.getDisplayValue();
            if (displayValue == null || displayValue.isEmpty()) {
                return null;
            }
            return inflateFormValue(element, displayValue);
        } else if (sourceIsTextual) {
            if (!(element instanceof ChoiceElement)) {
                return null;
            }
            textualValue = (TextualValue) sourceFormValue;
            if (textualValue.isEmpty()) {
                return null;
            }
            Collection values = new HashSet();
            values.add(textualValue.getValue());
            return new ChoiceValue((ChoiceElement) element, values, null);
        } else if (((element instanceof ChoiceElement) && (sourceFormValue instanceof ChoiceValue)) || (((element instanceof ClassificationElement) && (sourceFormValue instanceof ClassificationValue)) || ((element instanceof AddressElement) && (sourceFormValue instanceof AddressValue)))) {
            return inflateFormValue(element, sourceFormValue.toJSON());
        } else {
            return null;
        }
    }

    public static FormValue inflateFormValue(Element element, Object jsonValue) {
        return inflateFormValue(element, jsonValue, false);
    }

    public static FormValue inflateFormValue(Element element, Object jsonValue, boolean clearUniqueFields) {
        if (jsonValue instanceof String) {
            String stringJSONValue = (String) jsonValue;
            if (element instanceof TextElement) {
                return new TextValue((TextElement) element, stringJSONValue);
            }
            if (element instanceof DateElement) {
                return new DateValue((DateElement) element, stringJSONValue);
            }
            if (element instanceof TimeElement) {
                return new TimeValue((TimeElement) element, stringJSONValue);
            }
            if (element instanceof YesNoElement) {
                return new YesNoValue((YesNoElement) element, stringJSONValue);
            }
            if (element instanceof HyperlinkElement) {
                return new HyperlinkValue((HyperlinkElement) element, stringJSONValue);
            }
            if (element instanceof BarcodeElement) {
                return new BarcodeValue((BarcodeElement) element, stringJSONValue);
            }
            if (element instanceof CalculatedElement) {
                return new CalculatedValue((CalculatedElement) element, stringJSONValue, null);
            }
            if (element instanceof StatusElement) {
                return new StatusValue((StatusElement) element, stringJSONValue);
            }
            if (element instanceof ChoiceElement) {
                return new ChoiceValue((ChoiceElement) element, stringJSONValue, null);
            }
            if (element instanceof ClassificationElement) {
                return new ClassificationValue((ClassificationElement) element, stringJSONValue.split(","), null);
            }
        } else if (jsonValue instanceof HashMap) {
            Map hashMapJSONValue = (HashMap) jsonValue;
            if (element instanceof ChoiceElement) {
                return new ChoiceValue((ChoiceElement) element, hashMapJSONValue);
            }
            if (element instanceof ClassificationElement) {
                return new ClassificationValue((ClassificationElement) element, hashMapJSONValue);
            }
            if (element instanceof AddressElement) {
                return new AddressValue((AddressElement) element, hashMapJSONValue);
            }
            if ((element instanceof SignatureElement) && !clearUniqueFields) {
                return new SignatureValue((SignatureElement) element, hashMapJSONValue);
            }
        } else if (jsonValue instanceof ArrayList) {
            ArrayList arrayListJSONValue = (ArrayList) jsonValue;
            if ((element instanceof PhotoElement) && !clearUniqueFields) {
                return new PhotoValue((PhotoElement) element, arrayListJSONValue);
            }
            if ((element instanceof VideoElement) && !clearUniqueFields) {
                return new VideoValue((VideoElement) element, arrayListJSONValue);
            }
            if ((element instanceof AudioElement) && !clearUniqueFields) {
                return new AudioValue((AudioElement) element, arrayListJSONValue);
            }
            if (element instanceof RepeatableElement) {
                return new RepeatableValue((RepeatableElement) element, arrayListJSONValue, clearUniqueFields);
            }
            if (element instanceof RecordLinkElement) {
                return new RecordLinkValue((RecordLinkElement) element, arrayListJSONValue);
            }
        }
        return null;
    }

    public FormValueContainer(List<Element> elements) {
        this.mValues = new TreeMap();
        this.mElements = elements;
    }

    public FormValueContainer(List<Element> elements, Map attributes) {
        this(elements, attributes, false);
    }

    private FormValueContainer(List<Element> elements, Map attributes, boolean clearUniqueFields) {
        this.mValues = new TreeMap();
        this.mElements = elements;
        if (attributes != null) {
            loadValues(elements, attributes, clearUniqueFields);
        }
    }

    public FormValueContainer(Parcel parcel) {
        this.mValues = new TreeMap();
        this.mElements = new ArrayList();
        parcel.readList(this.mElements, getClass().getClassLoader());
        try {
            loadValues(this.mElements, JSONUtils.objectFromJSON(parcel.readString()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.mElements);
        dest.writeString(JSONUtils.toJSONString(toJSON()));
    }

    public int describeContents() {
        return 0;
    }

    public List<Element> getElements() {
        return this.mElements;
    }

    public FormValue getFormValue(Element element) {
        return getFormValue(element.getKey());
    }

    public FormValue getFormValue(String elementKey) {
        return (FormValue) this.mValues.get(elementKey);
    }

    public void setFormValue(Element element, FormValue value) {
        if (element != null) {
            setFormValue(element.getKey(), value);
        }
    }

    public void setFormValue(String elementKey, FormValue value) {
        if (value == null) {
            this.mValues.remove(elementKey);
        } else {
            this.mValues.put(elementKey, value);
        }
    }

    public Collection<FormValue> getAllValues() {
        return this.mValues.values();
    }

    public void loadValues(List<Element> elements, Map attributes) {
        loadValues(elements, attributes, false);
    }

    public void loadValues(List<Element> elements, Map attributes, boolean clearUniqueFields) {
        for (Element element : elements) {
            if (element instanceof SectionElement) {
                loadValues(((SectionElement) element).getElements(), attributes, clearUniqueFields);
            } else {
                setFormValue(element, loadValue(element, attributes, clearUniqueFields));
            }
        }
    }

    public FormValue loadValue(Element element, Map attributes, boolean clearUniqueFields) {
        String key = element.getKey();
        if (TextUtils.isEmpty(key) || !JSONUtils.hasKey(attributes, key)) {
            return null;
        }
        return inflateFormValue(element, attributes.get(key), clearUniqueFields);
    }

    public void mergeValues(FormValueContainer values) {
        this.mValues.putAll(values.mValues);
    }

    public String getSearchableValue() {
        List<String> tokens = new ArrayList();
        for (FormValue value : this.mValues.values()) {
            String token = value.getSearchableValue();
            if (!TextUtils.isEmpty(token)) {
                tokens.add(token);
            }
        }
        return TextUtils.join(" ", tokens);
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = new HashMap();
        for (Entry<String, FormValue> entry : this.mValues.entrySet()) {
            String key = (String) entry.getKey();
            Object jsonValue = ((FormValue) entry.getValue()).toJSON();
            if (jsonValue != null) {
                json.put(key, jsonValue);
            }
        }
        return json;
    }

    public FormValueContainer copy() {
        return copy(false);
    }

    public FormValueContainer copy(boolean clearUniqueFields) {
        return new FormValueContainer(getElements(), toJSON(), clearUniqueFields);
    }

    public void clearInvisibleValues(FormValueContainer valuesForConditions, Record record) {
        List<String> keysToRemove = new ArrayList();
        HashMap<String, Boolean> cache = new HashMap();
        for (Entry<String, FormValue> entry : this.mValues.entrySet()) {
            Element element = ((FormValue) entry.getValue()).getElement();
            boolean skipElement = element.isHidden() || record.getForm().elementHasHiddenParent(element) || record.getForm().isElementPreserved(element);
            if (!(skipElement || Condition.shouldElementBeVisible(element, record, valuesForConditions, cache))) {
                keysToRemove.add(element.getKey());
            }
        }
        for (String key : keysToRemove) {
            this.mValues.remove(key);
        }
    }

    public Set<String> getMediaValues() {
        HashSet<String> media = new HashSet();
        for (FormValue formValue : getAllValues()) {
            if (formValue instanceof MediaValue) {
                for (MediaItemValue item : ((MediaValue) formValue).getMediaItems()) {
                    media.add(item.getMediaID());
                }
            } else if (formValue instanceof SignatureValue) {
                media.add(((SignatureValue) formValue).getSignatureID());
            } else if (formValue instanceof RepeatableValue) {
                for (RepeatableItemValue item2 : ((RepeatableValue) formValue).getItems()) {
                    media.addAll(item2.getFormValues().getMediaValues());
                }
            }
        }
        return media;
    }
}
