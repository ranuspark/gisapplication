package com.example.bhumicloud.gisapplications.apiLayer;

import android.support.v4.media.session.PlaybackStateCompat;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;

public class CountingRequestBody extends RequestBody {
    private static final int SEGMENT_SIZE = 4096;
    private final File mFile ;
    private final long mFileLength ;
    private final UploadObserver mListener;
    private final MediaType mMediaType;

    public interface UploadObserver {
        void onUploadProgress(int i);
    }

    public CountingRequestBody(String mimeType, File file, UploadObserver listener) {
        this.mMediaType = MediaType.parse(mimeType);
        this.mFile = file;
        this.mFileLength= this.mFile.length();
        this.mListener = listener;
    }

    public long contentLength() {
        return this.mFileLength;
    }

    public MediaType contentType() {
        return this.mMediaType;
    }

    public void writeTo(BufferedSink sink) throws IOException {
        Source source = null;
        try {
            source = Okio.source(this.mFile);
            float fileLength = (float) this.mFileLength;
            long total = 0;
            while (true) {
                long read = source.read(sink.buffer(), PlaybackStateCompat.ACTION_SKIP_TO_QUEUE_ITEM);
                if (read == -1) {
                    break;
                }
                total += read;
                sink.flush();
                this.mListener.onUploadProgress((int) ((((float) total) / fileLength) * 100.0f));
            }
            if (source != null) {
                try {
                    source.close();
                } catch (IOException e) {
                }
            }
        } catch (Throwable th) {
            if (source != null) {
                try {
                    source.close();
                } catch (IOException e2) {
                }
            }
        }
    }
}
