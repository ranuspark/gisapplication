package com.example.bhumicloud.gisapplications.model.field.textual.hyperlink;

import android.net.Uri;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.example.bhumicloud.gisapplications.model.field.textual.TextualValue;

public class HyperlinkValue extends TextualValue {
    private final HyperlinkElement mElement;

    public HyperlinkValue(HyperlinkElement element, String aValue) {
        super(aValue);
        this.mElement = element;
    }

    public HyperlinkElement getElement() {
        return this.mElement;
    }

    public Uri getURL() {
        return TextUtils.isEmpty(this.mValue) ? null : Uri.parse(this.mValue);
    }
}
