package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import com.example.bhumicloud.gisapplications.R;


public class FlowLayout extends ViewGroup {

    public static class LayoutParams extends android.view.ViewGroup.LayoutParams {
        int f56x;
        int f57y;

        public LayoutParams(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public LayoutParams(int w, int h) {
            super(w, h);
        }
    }

    public FlowLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.getSize(widthMeasureSpec) - getPaddingRight();
        int width = 0;
        int height = getPaddingTop();
        int currentWidth = getPaddingLeft();
        int currentHeight = 0;
        int spacing = getResources().getDimensionPixelSize(R.dimen.flow_layout_padding);
        boolean newLine = false;
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            measureChild(child, widthMeasureSpec, heightMeasureSpec);
            LayoutParams lp = (LayoutParams) child.getLayoutParams();
            if (child.getMeasuredWidth() + currentWidth > widthSize) {
                height += currentHeight + spacing;
                currentHeight = 0;
                width = Math.max(width, currentWidth - spacing);
                currentWidth = getPaddingLeft();
                newLine = true;
                if (i == count - 1) {
                    currentHeight = 0 + child.getMeasuredHeight();
                    newLine = false;
                }
            } else {
                newLine = false;
            }
            lp.f56x = currentWidth;
            lp.f57y = height;
            currentWidth += child.getMeasuredWidth() + spacing;
            currentHeight = Math.max(currentHeight, child.getMeasuredHeight());
        }
        if (!newLine) {
            height += currentHeight;
            width = Math.max(width, currentWidth - spacing);
        }
        setMeasuredDimension(resolveSize(width + getPaddingRight(), widthMeasureSpec), resolveSize(height + getPaddingBottom(), heightMeasureSpec));
    }

    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            LayoutParams lp = (LayoutParams) child.getLayoutParams();
            child.layout(lp.f56x, lp.f57y, lp.f56x + child.getMeasuredWidth(), lp.f57y + child.getMeasuredHeight());
        }
    }

    protected boolean checkLayoutParams(android.view.ViewGroup.LayoutParams p) {
        return p instanceof LayoutParams;
    }

    protected LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-2, -2);
    }

    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new LayoutParams(getContext(), attrs);
    }

    protected LayoutParams generateLayoutParams(android.view.ViewGroup.LayoutParams p) {
        return new LayoutParams(p.width, p.height);
    }
}
