package com.example.bhumicloud.gisapplications.model.layer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.dossier.PersistentObject;
import com.example.bhumicloud.gisapplications.dossier.PersistentStore;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.util.CursorUtils;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

public abstract class MapLayer extends PersistentObject {
    public static final String COLUMN_ACCOUNT_ID = "account_id";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_DISPLAY_ORDER = "display_order";
    public static final String COLUMN_FILESIZE = "filesize";
    public static final String COLUMN_LOCAL_URL = "local_url";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_REMOTE_ID = "remote_id";
    public static final String COLUMN_REMOTE_URL = "remote_url";
    public static final String COLUMN_TYPE = "type";
    public static final String TABLE_NAME = "Maps";
    protected long mAccountID;
    protected String mDescription;
    private int mDisplayOrder;
    protected double mFilesize;
    protected String mName;
    protected String mRemoteID;
    private String mRemoteURL;

    public abstract File generateStorageLocation(Context context, String str);

    public abstract String getType();

    public abstract boolean isAvailable();

    public abstract void setAvailable(boolean z);

    public static MapLayer find(Long id, String remoteID, Account account) {
        SQLiteDatabase db = PersistentStore.getInstance(GIS.getInstance()).getReadableDatabase();
        ArrayList<String> predicates = new ArrayList ();
        ArrayList<String> valueList = new ArrayList ();
        if (id != null) {
            predicates.add("_id = ?");
            valueList.add( String.valueOf(id));
        }
        if (remoteID != null) {
            predicates.add("remote_id = ?");
            valueList.add(remoteID);
        }
        if (account != null) {
            predicates.add("account_id = ?");
            valueList.add( String.valueOf(account.getRowID()));
        }
        Cursor cursor = db.query(TABLE_NAME, null, TextUtils.join(" AND ", predicates), (String[]) valueList.toArray(new String[valueList.size()]), null, null, null);
        cursor.moveToFirst();
        if (cursor.isAfterLast()) {
            cursor.close();
            return null;
        }
        MapLayer layer = getTypedInstance(cursor);
        cursor.close();
        return layer;
    }

    public static MapLayer getMapLayer(long id) {
        MapLayer layer = null;
        Cursor c = GIS.getDatabase().query(TABLE_NAME, null, "_id = ?", new String[]{String.valueOf(id)}, null, null, null);
        if (c != null) {
            c.moveToFirst();
            if (!c.isAfterLast()) {
                layer = getTypedInstance(c);
            }
            c.close();
        }
        return layer;
    }

    public static ArrayList<MapLayer> getMaps(Account anAccount) {
        return getMaps(anAccount, null);
    }

    public static ArrayList<MapLayer> getMaps(Account anAccount, Set<Long> databaseIDs) {
        ArrayList<MapLayer> maps = new ArrayList ();
        ArrayList<String> predicateList = new ArrayList ();
        ArrayList<String> paramsList = new ArrayList ();
        if (anAccount != null) {
            predicateList.add("account_id = ?");
            paramsList.add( String.valueOf(anAccount.getRowID()));
        }
        if (databaseIDs != null) {
            StringBuilder sb = new StringBuilder ();
            sb.append("_id IN (");
            boolean first = true;
            for (Long databaseID : databaseIDs) {
                if (first) {
                    first = false;
                } else {
                    sb.append(", ");
                }
                sb.append("'").append(databaseID).append("'");
            }
            sb.append(")");
            predicateList.add(sb.toString());
        }
        String[] params = (String[]) paramsList.toArray(new String[paramsList.size()]);
        Cursor cursor = GIS.getDatabase().query(TABLE_NAME, null, TextUtils.join(" AND ", predicateList), params, null, null, "display_order DESC");
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            maps.add(getTypedInstance(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return maps;
    }

    public static void truncate(ArrayList<String> excludedRemoteIDs, Account account) {
        SQLiteDatabase db = GIS.getDatabase();
        ArrayList<String> predicates = new ArrayList ();
        ArrayList<String> paramsList = new ArrayList ();
        if (account != null) {
            predicates.add("account_id = ?");
            paramsList.add( String.valueOf(account.getRowID()));
        }
        if (excludedRemoteIDs != null) {
            StringBuilder sb = new StringBuilder ();
            sb.append("remote_id NOT IN (");
            for (int i = 0; i < excludedRemoteIDs.size(); i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append("'").append((String) excludedRemoteIDs.get(i)).append("'");
            }
            sb.append(")");
            predicates.add(sb.toString());
        }
        predicates.add("remote_url IS NOT NULL");
        db.delete(TABLE_NAME, TextUtils.join(" AND ", predicates), (String[]) paramsList.toArray(new String[paramsList.size()]));
    }

    public static MapLayer getTypedInstance(Cursor cursor) {
        String type = CursorUtils.getString(cursor, "type");
        if (type == null) {
            return new MBTilesMapLayer(cursor);
        }
        int obj = -1;
        switch (type.hashCode()) {
            case 119193:
                if (type.equals(XYZMapLayer.TYPE)) {
                    obj = 0;
                    break;
                }
                break;
            case 869673744:
                if (type.equals(MBTilesMapLayer.TYPE)) {
                    obj = 1;
                    break;
                }
                break;
        }
        switch (obj) {
            case 0:
                return new XYZMapLayer(cursor);
            default:
                return new MBTilesMapLayer(cursor);
        }
    }

    public static MapLayer getTypedInstance(Account account, Map jsonRepresentation) {
        String type = JSONUtils.getString(jsonRepresentation, "type");
        int obj = -1;
        switch (type.hashCode()) {
            case 119193:
                if (type.equals(XYZMapLayer.TYPE)) {
                    obj = 0;
                    break;
                }
                break;
            case 869673744:
                if (type.equals(MBTilesMapLayer.TYPE)) {
                    obj = 1;
                    break;
                }
                break;
        }
        switch (obj) {
            case 0:
                return new XYZMapLayer(account, jsonRepresentation);
            default:
                return new MBTilesMapLayer(account, jsonRepresentation);
        }
    }

    protected MapLayer() {
    }

    protected MapLayer(Cursor cursor) {
        super(cursor);
        this.mRemoteID = CursorUtils.getString(cursor, "remote_id");
        this.mAccountID = CursorUtils.getLong(cursor, "account_id");
        this.mName = CursorUtils.getString(cursor, "name");
        this.mDescription = CursorUtils.getString(cursor, "description");
        this.mFilesize = CursorUtils.getDouble(cursor, COLUMN_FILESIZE);
        this.mRemoteURL = CursorUtils.getString(cursor, COLUMN_REMOTE_URL);
        this.mDisplayOrder = CursorUtils.getInt(cursor, COLUMN_DISPLAY_ORDER);
    }

    protected MapLayer(Account account, Map jsonRepresentation) {
        if (account != null) {
            this.mAccountID = account.getRowID();
        }
        setAttributesFromJSON(jsonRepresentation);
    }

    protected MapLayer(Parcel parcel) {
        this.mRemoteID = parcel.readString();
        this.mAccountID = parcel.readLong();
        this.mName = parcel.readString();
        this.mDescription = parcel.readString();
        this.mFilesize = parcel.readDouble();
        this.mRemoteURL = parcel.readString();
        this.mDisplayOrder = parcel.readInt();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mRemoteID);
        dest.writeLong(this.mAccountID);
        dest.writeString(this.mName);
        dest.writeString(this.mDescription);
        dest.writeDouble(this.mFilesize);
        dest.writeString(this.mRemoteURL);
        dest.writeInt(this.mDisplayOrder);
    }

    public void setAttributesFromJSON(Map jsonRepresentation) {
        this.mRemoteID = JSONUtils.getString(jsonRepresentation, "id");
        this.mName = JSONUtils.getString(jsonRepresentation, "name", "No Title");
        this.mDescription = JSONUtils.getString(jsonRepresentation, "description", "");
        this.mFilesize = JSONUtils.getDouble(jsonRepresentation, "file_size", 0.0d);
        this.mRemoteURL = JSONUtils.getString(jsonRepresentation,"feed");
    }

    public String getRemoteID() {
        return this.mRemoteID;
    }

    public long getAccountID() {
        return this.mAccountID;
    }

    public String getName() {
        return this.mName;
    }

    public String getDescription() {
        return this.mDescription;
    }

    public double getFilesize() {
        return this.mFilesize;
    }

    public URL getRemoteURL() {
        if (!TextUtils.isEmpty(this.mRemoteURL)) {
            try {
                return new URL (this.mRemoteURL);
            } catch (Throwable e) {
                GISLogger.log(e);
            }
        }
        return null;
    }

    public int getDisplayOrder() {
        return this.mDisplayOrder;
    }

    public void setDisplayOrder(int order) {
        this.mDisplayOrder = order;
    }

    public File generateStorageLocation(Context context) {
        return generateStorageLocation(context, getRemoteID());
    }

    public String toString() {
        return getName();
    }

    protected String getTableName() {
        return TABLE_NAME;
    }

    protected ContentValues getContentValues() {
        ContentValues values = super.getContentValues();
        values.put("remote_id", getRemoteID());
        values.put("account_id", Long.valueOf(getAccountID()));
        values.put("name", getName());
        values.put("description", getDescription());
        values.put(COLUMN_FILESIZE, Double.valueOf(getFilesize()));
        values.put(COLUMN_REMOTE_URL, this.mRemoteURL);
        values.put(COLUMN_DISPLAY_ORDER, Integer.valueOf(this.mDisplayOrder));
        values.put("type", getType());
        return values;
    }
}
