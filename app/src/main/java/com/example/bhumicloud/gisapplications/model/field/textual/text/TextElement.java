package com.example.bhumicloud.gisapplications.model.field.textual.text;

import android.os.Parcel;
import android.os.Parcelable.Creator;

import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.textual.TextualElement;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.HashMap;
import java.util.Map;

public class TextElement extends TextualElement {
    public static final Creator<TextElement> CREATOR = new C12061();
    private static final double DEFAULT_MAX = Double.MAX_VALUE;
    private static final double DEFAULT_MIN = -1.7976931348623157E308d;
    public static final String FORMAT_DECIMAL = "decimal";
    public static final String FORMAT_INTEGER = "integer";
    private static final String JSON_FORMAT = "format";
    private static final String JSON_MAX_VALUE = "max";
    private static final String JSON_MIN_VALUE = "min";
    private static final String JSON_NUMERIC = "numeric";
    private static final String JSON_PATTERN = "pattern";
    private static final String JSON_PATTERN_DESCRIPTION = "pattern_description";
    private double mMaxValue;
    private double mMinValue;
    private boolean mNumeric;
    private String mNumericFormat;
    private String mPattern;
    private String mPatternDescription;

    static class C12061 implements Creator<TextElement> {
        C12061() {
        }

        public TextElement createFromParcel(Parcel source) {
            return new TextElement(source);
        }

        public TextElement[] newArray(int size) {
            return new TextElement[size];
        }
    }

    public TextElement(Element parent, Map json) {
        super(parent, json);
    }

    private TextElement(Parcel parcel) {
        super(parcel);
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = super.toJSON();
        json.put(JSON_NUMERIC, Boolean.valueOf(this.mNumeric));
        if (this.mMinValue != DEFAULT_MIN) {
            json.put(JSON_MIN_VALUE, Double.valueOf(this.mMinValue));
        }
        if (this.mMaxValue != DEFAULT_MAX) {
            json.put(JSON_MAX_VALUE, Double.valueOf(this.mMaxValue));
        }
        json.put(JSON_FORMAT, this.mNumericFormat);
        return json;
    }

    public String getType() {
        return Element.TYPE_TEXT;
    }

    public boolean isNumeric() {
        return this.mNumeric;
    }

    public double getMinValue() {
        return this.mMinValue;
    }

    public double getMaxValue() {
        return this.mMaxValue;
    }

    public String getNumericFormat() {
        return this.mNumericFormat;
    }

    public boolean isPatternEmpty() {
        return this.mPattern == null;
    }

    public String getPattern() {
        return "^(?:" + this.mPattern + ")$";
    }

    public String getPatternDescription() {
        return this.mPatternDescription;
    }

    public boolean isInteger() {
        return this.mNumeric && FORMAT_INTEGER.equals(this.mNumericFormat);
    }

    public boolean isDecimal() {
        return this.mNumeric && FORMAT_DECIMAL.equals(this.mNumericFormat);
    }

    public void setJSONAttributes(Map json) {
        super.setJSONAttributes(json);
        this.mNumeric = JSONUtils.getBoolean(json, JSON_NUMERIC, false);
        this.mMinValue = JSONUtils.getDouble(json, JSON_MIN_VALUE, DEFAULT_MIN);
        this.mMaxValue = JSONUtils.getDouble(json, JSON_MAX_VALUE, DEFAULT_MAX);
        this.mNumericFormat = JSONUtils.getString(json, JSON_FORMAT, FORMAT_DECIMAL);
        this.mPattern = JSONUtils.getString(json, JSON_PATTERN);
        this.mPatternDescription = JSONUtils.getString(json, JSON_PATTERN_DESCRIPTION);
    }
}
