package com.example.bhumicloud.gisapplications.model.field.textual.status;

import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.model.Jsonable;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.choice.ChoiceItem;
import com.example.bhumicloud.gisapplications.model.field.textual.TextualElement;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StatusElement extends TextualElement {
    public static final Creator<StatusElement> CREATOR = new C12051();
    private static final String JSON_CHOICES = "choices";
    private static final String JSON_ENABLED = "enabled";
    private static final String JSON_FILTER = "filter";
    private static final String JSON_READ_ONLY = "read_only";
    private boolean mEnabled;
    private List<StatusOption> mOptions;
    private boolean mReadOnly;
    private List<String> mStatusFilter;

    static class C12051 implements Creator<StatusElement> {
        C12051() {
        }

        public StatusElement createFromParcel(Parcel source) {
            return new StatusElement(source);
        }

        public StatusElement[] newArray(int size) {
            return new StatusElement[size];
        }
    }

    public static class StatusOption extends ChoiceItem implements Jsonable {
        private static final String JSON_COLOR = "color";
        private final String mColor;

        public StatusOption(Map json) {
            super(json);
            this.mColor = JSONUtils.getString(json, JSON_COLOR);
        }

        public HashMap<String, Object> toJSON() {
            HashMap<String, Object> json = super.toJSON();
            json.put(JSON_COLOR, this.mColor);
            return json;
        }

        public int getColor() {
            return Color.parseColor(this.mColor);
        }

        public String getColorString() {
            return this.mColor;
        }
    }

    public StatusElement(Element parent, Map json) {
        super(parent, json);
    }

    private StatusElement(Parcel parcel) {
        super(parcel);
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = super.toJSON();
        json.put(JSON_READ_ONLY, Boolean.valueOf(this.mReadOnly));
        json.put(JSON_ENABLED, Boolean.valueOf(this.mEnabled));
        json.put("choices", JSONUtils.jsonableListToArrayNode(this.mOptions));
        json.put(JSON_FILTER, this.mStatusFilter);
        return json;
    }

    public String getType() {
        return Element.TYPE_STATUS;
    }

    public boolean isReadOnly() {
        return getOverrideIsDisabled() != null ? getOverrideIsDisabled().booleanValue() : this.mReadOnly;
    }

    public boolean isEnabled() {
        return this.mEnabled;
    }

    public List<StatusOption> getOptions() {
        return this.mOptions;
    }

    public StatusOption getOption(String value) {
        if (!(this.mOptions == null || TextUtils.isEmpty(value))) {
            for (StatusOption option : this.mOptions) {
                if (value.equals(option.getValue())) {
                    return option;
                }
            }
        }
        return null;
    }

    public void setJSONAttributes(Map json) {
        int i;
        super.setJSONAttributes(json);
        this.mReadOnly = JSONUtils.getBoolean(json, JSON_READ_ONLY);
        this.mEnabled = JSONUtils.getBoolean(json, JSON_ENABLED);
        this.mOptions = new ArrayList();
        List optionsJSON = JSONUtils.getArrayList(json, "choices");
        if (!(optionsJSON == null || optionsJSON.isEmpty())) {
            for (i = 0; i < optionsJSON.size(); i++) {
                HashMap<String, Object> optionJSON = JSONUtils.getHashMap(optionsJSON, i);
                if (optionJSON != null) {
                    this.mOptions.add(new StatusOption(optionJSON));
                }
            }
        }
        List filterJSON = JSONUtils.getArrayList(json, JSON_FILTER);
        if (filterJSON != null && !filterJSON.isEmpty()) {
            ArrayList<String> statusFilter = new ArrayList();
            for (i = 0; i < filterJSON.size(); i++) {
                String value = JSONUtils.getString(filterJSON, i);
                if (value != null) {
                    statusFilter.add(value);
                }
            }
            setStatusFilter(statusFilter);
        }
    }

    public void setStatusFilter(List choices) {
        if (choices == null || choices.isEmpty()) {
            this.mStatusFilter = null;
            return;
        }
        this.mStatusFilter = new ArrayList();
        for (Object item : choices) {
            if (item instanceof String) {
                this.mStatusFilter.add(((String) item).toLowerCase());
            }
        }
    }

    public List<StatusOption> getFilteredChoices() {
        List<StatusOption> items = this.mOptions;
        if (this.mStatusFilter == null || this.mStatusFilter.isEmpty()) {
            return items;
        }
        List<StatusOption> filteredItems = new ArrayList();
        for (StatusOption item : items) {
            for (String filter : this.mStatusFilter) {
                if (item.getValue().toLowerCase().contains(filter.toLowerCase())) {
                    filteredItems.add(item);
                }
            }
        }
        return filteredItems;
    }
}
