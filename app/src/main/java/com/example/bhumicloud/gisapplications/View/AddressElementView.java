package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import com.example.bhumicloud.gisapplications.model.field.address.AddressElement;

public class AddressElementView extends ElementView<AddressElement> {
    private AddressElementViewListener mListener;

    public interface AddressElementViewListener {
        void onShowAddressDialog(AddressElement addressElement);
    }

    public AddressElementView(Context context, AddressElement element, boolean viewOnly) {
        super(context, element, viewOnly);
    }

    public void setListener(AddressElementViewListener listener) {
        this.mListener = listener;
    }

    protected void onFieldViewClicked() {
        super.onFieldViewClicked();
        if (this.mListener != null) {
            this.mListener.onShowAddressDialog((AddressElement) getElement());
        }
    }
}
