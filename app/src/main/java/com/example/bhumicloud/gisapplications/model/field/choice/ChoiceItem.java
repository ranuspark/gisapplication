package com.example.bhumicloud.gisapplications.model.field.choice;

import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.model.Jsonable;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.HashMap;
import java.util.Map;

public class ChoiceItem implements Jsonable {
    private static final String JSON_LABEL = "label";
    private static final String JSON_VALUE = "value";
    private final String mLabel;
    private final String mValue;

    public ChoiceItem(Map json) {
        this.mLabel = JSONUtils.getString(json, JSON_LABEL);
        this.mValue = JSONUtils.getString(json, "value");
    }

    public ChoiceItem(String label, String value) {
        this.mLabel = label;
        this.mValue = value;
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = new HashMap();
        json.put(JSON_LABEL, this.mLabel);
        json.put("value", this.mValue);
        return json;
    }

    public String getLabel() {
        return this.mLabel;
    }

    public String getValue() {
        return this.mValue;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof ChoiceItem)) {
            return false;
        }
        ChoiceItem aChoiceItem = (ChoiceItem) object;
        if (!aChoiceItem.canEqual(this)) {
            return false;
        }
        if (TextUtils.equals(getLabel(), aChoiceItem.getLabel()) && TextUtils.equals(getValue(), aChoiceItem.getValue())) {
            return true;
        }
        return false;
    }

    public boolean canEqual(Object other) {
        return other instanceof ChoiceItem;
    }

    public int hashCode() {
        int result;
        int i = 0;
        if (this.mLabel != null) {
            result = this.mLabel.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.mValue != null) {
            i = this.mValue.hashCode();
        }
        return i2 + i;
    }

    public String toString() {
        return this.mLabel;
    }
}
