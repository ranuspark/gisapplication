package com.example.bhumicloud.gisapplications.model.appgallery;

import android.os.Parcel;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class App extends Item {
    public static final Creator<App> CREATOR = new C11721();
    private final ArrayList<String> mCategoryIds = new ArrayList ();
    private final String mDescription;
    private final String mImageURL;
    private final String mTagline;

    static class C11721 implements Creator<App> {
        C11721() {
        }

        public App createFromParcel(Parcel source) {
            return new App(source);
        }

        public App[] newArray(int size) {
            return new App[size];
        }
    }

    public App(Map<String, Object> json) {
        super((Map) json);
        this.mTagline = JSONUtils.getString((Map) json, "tagline");
        this.mDescription = JSONUtils.getString((Map) json, "description");
        this.mImageURL = JSONUtils.getString((Map) json, "image");
        List categoryIds = JSONUtils.getArrayList(json, "category_ids");
        if (categoryIds != null && categoryIds.size() > 0) {
            for (int i = 0; i < categoryIds.size(); i++) {
                String categoryId = JSONUtils.getString(categoryIds, i);
                if (!TextUtils.isEmpty(categoryId)) {
                    this.mCategoryIds.add(categoryId);
                }
            }
        }
    }

    protected App(Parcel parcel) {
        super(parcel);
        this.mTagline = parcel.readString();
        this.mDescription = parcel.readString();
        this.mImageURL = parcel.readString();
        ArrayList<String> categoryIds = parcel.createStringArrayList();
        if (categoryIds != null && categoryIds.size() > 0) {
            this.mCategoryIds.addAll(categoryIds);
        }
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.mDescription);
        dest.writeString(this.mTagline);
        dest.writeString(this.mImageURL);
        dest.writeStringList(this.mCategoryIds);
    }

    public String getDescription() {
        return this.mDescription;
    }

    public String getTagline() {
        return this.mTagline;
    }

    public String getImageURL() {
        return this.mImageURL;
    }

    public ArrayList<String> getCategoryIds() {
        return this.mCategoryIds;
    }
}
