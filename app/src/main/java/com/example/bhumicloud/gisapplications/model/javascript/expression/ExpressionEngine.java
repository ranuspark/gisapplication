package com.example.bhumicloud.gisapplications.model.javascript.expression;

import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;

import com.eclipsesource.v8.JavaCallback;
import com.eclipsesource.v8.JavaVoidCallback;
import com.eclipsesource.v8.V8Array;
import com.eclipsesource.v8.V8Object;
import com.eclipsesource.v8.V8ScriptExecutionException;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.geoJson.GeoJSONPoint;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Feature;
import com.example.bhumicloud.gisapplications.model.FormValueContainer;
import com.example.bhumicloud.gisapplications.model.Project;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.Storage;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableItemValue;
import com.example.bhumicloud.gisapplications.model.field.textual.calculated.CalculatedElement;
import com.example.bhumicloud.gisapplications.model.javascript.JavaScriptResource;
import com.example.bhumicloud.gisapplications.util.ApplicationUtils;
import com.example.bhumicloud.gisapplications.util.DeviceInfo;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.io.File;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class ExpressionEngine extends JavaScriptResource {
    private static final String FUNCTION_EVALUATE = "$$evaluate";
    private static final String FUNCTION_FINISH_ASYNC = "$$finishAsync";
    private static final String FUNCTION_FORMAT_NUMBER = "$$formatNumber";
    private static final String FUNCTION_HTTP_REQUEST = "$$httpRequest";
    private static final String FUNCTION_PREPARE = "$$prepare";
    private static final String FUNCTION_SET_TIMEOUT = "$$setTimeout";
    private static final String FUNCTION_STORAGE_CLEAR = "$$storageClear";
    private static final String FUNCTION_STORAGE_GET_ITEM = "$$storageGetItem";
    private static final String FUNCTION_STORAGE_KEY = "$$storageKey";
    private static final String FUNCTION_STORAGE_LENGTH = "$$storageLength";
    private static final String FUNCTION_STORAGE_REMOVE_ITEM = "$$storageRemoveItem";
    private static final String FUNCTION_STORAGE_SET_ITEM = "$$storageSetItem";
    private static final String FUNCTION_TRIGGER = "$$trigger";
    private static final String VARIABLE_APPLICATION = "application";
    private static final String VARIABLE_APPLICATION_BUILD = "applicationBuild";
    private static final String VARIABLE_APPLICATION_VERSION = "applicationVersion";
    private static final String VARIABLE_CALLBACK_ARGUMENTS = "callbackArguments";
    private static final String VARIABLE_CALLBACK_ID = "callbackID";
    private static final String VARIABLE_COUNTRY = "country";
    private static final String VARIABLE_CURRENCY_CODE = "currencyCode";
    private static final String VARIABLE_CURRENCY_SYMBOL = "currencySymbol";
    private static final String VARIABLE_CURRENT_LOCATION = "currentLocation";
    private static final String VARIABLE_DECIMAL_SEPARATOR = "decimalSeparator";
    private static final String VARIABLE_DEVICE_ID = "deviceIdentifier";
    private static final String VARIABLE_DEVICE_MANUFACTURER = "deviceManufacturer";
    private static final String VARIABLE_DEVICE_MODEL = "deviceModel";
    private static final String VARIABLE_EVENT = "event";
    private static final String VARIABLE_EXPRESSIONS = "expressions";
    private static final String VARIABLE_FEATURE_CREATED_ACCURACY = "featureCreatedAccuracy";
    private static final String VARIABLE_FEATURE_CREATED_ALTITUDE = "featureCreatedAltitude";
    private static final String VARIABLE_FEATURE_CREATED_AT = "featureCreatedAt";
    private static final String VARIABLE_FEATURE_CREATED_DURATION = "featureCreatedDuration";
    private static final String VARIABLE_FEATURE_CREATED_LATITUDE = "featureCreatedLatitude";
    private static final String VARIABLE_FEATURE_CREATED_LONGITUDE = "featureCreatedLongitude";
    private static final String VARIABLE_FEATURE_EDITED_DURATION = "featureEditedDuration";
    private static final String VARIABLE_FEATURE_GEOMETRY = "featureGeometry";
    private static final String VARIABLE_FEATURE_ID = "featureID";
    private static final String VARIABLE_FEATURE_INDEX = "featureIndex";
    private static final String VARIABLE_FEATURE_IS_NEW = "featureIsNew";
    private static final String VARIABLE_FEATURE_UPDATED_ACCURACY = "featureUpdatedAccuracy";
    private static final String VARIABLE_FEATURE_UPDATED_ALTITUDE = "featureUpdatedAltitude";
    private static final String VARIABLE_FEATURE_UPDATED_AT = "featureUpdatedAt";
    private static final String VARIABLE_FEATURE_UPDATED_DURATION = "featureUpdatedDuration";
    private static final String VARIABLE_FEATURE_UPDATED_LATITUDE = "featureUpdatedLatitude";
    private static final String VARIABLE_FEATURE_UPDATED_LONGITUDE = "featureUpdatedLongitude";
    private static final String VARIABLE_FORM = "form";
    private static final String VARIABLE_GROUPING_SEPARATOR = "groupingSeparator";
    private static final String VARIABLE_LANGUAGE = "language";
    private static final String VARIABLE_LOCALE = "locale";
    private static final String VARIABLE_PLATFORM = "platform";
    private static final String VARIABLE_PLATFORM_VERSION = "platformVersion";
    private static final String VARIABLE_RECORD_ALTITUDE = "recordAltitude";
    private static final String VARIABLE_RECORD_CLIENT_CREATED_AT = "recordClientCreatedAt";
    private static final String VARIABLE_RECORD_CLIENT_UPDATED_AT = "recordClientUpdatedAt";
    private static final String VARIABLE_RECORD_CREATED_ACCURACY = "recordCreatedAccuracy";
    private static final String VARIABLE_RECORD_CREATED_ALTITUDE = "recordCreatedAltitude";
    private static final String VARIABLE_RECORD_CREATED_DURATION = "recordCreatedDuration";
    private static final String VARIABLE_RECORD_CREATED_LATITUDE = "recordCreatedLatitude";
    private static final String VARIABLE_RECORD_CREATED_LONGITUDE = "recordCreatedLongitude";
    private static final String VARIABLE_RECORD_EDITED_DURATION = "recordEditedDuration";
    private static final String VARIABLE_RECORD_GEOMETRY = "recordGeometry";
    private static final String VARIABLE_RECORD_HORIZONTAL_ACCURACY = "recordHorizontalAccuracy";
    private static final String VARIABLE_RECORD_ID = "recordID";
    private static final String VARIABLE_RECORD_PROJECT = "recordProject";
    private static final String VARIABLE_RECORD_PROJECT_NAME = "recordProjectName";
    private static final String VARIABLE_RECORD_STATUS = "recordStatus";
    private static final String VARIABLE_RECORD_UPDATED_ACCURACY = "recordUpdatedAccuracy";
    private static final String VARIABLE_RECORD_UPDATED_ALTITUDE = "recordUpdatedAltitude";
    private static final String VARIABLE_RECORD_UPDATED_DURATION = "recordUpdatedDuration";
    private static final String VARIABLE_RECORD_UPDATED_LATITUDE = "recordUpdatedLatitude";
    private static final String VARIABLE_RECORD_UPDATED_LONGITUDE = "recordUpdatedLongitude";
    private static final String VARIABLE_RECORD_VERTICAL_ACCURACY = "recordVerticalAccuracy";
    private static final String VARIABLE_REPEATABLE = "repeatable";
    private static final String VARIABLE_SCRIPT = "script";
    private static final String VARIABLE_TIME_ZONE = "timeZone";
    private static final String VARIABLE_USER_EMAIL = "userEmail";
    private static final String VARIABLE_USER_FULL_NAME = "userFullName";
    private static final String VARIABLE_USER_ROLE_NAME = "userRoleName";
    private static final String VARIABLE_VALUES = "values";
    private static Handler sHandler;
    private List<CalculatedElement> mCalculatedElements;
    private Location mCurrentLocation;
    private ExpressionEngineListener mExpressionEngineListener;
    private ArrayList<Map<String, String>> mExpressions;
    private Feature mFeature;
    private boolean mIsNewFeature;
    private Record mRecord;
    private HashMap<String, Object> mValuesAsJSON;

    public interface ExpressionEngineListener {
        void onExpressionError(String str);

        void onExpressionResults(List<ExpressionResult> list);
    }

    public ExpressionEngine(Context context) {
        super(context);
    }

    public void setFeature(Feature feature) {
        this.mFeature = feature;
    }

    public void setRecord(Record record) {
        if (this.mRecord != record) {
            this.mRecord = record;
            evaluateRuntimeVariableAssignment(VARIABLE_FORM, record.getForm().toJSON());
            String script = record.getForm().getScript();
            if (script != null) {
                assignRuntimeVariable(VARIABLE_SCRIPT, script);
            }
            try {
                evaluateVoidFunctionCall(FUNCTION_PREPARE);
            } catch (V8ScriptExecutionException jsx) {
                JavaScriptResource.logError(String.format("JS Error: %s - %s - %d", new Object[]{jsx.getJSStackTrace(), jsx.getFileName(), Integer.valueOf(jsx.getLineNumber())}));
                onError(jsx);
            }
        }
    }

    public Record getRecord() {
        return this.mRecord;
    }

    public void setIsNewFeature(boolean isNewFeature) {
        this.mIsNewFeature = isNewFeature;
    }

    public void setValues(FormValueContainer values) {
        this.mValuesAsJSON = values.toJSON();
        setupFeatureVariables();
    }

    public void setCurrentLocation(Location location) {
        this.mCurrentLocation = location;
        setupLocationVariables();
    }

    public void setExpressionEngineListener(ExpressionEngineListener listener) {
        this.mExpressionEngineListener = listener;
    }

    public List<ExpressionResult> evaluate() {
        evaluateRuntimeVariableAssignment(VARIABLE_VALUES, this.mValuesAsJSON);
        ArrayList<ExpressionResult> results = new ArrayList();
        V8Array nativeResults = null;
        try {
            nativeResults = (V8Array) evaluateFunctionCall(FUNCTION_EVALUATE);
            for (int i = 0; i < nativeResults.length(); i++) {
                V8Object result = nativeResults.getObject(i);
                results.add(new ExpressionResult(result));
                result.release();
            }
            if (nativeResults != null) {
                nativeResults.release();
            }
        } catch (V8ScriptExecutionException jsx) {
            JavaScriptResource.logError(String.format("JS Error: %s - %s - %d", new Object[]{jsx.getJSStackTrace(), jsx.getFileName(), Integer.valueOf(jsx.getLineNumber())}));
            onError(jsx);
            if (nativeResults != null) {
                nativeResults.release();
            }
        } catch (Throwable th) {
            if (nativeResults != null) {
                nativeResults.release();
            }
        }
        return results;
    }

    public List<ExpressionResult> triggerEvent(Map<String, Object> event) {
        evaluateRuntimeVariableAssignment(VARIABLE_VALUES, this.mValuesAsJSON);
        evaluateRuntimeVariableAssignment(VARIABLE_EVENT, (Map) event);
        ArrayList<ExpressionResult> results = new ArrayList();
        V8Array nativeResults = null;
        try {
            nativeResults = (V8Array) evaluateFunctionCall(FUNCTION_TRIGGER);
            for (int i = 0; i < nativeResults.length(); i++) {
                V8Object result = nativeResults.getObject(i);
                results.add(new ExpressionResult(result));
                result.release();
            }
            if (nativeResults != null) {
                nativeResults.release();
            }
        } catch (V8ScriptExecutionException jsx) {
            JavaScriptResource.logError(String.format("JS Error: %s - %s - %d", new Object[]{jsx.getJSStackTrace(), jsx.getFileName(), Integer.valueOf(jsx.getLineNumber())}));
            onError(jsx);
            if (nativeResults != null) {
                nativeResults.release();
            }
        } catch (Throwable th) {
            if (nativeResults != null) {
                nativeResults.release();
            }
        }
        return results;
    }

    public void setCalculatedElements(List<CalculatedElement> elements) {
        if (this.mCalculatedElements != elements) {
            this.mCalculatedElements = elements;
            this.mExpressions = null;
            evaluateRuntimeVariableAssignmentFromArray(VARIABLE_EXPRESSIONS, getExpressions());
        }
    }

    protected File getAutoUpdatingScriptPath(Context context) {
        return new File(new File(context.getFilesDir(), "resources"), "expressions.js");
    }

    protected int getJavaScriptRawResourceID() {
        return R.raw.expressions;
    }

    protected String getEnvironmentName() {
        return ExpressionEngine.class.getCanonicalName();
    }

    protected void setupRuntimeVariables(Context context) {
        Locale locale = Locale.getDefault();
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
        assignRuntimeVariable(VARIABLE_TIME_ZONE, TimeZone.getDefault().getID());
        assignRuntimeVariable(VARIABLE_LOCALE, locale.toString());
        assignRuntimeVariable(VARIABLE_LANGUAGE, String.format("%s-%s", new Object[]{locale.getLanguage(), locale.getCountry()}));
        assignRuntimeVariable(VARIABLE_DECIMAL_SEPARATOR, String.valueOf(symbols.getDecimalSeparator()));
        assignRuntimeVariable(VARIABLE_GROUPING_SEPARATOR, String.valueOf(symbols.getGroupingSeparator()));
        assignRuntimeVariable(VARIABLE_CURRENCY_SYMBOL, symbols.getCurrencySymbol());
        assignRuntimeVariable(VARIABLE_CURRENCY_CODE, symbols.getCurrency().getCurrencyCode());
        assignRuntimeVariable(VARIABLE_COUNTRY, locale.getCountry());
        assignRuntimeVariable(VARIABLE_DEVICE_ID, DeviceInfo.getDeviceIdentifier(context));
        assignRuntimeVariable(VARIABLE_DEVICE_MODEL, DeviceInfo.getDeviceModel());
        assignRuntimeVariable(VARIABLE_DEVICE_MANUFACTURER, DeviceInfo.getDeviceManufacturer());
        assignRuntimeVariable(VARIABLE_PLATFORM, "Android");
        assignRuntimeVariable(VARIABLE_PLATFORM_VERSION, DeviceInfo.getSystemVersion());
        assignRuntimeVariable(VARIABLE_APPLICATION, "Fulcrum Android");
        assignRuntimeVariable(VARIABLE_APPLICATION_VERSION, ApplicationUtils.getVersion(context));
        assignRuntimeVariable(VARIABLE_APPLICATION_BUILD, String.valueOf(ApplicationUtils.getBuildNumber(context)));
    }

    protected void setupRuntimeFunctions() {
        JavaCallback formatNumber = new JavaCallback() {
            public Object invoke(V8Object receiver, V8Array parameters) {
                Number number = Double.valueOf(parameters.getDouble(0));
                String locale = parameters.getString(1);
                V8Object options = parameters.getObject(2);
                String result = ExpressionEngineFunctions.formatNumber(number, locale, options);
                options.release();
                return result;
            }
        };
        JavaVoidCallback httpRequest = new JavaVoidCallback() {
            public void invoke(V8Object receiver, V8Array parameters) {
                String optionsJSON = parameters.getString(0);
                int callbackID = parameters.getInteger(1);
                ExpressionEngineFunctions.httpRequest(ExpressionEngine.this, JSONUtils.tryObjectFromJSON(optionsJSON), callbackID);
            }
        };
        JavaVoidCallback setTimeout = new JavaVoidCallback() {
            public void invoke(V8Object receiver, V8Array parameters) {
                ExpressionEngineFunctions.setTimeout(ExpressionEngine.this, (long) parameters.getInteger(0), parameters.getInteger(1));
            }
        };
        JavaCallback storageLength = new JavaCallback() {
            public Object invoke(V8Object receiver, V8Array parameters) {
                return Account.getActiveAccount().getStorage().getLength();
            }
        };
        JavaCallback storageKey = new JavaCallback() {
            public Object invoke(V8Object receiver, V8Array parameters) {
                return Account.getActiveAccount().getStorage().getKey(parameters.getInteger(1));
            }
        };
        JavaCallback storageGetItem = new JavaCallback() {
            public Object invoke(V8Object receiver, V8Array parameters) {
                return Account.getActiveAccount().getStorage().getItem(parameters.getString(1));
            }
        };
        JavaVoidCallback storageSetItem = new JavaVoidCallback() {
            public void invoke(V8Object receiver, V8Array parameters) {
                Account.getActiveAccount().getStorage().setItem(parameters.getString(1), parameters.getString(2));
            }
        };
        JavaVoidCallback storageRemoveItem = new JavaVoidCallback() {
            public void invoke(V8Object receiver, V8Array parameters) {
                Account.getActiveAccount().getStorage().removeItem(parameters.getString(1));
            }
        };
        JavaVoidCallback storageClear = new JavaVoidCallback() {
            public void invoke(V8Object receiver, V8Array parameters) {
                Account.getActiveAccount().getStorage().clear();
            }
        };
        V8Object runtime = getRuntimeVariable();
        runtime.registerJavaMethod(formatNumber, FUNCTION_FORMAT_NUMBER);
        runtime.registerJavaMethod(httpRequest, FUNCTION_HTTP_REQUEST);
        runtime.registerJavaMethod(setTimeout, FUNCTION_SET_TIMEOUT);
        runtime.registerJavaMethod(storageLength, FUNCTION_STORAGE_LENGTH);
        runtime.registerJavaMethod(storageKey, FUNCTION_STORAGE_KEY);
        runtime.registerJavaMethod(storageGetItem, FUNCTION_STORAGE_GET_ITEM);
        runtime.registerJavaMethod(storageSetItem, FUNCTION_STORAGE_SET_ITEM);
        runtime.registerJavaMethod(storageRemoveItem, FUNCTION_STORAGE_REMOVE_ITEM);
        runtime.registerJavaMethod(storageClear, FUNCTION_STORAGE_CLEAR);
        runtime.release();
    }

    private void setupFeatureGeometry(String variableName, Feature feature) {
        Map geojson = null;
        if (feature.hasLocation()) {
            geojson = new GeoJSONPoint (feature.getLocation()).toGeoJSON();
        }
        if (geojson != null) {
            evaluateRuntimeVariableAssignment(variableName, geojson);
        } else {
            assignNullRuntimeVariable(variableName);
        }
    }

    private void setupDateVariable(String variableName, Date date) {
        if (date != null) {
            assignRuntimeVariable(variableName, String.valueOf((long) (((double) date.getTime()) / 1000.0d)));
        } else {
            assignNullRuntimeVariable(variableName);
        }
    }

    private void setupFeatureVariables() {
        Record record = getRecord();
        Feature feature = this.mFeature;
        Project project = record.getProject();
        Account account = record.getAccount();
        assignRuntimeVariable(VARIABLE_RECORD_ID, record.getIdentifier());
        assignRuntimeVariable(VARIABLE_RECORD_STATUS, record.getStatusValue().getDisplayValue());
        setupDateVariable(VARIABLE_RECORD_CLIENT_CREATED_AT, record.getCreatedAt());
        setupDateVariable(VARIABLE_RECORD_CLIENT_UPDATED_AT, record.getUpdatedAt());
        if (project != null) {
            assignRuntimeVariable(VARIABLE_RECORD_PROJECT, project.getRemoteID());
            assignRuntimeVariable(VARIABLE_RECORD_PROJECT_NAME, project.getName());
        } else {
            assignNullRuntimeVariable(VARIABLE_RECORD_PROJECT);
            assignNullRuntimeVariable(VARIABLE_RECORD_PROJECT_NAME);
        }
        setupFeatureGeometry(VARIABLE_RECORD_GEOMETRY, record);
        if (record.hasLocation()) {
            Location loc = record.getLocation();
            if (loc.hasAltitude()) {
                assignRuntimeVariable(VARIABLE_RECORD_ALTITUDE, Double.valueOf(loc.getAltitude()));
            } else {
                assignNullRuntimeVariable(VARIABLE_RECORD_ALTITUDE);
            }
            if (loc.hasAccuracy()) {
                assignRuntimeVariable(VARIABLE_RECORD_HORIZONTAL_ACCURACY, Float.valueOf(loc.getAccuracy()));
            } else {
                assignNullRuntimeVariable(VARIABLE_RECORD_HORIZONTAL_ACCURACY);
            }
        } else {
            assignNullRuntimeVariable(VARIABLE_RECORD_ALTITUDE);
            assignNullRuntimeVariable(VARIABLE_RECORD_HORIZONTAL_ACCURACY);
        }
        assignNullRuntimeVariable(VARIABLE_RECORD_VERTICAL_ACCURACY);
        setupAuditLocation(record.getCreatedLocation(), VARIABLE_RECORD_CREATED_LATITUDE, VARIABLE_RECORD_CREATED_LONGITUDE, VARIABLE_RECORD_CREATED_ALTITUDE, VARIABLE_RECORD_CREATED_ACCURACY);
        setupAuditLocation(record.getUpdatedLocation(), VARIABLE_RECORD_UPDATED_LATITUDE, VARIABLE_RECORD_UPDATED_LONGITUDE, VARIABLE_RECORD_UPDATED_ALTITUDE, VARIABLE_RECORD_UPDATED_ACCURACY);
        setupAuditLocation(feature.getCreatedLocation(), VARIABLE_FEATURE_CREATED_LATITUDE, VARIABLE_FEATURE_CREATED_LONGITUDE, VARIABLE_FEATURE_CREATED_ALTITUDE, VARIABLE_FEATURE_CREATED_ACCURACY);
        setupAuditLocation(feature.getUpdatedLocation(), VARIABLE_FEATURE_UPDATED_LATITUDE, VARIABLE_FEATURE_UPDATED_LONGITUDE, VARIABLE_FEATURE_UPDATED_ALTITUDE, VARIABLE_FEATURE_UPDATED_ACCURACY);
        assignRuntimeVariable(VARIABLE_RECORD_CREATED_DURATION, record.getCreatedDuration());
        assignRuntimeVariable(VARIABLE_RECORD_UPDATED_DURATION, record.getUpdatedDuration());
        assignRuntimeVariable(VARIABLE_RECORD_EDITED_DURATION, record.getEditedDuration());
        assignRuntimeVariable(VARIABLE_FEATURE_CREATED_DURATION, feature.getCreatedDuration());
        assignRuntimeVariable(VARIABLE_FEATURE_UPDATED_DURATION, feature.getUpdatedDuration());
        assignRuntimeVariable(VARIABLE_FEATURE_EDITED_DURATION, feature.getEditedDuration());
        assignRuntimeVariable(VARIABLE_FEATURE_ID, feature.getIdentifier());
        if (feature instanceof RepeatableItemValue) {
            RepeatableItemValue item = (RepeatableItemValue) feature;
            assignRuntimeVariable(VARIABLE_FEATURE_INDEX, Integer.valueOf(item.getIndex()));
            assignRuntimeVariable(VARIABLE_REPEATABLE, item.getElement().getKey());
        } else {
            assignRuntimeVariable(VARIABLE_FEATURE_INDEX, Integer.valueOf(0));
            assignNullRuntimeVariable(VARIABLE_REPEATABLE);
        }
        assignRuntimeVariable(VARIABLE_FEATURE_IS_NEW, Boolean.valueOf(this.mIsNewFeature));
        setupDateVariable(VARIABLE_FEATURE_CREATED_AT, feature.getCreatedAt());
        setupDateVariable(VARIABLE_FEATURE_UPDATED_AT, feature.getUpdatedAt());
        setupFeatureGeometry(VARIABLE_FEATURE_GEOMETRY, feature);
        assignRuntimeVariable(VARIABLE_USER_EMAIL, account.getUserEmailAddress());
        assignRuntimeVariable(VARIABLE_USER_FULL_NAME, account.getUserName());
        assignRuntimeVariable(VARIABLE_USER_ROLE_NAME, account.getRole().getName());
    }

    private void setupAuditLocation(Location location, String latitudeName, String longitudeName, String altitudeName, String accuracyName) {
        if (location != null) {
            assignRuntimeVariable(latitudeName, Double.valueOf(location.getLatitude()));
            assignRuntimeVariable(longitudeName, Double.valueOf(location.getLongitude()));
            if (location.hasAltitude()) {
                assignRuntimeVariable(altitudeName, Double.valueOf(location.getAltitude()));
            } else {
                assignNullRuntimeVariable(altitudeName);
            }
            if (location.hasAccuracy()) {
                assignRuntimeVariable(accuracyName, Float.valueOf(location.getAccuracy()));
                return;
            } else {
                assignNullRuntimeVariable(accuracyName);
                return;
            }
        }
        assignNullRuntimeVariable(latitudeName);
        assignNullRuntimeVariable(longitudeName);
        assignNullRuntimeVariable(altitudeName);
        assignNullRuntimeVariable(accuracyName);
    }

    private void setupLocationVariables() {
        Object obj = null;
        if (this.mCurrentLocation != null) {
            Object valueOf;
            HashMap<String, Object> location = new HashMap();
            location.put(Record.COLUMN_LATITUDE, Double.valueOf(this.mCurrentLocation.getLatitude()));
            location.put(Record.COLUMN_LONGITUDE, Double.valueOf(this.mCurrentLocation.getLongitude()));
            location.put("timestamp", Double.valueOf(((double) this.mCurrentLocation.getTime()) / 1000.0d));
            String str = Record.COLUMN_ALTITUDE;
            if (this.mCurrentLocation.hasAltitude()) {
                valueOf = Double.valueOf(this.mCurrentLocation.getAltitude());
            } else {
                valueOf = null;
            }
            location.put(str, valueOf);
            str = Record.COLUMN_ACCURACY;
            if (this.mCurrentLocation.hasAccuracy()) {
                valueOf = Float.valueOf(this.mCurrentLocation.getAccuracy());
            } else {
                valueOf = null;
            }
            location.put(str, valueOf);
            str = "course";
            if (this.mCurrentLocation.hasBearing()) {
                valueOf = Float.valueOf(this.mCurrentLocation.getBearing());
            } else {
                valueOf = null;
            }
            location.put(str, valueOf);
            String str2 = "speed";
            if (this.mCurrentLocation.hasSpeed()) {
                obj = Float.valueOf(this.mCurrentLocation.getSpeed());
            }
            location.put(str2, obj);
            evaluateRuntimeVariableAssignment(VARIABLE_CURRENT_LOCATION, (Map) location);
            return;
        }
        assignNullRuntimeVariable(VARIABLE_CURRENT_LOCATION);
    }

    private void onError(V8ScriptExecutionException jsx) {
        if (this.mExpressionEngineListener != null) {
            this.mExpressionEngineListener.onExpressionError(jsx.getJSMessage());
        }
    }

    private List<Map<String, String>> getExpressions() {
        if (this.mExpressions != null) {
            return this.mExpressions;
        }
        this.mExpressions = new ArrayList();
        for (CalculatedElement element : this.mCalculatedElements) {
            HashMap<String, String> context = new HashMap();
            context.put(Storage.COLUMN_KEY, element.getKey());
            context.put("dataName", element.getDataName());
            context.put("expression", element.getExpression());
            this.mExpressions.add(context);
        }
        return this.mExpressions;
    }

    public void finishAsyncCallback(final int callbackID, final ArrayList<Object> arguments) {
        getSharedHandler().post(new Runnable() {
            public void run() {
                if (!ExpressionEngine.this.getContext().isReleased()) {
                    ExpressionEngine.this.assignRuntimeVariable(ExpressionEngine.VARIABLE_CALLBACK_ID, Integer.valueOf(callbackID));
                    ExpressionEngine.this.evaluateRuntimeVariableAssignmentFromArray(ExpressionEngine.VARIABLE_CALLBACK_ARGUMENTS, arguments);
                    ArrayList<ExpressionResult> results = new ArrayList();
                    V8Array nativeResults = null;
                    try {
                        nativeResults = (V8Array) ExpressionEngine.this.evaluateFunctionCall(ExpressionEngine.FUNCTION_FINISH_ASYNC);
                        for (int i = 0; i < nativeResults.length(); i++) {
                            V8Object result = nativeResults.getObject(i);
                            results.add(new ExpressionResult(result));
                            result.release();
                        }
                        if (nativeResults != null) {
                            nativeResults.release();
                        }
                    } catch (V8ScriptExecutionException jsx) {
                        JavaScriptResource.logError(String.format("JS Error: %s - %s - %d", new Object[]{jsx.getJSStackTrace(), jsx.getFileName(), Integer.valueOf(jsx.getLineNumber())}));
                        ExpressionEngine.this.onError(jsx);
                        if (nativeResults != null) {
                            nativeResults.release();
                        }
                    } catch (Throwable th) {
                        if (nativeResults != null) {
                            nativeResults.release();
                        }
                    }
                    if (ExpressionEngine.this.mExpressionEngineListener != null) {
                        ExpressionEngine.this.mExpressionEngineListener.onExpressionResults(results);
                    }
                }
            }
        });
    }

    public static Handler getSharedHandler() {
        if (sHandler == null) {
            sHandler = new Handler(Looper.getMainLooper());
        }
        return sHandler;
    }
}
