package com.example.bhumicloud.gisapplications.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.WrapperListAdapter;

import com.example.bhumicloud.gisapplications.R;

import java.util.Collection;
import java.util.HashSet;

public class DynamicListView extends ListView {
    static final /* synthetic */ boolean $assertionsDisabled;
    private static final TypeEvaluator<Rect> sBoundEvaluator = new C12545();
    private final int INVALID_ID = -1;
    private final int INVALID_POINTER_ID = -1;
    private final int LINE_THICKNESS = 4;
    private final int MOVE_DURATION = 150;
    private final int SMOOTH_SCROLL_AMOUNT_AT_EDGE = 100;
    private long mAboveItemId = -1;
    private int mActivePointerId = -1;
    public final AllOnScrollListener mAllOnScrollListener = new AllOnScrollListener();
    private long mBelowItemId = -1;
    private boolean mCellIsMobile = false;
    private int mDownX = -1;
    private int mDownY = -1;
    private boolean mDragAndDropEnabled;
    private int mExtraBottomPadding;
    private boolean mHasExtraBottomPadding;
    private BitmapDrawable mHoverCell;
    private Rect mHoverCellCurrentBounds;
    private Rect mHoverCellOriginalBounds;
    private boolean mIsMobileScrolling = false;
    private boolean mIsWaitingForScrollFinish = false;
    private int mLastEventY = -1;
    private long mMobileItemId = -1;
    private OnItemLongClickListener mOnItemLongClickListener = new C12501();
    private OnScrollListener mScrollListener = new C12556();
    private int mScrollState = 0;
    private int mSmoothScrollAmountAtEdge = 0;
    private int mTotalOffset = 0;
    @IdRes
    private int mTouchViewResId;

    class C12501 implements OnItemLongClickListener {
        C12501() {
        }

        public boolean onItemLongClick(AdapterView<?> adapterView, View arg1, int pos, long id) {
            DynamicListView.this.mTotalOffset = 0;
            int position = DynamicListView.this.pointToPosition(DynamicListView.this.mDownX, DynamicListView.this.mDownY);
            View selectedView = DynamicListView.this.getChildAt(position - DynamicListView.this.getFirstVisiblePosition());
            DynamicListView.this.mMobileItemId = DynamicListView.this.getAdapter().getItemId(position);
            DynamicListView.this.mHoverCell = DynamicListView.this.getAndAddHoverView(selectedView);
            selectedView.setVisibility(4);
            DynamicListView.this.mCellIsMobile = true;
            DynamicListView.this.updateNeighborViewsForID(DynamicListView.this.mMobileItemId);
            return true;
        }
    }

    class C12523 implements AnimatorUpdateListener {
        C12523() {
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            DynamicListView.this.invalidate();
        }
    }

    static class C12545 implements TypeEvaluator<Rect> {
        C12545() {
        }

        public Rect evaluate(float fraction, Rect startValue, Rect endValue) {
            return new Rect (interpolate(startValue.left, endValue.left, fraction), interpolate(startValue.top, endValue.top, fraction), interpolate(startValue.right, endValue.right, fraction), interpolate(startValue.bottom, endValue.bottom, fraction));
        }

        public int interpolate(int start, int end, float fraction) {
            return (int) (((float) start) + (((float) (end - start)) * fraction));
        }
    }

    class C12556 implements OnScrollListener {
        private int mCurrentFirstVisibleItem;
        private int mCurrentScrollState;
        private int mCurrentVisibleItemCount;
        private int mPreviousFirstVisibleItem = -1;
        private int mPreviousVisibleItemCount = -1;

        C12556() {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            this.mCurrentFirstVisibleItem = firstVisibleItem;
            this.mCurrentVisibleItemCount = visibleItemCount;
            this.mPreviousFirstVisibleItem = this.mPreviousFirstVisibleItem == -1 ? this.mCurrentFirstVisibleItem : this.mPreviousFirstVisibleItem;
            this.mPreviousVisibleItemCount = this.mPreviousVisibleItemCount == -1 ? this.mCurrentVisibleItemCount : this.mPreviousVisibleItemCount;
            checkAndHandleFirstVisibleCellChange();
            checkAndHandleLastVisibleCellChange();
            this.mPreviousFirstVisibleItem = this.mCurrentFirstVisibleItem;
            this.mPreviousVisibleItemCount = this.mCurrentVisibleItemCount;
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            this.mCurrentScrollState = scrollState;
            DynamicListView.this.mScrollState = scrollState;
            isScrollCompleted();
        }

        private void isScrollCompleted() {
            if (this.mCurrentVisibleItemCount > 0 && this.mCurrentScrollState == 0) {
                if (DynamicListView.this.mCellIsMobile && DynamicListView.this.mIsMobileScrolling) {
                    DynamicListView.this.handleMobileCellScroll();
                } else if (DynamicListView.this.mIsWaitingForScrollFinish) {
                    DynamicListView.this.touchEventsEnded();
                }
            }
        }

        public void checkAndHandleFirstVisibleCellChange() {
            if (this.mCurrentFirstVisibleItem != this.mPreviousFirstVisibleItem && DynamicListView.this.mCellIsMobile && DynamicListView.this.mMobileItemId != -1) {
                DynamicListView.this.updateNeighborViewsForID(DynamicListView.this.mMobileItemId);
                DynamicListView.this.handleCellSwitch();
            }
        }

        public void checkAndHandleLastVisibleCellChange() {
            if (this.mCurrentFirstVisibleItem + this.mCurrentVisibleItemCount != this.mPreviousFirstVisibleItem + this.mPreviousVisibleItemCount && DynamicListView.this.mCellIsMobile && DynamicListView.this.mMobileItemId != -1) {
                DynamicListView.this.updateNeighborViewsForID(DynamicListView.this.mMobileItemId);
                DynamicListView.this.handleCellSwitch();
            }
        }
    }

    private class AllOnScrollListener implements OnScrollListener {
        private final Collection<OnScrollListener> mOnScrollListeners;

        private AllOnScrollListener() {
            this.mOnScrollListeners = new HashSet ();
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            for (OnScrollListener onScrollListener : this.mOnScrollListeners) {
                onScrollListener.onScrollStateChanged(view, scrollState);
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            for (OnScrollListener onScrollListener : this.mOnScrollListeners) {
                onScrollListener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
            }
        }

        public void addOnScrollListener(OnScrollListener onScrollListener) {
            this.mOnScrollListeners.add(onScrollListener);
        }
    }

    static {
        boolean z;
        if (DynamicListView.class.desiredAssertionStatus()) {
            z = false;
        } else {
            z = true;
        }
        $assertionsDisabled = z;
    }

    public DynamicListView(Context context) {
        super(context);
        init(context);
    }

    public DynamicListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public DynamicListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void init(Context context) {
        super.setOnScrollListener(this.mAllOnScrollListener);
        setOnScrollListener(this.mScrollListener);
        Resources resources = context.getResources();
        this.mSmoothScrollAmountAtEdge = (int) (100.0f / resources.getDisplayMetrics().density);
        this.mExtraBottomPadding = resources.getDimensionPixelSize(R.dimen.layers_extra_bottom_padding);
    }

    private BitmapDrawable getAndAddHoverView(View v) {
        int w = v.getWidth();
        int h = v.getHeight();
        int top = v.getTop();
        int left = v.getLeft();
        BitmapDrawable drawable = new BitmapDrawable (getResources(), getBitmapWithBorder(v));
        this.mHoverCellOriginalBounds = new Rect (left, top, left + w, top + h);
        this.mHoverCellCurrentBounds = new Rect (this.mHoverCellOriginalBounds);
        drawable.setBounds(this.mHoverCellCurrentBounds);
        return drawable;
    }

    private Bitmap getBitmapWithBorder(View v) {
        Bitmap bitmap = getBitmapFromView(v);
        Canvas can = new Canvas (bitmap);
        Rect rect = new Rect (0, 0, bitmap.getWidth(), bitmap.getHeight());
        Paint paint = new Paint ();
        paint.setStyle( Style.STROKE);
        paint.setStrokeWidth(4.0f);
        paint.setColor( ContextCompat.getColor(getContext(), R.color.black_12_opacity));
        can.drawBitmap(bitmap, 0.0f, 0.0f, null);
        can.drawRect(rect, paint);
        return bitmap;
    }

    private Bitmap getBitmapFromView(View v) {
        Bitmap bitmap = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Config.ARGB_8888);
        v.draw(new Canvas (bitmap));
        return bitmap;
    }

    private void updateNeighborViewsForID(long itemID) {
        int position = getPositionForID(itemID);
        ListAdapter adapter = getAdapter();
        this.mAboveItemId = adapter.getItemId(position - 1);
        this.mBelowItemId = adapter.getItemId(position + 1);
    }

    public View getViewForID(long itemID) {
        int firstVisiblePosition = getFirstVisiblePosition();
        ListAdapter adapter = getAdapter();
        for (int i = 0; i < getChildCount(); i++) {
            View v = getChildAt(i);
            if (adapter.getItemId(firstVisiblePosition + i) == itemID) {
                return v;
            }
        }
        return null;
    }

    public int getPositionForID(long itemID) {
        View v = getViewForID(itemID);
        if (v == null) {
            return -1;
        }
        return getPositionForView(v);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        boolean canScrollUp = canScrollVertically(-1);
        if (canScrollVertically(1) || (canScrollUp && !this.mHasExtraBottomPadding)) {
            setPadding(0, 0, 0, 0);
            setClipToPadding(true);
            this.mHasExtraBottomPadding = false;
        } else {
            setPadding(0, 0, 0, this.mExtraBottomPadding);
            setClipToPadding(false);
            this.mHasExtraBottomPadding = true;
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    protected void dispatchDraw(@NonNull Canvas canvas) {
        super.dispatchDraw(canvas);
        if (this.mHoverCell != null) {
            this.mHoverCell.draw(canvas);
        }
    }

    public boolean onTouchEvent(@NonNull MotionEvent event) {
        switch (event.getAction() & 255) {
            case 0:
                this.mDownX = (int) event.getX();
                this.mDownY = (int) event.getY();
                this.mActivePointerId = event.getPointerId(0);
                int position = pointToPosition((int) event.getX(), (int) event.getY());
                if (position != -1 && this.mDragAndDropEnabled) {
                    View downView = getChildAt(position - getFirstVisiblePosition());
                    if ($assertionsDisabled || downView != null) {
                        if (onDraggable(downView, event.getX() - downView.getX(), event.getY() - downView.getY())) {
                            setUpDrag();
                            break;
                        }
                    }
                    throw new AssertionError ();
                }
                break;
            case 1:
                touchEventsEnded();
                break;
            case 2:
                if (this.mActivePointerId != -1) {
                    this.mLastEventY = (int) event.getY(event.findPointerIndex(this.mActivePointerId));
                    int deltaY = this.mLastEventY - this.mDownY;
                    if (this.mCellIsMobile) {
                        this.mHoverCellCurrentBounds.offsetTo(this.mHoverCellOriginalBounds.left, (this.mHoverCellOriginalBounds.top + deltaY) + this.mTotalOffset);
                        this.mHoverCell.setBounds(this.mHoverCellCurrentBounds);
                        invalidate();
                        handleCellSwitch();
                        this.mIsMobileScrolling = false;
                        handleMobileCellScroll();
                        return false;
                    }
                }
                break;
            case 3:
                touchEventsCancelled();
                break;
            case 6:
                if (event.getPointerId((event.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8) == this.mActivePointerId) {
                    touchEventsEnded();
                    break;
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    private void handleCellSwitch() {
        final int deltaY = this.mLastEventY - this.mDownY;
        int deltaYTotal = (this.mHoverCellOriginalBounds.top + this.mTotalOffset) + deltaY;
        View belowView = getViewForID(this.mBelowItemId);
        View mobileView = getViewForID(this.mMobileItemId);
        View aboveView = getViewForID(this.mAboveItemId);
        boolean isBelow = belowView != null && deltaYTotal > belowView.getTop();
        boolean isAbove = aboveView != null && deltaYTotal < aboveView.getTop();
        if (isBelow || isAbove) {
            View switchView;
            final long switchItemID = isBelow ? this.mBelowItemId : this.mAboveItemId;
            if (isBelow) {
                switchView = belowView;
            } else {
                switchView = aboveView;
            }
            swapElements(getPositionForView(mobileView), getPositionForView(switchView));
            mobileView.setVisibility(0);
            ((BaseAdapter) getAdapter()).notifyDataSetChanged();
            this.mDownY = this.mLastEventY;
            final int switchViewStartTop = switchView.getTop();
            updateNeighborViewsForID(this.mMobileItemId);
            final ViewTreeObserver observer = getViewTreeObserver();
            observer.addOnPreDrawListener(new OnPreDrawListener () {
                public boolean onPreDraw() {
                    observer.removeOnPreDrawListener(this);
                    View mobileView = DynamicListView.this.getViewForID(DynamicListView.this.mMobileItemId);
                    if (mobileView != null) {
                        mobileView.setVisibility(4);
                    }
                    View switchView = DynamicListView.this.getViewForID(switchItemID);
                    DynamicListView.this.mTotalOffset = DynamicListView.this.mTotalOffset + deltaY;
                    switchView.setTranslationY((float) (switchViewStartTop - switchView.getTop()));
                    ObjectAnimator animator = ObjectAnimator.ofFloat(switchView, View.TRANSLATION_Y, new float[]{0.0f});
                    animator.setDuration(150);
                    animator.start();
                    return true;
                }
            });
        }
    }

    private void swapElements(int indexOne, int indexTwo) {
        ListAdapter listAdapter = getAdapter();
        if (listAdapter instanceof WrapperListAdapter) {
            listAdapter = ((WrapperListAdapter) listAdapter).getWrappedAdapter();
        }
        if (listAdapter instanceof ArrayAdapter) {
            ArrayAdapter arrayAdapter = (ArrayAdapter) listAdapter;
            Object obj2 = arrayAdapter.getItem(indexTwo);
            arrayAdapter.remove(obj2);
            arrayAdapter.insert(obj2, indexOne);
            return;
        }
        throw new RuntimeException ("DynamicListView can only swap elements using an ArrayAdapter");
    }

    private void touchEventsEnded() {
        final View mobileView = getViewForID(this.mMobileItemId);
        if (this.mCellIsMobile || this.mIsWaitingForScrollFinish) {
            this.mCellIsMobile = false;
            this.mIsWaitingForScrollFinish = false;
            this.mIsMobileScrolling = false;
            this.mActivePointerId = -1;
            if (this.mScrollState != 0) {
                this.mIsWaitingForScrollFinish = true;
                return;
            }
            this.mHoverCellCurrentBounds.offsetTo(this.mHoverCellOriginalBounds.left, mobileView.getTop());
            ObjectAnimator hoverViewAnimator = ObjectAnimator.ofObject(this.mHoverCell, "bounds", sBoundEvaluator, new Object[]{this.mHoverCellCurrentBounds});
            hoverViewAnimator.addUpdateListener(new C12523());
            hoverViewAnimator.addListener(new AnimatorListenerAdapter () {
                public void onAnimationStart(Animator animation) {
                    DynamicListView.this.setEnabled(false);
                }

                public void onAnimationEnd(Animator animation) {
                    DynamicListView.this.mAboveItemId = -1;
                    DynamicListView.this.mMobileItemId = -1;
                    DynamicListView.this.mBelowItemId = -1;
                    mobileView.setVisibility(0);
                    DynamicListView.this.mHoverCell = null;
                    DynamicListView.this.setEnabled(true);
                    DynamicListView.this.invalidate();
                }
            });
            hoverViewAnimator.start();
            return;
        }
        touchEventsCancelled();
    }

    private void touchEventsCancelled() {
        View mobileView = getViewForID(this.mMobileItemId);
        if (this.mCellIsMobile) {
            this.mAboveItemId = -1;
            this.mMobileItemId = -1;
            this.mBelowItemId = -1;
            mobileView.setVisibility(0);
            this.mHoverCell = null;
            invalidate();
        }
        this.mCellIsMobile = false;
        this.mIsMobileScrolling = false;
        this.mActivePointerId = -1;
    }

    private void handleMobileCellScroll() {
        this.mIsMobileScrolling = handleMobileCellScroll(this.mHoverCellCurrentBounds);
    }

    public boolean handleMobileCellScroll(Rect r) {
        int offset = computeVerticalScrollOffset();
        int height = getHeight();
        int extent = computeVerticalScrollExtent();
        int range = computeVerticalScrollRange();
        int hoverViewTop = r.top;
        int hoverHeight = r.height();
        if (hoverViewTop <= 0 && offset > 0) {
            smoothScrollBy(-this.mSmoothScrollAmountAtEdge, 0);
            return true;
        } else if (hoverViewTop + hoverHeight < height || offset + extent >= range) {
            return false;
        } else {
            smoothScrollBy(this.mSmoothScrollAmountAtEdge, 0);
            return true;
        }
    }

    public void setOnScrollListener(OnScrollListener onScrollListener) {
        this.mAllOnScrollListener.addOnScrollListener(onScrollListener);
    }

    public void setDragViewResId(@IdRes int touchViewResId) {
        this.mTouchViewResId = touchViewResId;
    }

    public void setDragAndDropEnabled(boolean enabled) {
        this.mDragAndDropEnabled = enabled;
    }

    private void setUpDrag() {
        this.mTotalOffset = 0;
        int position = pointToPosition(this.mDownX, this.mDownY);
        View selectedView = getChildAt(position - getFirstVisiblePosition());
        this.mMobileItemId = getAdapter().getItemId(position);
        this.mHoverCell = getAndAddHoverView(selectedView);
        selectedView.setVisibility(View.INVISIBLE);
        this.mCellIsMobile = true;
        updateNeighborViewsForID(this.mMobileItemId);
    }

    public boolean onDraggable(@NonNull View rowView, float x, float y) {
        View touchView = rowView.findViewById(this.mTouchViewResId);
        if (touchView == null) {
            return false;
        }
        boolean xHit;
        float[] p = getPositions(new float[4], rowView, touchView, touchView.getWidth(), touchView.getHeight());
        if (p[0] > x || p[1] < x) {
            xHit = false;
        } else {
            xHit = true;
        }
        boolean yHit;
        if (p[2] > y || p[3] < y) {
            yHit = false;
        } else {
            yHit = true;
        }
        if (xHit && yHit) {
            return true;
        }
        return false;
    }

    private float[] getPositions(float[] p, View rowView, View touchView, int width, int height) {
        if (touchView == rowView) {
            p[1] = p[0] + ((float) width);
            p[3] = p[2] + ((float) height);
            return p;
        }
        p[0] = p[0] + ((float) touchView.getLeft());
        p[2] = p[2] + ((float) touchView.getTop());
        return getPositions(p, rowView, (View) touchView.getParent(), width, height);
    }
}
