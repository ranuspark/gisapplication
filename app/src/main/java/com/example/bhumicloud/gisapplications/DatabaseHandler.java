package com.example.bhumicloud.gisapplications;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by smrit on 7/22/2017.
 */
public class DatabaseHandler extends SQLiteOpenHelper {


    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "contactsManager";


    // Contacts table name
    private static final String TABLE_CONTACTS = "contacts";
    private static final String TABLE_FlOORINFO = "FloorInformation";
    private static final String TABLE_FormData = "formdummydata";
    private static final String TABLE_FormElements = "formelements";
    private static final String TABLE_FormIMAGES = "formimages";
    private static final String TABLE_FormParentData = "formparentdata";
    private static final String TABLE_FormParentSaveData = "formparentsave";
    private static final String TABLE_FormParentConditional = "formparent_conditional";
    private static final String TABLE_FormConditional = "form_conditional";
    private static final String TABLE_FormSaveData = "formsavedata";
    private static final String TABLE_FormProject = "formproject";


    // Contacts Table Columns names
    private static final String KEY_ID = "dataid";
    private static final String KEY_USER = "User_id";
    private static final String KEY_PROJECT = "Project_Id";
    private static final String KEY_FORM = "Form_Id";
    private static final String KEY_FIELD = "fieldId";
    private static final String KEY_VALUE = "Value";
    private static final String KEY_FLAG = "Flag";
    private static final String KEY_IMAGENAMME = "imageName";
    private static final String KEY_IMAGEPATH = "imagepath";
    private static final String KEY_LATLONG= "latLong";
    private static final String KEY_SID = "s_id";
    private static final String KEY_PID = "p_id";
    private static final String KEY_SAVEID = "save_id";
    private static final String KEY_GROUPID= "group_id";


    private static final String KEY_ELEID= "eleId";
    private static final String KEY_ORDERID= "Order_ID";
    private static final String KEY_ELEMENTNAME= "Element_Name";
    private static final String KEY_GRPID= "Group_id";
    private static final String KEY_DROPORDER= "Drop_value_Order";
    private static final String KEY_DROPNAME= "Drop_value_Name";
    private static final String KEY_SHOw_INSEARCH= "show_in_search";
    private static final String KEY_SHOwPARENT= "show_parent";
    private static final String KEY_CONLOGIC= "condition_logic";



    private static final String KEY_InputType= "input_type";


    private static final String KEY_IMGID= "imgid";
    private static final String KEY_Imgval= "Value";
    private static final String KEY_CURRLOC= "curr_loc";

    private static final String KEY_ParentID= "p_id";

    private static final String KEY_id= "id";
    private static final String KEY_Projectid= "project_id";
    private static final String KEY_fieldid= "fieldid";
    private static final String KEY_values1= "values1";
    private static final String KEY_fieldaction= "field_action";
    private static final String KEY_flagmode= "flag";

    private static final String KEY_Date= "Date";

    private static final String KEY_SaveFormId= "Save_formid";

    private static final String KEY_Projectuserid= "userid";
    private static final String KEY_Projectname= "name";
    private static final String KEY_Projectdescription= "description";
    private static final String KEY_Projectdate1= "date1";
    private static final String KEY_Projectstatus= "status";

    private static final String KEY_Dummyimage= "dummy_img";



    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {



        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_USER + " INTEGER,"
                + KEY_PROJECT + " INTEGER," + KEY_FORM + " INTEGER,"
                + KEY_FIELD + " INTEGER," +  KEY_VALUE + " TEXT," +  KEY_FLAG + " TEXT," + KEY_IMAGENAMME + " TEXT," + KEY_IMAGEPATH + " TEXT," + KEY_SaveFormId + " INTEGER," + KEY_LATLONG + " TEXT);";
        db.execSQL(CREATE_CONTACTS_TABLE);



        String CREATE_FLOOR_TABLE = "CREATE TABLE " + TABLE_FlOORINFO + "("
                + KEY_SID + " INTEGER PRIMARY KEY," + KEY_USER + " INTEGER,"
                + KEY_PID + " INTEGER," + KEY_VALUE + " TEXT,"
                + KEY_PROJECT + " INTEGER," +  KEY_FORM + " INTEGER," +  KEY_SAVEID + " INTEGER," + KEY_GROUPID + " INTEGER," + KEY_SaveFormId + " INTEGER," + KEY_FLAG + " TEXT);";
        db.execSQL(CREATE_FLOOR_TABLE);




        String CREATE_FORMDATA_TABLE = "CREATE TABLE " + TABLE_FormData + "("
                + KEY_SID + " INTEGER PRIMARY KEY," + KEY_FIELD + " INTEGER," + KEY_PROJECT + " INTEGER,"
                + KEY_FORM + " INTEGER," + KEY_ELEID + " INTEGER,"
                + KEY_ORDERID + " INTEGER," +  KEY_ELEMENTNAME + " TEXT," +  KEY_GRPID + " INTEGER," + KEY_DROPORDER + " INTEGER,"
                + KEY_DROPNAME + " TEXT," + KEY_SHOw_INSEARCH + " INTEGER," + KEY_SHOwPARENT + " INTEGER," + KEY_CONLOGIC + " INTEGER);";
        db.execSQL(CREATE_FORMDATA_TABLE);



        String CREATE_FORMELEMENT_TABLE = "CREATE TABLE " + TABLE_FormElements + "("
                + KEY_SID + " INTEGER PRIMARY KEY," + KEY_ELEID + " INTEGER," + KEY_InputType + " TEXT);";
        db.execSQL(CREATE_FORMELEMENT_TABLE);





        String CREATE_FORMIMAGE_TABLE = "CREATE TABLE " + TABLE_FormIMAGES + "("
                + KEY_IMGID + " INTEGER PRIMARY KEY," + KEY_PROJECT + " INTEGER,"
                + KEY_FORM + " INTEGER," + KEY_USER + " INTEGER,"
                + KEY_Imgval + " TEXT," + KEY_Dummyimage + " TEXT," + KEY_CURRLOC + " TEXT);";
        db.execSQL(CREATE_FORMIMAGE_TABLE);




        String CREATE_FORMPARENTDATA_TABLE = "CREATE TABLE " + TABLE_FormParentData + "("
                + KEY_SID + " INTEGER PRIMARY KEY," + KEY_ParentID + " INTEGER," + KEY_PROJECT + " INTEGER,"
                + KEY_ELEID + " INTEGER," + KEY_ORDERID + " INTEGER," +  KEY_ELEMENTNAME + " TEXT," +  KEY_GRPID + " INTEGER," + KEY_DROPORDER + " INTEGER,"
                + KEY_DROPNAME + " TEXT," + KEY_CONLOGIC + " INTEGER);";
        db.execSQL(CREATE_FORMPARENTDATA_TABLE);




        String CREATE_FORMPARENTSAVEDATA_TABLE = "CREATE TABLE " + TABLE_FormParentSaveData + "("
                + KEY_SID + " INTEGER PRIMARY KEY," + KEY_USER + " INTEGER,"
                + KEY_ParentID + " INTEGER," + KEY_Imgval + " TEXT," +  KEY_PROJECT + " INTEGER," +  KEY_FORM + " INTEGER," + KEY_SAVEID + " INTEGER,"
                + KEY_GROUPID + " INTEGER);";
        db.execSQL(CREATE_FORMPARENTSAVEDATA_TABLE);



        String CREATE_FORMPARENTCONDITIONAL_TABLE = "CREATE TABLE " + TABLE_FormParentConditional + "("
                + KEY_id + " INTEGER PRIMARY KEY," + KEY_Projectid + " INTEGER,"
                + KEY_GRPID + " INTEGER," + KEY_fieldid + " INTEGER," +  KEY_values1 + " TEXT," +  KEY_fieldaction + " INTEGER,"
                + KEY_flagmode + " INTEGER);";
        db.execSQL(CREATE_FORMPARENTCONDITIONAL_TABLE);



        String CREATE_FORMCONDITIONAL_TABLE = "CREATE TABLE " + TABLE_FormConditional + "("
                + KEY_id + " INTEGER PRIMARY KEY," + KEY_Projectid + " INTEGER,"
                + KEY_fieldid + " INTEGER," +  KEY_values1 + " TEXT," +  KEY_fieldaction + " INTEGER,"
                + KEY_flagmode + " INTEGER);";
        db.execSQL(CREATE_FORMCONDITIONAL_TABLE);




        String CREATE_FORMSAVEDATA_TABLE = "CREATE TABLE " + TABLE_FormSaveData + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_USER + " INTEGER,"
                + KEY_FIELD + " INTEGER," + KEY_Imgval + " TEXT," +  KEY_PROJECT + " INTEGER," +  KEY_FORM + " INTEGER,"
                + KEY_Date + " TEXT);";
        db.execSQL(CREATE_FORMSAVEDATA_TABLE);


        String CREATE_FORMPROJECTDATA_TABLE = "CREATE TABLE " + TABLE_FormProject + "("
                + KEY_SID + " INTEGER PRIMARY KEY," + KEY_id + " INTEGER,"
                + KEY_Projectuserid + " TEXT," + KEY_Projectname + " TEXT," +  KEY_Projectdescription + " TEXT," +  KEY_Projectdate1 + " TEXT,"
                + KEY_Projectstatus + " INTEGER);";
        db.execSQL(CREATE_FORMPROJECTDATA_TABLE);





    }


    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FlOORINFO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FormData);
        // Create tables again
        onCreate(db);
    }




    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */



    ////////////////////////////////////Getlast Row id/////////////////


    public List<FormSaveDetails> getLastrowid(int userid) {
        List<FormSaveDetails> floortList = new ArrayList<FormSaveDetails>();

        //  String selectQuery = "SELECT  * FROM " + TABLE_FlOORINFO;

        String selectQuery = "SELECT dataid from contacts WHERE User_id = '" + userid + "' order by dataid DESC limit 1";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setID(cursor.getInt(0));
                floortList.add(formsave);
            } while (cursor.moveToNext());
        }
        return floortList;
    }


    public List<FormSaveDetails> getLastprojectid(int rowid) {
        List<FormSaveDetails> floortList = new ArrayList<FormSaveDetails>();

        //  String selectQuery = "SELECT  * FROM " + TABLE_FlOORINFO;

        String selectQuery = "SELECT Project_Id FROM `contacts` WHERE dataid = '" + rowid + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setprojectID(cursor.getInt(0));
                floortList.add(formsave);
            } while (cursor.moveToNext());
        }
        return floortList;
    }



    ////////////////////////////////////Getlast Row id/////////////////










    //********************** formProject





    void addFormProject(FormSaveDetails formsave) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_id, formsave.getuserprojectId());
        values.put(KEY_Projectuserid, formsave.getProjectuserid());
        values.put(KEY_Projectname, formsave.getProjectname());
        values.put(KEY_Projectdescription, formsave.getProjectdesc());
        values.put(KEY_Projectdate1, formsave.getProjectdate());
        values.put(KEY_Projectstatus, formsave.getProjectstatus());

        // Inserting Row
        db.insert(TABLE_FormProject, null, values);
        db.close(); // Closing database connection
    }


    public void deleteFormFormproject() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "DELETE FROM " + TABLE_FormProject;
        /*SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });*/
        db.execSQL(selectQuery);
        db.close();
    }



    public List<FormSaveDetails> GetprojectId(int Userid) {

        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();

        // Log.d("mainidddd: ", String.valueOf(fieldid));
        //  String selectQuery = "SELECT * FROM `formparent_conditional` WHERE fieldid = '" + fieldid + "' AND project_id='10' AND Group_id = '" + groupId + "'";
        //String selectQuery = "select * from `formproject` where FIND_IN_SET('" + Userid + "',`userid`) AND status='1'";

        String selectQuery = "select * from `formproject` where  (',' || userid || ',') LIKE '%," + Userid + ",%' AND status='1'";

        // String selectQuery = "SELECT * FROM `form_conditional` WHERE fieldid='23'  AND project_id='10'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setuserprojectId(cursor.getInt(1));
                contactList.add(formsave);

            } while (cursor.moveToNext());

        }

        return contactList;
    }


    public List<FormSaveDetails> GetMultiprojectId(int Userid) {

        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();

        // Log.d("mainidddd: ", String.valueOf(fieldid));
        //  String selectQuery = "SELECT * FROM `formparent_conditional` WHERE fieldid = '" + fieldid + "' AND project_id='10' AND Group_id = '" + groupId + "'";
        String selectQuery = "select * from `formproject` where FIND_IN_SET('" + Userid + "',`userid`)";

        // String selectQuery = "SELECT * FROM `form_conditional` WHERE fieldid='23'  AND project_id='10'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setuserprojectId(cursor.getInt(1));
                contactList.add(formsave);

            } while (cursor.moveToNext());

        }

        return contactList;
    }







    //********************** formProject












    ///////////// Show form to edit/////////


    public List<FormSaveDetails> ShowformtoEdit(int projectid,int formid) {

        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();


        String selectQuery = "SELECT Fe.condition_logic,Fe.show_parent,Fd.dataid,Fd.fieldId,Fd.Value,Fe.eleId,Fe.Order_ID,Fe.Element_Name,Fe.Group_id,Fe.Drop_value_Order,Fe.Drop_value_Name,Fee.input_type FROM formsavedata Fd INNER JOIN formdummydata Fe ON Fd.fieldId = Fe.fieldId INNER JOIN formelements Fee ON Fe.eleId = Fee.eleId WHERE Fd.Project_Id = '" + projectid + "' and Fd.Form_Id = '" + formid + "' Order by Fe.Order_ID";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setconlogic(Integer.parseInt(cursor.getString(0)));
                formsave.setshowparent(Integer.parseInt(cursor.getString(1)));
                formsave.setID(Integer.parseInt(cursor.getString(2)));
                formsave.setfieldID(cursor.getInt(3));
                formsave.setValue(cursor.getString(4));
                formsave.seteleId(cursor.getInt(5));
                formsave.setorderid(cursor.getInt(6));
                formsave.setelename(cursor.getString(7));
                formsave.setgrpid(cursor.getInt(8));
                formsave.setDroporder(cursor.getInt(9));
                formsave.setDropname(cursor.getString(10));
                formsave.setFormElement(cursor.getString(11));
                contactList.add(formsave);

            } while (cursor.moveToNext());

        }

        return contactList;
    }



    public List<FormSaveDetails> SetMaxparentId(int projectid,int formid) {

        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();

        // Log.d("mainidddd: ", String.valueOf(fieldid));
        //  String selectQuery = "SELECT * FROM `formparent_conditional` WHERE fieldid = '" + fieldid + "' AND project_id='10' AND Group_id = '" + groupId + "'";
        String selectQuery = "SELECT max(save_id) as save_id FROM `formparentsave` WHERE Project_Id = '" + projectid + "' AND Form_Id = '" + formid + "'";

        // String selectQuery = "SELECT * FROM `form_conditional` WHERE fieldid='23'  AND project_id='10'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setSaveID(cursor.getInt(0));
                contactList.add(formsave);

            } while (cursor.moveToNext());

        }

        return contactList;
    }



    public List<FormSaveDetails> Getformimages(int projectid,int formid) {

        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();

        // Log.d("mainidddd: ", String.valueOf(fieldid));
        //  String selectQuery = "SELECT * FROM `formparent_conditional` WHERE fieldid = '" + fieldid + "' AND project_id='10' AND Group_id = '" + groupId + "'";
        String selectQuery = "SELECT * FROM formimages WHERE Project_Id = '" + projectid + "' and Form_Id = '" + formid + "'";

        // String selectQuery = "SELECT * FROM `form_conditional` WHERE fieldid='23'  AND project_id='10'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setimgid(cursor.getInt(0));
                formsave.setprojectID(cursor.getInt(1));
                formsave.setFormID(cursor.getInt(2));
                formsave.setuserID(cursor.getInt(3));
                formsave.setValue(cursor.getString(4));
                formsave.setdummyimg(cursor.getString(5));
                formsave.setcurrloc(cursor.getString(6));
                contactList.add(formsave);

            } while (cursor.moveToNext());

        }

        return contactList;
    }





    public List<FormSaveDetails> Showparentlist(int projectid,int formid,int groupid) {

        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();

        // Log.d("mainidddd: ", String.valueOf(fieldid));
        //  String selectQuery = "SELECT * FROM `formparent_conditional` WHERE fieldid = '" + fieldid + "' AND project_id='10' AND Group_id = '" + groupId + "'";
        String selectQuery = "SELECT * FROM formparentsave WHERE Project_Id = '" + projectid + "' and Form_Id = '" + formid + "' and group_id = '" + groupid + "'";

        // String selectQuery = "SELECT * FROM `form_conditional` WHERE fieldid='23'  AND project_id='10'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setSaveID(cursor.getInt(6));
                contactList.add(formsave);

            } while (cursor.moveToNext());

        }

        return contactList;
    }





    public List<FormSaveDetails> ShowAddedParentform(int projectid,int FormId,int saveId,int groupId) {

        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();


        String selectQuery = "SELECT Fp.condition_logic,Fd.s_id,Fd.p_id,Fd.Value,Fd.save_id,Fd.group_id,Fe.input_type,Fp.eleId,Fp.Order_ID,Fp.Element_Name,Fp.Group_id,Fp.Drop_value_Order,Fp.Drop_value_Name FROM formparentsave Fd INNER JOIN formparentdata Fp ON Fd.p_id = Fp.p_id INNER JOIN formelements Fe ON Fp.eleId = Fe.eleId WHERE Fd.Project_Id = '" + projectid + "' and Fd.Form_Id = '" + FormId + "' and Fd.save_id = '" + saveId + "' and Fd.group_id = '" + groupId + "' Order by Fp.Order_ID";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setconlogic(Integer.parseInt(cursor.getString(0)));
                formsave.setSID(Integer.parseInt(cursor.getString(1)));
                formsave.setParentid(Integer.parseInt(cursor.getString(2)));
                formsave.setValue(cursor.getString(3));
                formsave.setSaveID(cursor.getInt(4));
                formsave.setGroupID(cursor.getInt(5));
                formsave.setFormElement(cursor.getString(6));
                formsave.seteleId(cursor.getInt(7));
                formsave.setorderid(cursor.getInt(8));
                formsave.setelename(cursor.getString(9));
                formsave.setgrpid(cursor.getInt(10));
                formsave.setDroporder(cursor.getInt(11));
                formsave.setDropname(cursor.getString(12));

                contactList.add(formsave);

            } while (cursor.moveToNext());

        }

        return contactList;
    }






    ///////////// Show form to edit/////////







    ///////////// Show form to add/////////


            public List<FormSaveDetails> ShowformtoAdd(int projectid) {

                List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();


                String selectQuery = "SELECT Fd.condition_logic,Fd.show_parent,Fd.fieldId,Fd.Project_Id,Fd.Form_Id,Fd.eleId,Fd.Order_ID,Fd.Element_Name,Fd.Group_id,Fd.Drop_value_Order,Fd.Drop_value_Name,Fe.input_type FROM formdummydata Fd INNER JOIN formelements Fe ON Fd.eleId = Fe.eleId WHERE Fd.Project_Id = '" + projectid + "' Order by Fd.Order_ID";

                  SQLiteDatabase db = this.getWritableDatabase();
                Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setconlogic(Integer.parseInt(cursor.getString(0)));
                formsave.setshowparent(Integer.parseInt(cursor.getString(1)));
                formsave.setfieldID(cursor.getInt(2));
                formsave.setprojectID(cursor.getInt(3));
                formsave.setFormID(cursor.getInt(4));
                formsave.seteleId(cursor.getInt(5));
                formsave.setorderid(cursor.getInt(6));
                formsave.setelename(cursor.getString(7));
                formsave.setgrpid(cursor.getInt(8));
                formsave.setDroporder(cursor.getInt(9));
                formsave.setDropname(cursor.getString(10));
                formsave.setFormElement(cursor.getString(11));
                contactList.add(formsave);

            } while (cursor.moveToNext());

        }

        return contactList;
    }



    public List<FormSaveDetails> Showformconditional(int fieldid,int projectid) {

        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();

        Log.d("mainidddd: ", String.valueOf(fieldid));
       String selectQuery = "SELECT * FROM `form_conditional` WHERE fieldid = '" + fieldid + "' AND project_id = '" + projectid + "'";
       // String selectQuery = "SELECT * FROM `form_conditional` WHERE fieldid='23'  AND project_id='10'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setParentprojectid(Integer.parseInt(cursor.getString(1)));
                formsave.setParentfieldid(Integer.parseInt(cursor.getString(2)));
                formsave.setParentconvalues(cursor.getString(3));
                formsave.setfieldaction(cursor.getInt(4));
                formsave.setflagmode(cursor.getInt(5));
                contactList.add(formsave);

            } while (cursor.moveToNext());

        }

        return contactList;
    }



    public List<FormSaveDetails> ShowParentform(int groupId,int projectid) {

        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();


        String selectQuery = "SELECT Fd.condition_logic,Fd.p_id,Fd.Project_Id,Fd.eleId,Fd.Order_ID,Fd.Element_Name,Fd.Group_id,Fd.Drop_value_Order,Fd.Drop_value_Name,Fe.input_type FROM formparentdata Fd INNER JOIN formelements Fe ON Fd.eleId = Fe.eleId WHERE Fd.Project_Id = '" + projectid + "' and Fd.Group_id = '" + groupId + "' Order by Fd.Order_ID";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setconlogic(Integer.parseInt(cursor.getString(0)));
                formsave.setParentid(Integer.parseInt(cursor.getString(1)));
                formsave.setprojectID(cursor.getInt(2));
                formsave.seteleId(cursor.getInt(3));
                formsave.setorderid(cursor.getInt(4));
                formsave.setelename(cursor.getString(5));
                formsave.setgrpid(cursor.getInt(6));
                formsave.setDroporder(cursor.getInt(7));
                formsave.setDropname(cursor.getString(8));
                formsave.setFormElement(cursor.getString(9));
                contactList.add(formsave);

            } while (cursor.moveToNext());

        }

        return contactList;
    }



    public List<FormSaveDetails> ShowParentconditional(int fieldid,int groupId,int projectid) {

        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();

       // Log.d("mainidddd: ", String.valueOf(fieldid));
        String selectQuery = "SELECT * FROM `formparent_conditional` WHERE fieldid = '" + fieldid + "' AND project_id = '" + projectid + "' AND Group_id = '" + groupId + "'";
        // String selectQuery = "SELECT * FROM `form_conditional` WHERE fieldid='23'  AND project_id='10'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setParentprojectid(Integer.parseInt(cursor.getString(1)));
                formsave.setgrpid(Integer.parseInt(cursor.getString(2)));
                formsave.setParentfieldid(Integer.parseInt(cursor.getString(3)));
                formsave.setParentconvalues(cursor.getString(4));
                formsave.setfieldaction(cursor.getInt(5));
                formsave.setflagmode(cursor.getInt(6));
                contactList.add(formsave);

            } while (cursor.moveToNext());

        }

        return contactList;
    }





    public List<FormSaveDetails> ShowSaveFormId(int projectid) {

        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();

        // Log.d("mainidddd: ", String.valueOf(fieldid));
      //  String selectQuery = "SELECT * FROM `formparent_conditional` WHERE fieldid = '" + fieldid + "' AND project_id='10' AND Group_id = '" + groupId + "'";
        String selectQuery = "SELECT max(Save_formid) as Save_formid FROM `contacts` WHERE Project_Id = '" + projectid + "' AND Flag='insert'";

        // String selectQuery = "SELECT * FROM `form_conditional` WHERE fieldid='23'  AND project_id='10'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setsavformid(cursor.getInt(0));
                contactList.add(formsave);

            } while (cursor.moveToNext());

        }

        return contactList;
    }






    public List<FormSaveDetails> SearchsavedForm(String searchval,int projectid) {

        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();

        // Log.d("mainidddd: ", String.valueOf(fieldid));
        String selectQuery = "SELECT * FROM formsavedata WHERE Project_Id = '" + projectid + "' AND Value LIKE '%" + searchval + "%'";
        // String selectQuery = "SELECT * FROM `form_conditional` WHERE fieldid='23'  AND project_id='10'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        int cnt = cursor.getCount();

        if (cnt > 0)
        {

            if (cursor.moveToFirst()) {
                do {
                    FormSaveDetails formsave = new FormSaveDetails();
                    formsave.setID(cursor.getInt(0));
                    formsave.setuserID(cursor.getInt(1));
                    formsave.setfieldID(cursor.getInt(2));
                    formsave.setValue(cursor.getString(3));
                    formsave.setprojectID(cursor.getInt(4));
                    formsave.setFormID(cursor.getInt(5));
                    contactList.add(formsave);

                } while (cursor.moveToNext());

            }
    }
   /* else{

    }*/

        return contactList;
    }




    public List<FormSaveDetails> FormElementname(int projectid) {

        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();

        // Log.d("mainidddd: ", String.valueOf(fieldid));
        String selectQuery = "SELECT Element_Name,fieldId FROM formdummydata WHERE Project_Id = '" + projectid + "' AND show_in_search='1'";
        // String selectQuery = "SELECT * FROM `form_conditional` WHERE fieldid='23'  AND project_id='10'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


            if (cursor.moveToFirst()) {
                do {
                    FormSaveDetails formsave = new FormSaveDetails();
                    formsave.setelename(cursor.getString(0));
                    formsave.setfieldID(cursor.getInt(1));
                    contactList.add(formsave);

                } while (cursor.moveToNext());

            }

   /* else{

    }*/

        return contactList;
    }



    public List<FormSaveDetails> SearchformValue(int userid,int projectid,int formid,int fieldid) {

        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();

        // Log.d("mainidddd: ", String.valueOf(fieldid));
        String selectQuery = "SELECT Value FROM formsavedata WHERE Project_Id = '" + projectid + "' AND Form_Id = '" + formid + "' AND fieldId = '" + fieldid + "'";
        // String selectQuery = "SELECT * FROM `form_conditional` WHERE fieldid='23'  AND project_id='10'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setValue(cursor.getString(0));
                contactList.add(formsave);

            } while (cursor.moveToNext());

        }

   /* else{

    }*/

        return contactList;
    }













    ///////////// Show form to add/////////










    //********************** formsavedata



    void addFormSaveData(FormSaveDetails formsave) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        ContentValues values = new ContentValues();
        values.put(KEY_USER, formsave.getuserID());
        values.put(KEY_FIELD, formsave.getfiledID());
        values.put(KEY_Imgval, formsave.getFormimgval());
        values.put(KEY_PROJECT, formsave.getProjectId());
        values.put(KEY_FORM, formsave.getFormId());
        values.put(KEY_Date, formsave.getdate());

        // Inserting Row
        db.insert(TABLE_FormSaveData, null, values);
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close(); // Closing database connection
    }


    public void deleteFormSavedata() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "DELETE FROM " + TABLE_FormSaveData;
        /*SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });*/
        db.execSQL(selectQuery);
        db.close();
    }





    //********************** formsavedata













    //********************** formConditional



    void addFormConditional(FormSaveDetails formsave) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_Projectid, formsave.getParentprojectid());
        values.put(KEY_fieldid, formsave.getParentfieldid());
        values.put(KEY_values1, formsave.getParentconvalues());
        values.put(KEY_fieldaction, formsave.getfieldaction());
        values.put(KEY_flagmode, formsave.getflagmode());

        // Inserting Row
        db.insert(TABLE_FormConditional, null, values);
        db.close(); // Closing database connection
    }


    public void deleteFormConditional() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "DELETE FROM " + TABLE_FormConditional;
        /*SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });*/
        db.execSQL(selectQuery);
        db.close();
    }





    //********************** formConditional




    //********************** formparentConditional



    void addFormPArentConditional(FormSaveDetails formsave) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_Projectid, formsave.getParentprojectid());
        values.put(KEY_GRPID, formsave.getgrpid());
        values.put(KEY_fieldid, formsave.getParentfieldid());
        values.put(KEY_values1, formsave.getParentconvalues());
        values.put(KEY_fieldaction, formsave.getfieldaction());
        values.put(KEY_flagmode, formsave.getflagmode());

        // Inserting Row
        db.insert(TABLE_FormParentConditional, null, values);
        db.close(); // Closing database connection
    }


    public void deleteFormParentConditional() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "DELETE FROM " + TABLE_FormParentConditional;
        /*SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });*/
        db.execSQL(selectQuery);
        db.close();
    }





    //********************** formparentConditional











    //********************** formparentsavedata



    void addFormPArentSaveData(FormSaveDetails formsave) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_USER, formsave.getuserID());
        values.put(KEY_ParentID, formsave.getParentid());
        values.put(KEY_Imgval, formsave.getFormimgval());
        values.put(KEY_PROJECT, formsave.getProjectId());
        values.put(KEY_FORM, formsave.getFormId());
        values.put(KEY_SAVEID, formsave.getSaveID());
        values.put(KEY_GROUPID, formsave.getGroupID());


        // Inserting Row
        db.insert(TABLE_FormParentSaveData, null, values);
        db.close(); // Closing database connection
    }


    public void deleteFormParentSavedata() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "DELETE FROM " + TABLE_FormParentSaveData;
        /*SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });*/
        db.execSQL(selectQuery);
        db.close();
    }





    //********************** formparentsavedata









    //********************** formparentdata



    void addFormPArentData(FormSaveDetails formsave) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ParentID, formsave.getParentid());
        values.put(KEY_PROJECT, formsave.getProjectId());
        values.put(KEY_ELEID, formsave.geteleId());
        values.put(KEY_ORDERID, formsave.getorderid());
        values.put(KEY_ELEMENTNAME, formsave.getelename());
        values.put(KEY_GRPID, formsave.getgrpid());
        values.put(KEY_DROPORDER, formsave.getDroporder());
        values.put(KEY_DROPNAME, formsave.getDropname());
        values.put(KEY_CONLOGIC, formsave.getconlogic());


        // Inserting Row
        db.insert(TABLE_FormParentData, null, values);
        db.close(); // Closing database connection
    }


    public void deleteFormParentdata() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "DELETE FROM " + TABLE_FormParentData;
        /*SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });*/
        db.execSQL(selectQuery);
        db.close();
    }





    //********************** formparentdata











    //********************** FormImages



    void addFormImages(FormSaveDetails formsave) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PROJECT, formsave.getProjectId());
        values.put(KEY_FORM, formsave.getFormId());
        values.put(KEY_USER, formsave.getuserID());
        values.put(KEY_Imgval, formsave.getFormimgval());
        values.put(KEY_Dummyimage, formsave.getdummyimg());
        values.put(KEY_CURRLOC, formsave.getcurrloc());


        // Inserting Row
        db.insert(TABLE_FormIMAGES, null, values);
        db.close(); // Closing database connection
    }


    public void deleteFormImages() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "DELETE FROM " + TABLE_FormIMAGES;
        /*SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });*/
        db.execSQL(selectQuery);
        db.close();
    }





    //********************** FormImages









    //********************** FormELEMENTdata



    void addFormElement(FormSaveDetails formsave) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ELEID, formsave.geteleId());
        values.put(KEY_InputType, formsave.getFormElement());


        // Inserting Row
        db.insert(TABLE_FormElements, null, values);
        db.close(); // Closing database connection
    }


    public void deleteFormElement() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "DELETE FROM " + TABLE_FormElements;
        /*SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });*/
        db.execSQL(selectQuery);
        db.close();
    }





    //********************** FormELEMENTdata











    //********************** FormDummydata




    void addFormdata(FormSaveDetails formsave) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_FIELD, formsave.getfiledID());
        values.put(KEY_PROJECT, formsave.getProjectId());
        values.put(KEY_FORM, formsave.getFormId());
        values.put(KEY_ELEID, formsave.geteleId());
        values.put(KEY_ORDERID, formsave.getorderid());
        values.put(KEY_ELEMENTNAME, formsave.getelename());
        values.put(KEY_GRPID, formsave.getgrpid());
        values.put(KEY_DROPORDER, formsave.getDroporder());
        values.put(KEY_DROPNAME, formsave.getDropname());
        values.put(KEY_SHOw_INSEARCH, formsave.getshowinsearch());
        values.put(KEY_SHOwPARENT, formsave.getshowparent());
        values.put(KEY_CONLOGIC, formsave.getconlogic());

        // Inserting Row
        db.insert(TABLE_FormData, null, values);
        db.close(); // Closing database connection
    }


    public void deleteFormdata() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "DELETE FROM " + TABLE_FormData;
        /*SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });*/
        db.execSQL(selectQuery);
        db.close();
    }







    public List<FormSaveDetails> ShowformData() {
        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();
        String selectQuery = "SELECT  * FROM " + TABLE_FormData;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setSID(Integer.parseInt(cursor.getString(0)));
                formsave.setfieldID(Integer.parseInt(cursor.getString(1)));
                formsave.setprojectID(cursor.getInt(2));
                formsave.setFormID(cursor.getInt(3));
                formsave.seteleId(cursor.getInt(4));
                formsave.setorderid(cursor.getInt(5));
                formsave.setelename(cursor.getString(6));
                formsave.setgrpid(cursor.getInt(7));
                formsave.setDroporder(cursor.getInt(8));
                formsave.setDropname(cursor.getString(9));
                formsave.setshowinsearch(cursor.getInt(10));
                formsave.setshowparent(cursor.getInt(11));
                formsave.setconlogic(cursor.getInt(12));
                contactList.add(formsave);
            } while (cursor.moveToNext());
        }
        return contactList;
    }







    //********************** FormDummydata








    //********************** Add Contacts




    void addContact(FormSaveDetails formsave) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_USER, formsave.getuserID());
        values.put(KEY_PROJECT, formsave.getProjectId());
        values.put(KEY_FORM, formsave.getFormId());
        values.put(KEY_FIELD, formsave.getfiledID());
        values.put(KEY_VALUE, formsave.getValue());
        values.put(KEY_FLAG, formsave.getFlag());
        values.put(KEY_IMAGENAMME, formsave.getImageName());
        values.put(KEY_IMAGEPATH, formsave.getImagepath());
        values.put(KEY_SaveFormId, formsave.getsavformid());
        values.put(KEY_LATLONG, formsave.getLatLong());


        // Inserting Row
        db.insert(TABLE_CONTACTS, null, values);
        db.close(); // Closing database connection
    }
    //********************** Add Contacts



    //********************** Add Floor Information

    void addFloorDetails(FormSaveDetails formsave) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
       // values.put(KEY_SID, formsave.getSID());
        values.put(KEY_USER, formsave.getuserID());
        values.put(KEY_PID, formsave.getPID());
        values.put(KEY_VALUE, formsave.getValue());
        values.put(KEY_PROJECT, formsave.getProjectId());

        values.put(KEY_FORM, formsave.getFormId());
        values.put(KEY_SAVEID, formsave.getSaveID());
        values.put(KEY_GROUPID, formsave.getGroupID());
        values.put(KEY_SaveFormId, formsave.getsavformid());
        values.put(KEY_FLAG, formsave.getFlag());


        // Inserting Row
        db.insert(TABLE_FlOORINFO, null, values);
        db.close(); // Closing database connection
    }

    //**********************Add Floor Information






    // Getting single contact
    /*FormSaveDetails getData(int dataid) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CONTACTS, new String[] { KEY_ID,
                        KEY_USER, KEY_PROJECT,KEY_FORM,KEY_FIELD,KEY_VALUE,KEY_FLAG,KEY_IMAGENAMME,KEY_LATLONG}, KEY_ID + "=?",
                new String[] { String.valueOf(dataid) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        FormSaveDetails formsave = new FormSaveDetails(cursor.getInt(1),cursor.getInt(2),cursor.getInt(3),cursor.getInt(4),(cursor.getString(5)),(cursor.getString(6)),(cursor.getString(7)),(cursor.getString(8)));
        // return contact
        return formsave;
    }*/





    //********************** Getting AllContactDteails


    public List<FormSaveDetails> getAllContacts(int projectid,int SaveFormId) {
        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();
        String selectQuery = "SELECT * FROM  `contacts` WHERE Flag = 'insert' AND Project_Id = '" + projectid + "' AND Save_formid = '" + SaveFormId + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setID(Integer.parseInt(cursor.getString(0)));
                formsave.setuserID(cursor.getInt(1));
                formsave.setprojectID(cursor.getInt(2));
                formsave.setFormID(cursor.getInt(3));
                formsave.setfieldID(cursor.getInt(4));
                formsave.setValue(cursor.getString(5));
                formsave.setFlag(cursor.getString(6));
                formsave.setImageName(cursor.getString(7));
                formsave.setImagepath(cursor.getString(8));
                formsave.setsavformid(cursor.getInt(9));
                formsave.setLatLong(cursor.getString(10));
                contactList.add(formsave);
            } while (cursor.moveToNext());
        }
        return contactList;
    }

    public List<FormSaveDetails> getAllContacts_update(int projectid,int FormId) {
        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();
        String selectQuery = "SELECT * FROM  `contacts` WHERE Flag = 'Update' AND Project_Id = '" + projectid + "' AND Form_Id = '" + FormId + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setID(Integer.parseInt(cursor.getString(0)));
                formsave.setuserID(cursor.getInt(1));
                formsave.setprojectID(cursor.getInt(2));
                formsave.setFormID(cursor.getInt(3));
                formsave.setfieldID(cursor.getInt(4));
                formsave.setValue(cursor.getString(5));
                formsave.setFlag(cursor.getString(6));
                formsave.setImageName(cursor.getString(7));
                formsave.setImagepath(cursor.getString(8));
                formsave.setsavformid(cursor.getInt(9));
                formsave.setLatLong(cursor.getString(10));
                contactList.add(formsave);
            } while (cursor.moveToNext());
        }
        return contactList;
    }


    public List<FormSaveDetails> getAllContacts_Editupdate(int projectid,int FormId,int saveid) {
        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();
        String selectQuery = "SELECT * FROM  `FloorInformation` WHERE Flag = 'editupdate' AND Project_Id = '" + projectid + "' AND Form_Id = '" + FormId + "' AND save_id = '" + saveid + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setSID(cursor.getInt(0));
                formsave.setuserID(cursor.getInt(1));
                formsave.setPID(cursor.getInt(2));
                formsave.setValue(cursor.getString(3));
                formsave.setprojectID(cursor.getInt(4));

                formsave.setFormID(cursor.getInt(5));
                formsave.setSaveID(cursor.getInt(6));
                formsave.setGroupID(cursor.getInt(7));
                formsave.setsavformid(cursor.getInt(8));
                formsave.setFlag(cursor.getString(9));
                contactList.add(formsave);
            } while (cursor.moveToNext());
        }
        return contactList;
    }


    public List<FormSaveDetails> CheckAllContacts_Size(int projectid) {
        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();
        String selectQuery = "SELECT * FROM `contacts` WHERE Flag = 'insert' AND Project_Id = '" + projectid + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setID(Integer.parseInt(cursor.getString(0)));
                formsave.setuserID(cursor.getInt(1));
                formsave.setprojectID(cursor.getInt(2));
                formsave.setFormID(cursor.getInt(3));
                formsave.setfieldID(cursor.getInt(4));
                formsave.setValue(cursor.getString(5));
                formsave.setFlag(cursor.getString(6));
                formsave.setImageName(cursor.getString(7));
                formsave.setImagepath(cursor.getString(8));
                formsave.setsavformid(cursor.getInt(9));
                formsave.setLatLong(cursor.getString(10));
                contactList.add(formsave);
            } while (cursor.moveToNext());
        }
        return contactList;
    }

    public List<FormSaveDetails> CheckAllContacts_Size_update(int projectid) {
        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();
        String selectQuery = "SELECT * FROM `contacts` WHERE Flag = 'Update' AND Project_Id = '" + projectid + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setID(Integer.parseInt(cursor.getString(0)));
                formsave.setuserID(cursor.getInt(1));
                formsave.setprojectID(cursor.getInt(2));
                formsave.setFormID(cursor.getInt(3));
                formsave.setfieldID(cursor.getInt(4));
                formsave.setValue(cursor.getString(5));
                formsave.setFlag(cursor.getString(6));
                formsave.setImageName(cursor.getString(7));
                formsave.setImagepath(cursor.getString(8));
                formsave.setsavformid(cursor.getInt(9));
                formsave.setLatLong(cursor.getString(10));
                contactList.add(formsave);
            } while (cursor.moveToNext());
        }
        return contactList;
    }






    public List<FormSaveDetails> getuniqueid(int project) {
        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();
        String selectQuery = "SELECT DISTINCT Save_formid FROM contacts WHERE Flag = 'insert' AND Project_Id = '" + project + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setsavformid(cursor.getInt(0));
                contactList.add(formsave);
            } while (cursor.moveToNext());
        }
        return contactList;
    }



    public List<FormSaveDetails> getuniqueid_update(int project) {
        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();
        String selectQuery = "SELECT DISTINCT Form_Id FROM contacts WHERE Flag = 'Update' AND Project_Id = '" + project + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setFormID(cursor.getInt(0));
                contactList.add(formsave);
            } while (cursor.moveToNext());
        }
        return contactList;
    }




    public List<FormSaveDetails> getuniqueid_Editupdate(int project) {
        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();
        String selectQuery = "SELECT DISTINCT Form_Id FROM FloorInformation WHERE Flag = 'editupdate' AND Project_Id = '" + project + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setFormID(cursor.getInt(0));
                contactList.add(formsave);
            } while (cursor.moveToNext());
        }
        return contactList;
    }

    public List<FormSaveDetails> getunique_Saveid(int project,int formid) {
        List<FormSaveDetails> contactList = new ArrayList<FormSaveDetails>();
        String selectQuery = "SELECT DISTINCT save_id FROM FloorInformation WHERE Flag = 'editupdate' AND Project_Id = '" + project + "' AND Form_Id = '" + formid + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setSaveID(cursor.getInt(0));
                contactList.add(formsave);
            } while (cursor.moveToNext());
        }
        return contactList;
    }















    //********************** Getting AllContactDteails



    //********************** Getting All Floor Information

    public List<FormSaveDetails> getAllFloorDetails(int projectid,int SaveFormId) {
        List<FormSaveDetails> floortList = new ArrayList<FormSaveDetails>();

      //  String selectQuery = "SELECT  * FROM " + TABLE_FlOORINFO;

        String selectQuery = "SELECT * FROM `FloorInformation` WHERE Flag = 'insert' AND Project_Id = '" + projectid + "' AND Save_formid = '" + SaveFormId + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setSID(cursor.getInt(0));
                formsave.setuserID(cursor.getInt(1));
                formsave.setPID(cursor.getInt(2));
                formsave.setValue(cursor.getString(3));
                formsave.setprojectID(cursor.getInt(4));

                formsave.setFormID(cursor.getInt(5));
                formsave.setSaveID(cursor.getInt(6));
                formsave.setGroupID(cursor.getInt(7));
                formsave.setsavformid(cursor.getInt(8));
                formsave.setFlag(cursor.getString(9));
                floortList.add(formsave);
            } while (cursor.moveToNext());
        }
        return floortList;
    }


    public List<FormSaveDetails> getAllFloorDetails_update(int projectid,int Formid) {
        List<FormSaveDetails> floortList = new ArrayList<FormSaveDetails>();

        //  String selectQuery = "SELECT  * FROM " + TABLE_FlOORINFO;

        String selectQuery = "SELECT * FROM `FloorInformation` WHERE Flag = 'Update' AND Project_Id = '" + projectid + "' AND Form_Id = '" + Formid + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                FormSaveDetails formsave = new FormSaveDetails();
                formsave.setSID(cursor.getInt(0));
                formsave.setuserID(cursor.getInt(1));
                formsave.setPID(cursor.getInt(2));
                formsave.setValue(cursor.getString(3));
                formsave.setprojectID(cursor.getInt(4));

                formsave.setFormID(cursor.getInt(5));
                formsave.setSaveID(cursor.getInt(6));
                formsave.setGroupID(cursor.getInt(7));
                formsave.setsavformid(cursor.getInt(8));
                formsave.setFlag(cursor.getString(9));
                floortList.add(formsave);
            } while (cursor.moveToNext());
        }
        return floortList;
    }




    //********************** Getting All Floor Information






    // Updating single contact
    public int updateContact(FormSaveDetails formsave) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_USER, formsave.getuserID());
        values.put(KEY_PROJECT, formsave.getProjectId());
        values.put(KEY_FORM, formsave.getFormId());
        values.put(KEY_FIELD, formsave.getfiledID());
        values.put(KEY_VALUE, formsave.getValue());
        values.put(KEY_FLAG, formsave.getFlag());
        values.put(KEY_IMAGENAMME, formsave.getImageName());
        values.put(KEY_LATLONG, formsave.getLatLong());

        // updating row
        return db.update(TABLE_CONTACTS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(formsave.getID()) });
    }

    // Deleting single contact
   /* public void deleteContact(FormSaveDetails contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });
        db.close();
    }*/


    // Deleting single contact
    public void deleteContact(int projectid) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "DELETE FROM `contacts` WHERE Project_Id = '" + projectid + "'";
        /*SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });*/
        db.execSQL(selectQuery);
        db.close();
    }




    public void deleteFloorContact(int projectid) {
        SQLiteDatabase db = this.getReadableDatabase();
     //   String selectQuery = "DELETE FROM " + TABLE_FlOORINFO;

        String selectQuery = "DELETE FROM `FloorInformation` WHERE Project_Id = '" + projectid + "'";
    /*SQLiteDatabase db = this.getWritableDatabase();
    db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
            new String[] { String.valueOf(contact.getID()) });*/
        db.execSQL(selectQuery);
        db.close();
    }

    //DELETE FROM SQLITE_SEQUENCE WHERE name='TableName';


    // Getting contacts Count
    public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CONTACTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }


}
