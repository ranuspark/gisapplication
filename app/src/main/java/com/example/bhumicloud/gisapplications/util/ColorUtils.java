package com.example.bhumicloud.gisapplications.util;

import android.graphics.Color;
import android.support.v4.view.ViewCompat;

public class ColorUtils {
    public static int getContentColor(int backgroundColor) {
        return getContentColor(backgroundColor, -1, ViewCompat.MEASURED_STATE_MASK);
    }

    public static int getContentColor(int backgroundColor, int lightContent, int darkContent) {
        return ((0.299f * (((float) Color.red(backgroundColor)) / 255.0f)) + (0.587f * (((float) Color.green(backgroundColor)) / 255.0f))) + (0.114f * (((float) Color.blue(backgroundColor)) / 255.0f)) >= 0.7f ? darkContent : lightContent;
    }

    public static int darken(int color, float ratio) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] = hsv[2] - (hsv[2] * ratio);
        return Color.HSVToColor( Color.alpha(color), hsv);
    }
}
