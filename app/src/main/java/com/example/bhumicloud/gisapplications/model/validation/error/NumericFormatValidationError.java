package com.example.bhumicloud.gisapplications.model.validation.error;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.textual.text.TextElement;
import com.example.bhumicloud.gisapplications.model.validation.FeatureValidationError;

public class NumericFormatValidationError extends FeatureValidationError {
    public NumericFormatValidationError(Record record, TextElement element) {
        String numericFormat = element.getNumericFormat();
        String label = record == null ? element.getLabel() : element.getAbsoluteLabel(record);
        if (TextElement.FORMAT_INTEGER.equals(numericFormat)) {
            this.mMessage = String.format(GIS.getInstance().getResources().getString(R.string.numeric_format_integer_validation_error), new Object[]{label});
        } else {
            this.mMessage = String.format(GIS.getInstance().getResources().getString(R.string.numeric_format_double_validation_error), new Object[]{label});
        }
    }
}
