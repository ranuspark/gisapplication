CREATE TRIGGER IF NOT EXISTS delete_map_layer_files DELETE ON Maps
    BEGIN
        INSERT INTO trash_can (path) VALUES (OLD.local_url);
    END;