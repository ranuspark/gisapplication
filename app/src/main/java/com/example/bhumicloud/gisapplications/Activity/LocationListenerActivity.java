package com.example.bhumicloud.gisapplications.Activity;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.example.bhumicloud.gisapplications.location.SimpleLocationClient;


public abstract class LocationListenerActivity extends GISActivity implements LocationListener {
    private static final String STATE_LAST_LOCATION = "mLastLocation";
    private Location mLastLocation;
    private SimpleLocationClient mLocationClient;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mLocationClient = new SimpleLocationClient(this, getLocationRequest());
        this.mLocationClient.setListener(this);
        if (savedInstanceState != null) {
            this.mLastLocation = (Location) savedInstanceState.getParcelable(STATE_LAST_LOCATION);
        }
    }

    protected void onStart() {
        super.onStart();
        this.mLocationClient.connect();
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(STATE_LAST_LOCATION, this.mLastLocation);
    }

    protected void onStop() {
        this.mLocationClient.disconnect();
        super.onStop();
    }

    protected void onDestroy() {
        if (this.mLocationClient != null) {
            this.mLocationClient.setListener(null);
            this.mLocationClient = null;
        }
        super.onDestroy();
    }

    public void onLocationChanged(Location location) {
        this.mLastLocation = location;
        Fragment fragment = getCurrentFragment();
        if (fragment != null && (fragment instanceof LocationListener)) {
            ((LocationListener) fragment).onLocationChanged(location);
        }
    }

    public void startLocationUpdates() {
        this.mLocationClient.beginUpdates();
    }

    public void stopLocationUpdates() {
        if (this.mLocationClient != null) {
            this.mLocationClient.ceaseUpdates();
        }
    }

    public Location getLastLocation() {
        Location location = this.mLocationClient.getLastLocation();
        if (location == null) {
            return this.mLastLocation;
        }
        return location;
    }

    protected LocationRequest getLocationRequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(100);
        locationRequest.setInterval(250);
        locationRequest.setFastestInterval(200);
        return locationRequest;
    }
}

