package com.example.bhumicloud.gisapplications.View.media;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;

public abstract class MediaImageView extends AppCompatImageView {
    public abstract void setImage(Attachment attachment);

    public MediaImageView(Context context) {
        super(context);
    }

    public MediaImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MediaImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
