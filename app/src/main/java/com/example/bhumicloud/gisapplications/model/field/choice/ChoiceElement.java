package com.example.bhumicloud.gisapplications.model.field.choice;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ChoiceElement extends Element {
    public static final Creator<ChoiceElement> CREATOR = new C11861();
    private static final String JSON_ALLOW_OTHER = "allow_other";
    private static final String JSON_CHOICES = "choices";
    private static final String JSON_CHOICE_LIST_ID = "choice_list_id";
    private static final String JSON_MULTIPLE = "multiple";
    private boolean mAllowOther;
    private ChoiceList mChoiceList;
    private String mChoiceListRemoteID;
    private ArrayList<ChoiceItem> mChoices;
    private HashMap<String, ChoiceItem> mChoicesByValue;
    private ArrayList<String> mChoicesFilter;
    private ArrayList<Object> mChoicesJSON;
    private boolean mMultipleSelection;
    private ArrayList<ChoiceItem> mOverrideChoices;

    static class C11861 implements Creator<ChoiceElement> {
        C11861() {
        }

        public ChoiceElement createFromParcel(Parcel source) {
            return new ChoiceElement(source);
        }

        public ChoiceElement[] newArray(int size) {
            return new ChoiceElement[size];
        }
    }

    public ChoiceElement(Element parent, Map json) {
        super(parent, json);
    }

    private ChoiceElement(Parcel parcel) {
        super(parcel);
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = super.toJSON();
        json.put("choices", this.mChoicesJSON);
        json.put(JSON_MULTIPLE, Boolean.valueOf(isMultipleChoice()));
        json.put(JSON_ALLOW_OTHER, Boolean.valueOf(isOtherFieldAllowed()));
        json.put(JSON_CHOICE_LIST_ID, this.mChoiceListRemoteID);
        return json;
    }

    public String getType() {
        return Element.TYPE_CHOICE;
    }

    public ArrayList<ChoiceItem> getChoices() {
        return this.mOverrideChoices != null ? this.mOverrideChoices : getFilteredChoices();
    }

    public ChoiceItem getChoice(String value) {
        return (ChoiceItem) getChoicesByValue().get(value);
    }

    public void setChoicesFilter(List choices) {
        this.mChoicesByValue = null;
        if (choices == null || choices.isEmpty()) {
            this.mChoicesFilter = null;
            return;
        }
        this.mChoicesFilter = new ArrayList();
        for (Object item : choices) {
            if (item instanceof String) {
                this.mChoicesFilter.add(((String) item).toLowerCase());
            }
        }
    }

    public void setOverrideChoices(List choices) {
        this.mChoicesByValue = null;
        if (choices == null || choices.isEmpty()) {
            this.mOverrideChoices = null;
            return;
        }
        ArrayList<ChoiceItem> newChoices = new ArrayList();
        for (Object choice : choices) {
            if (choice instanceof Map) {
                newChoices.add(new ChoiceItem((Map) choice));
            }
        }
        this.mOverrideChoices = newChoices;
    }

    private ArrayList<ChoiceItem> getFilteredChoices() {
        ArrayList<ChoiceItem> items = getChoiceItems();
        if (this.mChoicesFilter == null) {
            return items;
        }
        ArrayList<ChoiceItem> filteredItems = new ArrayList();
        Iterator it = items.iterator();
        while (it.hasNext()) {
            ChoiceItem item = (ChoiceItem) it.next();
            Iterator it2 = this.mChoicesFilter.iterator();
            while (it2.hasNext()) {
                if (item.getValue().toLowerCase().contains(((String) it2.next()).toLowerCase())) {
                    filteredItems.add(item);
                }
            }
        }
        return filteredItems;
    }

    private ArrayList<ChoiceItem> getChoiceItems() {
        if (getChoiceList() != null) {
            return getChoiceList().getChoices();
        }
        if (this.mChoices != null) {
            return this.mChoices;
        }
        this.mChoices = new ArrayList();
        if (!(this.mChoicesJSON == null || this.mChoicesJSON.isEmpty())) {
            Iterator it = this.mChoicesJSON.iterator();
            while (it.hasNext()) {
                HashMap<String, Object> itemJSON = JSONUtils.getHashMap(it.next());
                if (!(itemJSON == null || itemJSON.isEmpty())) {
                    this.mChoices.add(new ChoiceItem(itemJSON));
                }
            }
        }
        return this.mChoices;
    }

    private ChoiceList getChoiceList() {
        if (this.mChoiceList != null) {
            return this.mChoiceList;
        }
        if (!TextUtils.isEmpty(this.mChoiceListRemoteID)) {
            this.mChoiceList = ChoiceList.getChoiceList(Account.getActiveAccount(), this.mChoiceListRemoteID);
        }
        return this.mChoiceList;
    }

    private HashMap<String, ChoiceItem> getChoicesByValue() {
        if (this.mChoicesByValue != null) {
            return this.mChoicesByValue;
        }
        this.mChoicesByValue = new HashMap();
        Iterator it = getChoices().iterator();
        while (it.hasNext()) {
            ChoiceItem choice = (ChoiceItem) it.next();
            this.mChoicesByValue.put(choice.getValue(), choice);
        }
        return this.mChoicesByValue;
    }

    public boolean isMultipleChoice() {
        return this.mMultipleSelection;
    }

    public boolean isOtherFieldAllowed() {
        return this.mAllowOther;
    }

    public void setJSONAttributes(Map json) {
        super.setJSONAttributes(json);
        this.mChoiceListRemoteID = JSONUtils.getString(json, JSON_CHOICE_LIST_ID);
        this.mChoicesJSON = JSONUtils.getArrayList(json, "choices");
        this.mChoices = null;
        this.mMultipleSelection = JSONUtils.getBoolean(json, JSON_MULTIPLE, false);
        this.mAllowOther = JSONUtils.getBoolean(json, JSON_ALLOW_OTHER, false);
    }
}
