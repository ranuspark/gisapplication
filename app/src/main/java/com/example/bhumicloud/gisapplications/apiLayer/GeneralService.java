package com.example.bhumicloud.gisapplications.apiLayer;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;

public interface GeneralService {
    @Streaming
    @GET("{path}")
    Call<ResponseBody> download(@Header("Accept") String str, @Path("path") String str2, @QueryMap(encoded = true) HashMap<String, String> hashMap);
}
