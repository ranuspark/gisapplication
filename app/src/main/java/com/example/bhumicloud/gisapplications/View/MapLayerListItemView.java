package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.Activity.LayerListAdapter.LayerListAdapterListener;
import com.example.bhumicloud.gisapplications.model.layer.MBTilesMapLayer;
import com.example.bhumicloud.gisapplications.model.layer.MapLayer;
import com.example.bhumicloud.gisapplications.util.FileUtils;
import com.example.bhumicloud.gisapplications.util.NetworkUtils;
import com.example.bhumicloud.gisapplications.widget.CircularProgressBar;

public class MapLayerListItemView extends FrameLayout {
    private final CheckBox mCheckBox;
    private final ImageView mDeleteIcon;
    private final TextView mDescLabel;
    private final ImageView mDownloadIcon;
    private final TextView mNameLabel;
    private final CircularProgressBar mProgress;
    private final ImageView mReorderIcon;
    private final TextView mSizeLabel;
    private final View mStatusIndicator;

    public MapLayerListItemView(Context context) {
        this(context, null);
    }

    public MapLayerListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.list_item_map_layer, this, true);
        this.mReorderIcon = (ImageView) findViewById(R.id.layer_reorder);
        this.mNameLabel = (TextView) findViewById(R.id.name);
        this.mSizeLabel = (TextView) findViewById(R.id.size);
        this.mDescLabel = (TextView) findViewById(R.id.desc);
        this.mCheckBox = (CheckBox) findViewById(R.id.enabled_checkbox);
        this.mDownloadIcon = (ImageView) findViewById(R.id.download);
        this.mDeleteIcon = (ImageView) findViewById(R.id.delete_layer);
        this.mProgress = (CircularProgressBar) findViewById(R.id.layer_progress);
        this.mStatusIndicator = findViewById(R.id.status_bar);
    }

    public void configure(final MapLayer layer, final LayerListAdapterListener listener, boolean isEditMode) {
        boolean z = true;
        configureInformationViews(layer);
        boolean z2 = listener != null && listener.isDownloading(layer.getRowID());
        configureAvailabilityViews(layer, z2, isEditMode);
        setTag(layer.getRemoteID());
        CheckBox checkBox = this.mCheckBox;
        if (listener == null || !listener.isEnabled(layer.getRowID())) {
            z = false;
        }
        checkBox.setChecked(z);
        this.mDeleteIcon.setOnClickListener(new OnClickListener () {
            public void onClick(View v) {
                if (listener != null) {
                    listener.onDelete(layer);
                }
            }
        });
    }

    public void configureAvailabilityViews(MapLayer mapLayer, boolean isDownloading, boolean editMode) {
        View view = this.mStatusIndicator;
        boolean z = mapLayer.isAvailable() && ((mapLayer instanceof MBTilesMapLayer) || NetworkUtils.isConnected(getContext()));
        view.setSelected(z);
        if (editMode) {
            if ((mapLayer instanceof MBTilesMapLayer) && mapLayer.isAvailable()) {
                this.mDeleteIcon.setVisibility(View.VISIBLE);
            } else {
                this.mDeleteIcon.setVisibility( View.GONE);
            }
            this.mReorderIcon.setVisibility(View.VISIBLE);
            this.mCheckBox.setVisibility(View.GONE);
            this.mDownloadIcon.setVisibility(View.GONE);
            this.mProgress.setVisibility(View.GONE);
            return;
        }
        this.mDeleteIcon.setVisibility(View.GONE);
        this.mReorderIcon.setVisibility(View.GONE);
        if (!(mapLayer instanceof MBTilesMapLayer) || mapLayer.isAvailable()) {
            this.mCheckBox.setVisibility(View.VISIBLE);
            this.mDownloadIcon.setVisibility(View.GONE);
            this.mProgress.setVisibility(View.GONE);
            return;
        }
        this.mCheckBox.setVisibility(View.GONE);
        if (isDownloading) {
            this.mDownloadIcon.setVisibility(View.GONE);
            this.mProgress.setVisibility(View.VISIBLE);
            return;
        }
        this.mDownloadIcon.setVisibility(View.VISIBLE);
        this.mProgress.setVisibility(View.GONE);
    }

    public void setDownloadProgress(float progress) {
        this.mProgress.setProgress(progress);
    }

    public void setChecked(boolean checked) {
        this.mCheckBox.setChecked(checked);
    }

    private void configureInformationViews(MapLayer mapLayer) {
        this.mNameLabel.setText(mapLayer.getName());
        double size = mapLayer.getFilesize();
        if (size > 0.0d) {
            this.mSizeLabel.setText(FileUtils.toHumanReadableBytes(size));
            this.mSizeLabel.setVisibility(View.VISIBLE);
        } else {
            this.mSizeLabel.setVisibility(View.GONE);
        }
        this.mDescLabel.setText(mapLayer.getDescription());
    }
}
