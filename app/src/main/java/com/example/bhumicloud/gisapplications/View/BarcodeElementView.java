package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import com.example.bhumicloud.gisapplications.model.field.textual.barcode.BarcodeElement;

public class BarcodeElementView extends ElementView<BarcodeElement> {
    private BarcodeElementViewListener mListener;

    public interface BarcodeElementViewListener {
        void onShowBarcodeDialog(BarcodeElement barcodeElement);
    }

    public BarcodeElementView(Context context, BarcodeElement element, boolean viewOnly) {
        super(context, element, viewOnly);
    }

    public void setListener(BarcodeElementViewListener listener) {
        this.mListener = listener;
    }

    protected void onFieldViewClicked() {
        super.onFieldViewClicked();
        if (this.mListener != null) {
            this.mListener.onShowBarcodeDialog((BarcodeElement) getElement());
        }
    }
}
