CREATE TABLE IF NOT EXISTS Forms (
    _id INTEGER PRIMARY KEY AUTOINCREMENT,
    remote_id TEXT,
    account_id INTEGER,
    name TEXT,
    description TEXT,
    title_key TEXT,
    remote_record_count INTEGER,
    elements TEXT,
    remote_thumbnail_path TEXT,
    local_thumbnail_path TEXT,
    hidden_on_dashboard INTEGER NOT NULL DEFAULT 0,
    geometry_types TEXT,
    geometry_required INTEGER NOT NULL DEFAULT 0,
    report_templates TEXT,
    status_field TEXT,
    sticky_defaults TEXT,
    script TEXT,
    projects_enabled INTEGER NOT NULL DEFAULT 1,
    assignment_enabled INTEGER NOT NULL DEFAULT 1,
    deleted_at REAL
);

CREATE INDEX IF NOT EXISTS forms_account_id_idx ON Forms (account_id);
CREATE INDEX IF NOT EXISTS forms_account_id_name_idx ON Forms (account_id, name);
CREATE INDEX IF NOT EXISTS forms_account_id_remote_id_idx ON Forms (account_id, remote_id);