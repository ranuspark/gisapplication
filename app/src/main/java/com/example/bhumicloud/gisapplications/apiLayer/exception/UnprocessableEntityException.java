package com.example.bhumicloud.gisapplications.apiLayer.exception;

import android.content.Context;

import com.example.bhumicloud.gisapplications.R;

public class UnprocessableEntityException extends GISServiceException {
    public UnprocessableEntityException(String message) {
        super(message);
    }

    public String getUserFriendlyMessage(Context context) {
        return context.getString(R.string.unprocessable_entity_message);
    }
}
