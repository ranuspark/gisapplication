package com.example.bhumicloud.gisapplications.maps;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.example.bhumicloud.gisapplications.R;

import java.util.HashMap;
import java.util.Map;

public class GISMarker {
    private static Map<String, BitmapDescriptor> sBitmapCache;

    public enum Color {
        CB0D0C
    }

    public static BitmapDescriptor getMarkerBitmapDescriptor(Color color) {
        return getMarkerBitmapDescriptor("#" + color.name());
    }

    public static BitmapDescriptor getMarkerBitmapDescriptor(String color) {
        if (sBitmapCache == null) {
            sBitmapCache = new HashMap ();
            sBitmapCache.put("#242424", BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_242424));
            sBitmapCache.put("#B3B3B3", BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_b3b3b3));
            sBitmapCache.put("#FFFFFF", BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_ffffff));
            sBitmapCache.put("#704B10", BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_704b10));
            sBitmapCache.put("#DA0796", BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_da0796));
            sBitmapCache.put("#CB0D0C", BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_cb0d0c));
            sBitmapCache.put("#FF8819", BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_ff8819));
            sBitmapCache.put("#FFD300", BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_ffd300));
            sBitmapCache.put("#87D30F", BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_87d30f));
            sBitmapCache.put("#2D5D00", BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_2d5d00));
            sBitmapCache.put("#294184", BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_294184));
            sBitmapCache.put("#1891C9", BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_1891c9));
        }
        return (BitmapDescriptor) sBitmapCache.get(color.toUpperCase());
    }

    public static void setAnchors(MarkerOptions opts) {
        setPinAnchor(opts);
        setInfoWindowAnchor(opts);
    }

    public static void setPinAnchor(MarkerOptions opts) {
        opts.anchor(0.5f, 0.739f);
    }

    public static void setInfoWindowAnchor(MarkerOptions opts) {
        opts.infoWindowAnchor(0.5f, 0.261f);
    }
}
