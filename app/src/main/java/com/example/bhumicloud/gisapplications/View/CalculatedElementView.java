package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.field.textual.calculated.CalculatedElement;
import com.example.bhumicloud.gisapplications.model.field.textual.calculated.CalculatedValue;

public class CalculatedElementView extends ElementView<CalculatedElement> {
    public CalculatedElementView(Context context, CalculatedElement element, boolean viewOnly) {
        super(context, element, viewOnly);
    }

    public void setValue(FormValue formValue) {
        if (formValue instanceof CalculatedValue) {
            CalculatedValue calculatedValue = (CalculatedValue) formValue;
            if (calculatedValue.hasError()) {
                setFieldTextColor(-3355444);
            } else {
                setFieldTextColor(ViewCompat.MEASURED_STATE_MASK);
            }
            String stringValue = calculatedValue.getDisplayValue();
            if (!TextUtils.equals(stringValue, getFieldValue())) {
                setValue((String) stringValue);
            }
        }
    }
}
