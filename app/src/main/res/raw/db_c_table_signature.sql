CREATE TABLE signatures (
    _id INTEGER PRIMARY KEY AUTOINCREMENT,
    record_id TEXT,
    unique_id TEXT,
    local_path TEXT,
    timestamp TEXT,
    uploaded INTEGER
);

CREATE INDEX idx_signatures_record_id ON signatures (record_id);
CREATE INDEX idx_signatures_unique_id ON signatures (unique_id);