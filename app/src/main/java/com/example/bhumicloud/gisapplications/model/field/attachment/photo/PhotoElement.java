package com.example.bhumicloud.gisapplications.model.field.attachment.photo;

import android.os.Parcel;
import android.os.Parcelable.Creator;

import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaElement;

import java.util.Map;

public class PhotoElement extends MediaElement {
    public static final Creator<PhotoElement> CREATOR = new Creator<PhotoElement>() {
        public PhotoElement createFromParcel(Parcel source) {
            return new PhotoElement(source);
        }

        public PhotoElement[] newArray(int size) {
            return new PhotoElement[size];
        }
    };

    public PhotoElement(Element parent, Map json) {
        super(parent, json);
    }

    private PhotoElement(Parcel parcel) {
        super(parcel);
    }

    public String getType() {
        return Element.TYPE_PHOTO;
    }
}
