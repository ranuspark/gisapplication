package com.example.bhumicloud.gisapplications.model.field.recordlink;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.Map;

public class RecordLinkCondition implements Parcelable {
    public static final Creator<RecordLinkCondition> CREATOR = new C11931();
    private static final String JSON_RECORD_CONDITION_LINKED_FORM_FIELD_KEY = "linked_form_field_key";
    private static final String JSON_RECORD_CONDITION_OPERATOR = "operator";
    private static final String JSON_RECORD_CONDITION_VALUE = "value";
    private static final String JSON_RECORD_CONDITION_VALUE_FIELD_KEY = "value_field_key";
    private String mFieldKey;
    private Operator mOperator;
    private String mValue;
    private String mValueFieldKey;

    static class C11931 implements Creator<RecordLinkCondition> {
        C11931() {
        }

        public RecordLinkCondition createFromParcel(Parcel source) {
            return new RecordLinkCondition(source);
        }

        public RecordLinkCondition[] newArray(int size) {
            return new RecordLinkCondition[size];
        }
    }

    public enum Operator {
        and,
        or,
        equal_to,
        not_equal_to,
        contains,
        starts_with,
        greater_than,
        less_than,
        is_empty,
        is_not_empty
    }

    public RecordLinkCondition(Map condition) {
        this.mFieldKey = JSONUtils.getString(condition, JSON_RECORD_CONDITION_LINKED_FORM_FIELD_KEY);
        this.mOperator = Operator.valueOf(JSONUtils.getString(condition, JSON_RECORD_CONDITION_OPERATOR));
        this.mValue = JSONUtils.getString(condition, "value");
        this.mValueFieldKey = JSONUtils.getString(condition, JSON_RECORD_CONDITION_VALUE_FIELD_KEY);
    }

    private RecordLinkCondition(Parcel parcel) {
        this.mFieldKey = parcel.readString();
        String value = parcel.readString();
        if (value != null) {
            this.mOperator = Operator.valueOf(value);
        }
        this.mValue = parcel.readString();
        this.mValueFieldKey = parcel.readString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mFieldKey);
        if (this.mOperator == null) {
            dest.writeString(null);
        } else {
            dest.writeString(this.mOperator.name());
        }
        dest.writeString(this.mValue);
        dest.writeString(this.mValueFieldKey);
    }

    public int describeContents() {
        return 0;
    }

    public String getLinkedFormFieldKey() {
        return this.mFieldKey;
    }

    public Operator getOperator() {
        return this.mOperator;
    }

    public String getValue() {
        return this.mValue;
    }

    public String getValueFieldKey() {
        return this.mValueFieldKey;
    }
}
