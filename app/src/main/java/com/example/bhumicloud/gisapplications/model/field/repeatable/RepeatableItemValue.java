package com.example.bhumicloud.gisapplications.model.field.repeatable;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;
import com.example.bhumicloud.gisapplications.geoJson.GeoJSONPoint;
import com.example.bhumicloud.gisapplications.model.Feature;
import com.example.bhumicloud.gisapplications.model.FeatureEditState;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.FormValueContainer;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.util.DateUtils;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import com.example.bhumicloud.gisapplications.util.LocationUtils;
import com.example.bhumicloud.gisapplications.util.ParcelUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class RepeatableItemValue implements Feature, Comparable<RepeatableItemValue> {
    public static final Creator<RepeatableItemValue> CREATOR = new C11981();
    private static final String JSON_CREATED_AT = "created_at";
    private static final String JSON_GEOMETRY = "geometry";
    private static final String JSON_IDENTIFIER = "id";
    private static final String JSON_KEY_ALTITUDE = "altitude";
    private static final String JSON_KEY_COURSE = "course";
    private static final String JSON_KEY_CREATED_DURATION = "created_duration";
    private static final String JSON_KEY_CREATED_LOCATION = "created_location";
    private static final String JSON_KEY_EDITED_DURATION = "edited_duration";
    private static final String JSON_KEY_HORIZONTAL_ACCURACY = "horizontal_accuracy";
    private static final String JSON_KEY_LATITUDE = "latitude";
    private static final String JSON_KEY_LONGITUDE = "longitude";
    private static final String JSON_KEY_UPDATED_DURATION = "updated_duration";
    private static final String JSON_KEY_UPDATED_LOCATION = "updated_location";
    private static final String JSON_UPDATED_AT = "updated_at";
    private static final String JSON_VALUES = "form_values";
    private Date mCreatedAt;
    private Integer mCreatedDuration;
    private Location mCreatedLocation;
    private Integer mEditedDuration;
    private final RepeatableElement mElement;
    private final String mIdentifier;
    private int mIndex;
    private Location mLocation;
    private Date mUpdatedAt;
    private Integer mUpdatedDuration;
    private Location mUpdatedLocation;
    private FormValueContainer mValues;

    static class C11981 implements Creator<RepeatableItemValue> {
        C11981() {
        }

        public RepeatableItemValue createFromParcel(Parcel source) {
            return new RepeatableItemValue(source);
        }

        public RepeatableItemValue[] newArray(int size) {
            return new RepeatableItemValue[size];
        }
    }

    private RepeatableItemValue(RepeatableElement element, int index, String identifier) {
        this.mIndex = -1;
        this.mIdentifier = identifier;
        this.mElement = element;
        this.mIndex = index;
        this.mValues = new FormValueContainer(element.getElements());
    }

    public RepeatableItemValue(RepeatableElement element, int index) {
        this(element, index, UUID.randomUUID().toString());
    }

    public RepeatableItemValue(RepeatableElement element, Map itemJSON, int index, boolean clearUniqueFields,String uuid) {
        this ( element, index, uuid );
        Map geometry = JSONUtils.getHashMap(itemJSON, JSON_GEOMETRY);
        if (geometry != null) {
            this.mLocation = new GeoJSONPoint(geometry).toLocation();
        }
        String createdAt = JSONUtils.getString(itemJSON, "created_at");
        if (!TextUtils.isEmpty(createdAt)) {
            this.mCreatedAt = DateUtils.parseEpochTimestamp(createdAt);
        }
        String updatedAt = JSONUtils.getString(itemJSON, "updated_at");
        if (!TextUtils.isEmpty(updatedAt)) {
            this.mUpdatedAt = DateUtils.parseEpochTimestamp(updatedAt);
        }
        ArrayList<Element> elements = element.getElements();
        HashMap<String, Object> valuesJSON = JSONUtils.getHashMap(itemJSON, "form_values");
        if (!(valuesJSON == null || valuesJSON.isEmpty())) {
            this.mValues.loadValues(elements, valuesJSON, clearUniqueFields);
        }
        this.mCreatedLocation = loadLocation(JSONUtils.getHashMap(itemJSON, JSON_KEY_CREATED_LOCATION));
        this.mUpdatedLocation = loadLocation(JSONUtils.getHashMap(itemJSON, JSON_KEY_UPDATED_LOCATION));
        this.mCreatedDuration = JSONUtils.getInt(itemJSON, "created_duration", null);
        this.mUpdatedDuration = JSONUtils.getInt(itemJSON, "updated_duration", null);
        this.mEditedDuration = JSONUtils.getInt(itemJSON, "edited_duration", null);
    }

    public RepeatableItemValue(RepeatableItemValue original) {
        this.mIndex = -1;
        this.mIdentifier = original.getIdentifier();
        this.mElement = original.getElement();
        this.mLocation = original.getLocation();
        this.mCreatedAt = original.getCreatedAt();
        this.mUpdatedAt = original.getUpdatedAt();
        this.mIndex = original.getIndex();
        if (original.getFormValues() != null) {
            this.mValues = original.getFormValues().copy();
        } else {
            this.mValues = new FormValueContainer(original.getElement().getElements());
        }
        this.mCreatedLocation = original.getCreatedLocation();
        this.mUpdatedLocation = original.getUpdatedLocation();
        this.mCreatedDuration = original.getCreatedDuration();
        this.mUpdatedDuration = original.getUpdatedDuration();
        this.mEditedDuration = original.getEditedDuration();
    }

    public RepeatableItemValue(RepeatableItemValue original, boolean locationCopy, boolean valuesCopy, int index) {
        this.mIndex = -1;
        this.mIdentifier = UUID.randomUUID().toString();
        this.mElement = original.getElement();
        this.mLocation = locationCopy ? original.getLocation() : null;
        this.mCreatedAt = null;
        this.mUpdatedAt = null;
        this.mIndex = index;
        if (valuesCopy) {
            this.mValues = original.getFormValues().copy(true);
        } else {
            this.mValues = new FormValueContainer(original.getElement().getElements());
        }
        this.mCreatedLocation = null;
        this.mUpdatedLocation = null;
        this.mCreatedDuration = null;
        this.mUpdatedDuration = null;
        this.mEditedDuration = null;
    }

    public RepeatableItemValue(Parcel parcel) {
        this.mIndex = -1;
        ClassLoader loader = getClass().getClassLoader();
        this.mIdentifier = parcel.readString();
        this.mElement = (RepeatableElement) parcel.readParcelable(loader);
        this.mLocation = (Location) parcel.readParcelable(null);
        this.mCreatedAt = ParcelUtils.readDate(parcel);
        this.mUpdatedAt = ParcelUtils.readDate(parcel);
        this.mIndex = parcel.readInt();
        this.mValues = (FormValueContainer) parcel.readParcelable(loader);
        this.mCreatedLocation = (Location) parcel.readParcelable(null);
        this.mUpdatedLocation = (Location) parcel.readParcelable(null);
        this.mCreatedDuration = (Integer) parcel.readValue(null);
        this.mUpdatedDuration = (Integer) parcel.readValue(null);
        this.mEditedDuration = (Integer) parcel.readValue(null);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mIdentifier);
        dest.writeParcelable(this.mElement, 0);
        dest.writeParcelable(this.mLocation, 0);
        ParcelUtils.writeDate(dest, this.mCreatedAt);
        ParcelUtils.writeDate(dest, this.mUpdatedAt);
        dest.writeInt(this.mIndex);
        dest.writeParcelable(this.mValues, 0);
        dest.writeParcelable(this.mCreatedLocation, 0);
        dest.writeParcelable(this.mUpdatedLocation, 0);
        dest.writeValue(this.mCreatedDuration);
        dest.writeValue(this.mUpdatedDuration);
        dest.writeValue(this.mEditedDuration);
    }

    public FormValueContainer getFormValues() {
        return this.mValues;
    }

    public FormValue getFormValue(String elementKey) {
        return this.mValues.getFormValue(elementKey);
    }

    public void setFormValue(Element element, FormValue value) {
        this.mValues.setFormValue(element, value);
    }

    public Location getLocation() {
        return this.mLocation;
    }

    public boolean hasLocation() {
        return this.mLocation != null;
    }

    public void setLocation(Location location) {
        this.mLocation = location;
    }

    public Date getCreatedAt() {
        return this.mCreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.mCreatedAt = createdAt;
    }

    public Date getUpdatedAt() {
        return this.mUpdatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.mUpdatedAt = updatedAt;
    }

    public Location getCreatedLocation() {
        return this.mCreatedLocation;
    }

    public void setCreatedLocation(Location location) {
        this.mCreatedLocation = location;
    }

    public Location getUpdatedLocation() {
        return this.mUpdatedLocation;
    }

    public void setUpdatedLocation(Location location) {
        this.mUpdatedLocation = location;
    }

    public Integer getCreatedDuration() {
        return this.mCreatedDuration;
    }

    public void setCreatedDuration(Integer seconds) {
        this.mCreatedDuration = seconds;
    }

    public Integer getUpdatedDuration() {
        return this.mUpdatedDuration;
    }

    public void setUpdatedDuration(Integer seconds) {
        this.mUpdatedDuration = seconds;
    }

    public Integer getEditedDuration() {
        return this.mEditedDuration;
    }

    public void setEditedDuration(Integer seconds) {
        this.mEditedDuration = seconds;
    }

    public LatLng getLatLng() {
        if (hasLocation()) {
            return LocationUtils.getLatLng(this.mLocation);
        }
        return null;
    }

    public void loadFromFeatureState(FeatureEditState state) {
        this.mLocation = state.getLocation();
        this.mCreatedAt = state.getCreatedAt();
        this.mUpdatedAt = state.getUpdatedAt();
        this.mValues = state.getFormValues();
        this.mCreatedLocation = state.getCreatedLocation();
        this.mUpdatedLocation = state.getUpdatedLocation();
        this.mCreatedDuration = state.getCreatedDuration();
        this.mUpdatedDuration = state.getUpdatedDuration();
        this.mEditedDuration = state.getEditedDuration();
    }

    public RepeatableElement getElement() {
        return this.mElement;
    }

    public String getIdentifier() {
        return this.mIdentifier;
    }

    public int getIndex() {
        return this.mIndex;
    }

    public void setIndex(int index) {
        this.mIndex = index;
    }

    public String getDisplayValue() {
        String displayValue = null;
        Element[] titleElements = this.mElement.getTitleElements();
        if (titleElements.length > 0) {
            boolean first = true;
            for (Element titleElement : titleElements) {
                if (titleElement != null) {
                    FormValue value = getFormValue(titleElement);
                    if (value != null) {
                        if (first) {
                            first = false;
                            displayValue = value.getDisplayValue();
                        } else {
                            displayValue = displayValue + ", " + value.getDisplayValue();
                        }
                    }
                }
            }
        }
        if (TextUtils.isEmpty(displayValue)) {
            return "Untitled";
        }
        return displayValue;
    }

    public String getSearchableValue() {
        return this.mValues.getSearchableValue();
    }

    public String toString() {
        return getDisplayValue();
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof RepeatableItemValue)) {
            return false;
        }
        RepeatableItemValue aRepeatableItemValue = (RepeatableItemValue) object;
        if (aRepeatableItemValue.canEqual(this)) {
            return TextUtils.equals(getIdentifier(), aRepeatableItemValue.getIdentifier());
        }
        return false;
    }

    public boolean canEqual(Object other) {
        return other instanceof RepeatableItemValue;
    }

    public int hashCode() {
        return this.mIdentifier != null ? this.mIdentifier.hashCode() : 0;
    }

    public int compareTo(@NonNull RepeatableItemValue item) {
        return Double.compare((double) getIndex(), (double) item.getIndex());
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = new HashMap();
        json.put("id", getIdentifier());
        if (hasLocation()) {
            json.put(JSON_GEOMETRY, new GeoJSONPoint(this.mLocation).toGeoJSON());
        }
        if (this.mCreatedAt != null) {
            json.put("created_at", DateUtils.formatEpochTimestamp(this.mCreatedAt));
        }
        if (this.mUpdatedAt != null) {
            json.put("updated_at", DateUtils.formatEpochTimestamp(this.mUpdatedAt));
        }
        json.put("form_values", this.mValues.toJSON());
        json.put(JSON_KEY_CREATED_LOCATION, saveLocation(this.mCreatedLocation));
        json.put(JSON_KEY_UPDATED_LOCATION, saveLocation(this.mUpdatedLocation));
        json.put("created_duration", this.mCreatedDuration);
        json.put("updated_duration", this.mUpdatedDuration);
        json.put("edited_duration", this.mEditedDuration);
        return json;
    }

    private FormValue getFormValue(Element element) {
        return this.mValues.getFormValue(element);
    }

    private Location loadLocation(Map json) {
        Location location = null;
        if (JSONUtils.hasKey(json, "latitude") && JSONUtils.hasKey(json, "longitude")) {
            location = new Location("Unknown");
            location.setLatitude(JSONUtils.getDouble(json, "latitude"));
            location.setLongitude(JSONUtils.getDouble(json, "longitude"));
            if (JSONUtils.hasKey(json, "altitude")) {
                location.setAltitude(JSONUtils.getDouble(json, "altitude"));
            }
            if (JSONUtils.hasKey(json, JSON_KEY_COURSE)) {
                location.setBearing((float) JSONUtils.getDouble(json, JSON_KEY_COURSE));
            }
            if (JSONUtils.hasKey(json, JSON_KEY_HORIZONTAL_ACCURACY)) {
                location.setAccuracy((float) JSONUtils.getDouble(json, JSON_KEY_HORIZONTAL_ACCURACY));
            }
        }
        return location;
    }

    private HashMap<String, Object> saveLocation(Location location) {
        Object obj = null;
        HashMap<String, Object> json = null;
        if (location != null) {
            Object valueOf;
            json = new HashMap();
            json.put("latitude", Double.valueOf(location.getLatitude()));
            json.put("longitude", Double.valueOf(location.getLongitude()));
            String str = "altitude";
            if (location.hasAltitude()) {
                valueOf = Double.valueOf(location.getAltitude());
            } else {
                valueOf = null;
            }
            json.put(str, valueOf);
            String str2 = JSON_KEY_HORIZONTAL_ACCURACY;
            if (location.hasAccuracy()) {
                obj = Float.valueOf(location.getAccuracy());
            }
            json.put(str2, obj);
        }
        return json;
    }
}
