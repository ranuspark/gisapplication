package com.example.bhumicloud.gisapplications.Activity;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement.StatusOption;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.io.IOException;
import java.util.List;

public class RecordStatusDialog extends ChoicePickerDialog {
    private static final String STATE_STATUS_FIELD = "STATE_STATUS_FIELD";
    private OnStatusOptionSelectedListener mListener;
    private List<StatusOption> mOptions;
    private StatusElement mStatusElement;

    public interface OnStatusOptionSelectedListener {
        void onStatusOptionSelected(StatusElement statusElement, StatusOption statusOption);
    }

    private static class StatusOptionDrawable extends GradientDrawable {
        public StatusOptionDrawable(Context context, StatusOption option) {
            setStroke(context.getResources().getDimensionPixelSize(R.dimen.status_color_stroke_width), -3355444);
            int statusColor = 0;
            try {
                statusColor = option.getColor();
            } catch (Throwable e) {
                GISLogger.log(e);
            }
            setColor(statusColor);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setChoiceMode(0);
        if (savedInstanceState != null) {
            String jsonString = savedInstanceState.getString(STATE_STATUS_FIELD);
            if (jsonString != null) {
                try {
                    setStatusElement(new StatusElement(null, JSONUtils.objectFromJSON(jsonString)));
                } catch (IOException e) {
                    throw new RuntimeException (e);
                }
            }
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (this.mStatusElement != null) {
            outState.putString(STATE_STATUS_FIELD, JSONUtils.toJSONString(this.mStatusElement.toJSON()));
        }
    }

    public void onDestroy() {
        this.mListener = null;
        super.onDestroy();
    }

    public int getCount() {
        if (this.mOptions != null) {
            return this.mOptions.size();
        }
        return 0;
    }

    public boolean isEmpty() {
        return this.mOptions == null || this.mOptions.isEmpty();
    }

    public StatusOption getItem(int position) {
        return (StatusOption) this.mOptions.get(position);
    }

    protected int getItemViewLayout() {
        return R.layout.list_item_record_status;
    }

    protected void onBindItemView(View view, int position) {
        Context context = getActivity();
        StatusOption option = getItem(position);
        view.findViewById(R.id.status_color).setBackgroundDrawable(new StatusOptionDrawable(context, option));
        ((TextView) view.findViewById(R.id.status_label)).setText(option.getLabel());
    }

    public void setStatusElement(StatusElement statusElement) {
        this.mStatusElement = statusElement;
        if (statusElement != null) {
            setTitle(statusElement.getLabel());
            this.mOptions = statusElement.getFilteredChoices();
        } else {
            this.mOptions = null;
        }
        notifyDataSetChanged();
    }

    public void setOnStatusOptionSelectedListener(OnStatusOptionSelectedListener listener) {
        this.mListener = listener;
    }

    protected boolean onChoiceItemClicked(ListView listView, int position, boolean selected) {
        if (this.mListener != null) {
            this.mListener.onStatusOptionSelected(this.mStatusElement, (StatusOption) this.mOptions.get(position));
        }
        return true;
    }
}
