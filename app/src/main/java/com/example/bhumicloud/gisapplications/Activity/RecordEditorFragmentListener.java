package com.example.bhumicloud.gisapplications.Activity;

import com.example.bhumicloud.gisapplications.model.Feature;

public interface RecordEditorFragmentListener {
    void onFeatureDeleted(Feature feature);

    void onFeatureEdited(Feature feature, boolean z);
}
