package com.example.bhumicloud.gisapplications.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Project;

import java.util.List;

public class ProjectPickerDialog extends ChoicePickerDialog {
    private static final String STATE_PROJECT_ID = "GIS:state:project";
    private ProjectPickerDialogListener mListener;
    private List<Project> mProjects;
    private long mSelectedProjectID;

    public interface ProjectPickerDialogListener {
        void onProjectSelected(Project project);
    }

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        if (savedState != null) {
            this.mSelectedProjectID = savedState.getLong(STATE_PROJECT_ID, -1);
        }
        setTitle((int) R.string.select_project);
        setChoiceMode(1);
        setNegativeButtonText((int) R.string.clear);
        reloadProjects();
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView list = getListView();
        if (this.mSelectedProjectID > 0) {
            for (int i = 0; i < this.mProjects.size(); i++) {
                if (((Project) this.mProjects.get(i)).getRowID() == this.mSelectedProjectID) {
                    list.setItemChecked(i, true);
                    return;
                }
            }
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(STATE_PROJECT_ID, this.mSelectedProjectID);
    }

    public void setSelectedProject(Project project) {
        if (project != null) {
            this.mSelectedProjectID = project.getRowID();
        } else {
            this.mSelectedProjectID = 0;
        }
    }

    public void setProjectPickerDialogListener(ProjectPickerDialogListener listener) {
        this.mListener = listener;
    }

    public boolean isEmpty() {
        return false;
    }

    public int getCount() {
        return this.mProjects.size();
    }

    public Project getItem(int position) {
        try {
            return (Project) this.mProjects.get(position);
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    protected void onNegativeButtonClicked() {
        super.onNegativeButtonClicked();
        ListView lv = getListView();
        lv.setItemChecked(lv.getCheckedItemPosition(), false);
        notifyProjectSelection();
        dismissAllowingStateLoss();
    }

    protected boolean onChoiceItemClicked(ListView listView, int position, boolean selected) {
        notifyProjectSelection();
        dismissAllowingStateLoss();
        return true;
    }

    private void reloadProjects() {
        this.mProjects = Project.getProjects(Account.getActiveAccount());
        notifyDataSetChanged();
    }

    private void notifyProjectSelection() {
        if (this.mListener != null) {
            this.mListener.onProjectSelected(getItem(getListView().getCheckedItemPosition()));
        }
    }
}
