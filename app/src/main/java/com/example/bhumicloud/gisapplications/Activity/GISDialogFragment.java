package com.example.bhumicloud.gisapplications.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.bhumicloud.gisapplications.util.GISLogger;

public abstract class GISDialogFragment extends DialogFragment {
    public void onCreate(Bundle savedInstanceState) {
        logDebugEvent("onCreate()");
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        logDebugEvent("onCreateView()");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        logDebugEvent("onCreateDialog()");
        return super.onCreateDialog(savedInstanceState);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        logDebugEvent("onCreateOptionsMenu()");
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void onAttach(Context context) {
        logDebugEvent("onAttach()");
        super.onAttach(context);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        logDebugEvent("onActivityCreated()");
        super.onActivityCreated(savedInstanceState);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        logDebugEvent("onActivityResult()");
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onViewStateRestored(Bundle savedInstanceState) {
        logDebugEvent("onViewStateRestored()");
        super.onViewStateRestored(savedInstanceState);
    }

    public void onStart() {
        logDebugEvent("onStart()");
        super.onStart();
    }

    public void onResume() {
        logDebugEvent("onResume()");
        super.onResume();
    }

    public void onPause() {
        logDebugEvent("onPause()");
        super.onPause();
    }

    public void onStop() {
        logDebugEvent("onStop()");
        super.onStop();
    }

    public void onDetach() {
        logDebugEvent("onDetach()");
        super.onDetach();
    }

    public void onDestroyView() {
        logDebugEvent("onDestroyView()");
        super.onDestroyView();
    }

    public void onDestroy() {
        logDebugEvent("onDestroy()");
        super.onDestroy();
    }

    public void onCancel(DialogInterface dialog) {
        logDebugEvent("onCancel()");
        super.onCancel(dialog);
    }

    public void onDismiss(DialogInterface dialog) {
        logDebugEvent("onDismiss()");
        super.onDismiss(dialog);
    }

    public void onLowMemory() {
        logDebugEvent("onLowMemory()");
        super.onLowMemory();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        logDebugEvent("onConfigurationChanged()");
        super.onConfigurationChanged(newConfig);
    }

    public void onSaveInstanceState(Bundle outState) {
        logDebugEvent("onSaveInstanceState()");
        super.onSaveInstanceState(outState);
    }

    public void onHiddenChanged(boolean hidden) {
        logDebugEvent("onHiddenChanged()");
        super.onHiddenChanged(hidden);
    }

    protected void logDebugEvent(String event) {
        GISLogger.log(getClass(), event);
    }
}
