package com.example.bhumicloud.gisapplications.dossier;

import android.support.v4.util.LruCache;

public abstract class RowCache<T> extends LruCache<Long, T> {
    public RowCache(int maxSize) {
        super(maxSize);
    }
}
