CREATE TABLE IF NOT EXISTS sync_states (
  _id INTEGER PRIMARY KEY AUTOINCREMENT,
  account_id INTEGER NOT NULL,
  resource TEXT NOT NULL,
  scope TEXT NOT NULL,
  state TEXT
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_sync_states_account_resource_scope ON sync_states (account_id ASC, resource ASC, scope ASC);

DROP INDEX IF EXISTS records_unsynced_idx;
