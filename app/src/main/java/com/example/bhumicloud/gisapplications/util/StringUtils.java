package com.example.bhumicloud.gisapplications.util;

import android.content.res.Resources;

public class StringUtils {
    public static String prependRightToLeft(String text) {
        return "‎" + text;
    }

    public static String correctRightToLeft(Resources resources, String text) {
        if (DeviceInfo.isRightToLeft(resources)) {
            return prependRightToLeft(text);
        }
        return text;
    }
}
