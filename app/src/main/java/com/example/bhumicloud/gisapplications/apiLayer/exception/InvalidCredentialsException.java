package com.example.bhumicloud.gisapplications.apiLayer.exception;

import android.content.Context;

import com.example.bhumicloud.gisapplications.R;

public class InvalidCredentialsException extends GISServiceException {
    public InvalidCredentialsException() {
        super("The username and password combination you provided is incorrect.");
    }

    public String getUserFriendlyMessage(Context context) {
        return context.getString(R.string.invalid_credentials_message);
    }
}
