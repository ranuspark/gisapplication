package com.example.bhumicloud.gisapplications.widget;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(View view, int i);
}
