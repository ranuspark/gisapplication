CREATE TRIGGER IF NOT EXISTS delete_attachment_files DELETE ON attachments
    BEGIN
        INSERT INTO trash_can (path) VALUES (OLD.path_one);
        INSERT INTO trash_can (path) VALUES (OLD.path_two);
        INSERT INTO trash_can (path) VALUES (OLD.thumbnail_path);
    END;