package com.example.bhumicloud.gisapplications.Activity;

import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableItemValue;

import java.util.List;

public interface RepeatableItemCollectionView {
    List<RepeatableItemValue> getItems();

    void setEditMode(boolean z);

    void setRepeatableItems(List<RepeatableItemValue> list);

    void setViewOnlyMode(boolean z);
}
