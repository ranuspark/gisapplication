package com.example.bhumicloud.gisapplications.model;

import android.text.TextUtils;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.SectionElement;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkDefault;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkElement;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkValue;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement.StatusOption;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusValue;
import com.example.bhumicloud.gisapplications.model.validation.Condition;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.example.bhumicloud.gisapplications.util.LocationUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DefaultValues {
    public static void applyStickyDefaults(Form form, Feature feature) {
        FormValueContainer formValueContainer = feature.getFormValues();
        HashMap<String, Object> stickyDefaults = form.getStickyDefaults();
        if (stickyDefaults != null && !stickyDefaults.isEmpty()) {
            Iterator it = flattenStickyElements(formValueContainer.getElements(), form).iterator();
            while (it.hasNext()) {
                Element element = (Element) it.next();
                Object stickyValueAsJSON = stickyDefaults.get(element.getKey());
                if (stickyValueAsJSON != null) {
                    FormValue fieldValue = FormValueContainer.inflateFormValue(element, stickyValueAsJSON);
                    FormValue currentValue = getFormValueFromValues(feature, element.getKey(), formValueContainer);
                    if (fieldValue != null && (currentValue == null || currentValue.isEmpty())) {
                        setFormValueInValues(feature, element.getKey(), fieldValue, formValueContainer);
                        if (element instanceof RecordLinkElement) {
                            Record linkedRecord = ((RecordLinkValue) fieldValue).getLastRecordLinkItemValue().getRecord(true);
                            if (linkedRecord != null) {
                                applyDefaultValuesForRecordLinkValue(form, (RecordLinkElement) element, linkedRecord, formValueContainer);
                            }
                        }
                    }
                }
            }
        }
    }

    public static void applyDefaultValuesForRecordLinkValue(Form form, RecordLinkElement recordLinkElement, Record linkedRecord, FormValueContainer formValues) {
        Iterator it = recordLinkElement.getRecordDefaults().iterator();
        while (it.hasNext()) {
            RecordLinkDefault recordLinkDefault = (RecordLinkDefault) it.next();
            FormValue sourceFormValue = linkedRecord.getFormValue(recordLinkDefault.getSourceKey());
            Element element = form.getElement(recordLinkDefault.getDestinationKey());
            if (element != null) {
                formValues.setFormValue(element, FormValueContainer.convertToNewElementType(element, sourceFormValue));
            }
        }
    }

    public static void applyDefaultValues(List<Element> elements, FormValueContainer formValueContainer, Record record) {
        if (record != null) {
            StatusElement statusElement = record.getForm().getStatusElement();
            if (statusElement != null && !TextUtils.isEmpty(statusElement.getDefaultValue()) && statusElement.isEnabled() && TextUtils.isEmpty(record.getRecordStatus())) {
                record.setRecordStatus(statusElement.getDefaultValue());
            }
            if (!record.isPersisted() && record.getProject() == null && record.getForm().isProjectsEnabled()) {
                long projectID = PatronSettings.getActiveProjectID(GIS.getInstance());
                if (projectID > 0 && record.getProject() == null) {
                    Project project = Project.findByID(projectID);
                    if (project != null && project.getDeletedAt() == null) {
                        record.setProject(project);
                    }
                }
            }
        }
        applyDefaultValuesRecursive(elements, formValueContainer);
    }

    public static void applyDefaultValues(Record record, Map<String, Object> defaults) {
        if (defaults != null) {
            if (defaults.containsKey(Record.COLUMN_LATITUDE) && defaults.containsKey(Record.COLUMN_LONGITUDE)) {
                record.setLocation(LocationUtils.create(Double.valueOf((String) defaults.get(Record.COLUMN_LATITUDE)).doubleValue(), Double.valueOf((String) defaults.get(Record.COLUMN_LONGITUDE)).doubleValue()));
            }
            Account account = record.getAccount();
            for (String key : defaults.keySet()) {
                Object obj = -1;
                switch (key.hashCode()) {
                    case -1969970175:
                        if (key.equals(Record.COLUMN_PROJECT_ID)) {
                            obj = 1;
                            break;
                        }
                        break;
                    case -892481550:
                        if (key.equals("status")) {
                            obj = 0;
                            break;
                        }
                        break;
                }

                switch ((int)obj) {
                    case 0 :
                        StatusElement statusElement = record.getForm().getStatusElement();
                        if (!statusElement.isReadOnly()) {
                            StatusOption option = statusElement.getOption((String) defaults.get("status"));
                            if (option == null) {
                                break;
                            }
                            record.setRecordStatus(option.getValue());
                            break;
                        }
                        break;
                    case 1:
                        record.setProject(Project.findByRemoteID(account, (String) defaults.get(Record.COLUMN_PROJECT_ID)));
                        break;
                    default:
                        Element element = record.getForm().getElementByDataName(key);
                        FormValue value = FormValueContainer.inflateFormValue(element, defaults.get(key));
                        if (element == null) {
                            break;
                        }
                        record.setFormValue(element, value);
                        break;
                }
            }
        }
    }

    public static void applyDefaultValuesRecursive(List<Element> elements, FormValueContainer formValueContainer) {
        for (Element element : elements) {
            if (element instanceof SectionElement) {
                applyDefaultValuesRecursive(((SectionElement) element).getElements(), formValueContainer);
            } else {
                applyDefaultValue(element, formValueContainer);
            }
        }
    }

    public static void applyDefaultValue(Element element, FormValueContainer formValueContainer) {
        String defaultValue = element.getDefaultValue();
        FormValue value = formValueContainer.getFormValue(element);
        if ((value == null || value.isEmpty()) && !TextUtils.isEmpty(defaultValue)) {
            FormValue fieldValue = FormValueContainer.inflateFormValue(element, defaultValue);
            if (fieldValue != null) {
                formValueContainer.setFormValue(element, fieldValue);
            }
        }
    }

    public static ArrayList<Element> flattenStickyElements(List<Element> elements, Form form) {
        ArrayList<Element> results = new ArrayList();
        if (form != null) {
            StatusElement statusElement = form.getStatusElement();
            if (statusElement != null && statusElement.isEnabled() && statusElement.isDefaultPreviousValueEnabled()) {
                results.add(statusElement);
            }
        }
        for (Element element : elements) {
            if (element instanceof SectionElement) {
                results.addAll(flattenStickyElements(((SectionElement) element).getElements(), null));
            } else if (element.isDefaultPreviousValueEnabled()) {
                results.add(element);
            }
        }
        return results;
    }

    public static FormValue getFormValueFromValues(Feature feature, String key, FormValueContainer formValues) {
        if ((feature instanceof Record) && key.equalsIgnoreCase(Condition.STATUS_ELEMENT_KEY)) {
            return ((Record) feature).getStatusValue();
        }
        return formValues.getFormValue(key);
    }

    public static void setFormValueInValues(Feature feature, String key, FormValue formValue, FormValueContainer formValueContainer) {
        if (!key.equalsIgnoreCase(Condition.STATUS_ELEMENT_KEY)) {
            formValueContainer.setFormValue(key, formValue);
        } else if ((formValue instanceof StatusValue) && (feature instanceof Record)) {
            ((Record) feature).setRecordStatus(((StatusValue) formValue).getValue());
        }
    }
}

