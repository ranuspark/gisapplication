package com.example.bhumicloud.gisapplications.model.validation.error;

import  com.example.bhumicloud.gisapplications.R;
import  com.example.bhumicloud.gisapplications.GIS;
import  com.example.bhumicloud.gisapplications.model.Record;
import  com.example.bhumicloud.gisapplications.model.field.Element;
import  com.example.bhumicloud.gisapplications.model.field.attachment.audio.AudioElement;
import  com.example.bhumicloud.gisapplications.model.field.attachment.photo.PhotoElement;
import  com.example.bhumicloud.gisapplications.model.field.attachment.video.VideoElement;
import  com.example.bhumicloud.gisapplications.model.field.choice.ChoiceElement;
import  com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableElement;
import  com.example.bhumicloud.gisapplications.model.field.textual.text.TextElement;
import  com.example.bhumicloud.gisapplications.model.validation.FeatureValidationError;

public class FieldLengthValidationError extends FeatureValidationError {

    private enum FieldLengthValidationErrorType {
        AT_LEAST,
        AT_MOST,
        BETWEEN,
        EXACTLY
    }

    public FieldLengthValidationError(Record record, Element element) {
        FieldLengthValidationErrorType errorType;
        if (element.hasMinLength() && element.hasMaxLength() && element.getMinLength() == element.getMaxLength()) {
            errorType = FieldLengthValidationErrorType.EXACTLY;
        } else if (element.hasMinLength() && element.hasMaxLength()) {
            errorType = FieldLengthValidationErrorType.BETWEEN;
        } else if (element.hasMinLength()) {
            errorType = FieldLengthValidationErrorType.AT_LEAST;
        } else if (element.hasMaxLength()) {
            errorType = FieldLengthValidationErrorType.AT_MOST;
        } else {
            return;
        }
        String label = record == null ? element.getLabel() : element.getAbsoluteLabel(record);
        if (element instanceof TextElement) {
            this.mMessage = makeTextElementMessage(errorType, label, element.getMinLength(), element.getMaxLength());
        } else if (element instanceof ChoiceElement) {
            this.mMessage = makeChoiceElementMessage(errorType, label, element.getMinLength(), element.getMaxLength());
        } else if (element instanceof PhotoElement) {
            this.mMessage = makePhotoElementMessage(errorType, label, element.getMinLength(), element.getMaxLength());
        } else if (element instanceof VideoElement) {
            this.mMessage = makeVideoElementMessage(errorType, label, element.getMinLength(), element.getMaxLength());
        } else if (element instanceof AudioElement) {
            this.mMessage = makeAudioElementMessage(errorType, label, element.getMinLength(), element.getMaxLength());
        } else if (element instanceof RepeatableElement) {
            this.mMessage = makeRepeatableElementMessage(errorType, label, element.getMinLength(), element.getMaxLength());
        }
    }

    public FieldLengthValidationError(Element element) {
        this(null, element);
    }

    private String makeTextElementMessage(FieldLengthValidationErrorType errorType, String label, int minLength, int maxLength) {
        if (errorType == null) {
            return "";
        }
        if (errorType == FieldLengthValidationErrorType.AT_LEAST) {
            return createMessage(R.plurals.field_length_at_least_text, label, minLength);
        }
        if (errorType == FieldLengthValidationErrorType.AT_MOST) {
            return createMessage(R.plurals.field_length_at_most_text, label, maxLength);
        }
        if (errorType != FieldLengthValidationErrorType.BETWEEN) {
            return createMessage(R.plurals.field_length_exactly_text, label, minLength);
        }
        return GIS.getInstance().getResources().getQuantityString(R.plurals.field_length_between_text, maxLength, new Object[]{label, Integer.valueOf(minLength), Integer.valueOf(maxLength)});
    }

    private String makeChoiceElementMessage(FieldLengthValidationErrorType errorType, String label, int minLength, int maxLength) {
        if (errorType == null) {
            return "";
        }
        if (errorType == FieldLengthValidationErrorType.AT_LEAST) {
            return createMessage(R.plurals.field_length_at_least_choice, label, minLength);
        }
        if (errorType == FieldLengthValidationErrorType.AT_MOST) {
            return createMessage(R.plurals.field_length_at_most_choice, label, maxLength);
        }
        if (errorType != FieldLengthValidationErrorType.BETWEEN) {
            return createMessage(R.plurals.field_length_exactly_choice, label, minLength);
        }
        return GIS.getInstance().getResources().getQuantityString(R.plurals.field_length_between_choice, maxLength, new Object[]{label, Integer.valueOf(minLength), Integer.valueOf(maxLength)});
    }

    private String makePhotoElementMessage(FieldLengthValidationErrorType errorType, String label, int minLength, int maxLength) {
        if (errorType == null) {
            return "";
        }
        if (errorType == FieldLengthValidationErrorType.AT_LEAST) {
            return createMessage(R.plurals.field_length_at_least_photos, label, minLength);
        }
        if (errorType == FieldLengthValidationErrorType.AT_MOST) {
            return createMessage(R.plurals.field_length_at_most_photos, label, maxLength);
        }
        if (errorType != FieldLengthValidationErrorType.BETWEEN) {
            return createMessage(R.plurals.field_length_exactly_photos, label, minLength);
        }
        return GIS.getInstance().getResources().getQuantityString(R.plurals.field_length_between_photos, maxLength, new Object[]{label, Integer.valueOf(minLength), Integer.valueOf(maxLength)});
    }

    private String makeVideoElementMessage(FieldLengthValidationErrorType errorType, String label, int minLength, int maxLength) {
        if (errorType == null) {
            return "";
        }
        if (errorType == FieldLengthValidationErrorType.AT_LEAST) {
            return createMessage( R.plurals.field_length_at_least_videos, label, minLength);
        }
        if (errorType == FieldLengthValidationErrorType.AT_MOST) {
            return createMessage( R.plurals.field_length_at_most_videos, label, maxLength);
        }
        if (errorType != FieldLengthValidationErrorType.BETWEEN) {
            return createMessage( R.plurals.field_length_exactly_videos, label, minLength);
        }
        return GIS.getInstance().getResources().getQuantityString( R.plurals.field_length_between_videos, maxLength, new Object[]{label, Integer.valueOf(minLength), Integer.valueOf(maxLength)});
    }

    private String makeAudioElementMessage(FieldLengthValidationErrorType errorType, String label, int minLength, int maxLength) {
        if (errorType == null) {
            return "";
        }
        if (errorType == FieldLengthValidationErrorType.AT_LEAST) {
            return createMessage( R.plurals.field_length_at_least_audios, label, minLength);
        }
        if (errorType == FieldLengthValidationErrorType.AT_MOST) {
            return createMessage( R.plurals.field_length_at_most_audios, label, maxLength);
        }
        if (errorType != FieldLengthValidationErrorType.BETWEEN) {
            return createMessage( R.plurals.field_length_exactly_audios, label, minLength);
        }
        return GIS.getInstance().getResources().getQuantityString( R.plurals.field_length_between_audios, maxLength, new Object[]{label, Integer.valueOf(minLength), Integer.valueOf(maxLength)});
    }

    private String makeRepeatableElementMessage(FieldLengthValidationErrorType errorType, String label, int minLength, int maxLength) {
        if (errorType == null) {
            return "";
        }
        if (errorType == FieldLengthValidationErrorType.AT_LEAST) {
            return createMessage( R.plurals.field_length_at_least_repeatable, label, minLength);
        }
        if (errorType == FieldLengthValidationErrorType.AT_MOST) {
            return createMessage( R.plurals.field_length_at_most_repeatable, label, maxLength);
        }
        if (errorType != FieldLengthValidationErrorType.BETWEEN) {
            return createMessage( R.plurals.field_length_exactly_repeatable, label, minLength);
        }
        return GIS.getInstance().getResources().getQuantityString( R.plurals.field_length_between_repeatable, maxLength, new Object[]{label, Integer.valueOf(minLength), Integer.valueOf(maxLength)});
    }

    private String createMessage(int resId, String label, int length) {
        return GIS.getInstance().getResources().getQuantityString(resId, length, new Object[]{label, Integer.valueOf(length)});
    }
}
