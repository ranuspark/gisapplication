package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableElement;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableValue;

public class RepeatableElementView extends ElementView<RepeatableElement> {
    private final TextView mItemCountLabel = ((TextView) findViewById(R.id.item_count_label));
    private RepeatableElementViewListener mListener;
    private final String mNoItems = getResources().getQuantityString(R.plurals.item_count, 0, new Object[]{Integer.valueOf(0)});

    public interface RepeatableElementViewListener {
        void onRepeatableElementViewSelected(RepeatableElement repeatableElement);
    }

    class C12411 implements OnClickListener {
        C12411() {
        }

        public void onClick(View v) {
            RepeatableElementView.this.onRepeatableElementViewClicked();
        }
    }

    public RepeatableElementView(Context context, RepeatableElement element, boolean viewOnly) {
        super(context, element, viewOnly);
        setOnClickListener(new C12411());
    }

    public void setRepeatableElementViewListener(RepeatableElementViewListener listener) {
        this.mListener = listener;
    }

    public void setValue(FormValue value) {
        if (value instanceof RepeatableValue) {
            this.mItemCountLabel.setText(((RepeatableValue) value).getDisplayValue());
            return;
        }
        this.mItemCountLabel.setText(this.mNoItems);
    }

    private void onRepeatableElementViewClicked() {
        onFieldViewClicked();
        if (this.mListener != null) {
            this.mListener.onRepeatableElementViewSelected((RepeatableElement) getElement());
        }
    }

    protected int getElementViewLayout() {
        return R.layout.view_repeatable_element;
    }
}
