package com.example.bhumicloud.gisapplications.apiLayer;

import java.util.HashMap;
import java.util.Map;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;

public interface GisService {
    public static final String API_FORMS = "/api/forms";
    public static final String API_PHOTOS = "/api/photos";
    public static final String API_PRIVATE_APP_GALLERY = "/api/_private/gallery";
    public static final String API_PRIVATE_USERS = "/api/_private/users";
    public static final String API_PRIVATE_USERS_CHECK = "/api/_private/users/check";
    public static final String API_PRIVATE_USERS_CREATE = "/api/_private/users/create";
    public static final String API_PRIVATE_USERS_RESET_PASSWORD = "/api/_private/users/send_password_reset";
    public static final String API_PRIVATE_USERS_UPDATE = "/api/_private/users/update";
    public static final String API_PRIVATE_USERS_UPLOAD_IMAGE = "/api/_private/users/upload_image";
    public static final String API_PROJECTS = "/api/projects";
    public static final String API_RECORDS = "/api/records";
    public static final String API_RECORD_HISTORY = "/api/records/history";
    public static final String API_REPORTS = "/api/reports";
    public static final String API_SYSTEM_RESOURCES = "/api/system_resources";
    public static final String API_USERS = "/api/users";
    public static final String API_V2 = "/api/";
    public static final String API_VIDEOS = "/api/videos";


    @POST("/api/_private/gallery/{appRemoteID}/use")
    Call<HashMap<String, Object>> addGalleryApp(@Path("appRemoteID") String str, @Body String str2);

    @GET("/api/_private/users/check")
    Call<HashMap<String, Object>> checkEmail(@Query("email") String str);

    @Streaming
    @GET("{path}")
    Call<ResponseBody> download(@Header("Accept") String str, @Path("path") String str2, @QueryMap(encoded = true) HashMap<String, String> hashMap);

    @GET("/api/users/getUser")
    Call<HashMap<String, Object>> getAccount();

    @GET("/api/users/getUA")
    Call<HashMap<String, Object>> getAccount(@Header("Authorization") String str);

    @GET("/api/_private/gallery")
    Call<HashMap<String, Object>> getAppGallery(@Query("featured") boolean z);

    @GET("/api/forms/getFormByUserAssignment")
    Call<HashMap<String, Object>> getForms(@Query("page") int i);

    @GET("/api/v2/photos/{remoteID}")
    Call<HashMap<String, Object>> getPhotoJson(@Path("remoteID") String str);

    @GET("/api/project/getProjectByUserId")
    Call<HashMap<String, Object>> getProjects(@Query("page") int i);

    @GET("/api/record/GetRecordByFormId?per_page=500")
    Call<HashMap<String, Object>> getRecordHistory(@Query("form_id") String str, @Query("sequence") long j, @Query("bounding_box") String str2);

    @GET("/api/record/GetRecordByFormId?per_page=500")
    Call<HashMap<String, Object>> getRecords(@Query("form_id") String str, @Query("sequence") long j, @Query("bounding_box") String str2);

    @Streaming
    @GET("/api/v2/system_resources/{resourceID}")
    Call<ResponseBody> getSystemResource(@Header("Accept") String str, @Path("resourceID") String str2);

    @POST("/api/_private/users/send_password_reset")
    Call<HashMap<String, Object>> resetPassword(@Body Map map);

    @POST("/api/record/USync")
    Call<HashMap<String, Object>> syncrecord(@Body Map map);

    @PUT("/api/_private/users/update")
    Call<HashMap<String, Object>> updateProfile(@Body Map map);

    @POST("/api/picture/setImage")
    @Multipart
    Call<HashMap<String, Object>> uploadPhoto(@Part MultipartBody.Part part, @PartMap Map<String, RequestBody> map);

    @POST("/api/_private/users/upload_image")
    @Multipart
    Call<HashMap<String, Object>> uploadProfileImage(@Part MultipartBody.Part part);

    @GET("/api/v2/messages")
    Call<HashMap<String, Object>> getMessages(@Query("app_version") String str, @Query("app_version_build") String str2, @Query("app_name") String str3, @Query("os") String str4, @Query("device") String str5, @Query("platform") String str6);


    @POST("/api/AppInfo/OInfoSet")
    Call<HashMap<String, Object>> openChangeset(@Body Map map);


    @PUT("/api/v2/changesets/{changeset}/close")
    Call<HashMap<String, Object>> closeChangeset(@Path("changeset") String str, @Body String str2);


}

