package com.example.bhumicloud.gisapplications.geoJson;

import android.location.Location;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GeoJSONPoint extends GeoJSONObject {
    private final Coordinate mCoordinate;

    public GeoJSONPoint(Map attributes) {
        List coordinates = JSONUtils.getArrayList(attributes, "coordinates");
        if (coordinates != null) {
            this.mCoordinate = new Coordinate(coordinates);
        } else {
            this.mCoordinate = new Coordinate(0.0d, 0.0d);
        }
    }

    public GeoJSONPoint(Location location) {
        this.mCoordinate = new Coordinate(location);
    }

    public String getType() {
        return GeoJSON.TYPE_POINT;
    }

    public Location toLocation() {
        return this.mCoordinate.toLocation();
    }

    public HashMap<String, Object> toGeoJSON() {
        HashMap<String, Object> json = super.toGeoJSON();
        json.put("coordinates", this.mCoordinate.toGeoJSON());
        return json;
    }

    public String toString() {
        return this.mCoordinate.toString();
    }
}