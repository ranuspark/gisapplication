package com.example.bhumicloud.gisapplications.model.field.repeatable;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.MultipleValueItem;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class RepeatableValue implements FormValue, Parcelable {
    public static final Creator<RepeatableValue> CREATOR = new C11991();
    private static final String PARCELABLE_ELEMENT_KEY = "repeatablevalue:element";
    private static final String PARCELABLE_ITEMS_KEY = "repeatablevalue:items";
    private final RepeatableElement mElement;
    private final Map<String, RepeatableItemValue> mItems;

    static class C11991 implements Creator<RepeatableValue> {
        C11991() {
        }

        public RepeatableValue createFromParcel(Parcel source) {
            return new RepeatableValue(source);
        }

        public RepeatableValue[] newArray(int size) {
            return new RepeatableValue[size];
        }
    }

    public static boolean isOrderDifferent(List<RepeatableItemValue> values1, List<RepeatableItemValue> values2) {
        if (values1 == null && values2 == null) {
            return false;
        }
        if (values1 == null) {
            return true;
        }
        if (values2 == null) {
            return true;
        }
        if (values1.isEmpty() && values2.isEmpty()) {
            return false;
        }
        if (values1.size() != values2.size()) {
            return true;
        }
        for (int index = 0; index < values1.size(); index++) {
            if (((RepeatableItemValue) values1.get(index)).getIndex() != ((RepeatableItemValue) values2.get(index)).getIndex()) {
                return true;
            }
        }
        return false;
    }

    public static void rebuildIndexes(List<RepeatableItemValue> list) {
        int index = 0;
        for (RepeatableItemValue item : list) {
            int index2 = index + 1;
            item.setIndex(index);
            index = index2;
        }
    }

    public RepeatableValue(RepeatableElement element) {
        this.mItems = new HashMap();
        this.mElement = element;
    }

    public RepeatableValue(RepeatableElement element, List itemsJSON, boolean clearUniqueFields) {
        this(element);
        setJSONAttributes(itemsJSON, clearUniqueFields);
    }

    public RepeatableValue(Parcel parcel) {
        this.mItems = new HashMap();
        Bundle bundle = parcel.readBundle(getClass().getClassLoader());
        this.mElement = (RepeatableElement) bundle.getParcelable(PARCELABLE_ELEMENT_KEY);
        try {
            setJSONAttributes(JSONUtils.arrayFromJSON(bundle.getString(PARCELABLE_ITEMS_KEY)), false);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PARCELABLE_ELEMENT_KEY, this.mElement);
        bundle.putString(PARCELABLE_ITEMS_KEY, JSONUtils.toJSONString(toJSON()));
        dest.writeBundle(bundle);
    }

    public RepeatableElement getElement() {
        return this.mElement;
    }

    public String getDisplayValue() {
        if (isEmpty()) {
            return "0 Items";
        }
        if (this.mItems.size() == 1) {
            return "1 Item";
        }
        return String.format("%d Items", new Object[]{Integer.valueOf(this.mItems.size())});
    }

    public String getSearchableValue() {
        if (isEmpty()) {
            return "";
        }
        ArrayList<String> tokens = new ArrayList();
        for (RepeatableItemValue item : getItems()) {
            String token = item.getSearchableValue();
            if (!TextUtils.isEmpty(token)) {
                tokens.add(token);
            }
        }
        return TextUtils.join(" ", tokens);
    }

    public int length() {
        return this.mItems.size();
    }

    public ArrayList<HashMap<String, Object>> toJSON() {
        if (isEmpty()) {
            return null;
        }
        ArrayList<HashMap<String, Object>> itemsJSON = new ArrayList();
        if (isEmpty()) {
            return itemsJSON;
        }
        for (RepeatableItemValue item : getSortedList()) {
            itemsJSON.add(item.toJSON());
        }
        return itemsJSON;
    }

    public boolean isEmpty() {
        return this.mItems.isEmpty();
    }

    public String getColumnValue() {
        return null;
    }

    public ArrayList<MultipleValueItem> getMultipleValues() {
        return null;
    }

    public boolean isEqualForConditions(String testValue) {
        return false;
    }

    public boolean contains(String testValue) {
        return false;
    }

    public boolean startsWith(String testValue) {
        return false;
    }

    public boolean isLessThan(String testValue) {
        return false;
    }

    public boolean isGreaterThan(String testValue) {
        return false;
    }

    public void setJSONAttributes(List attributes, boolean clearUniqueFields) {
        RepeatableElement element = getElement();
        if (attributes != null && !attributes.isEmpty()) {
            int index = 0;
            for (int i = 0; i < attributes.size(); i++) {
                Map itemJSON = JSONUtils.getHashMap(attributes, i);
                if (itemJSON != null) {
                    int index2 = index + 1;
                    String uuid;
                    if (clearUniqueFields) {
                        uuid = UUID.randomUUID().toString();
                    } else {
                        uuid = JSONUtils.getString(itemJSON, "id");
                    }
                    RepeatableItemValue item = new RepeatableItemValue(element, itemJSON, index, clearUniqueFields,uuid);
                    this.mItems.put(item.getIdentifier(), item);
                    index = index2;
                }
            }
        }
    }

    public void setItems(List<RepeatableItemValue> items) {
        this.mItems.clear();
        int index = 0;
        for (RepeatableItemValue item : items) {
            item.setIndex(index);
            this.mItems.put(item.getIdentifier(), item);
            index++;
        }
    }

    public Collection<RepeatableItemValue> getItems() {
        return this.mItems.values();
    }

    public List<RepeatableItemValue> getSortedList() {
        ArrayList<RepeatableItemValue> list = new ArrayList(getItems());
        Collections.sort(list);
        return list;
    }

    public void insertItem(RepeatableItemValue item) {
        if (item.getIndex() < 0) {
            item.setIndex(this.mItems.size());
        }
        this.mItems.put(item.getIdentifier(), item);
    }

    public void removeItem(RepeatableItemValue item) {
        this.mItems.remove(item.getIdentifier());
        rebuildIndexes(getSortedList());
    }
}
