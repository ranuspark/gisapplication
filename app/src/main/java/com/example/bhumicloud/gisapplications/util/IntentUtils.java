package com.example.bhumicloud.gisapplications.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.v4.app.ShareCompat.IntentBuilder;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.example.bhumicloud.gisapplications.R;
import  com.example.bhumicloud.gisapplications.model.field.attachment.Attachment.Type;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class IntentUtils {
    public static Intent viewMedia(Context context, File attachment, Type type) {
        Uri location = FileUtils.getShareableUriForFile(context, attachment);
        String mimeType = MimeUtils.getMimeType(location, type);
        Intent intent = new Intent ("android.intent.action.VIEW");
        intent.setDataAndType(location, mimeType);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        return showIntentSafe(context, intent) ? intent : null;
    }

    public static Intent viewPDF(Context context, File pdf) {
        Intent intent = new Intent ("android.intent.action.VIEW");
        intent.setDataAndType(FileUtils.getShareableUriForFile(context, pdf), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        return showIntentSafe(context, intent) ? intent : null;
    }

    public static Intent shareMedia(Activity activity, File attachment, Type type) {
        Uri uri = FileUtils.getShareableUriForFile(activity, attachment);
        Intent intent = IntentBuilder.from(activity).setStream(uri).setType(MimeUtils.getMimeType(uri, type)).getIntent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        return showIntentSafe(activity, intent) ? intent : null;
    }

    @TargetApi(16)
    public static Intent sendEmail(Activity activity, String[] to, String subject, File attachment) {
        String mimeType;
        int flags = 1073741824;
        Uri uri = FileUtils.getShareableUriForFile(activity, attachment);
        IntentBuilder builder = IntentBuilder.from(activity);
        if (to != null) {
            builder.setEmailTo(to);
        }
        if (!TextUtils.isEmpty(subject)) {
            builder.setSubject(subject);
        }
        if (attachment != null) {
            mimeType = MimeUtils.getMimeType(attachment, null);
            builder.setStream(uri);
            flags = 1073741824 | 1;
        } else {
            mimeType = "text/plain";
        }
        builder.setType(mimeType);
        Intent intent = builder.getIntent();
        intent.setFlags(flags);
        if (showIntentSafe(activity, intent)) {
            return intent;
        }
        return null;
    }

    public static Intent captureImage(Context context, File file) {
        Uri imageUri = FileUtils.getShareableUriForFile(context, file);
        Intent intent = new Intent ("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", imageUri);
        return showIntentSafe(context, intent) ? intent : null;
    }

    public static Intent getDrivingDirections(Context context, LatLng location) {
        return getDrivingDirections(context, location.latitude, location.longitude);
    }

    public static Intent getDrivingDirections(Context context, double latitude, double longitude) {
        Intent intent = new Intent ("android.intent.action.VIEW", Uri.parse("http://maps.google.com/maps?daddr=" + latitude + "," + longitude));
        return showIntentSafe(context, intent) ? intent : null;
    }

    public static boolean showIntentSafe(Context context, Intent intent) {
        boolean isIntentSafe = isIntentSafe(context, intent);
        if (!isIntentSafe) {
            Toast.makeText(context, context.getString(R.string.no_activity, new Object[]{intent.getAction(), intent.getDataString()}), Toast.LENGTH_LONG).show();
        }
        return isIntentSafe;
    }

    public static boolean isIntentSafe(Context context, Intent intent) {
        return context.getPackageManager().queryIntentActivities(intent,PackageManager.MATCH_DEFAULT_ONLY ).size() > 0;
    }

    public static Set<String> getNativeAppPackages(Context context, Uri uri) {
        PackageManager pm = context.getPackageManager();
        Set<String> allApps = extractPackagenames(pm.queryIntentActivities(new Intent ("android.intent.action.VIEW", Uri.parse("http://www.example.com")), 0));
        Set<String> nativeApps = extractPackagenames(pm.queryIntentActivities(new Intent ("android.intent.action.VIEW", uri), 0));
        nativeApps.removeAll(allApps);
        return nativeApps;
    }

    private static Set<String> extractPackagenames(List<ResolveInfo> resolveInfos) {
        Set<String> packageNameSet = new HashSet ();
        for (ResolveInfo ri : resolveInfos) {
            packageNameSet.add(ri.activityInfo.packageName);
        }
        return packageNameSet;
    }
}
