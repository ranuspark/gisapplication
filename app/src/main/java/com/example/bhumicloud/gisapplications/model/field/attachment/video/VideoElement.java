package com.example.bhumicloud.gisapplications.model.field.attachment.video;

import android.os.Parcel;
import android.os.Parcelable.Creator;

import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaElement;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.HashMap;
import java.util.Map;

public class VideoElement extends MediaElement {
    public static final Creator<VideoElement> CREATOR = new C11851();
    private static final String JSON_AUDIO_ENABLED = "audio_enabled";
    private static final String JSON_TRACK_ENABLED = "track_enabled";
    private boolean mAudioEnabled;
    private boolean mTrackEnabled;

    static class C11851 implements Creator<VideoElement> {
        C11851() {
        }

        public VideoElement createFromParcel(Parcel source) {
            return new VideoElement(source);
        }

        public VideoElement[] newArray(int size) {
            return new VideoElement[size];
        }
    }

    public VideoElement(Element parent, Map json) {
        super(parent, json);
    }

    private VideoElement(Parcel parcel) {
        super(parcel);
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = super.toJSON();
        json.put(JSON_TRACK_ENABLED, Boolean.valueOf(this.mTrackEnabled));
        json.put(JSON_AUDIO_ENABLED, Boolean.valueOf(this.mAudioEnabled));
        return json;
    }

    public boolean isTrackEnabled() {
        return this.mTrackEnabled;
    }

    public boolean isAudioEnabled() {
        return this.mAudioEnabled;
    }

    public String getType() {
        return Element.TYPE_VIDEO;
    }

    public void setJSONAttributes(Map json) {
        super.setJSONAttributes(json);
        this.mTrackEnabled = JSONUtils.getBoolean(json, JSON_TRACK_ENABLED, true);
        this.mAudioEnabled = JSONUtils.getBoolean(json, JSON_AUDIO_ENABLED, true);
    }
}
