package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import com.example.bhumicloud.gisapplications.model.field.classification.ClassificationElement;

public class ClassificationElementView extends ElementView<ClassificationElement> {
    private ClassificationElementViewListener mListener;

    public interface ClassificationElementViewListener {
        void onPresentClassificationDialog(ClassificationElement classificationElement);
    }

    public ClassificationElementView(Context context, ClassificationElement element, boolean viewOnly) {
        super(context, element, viewOnly);
    }

    public void setListener(ClassificationElementViewListener listener) {
        this.mListener = listener;
    }

    protected void onFieldViewClicked() {
        super.onFieldViewClicked();
        if (this.mListener != null) {
            this.mListener.onPresentClassificationDialog((ClassificationElement) getElement());
        }
    }
}
