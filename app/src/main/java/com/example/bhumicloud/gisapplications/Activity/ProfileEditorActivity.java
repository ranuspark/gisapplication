package com.example.bhumicloud.gisapplications.Activity;

import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.apiLayer.Client;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.example.bhumicloud.gisapplications.util.ActivityRequestCodes;
import com.example.bhumicloud.gisapplications.util.FileUtils;
import com.example.bhumicloud.gisapplications.util.FileUtils.GISDirectoryType;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.IntentUtils;
import com.example.bhumicloud.gisapplications.util.KeyboardUtils;
import com.example.bhumicloud.gisapplications.util.PermissionsUtils;
import com.example.bhumicloud.gisapplications.util.RoundedTransformation;
import com.example.bhumicloud.gisapplications.util.StreamUtils;


import com.squareup.picasso.Picasso;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;


public class ProfileEditorActivity extends GISActivity {
    private static final String TAG_INVALID_DATA = "TAG_INVALID_DATA";
    private static final String TAG_UPDATE_PROFILE_EXCEPTION_DIALOG = "TAG_UPDATE_PROFILE_EXCEPTION_DIALOG";
    private Account mAccount;
    private TextInputEditText mEmailInput;
    private TextInputEditText mFirstNameInput;
    private TextInputEditText mLastNameInput;
    private ImageView mProfileImageView;
    private ProgressDialog mProgressDialog;
    private UpdateProfileImageTask mUpdateProfileImageTask;
    private UpdateProfileTask mUpdateProfileTask;

    private static class UpdateProfileImageTask extends AsyncTask<File, Void, HashMap<String, Object>> {
        private final ProfileEditorActivity mActivity;
        private final Client mClient;
        private Exception mException;

        public UpdateProfileImageTask(ProfileEditorActivity activity, Account account) {
            this.mActivity = activity;
            this.mClient = new Client(this.mActivity, account);
        }

        protected HashMap<String, Object> doInBackground(File... args) {
            try {
                return this.mClient.uploadProfileImage(args[0]);
            } catch (IOException e) {
                this.mException = e;
                return null;
            }
        }

        protected void onPostExecute(HashMap<String, Object> result) {
            super.onPostExecute(result);
            this.mActivity.onUpdateProfile(result, this.mException, false);
        }
    }

    private static class UpdateProfileTask extends AsyncTask<String, Void, HashMap<String, Object>> {
        private final ProfileEditorActivity mActivity;
        private final Client mClient;
        private Exception mException;

        public UpdateProfileTask(ProfileEditorActivity activity, Account account) {
            this.mActivity = activity;
            this.mClient = new Client(this.mActivity, account);
        }

        protected HashMap<String, Object> doInBackground(String... args) {
            try {
                return this.mClient.updateProfile(args[0], args[1], args[2]);
            } catch (IOException e) {
                this.mException = e;
                return null;
            }
        }

        protected void onPostExecute(HashMap<String, Object> result) {
            super.onPostExecute(result);
            this.mActivity.onUpdateProfile(result, this.mException, true);
        }
    }

    private static File getProfilePhotoFile(Context context) {
        return new File(FileUtils.getGisDataDir (context, GISDirectoryType.thumbnails), "profile_photo.jpg");
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_editor);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        setTitle("Edit Profile");
        this.mAccount = Account.getActiveAccount(this);
        if (this.mAccount == null) {
            finish();
            return;
        }
        this.mProfileImageView = (ImageView) findViewById(R.id.profile_image);
        this.mFirstNameInput = (TextInputEditText) findViewById(R.id.profile_first_name);
        this.mLastNameInput = (TextInputEditText) findViewById(R.id.profile_last_name);
        this.mEmailInput = (TextInputEditText) findViewById(R.id.profile_email_address);
        this.mProgressDialog = new ProgressDialog(this);
        this.mProgressDialog.setMessage(getString(R.string.updating));
        this.mProgressDialog.setIndeterminate(true);
        this.mProgressDialog.setCancelable(false);
        this.mFirstNameInput.setText(this.mAccount.getFirstName());
        this.mLastNameInput.setText(this.mAccount.getLastName());
        this.mEmailInput.setText(this.mAccount.getUserEmailAddress());
        this.mProfileImageView.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Builder b = new Builder(ProfileEditorActivity.this);
                b.setItems(R.array.profile_photo_options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case PatronSettings.SYNC_MODE_ALWAYS /*0*/:
                                if (PermissionsUtils.checkPermissionsAndRequest(ProfileEditorActivity.this, 16)) {
                                    ProfileEditorActivity.this.startNativeCameraActivity();
                                    return;
                                }
                                return;
                            case PermissionsUtils.REQUEST_01 /*1*/:
                                if (PermissionsUtils.checkPermissionsAndRequest(ProfileEditorActivity.this, PermissionsUtils.STORAGE)) {
                                    ProfileEditorActivity.this.startPhotoGalleryActivity();
                                    return;
                                }
                                return;
                            default:
                                return;
                        }
                    }
                });
                b.create().show();
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_profile_editor, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_save_changes /*2131820942*/:
                if (this.mUpdateProfileTask != null && this.mUpdateProfileTask.getStatus() == Status.RUNNING) {
                    return true;
                }
                String firstName = this.mFirstNameInput.getText().toString();
                String lastName = this.mLastNameInput.getText().toString();
                String email = this.mEmailInput.getText().toString();
                ArrayList<String> errors = new ArrayList();
                if (TextUtils.isEmpty(firstName)) {
                    errors.add(getResources().getString(R.string.required_field_validation_error, new Object[]{"First Name"}));
                }
                if (TextUtils.isEmpty(lastName)) {
                    errors.add(getResources().getString(R.string.required_field_validation_error, new Object[]{"Last Name"}));
                }
                if (TextUtils.isEmpty(email)) {
                    errors.add(getResources().getString(R.string.required_field_validation_error, new Object[]{"Business Email"}));
                }
                if (errors.isEmpty()) {
                    KeyboardUtils.hideSoftKeyboard(this);
                    this.mProgressDialog.show();
                    this.mUpdateProfileTask = new UpdateProfileTask(this, this.mAccount);
                    this.mUpdateProfileTask.execute(new String[]{firstName.trim(), lastName.trim(), email.trim()});
                    return true;
                }
                GenericDialog dialog = new GenericDialog();
                dialog.setMessage(TextUtils.join("\n", errors));
                dialog.setPositiveButtonText((int) R.string.okay);
                dialog.show(getSupportFragmentManager(), TAG_INVALID_DATA);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case ActivityRequestCodes.REQUEST_TAKE_PHOTO /*101*/:
                    break;
                case ActivityRequestCodes.REQUEST_PICK_PHOTO /*102*/:
                    copyProfileImage(data);
                    break;
                case 203:
                    this.mProgressDialog.show();
                    this.mUpdateProfileImageTask = new UpdateProfileImageTask(this, this.mAccount);
                    this.mUpdateProfileImageTask.execute(new File[]{getProfilePhotoFile(this)});
                    break;
            }
            Uri fileUri = FileUtils.getShareableUriForFile(this, getProfilePhotoFile(this));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    protected void onPause() {
        super.onPause();
        if (this.mUpdateProfileImageTask != null) {
            this.mUpdateProfileImageTask.cancel(false);
        }
        if (this.mUpdateProfileTask != null) {
            this.mUpdateProfileTask.cancel(false);
        }
        if (this.mProgressDialog != null) {
            this.mProgressDialog.dismiss();
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean granted;
        if ((requestCode & 16) == 16) {
            if (grantResults.length > 0) {
                granted = true;
                for (int result : grantResults) {
                    if (result != 0) {
                        granted = false;
                    }
                }
                if (granted) {
                    startNativeCameraActivity();
                    return;
                }
            }
            Toast.makeText(this, R.string.permissions_required, Toast.LENGTH_SHORT).show();
        } else if ((requestCode & PermissionsUtils.STORAGE) == PermissionsUtils.STORAGE) {
            if (grantResults.length > 0) {
                granted = true;
                for (int result2 : grantResults) {
                    if (result2 != 0) {
                        granted = false;
                    }
                }
                if (granted) {
                    startPhotoGalleryActivity();
                    return;
                }
            }
            Toast.makeText(this, R.string.permissions_required, Toast.LENGTH_SHORT).show();
        }
    }

    private void onUpdateProfile(HashMap<String, Object> userResultObj, Exception exception, boolean finish) {
        if (userResultObj == null) {
            GenericDialog dialog = new GenericDialog();
            dialog.setMessage(getString(R.string.account_update_error, new Object[]{exception.getClass().getSimpleName()}));
            dialog.setPositiveButtonText((int) R.string.okay);
            dialog.show(getSupportFragmentManager(), TAG_UPDATE_PROFILE_EXCEPTION_DIALOG);
            return;
        }
        this.mAccount.synchronizeAccounts(userResultObj);
        this.mAccount = Account.getAccount(this.mAccount.getRowID(), true);
        Account.setActiveAccount(this.mAccount);
        if (this.mProgressDialog != null) {
            this.mProgressDialog.dismiss();
        }
        Intent data = new Intent();
        data.putExtra(SettingsFragment.ARG_ACCOUNT, this.mAccount);
        setResult(-1, data);
        if (finish) {
            finish();
        } else {

        }
    }

    public void startNativeCameraActivity() {
        Intent intent = IntentUtils.captureImage(this, getProfilePhotoFile(this));
        if (intent != null) {
            startActivityForResult(intent, ActivityRequestCodes.REQUEST_TAKE_PHOTO);
        }
    }

    private void startPhotoGalleryActivity() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.putExtra("android.intent.extra.LOCAL_ONLY", true);
        intent.setType("image/jpeg");
        intent.addCategory("android.intent.category.OPENABLE");
        if (IntentUtils.showIntentSafe(this, intent)) {
            startActivityForResult(intent, ActivityRequestCodes.REQUEST_PICK_PHOTO);
        }
    }

    private void copyProfileImage(Intent data) {
        Uri sourceUri = data.getData();
        if (sourceUri == null) {
            Toast.makeText(this, R.string.import_photo_failed, Toast.LENGTH_LONG).show();
            return;
        }
        File destination = getProfilePhotoFile(this);
        InputStream photoContent = null;
        try {
            photoContent = getContentResolver().openInputStream(sourceUri);
            StreamUtils.writeToFile(photoContent, destination);
            if (photoContent != null) {
                try {
                    photoContent.close();
                } catch (IOException e) {
                }
            }
        } catch (Throwable e2) {
            destination.delete();
            GISLogger.log(e2);
            if (photoContent != null) {
                try {
                    photoContent.close();
                } catch (IOException e3) {
                }
            }
        }
    }
}

