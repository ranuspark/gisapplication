package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.util.KeyboardUtils;

public abstract class ElementView<T extends Element> extends FrameLayout {
    private int mDefaultLabelColor ;
    private ImageView mDescriptionView;
    private final T mElement;
    private ElementViewDataSource mElementViewDataSource;
    private ElementViewListener mElementViewListener;
    private EditText mFieldView;
    private TextView mLabelView;
    private TextView mRequiredView;
    private final boolean mViewOnly;

    public interface ElementViewListener {
        void onFormValueDidChange(Element element, FormValue formValue);

        void onShowElementDescriptionDialog(Element element);
    }

    public interface ElementViewDataSource {
        Record getEditingRecord();
    }


    public ElementView(Context context, T element, boolean viewOnly) {
        super(context);
        setTag(element.getDataName());
        this.mElement = element;
        this.mViewOnly = viewOnly;
        initialize();
        if (this.mFieldView != null) {
            this.mFieldView.setTag(element.getDataName() + ":edit");
        }
        update();
    }

    public T getElement() {
        return this.mElement;
    }

    public void setElementViewListener(ElementViewListener listener) {
        this.mElementViewListener = listener;
    }

    public void setElementViewDataSource(ElementViewDataSource dataSource) {
        this.mElementViewDataSource = dataSource;
    }

    public boolean isVisible() {
        return getVisibility() == View.VISIBLE;
    }

    public void setVisible(boolean visible) {
        setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    public void setValue(FormValue value) {
        if (this.mFieldView == null) {
            return;
        }
        if (value == null) {
            this.mFieldView.setText(null);
        } else {
            this.mFieldView.setText(value.getDisplayValue());
        }
    }

    public void setRequired(boolean required) {
        if (this.mRequiredView == null) {
            return;
        }
        if (required) {
            this.mRequiredView.setVisibility(View.VISIBLE);
        } else {
            this.mRequiredView.setVisibility(View.INVISIBLE);
        }
    }

    public void onDestroy(Context context) {
        this.mElementViewListener = null;
        this.mElementViewDataSource = null;
    }

    public void update() {
        Element element = getElement();
        if (this.mLabelView != null) {
            this.mLabelView.setText(element.getLabel());
            if (element.isDisabled()) {
                this.mLabelView.setTextColor(-3355444);
            } else {
                this.mLabelView.setTextColor(this.mDefaultLabelColor);
            }
        }
        if (this.mDescriptionView != null) {
            if (element.hasDescription()) {
                this.mDescriptionView.setVisibility(View.VISIBLE);
                this.mDescriptionView.setEnabled(true);
            } else {
                this.mDescriptionView.setVisibility(View.GONE);
                this.mDescriptionView.setEnabled(false);
            }
        }
        if (this.mFieldView != null) {
            this.mFieldView.setEnabled(element.isEnabled());
            this.mFieldView.setId(element.getKey().hashCode());
        }
    }

    protected void initialize() {
        LayoutInflater.from(getContext()).inflate(getElementViewLayout(), this, true);
        this.mLabelView = (TextView) findViewById(R.id.element_label);
        this.mFieldView = (EditText) findViewById(R.id.element_field);
        this.mRequiredView = (TextView) findViewById(R.id.element_required);
        this.mDescriptionView = (ImageView) findViewById(R.id.element_info);
        this.mDefaultLabelColor = this.mLabelView.getCurrentTextColor();
        if (this.mDescriptionView != null) {
            this.mDescriptionView.setOnClickListener ( new OnClickListener () {
                @Override
                public void onClick(View v) {
                    ElementView.this.onDescriptionViewClicked();
                }
            } );
        }
        if (this.mFieldView != null) {
            this.mFieldView.setFocusable(false);
            this.mFieldView.setFocusableInTouchMode(false);
            this.mFieldView.setSaveEnabled(false);
            this.mFieldView.setOnClickListener ( new OnClickListener () {
                @Override
                public void onClick(View v) {
                    if (!ElementView.this.isReadOnly()) {
                        ElementView.this.onFieldViewClicked();
                    }
                }
            } );
        }
    }

    protected int getElementViewLayout() {
        return R.layout.view_base_element;
    }

    protected void onFieldViewClicked() {
        if (this.mFieldView == null || !this.mFieldView.hasFocus()) {
            KeyboardUtils.hideSoftKeyboard(getContext(), this);
            this.mLabelView.requestFocus();
        }
    }

    protected void onDescriptionViewClicked() {
        if (this.mElementViewListener != null) {
            this.mElementViewListener.onShowElementDescriptionDialog(getElement());
        }
    }

    protected void notifyOnFormValueDidChange(FormValue value) {
        if (this.mElementViewListener != null) {
            this.mElementViewListener.onFormValueDidChange(getElement(), value);
        }
    }

    protected boolean isReadOnly() {
        return getElement().isDisabled() || this.mViewOnly;
    }

    protected Record getRecord() {
        if (this.mElementViewDataSource != null) {
            return this.mElementViewDataSource.getEditingRecord();
        }
        return null;
    }

    protected void setValue(String text) {
        if (this.mFieldView != null) {
            if (text == null) {
                text = "";
            }
            this.mFieldView.setTextKeepState(text);
        }
    }

    protected String getFieldValue() {
        return this.mFieldView != null ? this.mFieldView.getText().toString() : null;
    }

    protected void clearFieldValue() {
        if (this.mFieldView != null) {
            this.mFieldView.setText(null);
        }
    }

    protected EditText getFieldView() {
        return this.mFieldView;
    }

    protected void setFieldTextColor(int color) {
        this.mFieldView.setTextColor(color);
    }

    protected void addFieldTextChangedListener(TextWatcher textWatcher) {
        if (this.mFieldView != null) {
            this.mFieldView.addTextChangedListener(textWatcher);
        }
    }

    protected void setFieldInputType(int inputType) {
        if (this.mFieldView != null) {
            this.mFieldView.setInputType(inputType);
        }
    }

    protected void setFieldImeOptions(int imeOptions) {
        if (this.mFieldView != null) {
            this.mFieldView.setImeOptions(imeOptions);
        }
    }

    protected void setFieldKeyListener(KeyListener keyListener) {
        if (this.mFieldView != null) {
            this.mFieldView.setKeyListener(keyListener);
        }
    }
}
