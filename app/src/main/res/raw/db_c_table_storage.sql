CREATE TABLE IF NOT EXISTS storage (
    _id INTEGER PRIMARY KEY AUTOINCREMENT,
    account_id INTEGER NOT NULL,
    scope TEXT,
    key TEXT,
    value TEXT
);
CREATE INDEX IF NOT EXISTS storage_account_id_scope_key_idx ON storage (account_id, scope, key);