package com.example.bhumicloud.gisapplications.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.Activity.GISActivity.OnBackPressedListener;
import com.example.bhumicloud.gisapplications.model.DefaultValues;
import com.example.bhumicloud.gisapplications.model.FeatureEditState;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.javascript.expression.ExpressionEngine;
import com.example.bhumicloud.gisapplications.util.FileBasedStateStorage;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class RecordEditorActivity extends LocationListenerActivity {
    public static final String EXTRA_DUPLICATE_ID = "RecordEditorActivity.EXTRA_DUPLICATE_ID";
    public static final String EXTRA_DUPLICATE_LOCATION = "RecordEditorActivity.EXTRA_DUPLICATE_LOCATION";
    public static final String EXTRA_DUPLICATE_VALUES = "RecordEditorActivity.EXTRA_DUPLICATE_VALUES";
    public static final String EXTRA_FORM_ID = "RecordEditorActivity.EXTRA_FORM_ID";
    public static final String EXTRA_RECORD_DEFAULTS = "RecordEditorActivity.EXTRA_RECORD_DEFAULTS";
    public static final String EXTRA_RECORD_ID = "RecordEditorActivity.EXTRA_RECORD_ID";
    public static final String EXTRA_VIEW_ONLY_MODE = "RecordEditorActivity.EXTRA_VIEW_ONLY_MODE";
    private static final String TAG_STORAGE_FRAGMENT = "RecordEditorActivity.STORAGE";
    private Fragment fragment;
    private int request;
    private Attachment attachment;

    public static class StorageFragment extends GISFragment {
        private static final String STATE_FILE_SAVED_STORAGE = "STATE_FILE_SAVED_STORAGE";
        private static final String STATE_HAS_RECORD = "STATE_HAS_RECORD";
        private static final String STATE_INSERT_ATTACHMENTS = "STATE_INSERT_ATTACHMENTS";
        private static final String STATE_RECORD = "STATE_RECORD";
        private static final String STATE_REMOVE_ATTACHMENTS = "STATE_REMOVE_ATTACHMENTS";
        private final Set<Attachment> mAddRemovedAttachments = new HashSet();
        private ExpressionEngine mExpressionEngine;
        private final Set<Attachment> mInsertAttachments = new HashSet();
        private Record mRecord;
        private final Set<Attachment> mRemoveAttachments = new HashSet();

        public void onCreate(Bundle savedState) {
            super.onCreate(savedState);
            if (savedState != null) {
                if (savedState.getInt(STATE_HAS_RECORD) == 1) {
                    FileBasedStateStorage stateStorage = (FileBasedStateStorage) savedState.getParcelable(STATE_FILE_SAVED_STORAGE);
                    if (stateStorage != null) {
                        this.mRecord = (Record) stateStorage.get(STATE_RECORD);
                    } else {
                        GISLogger.log("FileBasedStateStorage was null");
                    }
                }
                List<Attachment> insert = savedState.getParcelableArrayList(STATE_INSERT_ATTACHMENTS);
                if (insert != null) {
                    this.mInsertAttachments.addAll(insert);
                }
                List<Attachment> remove = savedState.getParcelableArrayList(STATE_REMOVE_ATTACHMENTS);
                if (remove != null) {
                    this.mRemoveAttachments.addAll(remove);
                }
            }
        }

        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
            if (this.mRecord == null) {
                outState.putInt(STATE_HAS_RECORD, 0);
            } else {
                outState.putInt(STATE_HAS_RECORD, 1);
                FileBasedStateStorage stateStorage = new FileBasedStateStorage(getActivity());
                stateStorage.store(STATE_RECORD, this.mRecord);
                outState.putParcelable(STATE_FILE_SAVED_STORAGE, stateStorage);
            }
            ArrayList<Attachment> insert = new ArrayList();
            insert.addAll(this.mInsertAttachments);
            outState.putParcelableArrayList(STATE_INSERT_ATTACHMENTS, insert);
            ArrayList<Attachment> remove = new ArrayList();
            remove.addAll(this.mRemoveAttachments);
            outState.putParcelableArrayList(STATE_REMOVE_ATTACHMENTS, remove);
        }

        public void onDestroy() {
            super.onDestroy();
        }

        public Record getRecord() {
            return this.mRecord;
        }

        public void setRecord(Record record) {
            this.mRecord = record;
        }

        public void insertAttachment(Attachment attachment) {
            this.mInsertAttachments.add(attachment);
            this.mRemoveAttachments.remove(attachment);
        }

        public void removeAttachment(Attachment attachment) {
            if (attachment != null) {
                if (this.mInsertAttachments.contains(attachment)) {
                    this.mAddRemovedAttachments.add(attachment);
                    this.mInsertAttachments.remove(attachment);
                }
                this.mRemoveAttachments.add(attachment);
            }
        }

        public void clearAttachments() {
            this.mInsertAttachments.clear();
            this.mAddRemovedAttachments.clear();
            this.mRemoveAttachments.clear();
        }

        public Set<Attachment> getInsertedAttachments() {
            return this.mInsertAttachments;
        }

        public Set<Attachment> getAddRemovedAttachments() {
            return this.mAddRemovedAttachments;
        }

        public Set<Attachment> getRemovedAttachments() {
            return this.mRemoveAttachments;
        }

        public ExpressionEngine getExpressionEngine() {
            if (this.mExpressionEngine != null) {
                return this.mExpressionEngine;
            }
            this.mExpressionEngine = new ExpressionEngine(getActivity());
            return this.mExpressionEngine;
        }

        public void releaseExpressionEngine() {
            if (this.mExpressionEngine != null) {
                this.mExpressionEngine.setExpressionEngineListener(null);
                this.mExpressionEngine.release();
                this.mExpressionEngine = null;
            }
        }
    }

    public static Intent getNewRecordIntent(Context context, long formID) {
        Intent i = new Intent(context, RecordEditorActivity.class);
        i.putExtra(EXTRA_FORM_ID, formID);
        return i;
    }

    public static Intent getEditRecordIntent(Context context, long recordID) {
        Intent i = new Intent(context, RecordEditorActivity.class);
        i.putExtra(EXTRA_RECORD_ID, recordID);
        return i;
    }

    public static Intent getEditRecordIntent(Context context, long recordID, boolean viewOnlyMode) {
        Intent i = new Intent(context, RecordEditorActivity.class);
        i.putExtra(EXTRA_RECORD_ID, recordID);
        i.putExtra(EXTRA_VIEW_ONLY_MODE, viewOnlyMode);
        return i;
    }

    public static Intent getDuplicateRecordIntent(Context context, long recordID, boolean duplicateLocation, boolean duplicateValues) {
        Intent i = new Intent(context, RecordEditorActivity.class);
        i.putExtra(EXTRA_DUPLICATE_ID, recordID);
        i.putExtra(EXTRA_DUPLICATE_LOCATION, duplicateLocation);
        i.putExtra(EXTRA_DUPLICATE_VALUES, duplicateValues);
        return i;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        if (savedInstanceState == null) {
            long formID = -1;
            long recordID = -1;
            long sourceRecordID = -1;
            HashMap<String, Object> defaults = null;
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                formID = extras.getLong(EXTRA_FORM_ID, -1);
                recordID = extras.getLong(EXTRA_RECORD_ID, -1);
                sourceRecordID = extras.getLong(EXTRA_DUPLICATE_ID, -1);
                defaults = JSONUtils.tryObjectFromJSON(extras.getString(EXTRA_RECORD_DEFAULTS));
            }
            Record record = null;
            if (sourceRecordID > 0) {
                record = Record.duplicate(sourceRecordID, extras.getBoolean(EXTRA_DUPLICATE_LOCATION), extras.getBoolean(EXTRA_DUPLICATE_VALUES));
            } else if (recordID > 0) {
                record = Record.getRecord(recordID);
            } else if (formID > 0) {
                Form form = Form.getForm(formID);
                if (form != null) {
                    record = new Record(form);
                }
            }
            if (record == null) {
                GISLogger.log(new IllegalStateException("A record could not be generated from sourceRecordID: " + sourceRecordID + " recordID: " + recordID + " formID: " + formID));
                Toast.makeText(this, R.string.error_editing_record, Toast.LENGTH_LONG).show();
                finish();
                return;
            }
            RecordEditorFragment editor = new RecordEditorFragment();
            editor.setRootEditor(true);
            editor.setAddMode(!record.isPersisted());
            editor.setViewOnlyMode(extras.getBoolean(EXTRA_VIEW_ONLY_MODE, false));
            editor.setFeatureStateBeforeEdit(record);
            DefaultValues.applyDefaultValues(record, defaults);
            DefaultValues.applyStickyDefaults(record.getForm(), record);
            DefaultValues.applyDefaultValues(record.getForm().getElements(), record.getFormValues(), record);
            GISLogger.setActiveForm(record.getForm());
            setRecord(record);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container, editor, "root-editor");
            ft.commit();
        }
    }

    public void onBackPressed() {
        Fragment fragment = getCurrentFragment();
        if (fragment == null || !(fragment instanceof OnBackPressedListener)) {
            super.onBackPressed();
        } else if (((OnBackPressedListener) fragment).onBackPressed()) {
            super.onBackPressed();
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean granted;
        if ((requestCode & 64) == 64) {
            if (grantResults.length > 0) {
                granted = true;
                for (int result : grantResults) {
                    if (result != 0) {
                        granted = false;
                    }
                }
                if (granted) {
                    startLocationUpdates();
                    return;
                }
            }
            finish();
            Toast.makeText(this, R.string.permissions_required, Toast.LENGTH_SHORT).show();
        } else if ((requestCode & 16) == 16) {
            if (grantResults.length > 0) {
                granted = true;
                for (int result2 : grantResults) {
                    if (result2 != 0) {
                        granted = false;
                    }
                }
                if (granted) {
                    fragment = getCurrentFragment();
                    if (fragment instanceof RecordEditorFragment) {
                        request = Integer.lowestOneBit(requestCode);
                        if (request == 1) {
                            ((RecordEditorFragment) fragment).startNativeCameraActivity();
                            return;
                        } else if (request == 2) {
                            ((RecordEditorFragment) fragment).startBarcodeScanner();
                            return;
                        } else {
                            return;
                        }
                    }
                    return;
                }
            }
            Toast.makeText(this, R.string.permissions_required, Toast.LENGTH_SHORT).show();
        } else if ((requestCode & 128) == 128) {
            if (grantResults.length > 0) {
                granted = true;
                for (int result22 : grantResults) {
                    if (result22 != 0) {
                        granted = false;
                    }
                }
                if (granted) {
                    fragment = getCurrentFragment();
                    if (fragment instanceof RecordEditorFragment) {
                        request = Integer.lowestOneBit(requestCode);
                        if (request == 1) {
                            ((RecordEditorFragment) fragment).startPhotoGalleryActivity();
                            return;
                        } else if (request == 2) {
                            //((RecordEditorFragment) fragment).startVideoImportActivity();
                            return;
                        } else if (request == 4) {
                            //((RecordEditorFragment) fragment).startAudioImportActivity();
                            return;
                        } else {
                            return;
                        }
                    }
                    return;
                }
            }
            Toast.makeText(this, R.string.permissions_required, Toast.LENGTH_SHORT).show();
        }
    }

    public ExpressionEngine getExpressionEngine() {
        return getStorageFragment().getExpressionEngine();
    }

    public void releaseExpressionEngine() {
        getStorageFragment().releaseExpressionEngine();
    }

    public Record getRecord() {
        return getStorageFragment().getRecord();
    }

    public void pushFragment(Fragment oldFragment, Fragment newFragment, String tag) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.detach(oldFragment);
        ft.add(R.id.container, newFragment, tag);
        ft.addToBackStack(null);
        try {
            ft.commit();
        } catch (Throwable e) {
            GISLogger.log(e);
        }
    }

    public void insertAttachment(Attachment attachment) {
        getStorageFragment().insertAttachment(attachment);
    }

    public void removeAttachment(Attachment attachment) {
        getStorageFragment().removeAttachment(attachment);
    }

    public void restoreAttachments(FeatureEditState state) {
        Set<String> oldMedia = getRecord().getFormValues().getMediaValues();
        Set<String> newMedia = state.getFormValues().getMediaValues();
        HashSet<String> deletes = new HashSet(oldMedia);
        HashSet<String> inserts = new HashSet(newMedia);
        deletes.removeAll(newMedia);
        inserts.removeAll(oldMedia);
        Iterator it = deletes.iterator();
        while (it.hasNext()) {
            Attachment attachment = Attachment.findByRemoteID((String) it.next());
            if (attachment != null) {
                removeAttachment(attachment);
            }
        }
        it = inserts.iterator();
        while (it.hasNext()) {
            attachment = Attachment.findByRemoteID((String) it.next());
            if (attachment != null) {
                insertAttachment(attachment);
            }
        }
        persistAttachmentChanges();
    }

    public void persistAttachmentChanges() {
        for (Attachment attachment : getStorageFragment().getInsertedAttachments()) {
            attachment.setRecord(getRecord());
            attachment.save();
        }
        for (Attachment attachment2 : getStorageFragment().getRemovedAttachments()) {
            attachment2.setRecordID(null);
            attachment2.save();
        }
        getStorageFragment().clearAttachments();
    }

    public void discardAttachmentChanges() {
        for (Attachment attachment : getStorageFragment().getInsertedAttachments()) {
            attachment.setRecordID(null);
            attachment.save();
        }
        for (Attachment attachment2 : getStorageFragment().getAddRemovedAttachments()) {
            attachment2.setRecordID(null);
            attachment2.save();
        }
        getStorageFragment().clearAttachments();
    }

    private void setRecord(Record record) {
        getStorageFragment().setRecord(record);
    }

    private StorageFragment getStorageFragment() {
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentByTag(TAG_STORAGE_FRAGMENT);
        if (fragment != null) {
            return (StorageFragment) fragment;
        }
        Fragment storage = new StorageFragment();
        storage.setRetainInstance(true);
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(storage, TAG_STORAGE_FRAGMENT);
        ft.commitAllowingStateLoss();
        fm.executePendingTransactions();
        return (StorageFragment) storage;
    }
}

