CREATE TABLE IF NOT EXISTS Records (
    _id INTEGER PRIMARY KEY AUTOINCREMENT,
    remote_id TEXT,
    unique_id TEXT,
    account_id INTEGER,
    form_id INTEGER,
    project_id INTEGER,
    assigned_to TEXT,
    status TEXT,
    title TEXT,
    latitude REAL,
    longitude REAL,
    altitude REAL,
    bearing REAL,
    accuracy REAL,
    form_values TEXT,
    is_draft INTEGER,
    is_synchronized INTEGER,
    synchronized_at REAL,
    created_at REAL,
    updated_at REAL,
    version INTEGER,
    values_index_row_id INTEGER,
    is_new INTEGER NOT NULL DEFAULT 0,
    created_latitude REAL,
    created_longitude REAL,
    created_altitude REAL,
    created_horizontal_accuracy REAL,
    updated_latitude REAL,
    updated_longitude REAL,
    updated_altitude REAL,
    updated_horizontal_accuracy REAL,
    created_duration INTEGER,
    updated_duration INTEGER,
    edited_duration INTEGER
);
CREATE INDEX IF NOT EXISTS records_account_id_idx ON Records (account_id);
CREATE INDEX IF NOT EXISTS records_account_id_remote_id_idx ON Records (account_id, remote_id);
CREATE INDEX IF NOT EXISTS records_form_latitude_idx ON Records (form_id, latitude);
CREATE INDEX IF NOT EXISTS records_form_status_idx ON Records (form_id, status);
CREATE INDEX IF NOT EXISTS records_form_created_at_idx ON Records (form_id, created_at);
CREATE INDEX IF NOT EXISTS records_form_updated_at_idx ON Records (form_id, updated_at);
CREATE INDEX IF NOT EXISTS records_form_remote_id_idx ON Records (form_id, remote_id);
CREATE INDEX IF NOT EXISTS records_form_latitude_longitude_idx ON Records (form_id, latitude, longitude);
CREATE INDEX IF NOT EXISTS records_form_project_idx ON Records (form_id, project_id);
CREATE INDEX IF NOT EXISTS records_latitude_longitude_idx ON Records (latitude, longitude);
CREATE INDEX IF NOT EXISTS values_index_row_id_idx ON Records (values_index_row_id);
CREATE INDEX IF NOT EXISTS records_title_nocase_idx ON Records (form_id, title COLLATE NOCASE ASC);