package com.example.bhumicloud.gisapplications.model.field;

import android.os.Parcel;

import java.util.Map;

public class LabelElement extends Element {
    public static final Creator<LabelElement> CREATOR = new C11741();

    static class C11741 implements Creator<LabelElement> {
        C11741() {
        }

        public LabelElement createFromParcel(Parcel source) {
            return new LabelElement(source);
        }

        public LabelElement[] newArray(int size) {
            return new LabelElement[size];
        }
    }

    public LabelElement(Element parent, Map json) {
        super(parent, json);
    }

    private LabelElement(Parcel parcel) {
        super(parcel);
    }

    public String getType() {
        return Element.TYPE_LABEL;
    }
}
