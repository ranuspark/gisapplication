package com.example.bhumicloud.gisapplications.dossier;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.maps.model.LatLngBounds;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.RecordValues;
import com.example.bhumicloud.gisapplications.util.RecordFilterOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RecordCursorLoader extends CursorLoader {
    private Account mAccount;
    private LatLngBounds mBounds;
    private ArrayList<String> mColumns;
    private ArrayList<String> mExcludedRecordLinkIDs;
    private RecordFilterOptions mFilter;
    private Form mForm;
    private int mLimit;
    private String mOrderBy;
    private RecordPredicate mRecordConditions;
    private String mValueQuery;

    private RecordCursorLoader(Context context, Account account, Form form, RecordFilterOptions filter, RecordPredicate recordConditions, ArrayList<String> excludedRecordLinkIDs, String valueQuery, LatLngBounds bounds, String... columns) {
        super(context);
        this.mLimit = -1;
        this.mColumns = new ArrayList ();
        this.mAccount = account;
        this.mForm = form;
        for (String column : columns) {
            this.mColumns.add("Records." + column);
        }
        this.mFilter = filter;
        this.mRecordConditions = recordConditions;
        this.mExcludedRecordLinkIDs = excludedRecordLinkIDs;
        this.mBounds = bounds;
        this.mValueQuery = valueQuery;
    }

    public RecordCursorLoader(Context context, Account account, Form form, RecordFilterOptions filter, RecordPredicate recordConditions, ArrayList<String> excludedRecordLinkIDs, String valueQuery, LatLngBounds bounds, int limit, String... columns) {
        this(context, account, form, filter, recordConditions, excludedRecordLinkIDs, valueQuery, bounds, columns);
        this.mLimit = limit;
    }

    public RecordCursorLoader(Context context, Account account, Form form, RecordFilterOptions filter, RecordPredicate recordConditions, ArrayList<String> excludedRecordLinkIDs, String valueQuery, LatLngBounds bounds, String orderBy, String... columns) {
        this(context, account, form, filter, recordConditions, excludedRecordLinkIDs, valueQuery, bounds, columns);
        this.mOrderBy = orderBy;
    }



    public RecordCursorLoader addColumns(String... columns) {
        this.mColumns.addAll( Arrays.asList(columns));
        return this;
    }

    public Cursor loadInBackground() {
        StringBuilder builder;
        int i;
        ArrayList<String> predicates = new ArrayList ();
        ArrayList<String> paramsList = new ArrayList ();
        if (this.mAccount != null) {
            predicates.add("Records.account_id = ?");
            paramsList.add( String.valueOf(this.mAccount.getRowID()));
        }
        RecordFilterOptions filter = this.mFilter;
        if (filter != null) {
            List<Long> projects = filter.getSelectedProjects();
            if (!(projects == null || projects.isEmpty())) {
                builder = new StringBuilder ();
                builder.append("Records.project_id IN (").append( TextUtils.join(",", projects)).append(")");
                predicates.add(builder.toString());
            }
            List<String> statuses = filter.getSelectedStatusOptions();
            if (!(statuses == null || statuses.isEmpty())) {
                builder = new StringBuilder ();
                builder.append("Records.status IN (");
                for (i = 0; i < statuses.size(); i++) {
                    if (i > 0) {
                        builder.append(",");
                    }
                    builder.append("?");
                }
                builder.append(")");
                predicates.add(builder.toString());
                paramsList.addAll(statuses);
            }
        }
        boolean needFormPredicate = true;
        String valuesQuery = this.mValueQuery;
        if (!TextUtils.isEmpty(valuesQuery)) {
            long[] rowIDs = SearchIndex.getInstance(GIS.getInstance()).searchRecordValues(valuesQuery);
            if (rowIDs != null) {
                builder = new StringBuilder ();
                builder.append("Records.values_index_row_id IN (");
                for (i = 0; i < rowIDs.length; i++) {
                    if (i > 0) {
                        builder.append(",");
                    }
                    builder.append(rowIDs[i]);
                }
                builder.append(")");
                predicates.add(builder.toString());
            }
        }
        String tableName = Record.TABLE_NAME;
        if (this.mRecordConditions != null) {
            ArrayList<String> recordConditionsPredicates = new ArrayList ();
            ArrayList<String> recordConditionsParams = new ArrayList ();
            this.mRecordConditions.buildParts(recordConditionsPredicates, recordConditionsParams);
            if (recordConditionsParams.size() > 0) {
                predicates.addAll(recordConditionsPredicates);
                paramsList.addAll(recordConditionsParams);
                String queryTable = RecordValues.tableNameWithForm(this.mForm, null);
                tableName = String.format("%s INNER JOIN `%s` query ON query.record_id = %s.%s", new Object[]{Record.TABLE_NAME, queryTable, Record.TABLE_NAME, "_id"});
               Log.e("Table_Name",tableName );
                needFormPredicate = false;
            }
        }
        if (this.mForm != null && needFormPredicate) {
            predicates.add("Records.form_id = ?");
            paramsList.add( String.valueOf(this.mForm.getRowID()));
        }
        if (this.mExcludedRecordLinkIDs != null) {
            ArrayList<String> placeholders = new ArrayList ();
            for (i = 0; i < this.mExcludedRecordLinkIDs.size(); i++) {
                placeholders.add("?");
            }
            ArrayList<String> arrayList = predicates;
            arrayList.add( String.format("%s.%s NOT IN ( %s )", new Object[]{Record.TABLE_NAME, "remote_id", TextUtils.join(", ", placeholders)}));
            paramsList.addAll(this.mExcludedRecordLinkIDs);
        }
        LatLngBounds bounds = this.mBounds;
        if (bounds != null) {
            double n = bounds.northeast.latitude;
            double s = bounds.southwest.latitude;
            double e = bounds.northeast.longitude;
            double w = bounds.southwest.longitude;
            predicates.add("Records.latitude <= ?");
            predicates.add("Records.latitude >= ?");
            predicates.add("Records.longitude <= ?");
            predicates.add("Records.longitude >= ?");
            paramsList.add( String.valueOf(n));
            paramsList.add( String.valueOf(s));
            paramsList.add( String.valueOf(e));
            paramsList.add( String.valueOf(w));
        }
        String predicate = TextUtils.join(" AND ", predicates);
        String[] subArgs = (String[]) paramsList.toArray(new String[paramsList.size()]);
        String limit = null;
        if (this.mLimit > 0) {
            limit = String.valueOf(this.mLimit);
        }
        return GIS.getDatabase().query(tableName, (String[]) this.mColumns.toArray(new String[this.mColumns.size()]), predicate, subArgs, null, null, this.mOrderBy, limit);
    }
}
