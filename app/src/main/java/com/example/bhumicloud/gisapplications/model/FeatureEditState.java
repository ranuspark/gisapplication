package com.example.bhumicloud.gisapplications.model;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import com.example.bhumicloud.gisapplications.util.LocationUtils;
import com.example.bhumicloud.gisapplications.util.ParcelUtils;
import java.util.Date;

public class FeatureEditState implements Parcelable {
    public static final Creator<FeatureEditState> CREATOR = new C11641();
    private final Date mCreatedAt;
    private Integer mCreatedDuration;
    private Location mCreatedLocation;
    private final boolean mDraft;
    private Integer mEditedDuration;
    private final FormValueContainer mFormValues;
    private final Location mLocation;
    private final long mProjectID;
    private final String mStatus;
    private final boolean mSynchronized;
    private final Date mUpdatedAt;
    private Integer mUpdatedDuration;
    private Location mUpdatedLocation;

    static class C11641 implements Creator<FeatureEditState> {
        C11641() {
        }

        public FeatureEditState createFromParcel(Parcel source) {
            return new FeatureEditState(source);
        }

        public FeatureEditState[] newArray(int size) {
            return new FeatureEditState[size];
        }
    }

    public FeatureEditState(Feature feature) {
        this.mFormValues = feature.getFormValues().copy();
        Location featureLocation = feature.getLocation();
        this.mLocation = featureLocation == null ? null : new Location(featureLocation);
        if (feature instanceof Record) {
            Record record = (Record) feature;
            this.mStatus = record.getRecordStatus();
            Project project = record.getProject();
            this.mProjectID = project == null ? -1 : project.getRowID();
            this.mDraft = record.isDraft();
            this.mSynchronized = record.isSynchronized();
        } else {
            this.mStatus = null;
            this.mProjectID = -1;
            this.mDraft = false;
            this.mSynchronized = false;
        }
        this.mCreatedAt = feature.getCreatedAt();
        this.mUpdatedAt = feature.getUpdatedAt();
        this.mCreatedLocation = feature.getCreatedLocation();
        this.mUpdatedLocation = feature.getUpdatedLocation();
        this.mCreatedDuration = feature.getCreatedDuration();
        this.mUpdatedDuration = feature.getUpdatedDuration();
        this.mEditedDuration = feature.getEditedDuration();
    }

    private FeatureEditState(Parcel in) {
        boolean z = true;
        this.mStatus = in.readString();
        this.mProjectID = in.readLong();
        this.mFormValues = (FormValueContainer) in.readParcelable(getClass().getClassLoader());
        this.mLocation = (Location) in.readParcelable(null);
        this.mDraft = in.readInt() != 0;
        if (in.readInt() == 0) {
            z = false;
        }
        this.mSynchronized = z;
        this.mCreatedAt = ParcelUtils.readDate(in);
        this.mUpdatedAt = ParcelUtils.readDate(in);
        this.mCreatedLocation = (Location) in.readParcelable(null);
        this.mUpdatedLocation = (Location) in.readParcelable(null);
        this.mCreatedDuration = (Integer) in.readValue(null);
        this.mUpdatedDuration = (Integer) in.readValue(null);
        this.mEditedDuration = (Integer) in.readValue(null);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        int i = 1;
        dest.writeString(this.mStatus);
        dest.writeLong(this.mProjectID);
        dest.writeParcelable(this.mFormValues, flags);
        dest.writeParcelable(this.mLocation, flags);
        dest.writeInt(this.mDraft ? 1 : 0);
        if (!this.mSynchronized) {
            i = 0;
        }
        dest.writeInt(i);
        ParcelUtils.writeDate(dest, this.mCreatedAt);
        ParcelUtils.writeDate(dest, this.mUpdatedAt);
        dest.writeParcelable(this.mCreatedLocation, flags);
        dest.writeParcelable(this.mUpdatedLocation, flags);
        dest.writeValue(this.mCreatedDuration);
        dest.writeValue(this.mUpdatedDuration);
        dest.writeValue(this.mEditedDuration);
    }

    public FormValueContainer getFormValues() {
        return this.mFormValues;
    }

    public boolean isDraft() {
        return this.mDraft;
    }

    public boolean isSynchronized() {
        return this.mSynchronized;
    }

    public Date getCreatedAt() {
        return this.mCreatedAt;
    }

    public Date getUpdatedAt() {
        return this.mUpdatedAt;
    }

    public Location getLocation() {
        return this.mLocation;
    }

    public String getStatus() {
        return this.mStatus;
    }

    public long getProjectID() {
        return this.mProjectID;
    }

    public Location getCreatedLocation() {
        return this.mCreatedLocation;
    }

    public Location getUpdatedLocation() {
        return this.mUpdatedLocation;
    }

    public Integer getCreatedDuration() {
        return this.mCreatedDuration;
    }

    public Integer getUpdatedDuration() {
        return this.mUpdatedDuration;
    }

    public Integer getEditedDuration() {
        return this.mEditedDuration;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof FeatureEditState)) {
            return false;
        }
        FeatureEditState featureEditState = (FeatureEditState) object;
        if (!featureEditState.canEqual(this)) {
            return false;
        }
        boolean statusEqual = this.mStatus == null ? featureEditState.mStatus == null : this.mStatus.equals(featureEditState.mStatus);
        boolean projectEqual = this.mProjectID == featureEditState.mProjectID;
        boolean draftEqual = this.mDraft == featureEditState.mDraft;
        boolean synchronizedEqual = this.mSynchronized == featureEditState.mSynchronized;
        boolean valuesEqual;
        if (this.mFormValues != null && featureEditState.mFormValues != null) {
            String currentJson = JSONUtils.toJSONString(this.mFormValues.toJSON());
            String savedJson = JSONUtils.toJSONString(featureEditState.mFormValues.toJSON());
            if (currentJson == null || savedJson == null) {
                valuesEqual = currentJson == null && savedJson == null;
            } else {
                valuesEqual = currentJson.equals(savedJson);
            }
        } else if (this.mFormValues == null && featureEditState.mFormValues == null) {
            valuesEqual = true;
        } else {
            valuesEqual = false;
        }
        boolean locationEqual = (this.mLocation == null || featureEditState.mLocation == null) ? this.mLocation == null && featureEditState.mLocation == null : LocationUtils.equals(this.mLocation, featureEditState.mLocation);
        if (draftEqual && synchronizedEqual && statusEqual && projectEqual && valuesEqual && locationEqual) {
            return true;
        }
        return false;
    }

    public boolean canEqual(Object other) {
        return other instanceof FeatureEditState;
    }

    public int hashCode() {
        int result;
        int hashCode;
        int i = 1;
        if (this.mStatus != null) {
            result = this.mStatus.hashCode();
        } else {
            result = 0;
        }
        int i2 = ((result * 31) + ((int) (this.mProjectID ^ (this.mProjectID >>> 32)))) * 31;
        if (this.mFormValues != null) {
            hashCode = this.mFormValues.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mLocation != null) {
            hashCode = this.mLocation.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mSynchronized) {
            hashCode = 1;
        } else {
            hashCode = 0;
        }
        hashCode = (i2 + hashCode) * 31;
        if (!this.mDraft) {
            i = 0;
        }
        return hashCode + i;
    }
}

