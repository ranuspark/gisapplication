package com.example.bhumicloud.gisapplications.util;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;

import com.squareup.picasso.Transformation;

public class RoundedTransformation implements Transformation {
    private final String mKey;
    private final int mRadius;

    public RoundedTransformation() {
        this.mRadius = -1;
        this.mKey = "rounded(circle)";
    }

    public RoundedTransformation(int radius) {
        this.mRadius = radius;
        this.mKey = "rounded(radius=" + this.mRadius + ")";
    }

    public Bitmap transform(Bitmap source) {
        Paint paint = new Paint ();
        paint.setAntiAlias(true);
        paint.setShader(new BitmapShader (source, TileMode.CLAMP, TileMode.CLAMP));
        Bitmap output = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas (output);
        if (this.mRadius < 0) {
            float r = ((float) Math.min(source.getWidth(), source.getHeight())) / 2.0f;
            canvas.drawCircle(r, r, r, paint);
        } else {
            canvas.drawRoundRect(new RectF (0.0f, 0.0f, (float) source.getWidth(), (float) source.getHeight()), (float) this.mRadius, (float) this.mRadius, paint);
        }
        source.recycle();
        return output;
    }

    public String key() {
        return this.mKey;
    }
}
