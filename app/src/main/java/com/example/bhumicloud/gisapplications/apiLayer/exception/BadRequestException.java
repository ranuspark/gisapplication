package com.example.bhumicloud.gisapplications.apiLayer.exception;

import android.content.Context;

import com.example.bhumicloud.gisapplications.R;

public class BadRequestException extends GISServiceException {
    public BadRequestException(String message) {
        super(message);
    }

    public String getUserFriendlyMessage(Context context) {
        return context.getString(R.string.bad_request_exception);
    }
}
