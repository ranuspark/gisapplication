package com.example.bhumicloud.gisapplications.Activity;

import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.location.Address;
import com.example.bhumicloud.gisapplications.location.ReverseGeocodeTask;
import com.example.bhumicloud.gisapplications.location.ReverseGeocodeTask.Callback;

public class AddressFieldDialog extends ThemedDialogFragment implements Callback {
    private Address mAddress;
    private EditText mAddressCityField;
    private EditText mAddressCountryField;
    private EditText mAddressCountyField;
    private EditText mAddressNumberField;
    private EditText mAddressPostalField;
    private EditText mAddressStateField;
    private EditText mAddressStreetField;
    private EditText mAddressSuiteField;
    private AddressFieldDialogListener mListener;
    private Location mLocation;
    private ProgressDialog mProgressDialog;

    public interface AddressFieldDialogListener {
        void onAddressChanged(Address address);
    }

    public View onCreateContentView(LayoutInflater inflater, ViewGroup root, Bundle savedState) {
        setPositiveButtonText((int) R.string.done);
        setNeutralButtonText((int) R.string.populate_address);
        setNegativeButtonText((int) R.string.clear);
        View v = inflater.inflate(R.layout.dialog_address, root, false);
        this.mAddressNumberField = (EditText) v.findViewById(R.id.address_number_field);
        this.mAddressStreetField = (EditText) v.findViewById(R.id.address_street_field);
        this.mAddressSuiteField = (EditText) v.findViewById(R.id.address_suite_field);
        this.mAddressCityField = (EditText) v.findViewById(R.id.address_city_field);
        this.mAddressCountyField = (EditText) v.findViewById(R.id.address_county_field);
        this.mAddressStateField = (EditText) v.findViewById(R.id.address_state_field);
        this.mAddressPostalField = (EditText) v.findViewById(R.id.address_postal_field);
        this.mAddressCountryField = (EditText) v.findViewById(R.id.address_country_field);
        reloadAddressFields();
        return v;
    }

    public void onDestroy() {
        this.mListener = null;
        super.onDestroy();
    }

    public void onReverseGeocodeFinished(ReverseGeocodeTask task, android.location.Address result) {
        dismissProgressDialog();
        if (result != null) {
            this.mAddress = new Address(result);
            reloadAddressFields();
            return;
        }
        displayErrorDialog();
    }

    public void setAddress(Address address) {
        this.mAddress = address;
    }

    public void setLocation(Location location) {
        this.mLocation = location;
    }

    public void setAddressFieldDialogListener(AddressFieldDialogListener listener) {
        this.mListener = listener;
    }

    protected void onPositiveButtonClicked() {
        super.onPositiveButtonClicked();
        onDoneButtonPressed();
    }

    protected void onNeutralButtonClicked() {
        super.onNeutralButtonClicked();
        onPopulateButtonPressed();
    }

    protected void onNegativeButtonClicked() {
        super.onNegativeButtonClicked();
        onClearButtonPressed();
    }

    private void displayProgressDialog() {
        dismissProgressDialog();
        this.mProgressDialog = new ProgressDialog (getActivity());
        this.mProgressDialog.setCancelable(false);
        this.mProgressDialog.setMessage(getResources().getString(R.string.determining_address));
        this.mProgressDialog.setProgressStyle(0);
        this.mProgressDialog.show();
    }

    private void dismissProgressDialog() {
        if (this.mProgressDialog != null) {
            this.mProgressDialog.dismiss();
            this.mProgressDialog = null;
        }
    }

    private void displayErrorDialog() {
        Builder b = new Builder (getActivity());
      //  b.setIcon(17301543);
        b.setMessage(R.string.error_determining_address);
        b.setTitle(R.string.unknown_address);
        b.create().show();
    }

    private void reloadAddressFields() {
        if (this.mAddress != null) {
            this.mAddressNumberField.setText(this.mAddress.getStreetNumber());
            this.mAddressStreetField.setText(this.mAddress.getStreetName());
            this.mAddressSuiteField.setText(this.mAddress.getSuite());
            this.mAddressCityField.setText(this.mAddress.getCity());
            this.mAddressCountyField.setText(this.mAddress.getCounty());
            this.mAddressStateField.setText(this.mAddress.getState());
            this.mAddressPostalField.setText(this.mAddress.getPostalCode());
            this.mAddressCountryField.setText(this.mAddress.getCountryName());
        }
    }

    private void onDoneButtonPressed() {
        this.mAddress = new Address(this.mAddress == null ? null : this.mAddress.getLocale(), this.mAddressNumberField.getText().toString(), this.mAddressStreetField.getText().toString(), this.mAddressSuiteField.getText().toString(), this.mAddressCityField.getText().toString(), this.mAddressCountyField.getText().toString(), this.mAddressStateField.getText().toString(), this.mAddressPostalField.getText().toString(), this.mAddressCountryField.getText().toString());
        if (this.mListener != null) {
            this.mListener.onAddressChanged(this.mAddress);
        }
        dismiss();
    }

    private void onClearButtonPressed() {
        this.mAddressNumberField.setText(null);
        this.mAddressStreetField.setText(null);
        this.mAddressSuiteField.setText(null);
        this.mAddressCityField.setText(null);
        this.mAddressCountyField.setText(null);
        this.mAddressStateField.setText(null);
        this.mAddressPostalField.setText(null);
        this.mAddressCountryField.setText(null);
    }

    private void onPopulateButtonPressed() {
        if (this.mLocation != null) {
            displayProgressDialog();
            new ReverseGeocodeTask(getActivity(), this.mLocation, this).execute(new Void[0]);
            return;
        }
        displayErrorDialog();
    }
}
