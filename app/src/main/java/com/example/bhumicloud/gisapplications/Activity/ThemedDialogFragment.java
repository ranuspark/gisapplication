package com.example.bhumicloud.gisapplications.Activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.util.KeyboardUtils;

public abstract class ThemedDialogFragment extends GISDialogFragment implements OnKeyListener {
    private static final String STATE_NEGATIVE_BUTTON_TEXT = "STATE_NEGATIVE_BUTTON_TEXT";
    private static final String STATE_NEUTRAL_BUTTON_TEXT = "STATE_NEUTRAL_BUTTON_TEXT";
    private static final String STATE_POSITIVE_BUTTON_TEXT = "STATE_POSITIVE_BUTTON_TEXT";
    private static final String STATE_TITLE = "STATE_TITLE";
    private ViewGroup mButtonsContainer;
    private Button mNegativeButton;
    private String mNegativeButtonText;
    private int mNegativeButtonTextId;
    private Button mNeutralButton;
    private String mNeutralButtonText;
    private int mNeutralButtonTextId;
    private Button mPositiveButton;
    private String mPositiveButtonText;
    private int mPositiveButtonTextId;
    private String mTitle;
    private int mTitleId;
    private TextView mTitleView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(1, getTheme());
        if (this.mNegativeButtonTextId != View.VISIBLE) {
            this.mNegativeButtonText = getString(this.mNegativeButtonTextId);
        }
        if (this.mNeutralButtonTextId != View.VISIBLE) {
            this.mNeutralButtonText = getString(this.mNeutralButtonTextId);
        }
        if (this.mPositiveButtonTextId != View.VISIBLE) {
            this.mPositiveButtonText = getString(this.mPositiveButtonTextId);
        }
        if (this.mTitleId != View.VISIBLE) {
            this.mTitle = getString(this.mTitleId);
        }
        if (savedInstanceState != null) {
            this.mTitle = savedInstanceState.getString(STATE_TITLE);
            this.mNegativeButtonText = savedInstanceState.getString(STATE_NEGATIVE_BUTTON_TEXT);
            this.mNeutralButtonText = savedInstanceState.getString(STATE_NEUTRAL_BUTTON_TEXT);
            this.mPositiveButtonText = savedInstanceState.getString(STATE_POSITIVE_BUTTON_TEXT);
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState) {
        View view = inflater.inflate(R.layout.dialog_themed_fragment, container, false);
        this.mTitleView = (TextView) view.findViewById(R.id.title_view);
        this.mNegativeButton = (Button) view.findViewById(R.id.negative_button);
        this.mNeutralButton = (Button) view.findViewById(R.id.neutral_button);
        this.mPositiveButton = (Button) view.findViewById(R.id.positive_button);
        this.mNegativeButton.setContentDescription("negative");
        this.mNeutralButton.setContentDescription("neutral");
        this.mPositiveButton.setContentDescription("positive");
        ViewGroup contentContainer = (ViewGroup) view.findViewById(R.id.content_container);
        this.mButtonsContainer = (ViewGroup) view.findViewById(R.id.buttons_container);
        layoutTitle(this.mTitle);
        View content = onCreateContentView(inflater, contentContainer, savedState);
        if (content != null) {
            contentContainer.addView(content);
        }
        onLayoutButtons();
        return view;
    }

    public View onCreateContentView(LayoutInflater inflater, ViewGroup root, Bundle savedState) {
        return null;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.setOnKeyListener(this);
            Window window = dialog.getWindow();
            if (window != null) {
                window.setSoftInputMode(16);
            }
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_TITLE, this.mTitle);
        outState.putString(STATE_NEGATIVE_BUTTON_TEXT, this.mNegativeButtonText);
        outState.putString(STATE_NEUTRAL_BUTTON_TEXT, this.mNeutralButtonText);
        outState.putString(STATE_POSITIVE_BUTTON_TEXT, this.mPositiveButtonText);
    }

    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        return keyCode == 4 && event.getAction() == 1 && onBackButtonClicked();
    }

    public void setTitle(String aTitle) {
        this.mTitle = aTitle;
        layoutTitle(this.mTitle);
    }

    public void setTitle(int resId) {
        this.mTitleId = resId;
        if (getActivity() != null) {
            setTitle(getString(this.mTitleId));
        }
    }

    public void setNegativeButtonText(int resId) {
        this.mNegativeButtonTextId = resId;
        if (getActivity() != null) {
            setNegativeButtonText(getString(resId));
        }
    }

    public void setNegativeButtonText(String text) {
        this.mNegativeButtonText = text;
        if (this.mNegativeButton != null) {
            this.mNegativeButton.setText(text);
            onLayoutButtons();
        }
    }

    public void setNeutralButtonText(int resId) {
        this.mNeutralButtonTextId = resId;
        if (getActivity() != null) {
            setNeutralButtonText(getString(resId));
        }
    }

    public void setNeutralButtonText(String text) {
        this.mNeutralButtonText = text;
        if (this.mNeutralButton != null) {
            this.mNeutralButton.setText(text);
            onLayoutButtons();
        }
    }

    public void setPositiveButtonText(int resId) {
        this.mPositiveButtonTextId = resId;
        if (getActivity() != null) {
            setPositiveButtonText(getString(resId));
        }
    }

    public void setPositiveButtonText(String text) {
        this.mPositiveButtonText = text;
        if (this.mPositiveButton != null) {
            this.mPositiveButton.setText(text);
            onLayoutButtons();
        }
    }

    protected void onNegativeButtonClicked() {
        KeyboardUtils.hideDialogSoftKeyboard(getActivity(), getView());
    }

    protected void onNeutralButtonClicked() {
    }

    protected void onPositiveButtonClicked() {
        KeyboardUtils.hideDialogSoftKeyboard(getActivity(), getView());
    }

    protected boolean onBackButtonClicked() {
        return false;
    }

    protected boolean hasTitle() {
        return !TextUtils.isEmpty(this.mTitle);
    }

    protected boolean hasButtons() {
        return (TextUtils.isEmpty(this.mNegativeButtonText) && TextUtils.isEmpty(this.mNeutralButtonText) && TextUtils.isEmpty(this.mPositiveButtonText)) ? false : true;
    }

    private void layoutTitle(String title) {
        if (this.mTitleView != null) {
            if (TextUtils.isEmpty(title)) {
                this.mTitleView.setVisibility(View.GONE);
                return;
            }
            this.mTitleView.setText(title);
            this.mTitleView.setVisibility(View.VISIBLE);
        }
    }

    private void onLayoutButtons() {
        if (TextUtils.isEmpty(this.mNegativeButtonText) && TextUtils.isEmpty(this.mNeutralButtonText) && TextUtils.isEmpty(this.mPositiveButtonText)) {
            this.mButtonsContainer.setVisibility(View.GONE);
            return;
        }
        this.mButtonsContainer.setVisibility(View.VISIBLE);
        if (TextUtils.isEmpty(this.mNegativeButtonText)) {
            this.mNegativeButton.setVisibility(View.GONE);
        } else {
            this.mNegativeButton.setText(this.mNegativeButtonText);
            this.mNegativeButton.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    ThemedDialogFragment.this.onNegativeButtonClicked();
                }
            });
            this.mNegativeButton.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(this.mNeutralButtonText)) {
            this.mNeutralButton.setVisibility(View.GONE);
        } else {
            this.mNeutralButton.setText(this.mNeutralButtonText);
            this.mNeutralButton.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    ThemedDialogFragment.this.onNeutralButtonClicked();
                }
            });
            this.mNeutralButton.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(this.mPositiveButtonText)) {
            this.mPositiveButton.setVisibility(View.GONE);
            return;
        }
        this.mPositiveButton.setText(this.mPositiveButtonText);
        this.mPositiveButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ThemedDialogFragment.this.onPositiveButtonClicked();
            }
        });
        this.mPositiveButton.setVisibility(View.VISIBLE);
    }
}
