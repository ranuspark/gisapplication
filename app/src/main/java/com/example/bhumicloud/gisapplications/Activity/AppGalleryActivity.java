package com.example.bhumicloud.gisapplications.Activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.Activity.AddAppDialog.AddAppDialogListener;
import com.example.bhumicloud.gisapplications.Activity.AddAppService.AddAppListener;
import com.example.bhumicloud.gisapplications.Activity.AddAppService.AppGalleryServiceBinder;
import com.example.bhumicloud.gisapplications.Activity.AppDetailFragment.AppDetailFragmentListener;
import com.example.bhumicloud.gisapplications.Activity.AppGalleryFragment.AppGalleryFragmentListener;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.appgallery.App;
import com.example.bhumicloud.gisapplications.model.appgallery.Item;
import com.example.bhumicloud.gisapplications.util.ActivityRequestCodes;
import com.example.bhumicloud.gisapplications.util.AlertUtils;
import com.example.bhumicloud.gisapplications.util.KeyboardUtils;

import java.util.ArrayList;

public class AppGalleryActivity extends GISActivity implements ServiceConnection, AddAppDialogListener, AddAppListener, AppDetailFragmentListener, AppGalleryFragmentListener {

    private static final String STATE_FULL_GALLERY = "STATE_FULL_GALLERY";
    private static final String TAG_ADD_APP_DIALOG = "TAG_ADD_APP_DIALOG";
    private AddAppService mAppGalleryService;
    private ArrayList<Item> mFullGallery;

    public static void startForResult(Activity activity) {
        activity.startActivityForResult(new Intent(activity, AppGalleryActivity.class), ActivityRequestCodes.REQUEST_APP_GALLERY);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_gallery);
        AddAppService.bind(this, this);
        ((ImageButton) findViewById(R.id.close_btn)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                KeyboardUtils.hideSoftKeyboard(AppGalleryActivity.this);
                AppGalleryActivity.this.finish();
            }
        });
        if (savedInstanceState != null) {
            this.mFullGallery = savedInstanceState.getParcelableArrayList(STATE_FULL_GALLERY);
            return;
        }
        FragmentManager fm = getSupportFragmentManager();
        AppGalleryFragment categoryFragment = new AppGalleryFragment();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, categoryFragment);
        ft.commit();
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(STATE_FULL_GALLERY, this.mFullGallery);
    }

    protected void onStart() {
        super.onStart();
        AddAppService.bind(this, this);
    }

    public void onBackPressed() {
        Fragment fragment = getCurrentFragment();
        if (fragment == null || !(fragment instanceof OnBackPressedListener)) {
            super.onBackPressed();
        } else if (((OnBackPressedListener) fragment).onBackPressed()) {
            super.onBackPressed();
        }
    }

    protected void onStop() {
        super.onStop();
        unbindService(this);
    }

    public void onAppSelected(App app) {
        pushFragment(AppDetailFragment.getInstance(app));
    }

    public ArrayList<Item> getAppGallery() {
        return this.mFullGallery;
    }

    public void setAppGallery(ArrayList<Item> gallery) {
        this.mFullGallery = gallery;
    }

    public void onAddAppClick(App app) {
        AddAppDialog.show(getSupportFragmentManager(), TAG_ADD_APP_DIALOG, app);
    }

    public void onAddAppConfirmed(App app) {
        AddAppService.addAppToAccount(this, app, Account.getActiveAccount());
    }

    public void onAddAppSuccess(App app) {
        setResult(-1);
        AddAppDialog dialog = (AddAppDialog) getSupportFragmentManager().findFragmentByTag(TAG_ADD_APP_DIALOG);
        if (dialog != null && dialog.isAdded()) {
            dialog.onAddAppSuccess();
        }
    }

    public void onAddAppFailure(Exception e) {
        AlertUtils.showExceptionAlert(this, e);
        AddAppDialog dialog = (AddAppDialog) getSupportFragmentManager().findFragmentByTag(TAG_ADD_APP_DIALOG);
        if (dialog != null && dialog.isAdded()) {
            dialog.dismissAllowingStateLoss();
        }
    }

    public void onServiceConnected(ComponentName name, IBinder binder) {
        this.mAppGalleryService = ((AppGalleryServiceBinder) binder).getService();
        if (this.mAppGalleryService != null) {
            this.mAppGalleryService.addListener(this);
            AddAppDialog dialog = (AddAppDialog) getSupportFragmentManager().findFragmentByTag(TAG_ADD_APP_DIALOG);
            if (dialog != null && dialog.isAdded() && !this.mAppGalleryService.isAdding()) {
                dialog.dismissAllowingStateLoss();
            }
        }
    }

    public void onServiceDisconnected(ComponentName name) {
        if (this.mAppGalleryService != null) {
            this.mAppGalleryService.removeListener(this);
        }
        this.mAppGalleryService = null;
    }

    private void pushFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(4097);
        ft.replace(R.id.container, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }
}
