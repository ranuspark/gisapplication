package com.example.bhumicloud.gisapplications.model.javascript.expression;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Pair;

import com.eclipsesource.v8.V8Object;
import com.example.bhumicloud.gisapplications.model.javascript.JavaScriptResource;
import com.example.bhumicloud.gisapplications.util.DeviceInfo;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import io.fabric.sdk.android.services.network.HttpRequest;

public class ExpressionEngineFunctions {
    private static String key2;

    public static String formatNumber(Number number, String localeName, V8Object options) {
        NumberFormat formatter;
        Locale locale = getLocaleForLanguageTag(localeName);
        String style = getNullableStringProperty(options, "style");
        if (!TextUtils.isEmpty(style)) {
            Object obj = -1;
            int Value = 0;
            switch (style.hashCode()) {
                case -678927291:
                    if (style.equals("percent")) {
                        Value = 1;
                        break;
                    }
                    break;
                case 575402001:
                    if (style.equals("currency")) {
                        obj = 2;
                        break;
                    }
                    break;
            }
            switch (Value) {
                case 2:
                    formatter = NumberFormat.getCurrencyInstance(locale);
                    break;
                case 1:
                    formatter = NumberFormat.getPercentInstance(locale);
                    break;
                default:
                    formatter = NumberFormat.getNumberInstance(locale);
                    break;
            }
        }
        formatter = NumberFormat.getNumberInstance(locale);
        Number minimumIntegerDigits = getNullableNumberProperty(options, "minimumIntegerDigits");
        Number minimumFractionDigits = getNullableNumberProperty(options, "minimumFractionDigits");
        Number maximumFractionDigits = getNullableNumberProperty(options, "maximumFractionDigits");
        if (minimumIntegerDigits != null) {
            formatter.setMinimumIntegerDigits(minimumIntegerDigits.intValue());
        }
        if (minimumFractionDigits != null) {
            formatter.setMinimumFractionDigits(minimumFractionDigits.intValue());
        }
        if (maximumFractionDigits != null) {
            formatter.setMaximumFractionDigits(maximumFractionDigits.intValue());
        }
        Boolean usesGrouping = getNullableBooleanProperty(options, "useGrouping");
        if (usesGrouping != null) {
            formatter.setGroupingUsed(usesGrouping.booleanValue());
        }
        String currencyCode = getNullableStringProperty(options, "currency");
        if (currencyCode != null) {
            formatter.setCurrency( Currency.getInstance(currencyCode));
        }
        return formatter.format(number);
    }

    public static void httpRequest(final ExpressionEngine engine, final Map<String, Object> options, final int callbackID) {
        new AsyncTask<Void, Void, Void> () {
            protected Void doInBackground(Void... params) {
                ArrayList<Object> arguments;
                try {
                    Pair<HashMap<String, Object>, String> result = ExpressionEngineFunctions.httpRequest(options);
                    arguments = new ArrayList ();
                    arguments.add(null);
                    arguments.add(result.first);
                    arguments.add(result.second);
                    engine.finishAsyncCallback(callbackID, arguments);
                } catch (IOException ex) {
                    HashMap<String, Object> error = new HashMap ();
                    error.put("message", ex.getLocalizedMessage());
                    arguments = new ArrayList ();
                    arguments.add(error);
                    engine.finishAsyncCallback(callbackID, arguments);
                }
                return null;
            }
        }.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    public static void setTimeout(final ExpressionEngine engine, long timeout, final int callbackID) {
        ExpressionEngine.getSharedHandler().postDelayed(new Runnable () {
            public void run() {
                engine.finishAsyncCallback(callbackID, null);
            }
        }, timeout);
    }

    private static Number getNullableNumberProperty(V8Object object, String key) {
        Object value = object.get(key);
        if (value instanceof Number) {
            return (Number) value;
        }
        JavaScriptResource.release(value);
        return null;
    }

    private static Boolean getNullableBooleanProperty(V8Object object, String key) {
        Object value = object.get(key);
        if (value instanceof Boolean) {
            return (Boolean) value;
        }
        JavaScriptResource.release(value);
        return null;
    }

    private static String getNullableStringProperty(V8Object object, String key) {
        Object value = object.get(key);
        if (value instanceof String) {
            return (String) value;
        }
        JavaScriptResource.release(value);
        return null;
    }

    @TargetApi(21)
    private static Locale getLocaleForLanguageTag(String languageTag) {
        if (TextUtils.isEmpty(languageTag)) {
            return null;
        }
        Locale locale = null;
        if (DeviceInfo.isLollipop()) {
            locale = Locale.forLanguageTag(languageTag);
        }
        if (locale != null) {
            return locale;
        }
        String[] parts = languageTag.split("-");
        if (parts.length == 1) {
            return new Locale (parts[0]);
        }
        if (parts.length == 2) {
            return new Locale (parts[0], parts[1]);
        }
        if (parts.length == 3) {
            return new Locale (parts[0], parts[1], parts[2]);
        }
        return new Locale (languageTag);
    }

    public static Pair<HashMap<String, Object>, String> httpRequest(Map<String, Object> options) throws IOException {
        HashMap<String, Object> responseObject = new HashMap ();
        String url = (String) options.get("url");
        String method = (String) options.get("method");
        Map<String, String> requestHeaders = null;
        Boolean followRedirect = Boolean.valueOf(((Boolean) options.get("followRedirect")).booleanValue());
        String requestBody = (String) options.get("body");
        if (options.get("headers") instanceof Map) {
            requestHeaders = (Map) options.get("headers");
        }
        if (TextUtils.isEmpty(method)) {
            method = HttpRequest.METHOD_GET;
        }
        HttpURLConnection connection = null;
        String responseBody = null;
        try {
            String key;
            connection = (HttpURLConnection) new URL (url).openConnection();
            connection.setRequestMethod(method.toUpperCase());
            connection.setInstanceFollowRedirects(followRedirect.booleanValue());
            connection.setDoInput(true);
            if (requestHeaders != null) {
                for (String key2 : requestHeaders.keySet()) {
                    connection.setRequestProperty(key2, (String) requestHeaders.get(key2));
                }
            }
            if (requestBody != null) {
                connection.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream (connection.getOutputStream());
                wr.writeBytes(requestBody);
                wr.flush();
                wr.close();
            }
            byte[] buffer = new byte[4096];
            InputStream in = connection.getInputStream();
            ByteArrayOutputStream out = new ByteArrayOutputStream ();
            while (true) {
                int length = in.read(buffer);
                if (length < 0) {
                    break;
                }
                out.write(buffer, 0, length);
            }
            out.flush();
            out.close();
            responseBody = out.toString( HttpRequest.CHARSET_UTF8);
            HashMap<String, String> headers = new HashMap ();
            ArrayList<String> rawHeaders = new ArrayList ();
            for (Entry<String, List<String>> entry : connection.getHeaderFields().entrySet()) {
                key2 = (String) entry.getKey();
                if (key2 != null) {
                    List<String> valueList = (List) entry.getValue();
                    String value = null;
                    if (valueList.size() > 0) {
                        value = (String) valueList.get(valueList.size() - 1);
                        for (String rawValue : valueList) {
                            rawHeaders.add(key2);
                            rawHeaders.add(rawValue);
                        }
                    }
                    headers.put(key2.toLowerCase(), value);
                }
            }
            responseObject.put("statusCode", Integer.valueOf(connection.getResponseCode()));
            responseObject.put("headers", headers);
            responseObject.put("rawHeaders", rawHeaders);
            return new Pair (responseObject, responseBody);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
