package com.example.bhumicloud.gisapplications.View.media.signature;

import android.app.AlertDialog.Builder;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import  com.example.bhumicloud.gisapplications.R;
import  com.example.bhumicloud.gisapplications.apiLayer.exception.ResourceNotFoundException;
import  com.example.bhumicloud.gisapplications.Activity.AttachmentDownloadService;
import  com.example.bhumicloud.gisapplications.Activity.AttachmentDownloadService.AttachmentDownloadServiceBinder;
import  com.example.bhumicloud.gisapplications.Activity.AttachmentDownloadService.Observer;
import  com.example.bhumicloud.gisapplications.model.Account;
import  com.example.bhumicloud.gisapplications.model.FormValue;
import  com.example.bhumicloud.gisapplications.model.field.attachment.Attachment.Type;
import  com.example.bhumicloud.gisapplications.model.field.attachment.signature.Signature;
import  com.example.bhumicloud.gisapplications.model.field.attachment.signature.SignatureElement;
import  com.example.bhumicloud.gisapplications.model.field.attachment.signature.SignatureValue;
import  com.example.bhumicloud.gisapplications.util.AlertUtils;
import  com.example.bhumicloud.gisapplications.util.NetworkUtils;
import  com.example.bhumicloud.gisapplications.View.ElementView;

public class SignatureElementView extends ElementView<SignatureElement> implements OnClickListener, ServiceConnection, Observer {
    private AttachmentDownloadService mDownloadService;
    private SignatureElementViewListener mListener;
    private final ProgressBar mProgressBar = ((ProgressBar) findViewById(R.id.download_progress));
    private Button mSignatureButton = ((Button) findViewById(R.id.signature_button));
    private ImageView mSignatureImage = ((ImageView) findViewById(R.id.signature_image));
    private SignatureValue mSignatureValue;

    public interface SignatureElementViewListener {
        void onPresentSignatureEditor(SignatureElement signatureElement);
    }

    public SignatureElementView(Context context, SignatureElement element, boolean viewOnly) {
        super(context, element, viewOnly);
        this.mSignatureButton.setOnClickListener(this);
        this.mSignatureImage.setOnClickListener(this);
        AttachmentDownloadService.bind(context, this);
    }

    public void onClick(View v) {
        onFieldViewClicked();
        if (this.mSignatureValue != null) {
            Signature signature = this.mSignatureValue.getSignature();
            if (signature == null || !signature.isFileOneAvailable()) {
                onDownloadSignature();
            } else if (!isReadOnly()) {
                notifyOnPresentSignatureEditor((SignatureElement) getElement());
            }
        } else if (!isReadOnly()) {
            notifyOnPresentSignatureEditor((SignatureElement) getElement());
        }
    }

    public void onServiceConnected(ComponentName name, IBinder service) {
        if (service instanceof AttachmentDownloadServiceBinder) {
            this.mDownloadService = ((AttachmentDownloadServiceBinder) service).getService();
            this.mDownloadService.registerObserver(this);
        }
    }

    public void onServiceDisconnected(ComponentName name) {
        this.mDownloadService = null;
    }

    public void onAttachmentDownloadStarted(String attachment) {
        if (this.mSignatureValue != null && attachment != null && TextUtils.equals(this.mSignatureValue.getSignatureID(), attachment)) {
            showDownloadProgress();
            setDownloadProgress(0.0f);
            this.mSignatureButton.setClickable(false);
            this.mSignatureImage.setClickable(false);
        }
    }

    public void onAttachmentDownloadProgress(String attachment, int progress) {
        if (this.mSignatureValue != null && attachment != null && TextUtils.equals(this.mSignatureValue.getSignatureID(), attachment)) {
            showDownloadProgress();
            setDownloadProgress((float) progress);
            this.mSignatureButton.setClickable(false);
            this.mSignatureImage.setClickable(false);
        }
    }

    public void onAttachmentDownloadFinished(String attachment) {
        boolean z = true;
        if (this.mSignatureValue != null && attachment != null && TextUtils.equals(this.mSignatureValue.getSignatureID(), attachment)) {
            boolean z2;
            hideDownloadProgress();
            setDownloadProgress(0.0f);
            reloadSignaturePreview();
            Button button = this.mSignatureButton;
            if (this.mSignatureButton.getVisibility() == View.VISIBLE) {
                z2 = true;
            } else {
                z2 = false;
            }
            button.setClickable(z2);
            ImageView imageView = this.mSignatureImage;
            if (this.mSignatureImage.getVisibility() != View.VISIBLE) {
                z = false;
            }
            imageView.setClickable(z);
        }
    }

    public void onAttachmentDownloadFailed(String attachment, Exception e) {
        boolean z = true;
        if (this.mSignatureValue != null && attachment != null && TextUtils.equals(this.mSignatureValue.getSignatureID(), attachment)) {
            boolean z2;
            hideDownloadProgress();
            setDownloadProgress(0.0f);
            Button button = this.mSignatureButton;
            if (this.mSignatureButton.getVisibility() == View.VISIBLE) {
                z2 = true;
            } else {
                z2 = false;
            }
            button.setClickable(z2);
            ImageView imageView = this.mSignatureImage;
            if (this.mSignatureImage.getVisibility() != View.VISIBLE) {
                z = false;
            }
            imageView.setClickable(z);
            if (e instanceof ResourceNotFoundException) {
                Builder b = new Builder (getContext());
                b.setTitle(R.string.signature_unavailable_title);
                b.setMessage(R.string.signature_unavailable_message);
                b.show();
                return;
            }
            AlertUtils.showExceptionAlert(getContext(), e);
        }
    }

    public void setListener(SignatureElementViewListener listener) {
        this.mListener = listener;
    }

    public void setValue(FormValue value) {
        this.mSignatureValue = (SignatureValue) value;
        reloadSignaturePreview();
    }

    public void onDestroy(Context context) {
        super.onDestroy(context);
        this.mListener = null;
        if (this.mDownloadService != null) {
            this.mDownloadService.unregisterObserver(this);
        }
        AttachmentDownloadService.unbind(getContext(), this);
    }

    protected int getElementViewLayout() {
        return R.layout.view_signature_element;
    }

    private void showDownloadProgress() {
        if (this.mProgressBar.getVisibility() != View.VISIBLE) {
            this.mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    private void setDownloadProgress(float progress) {
        this.mProgressBar.setProgress((int) progress);
    }

    private void hideDownloadProgress() {
        if (this.mProgressBar.getVisibility() != View.GONE) {
            this.mProgressBar.setVisibility(View.GONE);
        }
    }

    private void reloadSignaturePreview() {
        if (this.mSignatureValue != null) {
            Signature signature = this.mSignatureValue.getSignature();
            if (signature == null || !signature.isFileOneAvailable()) {
                this.mSignatureButton.setVisibility(View.VISIBLE);
                this.mSignatureImage.setVisibility(View.GONE);
                return;
            }
            this.mSignatureImage.setImageURI(signature.getUriOne());
            this.mSignatureImage.setVisibility(View.VISIBLE);
            this.mSignatureButton.setVisibility(View.GONE);
            return;
        }
        this.mSignatureButton.setVisibility(View.VISIBLE);
        this.mSignatureImage.setVisibility(View.GONE);
    }

    private void onDownloadSignature() {
        Context context = getContext();
        if (NetworkUtils.isConnected(context)) {
            AttachmentDownloadService.startDownload(context, Account.getActiveAccount(), getRecord(), this.mSignatureValue.getSignatureID(), Type.SIGNATURE);
            return;
        }
        AlertUtils.showNetworkUnavailableAlert(context);
    }

    private void notifyOnPresentSignatureEditor(SignatureElement element) {
        if (this.mListener != null) {
            this.mListener.onPresentSignatureEditor(element);
        }
    }
}
