package com.example.bhumicloud.gisapplications.model.javascript;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.eclipsesource.v8.V8Array;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import io.fabric.sdk.android.services.events.EventsFilesManager;

import java.io.File;
import java.util.Map;


public class FormSchema extends JavaScriptResource {
    private static final String FUNCTION_COMPARE_FORMS = "compareForms";
    private static final String TAG = FormSchema.class.getCanonicalName();
    private static final String VARIABLE_OLD_FORM = "oldForm";
    private static final String VARIABLE_PREFIX = "prefix";
    private static final String VARIALBE_NEW_FORM = "newForm";
    private SQLiteDatabase mDB;

    public FormSchema(Context context, SQLiteDatabase db) {
        super(context);
        this.mDB = db;
    }

    public void updateSchemaWithOldForm(Map<String, Object> oldForm, Map<String, Object> newForm, long formRowID, Account account) {
        run(oldForm, newForm, formRowID, account);
    }

    public void deleteTablesAssociatedWithFormAndAccount(Map<String, Object> formJSON, long formRowID, Account account) {
        run(formJSON, null, formRowID, account);
    }

    protected File getAutoUpdatingScriptPath(Context context) {
        return new File(new File(context.getFilesDir(), "resources"), "sqlite.js");
    }

    protected int getJavaScriptRawResourceID() {
        return R.raw.sqlite;
    }

    protected String getEnvironmentName() {
        return FormSchema.class.getCanonicalName();
    }

    private void run(Map<String, Object> oldForm, Map<String, Object> newForm, long formRowID, Account account) {
        try {
            String[] statements = compare(oldForm, newForm, formRowID, account);
            int index = 1;
            int statementCount = statements.length;
            for (String sql : statements) {
                GISLogger.log(TAG, String.format("Running #%d of %d: %s", new Object[]{Integer.valueOf(index), Integer.valueOf(statementCount), sql}));
                this.mDB.execSQL(sql);
                GISLogger.log(TAG, "Completed #" + index);
                index++;
            }
        } catch (Throwable e) {
            GISLogger.log(e);
        }
    }

    private String[] compare(Map<String, Object> oldForm, Map<String, Object> newForm, long formRowID, Account account) {
        String oldFormJSON = null;
        String newFormJSON = null;
        if (oldForm != null) {
            oldForm.put("id", Long.valueOf(formRowID));
            oldFormJSON = JSONUtils.toJSONString(oldForm);
        }
        else
        {
            oldForm = null;
        }
        if (newForm != null) {
            newForm.put("id", Long.valueOf(formRowID));
            newFormJSON = JSONUtils.toJSONString(newForm);
        }
        evaluateGlobalVariableAssignment(VARIABLE_OLD_FORM, oldFormJSON);
        evaluateGlobalVariableAssignment(VARIALBE_NEW_FORM, newFormJSON);
        assignGlobalVariable(VARIABLE_PREFIX, "account_" + account.getRowID() + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        V8Array array = (V8Array) evaluateFunctionCall(FUNCTION_COMPARE_FORMS);
        String[] sqls = new String[array.length()];
        for (int i = 0; i < sqls.length; i++) {
            sqls[i] = array.getString(i);
        }
        array.release();
        return sqls;
    }
}
