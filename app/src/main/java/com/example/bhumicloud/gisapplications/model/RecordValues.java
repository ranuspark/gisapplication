package com.example.bhumicloud.gisapplications.model;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import com.google.android.gms.maps.model.LatLng;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableElement;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableItemValue;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableValue;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusValue;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class RecordValues {
    public static void updateForRecord(SQLiteDatabase db, Record record, boolean delete) {
        if (delete) {
            deleteForRecord(db, record);
        }
        insertForRecord(db, record);
    }

    public static void deleteForAccount(SQLiteDatabase db, long accountRowId) {
        try {
            db.beginTransactionNonExclusive();
            Iterator it = Form.getForms(accountRowId, db, false).iterator();
            while (it.hasNext()) {
                deleteRowsForForm(db, (Form) it.next());
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public static void deleteForRecord(SQLiteDatabase db, Record record) {
        ArrayList<RepeatableElement> repeatables = new ArrayList();
        String[] whereArgs = new String[]{String.valueOf(record.getRowID())};
        Form form = record.getForm();
        Iterator it = record.getForm().getElements().iterator();
        while (it.hasNext()) {
            Element element = (Element) it.next();
            if (element instanceof RepeatableElement) {
                repeatables.add((RepeatableElement) element);
            }
        }
        deleteRowsForRecordFromTable(db, whereArgs, tableNameWithForm(form, null));
        it = repeatables.iterator();
        while (it.hasNext()) {
            deleteRowsForRecordFromTable(db, whereArgs, tableNameWithForm(form, (RepeatableElement) it.next()));
        }
        deleteRowsForRecordFromTable(db, whereArgs, multipleValueTableNameWithForm(form));
    }

    private static void insertForRecord(SQLiteDatabase db, Record record) {
        insertRowForFeature(db, record, null, record);
        insertChildFeaturesForFeature(db, record, record);
        insertMultipleValuesForFeature(db, record, record);
        insertChildMultipleValuesForFeature(db, record, record);
    }

    private static void insertRowForFeature(SQLiteDatabase db, Feature feature, Feature parentFeature, Record record) {
        String tableName;
        ContentValues values = new ContentValues();
        columnValuesForFeature(values, feature);
        systemColumnValuesForFeature(values, feature, parentFeature, record);
        if (feature instanceof RepeatableItemValue) {
            tableName = tableNameWithForm(record.getForm(), ((RepeatableItemValue) feature).getElement());
        } else {
            tableName = tableNameWithForm(record.getForm(), null);
        }
        try {
            db.insertOrThrow(tableName, null, values);
        } catch (Throwable e) {
            GISLogger.log(e);
        }
    }

    private static void insertChildFeaturesForFeature(SQLiteDatabase db, Feature feature, Record record) {
        for (FormValue formValue : feature.getFormValues().getAllValues()) {
            if (formValue.getElement() instanceof RepeatableElement) {
                for (RepeatableItemValue repeatableItem : ((RepeatableValue) formValue).getItems()) {
                    insertRowForFeature(db, repeatableItem, feature, record);
                    insertChildFeaturesForFeature(db, repeatableItem, record);
                }
            }
        }
    }

    private static void columnValuesForFeature(ContentValues values, Feature feature) {
        for (FormValue formValue : feature.getFormValues ().getAllValues ()) {
            if (!formValue.isEmpty ()) {
                Object o = formValue.getColumnValue ();
                String key = "`f" + formValue.getElement ().getKey () + "`";
                if (o instanceof Double) {
                    values.put ( key, (Double) o );
                } else if (o instanceof Float) {
                    values.put ( key, (Float) o );
                } else if (o instanceof Integer) {
                    values.put ( key, (Integer) o );
                } else if (o instanceof Long) {
                    values.put ( key, (Long) o );
                } else if (o instanceof String) {
                    values.put ( key, (String) o );
                } /*else if (o instanceof HashMap) {
                 for   (Object oa :values.keySet ())
                {
                    values.put("`f" + oa.getKey().toString() + "`", entry.getValue().toString());
                }

                    for (HashMap<String,Object> entry :((HashMap) o).entrySet ()) {
                        values.put("`f" + entry.getKey().toString() + "`", entry.getValue().toString());
                    }

            }*/

            }
        }
    }
    private static void insertMultipleValuesForFeature(SQLiteDatabase db, Feature feature, Record record) {
        ContentValues values = new ContentValues();
        ArrayList<MultipleValueItem> items = multipleValuesForFeature(feature);
        String tableName = multipleValueTableNameWithForm(record.getForm());
        values.put(Attachment.COLUMN_RECORD_ID, Long.valueOf(record.getRowID()));
        if (feature instanceof RepeatableItemValue) {
            values.put("parent_resource_id", ((RepeatableItemValue) feature).getIdentifier());
        }
        Iterator it = items.iterator();
        while (it.hasNext()) {
            MultipleValueItem item = (MultipleValueItem) it.next();
            try {
                values.put(Storage.COLUMN_KEY, item.getKey());
                values.put("text_value", item.getTextValue());
                db.insertOrThrow(tableName, null, values);
            } catch (Throwable e) {
                GISLogger.log(e);
            }
        }
    }

    private static void insertChildMultipleValuesForFeature(SQLiteDatabase db, Feature feature, Record record) {
        for (FormValue formValue : feature.getFormValues().getAllValues()) {
            if (formValue.getElement() instanceof RepeatableElement) {
                for (RepeatableItemValue repeatableItem : ((RepeatableValue) formValue).getItems()) {
                    insertMultipleValuesForFeature(db, repeatableItem, record);
                    insertChildMultipleValuesForFeature(db, repeatableItem, record);
                }
            }
        }
    }

    private static ArrayList<MultipleValueItem> multipleValuesForFeature(Feature feature) {
        ArrayList<MultipleValueItem> values = new ArrayList();
        for (FormValue formValue : feature.getFormValues().getAllValues()) {
            if (!formValue.isEmpty()) {
                ArrayList<MultipleValueItem> items = formValue.getMultipleValues();
                if (items != null) {
                    values.addAll(items);
                }
            }
        }
        return values;
    }

    private static void systemColumnValuesForFeature(ContentValues values, Feature feature, Feature parentFeature, Record record) {
        values.put(Attachment.COLUMN_RECORD_ID, Long.valueOf(record.getRowID()));
        values.put("record_resource_id", record.getRemoteID());
        LatLng latLng;
        if (feature instanceof Record) {
            Project project = record.getProject();
            if (project != null) {
                values.put(Record.COLUMN_PROJECT_ID, Long.valueOf(project.getRowID()));
            }
            StatusValue statusValue = record.getStatusValue();
            if (statusValue != null) {
                values.put("status", statusValue.getDisplayValue());
            }
            if (record.hasLocation()) {
                latLng = record.getLatLng();
                values.put(Record.COLUMN_LATITUDE, Double.valueOf(latLng.latitude));
                values.put(Record.COLUMN_LONGITUDE, Double.valueOf(latLng.longitude));
            }
        } else if (feature instanceof RepeatableItemValue) {
            RepeatableItemValue item = (RepeatableItemValue) feature;
            values.put("resource_id", item.getIdentifier());
            if (parentFeature instanceof RepeatableItemValue) {
                values.put("parent_resource_id", ((RepeatableItemValue) parentFeature).getIdentifier());
            }
            if (item.hasLocation()) {
                latLng = item.getLatLng();
                values.put(Record.COLUMN_LATITUDE, Double.valueOf(latLng.latitude));
                values.put(Record.COLUMN_LONGITUDE, Double.valueOf(latLng.longitude));
            }
        }
        Date createdAt = feature.getCreatedAt();
        if (createdAt == null) {
            createdAt = new Date();
        }
        values.put(Record.COLUMN_CREATED_AT, Double.valueOf(((double) createdAt.getTime()) / 1000.0d));
        Date updatedAt = feature.getUpdatedAt();
        if (updatedAt == null) {
            updatedAt = createdAt;
        }
        values.put(Record.COLUMN_UPDATED_AT, Double.valueOf(((double) updatedAt.getTime()) / 1000.0d));
    }

    private static void deleteRowsForForm(SQLiteDatabase db, Form form) {
        ArrayList<String> tableNames = new ArrayList();
        tableNames.add(tableNameWithForm(form, null));
        Iterator it = form.getElements().iterator();
        while (it.hasNext()) {
            Element element = (Element) it.next();
            if (element instanceof RepeatableElement) {
                tableNames.add(tableNameWithForm(form, (RepeatableElement) element));
            }
        }
        tableNames.add(multipleValueTableNameWithForm(form));
        it = tableNames.iterator();
        while (it.hasNext()) {
            db.delete((String) it.next(), "record_id IN (SELECT _id FROM Records WHERE account_id = ? AND form_id = ? AND is_synchronized = 1 AND unique_id NOT IN (SELECT record_id FROM attachments WHERE record_id IS NOT NULL AND uploaded = 0))", new String[]{String.valueOf(form.getAccountID()), String.valueOf(form.getRowID())});
        }
    }

    private static void deleteRowsForRecordFromTable(SQLiteDatabase db, String[] whereArgs, String tableName) {
        db.delete(tableName, "record_id = ?", whereArgs);
    }

    private static String multipleValueTableNameWithForm(Form form) {
        return "account_" + form.getAccountID() + "_form_" + form.getRowID() + "_values";
    }

    public static String tableNameWithForm(Form form, RepeatableElement repeatable) {
        if (repeatable == null) {
            return "account_" + form.getAccountID() + "_form_" + form.getRowID();
        }
        return "account_" + form.getAccountID() + "_form_" + form.getRowID() + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + repeatable.getKey();
    }
}

