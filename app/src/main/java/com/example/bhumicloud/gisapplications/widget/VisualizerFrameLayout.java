package com.example.bhumicloud.gisapplications.widget;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.github.lassana.recorder.AudioRecorder;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import android.media.MediaRecorder.*;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

public class VisualizerFrameLayout extends FrameLayout {
    private static final int MAX_AMP_VALUE = 30500;
    private AudioRecorder mAudioRecorder;
    private Handler mHandler = new Handler ();
    private int mMaxHeight;
    private int mMostRecent;
    private int mPrevious;
    private AtomicBoolean mRunning = new AtomicBoolean (false);
    private Timer mTimer;
    private int mUpdateIntervalInMilliseconds;

    class C12601 extends TimerTask {

        class C12591 implements Runnable {
            C12591() {
            }

            public void run() {
                try {
                    VisualizerFrameLayout.this.mMostRecent = VisualizerFrameLayout.this.mAudioRecorder.hashCode ();
                    if (VisualizerFrameLayout.this.mMostRecent != VisualizerFrameLayout.this.mPrevious) {
                        VisualizerFrameLayout.this.mPrevious = VisualizerFrameLayout.this.mMostRecent;
                        VisualizerFrameLayout.this.getLayoutParams().height = (VisualizerFrameLayout.this.mMostRecent * VisualizerFrameLayout.this.mMaxHeight) / VisualizerFrameLayout.MAX_AMP_VALUE;
                    }
                } catch (RuntimeException e) {
                    GISLogger.log(e);
                }
            }
        }

        C12601() {
        }

        public void run() {
            VisualizerFrameLayout.this.mHandler.post(new C12591());
        }
    }

    class C12622 extends TimerTask {

        class C12611 implements Runnable {
            C12611() {
            }

            public void run() {
                LayoutParams p = (LayoutParams) VisualizerFrameLayout.this.getLayoutParams();
                p.height = 0;
                VisualizerFrameLayout.this.setLayoutParams(p);
            }
        }

        C12622() {
        }

        public void run() {
            VisualizerFrameLayout.this.mHandler.post(new C12611());
        }
    }

    public VisualizerFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void initialize(AudioRecorder audioRecorder, int maxHeight, int updateIntervalInMilliseconds) {
        this.mAudioRecorder = audioRecorder;
        this.mMaxHeight = maxHeight;
        this.mUpdateIntervalInMilliseconds = updateIntervalInMilliseconds;
    }

    public void start() {
        if (this.mRunning.compareAndSet(false, true)) {
            this.mTimer = new Timer ();
            try {
                this.mTimer.scheduleAtFixedRate(new C12601(), (long) this.mUpdateIntervalInMilliseconds, (long) this.mUpdateIntervalInMilliseconds);
            } catch (IllegalArgumentException e) {
                GISLogger.log(e);
            }
        }
    }

    public void stop() {
        if (this.mRunning.compareAndSet(true, false)) {
            this.mTimer.cancel();
        }
        new Timer ().schedule(new C12622(), (long) (this.mUpdateIntervalInMilliseconds * 2));
    }
}
