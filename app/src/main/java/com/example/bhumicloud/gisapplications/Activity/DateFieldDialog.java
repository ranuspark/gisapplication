package com.example.bhumicloud.gisapplications.Activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.field.temporal.date.DateValue;
import com.example.bhumicloud.gisapplications.util.GISLogger;

import java.util.Calendar;
import java.util.Date;

public class DateFieldDialog extends ThemedDialogFragment {
    private static final long JANUARY_01_1600 = -11676074400000L;
    private static final String STATE_CALENDAR_TIME = "STATE_CALENDAR_TIME";
    private final Calendar mCurrentDateTime = Calendar.getInstance();
    private DatePicker mDatePicker;
    private DateFieldDialogListener mListener;

    public interface DateFieldDialogListener {
        void onDateValueChanged(Date date);
    }

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        setNegativeButtonText((int) R.string.clear);
        setNeutralButtonText((int) R.string.today);
        setPositiveButtonText((int) R.string.done);
        if (savedState != null) {
            this.mCurrentDateTime.setTimeInMillis(savedState.getLong(STATE_CALENDAR_TIME));
        }
    }

    public View onCreateContentView(LayoutInflater inflater, ViewGroup root, Bundle savedState) {
        View view = inflater.inflate(R.layout.dialog_date_field, root, false);
        this.mDatePicker = (DatePicker) view.findViewById(R.id.date_picker);
        this.mDatePicker.setTag("date_picker");
        configureDatePicker(this.mCurrentDateTime);
        return view;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(STATE_CALENDAR_TIME, this.mCurrentDateTime.getTimeInMillis());
    }

    public void onDestroy() {
        this.mListener = null;
        super.onDestroy();
    }

    public void setDateValue(DateValue value) {
        Date date = null;
        if (value != null) {
            try {
                date = value.getTime();
            } catch (Throwable e) {
                GISLogger.log(e);
            }
        }
        if (date == null) {
            date = new Date ();
        }
        this.mCurrentDateTime.setTime(date);
    }

    public void setDateFieldDialogListener(DateFieldDialogListener listener) {
        this.mListener = listener;
    }

    protected void onNegativeButtonClicked() {
        super.onNegativeButtonClicked();
        notifyDateValueChanged(null);
        dismissAllowingStateLoss();
    }

    protected void onNeutralButtonClicked() {
        super.onNeutralButtonClicked();
        this.mCurrentDateTime.setTime(new Date ());
        configureDatePicker(this.mCurrentDateTime);
        notifyDateValueChanged(this.mCurrentDateTime.getTime());
        dismissAllowingStateLoss();
    }

    protected void onPositiveButtonClicked() {
        super.onPositiveButtonClicked();
        this.mCurrentDateTime.set(this.mDatePicker.getYear(), this.mDatePicker.getMonth(), this.mDatePicker.getDayOfMonth());
        notifyDateValueChanged(this.mCurrentDateTime.getTime());
        dismissAllowingStateLoss();
    }

    private void configureDatePicker(Calendar dateTime) {
        this.mDatePicker.updateDate(dateTime.get(Calendar.YEAR), dateTime.get(Calendar.MONTH), dateTime.get(Calendar.DAY_OF_MONTH));
        this.mDatePicker.setMinDate(JANUARY_01_1600);
    }

    private void notifyDateValueChanged(Date date) {
        if (this.mListener != null) {
            this.mListener.onDateValueChanged(date);
        }
    }
}
