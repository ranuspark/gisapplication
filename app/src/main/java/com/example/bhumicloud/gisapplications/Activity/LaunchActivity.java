package com.example.bhumicloud.gisapplications.Activity;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.example.bhumicloud.gisapplications.apiLayer.GISServiceInterceptor;
import com.example.bhumicloud.gisapplications.apiLayer.exception.GISServiceException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.InvalidCredentialsException;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.KeyboardUtils;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.google.android.gms.common.GoogleApiAvailability;
import com.mp4parser.iso14496.part15.SyncSampleEntry;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.apiLayer.Client;
import com.example.bhumicloud.gisapplications.Activity.LaunchFragment.LaunchFragmentListener;
import com.example.bhumicloud.gisapplications.Activity.LoginFragment.LoginFragmentListener;
import com.example.bhumicloud.gisapplications.dossier.DataMigrationTask;
import com.example.bhumicloud.gisapplications.dossier.DataMigrationTask.Callback;
import com.example.bhumicloud.gisapplications.dossier.MigrationHistory;
import com.example.bhumicloud.gisapplications.dossier.PersistentStore;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.util.AlertUtils;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import com.example.bhumicloud.gisapplications.View.GISLogoImageView;
import com.example.bhumicloud.gisapplications.View.GISLogoImageView.OnDoubleTapListener;
import io.fabric.sdk.android.Fabric;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class LaunchActivity extends GISActivity implements LaunchFragmentListener, LoginFragmentListener {
    private static final String LAUNCH_ACTION_EDIT_RECORD = "edit-record";
    private static final String LAUNCH_ACTION_NEW_RECORD = "new-record";
    private static final String LAUNCH_ACTION_OPEN = "open";
    private static final String TAG_LAUNCH_FRAGMENT = "TAG_LAUNCH_FRAGMENT";
    private static final String TAG_LOGIN_FRAGMENT = "TAG_LOGIN_FRAGMENT";
    private static final String TAG_RESET_PASSWORD_ERROR = "TAG_RESET_PASSWORD_ERROR";
    private static final String TAG_RESET_PASSWORD_FRAGMENT = "TAG_RESET_PASSWORD_FRAGMENT";
    private AuthenticationTask mAuthenticationTask;
    private ProgressDialog mMigrationProgress;
    private DataMigrationTask mMigrationTask;
    private ResetPasswordTask mResetPasswordTask;
    private Form form;


   class C10706 implements OnClickListener {
        C10706() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            LaunchActivity.this.getSupportFragmentManager().popBackStackImmediate();
        }
    }

    private static class AuthenticationTask extends AsyncTask<Void, Void, Map> {
        private final LaunchActivity mActivity;
        private final Client mClient ;
        private Exception mException;
        private final String mPassword;
        private final String mUsername;

        public AuthenticationTask(LaunchActivity activity, String username, String password ) {
            this.mActivity = activity;
            this.mUsername = username;
            this.mPassword = password;
            this.mClient = new Client ( activity );
        }

        protected HashMap doInBackground(Void... args) {
            HashMap<String, Object> result = null;
            try {
                result = this.mClient.getAccount(this.mUsername, this.mPassword);

            } catch (InvalidCredentialsException e) {
                this.mException = e;
            } catch (IOException e) {
                this.mException = e;
            }
            return result;
        }

        protected void onPostExecute(Map user) {
            super.onPostExecute(user);
            if (user != null) {
                ArrayList<Object> contexts = JSONUtils.getArrayList(user, "contexts");
                ArrayList<Account> accounts = null;
                if (contexts != null && contexts.size() > 0) {
                    accounts = new ArrayList ();
                    Iterator it = contexts.iterator();
                    while (it.hasNext()) {
                        accounts.add(Account.getAccount(user, JSONUtils.getHashMap(it.next())));
                    }
                }
                this.mActivity.onAuthenticationSuccess(accounts, JSONUtils.getString(user, "email"));
                return;
            }
            this.mActivity.onAuthenticationFailure(this.mException);
        }
    }

    private static class ResetPasswordTask extends AsyncTask<String, Void, Boolean> {
        private LaunchActivity mActivity;
        private final Client mClient = new Client(this.mActivity);
        private Exception mException;

        public ResetPasswordTask(LaunchActivity activity) {
            this.mActivity = activity;
        }

        protected Boolean doInBackground(String... args) {
            try {
                return Boolean.valueOf(this.mClient.resetPassword(args[0]));
            } catch (IOException e) {
                this.mException = e;
                return Boolean.valueOf(false);
            }
        }

        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        Fabric.with(this, new Crashlytics.Builder().core(new CrashlyticsCore.Builder().build()).build());
        super.onCreate(savedInstanceState);

        if (!Fabric.isInitialized()) {
            Fabric.with(this, new Crashlytics.Builder().core(new CrashlyticsCore.Builder().build()).build());
                    }


        if (!isTaskRoot()) {
            Intent intent = getIntent();
            String intentAction = intent.getAction();
            if (intent.hasCategory("android.intent.category.LAUNCHER") && intentAction != null && intentAction.equals("android.intent.action.MAIN")) {
                Log.w(LaunchActivity.class.getCanonicalName(), "Main Activity is not the root.  Finishing Main Activity instead of launching.");
                finish();
                return;
            }
        }
        setContentView((int) R.layout.activity_launch);
/*        ((GISLogoImageView) findViewById(R.id.gis_logo)).setOnDoubleTapListener ( new OnDoubleTapListener () {
            @Override
            public void onGisLogoDoubleTap() {
                new ServerConfigurationDialog().show(LaunchActivity.this.getSupportFragmentManager(), "ServerConfigurationDialog");
            }
        } );*/
        if (savedInstanceState == null) {
            FragmentManager fm = getSupportFragmentManager();
            LaunchFragment launchFragment = new LaunchFragment();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.container, launchFragment, TAG_LAUNCH_FRAGMENT);
            ft.commit();
        }
    }

    protected void onResume() {
        super.onResume();
        int result = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if (result != 0) {
            GoogleApiAvailability.getInstance().getErrorDialog(this, result, 0, new OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    LaunchActivity.this.finish();
                }
            }).show();
            return;
        }
        this.mMigrationTask = DataMigrationTask.getInstance();
        this.mMigrationTask.setCallback(new Callback() {
            public void onMigrationProgress(String message) {
                LaunchActivity.this.onDatabaseMigrationProgress(message);
            }

            public void onMigrationFinished() {
                LaunchActivity.this.onDatabaseMigrationFinished();
            }
        });
        if (this.mMigrationProgress == null) {
            this.mMigrationProgress = new ProgressDialog(this);
            this.mMigrationProgress.setTitle(R.string.configuring_database_title);
            this.mMigrationProgress.setIndeterminate(true);
            this.mMigrationProgress.setCancelable(false);
        }
        if (this.mMigrationTask.isRunning()) {
            this.mMigrationProgress.show();
            onDatabaseMigrationProgress(this.mMigrationTask.getRecentMessage());
        } else if (this.mMigrationTask.isPending() && (PersistentStore.needUpgrade(this) || MigrationHistory.isMigrationRequired(this))) {
            this.mMigrationTask.start(this);
            this.mMigrationProgress.show();
            onDatabaseMigrationProgress(this.mMigrationTask.getRecentMessage());
        } else {
            onDatabaseMigrationFinished();
        }
    }
    public void onBackPressed() {
        Fragment fragment = getCurrentFragment();
        if (fragment != null && (fragment instanceof LoginFragment)) {
            finish();
        }
        super.onBackPressed();
    }

    protected void onPause() {
        super.onPause();
        if (this.mAuthenticationTask != null) {
            this.mAuthenticationTask.cancel(false);
            this.mAuthenticationTask = null;
        }
        if (this.mResetPasswordTask != null) {
            this.mResetPasswordTask.cancel(false);
            this.mResetPasswordTask = null;
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.mMigrationTask != null) {
            this.mMigrationTask.setCallback(null);
            this.mMigrationTask = null;
        }
        if (this.mMigrationProgress != null) {
            this.mMigrationProgress.dismiss();
            this.mMigrationProgress = null;
        }
    }

    public void onLoginButtonClick() {
        pushFragment(new LoginFragment(), false, TAG_LOGIN_FRAGMENT);
    }


    public void onLogin(String username, String password) {
        this.mAuthenticationTask = new AuthenticationTask(this, username, password);
        this.mAuthenticationTask.execute(new Void[0]);
    }

    public void onResetPassword(String email) {
        KeyboardUtils.hideSoftKeyboard(this);
        this.mResetPasswordTask = new ResetPasswordTask(this);
        this.mResetPasswordTask.execute(new String[]{email});
    }

    private void pushFragment(Fragment fragment, boolean addToBackStack) {
        pushFragment(fragment, addToBackStack, null);
    }

    private void pushFragment(Fragment fragment, boolean addToBackStack, String tag) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,R.anim.slide_in_right, R.anim.slide_out_left);
            ft.addToBackStack(null);
        } else {
            ft.setTransition( FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        }
        ft.replace(R.id.container, fragment, tag);
        ft.commit();
    }

    private void onDatabaseMigrationProgress(String message) {
        if (this.mMigrationProgress != null && this.mMigrationProgress.isShowing() && message != null) {
            this.mMigrationProgress.setMessage(message);
        }
    }

    private void onDatabaseMigrationFinished() {
        if (this.mMigrationProgress != null) {
            this.mMigrationProgress.dismiss();
            this.mMigrationProgress = null;
        }
        Account currentAccount = Account.getActiveAccount(this);
        if (currentAccount != null) {
            if (PatronSettings.isAutoSyncPostLaunchEnabled(this)) {
                SyncService.synchronizeData(this, currentAccount);
            }
            startDashboardActivity();
        } else {
            if (PatronSettings.isFirstLaunch(this)) {
                //TODO:WELCOME PAGE
            }
            LaunchFragment launchFragment = (LaunchFragment) getSupportFragmentManager().findFragmentByTag(TAG_LAUNCH_FRAGMENT);
            if (launchFragment != null) {
                launchFragment.enableButtons();
            }
        }
        PatronSettings.setIsFirstLaunch(this, false);
    }

    private void startDashboardActivity() {
        Uri uri = getIntent().getData();
        if (uri != null) {
            startDashboardActivity(uri.getHost(), uri);
            return;
        }
        startActivity(new Intent (this, DashboardActivity.class));
        finish();
    }

    private void startDashboardActivity(String action, Uri uri) {
        Account account = Account.getActiveAccount(this);
        Intent intent = new Intent (this, DashboardActivity.class);
        if (TextUtils.isEmpty(action)) {
            Toast.makeText(this, R.string.error_opening_url_bad_action, Toast.LENGTH_LONG).show();
        } else if (action.equals(LAUNCH_ACTION_OPEN)) {
            String formID = uri.getQueryParameter(Record.COLUMN_FORM_ID);
            form = Form.getForm(account, formID);
            if (!TextUtils.isEmpty(formID) && form == null) {
                Toast.makeText(this, R.string.app_not_on_device, Toast.LENGTH_LONG).show();
            } else if (form != null) {
                PatronSettings.setActiveFormID(this, form.getRowID());
            }
        } else if (action.equals(LAUNCH_ACTION_NEW_RECORD)) {
            form = Form.getForm(account, uri.getQueryParameter(Record.COLUMN_FORM_ID));
            if (form == null) {
                Toast.makeText(this, R.string.app_not_on_device, Toast.LENGTH_LONG).show();
            } else {
                if (uri.getQueryParameterNames().contains(SyncSampleEntry.TYPE)) {
                    SyncService.synchronizeData(this, account);
                    intent.putExtra(DashboardActivity.EXTRA_ACTION, action);
                } else {
                    intent = new Intent (this, RecordEditorActivity.class);
                }
                intent.putExtra(RecordEditorActivity.EXTRA_FORM_ID, form.getRowID());
                intent.putExtra(RecordEditorActivity.EXTRA_RECORD_DEFAULTS, JSONUtils.toJSONString(getRecordParameters(uri)));
            }
        } else if (action.equals(LAUNCH_ACTION_EDIT_RECORD)) {
            Record record = Record.getRecordByRemoteID(account, uri.getQueryParameter(Attachment.COLUMN_RECORD_ID));
            if (record == null) {
                Toast.makeText(this, R.string.record_not_on_device, Toast.LENGTH_LONG).show();
            } else {
                if (uri.getQueryParameterNames().contains(SyncSampleEntry.TYPE)) {
                    SyncService.synchronizeData(this, account);
                    intent.putExtra(DashboardActivity.EXTRA_ACTION, action);
                } else {
                    intent = new Intent (this, RecordEditorActivity.class);
                }
                intent.putExtra(RecordEditorActivity.EXTRA_RECORD_ID, record.getRowID());
                intent.putExtra(RecordEditorActivity.EXTRA_RECORD_DEFAULTS, JSONUtils.toJSONString(getRecordParameters(uri)));
            }
        } else {
            Toast.makeText(this, R.string.error_opening_url_bad_action, Toast.LENGTH_LONG).show();
        }
        startActivity(intent);
        finish();
    }

    private HashMap<String, String> getRecordParameters(Uri uri) {
        HashMap<String, String> data = new HashMap ();
        for (String name : uri.getQueryParameterNames()) {
            if (!(name.equals(Record.COLUMN_FORM_ID) || name.equals(Attachment.COLUMN_RECORD_ID) || name.equals(SyncSampleEntry.TYPE))) {
                data.put(name, uri.getQueryParameter(name));
            }
        }
        return data;
    }

    private void onAuthenticationSuccess(ArrayList<Account> accounts, String emailAddress) {
        GISLogger.logLogin();
        if (accounts == null || accounts.size() <= 0) {
            Builder b = new Builder (this);
            b.setTitle(R.string.zero_contexts_title);
            b.setMessage(getResources().getString(R.string.zero_contexts_message, new Object[]{emailAddress}));
            b.show();
            resetIsLoginFormValid();
        } else if (accounts.size() == 1) {
            setAccount((Account) accounts.get(0));
        } else {
            showAccountPicker(accounts);
        }
    }

    private void onAuthenticationFailure(Exception error) {
        if (!isFinishing() && !isDestroyed()) {
            Builder b = new Builder (this);
            b.setTitle(R.string.login_failed_title);
            if (error == null) error = new InvalidCredentialsException ();
            b.setMessage(AlertUtils.getExceptionMessage(this, error));
            b.show();
            KeyboardUtils.hideSoftKeyboard(this);
            resetIsLoginFormValid();
        }
    }

    private void setAccount(Account account) {
        account.save();
        Account.setActiveAccount(account);
        SyncService.synchronizeData(this, account);
        startDashboardActivity();
    }

    private void showAccountPicker(final ArrayList<Account> accounts) {
        Builder b = new Builder (this);
        b.setTitle(R.string.select_account_title);
        String[] titlesArr = new String[accounts.size()];
        for (int i = 0; i < accounts.size(); i++) {
            titlesArr[i] = ((Account) accounts.get(i)).getContextName();
        }
        b.setItems(titlesArr, new OnClickListener () {
            public void onClick(DialogInterface dialog, int which) {
                LaunchActivity.this.setAccount((Account) accounts.get(which));
            }
        });
        AlertDialog alert = b.create();
        alert.setOnDismissListener ( new OnDismissListener () {
            @Override
            public void onDismiss(DialogInterface dialog) {
                LaunchActivity.this.resetIsLoginFormValid();
            }
        } );
        alert.show();
    }

    private void resetIsLoginFormValid() {
        LoginFragment loginFragment = (LoginFragment) getSupportFragmentManager().findFragmentByTag(TAG_LOGIN_FRAGMENT);
        if (loginFragment != null) {
            loginFragment.resetIsLoginFormValid();
        }
    }
}
