package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import android.view.ViewGroup;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.field.SectionElement;

public class InlineSectionElementView extends ElementView<SectionElement> {
    private final ViewGroup mContainer = ((ViewGroup) findViewById(R.id.container));

    public InlineSectionElementView(Context context, SectionElement element, boolean viewOnly) {
        super(context, element, viewOnly);
    }

    public ViewGroup getContainerView() {
        return this.mContainer;
    }

    protected int getElementViewLayout() {
        return R.layout.view_inline_section_element;
    }
}
