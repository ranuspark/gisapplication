package com.example.bhumicloud.gisapplications.Activity;

import android.content.DialogInterface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.Project;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement.StatusOption;
import com.example.bhumicloud.gisapplications.util.RecordFilterOptions;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class RecordFilterDialog extends ThemedDialogFragment {
    public static final String ARG_FORM_ID = "RecordFilterDialog.ARG_FORM_ID";
    private static final String STATE_FILTER_OPTIONS = "mFilterOptions";
    private RecordFilterOptions mFilterOptions;
    private Form mForm;
    private LayoutInflater mLayoutInflater;
    private ViewGroup mProjectsContainer;
    private ViewGroup mStatusesContainer;
    private ViewGroup mStatusesSection;
    private TextView mStatusesSectionTitle;


    public static RecordFilterDialog getInstance(long formID) {
        RecordFilterDialog dialog = new RecordFilterDialog();
        Bundle args = new Bundle();
        args.putLong(ARG_FORM_ID, formID);
        dialog.setArguments(args);
        return dialog;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle((int) R.string.record_filter_title);
        setNeutralButtonText((int) R.string.reset_filter);
        setPositiveButtonText((int) R.string.done);
        Bundle args = getArguments();
        if (args != null) {
            long formID = args.getLong(ARG_FORM_ID, -1);
            if (formID >= 0) {
                this.mForm = Form.getForm(formID);
            }
        }
        if (this.mForm == null) {
            dismissAllowingStateLoss();
            return;
        }
        this.mLayoutInflater = getActivity().getLayoutInflater();
        if (this.mFilterOptions == null) {
            this.mFilterOptions = new RecordFilterOptions();
        }
        if (savedInstanceState != null) {
            RecordFilterOptions opts = (RecordFilterOptions) savedInstanceState.getParcelable(STATE_FILTER_OPTIONS);
            if (opts != null) {
                this.mFilterOptions = opts;
            }
        }
    }

    public View onCreateContentView(LayoutInflater inflater, ViewGroup root, Bundle savedState) {
        View view = inflater.inflate(R.layout.dialog_record_filter, root, false);
        this.mStatusesSection = (ViewGroup) view.findViewById(R.id.statuses_section);
        this.mProjectsContainer = (ViewGroup) view.findViewById(R.id.projects_container);
        this.mStatusesContainer = (ViewGroup) view.findViewById(R.id.statuses_container);
        this.mStatusesSectionTitle = (TextView) view.findViewById(R.id.record_status_filter_title);
        return view;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onReloadProjects();
        onReloadStatuses();
    }

    public void onSaveInstanceState(Bundle outBundle) {
        super.onSaveInstanceState(outBundle);
        outBundle.putParcelable(STATE_FILTER_OPTIONS, this.mFilterOptions);
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.mFilterOptions.resetTemp();
    }

    protected void onNeutralButtonClicked() {
        super.onNeutralButtonClicked();
        this.mFilterOptions.resetFilter();
        onPositiveButtonClicked();
    }

    protected void onPositiveButtonClicked() {
        super.onPositiveButtonClicked();
        this.mFilterOptions.commitChanges();
        dismiss();
    }

    private void onReloadProjects() {
        this.mProjectsContainer.removeAllViews();
        ArrayList<Project> projects = Project.getProjects(Account.getActiveAccount());
        if (projects == null || projects.isEmpty()) {
            TextView emptyLabel = new TextView(getActivity());
            emptyLabel.setText(R.string.empty_projects_info);
            emptyLabel.setGravity(17);
            emptyLabel.setTypeface(null, 2);
            this.mProjectsContainer.addView(emptyLabel);
            return;
        }
        Iterator it = projects.iterator();
        while (it.hasNext()) {
            this.mProjectsContainer.addView(getProjectRowView((Project) it.next()));
        }
    }

    private void onReloadStatuses() {
        this.mStatusesContainer.removeAllViews();
        StatusElement statusElement = this.mForm.getStatusElement();
        if (statusElement != null) {
            this.mStatusesSection.setVisibility( View.VISIBLE);
            this.mStatusesSectionTitle.setText(statusElement.getLabel());
            List<StatusOption> options = statusElement.getOptions();
            if (options != null) {
                for (StatusOption option : options) {
                    this.mStatusesContainer.addView(getStatusRowView(option));
                }
                return;
            }
            return;
        }
        this.mStatusesSection.setVisibility(View.GONE);
    }

    private View getStatusRowView(final StatusOption option) {
        View row = this.mLayoutInflater.inflate(R.layout.dialog_record_filter_status_item, null);
        GradientDrawable statusDot = (GradientDrawable) ContextCompat.getDrawable(getContext(), R.drawable.circle);
        statusDot.setColor(option.getColor());
        CheckBox label = (CheckBox) row.findViewById(R.id.label);
        label.setText(option.getLabel());
        label.setChecked(this.mFilterOptions.isEnabled(option));
        label.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                RecordFilterDialog.this.onStatusRowValueChanged(option, isChecked);
            }
        });
        label.setCompoundDrawablesWithIntrinsicBounds(statusDot, null, null, null);
        return row;
    }

    private View getProjectRowView(final Project project) {
        View row = this.mLayoutInflater.inflate(R.layout.dialog_record_filter_project_item, null);
        CheckBox label = (CheckBox) row.findViewById(R.id.label);
        label.setText(project.getName());
        label.setChecked(this.mFilterOptions.isEnabled(project));
        label.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                RecordFilterDialog.this.onProjectRowValueChanged(project, isChecked);
            }
        });
        return row;
    }

    private void onStatusRowValueChanged(StatusOption option, boolean enabled) {
        if (enabled) {
            this.mFilterOptions.addStatusOption(option);
        } else {
            this.mFilterOptions.removeStatusOption(option);
        }
    }

    private void onProjectRowValueChanged(Project project, boolean enabled) {
        if (enabled) {
            this.mFilterOptions.addProject(project);
        } else {
            this.mFilterOptions.removeProject(project);
        }
    }
}

