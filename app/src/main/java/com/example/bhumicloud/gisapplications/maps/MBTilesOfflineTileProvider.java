package com.example.bhumicloud.gisapplications.maps;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Tile;
import com.google.android.gms.maps.model.TileProvider;
import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.Template;
import com.example.bhumicloud.gisapplications.model.InteractivityData;
import com.example.bhumicloud.gisapplications.util.CompressionUtils;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import io.fabric.sdk.android.services.network.HttpRequest;

public class MBTilesOfflineTileProvider implements TileProvider {
    private SQLiteDatabase mDatabase;
    private String mInteractivityTemplate;
    private int mMaxZoom = 255;
    private int mMinZoom = 0;

    public MBTilesOfflineTileProvider(File file) {
        this.mDatabase = SQLiteDatabase.openDatabase(file.getAbsolutePath(), null, 17);
        calculateZoomConstraints();
        retrieveInteractivityTemplate();
    }

    public Tile getTile(int x, int y, int z) {
        Tile tile = NO_TILE;
        if (isZoomLevelAvailable(z) && isDatabaseAvailable()) {
            String[] projection = new String[]{"tile_data"};
            String[] values = new String[]{String.valueOf(((int) (Math.pow(2.0d, (double) z) - ((double) y))) - 1), String.valueOf(x), String.valueOf(z)};
            Cursor c = this.mDatabase.query("tiles", projection, "tile_row = ? AND tile_column = ? AND zoom_level = ?", values, null, null, null);
            if (c != null) {
                c.moveToFirst();
                if (!c.isAfterLast()) {
                    tile = new Tile (256, 256, c.getBlob(0));
                }
                c.close();
            }
        }
        return tile;
    }

    public int getMaximumZoom() {
        return this.mMaxZoom;
    }

    public void close() {
        if (this.mDatabase != null) {
            this.mDatabase.close();
            this.mDatabase = null;
        }
    }

    public boolean supportsInteractivity() {
        return !TextUtils.isEmpty(this.mInteractivityTemplate);
    }

    public InteractivityData getInteractivityData(LatLng coordinate, int zoom) {
        if (TextUtils.isEmpty(this.mInteractivityTemplate)) {
            return null;
        }
        zoom = Math.max( Math.min(zoom, this.mMaxZoom), this.mMinZoom);
        double n = Math.pow(2.0d, (double) zoom);
        double latDeg = coordinate.latitude;
        double lonDeg = coordinate.longitude;
        double latRad = Math.toRadians(latDeg);
        double tileX = ((180.0d + lonDeg) / 360.0d) * n;
        double tileY = ((1.0d - (Math.log( Math.tan(latRad) + (1.0d / Math.cos(latRad))) / 3.141592653589793d)) / 2.0d) * n;
        int tileNumberX = (int) Math.floor(tileX);
        int tileNumberY = (int) ((n - ((double) ((int) Math.floor(tileY)))) - 1.0d);
        int tilePixelX = (int) Math.round((tileX - ((double) tileNumberX)) * 256.0d);
        int tilePixelY = (int) Math.round((((double) tileNumberY) - ((n - tileY) - 1.0d)) * 256.0d);
        HashMap<String, Object> grid = getInteractivityGrid(tileNumberX, tileNumberY, zoom);
        if (grid == null || grid.isEmpty()) {
            return null;
        }
        String keyName = getInteractivityKeyName(tilePixelX, tilePixelY, grid);
        if (TextUtils.isEmpty(keyName)) {
            return null;
        }
        HashMap<String, Object> data = getInteractivityGridData(tileNumberX, tileNumberY, zoom, keyName);
        HashMap<String, Object> context = new HashMap ();
        for (Entry<String, Object> entry : data.entrySet()) {
            if (!(TextUtils.isEmpty((CharSequence) entry.getKey()) || entry.getValue() == null)) {
                context.put(entry.getKey(), entry.getValue());
            }
        }
        Template template = Mustache.compiler().defaultValue("").compile(this.mInteractivityTemplate);
        InteractivityData result = new InteractivityData();
        context.put("__full__", Boolean.valueOf(false));
        context.put("__teaser__", Boolean.valueOf(true));
        result.setSnippet(template.execute(context));
        context.put("__full__", Boolean.valueOf(true));
        context.put("__teaser__", Boolean.valueOf(false));
        result.setContent(template.execute(context));
        return result;
    }

    public HashMap<String, Object> getInteractivityGrid(int x, int y, int z) {
        byte[] compressed = null;
        String[] selection = new String[]{"grid"};
        String[] values = new String[]{String.valueOf(x), String.valueOf(y), String.valueOf(z)};
        Cursor c = this.mDatabase.query("grids", selection, "tile_column = ? AND tile_row = ? AND zoom_level = ?", values, null, null, null);
        if (c != null) {
            c.moveToFirst();
            if (!c.isAfterLast()) {
                compressed = c.getBlob(0);
            }
            c.close();
        }
        if (compressed == null) {
            return null;
        }
        try {
            return JSONUtils.objectFromJSON(new String (CompressionUtils.inflate(compressed), HttpRequest.CHARSET_UTF8));
        } catch (IOException e) {
            throw new RuntimeException (e);
        }
    }

    public HashMap<String, Object> getInteractivityGridData(int x, int y, int z, String keyname) {
        String jsonString = null;
        String[] selection = new String[]{"key_json"};
        String[] values = new String[]{String.valueOf(x), String.valueOf(y), String.valueOf(z), keyname};
        Cursor c = this.mDatabase.query("grid_data", selection, "tile_column = ? AND tile_row = ? AND zoom_level = ? AND key_name = ?", values, null, null, null);
        if (c != null) {
            c.moveToFirst();
            if (!c.isAfterLast()) {
                jsonString = c.getString(0);
            }
            c.close();
        }
        if (TextUtils.isEmpty(jsonString)) {
            return null;
        }
        try {
            return JSONUtils.objectFromJSON(jsonString);
        } catch (IOException e) {
            throw new RuntimeException (e);
        }
    }

    public String getInteractivityKeyName(int x, int y, Map gridJSON) {
        List rows = JSONUtils.getArrayList(gridJSON, "grid");
        List keys = JSONUtils.getArrayList(gridJSON, "keys");
        if (rows == null || rows.isEmpty() || keys == null) {
            return null;
        }
        int row = y / (256 / rows.size());
        int col = x / (256 / rows.size());
        if (row >= rows.size()) {
            return null;
        }
        String line = JSONUtils.getString(rows, row);
        if (col >= line.length()) {
            return null;
        }
        int decoded = line.codePointAt(col);
        if (decoded >= 93) {
            decoded--;
        }
        if (decoded >= 35) {
            decoded--;
        }
        decoded -= 32;
        if (decoded < keys.size()) {
            return JSONUtils.getString(keys, decoded);
        }
        return null;
    }

    private void calculateZoomConstraints() {
        Integer min = getMetadataInteger("minzoom");
        if (min != null) {
            this.mMinZoom = min.intValue();
        }
        Integer max = getMetadataInteger("maxzoom");
        if (max != null) {
            this.mMaxZoom = max.intValue();
        }
    }

    private void retrieveInteractivityTemplate() {
        this.mInteractivityTemplate = getMetadataString("template");
    }

    private boolean isZoomLevelAvailable(int zoom) {
        return (this.mMinZoom < 0 || this.mMinZoom <= zoom) && (this.mMaxZoom < 0 || this.mMaxZoom >= zoom);
    }

    private boolean isDatabaseAvailable() {
        return this.mDatabase != null && this.mDatabase.isOpen();
    }

    private String getMetadataString(String name) {
        String result = null;
        Cursor c = getMetadataCursor(name);
        if (c != null) {
            c.moveToFirst();
            if (!c.isAfterLast()) {
                result = c.getString(0);
            }
            c.close();
        }
        return result;
    }

    private Integer getMetadataInteger(String name) {
        Integer result = null;
        Cursor c = getMetadataCursor(name);
        if (c != null) {
            c.moveToFirst();
            if (!c.isAfterLast()) {
                result = Integer.valueOf(c.getInt(0));
            }
            c.close();
        }
        return result;
    }

    private Cursor getMetadataCursor(String name) {
        if (!isDatabaseAvailable()) {
            return null;
        }
        return this.mDatabase.query("metadata", new String[]{"value"}, "name = ?", new String[]{name}, null, null, null);
    }
}
