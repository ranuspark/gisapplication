package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.Role;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkElement;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkItemValue;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkValue;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement.StatusOption;
import com.example.bhumicloud.gisapplications.model.validation.error.FieldLengthValidationError;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import java.util.Iterator;

public class RecordLinkElementView extends ElementView<Element> {
    private static final int MAX_RECORD_LINKS_SHOWN = 100;
    private Button mAddRecordLinkButton;
    private RecordLinkElementViewListener mListener;
    private LinearLayout mRecordLinkButtonsLayout;
    private RecordLinkValue mRecordLinkValue;
    private LinearLayout mRecordLinksLayout;
    private Button mSelectRecordLinkButton;

    public interface RecordLinkElementViewListener {
        void onCreateNewRecordLinkButtonClicked(RecordLinkElement recordLinkElement);

        void onDeleteRecordLinkItem(RecordLinkElement recordLinkElement, RecordLinkItemValue recordLinkItemValue);

        void onRecordLinkItemClicked(RecordLinkElement recordLinkElement, RecordLinkItemValue recordLinkItemValue);

        void onSelectExistingRecordLinkButtonClicked(RecordLinkElement recordLinkElement);
    }

    class C12371 implements OnClickListener {
        C12371() {
        }

        public void onClick(View v) {
            RecordLinkElementView.this.onAddNewRecordLinkButtonClicked(true);
        }
    }

    class C12382 implements OnClickListener {
        C12382() {
        }

        public void onClick(View v) {
            RecordLinkElementView.this.onAddNewRecordLinkButtonClicked(false);
        }
    }

    private static class RecordLinkItemView extends FrameLayout {
        private ImageView mDeleteView;
        private RecordLinkElement mElement;
        private RecordLinkElementViewListener mListener;
        private RecordLinkItemValue mRecordLinkItemValue;
        private View mStatusView;
        private TextView mTitleView;

        class C12391 implements OnClickListener {
            C12391() {
            }

            public void onClick(View v) {
                if (RecordLinkItemView.this.mListener != null) {
                    RecordLinkItemView.this.mListener.onRecordLinkItemClicked(RecordLinkItemView.this.mElement, RecordLinkItemView.this.mRecordLinkItemValue);
                }
            }
        }

        class C12402 implements OnClickListener {
            C12402() {
            }

            public void onClick(View v) {
                if (RecordLinkItemView.this.mListener != null) {
                    RecordLinkItemView.this.mListener.onDeleteRecordLinkItem(RecordLinkItemView.this.mElement, RecordLinkItemView.this.mRecordLinkItemValue);
                }
            }
        }

        public RecordLinkItemView(Context context, RecordLinkElement element, RecordLinkItemValue itemValue, RecordLinkElementViewListener listener, boolean isReadOnly) {
            super(context);
            this.mElement = element;
            this.mRecordLinkItemValue = itemValue;
            this.mListener = listener;
            View v = LayoutInflater.from(getContext()).inflate(R.layout.view_record_link_item, this, true);
            this.mStatusView = v.findViewById(R.id.record_link_status_indicator);
            this.mTitleView = (TextView) v.findViewById(R.id.record_link_record_title);
            this.mDeleteView = (ImageView) v.findViewById(R.id.record_link_delete_btn);
            if (isReadOnly) {
                this.mDeleteView.setVisibility(View.GONE);
            }
            reloadRecord(this.mRecordLinkItemValue);
        }

        public RecordLinkItemValue getRecordLinkItemValue() {
            return this.mRecordLinkItemValue;
        }

        public void reloadRecord(RecordLinkItemValue itemValue) {
            this.mRecordLinkItemValue = itemValue;
            Record record = this.mRecordLinkItemValue.getRecord();
            if (record == null) {
                this.mStatusView.setVisibility(View.GONE);
                this.mTitleView.setText(getResources().getText(R.string.record_not_on_device));
            } else {
                StatusElement statusElement = record.getStatusValue().getElement();
                String status = record.getRecordStatus();
                boolean setStatusColor = false;
                if (!(statusElement == null || TextUtils.isEmpty(status))) {
                    StatusOption statusOption = statusElement.getOption(status);
                    if (statusOption != null) {
                        try {
                            this.mStatusView.setBackgroundColor(statusOption.getColor());
                            setStatusColor = true;
                        } catch (Throwable e) {
                            GISLogger.log(e);
                        }
                    }
                }
                if (!setStatusColor) {
                    this.mStatusView.setVisibility(View.GONE);
                }
                this.mTitleView.setText(record.getTitle());
            }
            setOnClickListener(new C12391());
            this.mDeleteView.setOnClickListener(new C12402());
        }
    }

    public RecordLinkElementView(Context context, Element element, boolean viewOnly) {
        super(context, element, viewOnly);
    }

    public void setListener(RecordLinkElementViewListener listener) {
        this.mListener = listener;
    }

    public void setValue(FormValue value) {
        this.mRecordLinkValue = (RecordLinkValue) value;
        this.mRecordLinksLayout.removeAllViews();
        if (this.mRecordLinkValue != null) {
            int count = 0;
            Iterator it = this.mRecordLinkValue.getRecordLinks().iterator();
            while (it.hasNext()) {
                addRecordLinkToView((RecordLinkItemValue) it.next());
                count++;
                if (count >= 100) {
                    break;
                }
            }
        }
        setButtonVisibility((RecordLinkElement) getElement());
    }

    public void onDestroy(Context context) {
        super.onDestroy(context);
        this.mListener = null;
        this.mRecordLinksLayout.removeAllViews();
    }

    public void refreshRecordLinkItemView(RecordLinkItemValue recordLinkitemValue) {
        for (int i = 0; i < this.mRecordLinksLayout.getChildCount(); i++) {
            RecordLinkItemView view = (RecordLinkItemView) this.mRecordLinksLayout.getChildAt(i);
            if (view.getRecordLinkItemValue().getRecordIdentifier().equals(recordLinkitemValue.getRecordIdentifier())) {
                view.reloadRecord(recordLinkitemValue);
            }
        }
    }

    public void update() {
        super.update();
        setButtonVisibility((RecordLinkElement) getElement());
    }

    protected void initialize() {
        super.initialize();
        this.mRecordLinksLayout = (LinearLayout) findViewById(R.id.record_links_container);
        this.mRecordLinkButtonsLayout = (LinearLayout) findViewById(R.id.record_link_buttons);
        this.mAddRecordLinkButton = (Button) findViewById(R.id.record_link_create_new_btn);
        this.mAddRecordLinkButton.setTag(getElement().getDataName() + ":add");
        this.mAddRecordLinkButton.setOnClickListener(new C12371());
        this.mSelectRecordLinkButton = (Button) findViewById(R.id.record_link_select_existing_btn);
        this.mSelectRecordLinkButton.setTag(getElement().getDataName() + ":select");
        this.mSelectRecordLinkButton.setOnClickListener(new C12382());
    }

    protected int getElementViewLayout() {
        return R.layout.view_record_link_element;
    }

    private void onAddNewRecordLinkButtonClicked(boolean creating) {
        onFieldViewClicked();
        RecordLinkElement element = (RecordLinkElement) getElement();
        if (element.hasMaxLength() && this.mRecordLinkValue != null && this.mRecordLinkValue.length() >= element.getMaxLength()) {
            Toast.makeText(getContext(), new FieldLengthValidationError(element).getMessage(), Toast.LENGTH_SHORT).show();
        } else if (this.mListener == null) {
        } else {
            if (creating) {
                this.mListener.onCreateNewRecordLinkButtonClicked(element);
            } else {
                this.mListener.onSelectExistingRecordLinkButtonClicked(element);
            }
        }
    }

    private void addRecordLinkToView(RecordLinkItemValue recordLink) {
        this.mRecordLinksLayout.addView(new RecordLinkItemView(getContext(), (RecordLinkElement) getElement(), recordLink, this.mListener, isReadOnly()));
    }

    private void setButtonVisibility(RecordLinkElement element) {
        if (isReadOnly() || !(element.allowsMultiple() || this.mRecordLinkValue == null || this.mRecordLinkValue.length() <= 0)) {
            this.mRecordLinkButtonsLayout.setVisibility(View.GONE);
            return;
        }
        this.mRecordLinkButtonsLayout.setVisibility(View.VISIBLE);
        Role role = Account.getActiveAccount().getRole();
        if (element.allowsCreating() && role.canCreateRecords()) {
            this.mAddRecordLinkButton.setVisibility(View.VISIBLE);
        } else {
            this.mAddRecordLinkButton.setVisibility(View.INVISIBLE);
        }
        if (element.allowsExisting()) {
            this.mSelectRecordLinkButton.setVisibility(View.VISIBLE);
        } else {
            this.mSelectRecordLinkButton.setVisibility(View.GONE);
        }
    }
}
