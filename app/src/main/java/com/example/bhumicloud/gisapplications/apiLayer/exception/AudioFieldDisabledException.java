package com.example.bhumicloud.gisapplications.apiLayer.exception;

import android.content.Context;

import com.example.bhumicloud.gisapplications.R;

public class AudioFieldDisabledException extends GISServiceException {
    public AudioFieldDisabledException() {
        super("The subscription on this account must be upgraded to upload audio. Please contact the account owner for more information.");
    }

    public String getUserFriendlyMessage(Context context) {
        return context.getString(R.string.audio_field_disabled_message);
    }
}
