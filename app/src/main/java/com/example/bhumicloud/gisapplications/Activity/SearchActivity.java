package com.example.bhumicloud.gisapplications.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.dossier.FormLoader;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.Role;
import com.example.bhumicloud.gisapplications.model.Search;
import com.example.bhumicloud.gisapplications.model.TrashCan;
import com.example.bhumicloud.gisapplications.util.AlertUtils;
import com.example.bhumicloud.gisapplications.util.CursorUtils;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.PatronSettings;

import java.util.ArrayList;


public class SearchActivity extends LocationListenerActivity implements FormPickerFragment.FormPickerFragmentListener, SyncService.Listener, RecordCollectionViewListener, LoaderManager.LoaderCallbacks<Cursor> {
    private Toolbar mToolbar;
    private long mActiveFormID;
    private int mActiveViewMode;
    private RecordCollectionView mCollectionView;
    private FormPickerFragment mFormPickerFragment;
    public static final int VIEW_MODE_LIST = 2;
    public static final int VIEW_MODE_MAP = 1;
    private MenuItem mViewToggleItem;
    private boolean mMenuItemVisibility;
    ArrayList<Search> re_list=new ArrayList<>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_search);
        this.mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.mToolbar);
        this.mActiveFormID = PatronSettings.getActiveFormID(this);
        this.mActiveFormID = PatronSettings.getActiveFormID(this);
        this.mActiveViewMode = PatronSettings.getActiveViewMode(this, 2);





        FragmentManager fm = getSupportFragmentManager();
//        if (savedInstanceState != null) {
//            this.mSearchQuery = savedInstanceState.getString(STATE_SEARCH_QUERY);
//            this.mSearchVisible = savedInstanceState.getBoolean(STATE_SEARCH_VISIBLE, false);
//        }
        Fragment collectionView = fm.findFragmentById(R.id.container);
        if (collectionView instanceof RecordCollectionView) {
            this.mCollectionView = (RecordCollectionView) collectionView;
        }
        configureCollectionView();
        //this.mFormPickerFragment = (FormPickerFragment) getSupportFragmentManager().findFragmentById(R.id.form_picker_drawer);
//        this.mFormDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        this.mFormDrawer.addDrawerListener(this);
        reloadForms();
      //  FragmentManager fm = getSupportFragmentManager();
//        Bundle extras = getIntent().getExtras();
//         re_list= (ArrayList<Search>) extras.getSerializable("record_list");
//        Log.e("GetList1", re_list.get(0).getId()+" ");
//
//         Bundle bundle=new Bundle();
//        bundle.putSerializable("record_list", re_list);

        FragmentTransaction ft = fm.beginTransaction();
        RecordListFragment1 list = new RecordListFragment1();
      //  list.setArguments(bundle);
        ft.replace(R.id.container, list);
        ft.commit();


    }

    private void reloadForms() {
        logDebugEvent("reloadForms()");
        this.mFormPickerFragment.reloadForms();
        getSupportLoaderManager().restartLoader(100, null, this);
    }
    private void configureCollectionView() {
        if (this.mCollectionView != null) {
            this.mCollectionView.setFormID(getActiveFormID());
        }
    }

    private long getActiveFormID() {
        return this.mActiveFormID;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
      //  hideSyncAnimation();
        getMenuInflater().inflate(R.menu.activity_search, menu);
       // this.mFilterItem = menu.findItem(R.id.menu_item_filter);
        this.mViewToggleItem = menu.findItem(R.id.menu_item_view_toggle);
      //  this.mSearchRecordsItem = menu.findItem(R.id.menu_item_search);
       // MenuItem synchronizeItem = menu.findItem(R.id.menu_item_synchronize);
     //   final SearchRelativeLayout searchView = (SearchRelativeLayout) MenuItemCompat.getActionView(this.mSearchRecordsItem);
      //  this.mSearchActionView = searchView.getSearchEditText();
       /* searchView.setOnSearchQueryChangedListener ( new SearchRelativeLayout.OnSearchQueryChangedListener () {
            @Override
            public void onSearchQueryChanged(String query) {
                DashboardActivity.this.mSearchQuery = query;
                if (DashboardActivity.this.mCollectionView != null) {
                    DashboardActivity.this.mCollectionView.setSearchQuery(query);
                }
            }
        } ); */

     /*   MenuItemCompat.setOnActionExpandListener(this.mSearchRecordsItem, new MenuItemCompat.OnActionExpandListener() {
            public boolean onMenuItemActionExpand(MenuItem item) {
                DashboardActivity.this.mSearchVisible = true;
                searchView.onMenuItemActionExpand(item);
                return true;
            }

            public boolean onMenuItemActionCollapse(MenuItem item) {
                DashboardActivity.this.mSearchVisible = false;
                searchView.onMenuItemActionCollapse(item);
                return true;
            }
        });*/

      /*  this.mSearchActionView.setText(this.mSearchQuery);
        if (this.mSearchVisible) {
            this.mSearchRecordsItem.expandActionView();
            if (this.mSearchQuery != null) {
                this.mSearchActionView.setSelection(this.mSearchQuery.length());
            }
        }
        this.mSyncActionView = MenuItemCompat.getActionView(synchronizeItem);
        this.mSyncActionView.setOnClickListener ( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DashboardActivity.this.onSynchronizeDataOptionSelected();
            }
        } );
        if (isCurrentlySynchronizing()) {
            showSyncAnimation();
        } else {
            hideSyncAnimation();
        }
        reloadMenuItemVisibility(this.mMenuItemVisibility);*/
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.menu_item_synchronize:
//                onSynchronizeDataOptionSelected();
//                return true;
            case R.id.menu_item_view_toggle:
                onToggleViewModeOptionSelected();
                return true;
//            case R.id.menu_item_settings:
//                onAppSettingsMenuItemSelected();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onToggleViewModeOptionSelected() {
        switch (this.mActiveViewMode) {
            case VIEW_MODE_MAP:
                setViewMode(VIEW_MODE_LIST);
                return;

            case VIEW_MODE_LIST:
                setViewMode(VIEW_MODE_MAP);
                return;
            default:
                return;
        }
    }
    private void setViewMode(int mode) {
        setViewMode(mode, true);
    }

    private void setViewMode(int mode, boolean async) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        switch (mode) {
            case VIEW_MODE_MAP /*1*/:
                if (this.mViewToggleItem != null) {
                    this.mViewToggleItem.setIcon(R.drawable.ic_action_list);
                    this.mViewToggleItem.setTitle(R.string.list_view);
                }
                if (!(this.mCollectionView instanceof RecordMapFragment)) {
                    RecordMapFragment map = new RecordMapFragment();
                    ft.replace(R.id.container, map);
                    this.mCollectionView = map;
                    break;
                }
                break;
            case VIEW_MODE_LIST /*2*/:
                if (this.mViewToggleItem != null) {
                    this.mViewToggleItem.setIcon(R.drawable.ic_action_map);
                    this.mViewToggleItem.setTitle(R.string.map_view);
                }
                if (!(this.mCollectionView instanceof RecordListFragment1)) {
                    RecordListFragment1 list = new RecordListFragment1();
                    ft.replace(R.id.container, list);
                    this.mCollectionView = list;
                    break;
                }
                break;
        }
        try {
            ft.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
        }
        configureCollectionView();
        this.mActiveViewMode = mode;
        if (!async) {
            fm.executePendingTransactions();
        }
        PatronSettings.setActiveViewMode(this, mode);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new FormLoader(this, "_id", "name");

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        onFormsCursorLoaded(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onFormSelected(long selectedFormID) {
        Form form = Form.getForm(selectedFormID);
        try {
            this.mActiveFormID = form.getRowID();
            updateFormButton(form.getName());
        } catch (NullPointerException e) {
            GISLogger.log("No form found for selectedFormID: " + selectedFormID);
        }
        PatronSettings.setActiveFormID(this, this.mActiveFormID);
        if (this.mCollectionView != null) {
            this.mCollectionView.setFormID(this.mActiveFormID);
        }
//        this.mFormDrawer.setDrawerLockMode(0);
//        this.mFormDrawer.closeDrawer((int) GravityCompat.START);
    }

    @Override
    public void onDeleteRecord(long recordID) {
//        if (isCurrentlySynchronizing()) {
//            AlertUtils.showSynchronizingAlert(this);
//            return;
//        }
        Record record = Record.getRecord(recordID);
        if (record != null) {
            record.deleteWithTransaction();
            Toast.makeText(this, R.string.record_deleted_success, Toast.LENGTH_SHORT).show();
            if (this.mCollectionView != null) {
                this.mCollectionView.reloadData();
            }
            Form form = record.getForm();
            if (form != null) {
                form.decrementRecordCount();
            }
        }
        TrashCan.empty(this);
    }

    @Override
    public void onDuplicateRecord(long recordID, boolean location, boolean values) {
//        if (isCurrentlySynchronizing()) {
//            AlertUtils.showSynchronizingAlert(this);
//        } else {
            startActivity(RecordEditorActivity.getDuplicateRecordIntent(this, recordID, location, values));
      //  }
    }

    @Override
    public void onGenerateReport(long record) {

    }

    @Override
    public void onNewRecordOptionSelected() {
        Account account = Account.getActiveAccount();
        if (account == null) {
            return;
        }
        if (!account.getRole().canCreateRecords()) {
            AlertUtils.showPermissionsAlert(this, Role.PERMISSION_CREATE_RECORDS);
        }
//        else if (isCurrentlySynchronizing()) {
//            AlertUtils.showSynchronizingAlert(this);
//        }
        else {
            startActivity(RecordEditorActivity.getNewRecordIntent(this, getActiveFormID()));
        }
    }

    @Override
    public void onRecordSelected(long j) {

    }

    @Override
    public void onShowRecordLocation(long j, double d, double d2) {

    }

    @Override
    public void onSyncFailed(String str) {

    }

    @Override
    public void onSyncFinished() {

    }
    private void onFormsCursorLoaded(Cursor cursor) {
        logDebugEvent("onFormsCursorLoaded()");
        View empty = findViewById(R.id.empty);
        if (cursor == null || cursor.getCount() <= 0) {
            this.mToolbar.setLogo(null);
            setTitle(R.string.GIS);
         //   this.mFormDrawer.setDrawerLockMode(VIEW_MODE_MAP);
            if (empty != null) {
                empty.setVisibility(View.VISIBLE);
            }
            removeCollectionView();
            this.mMenuItemVisibility = false;
            Account account = Account.getActiveAccount();
            if (account == null || !account.getRole().isOwner()) {
                this.mToolbar.setNavigationIcon(null);
            } else {
                this.mToolbar.setNavigationIcon((int) R.drawable.ic_action_menu);
//                this.mToolbar.setNavigationOnClickListener ( new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        DashboardActivity.this.mFormDrawer.openDrawer((int) GravityCompat.START);
//                    }
//                });
//                if (!(this.mSyncService == null || this.mSyncService.isSynchronizing())) {
//                    this.mFormDrawer.openDrawer((int) GravityCompat.START);
//                    AppGalleryActivity.startForResult(this);
//                }
            }
        } else {
            boolean found = false;
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                if (CursorUtils.getLong(cursor, "_id") == this.mActiveFormID) {
                    found = true;
                    break;
                }
            }
            if (found) {
                updateFormButton(CursorUtils.getString(cursor, "name"));
            } else {
//                this.mFormDrawer.setDrawerLockMode(VIEW_MODE_LIST);
//                this.mFormDrawer.openDrawer((int) GravityCompat.START);
            }
            if (empty != null) {
                empty.setVisibility(View.INVISIBLE);
            }
            setViewMode(this.mActiveViewMode);
            this.mMenuItemVisibility = true;
        }
        reloadMenuItemVisibility(this.mMenuItemVisibility);
    }

    private void reloadMenuItemVisibility(boolean menuItemVisibility) {
        if (this.mViewToggleItem != null) {
            this.mViewToggleItem.setVisible(menuItemVisibility);
            this.mViewToggleItem.setEnabled(menuItemVisibility);
            if (this.mActiveViewMode == VIEW_MODE_MAP) {
                this.mViewToggleItem.setIcon(R.drawable.ic_action_list);
                this.mViewToggleItem.setTitle(R.string.list_view);
            } else {
                this.mViewToggleItem.setIcon(R.drawable.ic_action_map);
                this.mViewToggleItem.setTitle(R.string.map_view);
            }
        }
//        if (this.mSearchRecordsItem != null) {
//            this.mSearchRecordsItem.setVisible(menuItemVisibility);
//            this.mSearchRecordsItem.setEnabled(menuItemVisibility);
//        }
    }

    private void removeCollectionView() {
        FragmentManager fm = getSupportFragmentManager();
        Fragment frag = fm.findFragmentById(R.id.container);
        if (frag != null) {
            FragmentTransaction ft = fm.beginTransaction();
            ft.remove(frag);
            ft.commitAllowingStateLoss();
        }
        this.mCollectionView = null;
    }
    private void updateFormButton(String appName) {
        setTitle(appName);
        this.mToolbar.setNavigationIcon((int) R.drawable.ic_action_menu);
        this.mToolbar.setNavigationContentDescription((int) R.string.assigned_form);
//        this.mToolbar.setNavigationOnClickListener ( new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                DashboardActivity.this.mFormDrawer.openDrawer((int) GravityCompat.START);
//
//            }
//        } );
    }


}
