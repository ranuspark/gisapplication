package com.example.bhumicloud.gisapplications.Activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.util.GISLogger;

public abstract class GISActivity extends AppCompatActivity {
    private boolean mDestroyed = false;

    public interface OnBackPressedListener {
        boolean onBackPressed();
    }

    protected void onCreate(Bundle savedInstanceState) {
        logDebugEvent("onCreate()");
        super.onCreate(savedInstanceState);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        logDebugEvent("onCreateOptionsMenu()");
        return super.onCreateOptionsMenu(menu);
    }

    protected void onStart() {
        logDebugEvent("onStart()");
        super.onStart();
    }

    protected void onRestart() {
        logDebugEvent("onRestart()");
        super.onRestart();
    }

    protected void onResume() {
        logDebugEvent("onResume()");
        super.onResume();
    }

    protected void onPause() {
        logDebugEvent("onPause()");
        super.onPause();
    }

    protected void onStop() {
        logDebugEvent("onStop()");
        super.onStop();
    }

    protected void onDestroy() {
        logDebugEvent("onDestroy()");
        super.onDestroy();
        this.mDestroyed = true;
    }

    public void onLowMemory() {
        logDebugEvent("onLowMemory()");
        super.onLowMemory();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        logDebugEvent("onConfigurationChanged()");
        super.onConfigurationChanged(newConfig);
    }

    public void onBackPressed() {
        logDebugEvent("onBackPressed()");
        super.onBackPressed();
    }

    protected void onSaveInstanceState(Bundle outState) {
        logDebugEvent("onSaveInstanceState()");
        super.onSaveInstanceState(outState);
    }

    protected void onActivityResult(int arg0, int arg1, Intent arg2) {
        logDebugEvent("onActivityResult()");
        super.onActivityResult(arg0, arg1, arg2);
    }

    public void onAttachedToWindow() {
        logDebugEvent("onAttachedToWindow()");
        super.onAttachedToWindow();
    }

    public void onDetachedFromWindow() {
        logDebugEvent("onDetachedFromWindow()");
        super.onDetachedFromWindow();
    }

    public void recreate() {
        logDebugEvent("recreate()");
        super.recreate();
    }

    public void onTrimMemory(int level) {
        logDebugEvent("onTrimMemory()");
        super.onTrimMemory(level);
    }

    public boolean isDestroyed() {
        return this.mDestroyed;
    }

    protected void logDebugEvent(String event) {
        GISLogger.log(getClass(), event);
    }

    protected Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.container);
    }
}