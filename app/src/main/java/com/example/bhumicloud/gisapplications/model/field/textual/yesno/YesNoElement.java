package com.example.bhumicloud.gisapplications.model.field.textual.yesno;

import android.os.Parcel;
import android.os.Parcelable.Creator;

import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.choice.ChoiceItem;
import com.example.bhumicloud.gisapplications.model.field.textual.TextualElement;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.HashMap;
import java.util.Map;

public class YesNoElement extends TextualElement {
    public static final Creator<YesNoElement> CREATOR = new C12071();
    private static final String JSON_NEGATIVE_CHOICE = "negative";
    private static final String JSON_NEUTRAL_CHOICE = "neutral";
    private static final String JSON_NEUTRAL_ENABLED = "neutral_enabled";
    private static final String JSON_POSITIVE_CHOICE = "positive";
    private ChoiceItem mNegativeChoice;
    private ChoiceItem mNeutralChoice;
    private boolean mNeutralEnabled;
    private ChoiceItem mPositiveChoice;

    static class C12071 implements Creator<YesNoElement> {
        C12071() {
        }

        public YesNoElement createFromParcel(Parcel source) {
            return new YesNoElement(source);
        }

        public YesNoElement[] newArray(int size) {
            return new YesNoElement[size];
        }
    }

    public YesNoElement(Element parent, Map json) {
        super(parent, json);
    }

    private YesNoElement(Parcel parcel) {
        super(parcel);
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = super.toJSON();
        if (this.mPositiveChoice != null) {
            json.put(JSON_POSITIVE_CHOICE, this.mPositiveChoice.toJSON());
        }
        if (this.mNegativeChoice != null) {
            json.put(JSON_NEGATIVE_CHOICE, this.mNegativeChoice.toJSON());
        }
        if (this.mNeutralChoice != null) {
            json.put(JSON_NEUTRAL_CHOICE, this.mNeutralChoice.toJSON());
        }
        json.put(JSON_NEUTRAL_ENABLED, Boolean.valueOf(this.mNeutralEnabled));
        return json;
    }

    public ChoiceItem getPositiveChoice() {
        return this.mPositiveChoice;
    }

    public ChoiceItem getNegativeChoice() {
        return this.mNegativeChoice;
    }

    public ChoiceItem getNeutralChoice() {
        return this.mNeutralChoice;
    }

    public boolean isNeutralChoiceEnabled() {
        return this.mNeutralEnabled;
    }

    public String getType() {
        return Element.TYPE_YES_NO;
    }

    public void setJSONAttributes(Map json) {
        super.setJSONAttributes(json);
        HashMap<String, Object> positive = JSONUtils.getHashMap(json, JSON_POSITIVE_CHOICE);
        if (positive != null) {
            this.mPositiveChoice = new ChoiceItem(positive);
        }
        HashMap<String, Object> negative = JSONUtils.getHashMap(json, JSON_NEGATIVE_CHOICE);
        if (negative != null) {
            this.mNegativeChoice = new ChoiceItem(negative);
        }
        HashMap<String, Object> notApplicable = JSONUtils.getHashMap(json, JSON_NEUTRAL_CHOICE);
        if (notApplicable != null) {
            this.mNeutralChoice = new ChoiceItem(notApplicable);
        }
        this.mNeutralEnabled = JSONUtils.getBoolean(json, JSON_NEUTRAL_ENABLED);
    }
}
