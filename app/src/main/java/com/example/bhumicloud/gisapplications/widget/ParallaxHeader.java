package com.example.bhumicloud.gisapplications.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class ParallaxHeader extends LinearLayout {
    private int mOffset;

    public ParallaxHeader(Context context) {
        super(context);
    }

    public ParallaxHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ParallaxHeader(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected void dispatchDraw(Canvas canvas) {
        canvas.clipRect(new Rect (getLeft(), getTop(), getRight(), getBottom() + this.mOffset));
        super.dispatchDraw(canvas);
    }

    public void setClipY(int offset) {
        this.mOffset = offset;
        invalidate();
    }
}
