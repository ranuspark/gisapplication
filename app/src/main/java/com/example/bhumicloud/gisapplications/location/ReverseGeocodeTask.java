package com.example.bhumicloud.gisapplications.location;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;

import java.io.IOException;
import java.util.List;

public class ReverseGeocodeTask extends AsyncTask<Void, Void, Address> {
    private Callback mCallback;
    private final Geocoder mGeocoder;
    private final Location mLocation;

    public interface Callback {
        void onReverseGeocodeFinished(ReverseGeocodeTask reverseGeocodeTask, Address address);
    }

    public ReverseGeocodeTask(Context context, Location location, Callback callback) {
        this.mGeocoder = new Geocoder (context.getApplicationContext());
        this.mLocation = location;
        this.mCallback = callback;
    }

    public void setCallback(Callback callback) {
        this.mCallback = callback;
    }

    protected Address doInBackground(Void... params) {
        if (this.mLocation != null) {
            try {
                List<Address> results = this.mGeocoder.getFromLocation(this.mLocation.getLatitude(), this.mLocation.getLongitude(), 1);
                if (!(results == null || results.isEmpty())) {
                    return (Address) results.get(0);
                }
            } catch (IOException e) {
                return null;
            }
        }
        return null;
    }

    protected void onPostExecute(Address result) {
        super.onPostExecute(result);
        if (this.mCallback != null) {
            this.mCallback.onReverseGeocodeFinished(this, result);
        }
    }
}
