package com.example.bhumicloud.gisapplications.model.field.repeatable;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.ElementContainer;
import com.example.bhumicloud.gisapplications.model.field.ElementFactory;
import com.example.bhumicloud.gisapplications.util.ElementUtils;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RepeatableElement extends Element implements ElementContainer {
    public static final Creator<RepeatableElement> CREATOR = new C11971();
    private static final String JSON_ELEMENTS = "elements";
    private static final String JSON_GEOMETRY_REQUIRED = "geometry_required";
    private static final String JSON_GEOMETRY_TYPES = "geometry_types";
    private static final String JSON_TITLE_FIELD_COMPOSITE = "title_field_keys";
    private HashMap<String, Element> mElementIndex;
    private ArrayList<Element> mElements;
    private boolean mGeometryRequired;
    private Set<String> mGeometryTypes;
    private ArrayList<String> mTitleElementKeys;

    static class C11971 implements Creator<RepeatableElement> {
        C11971() {
        }

        public RepeatableElement createFromParcel(Parcel source) {
            return new RepeatableElement(source);
        }

        public RepeatableElement[] newArray(int size) {
            return new RepeatableElement[size];
        }
    }

    public RepeatableElement(Element parent, Map json) {
        super(parent, json);
    }

    private RepeatableElement(Parcel parcel) {
        super(parcel);
    }

    public ArrayList<Element> getElements() {
        if (this.mElements == null) {
            this.mElements = new ArrayList();
        }
        return this.mElements;
    }

    public Element getElement(String key) {
        return (Element) getElementIndex().get(key);
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = super.toJSON();
        ArrayList<String> titleElementKeys = new ArrayList();
        Iterator it = this.mTitleElementKeys.iterator();
        while (it.hasNext()) {
            titleElementKeys.add((String) it.next());
        }
        json.put(JSON_TITLE_FIELD_COMPOSITE, titleElementKeys);
        json.put("geometry_required", Boolean.valueOf(this.mGeometryRequired));
        ArrayList<String> geometryTypes = new ArrayList();
        for (String type : getGeometryTypes()) {
            geometryTypes.add(type);
        }
        json.put("geometry_types", geometryTypes);
        ArrayList<HashMap<String, Object>> elements = new ArrayList();
        it = getElements().iterator();
        while (it.hasNext()) {
            elements.add(((Element) it.next()).toJSON());
        }
        json.put("elements", elements);
        return json;
    }

    public String getType() {
        return Element.TYPE_REPEATABLE;
    }

    public Element[] getTitleElements() {
        Element[] elements = new Element[this.mTitleElementKeys.size()];
        int i = 0;
        Iterator it = this.mTitleElementKeys.iterator();
        while (it.hasNext()) {
            elements[i] = getElement((String) it.next());
            i++;
        }
        return elements;
    }

    public boolean isGeometryEnabled() {
        return !getGeometryTypes().isEmpty();
    }

    public boolean isGeometryRequired() {
        return this.mGeometryRequired;
    }

    public Set<String> getGeometryTypes() {
        if (this.mGeometryTypes == null) {
            this.mGeometryTypes = new HashSet();
        }
        return this.mGeometryTypes;
    }

    public void setJSONAttributes(Map json) {
        int i;
        super.setJSONAttributes(json);
        this.mElements = null;
        this.mElementIndex = null;
        List elementsJSON = JSONUtils.getArrayList(json, "elements");
        if (!(elementsJSON == null || elementsJSON.isEmpty())) {
            for (i = 0; i < elementsJSON.size(); i++) {
                HashMap<String, Object> elementJSON = JSONUtils.getHashMap(elementsJSON, i);
                if (!(elementJSON == null || elementJSON.isEmpty())) {
                    Element element = ElementFactory.getInstance(this, elementJSON);
                    if (element != null) {
                        getElements().add(element);
                    }
                }
            }
        }
        this.mTitleElementKeys = processJSONForTitles(json);
        this.mGeometryTypes = null;
        List geometryTypes = JSONUtils.getArrayList(json, "geometry_types");
        if (!(geometryTypes == null || geometryTypes.isEmpty())) {
            for (i = 0; i < geometryTypes.size(); i++) {
                String type = JSONUtils.getString(geometryTypes, i);
                if (!TextUtils.isEmpty(type)) {
                    getGeometryTypes().add(type);
                }
            }
        }
        this.mGeometryRequired = JSONUtils.getBoolean(json, "geometry_required", true);
    }

    private Map<String, Element> getElementIndex() {
        if (this.mElementIndex != null) {
            return this.mElementIndex;
        }
        this.mElementIndex = ElementUtils.flattenElements(getElements());
        return this.mElementIndex;
    }

    private ArrayList<String> processJSONForTitles(Map json) {
        List newTitlesJsonArray = JSONUtils.getArrayList(json, JSON_TITLE_FIELD_COMPOSITE);
        ArrayList<String> titles = new ArrayList();
        String value;
        if (newTitlesJsonArray == null || newTitlesJsonArray.size() <= 0) {
            value = JSONUtils.getString(json, "title_field_key");
            if (!TextUtils.isEmpty(value)) {
                titles.add(value);
            }
        } else {
            for (int i = 0; i < newTitlesJsonArray.size(); i++) {
                value = JSONUtils.getString(newTitlesJsonArray, i);
                if (!TextUtils.isEmpty(value)) {
                    titles.add(value);
                }
            }
        }
        return titles;
    }
}
