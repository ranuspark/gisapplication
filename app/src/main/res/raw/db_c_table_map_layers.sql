CREATE TABLE IF NOT EXISTS Maps (
    _id INTEGER PRIMARY KEY AUTOINCREMENT,
    remote_id TEXT,
    account_id INTEGER,
    display_order INTEGER,
    name TEXT,
    description TEXT,
    filesize REAL,
    remote_url TEXT,
    local_url TEXT,
    type TEXT
);
CREATE INDEX IF NOT EXISTS layers_account_id_idx ON Maps (account_id);
CREATE INDEX IF NOT EXISTS layers_account_id_remote_id_idx ON Maps (account_id, remote_id);