package com.example.bhumicloud.gisapplications.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.dossier.PersistentObject;
import com.example.bhumicloud.gisapplications.dossier.PersistentObject.QueryField;
import com.example.bhumicloud.gisapplications.geoJson.GeoJSON;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.ElementContainer;
import com.example.bhumicloud.gisapplications.model.field.ElementFactory;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkElement;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.example.bhumicloud.gisapplications.util.CursorUtils;
import com.example.bhumicloud.gisapplications.util.ElementUtils;
import com.example.bhumicloud.gisapplications.util.FileUtils;
import com.example.bhumicloud.gisapplications.util.FileUtils.GISDirectoryType;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class Form extends PersistentObject implements ElementContainer {
    @QueryField("account_id")
    public static final String COLUMN_ACCOUNT_ID = "account_id";
    @QueryField("assignment_enabled")
    public static final String COLUMN_ASSIGNMENT_ENABLED = "assignment_enabled";
    @QueryField("deleted_at")
    public static final String COLUMN_DELETED_AT = "deleted_at";
    @QueryField("description")
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_ELEMENTS = "elements";
    @QueryField("geometry_required")
    public static final String COLUMN_GEOMETRY_REQUIRED = "geometry_required";
    @QueryField("geometry_types")
    public static final String COLUMN_GEOMETRY_TYPES = "geometry_types";
    @QueryField("hidden_on_dashboard")
    public static final String COLUMN_HIDDEN_ON_DASHBOARD = "hidden_on_dashboard";
    @QueryField("local_thumbnail_path")
    public static final String COLUMN_LOCAL_THUMBNAIL_PATH = "local_thumbnail_path";
    @QueryField("name")
    public static final String COLUMN_NAME = "name";
    @QueryField("projects_enabled")
    public static final String COLUMN_PROJECTS_ENABLED = "projects_enabled";
    @QueryField("remote_record_count")
    public static final String COLUMN_RECORD_COUNT = "remote_record_count";
    @QueryField("remote_id")
    public static final String COLUMN_REMOTE_ID = "remote_id";
    @QueryField("remote_thumbnail_path")
    public static final String COLUMN_REMOTE_THUMBNAIL_PATH = "remote_thumbnail_path";
    @QueryField("report_templates")
    public static final String COLUMN_REPORT_TEMPLATES = "report_templates";
    public static final String COLUMN_SCRIPT = "script";
    @QueryField("status_field")
    public static final String COLUMN_STATUS_FIELD = "status_field";
    @QueryField("sticky_defaults")
    public static final String COLUMN_STICKY_DEFAULTS = "sticky_defaults";
    @QueryField("title_key")
    public static final String COLUMN_TITLE_KEY = "title_key";
    public static final Creator<Form> CREATOR = new Creator<Form>() {
        public Form createFromParcel(Parcel source) {
            return new Form(source);
        }

        public Form[] newArray(int size) {
            return new Form[size];
        }
    };
    public static final ArrayList<String> QUERY_COLUMNS = PersistentObject.buildColumns(Form.class);
    public static final String TABLE_NAME = "Forms";
    private static final String TAG = Form.class.getSimpleName();
    private static Form form;
    private Account mAccount;
    private long mAccountID;
    private boolean mAssignmentEnabled;
    private Date mDeletedAt;
    private String mDescription;
    private Map<String, Element> mElementByDataNameIndex;
    private Map<String, Element> mElementIndex;
    private ArrayList<Element> mElements;
    private ArrayList<Object> mElementsJSON;
    private boolean mGeometryRequired;
    private ArrayList<String> mGeometryTypes;
    private boolean mHiddenOnDashboard;
    private String mLocalPath;
    private String mName;
    private Boolean mOverrideAutoLocationEnabled;
    private Double mOverrideAutoLocationMinimumAccuracy;
    private Boolean mOverrideAutoSyncEnabled;
    private Boolean mOverrideDraftsEnabled;
    private Boolean mOverrideEditDurationsEnabled;
    private Boolean mOverrideEditLocationsEnabled;
    private Boolean mOverrideManualLocationEnabled;
    private Boolean mOverrideMediaCaptureEnabled;
    private Boolean mOverrideMediaGalleryEnabled;
    private String mOverridePhotoQuality;
    private String mOverrideVideoQuality;
    private boolean mProjectsEnabled;
    private long mRecordCount;
    private String mRemoteID;
    private String mRemotePath;
    private ArrayList<Object> mReportTemplatesJSON;
    private String mScript;
    private StatusElement mStatusElement;
    private HashMap<String, Object> mStatusJSON;
    private final HashMap<String, Object> mStickyDefaults;
    private ArrayList<String> mTitleKeys;

    public static Form getForm(long id) {
        return getForm(id, true);
    }

    public static Form getForm(long id, boolean requireActive) {
        SQLiteDatabase db = GIS.getDatabase();
        ArrayList<String> predicates = new ArrayList();
        ArrayList<String> valuesList = new ArrayList();
        predicates.add("_id = ?");
        valuesList.add(String.valueOf(id));
        if (requireActive) {
            predicates.add("deleted_at IS NULL");
        }
        String predicate = TextUtils.join(" AND ", predicates);
        String[] values = (String[]) valuesList.toArray(new String[valuesList.size()]);
        Cursor c = CursorUtils.queryTableWithLargeColumns(db, TABLE_NAME, QUERY_COLUMNS, Arrays.asList(new String[]{COLUMN_ELEMENTS, COLUMN_SCRIPT}), predicate, values, null);
        Form form = null;
        c.moveToFirst();
        if (!c.isAfterLast()) {
            form = new Form(c, db);
        }
        c.close();
        return form;
    }

    public static Form getForm(Account account, String remoteID) {
        return getForm(account, remoteID, true);
    }

    public static Form getForm(Account account, String remoteID, boolean requireActive) {
        if (account == null || TextUtils.isEmpty(remoteID)) {
            return null;
        }
        SQLiteDatabase db = GIS.getDatabase();
        ArrayList<String> predicates = new ArrayList();
        ArrayList<String> valuesList = new ArrayList();
        predicates.add("account_id = ?");
        valuesList.add(String.valueOf(account.getRowID()));
        predicates.add("remote_id = ?");
        valuesList.add(remoteID);
        if (requireActive) {
            predicates.add("deleted_at IS NULL");
        }
        String predicate = TextUtils.join(" AND ", predicates);
        String[] values = (String[]) valuesList.toArray(new String[valuesList.size()]);
        Cursor c = CursorUtils.queryTableWithLargeColumns(db, TABLE_NAME, QUERY_COLUMNS, Arrays.asList(new String[]{COLUMN_ELEMENTS, COLUMN_SCRIPT}), predicate, values, null);
        Form form = null;
        c.moveToFirst();
        if (!c.isAfterLast()) {
            form = new Form(c, db);
        }
        c.close();
        return form;
    }

    public static ArrayList<Form> getForms(Account account) {
        return getForms(account.getRowID(), GIS.getDatabase(), true);
    }

    public static ArrayList<Form> getForms(long accountRowId, SQLiteDatabase db, boolean requireActive) {
        ArrayList<Form> forms = new ArrayList();
        ArrayList<String> predicates = new ArrayList();
        ArrayList<String> valuesList = new ArrayList();
        if (accountRowId > 0) {
            predicates.add("account_id = ?");
            valuesList.add(String.valueOf(accountRowId));
        }
        if (requireActive) {
            predicates.add("deleted_at IS NULL");
        }
        String predicate = TextUtils.join(" AND ", predicates);
        String[] values = (String[]) valuesList.toArray(new String[valuesList.size()]);
        SQLiteDatabase sQLiteDatabase = db;
        Cursor c = CursorUtils.queryTableWithLargeColumns(sQLiteDatabase, TABLE_NAME, QUERY_COLUMNS, Arrays.asList(new String[]{COLUMN_ELEMENTS, COLUMN_SCRIPT}), predicate, values, "name ASC");
        c.moveToFirst();
        while (!c.isAfterLast()) {
            forms.add(new Form(c, db));
            c.moveToNext();
        }
        c.close();
        return forms;
    }

    public static Cursor getFormsCursor(Account account, SQLiteDatabase db, String[] columns, String searchText) {
        ArrayList<String> predicates = new ArrayList();
        ArrayList<String> paramsList = new ArrayList();
        if (account == null) {
            return null;
        }
        predicates.add("account_id = ?");
        paramsList.add(String.valueOf(account.getRowID()));
        if (!TextUtils.isEmpty(searchText)) {
            predicates.add("name LIKE ?");
            paramsList.add("%" + searchText + "%");
        }
        predicates.add("hidden_on_dashboard = 0");
        predicates.add("deleted_at IS NULL");
        return db.query(TABLE_NAME, columns, TextUtils.join(" AND ", predicates), (String[]) paramsList.toArray(new String[paramsList.size()]), null, null, "name ASC");
    }

    public static ArrayList<Form> getFormsSortedByDependencies(Account account) {
        return getFormsSortedByDependenciesRecursive(getForms(account), null, null);
    }

    public static ArrayList<Form> getFormsSortedByDependenciesRecursive(ArrayList<Form> input, ArrayList<Form> output, HashSet<String> outputFormIDs) {
        Form linkedForm;
        ArrayList<Form> uniqueLinkedForms = new ArrayList();
        HashSet<String> uniqueLinkedFormIDs = new HashSet();
        HashMap<String, Form> formsByID = new HashMap();
        Iterator it = input.iterator();
        while (it.hasNext()) {
            Form form = (Form) it.next();
            formsByID.put(form.getRemoteID(), form);
        }
        if (outputFormIDs == null) {
            outputFormIDs = new HashSet();
        }
        if (output == null) {
            output = new ArrayList();
        }
        it = input.iterator();
        while (it.hasNext()) {
            Iterator it2 = ((Form) it.next()).getElementsOfType(Element.TYPE_RECORD_LINK).iterator();
            while (it2.hasNext()) {
                RecordLinkElement recordLinkElement = (RecordLinkElement) ((Element) it2.next());
                if (!uniqueLinkedFormIDs.contains(recordLinkElement.getFormID())) {
                    linkedForm = (Form) formsByID.get(recordLinkElement.getFormID());
                    if (linkedForm != null) {
                        uniqueLinkedForms.add(linkedForm);
                        uniqueLinkedFormIDs.add(linkedForm.getRemoteID());
                    }
                }
            }
        }
        it = uniqueLinkedForms.iterator();
        while (it.hasNext()) {
            linkedForm = (Form) it.next();
            if (!outputFormIDs.contains(linkedForm.getRemoteID())) {
                output.add(0, linkedForm);
                outputFormIDs.add(linkedForm.getRemoteID());
                getFormsSortedByDependenciesRecursive(uniqueLinkedForms, output, outputFormIDs);
            }
        }
        it = input.iterator();
        while (it.hasNext()) {
            form = (Form) it.next();
            if (!outputFormIDs.contains(form.getRemoteID())) {
                output.add(form);
                outputFormIDs.add(form.getRemoteID());
            }
        }
        return output;
    }

    public static void truncate(ArrayList<String> excludedRemoteIDs, Account account) {
        SQLiteDatabase db = GIS.getDatabase();
        ArrayList<String> predicates = new ArrayList();
        ArrayList<String> paramsList = new ArrayList();
        if (account != null) {
            predicates.add("account_id = ?");
            paramsList.add(String.valueOf(account.getRowID()));
        }
        if (excludedRemoteIDs != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("remote_id NOT IN (");
            for (int i = 0; i < excludedRemoteIDs.size(); i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append("'").append((String) excludedRemoteIDs.get(i)).append("'");
            }
            sb.append(")");
            predicates.add(sb.toString());
        }
        String predicate = TextUtils.join(" AND ", predicates);
        String[] subArgs = (String[]) paramsList.toArray(new String[paramsList.size()]);
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_DELETED_AT, Long.valueOf(System.currentTimeMillis()));
        db.update(TABLE_NAME, contentValues, predicate, subArgs);
    }

    public static void resetStickyDefaults(Account account) {
        SQLiteDatabase db = GIS.getDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.putNull(COLUMN_STICKY_DEFAULTS);
        db.update(TABLE_NAME, contentValues, "account_id = ?", new String[]{String.valueOf(account.getRowID())});
    }

    public Form(Map<String, Object> jsonRepresentation) {
        this.mGeometryTypes = new ArrayList();
        this.mStickyDefaults = new HashMap();
        setAttributesFromJSON(jsonRepresentation);
    }

    public Form(Cursor cursor) {
        super(cursor);
        this.mGeometryTypes = new ArrayList();
        this.mStickyDefaults = new HashMap();
        this.mRemoteID = CursorUtils.getString(cursor, COLUMN_REMOTE_ID);
        this.mName = CursorUtils.getString(cursor, COLUMN_NAME);
        this.mLocalPath = CursorUtils.getString(cursor, COLUMN_LOCAL_THUMBNAIL_PATH);
    }

    public Form(Cursor cursor, SQLiteDatabase db) {
        super(cursor);
        this.mGeometryTypes = new ArrayList();
        this.mStickyDefaults = new HashMap();
        try {
            this.mRemoteID = CursorUtils.getString(cursor, COLUMN_REMOTE_ID);
            this.mAccountID = CursorUtils.getLong(cursor, COLUMN_ACCOUNT_ID);
            this.mName = CursorUtils.getString(cursor, COLUMN_NAME);
            this.mDescription = CursorUtils.getString(cursor, COLUMN_DESCRIPTION);
            this.mElementsJSON = JSONUtils.arrayFromJSON(CursorUtils.fetchLargeColumn(db, TABLE_NAME, cursor, COLUMN_ELEMENTS));
            this.mElements = buildElements(this.mElementsJSON);
            this.mRecordCount = (long) CursorUtils.getInt(cursor, COLUMN_RECORD_COUNT);
            this.mTitleKeys = processCursorForTitles(CursorUtils.getString(cursor, COLUMN_TITLE_KEY));
            this.mRemotePath = CursorUtils.getString(cursor, COLUMN_REMOTE_THUMBNAIL_PATH);
            this.mLocalPath = CursorUtils.getString(cursor, COLUMN_LOCAL_THUMBNAIL_PATH);
            this.mHiddenOnDashboard = CursorUtils.getBoolean(cursor, COLUMN_HIDDEN_ON_DASHBOARD);
            List typesJSON = JSONUtils.arrayFromJSON(CursorUtils.getString(cursor, COLUMN_GEOMETRY_TYPES));
            if (typesJSON != null) {
                for (int i = 0; i < typesJSON.size(); i++) {
                    this.mGeometryTypes.add(JSONUtils.getString(typesJSON, i));
                }
            }
            this.mGeometryRequired = CursorUtils.getBoolean(cursor, COLUMN_GEOMETRY_REQUIRED);
            this.mReportTemplatesJSON = JSONUtils.arrayFromJSON(CursorUtils.getString(cursor, COLUMN_REPORT_TEMPLATES));
            this.mStatusJSON = JSONUtils.objectFromJSON(CursorUtils.getString(cursor, COLUMN_STATUS_FIELD));
            this.mStatusElement = buildStatusElement(this.mStatusJSON);
            HashMap<String, Object> stickyDefaults = JSONUtils.objectFromJSON(CursorUtils.getString(cursor, COLUMN_STICKY_DEFAULTS));
            if (stickyDefaults != null) {
                this.mStickyDefaults.putAll(stickyDefaults);
            }
            this.mScript = CursorUtils.fetchLargeColumn(db, TABLE_NAME, cursor, COLUMN_SCRIPT);
            this.mProjectsEnabled = CursorUtils.getBoolean(cursor, COLUMN_PROJECTS_ENABLED);
            this.mAssignmentEnabled = CursorUtils.getBoolean(cursor, COLUMN_ASSIGNMENT_ENABLED);
            int idxDeletedAt = cursor.getColumnIndex(COLUMN_DELETED_AT);
            if (!cursor.isNull(idxDeletedAt)) {
                this.mDeletedAt = new Date(cursor.getLong(idxDeletedAt));
            }
        } catch (Throwable e) {
            GISLogger.log(e);
        }
    }

    private Form(Parcel source) {
        super(source);
        this.mGeometryTypes = new ArrayList();
        this.mStickyDefaults = new HashMap();
        try {
            setAttributesFromJSON(JSONUtils.objectFromJSON(source.readString()));
        } catch (Throwable e) {
            GISLogger.log(e);
        }
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(JSONUtils.toJSONString(toJSON()));
    }

    public ArrayList<Element> getElements() {
        return this.mElements;
    }

    public Element getElement(String key) {
        return (Element) getElementIndex().get(key);
    }

    public Element getElementByDataName(String dataName) {
        return (Element) getElementByDataNameIndex().get(dataName);
    }

    public File generateStorageLocation(Context context, String imageName) {
        return new File(FileUtils.getGisDataDir (context, GISDirectoryType.thumbnails), imageName);
    }

    public String getRemoteID() {
        return this.mRemoteID;
    }

    public long getAccountID() {
        return this.mAccountID;
    }

    public Account getAccount() {
        if (this.mAccount != null) {
            return this.mAccount;
        }
        if (this.mAccountID != 0) {
            this.mAccount = Account.getAccount(this.mAccountID);
        }
        return this.mAccount;
    }

    public String getName() {
        return this.mName;
    }

    public String getDescription() {
        return this.mDescription;
    }

    public ArrayList<String> getTitleKeys() {
        return this.mTitleKeys;
    }

    public Element[] getTitleElements() {
        Element[] elements = new Element[this.mTitleKeys.size()];
        int i = 0;
        Iterator it = this.mTitleKeys.iterator();
        while (it.hasNext()) {
            elements[i] = getElement((String) it.next());
            i++;
        }
        return elements;
    }

    public long getRecordCount() {
        return this.mRecordCount;
    }

    public boolean isGeometryEnabled() {
        return !getGeometryTypes().isEmpty();
    }

    public boolean isGeometryRequired() {
        return this.mGeometryRequired;
    }

    public URL getRemoteThumbnailPath() {
        if (!TextUtils.isEmpty(this.mRemotePath)) {
            try {
                return new URL(this.mRemotePath);
            } catch (Throwable e) {
                GISLogger.log(e);
            }
        }
        return null;
    }

    public void setLocalThumbnailPath(String localPath) {
        this.mLocalPath = localPath;
    }

    public String getLocalThumbnailPath() {
        return this.mLocalPath;
    }

    public void deleteLocalThumbnail() {
        String path = getLocalThumbnailPath();
        try {
            File file = new File(path);
            if (file.exists()) {
                GISLogger.log(TAG, "deleting " + path);
                file.delete();
            }
        } catch (Throwable e) {
            GISLogger.log(e);
        }
        setLocalThumbnailPath(null);
    }

    public ArrayList<String> getGeometryTypes() {
        return this.mGeometryTypes;
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = new HashMap();
        json.put("id", getRemoteID());
        json.put("account_id_local", Long.valueOf(getAccountID()));
        json.put(COLUMN_NAME, getName());
        json.put(COLUMN_DESCRIPTION, getDescription());
        json.put(COLUMN_ELEMENTS, this.mElementsJSON);
        json.put("record_count_local", Long.valueOf(getRecordCount()));
        json.put("title_field_keys", getTitleKeys());
        json.put("image_thumbnail", this.mRemotePath);
        json.put("image_thumbnail_local", getLocalThumbnailPath());
        json.put(COLUMN_HIDDEN_ON_DASHBOARD, Boolean.valueOf(isHiddenOnDashboard()));
        json.put(COLUMN_GEOMETRY_TYPES, getGeometryTypes());
        json.put(COLUMN_GEOMETRY_REQUIRED, Boolean.valueOf(isGeometryRequired()));
        json.put(COLUMN_REPORT_TEMPLATES, this.mReportTemplatesJSON);
        json.put(COLUMN_STATUS_FIELD, this.mStatusJSON);
        json.put("sticky_defaults_local", getStickyDefaults());
        json.put(COLUMN_SCRIPT, getScript());
        json.put("deleted_at_local", Long.valueOf(this.mDeletedAt == null ? -1 : this.mDeletedAt.getTime()));
        return json;
    }

    public StatusElement getStatusElement() {
        return this.mStatusElement;
    }

    public void setAttributesFromJSON(Map<String, Object> json) {
        Date date = null;
        this.mRemoteID = JSONUtils.getString((Map) json, "id");
        if (this.mAccountID == 0) {
            this.mAccountID = JSONUtils.getLong((Map) json, "account_id_local");
        }
        this.mName = JSONUtils.getString((Map) json, COLUMN_NAME);
        this.mDescription = JSONUtils.getString((Map) json, COLUMN_DESCRIPTION);
        this.mElementsJSON = JSONUtils.getArrayList(json, COLUMN_ELEMENTS);
        this.mElements = buildElements(this.mElementsJSON);
        if (this.mRecordCount == 0) {
            this.mRecordCount = JSONUtils.getLong((Map) json, "record_count_local");
        }
        this.mTitleKeys = processJSONForTitles(json);
        this.mRemotePath = JSONUtils.getString((Map) json, "image_thumbnail");
        if (this.mLocalPath == null) {
            this.mLocalPath = JSONUtils.getString((Map) json, "image_thumbnail_local");
        }
        this.mHiddenOnDashboard = JSONUtils.getBoolean(json, COLUMN_HIDDEN_ON_DASHBOARD);
        this.mGeometryTypes.clear();
        List geometryTypes = JSONUtils.getArrayList(json, COLUMN_GEOMETRY_TYPES);
        if (geometryTypes != null) {
            for (int i = 0; i < geometryTypes.size(); i++) {
                String type = JSONUtils.getString(geometryTypes, i);
                if (!TextUtils.isEmpty(type)) {
                    getGeometryTypes().add(type);
                }
            }
        } else {
            getGeometryTypes().add(GeoJSON.TYPE_POINT);
        }
        this.mGeometryRequired = JSONUtils.getBoolean(json, COLUMN_GEOMETRY_REQUIRED, false);
        this.mReportTemplatesJSON = JSONUtils.getArrayList(json, COLUMN_REPORT_TEMPLATES);
        this.mStatusJSON = JSONUtils.getHashMap((Map) json, COLUMN_STATUS_FIELD);
        this.mStatusElement = buildStatusElement(this.mStatusJSON);
        if (this.mStickyDefaults.size() < 1) {
            HashMap<String, Object> stickyDefaults = JSONUtils.getHashMap((Map) json, "sticky_defaults_local");
            if (stickyDefaults != null && stickyDefaults.size() > 0) {
                this.mStickyDefaults.putAll(stickyDefaults);
            }
        }
        this.mScript = JSONUtils.getString((Map) json, COLUMN_SCRIPT);
        this.mProjectsEnabled = JSONUtils.getBoolean(json, COLUMN_PROJECTS_ENABLED, true);
        this.mAssignmentEnabled = JSONUtils.getBoolean(json, COLUMN_ASSIGNMENT_ENABLED, true);
        this.mElementIndex = null;
        this.mElementByDataNameIndex = null;
        if (this.mDeletedAt == null) {
            long deleted_at = JSONUtils.getLong(json, "deleted_at_local", -1);
            if (deleted_at >= 0) {
                date = new Date(deleted_at);
            }
            this.mDeletedAt = date;
        }
    }

    public void setAccount(Account account) {
        if (account != null) {
            this.mAccountID = account.getRowID();
        } else {
            this.mAccountID = 0;
        }
    }

    public Element getParentElement(Element element) {
        if (element == null) {
            return null;
        }
        String parentKey = element.getParentKey();
        if (parentKey != null) {
            return getElement(parentKey);
        }
        return null;
    }

    public boolean elementHasHiddenParent(Element element) {
        Element parent = getParentElement(element);
        if (parent == null || element.isHidden()) {
            return element.isHidden();
        }
        return elementHasHiddenParent(parent);
    }

    public boolean isElementPreserved(Element element) {
        Element parent = getParentElement(element);
        if (parent == null || element.preserveValueWhenConditionallyHidden()) {
            return element.preserveValueWhenConditionallyHidden();
        }
        return isElementPreserved(parent);
    }

    public String toString() {
        return this.mName;
    }

    public boolean isHiddenOnDashboard() {
        return this.mHiddenOnDashboard;
    }

    public void updateStickyDefaults(FormValueContainer formValueContainer, Record record) {
        Iterator it = DefaultValues.flattenStickyElements(formValueContainer.getElements(), this).iterator();
        while (it.hasNext()) {
            Element element = (Element) it.next();
            FormValue value = DefaultValues.getFormValueFromValues(record, element.getKey(), formValueContainer);
            if (value == null || value.isEmpty()) {
                this.mStickyDefaults.remove(element.getKey());
            } else {
                this.mStickyDefaults.put(element.getKey(), value.toJSON());
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_STICKY_DEFAULTS, JSONUtils.toJSONString(this.mStickyDefaults));
        saveColumns(contentValues);
    }

    public HashMap<String, Object> getStickyDefaults() {
        return this.mStickyDefaults;
    }

    public String getScript() {
        return this.mScript;
    }

    public boolean isProjectsEnabled() {
        return this.mProjectsEnabled;
    }

    public void setOverrideAutoSyncEnabled(Boolean override) {
        this.mOverrideAutoSyncEnabled = override;
    }

    public void setOverrideAutoLocationEnabled(Boolean override) {
        this.mOverrideAutoLocationEnabled = override;
    }

    public void setOverrideAutoLocationMinimumAccuracy(Double override) {
        this.mOverrideAutoLocationMinimumAccuracy = override;
    }

    public void setOverrideManualLocationEnabled(Boolean override) {
        this.mOverrideManualLocationEnabled = override;
    }

    public void setOverrideMediaGalleryEnabled(Boolean override) {
        this.mOverrideMediaGalleryEnabled = override;
    }

    public void setOverrideMediaCaptureEnabled(Boolean override) {
        this.mOverrideMediaCaptureEnabled = override;
    }

    public void setOverrideDraftsEnabled(Boolean override) {
        this.mOverrideDraftsEnabled = override;
    }

    public void setOverrideEditLocationsEnabled(Boolean override) {
        this.mOverrideEditLocationsEnabled = override;
    }

    public void setOverrideEditDurationsEnabled(Boolean override) {
        this.mOverrideEditDurationsEnabled = override;
    }

    public void setOverridePhotoQuality(String override) {
        this.mOverridePhotoQuality = override;
    }

    public void setOverrideVideoQuality(String override) {
        this.mOverrideVideoQuality = override;
    }

    public boolean isAutoSyncEnabled(Context context) {
        return this.mOverrideAutoSyncEnabled != null ? this.mOverrideAutoSyncEnabled.booleanValue() : PatronSettings.isAutoSyncEnabled(context);
    }

    public boolean isAutoLocationEnabled() {
        return this.mOverrideAutoLocationEnabled != null ? this.mOverrideAutoLocationEnabled.booleanValue() : true;
    }

    public double getAutoLocationMinimumAccuracy() {
        return this.mOverrideAutoLocationMinimumAccuracy != null ? this.mOverrideAutoLocationMinimumAccuracy.doubleValue() : 1500.0d;
    }

    public Boolean isManualLocationEnabled() {
        return Boolean.valueOf(this.mOverrideManualLocationEnabled != null ? this.mOverrideManualLocationEnabled.booleanValue() : true);
    }

    public Boolean isMediaGalleryEnabled() {
        return Boolean.valueOf(this.mOverrideMediaGalleryEnabled != null ? this.mOverrideMediaGalleryEnabled.booleanValue() : true);
    }

    public Boolean isMediaCaptureEnabled() {
        return Boolean.valueOf(this.mOverrideMediaCaptureEnabled != null ? this.mOverrideMediaCaptureEnabled.booleanValue() : true);
    }

    public Boolean isDraftsEnabled() {
        return Boolean.valueOf(this.mOverrideDraftsEnabled != null ? this.mOverrideDraftsEnabled.booleanValue() : true);
    }

    public Boolean isEditLocationsEnabled() {
        return Boolean.valueOf(this.mOverrideEditLocationsEnabled != null ? this.mOverrideEditLocationsEnabled.booleanValue() : true);
    }

    public Boolean isEditDurationsEnabled() {
        return Boolean.valueOf(this.mOverrideEditDurationsEnabled != null ? this.mOverrideEditDurationsEnabled.booleanValue() : true);
    }

    public String getPhotoQuality() {
        return this.mOverridePhotoQuality;
    }

    public String getVideoQuality() {
        return this.mOverrideVideoQuality;
    }

    public void clearDeletedAt() {
        this.mDeletedAt = null;
    }

    public void updateRecordCount(SQLiteDatabase db) {
        this.mRecordCount = DatabaseUtils.queryNumEntries(db, Record.TABLE_NAME, "form_id = ?", new String[]{String.valueOf(getRowID())});
        ContentValues values = new ContentValues();
        values.put(COLUMN_RECORD_COUNT, Long.valueOf(this.mRecordCount));
        saveColumns(db, values);
    }

    public void incrementRecordCount() {
        this.mRecordCount++;
        ContentValues values = new ContentValues();
        values.put(COLUMN_RECORD_COUNT, Long.valueOf(this.mRecordCount));
        saveColumns(values);
    }

    public void decrementRecordCount() {
        this.mRecordCount = Math.max(0, this.mRecordCount - 1);
        ContentValues values = new ContentValues();
        values.put(COLUMN_RECORD_COUNT, Long.valueOf(this.mRecordCount));
        saveColumns(values);
    }

    protected ContentValues getContentValues() {
        ContentValues values = super.getContentValues();
        values.put(COLUMN_REMOTE_ID, getRemoteID());
        values.put(COLUMN_ACCOUNT_ID, Long.valueOf(getAccountID()));
        values.put(COLUMN_NAME, getName());
        values.put(COLUMN_DESCRIPTION, getDescription());
        if (!(this.mElementsJSON == null || this.mElementsJSON.isEmpty())) {
            String elementsJSONAsString = JSONUtils.toJSONString(this.mElementsJSON);
            if (elementsJSONAsString != null) {
                values.put(COLUMN_ELEMENTS, elementsJSONAsString);
            }
        }
        values.put(COLUMN_RECORD_COUNT, Long.valueOf(getRecordCount()));
        values.put(COLUMN_TITLE_KEY, TextUtils.join(",", getTitleKeys()));
        values.put(COLUMN_REMOTE_THUMBNAIL_PATH, this.mRemotePath);
        values.put(COLUMN_LOCAL_THUMBNAIL_PATH, getLocalThumbnailPath());
        values.put(COLUMN_HIDDEN_ON_DASHBOARD, Boolean.valueOf(isHiddenOnDashboard()));
        ArrayList<String> typesJSON = new ArrayList();
        Iterator it = getGeometryTypes().iterator();
        while (it.hasNext()) {
            typesJSON.add((String) it.next());
        }
        values.put(COLUMN_GEOMETRY_TYPES, JSONUtils.toJSONString(typesJSON));
        values.put(COLUMN_GEOMETRY_REQUIRED, Boolean.valueOf(isGeometryRequired()));
        if (!(this.mReportTemplatesJSON == null || this.mReportTemplatesJSON.isEmpty())) {
            String reportTemplatesAsJSON = JSONUtils.toJSONString(this.mReportTemplatesJSON);
            if (reportTemplatesAsJSON != null) {
                values.put(COLUMN_REPORT_TEMPLATES, reportTemplatesAsJSON);
            }
        }
        if (!(this.mStatusJSON == null || this.mStatusJSON.isEmpty())) {
            String statusAsJSON = JSONUtils.toJSONString(this.mStatusJSON);
            if (statusAsJSON != null) {
                values.put(COLUMN_STATUS_FIELD, statusAsJSON);
            }
        }
        values.put(COLUMN_STICKY_DEFAULTS, JSONUtils.toJSONString(this.mStickyDefaults));
        values.put(COLUMN_SCRIPT, this.mScript);
        values.put(COLUMN_PROJECTS_ENABLED, Boolean.valueOf(this.mProjectsEnabled));
        values.put(COLUMN_ASSIGNMENT_ENABLED, Boolean.valueOf(this.mAssignmentEnabled));
        if (this.mDeletedAt == null) {
            values.putNull(COLUMN_DELETED_AT);
        } else {
            values.put(COLUMN_DELETED_AT, Long.valueOf(this.mDeletedAt.getTime()));
        }
        return values;
    }

    protected String getTableName() {
        return TABLE_NAME;
    }

    private Map<String, Element> getElementIndex() {
        if (this.mElementIndex != null) {
            return this.mElementIndex;
        }
        this.mElementIndex = ElementUtils.flattenElements(getElements());
        return this.mElementIndex;
    }

    private Map<String, Element> getElementByDataNameIndex() {
        if (this.mElementByDataNameIndex != null) {
            return this.mElementByDataNameIndex;
        }
        this.mElementByDataNameIndex = ElementUtils.flattenElementsByDataName(getElements());
        return this.mElementByDataNameIndex;
    }

    private ArrayList<String> processCursorForTitles(String titlesValue) {
        ArrayList<String> titlesValues = new ArrayList();
        if (!TextUtils.isEmpty(titlesValue)) {
            for (String title : TextUtils.split(titlesValue, ",")) {
                if (!TextUtils.isEmpty(title)) {
                    titlesValues.add(title);
                }
            }
        }
        return titlesValues;
    }

    private ArrayList<String> processJSONForTitles(Map json) {
        List newTitlesJsonArray = JSONUtils.getArrayList(json, "title_field_keys");
        ArrayList<String> titles = new ArrayList();
        String value;
        if (newTitlesJsonArray == null || newTitlesJsonArray.isEmpty()) {
            value = JSONUtils.getString(json, "record_title_key");
            if (!TextUtils.isEmpty(value)) {
                titles.add(value);
            }
        } else {
            for (int i = 0; i < newTitlesJsonArray.size(); i++) {
                value = JSONUtils.getString(newTitlesJsonArray, i);
                if (!TextUtils.isEmpty(value)) {
                    titles.add(value);
                }
            }
        }
        return titles;
    }

    private ArrayList<Element> getElementsOfType(String type) {
        ArrayList<Element> elements = new ArrayList();
        for (Element element : getElementIndex().values()) {
            if (type.equals(element.getType())) {
                elements.add(element);
            }
        }
        return elements;
    }

    private ArrayList<Element> buildElements(ArrayList<Object> elementsJSON) {
        ArrayList<Element> elements = new ArrayList();
        if (!(elementsJSON == null || elementsJSON.isEmpty())) {
            Iterator it = elementsJSON.iterator();
            while (it.hasNext()) {
                HashMap<String, Object> elementJSON = JSONUtils.getHashMap(it.next());
                if (elementJSON != null) {
                    Element element = ElementFactory.getInstance(null, elementJSON);
                    if (element != null) {
                        elements.add(element);
                    }
                }
            }
        }
        return elements;
    }

    private StatusElement buildStatusElement(HashMap<String, Object> statusJSON) {
        if (statusJSON == null || statusJSON.isEmpty()) {
            return null;
        }
        return new StatusElement(null, (Map) statusJSON);
    }
}
