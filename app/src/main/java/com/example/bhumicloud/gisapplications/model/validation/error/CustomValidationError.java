package com.example.bhumicloud.gisapplications.model.validation.error;

import com.example.bhumicloud.gisapplications.model.validation.FeatureValidationError;

public class CustomValidationError extends FeatureValidationError {
    public CustomValidationError(String message) {
        this.mMessage = message;
    }
}
