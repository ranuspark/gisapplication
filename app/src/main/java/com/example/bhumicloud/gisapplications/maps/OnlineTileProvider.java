package com.example.bhumicloud.gisapplications.maps;

import com.google.android.gms.maps.model.UrlTileProvider;
import com.example.bhumicloud.gisapplications.model.layer.MapLayer;
import com.example.bhumicloud.gisapplications.util.GISLogger;

import java.net.URL;

public class OnlineTileProvider extends UrlTileProvider {
    private String mURL;

    public OnlineTileProvider(MapLayer layer) {
        super(256, 256);
        String url = layer.getRemoteURL().toString();
        if (url.contains("{x}") && url.contains("{y}") && url.contains("{z}")) {
            this.mURL = layer.getRemoteURL().toString();
        } else {
            layer.setAvailable(false);
        }
    }

    public URL getTileUrl(int x, int y, int z) {
        try {
            return new URL (this.mURL.replace("{x}", String.valueOf(x)).replace("{y}", String.valueOf(y)).replace("{z}", String.valueOf(z)));
        } catch (Throwable e) {
            GISLogger.log(e);
            return null;
        }
    }
}
