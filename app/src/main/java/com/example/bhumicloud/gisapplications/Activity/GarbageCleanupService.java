package com.example.bhumicloud.gisapplications.Activity;


import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.example.bhumicloud.gisapplications.BuildConfig;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.model.TrashCan;
import com.example.bhumicloud.gisapplications.util.CursorUtils;
import com.example.bhumicloud.gisapplications.util.FileBasedStateStorage;
import com.example.bhumicloud.gisapplications.util.FileUtils;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import java.io.File;

public class GarbageCleanupService extends IntentService {
    private static final String DEBUG_TAG = GarbageCleanupService.class.getSimpleName();

    public static void start(Context context) {
        context.startService(new Intent(context, GarbageCleanupService.class));
    }

    public GarbageCleanupService() {
        super(GarbageCleanupService.class.getName());
    }

    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            deleteFromTrashCan();
            deleteCachedStateFiles();
        }
    }

    private void deleteFromTrashCan() {
        GISLogger.log(DEBUG_TAG, "Deleting Garbage...");
        SQLiteDatabase db = GIS.getDatabase();
        Cursor c = db.query(TrashCan.TABLE_NAME, null, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                String path = CursorUtils.getString(c, TrashCan.COLUMN_PATH);
                if (!TextUtils.isEmpty(path)) {
                    if (path.contains(BuildConfig.APPLICATION_ID)) {
                        GISLogger.log(DEBUG_TAG, "Deleting " + path);
                        FileUtils.delete(new File(path));
                    } else {
                        GISLogger.log(DEBUG_TAG, "Skipping " + path);
                    }
                }
                long row = CursorUtils.getLong(c, "_id");
                db.delete(TrashCan.TABLE_NAME, "_id = ?", new String[]{String.valueOf(row)});
                c.moveToNext();
            }
            c.close();
        }
    }

    private void deleteCachedStateFiles() {
        int i = 0;
        long daysAgo = System.currentTimeMillis() - 604800000;
        File[] files = getDir(FileBasedStateStorage.CACHE_FILE_DIRECTORY, 0).listFiles();
        if (files != null) {
            int length = files.length;
            while (i < length) {
                File file = files[i];
                if (file.lastModified() < daysAgo) {
                    file.delete();
                }
                i++;
            }
        }
    }
}

