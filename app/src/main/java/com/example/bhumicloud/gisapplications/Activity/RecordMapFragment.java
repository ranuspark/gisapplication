package com.example.bhumicloud.gisapplications.Activity;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.Activity.RecordCollectionView;
import com.example.bhumicloud.gisapplications.dossier.RecordCursorLoader;
import com.example.bhumicloud.gisapplications.dossier.RecordPredicate;
import com.example.bhumicloud.gisapplications.maps.GISMarker;
import com.example.bhumicloud.gisapplications.maps.GISMarker.Color;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement.StatusOption;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.RecordFilterOptions;
import com.example.bhumicloud.gisapplications.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class RecordMapFragment extends MapViewFragment implements RecordCollectionView, LoaderCallbacks<Cursor> {
    private static final int LOADER_ID = 110;
    private static LatLngBounds sLastVisibleBounds;
    private boolean mCreateRecordsEnabled = true;
    private RecordFilterOptions mFilterOptions;
    private long mFocusedRecordID = -1;
    private Form mForm;
    private final HashMap<Marker, Long> mMarkers = new HashMap ();
    private String mNoStatusText;
    private String mNoTitleText;
    private String mPlaceName;
    private RecordPredicate mRecordConditions;
    private String mRecordSearchQuery;
    private final Handler mReloadHandler = new Handler ();
    private final Runnable mReloadRunnable = new Runnable () {
        @Override
        public void run() {
            RecordMapFragment.this.reloadData ();
        }
    };
    private Timer mReloadTimer;
    private ArrayList<String> mSelectedRecordIDs;
    private CameraPosition sLastCameraPosition;

    public static LatLngBounds getLastVisibleBounds() {
        return sLastVisibleBounds;
    }

    public void onCreate(Bundle savedInstanceState) {
        boolean z = true;
        super.onCreate(savedInstanceState);
        this.mNoStatusText = getString(R.string.no_status);
        this.mNoTitleText = getString(R.string.no_title);
        Bundle args = getArguments();
        if (!(args == null || args.getBoolean(RecordCollectionView.EXTRA_CREATE_RECORDS_ENABLED, true))) {
            z = false;
        }
        this.mCreateRecordsEnabled = z;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (this.mCreateRecordsEnabled) {
            this.mFloatingActionButton.setVisibility(View.VISIBLE);
            this.mFloatingActionButton.setOnClickListener ( new OnClickListener () {
                @Override
                public void onClick(View v) {
                    RecordMapFragment.this.getListener ().onNewRecordOptionSelected ();
                }
            } );
        }
        return view;
    }

    public void onPause() {
        super.onPause();
        Context context = getActivity();
        if (!(context == null || this.sLastCameraPosition == null)) {
            PatronSettings.setMapCameraPosition(context, this.sLastCameraPosition);
        }
        reset();
    }

    public void setFormID(long formID) {
        this.mForm = Form.getForm(formID);
        reset();
        if (getActivity() != null && this.mForm != null) {
            reloadData();
        }
    }

    public void setFilter(RecordFilterOptions options) {
        this.mFilterOptions = options;
        reset();
        if (getActivity() != null && this.mForm != null) {
            reloadData();
        }
    }

    public void setRecordConditions(RecordPredicate conditions) {
        this.mRecordConditions = conditions;
    }

    public void setSelectedRecordLinkIDs(ArrayList<String> selectedRecordLinkIDs) {
        this.mSelectedRecordIDs = selectedRecordLinkIDs;
    }

    public void setSearchQuery(String query) {
        this.mRecordSearchQuery = query;
        reset();
        reloadData();
    }

    public void reloadData() {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            this.mReloadHandler.post(this.mReloadRunnable);
            return;
        }
        if (this.mCreateRecordsEnabled && this.mFloatingActionButton != null) {
            this.mFloatingActionButton.show();
        }
        reloadRecords();
    }

    public void onSyncFinished() {
        if (isResumed()) {
            hideInteractivityView();
            reloadMapLayers(getActivity());
        }
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new RecordCursorLoader(getActivity(), Account.getActiveAccount(), this.mForm, this.mFilterOptions, this.mRecordConditions, this.mSelectedRecordIDs, this.mRecordSearchQuery, getVisibleBounds(), 500, "_id", "title", "status", Record.COLUMN_LATITUDE, Record.COLUMN_LONGITUDE, Record.COLUMN_UPDATED_AT);
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        onRecordCursorLoaded(data);
    }

    public void onLoaderReset(Loader<Cursor> loader) {
    }

    public void reset() {
        resetReloadTimer();
        clearMarkers();
    }

    public void zoomToFeature(long recordID, LatLng location) {
        this.mFocusedRecordID = recordID;
        super.zoomToFeature(recordID, location);
    }

    protected void onMapReady() {
        reloadData();
        resetFloatingActionButtonPosition();
    }

    protected void onConfigureMap(GoogleMap map) {
        super.onConfigureMap(map);
        Context context = getActivity();
        if (context != null) {
            this.sLastCameraPosition = PatronSettings.getMapCameraPosition(context);
        }
        if (this.sLastCameraPosition != null) {
            map.moveCamera( CameraUpdateFactory.newCameraPosition(this.sLastCameraPosition));
        }
    }

    protected void onCameraIdle(CameraPosition position) {
        this.sLastCameraPosition = position;
        super.onCameraIdle(position);
    }

    protected void onMarkerInfoWindowClicked(Marker marker) {
        super.onMarkerInfoWindowClicked(marker);
        Long recordID = (Long) this.mMarkers.get(marker);
        if (recordID != null) {
            RecordCollectionViewListener listener = getListener();
            if (listener != null) {
                listener.onRecordSelected(recordID.longValue());
            }
        }
    }

    protected void onVisibleRegionChanged(LatLngBounds region) {
        sLastVisibleBounds = region;
        super.onVisibleRegionChanged(region);
        resetReloadTimer();
        this.mReloadTimer = new Timer ();
        this.mReloadTimer.schedule ( new TimerTask () {
            @Override
            public void run() {
                RecordMapFragment.this.reloadData ();
            }
        }, 750 );
    }

    private void reloadRecords() {
        if (this.mForm != null && isResumed() && getActivity() != null) {
            getLoaderManager().restartLoader(110, null, this);
        }
    }

    private void onRecordCursorLoaded(Cursor cursor) {
        if (getMap() != null && cursor != null) {
            try {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    addRecordMarker(cursor);
                    cursor.moveToNext();
                }
            } catch (Throwable e) {
                GISLogger.log(e);
            } finally {
                cursor.close();
            }
        }
    }

    private void resetReloadTimer() {
        if (this.mReloadTimer != null) {
            this.mReloadTimer.cancel();
            this.mReloadTimer = null;
        }
        this.mReloadHandler.removeCallbacks(this.mReloadRunnable);
    }

    private void clearMarkers() {
        for (Marker marker : this.mMarkers.keySet()) {
            marker.hideInfoWindow();
            marker.remove();
        }
        this.mMarkers.clear();
    }

    private void addRecordMarker(Cursor cursor) {
        long recordID = cursor.getLong(0);
        if (!this.mMarkers.containsValue( Long.valueOf(recordID))) {
            String title = cursor.getString(1);
            String status = cursor.getString(2);
            double lat = cursor.getDouble(3);
            double lon = cursor.getDouble(4);
            if (TextUtils.isEmpty(title)) {
                title = this.mNoTitleText;
            }
            MarkerOptions opts = new MarkerOptions ();
            opts.position(new LatLng (lat, lon));
            opts.draggable(false);
            GISMarker.setAnchors(opts);
            opts.title(StringUtils.correctRightToLeft(getResources(), title));
            opts.icon(GISMarker.getMarkerBitmapDescriptor(Color.CB0D0C));
            opts.snippet(this.mNoStatusText);
            if (!(this.mForm == null || TextUtils.isEmpty(status))) {
                StatusElement statusElement = this.mForm.getStatusElement();
                if (statusElement != null && statusElement.isEnabled()) {
                    StatusOption option = statusElement.getOption(status);
                    if (option != null) {
                        opts.icon(GISMarker.getMarkerBitmapDescriptor(option.getColorString()));
                        opts.snippet(option.getLabel());
                    }
                }
            }
            GoogleMap map = getMap();
            if (map != null) {
                Marker marker = map.addMarker(opts);
                this.mMarkers.put(marker, Long.valueOf(recordID));
                if (this.mFocusedRecordID == recordID) {
                    this.mFocusedRecordID = -1;
                    marker.showInfoWindow();
                }
            }
        }
    }

    private RecordCollectionViewListener getListener() {
        return (RecordCollectionViewListener) getActivity();
    }

}
