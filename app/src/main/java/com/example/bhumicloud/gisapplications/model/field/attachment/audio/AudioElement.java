package com.example.bhumicloud.gisapplications.model.field.attachment.audio;

import android.os.Parcel;
import android.os.Parcelable.Creator;

import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaElement;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.HashMap;
import java.util.Map;

public class AudioElement extends MediaElement {
    public static final Creator<AudioElement> CREATOR = new C11791();
    private static final String JSON_TRACK_ENABLED = "track_enabled";
    private boolean mPositionTrackEnabled;

    static class C11791 implements Creator<AudioElement> {
        C11791() {
        }

        public AudioElement createFromParcel(Parcel source) {
            return new AudioElement(source);
        }

        public AudioElement[] newArray(int size) {
            return new AudioElement[size];
        }
    }

    public AudioElement(Element parent, Map json) {
        super(parent, json);
    }

    private AudioElement(Parcel parcel) {
        super(parcel);
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = super.toJSON();
        json.put(JSON_TRACK_ENABLED, Boolean.valueOf(this.mPositionTrackEnabled));
        return json;
    }

    public boolean isPositionTrackEnabled() {
        return this.mPositionTrackEnabled;
    }

    public String getType() {
        return Element.TYPE_AUDIO;
    }

    public void setJSONAttributes(Map json) {
        super.setJSONAttributes(json);
        this.mPositionTrackEnabled = JSONUtils.getBoolean(json, JSON_TRACK_ENABLED, true);
    }
}
