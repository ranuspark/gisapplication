package com.example.bhumicloud.gisapplications.model.field.attachment.video;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaValue;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.List;
import java.util.Map;

public class VideoValue extends MediaValue {
    private final VideoElement mElement;

    public VideoValue(VideoElement element) {
        this.mElement = element;
    }

    public VideoValue(VideoElement element, List json) {
        this(element);
        if (json != null && !json.isEmpty()) {
            for (int i = 0; i < json.size(); i++) {
                Map videoJSON = JSONUtils.getHashMap(json, i);
                if (videoJSON != null) {
                    this.mMediaItems.add(new VideoItemValue(videoJSON));
                }
            }
        }
    }

    protected int getDisplayValueResource() {
        return R.plurals.video;
    }

    public VideoElement getElement() {
        return this.mElement;
    }

    public void addVideo(Video video) {
        if (video != null) {
            this.mMediaItems.add(new VideoItemValue((Attachment) video));
        }
    }
}
