package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.field.SectionElement;

public class NestedSectionElementView extends ElementView<SectionElement> implements OnClickListener {
    private NestedSectionElementViewListener mListener;

    public interface NestedSectionElementViewListener {
        void onNestedSectionElementViewSelected(SectionElement sectionElement);
    }

    public NestedSectionElementView(Context context, SectionElement element, boolean viewOnly) {
        super(context, element, viewOnly);
        setOnClickListener(this);
    }

    public void onClick(View view) {
        if (this.mListener != null) {
            this.mListener.onNestedSectionElementViewSelected((SectionElement) getElement());
        }
    }

    public void setNestedSectionElementViewListener(NestedSectionElementViewListener listener) {
        this.mListener = listener;
    }

    protected int getElementViewLayout() {
        return R.layout.view_nested_section_element;
    }
}
