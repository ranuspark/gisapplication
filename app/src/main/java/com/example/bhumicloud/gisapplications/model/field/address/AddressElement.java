package com.example.bhumicloud.gisapplications.model.field.address;

import android.os.Parcel;
import android.os.Parcelable.Creator;

import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.HashMap;
import java.util.Map;

public class AddressElement extends Element {
    public static final Creator<AddressElement> CREATOR = new C11761();
    private static final String JSON_AUTO_POPULATE = "auto_populate";
    private boolean mAutoPopulate;

    static class C11761 implements Creator<AddressElement> {
        C11761() {
        }

        public AddressElement createFromParcel(Parcel source) {
            return new AddressElement(source);
        }

        public AddressElement[] newArray(int size) {
            return new AddressElement[size];
        }
    }

    public AddressElement(Element parent, Map json) {
        super(parent, json);
    }

    private AddressElement(Parcel parcel) {
        super(parcel);
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = super.toJSON();
        json.put(JSON_AUTO_POPULATE, Boolean.valueOf(isAutoPopulateEnabled()));
        return json;
    }

    public String getType() {
        return Element.TYPE_ADDRESS;
    }

    public boolean isAutoPopulateEnabled() {
        return this.mAutoPopulate;
    }

    public void setJSONAttributes(Map json) {
        super.setJSONAttributes(json);
        this.mAutoPopulate = JSONUtils.getBoolean(json, JSON_AUTO_POPULATE, false);
    }
}
