package com.example.bhumicloud.gisapplications.model.field.attachment.signature;

import android.os.Parcel;
import android.os.Parcelable.Creator;

import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.HashMap;
import java.util.Map;

public class SignatureElement extends Element {
    public static final Creator<SignatureElement> CREATOR = new C11831();
    private static final String JSON_AGREEMENT_TEXT = "agreement_text";
    private String mAgreementText;

    static class C11831 implements Creator<SignatureElement> {
        C11831() {
        }

        public SignatureElement createFromParcel(Parcel source) {
            return new SignatureElement(source);
        }

        public SignatureElement[] newArray(int size) {
            return new SignatureElement[size];
        }
    }

    public SignatureElement(Element parent, Map json) {
        super(parent, json);
    }

    private SignatureElement(Parcel parcel) {
        super(parcel);
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = super.toJSON();
        json.put(JSON_AGREEMENT_TEXT, getAgreementText());
        return json;
    }

    public String getType() {
        return Element.TYPE_SIGNATURE;
    }

    public String getAgreementText() {
        return this.mAgreementText;
    }

    public void setJSONAttributes(Map json) {
        super.setJSONAttributes(json);
        this.mAgreementText = JSONUtils.getString(json, JSON_AGREEMENT_TEXT);
    }
}
