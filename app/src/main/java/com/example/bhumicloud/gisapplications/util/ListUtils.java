package com.example.bhumicloud.gisapplications.util;

import java.util.List;

public class ListUtils {
    public static <T> T getTail(List<T> list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(list.size() - 1);
    }
}
