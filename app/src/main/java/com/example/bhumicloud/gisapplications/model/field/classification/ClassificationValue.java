package com.example.bhumicloud.gisapplications.model.field.classification;

import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.MultipleValueItem;
import com.example.bhumicloud.gisapplications.util.DeviceInfo;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import com.example.bhumicloud.gisapplications.util.ListUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClassificationValue implements FormValue {
    private final ClassificationElement mElement;
    private final List<String> mOtherValues;
    private final List<String> mSelectedValues;

    public static ClassificationItem search(List<String> values, int index, ClassificationItem item) {
        List items = new ArrayList();
        items.add(item);
        return search((List) values, index, items);
    }

    public static ClassificationItem search(List<String> values, int index, List<ClassificationItem> childItems) {
        String val = (String) values.get(index);
        for (ClassificationItem evalItem : childItems) {
            if (evalItem.getValue().equals(val)) {
                if (!evalItem.hasChildItems()) {
                    return evalItem;
                }
                int newIndex = index + 1;
                if (values.size() <= newIndex) {
                    return evalItem;
                }
                ClassificationItem result = search((List) values, newIndex, evalItem.getChildItems());
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }

    public ClassificationValue(ClassificationElement element) {
        this.mSelectedValues = new ArrayList();
        this.mOtherValues = new ArrayList();
        this.mElement = element;
    }

    private ClassificationValue(ClassificationElement element, String other) {
        this(element);
        if (!TextUtils.isEmpty(other)) {
            this.mOtherValues.add(other);
        }
    }

    public ClassificationValue(ClassificationElement element, List<String> selected, String other) {
        this(element, other);
        if (selected != null) {
            this.mSelectedValues.addAll(selected);
        }
    }

    public ClassificationValue(ClassificationElement element, String[] selected, String other) {
        this(element, other);
        if (selected != null) {
            this.mSelectedValues.addAll(Arrays.asList(selected));
        }
    }

    public ClassificationValue(ClassificationElement element, Map json) {
        int i;
        String value;
        this.mSelectedValues = new ArrayList();
        this.mOtherValues = new ArrayList();
        this.mElement = element;
        List selectedJSON = JSONUtils.getArrayList(json, "choice_values");
        if (!(selectedJSON == null || selectedJSON.isEmpty())) {
            for (i = 0; i < selectedJSON.size(); i++) {
                value = JSONUtils.getString(selectedJSON, i);
                if (!TextUtils.isEmpty(value)) {
                    this.mSelectedValues.add(value);
                }
            }
        }
        List otherJSON = JSONUtils.getArrayList(json, "other_values");
        if (otherJSON != null && !otherJSON.isEmpty()) {
            for (i = 0; i < otherJSON.size(); i++) {
                value = JSONUtils.getString(otherJSON, i);
                if (!TextUtils.isEmpty(value)) {
                    this.mOtherValues.add(value);
                }
            }
        }
    }

    public ClassificationElement getElement() {
        return this.mElement;
    }

    public String getDisplayValue() {
        ArrayList<String> labels = toArrayList();
        if (!DeviceInfo.isRightToLeft()) {
            return TextUtils.join(" > ", labels);
        }
        Collections.reverse(labels);
        return TextUtils.join(" < ", labels);
    }

    public String getSearchableValue() {
        ArrayList<String> values = new ArrayList();
        ClassificationItem item = getSelectedClassification();
        if (item != null) {
            for (ClassificationItem part : item.getHierarchicalPath()) {
                if (part.getLabel() != null) {
                    values.add(part.getLabel());
                }
                if (part.getValue() != null) {
                    values.add(part.getValue());
                }
            }
        } else {
            values.addAll(this.mSelectedValues);
        }
        values.addAll(this.mOtherValues);
        return TextUtils.join( " ", values);
    }

    public int length() {
        return toArrayList().size();
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = new HashMap();
        json.put("choice_values", this.mSelectedValues);
        json.put("other_values", this.mOtherValues);
        return json;
    }

    public boolean isEmpty() {
        return this.mSelectedValues.isEmpty() && this.mOtherValues.isEmpty();
    }

    public String getColumnValue() {
        ArrayList<String> values = new ArrayList();
        values.addAll(this.mSelectedValues);
        values.addAll(this.mOtherValues);
        if (values.size() == 0) {
            return null;
        }
        return TextUtils.join("\t", values);
    }

    public ArrayList<MultipleValueItem> getMultipleValues() {
        return null;
    }

    public boolean isEqualForConditions(String value) {
        List<String> selectedValues = getSelectedValues();
        return TextUtils.join(",", selectedValues).startsWith(value.replace("\\,", ","));
    }

    public boolean contains(String value) {
        return isEqualForConditions(value);
    }

    public boolean startsWith(String value) {
        return contains(value);
    }

    public boolean isLessThan(String testValue) {
        if (isEmpty() && !TextUtils.isEmpty(testValue)) {
            return false;
        }
        try {
            if (Double.parseDouble(getLastValue()) < Double.parseDouble(testValue)) {
                return true;
            }
            return false;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean isGreaterThan(String testValue) {
        if (isEmpty() && !TextUtils.isEmpty(testValue)) {
            return false;
        }
        try {
            if (Double.parseDouble(getLastValue()) > Double.parseDouble(testValue)) {
                return true;
            }
            return false;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public List<String> getSelectedValues() {
        return this.mSelectedValues;
    }

    public String getOtherValue() {
        if (this.mOtherValues.size() > 0) {
            return (String) this.mOtherValues.get(0);
        }
        return null;
    }

    public String getLastValue() {
        String other = getOtherValue();
        return !TextUtils.isEmpty(other) ? other : (String) ListUtils.getTail(this.mSelectedValues);
    }

    private ArrayList<String> toArrayList() {
        ArrayList<String> labels = new ArrayList();
        ClassificationItem item = getSelectedClassification();
        if (item != null) {
            for (ClassificationItem part : item.getHierarchicalPath()) {
                if (part.getLabel() != null) {
                    labels.add(part.getLabel());
                }
            }
        } else {
            labels.addAll(this.mSelectedValues);
        }
        labels.addAll(this.mOtherValues);
        return labels;
    }

    private ClassificationItem getSelectedClassification() {
        if (this.mSelectedValues == null || this.mSelectedValues.isEmpty()) {
            return null;
        }
        return search(this.mSelectedValues, 0, getElement().getItems());
    }
}
