package com.example.bhumicloud.gisapplications.model.field.recordlink;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.HashMap;
import java.util.Map;

public class RecordLinkItemValue implements Parcelable {
    public static final Creator<RecordLinkItemValue> CREATOR = new C11961();
    private static final String JSON_RECORD_LINK_RECORD_IDENTIFIER_KEY = "record_id";
    private String mIdentifier;
    private Record mRecord;

    static class C11961 implements Creator<RecordLinkItemValue> {
        C11961() {
        }

        public RecordLinkItemValue createFromParcel(Parcel source) {
            return new RecordLinkItemValue(source);
        }

        public RecordLinkItemValue[] newArray(int size) {
            return new RecordLinkItemValue[size];
        }
    }

    public RecordLinkItemValue(String recordID) {
        this.mIdentifier = recordID;
    }

    public RecordLinkItemValue(Map json) {
        this(JSONUtils.getString(json, "record_id"));
    }

    private RecordLinkItemValue(Parcel parcel) {
        this.mIdentifier = parcel.readString();
        this.mRecord = (Record) parcel.readParcelable(getClass().getClassLoader());
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mIdentifier);
        dest.writeParcelable(this.mRecord, flags);
    }

    public int describeContents() {
        return 0;
    }

    public String getRecordIdentifier() {
        return this.mIdentifier;
    }

    public Record getRecord() {
        return getRecord(false);
    }

    public Record getRecord(boolean forceReload) {
        if (this.mRecord == null || forceReload) {
            this.mRecord = Record.getRecordByRemoteID(Account.getActiveAccount(), this.mIdentifier);
        }
        return this.mRecord;
    }

    public String getRecordTitle() {
        return getRecord() != null ? getRecord().getTitle() : null;
    }

    public HashMap<String, String> toJSON() {
        if (TextUtils.isEmpty(this.mIdentifier)) {
            return null;
        }
        HashMap<String, String> json = new HashMap();
        json.put("record_id", this.mIdentifier);
        return json;
    }
}
