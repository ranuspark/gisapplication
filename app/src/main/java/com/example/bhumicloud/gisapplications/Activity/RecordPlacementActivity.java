package com.example.bhumicloud.gisapplications.Activity;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CameraPosition.Builder;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.Activity.PredefinedListDialog.OnChoiceItemClickedListener;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.example.bhumicloud.gisapplications.util.LocationUtils;
import com.example.bhumicloud.gisapplications.util.PermissionsUtils;

public class RecordPlacementActivity extends LocationListenerActivity implements OnChoiceItemClickedListener {
    public static final String EXTRA_LOCATION = "RecordPlacementActivity.EXTRA_LOCATION";
    private static final String STATE_MAP_CAMERA_POSITION = "STATE_MAP_CAMERA_POSITION";
    public static final String TAG_BASEMAP_SETTING_DIALOG = "TAG_BASEMAP_SETTING_DIALOG";
    private static final String TAG_RECORD_PLACEMENT_MAP_FRAGMENT = "Gis:tag:record_placement_map";
    private RecordPlacementMapFragment mMapViewFragment;

    public static Intent getIntent(Context context, Location location) {
        Intent intent = new Intent(context, RecordPlacementActivity.class);
        intent.putExtra(EXTRA_LOCATION, location);
        return intent;
    }

    protected void onCreate(Bundle savedInstanceState) {
        CameraPosition placementPosition;
        float zoom = 18.0f;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_basic);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        setTitle(R.string.record_placement_activity_title);
        Location location = (Location) getIntent().getParcelableExtra(EXTRA_LOCATION);
        if (savedInstanceState != null) {
            placementPosition = (CameraPosition) savedInstanceState.getParcelable(STATE_MAP_CAMERA_POSITION);
        } else {
            CameraPosition recordViewMapPosition = PatronSettings.getMapCameraPosition(this);
            if (location != null || recordViewMapPosition == null) {
                if (recordViewMapPosition != null) {
                    zoom = Math.max(18.0f, recordViewMapPosition.zoom);
                }
                placementPosition = CameraPosition.fromLatLngZoom(LocationUtils.getLatLng(location), zoom);
            } else {
                placementPosition = new Builder(recordViewMapPosition).zoom(Math.max(18.0f, recordViewMapPosition.zoom)).build();
            }
        }
        FragmentManager fm = getSupportFragmentManager();
        Fragment mapFragment = fm.findFragmentByTag(TAG_RECORD_PLACEMENT_MAP_FRAGMENT);
        if (mapFragment == null) {
            this.mMapViewFragment = RecordPlacementMapFragment.getInstance(location, placementPosition);
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.container, this.mMapViewFragment, TAG_RECORD_PLACEMENT_MAP_FRAGMENT);
            ft.commit();
        } else {
            this.mMapViewFragment = (RecordPlacementMapFragment) mapFragment;
        }
        this.mMapViewFragment.setCrosshairEnabled(true);
        this.mMapViewFragment.setCoordinatesEnabled(true);
    }

    public void onStart() {
        super.onStart();
        if (PermissionsUtils.checkPermissions(this, 64)) {
            startLocationUpdates();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_record_placement, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_set_location:
                onSetLocationOptionSelected();
                return true;
            case R.id.menu_item_basemap:
                onBasemapOptionSelected();
                return true;
        //    case R.id.menu_item_layers:
             //   MapLayersActivity.start(this);
        //        return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(STATE_MAP_CAMERA_POSITION, this.mMapViewFragment.getCurrentCameraPosition());
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if ((requestCode & 64) == 64 && grantResults.length > 0 && grantResults[0] == 0) {
            startLocationUpdates();
            this.mMapViewFragment.setMyLocationEnabled(null);
        }
    }

    public void onChoiceItemClicked(Object selectedItem, int position, boolean selected) {
        if (selected && PatronSettings.getBasemap(this) != position) {
            PatronSettings.setBasemap(this, position);
            this.mMapViewFragment.setBasemap(position);
        }
    }

    private void onSetLocationOptionSelected() {
        Location location = this.mMapViewFragment.getCenterCoordinate();
        if (location != null) {
            Intent result = new Intent();
            result.putExtra(EXTRA_LOCATION, location);
            setResult(-1, result);
        }
        finish();
    }

    private void onBasemapOptionSelected() {
        PredefinedListDialog dialog = PredefinedListDialog.getInstance(R.string.basemap, R.array.basemap_options);
        dialog.setOnChoiceItemClickedListener(this);
        dialog.setSelectedItem(PatronSettings.getBasemap(this));
        dialog.show(getSupportFragmentManager(), TAG_BASEMAP_SETTING_DIALOG);
    }
}
