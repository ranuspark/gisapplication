package com.example.bhumicloud.gisapplications.geoJson;

import java.util.HashMap;

public abstract class GeoJSONObject {
    public abstract String getType();

    public HashMap<String, Object> toGeoJSON() {
        HashMap<String, Object> json = new HashMap();
        json.put("type", getType());
        return json;
    }
}
