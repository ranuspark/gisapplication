package com.example.bhumicloud.gisapplications;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import com.example.bhumicloud.gisapplications.dossier.PersistentStore;
import com.example.bhumicloud.gisapplications.dossier.SearchIndex;
import com.example.bhumicloud.gisapplications.util.ApplicationUtils;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.squareup.leakcanary.LeakCanary;


public class GIS extends MultiDexApplication {

    private static boolean isActivityVisible;
    private static GIS sSharedInstance;

    public static GIS getInstance() {
        return sSharedInstance;
    }

    public static SQLiteDatabase getDatabase() {
        return PersistentStore.getInstance(getInstance()).getWritableDatabase();
    }

    public static SearchIndex getSearchIndex() {
        Context instance = getInstance();
        return instance != null ? SearchIndex.getInstance(instance) : null;
    }

    public void onCreate() {
        super.onCreate();
        if (!LeakCanary.isInAnalyzerProcess(this)) {
            LeakCanary.install(this);
            sSharedInstance = this;
            GISLogger.initialize(this);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Launching ");
            stringBuilder.append(ApplicationUtils.getFormattedVersionString(this));
            GISLogger.log(stringBuilder.toString());
            System.setProperty("http.keepAlive", "true");
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static boolean isActivityVisible() {
        return isActivityVisible;
    }

    public static void activityResumed() {
        isActivityVisible = true;
    }

    public static void activityPaused() {
        isActivityVisible = false;
    }
}

