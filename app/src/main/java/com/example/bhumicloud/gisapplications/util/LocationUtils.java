package com.example.bhumicloud.gisapplications.util;

import android.content.res.Resources;
import android.hardware.GeomagneticField;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.text.DecimalFormat;

public class LocationUtils {
    public static LatLng getLatLng(Location location) {
        return new LatLng(location.getLatitude(), location.getLongitude());
    }

    public static float getDeclination(Location location) {
        return new GeomagneticField((float) location.getLatitude(), (float) location.getLongitude(), (float) location.getAltitude(), location.getTime()).getDeclination();
    }

    public static Location toLocation(LatLng latLng) {
        Location location = new Location("Unknown");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
        return location;
    }

    public static String toString(Location location, Resources resources) {
        if (location == null) {
            return null;
        }
        double lat = location.getLatitude();
        double lon = location.getLongitude();
        DecimalFormat format = NumberUtils.getCoordinateFormat(resources);
        return format.format(lat) + ", " + format.format(lon);
    }

    public static boolean equals(Location loc1, Location loc2) {
        if (loc1 == null || loc2 == null) {
            if (loc2 == null) {
                return true;
            }
            return false;
        } else if (loc1.getAccuracy() == loc2.getAccuracy() && loc1.getAltitude() == loc2.getAltitude() && loc1.getBearing() == loc2.getBearing() && loc1.getLatitude() == loc2.getLatitude() && loc1.getLongitude() == loc2.getLongitude() && loc1.getProvider().equals(loc2.getProvider()) && loc1.getSpeed() == loc2.getSpeed() && loc1.getTime() == loc2.getTime()) {
            return true;
        } else {
            return false;
        }
    }

    public static Location create(double latitude, double longitude) {
        Location location = new Location("manual");
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        location.setAccuracy(0.0f);
        return location;
    }
}
