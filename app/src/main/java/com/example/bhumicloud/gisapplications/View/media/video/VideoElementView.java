package com.example.bhumicloud.gisapplications.View.media.video;

import android.content.Context;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment.Type;
import com.example.bhumicloud.gisapplications.model.field.attachment.video.VideoElement;
import com.example.bhumicloud.gisapplications.View.media.MediaElementView;

public class VideoElementView extends MediaElementView {
    public VideoElementView(Context context, VideoElement element, boolean viewOnly) {
        super(context, element, viewOnly);
    }

    protected int getThumbnailLayoutResource() {
        return R.layout.view_video_thumbnail;
    }

    protected int getObserveActionResource() {
        return R.string.media_play;
    }

    protected Type getType() {
        return Type.VIDEO;
    }

    protected int getNewMediaDrawable() {
        return R.drawable.ic_media_videocam;
    }

    protected int getSelectMediaDrawable() {
        return R.drawable.ic_library_video;
    }
}
