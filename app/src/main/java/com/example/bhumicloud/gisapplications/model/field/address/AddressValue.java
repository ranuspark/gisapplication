package com.example.bhumicloud.gisapplications.model.field.address;

import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.example.bhumicloud.gisapplications.location.Address;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.MultipleValueItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class AddressValue implements FormValue {
    private Address mAddress;
    private final AddressElement mElement;

    public AddressValue(AddressElement element, Address address) {
        this.mElement = element;
        this.mAddress = address;
    }

    public AddressValue(AddressElement element, Map json) {
        this.mElement = element;
        if (json != null) {
            this.mAddress = new Address(json);
        }
    }

    public AddressElement getElement() {
        return this.mElement;
    }

    public String getDisplayValue() {
        if (this.mAddress == null) {
            return "";
        }
        List<String> lines = new ArrayList();
        lines.add(this.mAddress.getAddressLine1());
        lines.add(this.mAddress.getAddressLine2());
        lines.add(this.mAddress.getAddressLine3());
        return TextUtils.join("\n", lines);
    }

    public String getSearchableValue() {
        if (this.mAddress == null) {
            return "";
        }
        List<String> lines = new ArrayList();
        lines.add(this.mAddress.getAddressLine1());
        lines.add(this.mAddress.getAddressLine2());
        lines.add(this.mAddress.getAddressLine3());
        return TextUtils.join(" ", lines);
    }

    public int length() {
        return getDisplayValue().length();
    }

    public HashMap<String, Object> toJSON() {
        return this.mAddress != null ? this.mAddress.toJSON() : null;
    }

    public boolean isEmpty() {
        return this.mAddress == null || this.mAddress.isEmpty();
    }

    public HashMap<String, String> getColumnValue() {
        HashMap<String, String> columnValues = new HashMap();
        HashMap<String, Object> address = getAddress().toJSON();
        String addressKey = getElement().getKey();
        for (Entry<String, Object> entry : address.entrySet()) {
            String value = (String) entry.getValue();
            if (!TextUtils.isEmpty(value)) {
                columnValues.put(String.format("%s_%s", new Object[]{addressKey, entry.getKey()}), value);
            }
        }
        columnValues.put(addressKey, getSearchableValue());
        return columnValues;
    }

    public ArrayList<MultipleValueItem> getMultipleValues() {
        return new ArrayList();
    }

    public boolean isEqualForConditions(String testValue) {
        return false;
    }

    public boolean contains(String testValue) {
        return false;
    }

    public boolean startsWith(String testValue) {
        return false;
    }

    public boolean isLessThan(String testValue) {
        return false;
    }

    public boolean isGreaterThan(String testValue) {
        return false;
    }

    public Address getAddress() {
        return this.mAddress;
    }
}
