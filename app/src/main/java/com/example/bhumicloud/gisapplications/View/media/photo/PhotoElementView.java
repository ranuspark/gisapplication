package com.example.bhumicloud.gisapplications.View.media.photo;

import android.content.Context;
import com.example.bhumicloud.gisapplications.R;
import  com.example.bhumicloud.gisapplications.model.field.attachment.Attachment.Type;
import  com.example.bhumicloud.gisapplications.model.field.attachment.photo.PhotoElement;
import  com.example.bhumicloud.gisapplications.View.media.MediaElementView;

public class PhotoElementView extends MediaElementView {
    public PhotoElementView(Context context, PhotoElement element, boolean viewOnly) {
        super(context, element, viewOnly);
    }

    protected int getThumbnailLayoutResource() {
        return R.layout.view_photo_thumbnail;
    }

    protected int getObserveActionResource() {
        return R.string.media_view;
    }

    protected Type getType() {
        return Type.PHOTO;
    }

    protected int getNewMediaDrawable() {
        return R.drawable.ic_media_camera;
    }

    protected int getSelectMediaDrawable() {
        return R.drawable.ic_library_image;
    }
}
