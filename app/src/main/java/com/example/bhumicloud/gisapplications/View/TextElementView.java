package com.example.bhumicloud.gisapplications.View;

import android.app.Activity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.NumberKeyListener;
import android.widget.EditText;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.field.textual.text.TextElement;
import com.example.bhumicloud.gisapplications.model.field.textual.text.TextValue;
import com.example.bhumicloud.gisapplications.util.NumberUtils;
import java.text.DecimalFormatSymbols;

public class TextElementView extends ElementView<TextElement> implements TextWatcher {
    private static int DECIMAL_FLAGS;
    private static int DEFAULT_FLAGS;
    private static int INTEGER_FLAGS;
    private boolean mContainsInvalidValue = false;
    private boolean mKeyboardEntry = false;

    static {
        DEFAULT_FLAGS |= 1;
        DEFAULT_FLAGS |= 131072;
        DEFAULT_FLAGS |= 262144;
        DEFAULT_FLAGS |= 16384;
        DECIMAL_FLAGS |= 2;
        DECIMAL_FLAGS |= 8192;
        DECIMAL_FLAGS |= 4096;
        INTEGER_FLAGS |= 2;
        INTEGER_FLAGS |= 4096;
    }

    public TextElementView(Activity activity, TextElement element, boolean viewOnly) {
        super(activity, element, viewOnly);
        addFieldTextChangedListener(this);
        if (element.isNumeric()) {
            if (element.isInteger()) {
                setFieldInputType(INTEGER_FLAGS);
            } else {
                setFieldInputType(DECIMAL_FLAGS);
                char decimalSeparator = DecimalFormatSymbols.getInstance().getDecimalSeparator();
                char unicodeValue = decimalSeparator;
                String acceptedCharacters = "0123456789-" + decimalSeparator;
                if (unicodeValue == '٫') {
                    acceptedCharacters = acceptedCharacters + ',';
                }
                final char[] acceptedChars = acceptedCharacters.toCharArray();
                setFieldKeyListener(new NumberKeyListener () {
                    protected char[] getAcceptedChars() {
                        return acceptedChars;
                    }

                    public int getInputType() {
                        return TextElementView.DECIMAL_FLAGS;
                    }
                });
            }
            setFieldImeOptions(33554438);
        } else {
            setFieldInputType(DEFAULT_FLAGS);
        }
        this.mKeyboardEntry = true;
    }

    public void afterTextChanged(Editable s) {
        if (this.mKeyboardEntry) {
            boolean z;
            String newValue = getNormalizedValueFromLocalizedUserInput(s.toString());
            if (newValue.equals(s.toString())) {
                z = false;
            } else {
                z = true;
            }
            this.mContainsInvalidValue = z;
            notifyOnFormValueDidChange(new TextValue((TextElement) getElement(), newValue));
            this.mContainsInvalidValue = false;
        }
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    public void setValue(FormValue value) {
        if (!this.mContainsInvalidValue) {
            TextValue textValue = (TextValue) value;
            this.mKeyboardEntry = false;
            if (textValue == null || textValue.isEmpty()) {
                clearFieldValue();
            } else if (!TextUtils.equals(textValue.getDisplayValue(), getFieldValue())) {
                setValue(getLocalizedStringForCurrentValue(textValue.getValue()));
            }
            this.mKeyboardEntry = true;
        }
    }

    public void update() {
        boolean z;
        super.update();
        EditText fieldView = getFieldView();
        fieldView.setFocusable(!isReadOnly());
        if (isReadOnly()) {
            z = false;
        } else {
            z = true;
        }
        fieldView.setFocusableInTouchMode(z);
        if (((TextElement) getElement()).hasMaxLength()) {
            fieldView.setFilters(new InputFilter[]{new LengthFilter (((TextElement) getElement()).getMaxLength())});
        } else {
            fieldView.setFilters(new InputFilter[0]);
        }
    }

    private String getLocalizedStringForCurrentValue(String currentValue) {
        String value = currentValue;
        if (!((TextElement) getElement()).isNumeric()) {
            return value;
        }
        String formatted = NumberUtils.formatLocalizedStringFromMachineString(value, true);
        if (formatted != null) {
            return formatted;
        }
        return value;
    }

    private String getNormalizedValueFromLocalizedUserInput(String userInput) {
        String value = userInput;
        if (!((TextElement) getElement()).isNumeric()) {
            return value;
        }
        String formatted = NumberUtils.formatMachineStringFromLocalizedString(userInput);
        if (formatted != null) {
            return formatted;
        }
        return userInput;
    }
}
