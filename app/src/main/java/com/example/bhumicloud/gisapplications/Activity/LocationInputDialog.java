package com.example.bhumicloud.gisapplications.Activity;

import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.util.KeyboardUtils;
import com.example.bhumicloud.gisapplications.util.LocationUtils;
import com.example.bhumicloud.gisapplications.util.NumberUtils;

import java.text.DecimalFormat;
import java.text.ParseException;

public class LocationInputDialog extends ThemedDialogFragment {
    private EditText mLatField;
    private LocationInputDialogListener mListener;
    private Location mLocation;
    private EditText mLonField;

    public interface LocationInputDialogListener {
        void onLocationValueEntered(Location location);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle((int) R.string.location_input_dialog_title);
        setNegativeButtonText((int) R.string.clear);
        setPositiveButtonText((int) R.string.done);
    }

    public View onCreateContentView(LayoutInflater inflater, ViewGroup root, Bundle savedState) {
        View view = inflater.inflate(R.layout.dialog_location_input, root, false);
        this.mLatField = (EditText) view.findViewById(R.id.lat_field);
        this.mLonField = (EditText) view.findViewById(R.id.lon_field);
        if (this.mLocation != null) {
            double lat = this.mLocation.getLatitude();
            double lon = this.mLocation.getLongitude();
            DecimalFormat format = NumberUtils.getCoordinateFormat(getResources());
            this.mLatField.setText(format.format(lat));
            this.mLonField.setText(format.format(lon));
        }
        return view;
    }

    protected void onNegativeButtonClicked() {
        super.onNegativeButtonClicked();
        if (this.mLatField != null) {
            this.mLatField.setText(null);
        }
        if (this.mLonField != null) {
            this.mLonField.setText(null);
        }
    }

    protected void onPositiveButtonClicked() {
        super.onPositiveButtonClicked();
        try {
            double lat = parseCoordinateString(this.mLatField.getText().toString());
            double lon = parseCoordinateString(this.mLonField.getText().toString());
            if (isCoordinateValid(lat, lon)) {
                KeyboardUtils.hideDialogSoftKeyboard(getActivity(), getView());
                if (this.mListener != null) {
                    this.mListener.onLocationValueEntered(LocationUtils.create(lat, lon));
                }
                dismissAllowingStateLoss();
                return;
            }
            showParsingErrorDialog();
        } catch (ParseException e) {
            showParsingErrorDialog();
        }
    }

    public void onDestroy() {
        this.mListener = null;
        super.onDestroy();
    }

    public void setLocation(Location location) {
        this.mLocation = location;
    }

    public void setLocationInputDialogListener(LocationInputDialogListener listener) {
        this.mListener = listener;
    }

    private boolean isCoordinateValid(double lat, double lon) {
        return Math.abs(lat) <= 90.0d && Math.abs(lon) <= 180.0d;
    }

    private double parseCoordinateString(String coord) throws ParseException {
        try {
            return Double.parseDouble(coord);
        } catch (NumberFormatException e) {
            return DecimalFormat.getInstance().parse(coord).doubleValue();
        }
    }

    private void showParsingErrorDialog() {
        Toast.makeText(getActivity(), getResources().getString(R.string.invalid_coordinates), Toast.LENGTH_SHORT).show();
    }
}
