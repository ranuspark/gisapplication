package com.example.bhumicloud.gisapplications.model.field.textual.status;

import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.example.bhumicloud.gisapplications.model.field.textual.TextualValue;

public class StatusValue extends TextualValue {
    private final StatusElement mElement;

    public StatusValue(StatusElement element) {
        this.mElement = element;
    }

    public StatusValue(StatusElement element, String aValue) {
        super(aValue);
        this.mElement = element;
    }

    public StatusElement getElement() {
        return this.mElement;
    }
}
