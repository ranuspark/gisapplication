package com.example.bhumicloud.gisapplications.model.field.textual.yesno;

import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.example.bhumicloud.gisapplications.model.field.textual.TextualValue;

public class YesNoValue extends TextualValue {
    private final YesNoElement mElement;

    public YesNoValue(YesNoElement element, String aValue) {
        super(aValue);
        this.mElement = element;
    }

    public String getDisplayValue() {
        String displayValue = getValue();
        if (isPositive()) {
            return this.mElement.getPositiveChoice().getLabel();
        }
        if (isNegative()) {
            return this.mElement.getNegativeChoice().getLabel();
        }
        if (isNeutral()) {
            return this.mElement.getNeutralChoice().getLabel();
        }
        return displayValue;
    }

    public YesNoElement getElement() {
        return this.mElement;
    }


    public boolean isPositive() {
        return !isEmpty() && getValue().equals(this.mElement.getPositiveChoice().getValue());
    }

    public boolean isNegative() {
        return !isEmpty() && getValue().equals(this.mElement.getNegativeChoice().getValue());
    }

    public boolean isNeutral() {
        return !isEmpty() && this.mElement.isNeutralChoiceEnabled() && getValue().equals(this.mElement.getNeutralChoice().getValue());
    }
}
