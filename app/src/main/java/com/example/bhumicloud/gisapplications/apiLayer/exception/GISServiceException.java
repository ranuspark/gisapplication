package com.example.bhumicloud.gisapplications.apiLayer.exception;

import android.content.Context;

import java.io.IOException;

public class GISServiceException extends IOException {
    public GISServiceException(Throwable throwable) {
        super(throwable.getMessage() == null ? throwable.toString() : throwable.getMessage());
    }

    public GISServiceException(String message) {
        super(message);
    }

    public String getUserFriendlyMessage(Context context) {
        return getMessage();
    }
}
