package com.example.bhumicloud.gisapplications.util;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class SQLiteUtils {
    public static long getCount(SQLiteDatabase db, String table, String predicate, String[] args) {
        long count = 0;
        Cursor c = db.query(table, new String[]{"count(*)"}, predicate, args, null, null, null);
        if (c != null) {
            c.moveToFirst();
            if (!c.isAfterLast()) {
                count = c.getLong(0);
            }
            c.close();
        }
        return count;
    }
}
