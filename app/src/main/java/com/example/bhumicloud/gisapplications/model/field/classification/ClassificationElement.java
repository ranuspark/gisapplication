package com.example.bhumicloud.gisapplications.model.field.classification;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClassificationElement extends Element {
    public static final Creator<ClassificationElement> CREATOR = new C11891();
    private static final String JSON_ALLOW_OTHER = "allow_other";
    private static final String JSON_CLASSIFICATION_SET_ID = "classification_set_id";
    private boolean mAllowOther;
    private ArrayList<String> mClassificationFilter;
    private ClassificationSet mClassificationSet;
    private String mClassificationSetID;
    private ArrayList<ClassificationItem> mOverrideClassificationItems;

    static class C11891 implements Creator<ClassificationElement> {
        C11891() {
        }

        public ClassificationElement createFromParcel(Parcel source) {
            return new ClassificationElement(source);
        }

        public ClassificationElement[] newArray(int size) {
            return new ClassificationElement[size];
        }
    }

    public ClassificationElement(Element parent, Map json) {
        super(parent, json);
    }

    private ClassificationElement(Parcel parcel) {
        super(parcel);
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = super.toJSON();
        json.put(JSON_ALLOW_OTHER, Boolean.valueOf(isOtherAllowed()));
        json.put(JSON_CLASSIFICATION_SET_ID, this.mClassificationSetID);
        return json;
    }

    public String getType() {
        return Element.TYPE_CLASSIFICATION;
    }

    public boolean isOtherAllowed() {
        return this.mAllowOther;
    }

    public List<ClassificationItem> getItems() {
        return this.mOverrideClassificationItems != null ? this.mOverrideClassificationItems : getFilteredChoices();
    }

    public void setJSONAttributes(Map json) {
        super.setJSONAttributes(json);
        this.mAllowOther = JSONUtils.getBoolean(json, JSON_ALLOW_OTHER, false);
        this.mClassificationSetID = JSONUtils.getString(json, JSON_CLASSIFICATION_SET_ID);
        this.mClassificationSet = null;
    }

    public void setOverrideClassificationItems(List overrideItems) {
        if (overrideItems == null || overrideItems.isEmpty()) {
            this.mOverrideClassificationItems = null;
            return;
        }
        ArrayList<ClassificationItem> newItems = new ArrayList();
        for (Object item : overrideItems) {
            if (item instanceof Map) {
                newItems.add(new ClassificationItem(null, (Map) item));
            }
        }
        this.mOverrideClassificationItems = newItems;
    }

    public void setClassificationFilter(List items) {
        if (items == null || items.isEmpty()) {
            this.mClassificationFilter = null;
            return;
        }
        this.mClassificationFilter = new ArrayList();
        for (Object item : items) {
            if (item instanceof String) {
                this.mClassificationFilter.add(((String) item).toLowerCase());
            }
        }
    }

    private ClassificationSet getClassificationSet() {
        if (this.mClassificationSet == null && !TextUtils.isEmpty(this.mClassificationSetID)) {
            this.mClassificationSet = ClassificationSet.getClassificationSet(Account.getActiveAccount(), this.mClassificationSetID);
        }
        return this.mClassificationSet;
    }

    private List<ClassificationItem> getFilteredChoices() {
        List<ClassificationItem> items;
        ClassificationSet classificationSet = getClassificationSet();
        if (classificationSet == null) {
            items = new ArrayList();
        } else {
            items = classificationSet.getItems();
        }
        if (this.mClassificationFilter == null || items.isEmpty()) {
            return items;
        }
        List<ClassificationItem> filteredItems = new ArrayList();
        for (ClassificationItem item : items) {
            if (this.mClassificationFilter.contains(item.getValue().toLowerCase())) {
                filteredItems.add(item);
            }
        }
        return filteredItems;
    }
}
