package com.example.bhumicloud.gisapplications.Activity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.Role;
import com.example.bhumicloud.gisapplications.util.ActivityRequestCodes;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.example.bhumicloud.gisapplications.util.RecordFilterOptions;
import com.example.bhumicloud.gisapplications.widget.SearchRelativeLayout;
import com.google.android.gms.maps.model.LatLng;
import com.example.bhumicloud.gisapplications.Activity.FormPickerFragment.FormPickerFragmentListener;
import com.example.bhumicloud.gisapplications.Activity.SyncService.Listener;
import com.example.bhumicloud.gisapplications.Activity.SyncService.SyncServiceBinder;
import com.example.bhumicloud.gisapplications.dossier.FormLoader;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.TrashCan;
import com.example.bhumicloud.gisapplications.util.AlertUtils;
import com.example.bhumicloud.gisapplications.util.CursorUtils;
import com.example.bhumicloud.gisapplications.util.PermissionsUtils;
import com.google.zxing.client.android.Intents;

public class DashboardActivity extends LocationListenerActivity implements FormPickerFragmentListener, Listener, RecordCollectionViewListener, DrawerListener, LoaderCallbacks<Cursor> {
    public static final String EXTRA_ACTION = "DashboardActivity.EXTRA_ACTION";
    private static final int LOADER_ID = 100;
    private static final String STATE_FILTER_OPTS = "mFilterOptions";
    private static final String STATE_SEARCH_QUERY = "STATE_SEARCH_QUERY";
    private static final String STATE_SEARCH_VISIBLE = "STATE_SEARCH_VISIBLE";
    public static final int VIEW_MODE_LIST = 2;
    public static final int VIEW_MODE_MAP = 1;
    private long mActiveFormID;
    private long mAccountID;

    private int mActiveViewMode;
    private RecordCollectionView mCollectionView;
    private MenuItem mFilterItem;
    private MenuItem mAdvance_s;

    private DrawerLayout mFormDrawer;
    private FormPickerFragment mFormPickerFragment;
    private boolean mMenuItemVisibility;
    private EditText mSearchActionView;
    private String mSearchQuery;
    private MenuItem mSearchRecordsItem;
    private boolean mSearchVisible;
    private View mSyncActionView;
    private Pair<Boolean, String> mSyncResultWhileAway;
    private SyncService mSyncService;
    private boolean mSyncServiceBound;
    private Toolbar mToolbar;
    private MenuItem mViewToggleItem;
    String project_name;

    private final ServiceConnection mSyncConnection = new ServiceConnection () {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            if (binder instanceof SyncServiceBinder) {
                SyncService svc = ((SyncServiceBinder) binder).getService();
                svc.addListener(DashboardActivity.this);
                if (svc.isSynchronizing()) {
                    DashboardActivity.this.showSyncAnimation();
                } else {
                    DashboardActivity.this.hideSyncAnimation();
                }
                DashboardActivity.this.mSyncService = svc;
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            if (DashboardActivity.this.mSyncService != null) {
                DashboardActivity.this.mSyncService.removeListener(DashboardActivity.this);
            }
            DashboardActivity.this.mSyncService = null;
        }
    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_dashboard);
        this.mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.mToolbar);
        this.mActiveFormID = PatronSettings.getActiveFormID(this);
        this.mActiveFormID = PatronSettings.getActiveFormID(this);
        this.mActiveViewMode = PatronSettings.getActiveViewMode(this, 2);
        FragmentManager fm = getSupportFragmentManager();
        if (savedInstanceState != null) {
            this.mSearchQuery = savedInstanceState.getString(STATE_SEARCH_QUERY);
            this.mSearchVisible = savedInstanceState.getBoolean(STATE_SEARCH_VISIBLE, false);
        }
        Fragment collectionView = fm.findFragmentById(R.id.container);
        if (collectionView instanceof RecordCollectionView) {
            this.mCollectionView = (RecordCollectionView) collectionView;
        }
        configureCollectionView();
        this.mFormPickerFragment = (FormPickerFragment) getSupportFragmentManager().findFragmentById(R.id.form_picker_drawer);
        this.mFormDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        this.mFormDrawer.addDrawerListener(this);
        reloadForms();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        hideSyncAnimation();
        getMenuInflater().inflate(R.menu.activity_dashboard, menu);//
        this.mAdvance_s = menu.findItem(R.id.search1);
        this.mFilterItem = menu.findItem(R.id.menu_item_filter);
        this.mViewToggleItem = menu.findItem(R.id.menu_item_view_toggle);
        this.mSearchRecordsItem = menu.findItem(R.id.menu_item_search);
        MenuItem synchronizeItem = menu.findItem(R.id.menu_item_synchronize);
        final SearchRelativeLayout searchView = (SearchRelativeLayout) MenuItemCompat.getActionView(this.mSearchRecordsItem);
        this.mSearchActionView = searchView.getSearchEditText();
        searchView.setOnSearchQueryChangedListener ( new SearchRelativeLayout.OnSearchQueryChangedListener () {
            @Override
            public void onSearchQueryChanged(String query) {
                DashboardActivity.this.mSearchQuery = query;
                if (DashboardActivity.this.mCollectionView != null) {
                    DashboardActivity.this.mCollectionView.setSearchQuery(query);
                }
            }
        } );

        MenuItemCompat.setOnActionExpandListener(this.mSearchRecordsItem, new OnActionExpandListener() {
            public boolean onMenuItemActionExpand(MenuItem item) {
                DashboardActivity.this.mSearchVisible = true;
                searchView.onMenuItemActionExpand(item);
                return true;
            }

            public boolean onMenuItemActionCollapse(MenuItem item) {
                DashboardActivity.this.mSearchVisible = false;
                searchView.onMenuItemActionCollapse(item);
                return true;
            }
        });

        this.mSearchActionView.setText(this.mSearchQuery);
        if (this.mSearchVisible) {
            this.mSearchRecordsItem.expandActionView();
            if (this.mSearchQuery != null) {
                this.mSearchActionView.setSelection(this.mSearchQuery.length());
            }
        }
        this.mSyncActionView = MenuItemCompat.getActionView(synchronizeItem);
        this.mSyncActionView.setOnClickListener ( new OnClickListener () {
            @Override
            public void onClick(View view) {
                DashboardActivity.this.onSynchronizeDataOptionSelected();
            }
        } );
        if (isCurrentlySynchronizing()) {
            showSyncAnimation();
        } else {
            hideSyncAnimation();
        }
        reloadMenuItemVisibility(this.mMenuItemVisibility);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search1:
                onAdvanceSearch();
                return true;
            case R.id.menu_item_synchronize:
                onSynchronizeDataOptionSelected();
                return true;
            case R.id.menu_item_view_toggle:
                onToggleViewModeOptionSelected();
                return true;
            case R.id.menu_item_settings:
                onAppSettingsMenuItemSelected();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_SEARCH_QUERY, this.mSearchQuery);
        outState.putBoolean(STATE_SEARCH_VISIBLE, this.mSearchVisible);
    }

    protected void onStart() {
        super.onStart();
        this.mSyncServiceBound = SyncService.bind(this, this.mSyncConnection);
    }

    protected void onResume() {
        super.onResume();
        GIS.activityResumed();
        if (this.mSyncResultWhileAway != null) {
            if (((Boolean) this.mSyncResultWhileAway.first).booleanValue()) {
                onSyncFinished();
            } else {
                onSyncFailed((String) this.mSyncResultWhileAway.second);
            }
            this.mSyncResultWhileAway = null;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case ActivityRequestCodes.REQUEST_RECORD_BARCODE:
                    String contents = data.getStringExtra( Intents.Scan.RESULT);
                    if (contents != null) {
                        if (this.mSearchActionView == null) {
                            this.mSearchQuery = contents;
                            break;
                        }
                        this.mSearchActionView.setText(contents);
                        this.mSearchActionView.setSelection(contents.length());
                        break;
                    }
                    break;
                case ActivityRequestCodes.REQUEST_APP_GALLERY:
                    onSynchronizeDataOptionSelected();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onBackPressed() {
        if (!this.mFormDrawer.isDrawerOpen((int) GravityCompat.START) || this.mFormDrawer.getDrawerLockMode((int) GravityCompat.START) == 2) {
            super.onBackPressed();
        } else {
            this.mFormDrawer.closeDrawer((int) GravityCompat.START);
        }
    }

    protected void onPause() {
        super.onPause();
        GIS.activityPaused();
    }

    protected void onStop() {
        super.onStop();
        if (this.mSyncServiceBound) {
            this.mSyncServiceBound = false;
            unbindService(this.mSyncConnection);
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.mFormDrawer != null) {
            this.mFormDrawer.removeDrawerListener(this);
        }
    }

    public void onFormSelected(long selectedFormID) {
        Form form = Form.getForm(selectedFormID);
        try {
            this.mActiveFormID = form.getRowID();
            this.mAccountID = form.getAccountID();
            updateFormButton(form.getName());
        } catch (NullPointerException e) {
            GISLogger.log("No form found for selectedFormID: " + selectedFormID);
        }
        PatronSettings.setActiveFormID(this, this.mActiveFormID);
        if (this.mCollectionView != null) {
            this.mCollectionView.setFormID(this.mActiveFormID);
        }
        this.mFormDrawer.setDrawerLockMode(0);
        this.mFormDrawer.closeDrawer((int) GravityCompat.START);
    }

    public void onSyncFailed(String reason) {
        if (!GIS.isActivityVisible()) {
            this.mSyncResultWhileAway = new Pair(Boolean.valueOf(false), reason);
        }
    }

    public void onSyncFinished() {
        if (GIS.isActivityVisible()) {
            reloadForms();
            if (this.mCollectionView != null) {
                this.mCollectionView.onSyncFinished();
                this.mCollectionView.reloadData();
            }
            hideSyncAnimation();
            Intent oldIntent = getIntent();
            if (oldIntent.hasExtra(EXTRA_ACTION)) {
                Intent newIntent = new Intent(this, RecordEditorActivity.class);
                newIntent.putExtras(oldIntent);
                startActivity(newIntent);
                finish();
                return;
            }
            return;
        }
        this.mSyncResultWhileAway = new Pair(Boolean.valueOf(true), null);
    }

    public void onRecordSelected(long recordID) {
        if (isCurrentlySynchronizing()) {
            AlertUtils.showSynchronizingAlert(this);
        } else {
            startActivity(RecordEditorActivity.getEditRecordIntent(this, recordID));
        }
    }

    public void onShowRecordLocation(long recordID, double latitude, double longitude) {
        LatLng location = new LatLng(latitude, longitude);
        if (this.mActiveViewMode != VIEW_MODE_MAP) {
            setViewMode(VIEW_MODE_MAP, false);
        }
        ((MapViewFragment) this.mCollectionView).zoomToFeature(recordID, location);
    }

    public void onDuplicateRecord(long recordID, boolean location, boolean values) {
        if (isCurrentlySynchronizing()) {
            AlertUtils.showSynchronizingAlert(this);
        } else {
            startActivity(RecordEditorActivity.getDuplicateRecordIntent(this, recordID, location, values));
        }
    }

    public void onGenerateReport(long record) {
    }

    public void onDeleteRecord(long recordID) {
        if (isCurrentlySynchronizing()) {
            AlertUtils.showSynchronizingAlert(this);
            return;
        }
        Record record = Record.getRecord(recordID);
        if (record != null) {
            record.deleteWithTransaction();
            Toast.makeText(this, R.string.record_deleted_success, Toast.LENGTH_SHORT).show();
            if (this.mCollectionView != null) {
                this.mCollectionView.reloadData();
            }
            Form form = record.getForm();
            if (form != null) {
                form.decrementRecordCount();
            }
        }
        TrashCan.empty(this);
    }

    public void onNewRecordOptionSelected() {
        Account account = Account.getActiveAccount();
        if (account == null) {
            return;
        }
        if (!account.getRole().canCreateRecords()) {
            AlertUtils.showPermissionsAlert(this, Role.PERMISSION_CREATE_RECORDS);
        } else if (isCurrentlySynchronizing()) {
            AlertUtils.showSynchronizingAlert(this);
        } else {
            startActivity(RecordEditorActivity.getNewRecordIntent(this, getActiveFormID()));
        }
    }

    public void onDrawerSlide(View drawerView, float slideOffset) {
    }

    public void onDrawerOpened(View drawerView) {
        logDebugEvent("onDrawerOpened -> formPickerFragment.reloadForms()");
        this.mFormPickerFragment.reloadForms();
    }

    public void onDrawerClosed(View drawerView) {
        this.mFormPickerFragment.clearSearch();
    }

    public void onDrawerStateChanged(int newState) {
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new FormLoader(this, "_id", "name");
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        onFormsCursorLoaded(data);
    }

    public void onLoaderReset(Loader<Cursor> loader) {
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        int i = 0;
        boolean granted;
        int length;
        if ((requestCode & 64) == 64) {
            if (grantResults.length > 0) {
                granted = true;
                length = grantResults.length;
                while (i < length) {
                    if (grantResults[i] != 0) {
                        granted = false;
                    }
                    i++;
                }
                if (granted) {
                    Fragment frag = getSupportFragmentManager().findFragmentById(R.id.container);
                    if (frag != null && (frag instanceof MapViewFragment)) {
                        ((MapViewFragment) frag).setMyLocationEnabled(null);
                    }
                }
            }
        } else if ((requestCode & 16) == 16) {
            if (grantResults.length > 0) {
                granted = true;
                for (int result : grantResults) {
                    if (result != 0) {
                        granted = false;
                    }
                }
                if (granted) {
                    return;
                }
            }
            Toast.makeText(this, R.string.permissions_required, Toast.LENGTH_SHORT).show();
        }
    }

    private void onSynchronizeDataOptionSelected() {
        if (!isCurrentlySynchronizing()) {
            showSyncAnimation();
            SyncService.synchronizeData(this, Account.getActiveAccount());
        }
    }

    private void onAdvanceSearch() {
//        if (!isCurrentlySynchronizing()) {
//            showSyncAnimation();
//            SyncService.synchronizeData(this, Account.getActiveAccount());
//        }
       Intent intent = new Intent(DashboardActivity.this,Search_Activity.class);
        intent.putExtra("project_Id", mActiveFormID);
        intent.putExtra("account_Id", mAccountID);
       intent.putExtra("project_name", project_name);
        startActivity(intent);

    }

    private void showSyncAnimation() {
        if (this.mSyncActionView != null) {
            Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotate);
            rotation.setRepeatCount(-1);
            this.mSyncActionView.startAnimation(rotation);
        }
        ((TextView) findViewById(R.id.empty_text)).setText(R.string.synchronizing_forms_message);
        this.mFormPickerFragment.setSynchronizing(true);
    }

    private void hideSyncAnimation() {
        if (this.mSyncActionView != null) {
            this.mSyncActionView.clearAnimation();
        }
        ((TextView) findViewById(R.id.empty_text)).setText(R.string.missing_forms_message);
        this.mFormPickerFragment.setSynchronizing(false);
    }

    private void onToggleViewModeOptionSelected() {
        switch (this.mActiveViewMode) {
            case VIEW_MODE_MAP:
                setViewMode(VIEW_MODE_LIST);
                return;

            case VIEW_MODE_LIST:
                setViewMode(VIEW_MODE_MAP);
                return;
            default:
                return;
        }
    }


    private void onAppSettingsMenuItemSelected() {
        SettingsActivity.start(this);
    }


    private long getActiveFormID() {
        return this.mActiveFormID;
    }

    private void setViewMode(int mode) {
        setViewMode(mode, true);
    }

    private void setViewMode(int mode, boolean async) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        switch (mode) {
            case VIEW_MODE_MAP /*1*/:
                if (this.mViewToggleItem != null) {
                    this.mViewToggleItem.setIcon(R.drawable.ic_action_list);
                    this.mViewToggleItem.setTitle(R.string.list_view);
                }
                if (!(this.mCollectionView instanceof RecordMapFragment)) {
                    RecordMapFragment map = new RecordMapFragment();
                    ft.replace(R.id.container, map);
                    this.mCollectionView = map;
                    break;
                }
                break;
            case VIEW_MODE_LIST /*2*/:
                if (this.mViewToggleItem != null) {
                    this.mViewToggleItem.setIcon(R.drawable.ic_action_map);
                    this.mViewToggleItem.setTitle(R.string.map_view);
                }
                if (!(this.mCollectionView instanceof RecordListFragment)) {
                    RecordListFragment list = new RecordListFragment();
                    ft.replace(R.id.container, list);
                    this.mCollectionView = list;
                    break;
                }
                break;
        }
        try {
            ft.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
        }
        configureCollectionView();
        this.mActiveViewMode = mode;
        if (!async) {
            fm.executePendingTransactions();
        }
        PatronSettings.setActiveViewMode(this, mode);
    }

    private void removeCollectionView() {
        FragmentManager fm = getSupportFragmentManager();
        Fragment frag = fm.findFragmentById(R.id.container);
        if (frag != null) {
            FragmentTransaction ft = fm.beginTransaction();
            ft.remove(frag);
            ft.commitAllowingStateLoss();
        }
        this.mCollectionView = null;
    }

    private void configureCollectionView() {
        if (this.mCollectionView != null) {
            this.mCollectionView.setFormID(getActiveFormID());
        }
    }

    private void reloadForms() {
        logDebugEvent("reloadForms()");
        this.mFormPickerFragment.reloadForms();
        getSupportLoaderManager().restartLoader(100, null, this);
    }


    private void onFormsCursorLoaded(Cursor cursor) {
        logDebugEvent("onFormsCursorLoaded()");
        View empty = findViewById(R.id.empty);
        if (cursor == null || cursor.getCount() <= 0) {
            this.mToolbar.setLogo(null);
            setTitle(R.string.GIS);
            this.mFormDrawer.setDrawerLockMode(VIEW_MODE_MAP);
            if (empty != null) {
                empty.setVisibility(View.VISIBLE);
            }
            removeCollectionView();
            this.mMenuItemVisibility = false;
            Account account = Account.getActiveAccount();
            if (account == null || !account.getRole().isOwner()) {
                this.mToolbar.setNavigationIcon(null);
            } else {
                this.mToolbar.setNavigationIcon((int) R.drawable.ic_action_menu);
                this.mToolbar.setNavigationOnClickListener ( new OnClickListener () {
                    @Override
                    public void onClick(View view) {
                        DashboardActivity.this.mFormDrawer.openDrawer((int) GravityCompat.START);
                    }
                });
                if (!(this.mSyncService == null || this.mSyncService.isSynchronizing())) {
                    this.mFormDrawer.openDrawer((int) GravityCompat.START);
                    AppGalleryActivity.startForResult(this);
                }
            }
        } else {
            boolean found = false;
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                if (CursorUtils.getLong(cursor, "_id") == this.mActiveFormID) {
                    found = true;
                    break;
                }
            }
            if (found) {
                updateFormButton(CursorUtils.getString(cursor, "name"));
               // project_name=CursorUtils.getString(cursor, "name");
            } else {
                this.mFormDrawer.setDrawerLockMode(VIEW_MODE_LIST);
                this.mFormDrawer.openDrawer((int) GravityCompat.START);
            }
            if (empty != null) {
                empty.setVisibility(View.INVISIBLE);
            }
            setViewMode(this.mActiveViewMode);
            this.mMenuItemVisibility = true;
        }
        reloadMenuItemVisibility(this.mMenuItemVisibility);
    }

    private void reloadMenuItemVisibility(boolean menuItemVisibility) {
        if (this.mViewToggleItem != null) {
            this.mViewToggleItem.setVisible(menuItemVisibility);
            this.mViewToggleItem.setEnabled(menuItemVisibility);
            if (this.mActiveViewMode == VIEW_MODE_MAP) {
                this.mViewToggleItem.setIcon(R.drawable.ic_action_list);
                this.mViewToggleItem.setTitle(R.string.list_view);
            } else {
                this.mViewToggleItem.setIcon(R.drawable.ic_action_map);
                this.mViewToggleItem.setTitle(R.string.map_view);
            }
        }
        if (this.mSearchRecordsItem != null) {
            this.mSearchRecordsItem.setVisible(menuItemVisibility);
            this.mSearchRecordsItem.setEnabled(menuItemVisibility);
        }
    }

    private boolean isCurrentlySynchronizing() {
        return this.mSyncService != null && this.mSyncService.isSynchronizing();
    }

    private void updateFormButton(String appName) {
        setTitle(appName);
        project_name=appName;
        this.mToolbar.setNavigationIcon((int) R.drawable.ic_action_menu);
        this.mToolbar.setNavigationContentDescription((int) R.string.assigned_form);
        this.mToolbar.setNavigationOnClickListener ( new OnClickListener () {
            @Override
            public void onClick(View view) {
                DashboardActivity.this.mFormDrawer.openDrawer((int) GravityCompat.START);

            }
        } );
    }
}
