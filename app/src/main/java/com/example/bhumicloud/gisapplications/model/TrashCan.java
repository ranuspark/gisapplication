package com.example.bhumicloud.gisapplications.model;

import android.content.Context;
import com.example.bhumicloud.gisapplications.Activity.GarbageCleanupService;

public class TrashCan {
    public static final String COLUMN_PATH = "path";
    public static final String COLUMN_ROW_ID = "_id";
    public static final String TABLE_NAME = "trash_can";

    public static void empty(Context context) {
        GarbageCleanupService.start(context);
    }
}

