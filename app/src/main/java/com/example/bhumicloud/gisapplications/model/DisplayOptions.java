package com.example.bhumicloud.gisapplications.model;

import android.text.TextUtils;
import com.example.bhumicloud.gisapplications.util.DateUtils;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import com.example.bhumicloud.gisapplications.util.NumberUtils;
import java.util.HashMap;
import java.util.Map;

public class DisplayOptions {
    private static final String JSON_CURRENCY = "currency";
    private static final String JSON_STYLE = "style";
    private static final String STYLE_CURRENCY = "currency";
    private static final String STYLE_DATE = "date";
    private static final String STYLE_NUMBER = "number";
    private static final String STYLE_TEXT = "text";
    private String mCurrency;
    private String mStyle;

    public DisplayOptions(Map json) {
        this.mStyle = JSONUtils.getString(json, "style");
        this.mCurrency = JSONUtils.getString(json, "currency");
        if (TextUtils.isEmpty(this.mStyle)) {
            this.mStyle = "text";
        }
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = new HashMap();
        json.put("style", this.mStyle);
        json.put("currency", this.mCurrency);
        return json;
    }

    public String format(String value) {
        if (TextUtils.isEmpty(value)) {
            return value;
        }
        if (isNumber()) {
            return NumberUtils.formatLocalizedStringFromMachineString(value, true);
        }
        if (isCurrency()) {
            return NumberUtils.formatLocalizedCurrencyStringFromNumberString(value, this.mCurrency);
        }
        if (isDate()) {
            return DateUtils.formatLocalizedStringFromMachineString(value);
        }
        return isText() ? value : value;
    }

    private boolean isNumber() {
        return STYLE_NUMBER.equals(this.mStyle);
    }

    private boolean isText() {
        return "text".equals(this.mStyle);
    }

    private boolean isDate() {
        return STYLE_DATE.equals(this.mStyle);
    }

    private boolean isCurrency() {
        return "currency".equals(this.mStyle);
    }
}

