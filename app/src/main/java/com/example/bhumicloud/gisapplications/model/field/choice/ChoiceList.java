package com.example.bhumicloud.gisapplications.model.field.choice;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.dossier.PersistentObject;
import com.example.bhumicloud.gisapplications.dossier.PersistentObject.QueryField;
import com.example.bhumicloud.gisapplications.dossier.RowCache;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.util.CursorUtils;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChoiceList extends PersistentObject {
    @QueryField("account_id")
    public static final String COLUMN_ACCOUNT_ID = "account_id";
    public static final String COLUMN_CHOICES = "choices";
    @QueryField("description")
    public static final String COLUMN_DESCRIPTION = "description";
    @QueryField("name")
    public static final String COLUMN_NAME = "name";
    @QueryField("remote_id")
    public static final String COLUMN_REMOTE_ID = "remote_id";
    private static final ArrayList<String> QUERY_COLUMNS = PersistentObject.buildColumns(ChoiceList.class);
    public static final String TABLE_NAME = "FMChoiceLists";
    private static RowCache<ChoiceList> sRowCache = new RowCache<ChoiceList>(50) {
        protected ChoiceList create(Long row) {
            ChoiceList result = null;
            GISLogger.log("ChoiceList", "Row Cache Miss: %d", row);
            SQLiteDatabase db = GIS.getDatabase();
            String[] values = new String[]{String.valueOf(row)};
            Cursor c = CursorUtils.queryTableWithLargeColumn(db, ChoiceList.TABLE_NAME, ChoiceList.QUERY_COLUMNS, ChoiceList.COLUMN_CHOICES, "_id = ?", values, null);
            if (c != null) {
                c.moveToFirst();
                if (!c.isAfterLast()) {
                    result = new ChoiceList(c, db);
                }
                c.close();
            }
            return result;
        }
    };
    private long mAccountID;
    private ArrayList<ChoiceItem> mChoices;
    private List<Object> mChoicesJSON;
    private String mDescription;
    private String mName;
    private String mRemoteID;


    public static synchronized ChoiceList getChoiceList(long id) {
        ChoiceList choiceList;
        synchronized (ChoiceList.class) {
            choiceList = (ChoiceList) sRowCache.get(Long.valueOf(id));
        }
        return choiceList;
    }

    public static synchronized void clearCache() {
        synchronized (ChoiceList.class) {
            sRowCache.evictAll();
        }
    }

    public static ChoiceList getChoiceList(Account account, String remoteID) {
        return getChoiceList(account, remoteID, GIS.getDatabase());
    }

    public static ChoiceList getChoiceList(Account account, String remoteID, SQLiteDatabase db) {
        ChoiceList choiceList = null;
        List<String> predicates = new ArrayList();
        List<String> valuesList = new ArrayList();
        if (account != null) {
            predicates.add("account_id = ?");
            valuesList.add(String.valueOf(account.getRowID()));
        }
        if (!TextUtils.isEmpty(remoteID)) {
            predicates.add("remote_id = ?");
            valuesList.add(remoteID);
        }
        String[] columns = new String[]{"_id"};
        SQLiteDatabase sQLiteDatabase = db;
        Cursor c = sQLiteDatabase.query(TABLE_NAME, columns, TextUtils.join(" AND ", predicates), (String[]) valuesList.toArray(new String[valuesList.size()]), null, null, null);
        if (c != null) {
            c.moveToFirst();
            if (!c.isAfterLast()) {
                choiceList = getChoiceList(c.getLong(0));
            }
            c.close();
        }
        return choiceList;
    }

    public static void truncate(ArrayList<String> excludedRemoteIDs, Account account) {
        SQLiteDatabase db = GIS.getDatabase();
        List<String> predicates = new ArrayList();
        List<String> paramsList = new ArrayList();
        if (account != null) {
            predicates.add("account_id = ?");
            paramsList.add(String.valueOf(account.getRowID()));
        }
        if (excludedRemoteIDs != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("remote_id NOT IN (");
            for (int i = 0; i < excludedRemoteIDs.size(); i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append("'").append((String) excludedRemoteIDs.get(i)).append("'");
            }
            sb.append(")");
            predicates.add(sb.toString());
        }
        db.delete(TABLE_NAME, TextUtils.join(" AND ", predicates), (String[]) paramsList.toArray(new String[paramsList.size()]));
    }

    public ChoiceList(Map jsonRepresentation, Account account) {
        setAttributesFromJSON(jsonRepresentation);
        this.mAccountID = account.getRowID();
    }

    public ChoiceList(Cursor cursor, SQLiteDatabase db) {
        super(cursor);
        this.mRemoteID = cursor.getString(cursor.getColumnIndex("remote_id"));
        this.mAccountID = cursor.getLong(cursor.getColumnIndex("account_id"));
        this.mName = cursor.getString(cursor.getColumnIndex("name"));
        this.mDescription = cursor.getString(cursor.getColumnIndex("description"));
        try {
            this.mChoicesJSON = JSONUtils.arrayFromJSON(CursorUtils.fetchLargeColumn(db, TABLE_NAME, cursor, COLUMN_CHOICES));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private ChoiceList(Parcel source) {
        super(source);
        boolean isChoicesJSON = true;
        this.mRemoteID = source.readString();
        this.mAccountID = source.readLong();
        this.mName = source.readString();
        this.mDescription = source.readString();
        if (source.readInt() != 1) {
            isChoicesJSON = false;
        }
        if (isChoicesJSON) {
            this.mChoicesJSON = new ArrayList();
            source.readList(this.mChoicesJSON, null);
        }
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(getRemoteID());
        dest.writeLong(this.mAccountID);
        dest.writeString(getName());
        dest.writeString(getDescription());
        if (this.mChoicesJSON != null) {
            dest.writeInt(1);
            dest.writeList(this.mChoicesJSON);
            return;
        }
        dest.writeInt(0);
    }

    public String getRemoteID() {
        return this.mRemoteID;
    }

    public String getName() {
        return this.mName;
    }

    public String getDescription() {
        return this.mDescription;
    }

    public ArrayList<ChoiceItem> getChoices() {
        if (this.mChoices != null) {
            return this.mChoices;
        }
        this.mChoices = new ArrayList();
        if (!(this.mChoicesJSON == null || this.mChoicesJSON.isEmpty())) {
            for (Object obj : this.mChoicesJSON) {
                HashMap<String, Object> itemJSON = JSONUtils.getHashMap(obj);
                if (!(itemJSON == null || itemJSON.isEmpty())) {
                    this.mChoices.add(new ChoiceItem(itemJSON));
                }
            }
        }
        return this.mChoices;
    }

    public void setAttributesFromJSON(Map json) {
        this.mName = JSONUtils.getString(json, "name");
        this.mDescription = JSONUtils.getString(json, "description");
        this.mChoicesJSON = JSONUtils.getArrayList(json, COLUMN_CHOICES);
    }

    protected ContentValues getContentValues() {
        ContentValues values = super.getContentValues();
        values.put("remote_id", getRemoteID());
        values.put("account_id", Long.valueOf(this.mAccountID));
        values.put("name", getName());
        values.put("description", getDescription());
        if (!(this.mChoicesJSON == null || this.mChoicesJSON.isEmpty())) {
            String choicesAsJSONString = JSONUtils.toJSONString(this.mChoicesJSON);
            if (choicesAsJSONString != null) {
                values.put(COLUMN_CHOICES, choicesAsJSONString);
            }
        }
        return values;
    }

    protected String getTableName() {
        return TABLE_NAME;
    }
}
