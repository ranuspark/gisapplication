package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import com.example.bhumicloud.gisapplications.model.field.choice.ChoiceElement;

public class ChoiceElementView extends ElementView<ChoiceElement> {
    private ChoiceElementViewListener mListener;

    public interface ChoiceElementViewListener {
        void onShowChoiceDialog(ChoiceElement choiceElement);
    }

    public ChoiceElementView(Context context, ChoiceElement element, boolean viewOnly) {
        super(context, element, viewOnly);
    }

    public void setListener(ChoiceElementViewListener listener) {
        this.mListener = listener;
    }

    protected void onFieldViewClicked() {
        super.onFieldViewClicked();
        if (this.mListener != null) {
            this.mListener.onShowChoiceDialog((ChoiceElement) getElement());
        }
    }
}
