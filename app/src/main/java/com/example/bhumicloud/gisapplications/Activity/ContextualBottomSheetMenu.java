package com.example.bhumicloud.gisapplications.Activity;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Role;

public abstract class ContextualBottomSheetMenu extends BottomSheetDialogFragment {
    public static final String ARG_TITLE = "Gis:arg:contextual_menu_title";

    class C10511 implements OnClickListener {

        class C10501 implements DialogInterface.OnClickListener {
            C10501() {
            }

            public void onClick(DialogInterface dialog, int which) {
                boolean values = true;
                boolean location = true;
                switch (which) {
                    case 1:
                        location = false;
                        break;
                    case 2:
                        values = false;
                        break;
                }
                ContextualBottomSheetMenu.this.duplicate(location, values);
                if (ContextualBottomSheetMenu.this.isAdded()) {
                    ContextualBottomSheetMenu.this.dismiss();
                }
            }
        }

        C10511() {
        }

        public void onClick(View v) {
            Builder b = new Builder (ContextualBottomSheetMenu.this.getContext());
            b.setTitle(R.string.duplicate);
            b.setItems(R.array.duplication_options, new C10501());
            b.create().show();
        }
    }

    class C10522 implements OnClickListener {
        C10522() {
        }

        public void onClick(View v) {
            ContextualBottomSheetMenu.this.showOnMap();
            if (ContextualBottomSheetMenu.this.isAdded()) {
                ContextualBottomSheetMenu.this.dismiss();
            }
        }
    }

    class C10533 implements OnClickListener {
        C10533() {
        }

        public void onClick(View v) {
            ContextualBottomSheetMenu.this.drivingDirections();
            if (ContextualBottomSheetMenu.this.isAdded()) {
                ContextualBottomSheetMenu.this.dismiss();
            }
        }
    }

    class C10544 implements OnClickListener {
        C10544() {
        }

        public void onClick(View v) {
            ContextualBottomSheetMenu.this.deleteItem();
            if (ContextualBottomSheetMenu.this.isAdded()) {
                ContextualBottomSheetMenu.this.dismiss();
            }
        }
    }

    protected abstract void deleteItem();

    protected abstract void drivingDirections();

    protected abstract void duplicate(boolean z, boolean z2);

    protected abstract String getTitleText();

    protected abstract void showOnMap();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState) {
        View view = inflater.inflate(R.layout.dialog_contextual_bottom_sheet_menu, container, false);
     //   LinearLayout duplication = (LinearLayout) view.findViewById(R.id.layout_duplicate);
     //   LinearLayout showOnMap = (LinearLayout) view.findViewById(R.id.layout_show_on_map);
     //   LinearLayout drivingDirections = (LinearLayout) view.findViewById(R.id.layout_driving_directions);
        LinearLayout deleteItem = (LinearLayout) view.findViewById(R.id.layout_delete_item);
        ((TextView) view.findViewById(R.id.item_title)).setText(getTitleText());
        Role role = Account.getActiveAccount(getContext()).getRole();
        if (role.canCreateRecords()) {
       //     duplication.setVisibility(View.VISIBLE);
       //     duplication.setOnClickListener(new C10511());
        } else {
        //    duplication.setVisibility(View.GONE);
        }
        //showOnMap.setOnClickListener(new C10522());
        //drivingDirections.setOnClickListener(new C10533());
        if (role.canUpdateRecords()) {
            deleteItem.setVisibility(View.VISIBLE);
            deleteItem.setOnClickListener(new C10544());
        } else {
            deleteItem.setVisibility(View.GONE);
        }
        return view;
    }
}
