package com.example.bhumicloud.gisapplications.model.field.attachment.photo;

import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;

import java.util.Map;

public class PhotoItemValue extends MediaItemValue {
    private static final String JSON_PHOTO_ID = "photo_id";

    public PhotoItemValue(Attachment attachment) {
        super(attachment);
    }

    public PhotoItemValue(Map attributes) {
        super(attributes);
    }

    protected String getIDFieldTitle() {
        return JSON_PHOTO_ID;
    }

    protected boolean canEqual(Object object) {
        return object instanceof PhotoItemValue;
    }
}
