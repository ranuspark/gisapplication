package com.example.bhumicloud.gisapplications.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.apiLayer.Client;
import com.example.bhumicloud.gisapplications.model.layer.MBTilesMapLayer;
import com.example.bhumicloud.gisapplications.model.layer.MapLayer;
import com.google.android.gms.maps.model.CameraPosition;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.GoogleMapUtils;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PatronSettings {
    public static final int BASEMAP_GOOGLE_AERIAL = 1;
    public static final int BASEMAP_GOOGLE_HYBRID = 2;
    public static final int BASEMAP_GOOGLE_STREETS = 0;
    public static final int BASEMAP_GOOGLE_TERRAIN = 3;
    public static final int BASEMAP_NONE = 4;
    public static final String KEY_ACCOUNT_ACTIVE_FORM = "account:active_form";
    public static final String KEY_ACCOUNT_ACTIVE_PROJECT = "account:active_project";
    public static final String KEY_ACCOUNT_ACTIVE_VIEW_MODE = "account:active_view_mode";
    public static final String KEY_ACCOUNT_MAP_CAMERA_POSITION = "account:map_camera_position";
    public static final String KEY_API_HOSTNAME = "api:hostname";
    public static final String KEY_API_HTTPS = "api:https";
    public static final String KEY_APP_ACTIVE_ACCOUNT = "app:active_account";
    public static final String KEY_APP_IS_FIRST_LAUNCH = "app:is_first_launch";
    public static final String KEY_LIST_ASCENDING = "list:ascending";
    public static final String KEY_LIST_MAP_FILTER = "list:map_filter";
    public static final String KEY_LIST_SORT_BY = "list:sort_by";
    public static final String KEY_LOCATION_DEBUGGER_UNITS = "locationdebugger:units";
    public static final String KEY_MAP_BASEMAP = "map:basemap";
    public static final String KEY_MAP_LAYERS = "map:layers";
    public static final String KEY_PHOTOS_QUALITY = "photos:quality";
    public static final String KEY_PHOTOS_SAVE_TO_GALLERY = "photos:save_to_gallery";
    public static final String KEY_SYNC_AUDIO_FILES = "sync:audio_files";
    public static final String KEY_SYNC_AUTO_POST_LAUNCH = "sync:auto_sync_post_launch";
    public static final String KEY_SYNC_AUTO_POST_RECORD_EDITS = "sync:auto_sync_post_record_edits";
    public static final String KEY_SYNC_LAST_RECORD_DOWNLOAD_DATES = "sync:record_download_dates";
    public static final String KEY_SYNC_PHOTOS = "sync:photos";
    public static final String KEY_SYNC_SIGNATURES = "sync:signatures";
    public static final String KEY_SYNC_VIDEOS = "sync:videos";
    public static final String KEY_VIDEOS_QUALITY = "videos:quality";
    public static final String KEY_VIDEOS_SAVE_TO_GALLERY = "videos:save_to_gallery";
    public static final int PHOTO_QUALITY_HIGH = 1;
    public static final int PHOTO_QUALITY_LOW = 3;
    public static final int PHOTO_QUALITY_MEDIUM = 2;
    public static final int PHOTO_QUALITY_NATIVE = 0;
    public static final int SORT_MODE_CREATED_AT = 2;
    public static final int SORT_MODE_STATUS = 1;
    public static final int SORT_MODE_TITLE = 0;
    public static final int SORT_MODE_UPDATED_AT = 3;
    public static final int SYNC_MODE_ALWAYS = 0;
    public static final int SYNC_MODE_NEVER = 2;
    public static final int SYNC_MODE_WIFI_ONLY = 1;
    public static final int UNITS_METRIC = 0;

    public static boolean isFirstLaunch(Context context) {
        return getGlobalPreferences(context).getBoolean(KEY_APP_IS_FIRST_LAUNCH, true);
    }

    public static void setIsFirstLaunch(Context context, boolean isFirstLaunch) {
        getGlobalPreferences(context).edit().putBoolean(KEY_APP_IS_FIRST_LAUNCH, isFirstLaunch).apply();
    }

    public static Account getActiveAccount(Context context) {
        long accountID = getActiveAccountID(context);
        return accountID != -1 ? Account.getAccount(accountID) : null;
    }

    public static void setActiveAccount(Context context, Account account) {
        if (account != null) {
            setActiveAccountID(context, account.getRowID());
        } else {
            setActiveAccountID(context, -1);
        }
    }

    public static long getActiveAccountID(Context context) {
        return getGlobalPreferences(context).getLong(KEY_APP_ACTIVE_ACCOUNT, -1);
    }

    public static void setActiveAccountID(Context context, long accountID) {
        getGlobalPreferences(context).edit().putLong(KEY_APP_ACTIVE_ACCOUNT, accountID).apply();
    }

    public static String getHostname(Context context) {
        return getGlobalPreferences(context).getString(KEY_API_HOSTNAME, Client.DEFAULT_HOSTNAME);
    }

    public static void setHostname(Context context, String host) {
        if (TextUtils.isEmpty(host)) {
            getGlobalPreferences(context).edit().remove(KEY_API_HOSTNAME).apply();
        } else {
            getGlobalPreferences(context).edit().putString(KEY_API_HOSTNAME, host).apply();
        }
    }

    //TODO:Make True In Release - For Testing Set to the FALSE,
    public static boolean isHttpsEnabled(Context context) {
        return getGlobalPreferences(context).getBoolean(KEY_API_HTTPS, false);
    }

    public static void setHttpsEnabled(Context context, boolean enableHttps) {
        getGlobalPreferences(context).edit().putBoolean(KEY_API_HTTPS, enableHttps).apply();
    }

    public static SharedPreferences getAccountPreferences(Context context, Account account) {
        return context.getSharedPreferences(getAccountPreferencesName(account), SYNC_MODE_ALWAYS);
    }

    public static String getAccountPreferencesName(Account account) {
        return account.getUserID() + "_" +  account.getContextID();
    }

    public static long getActiveProjectID(Context context) {
        return getAccountPreferences(context).getLong(KEY_ACCOUNT_ACTIVE_PROJECT, -1);
    }

    public static void setActiveProjectID(Context context, long projectID) {
        getAccountPreferences(context).edit().putLong(KEY_ACCOUNT_ACTIVE_PROJECT, projectID).apply();
    }

    public static CameraPosition getMapCameraPosition(Context context) {
        CameraPosition cameraPosition = null;
        String jsonString = getAccountPreferences(context).getString(KEY_ACCOUNT_MAP_CAMERA_POSITION, null);
        if (!TextUtils.isEmpty(jsonString)) {
            try {
                cameraPosition = GoogleMapUtils.parseCameraPosition(JSONUtils.objectFromJSON(jsonString));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return cameraPosition;
    }

    public static void setMapCameraPosition(Context context, CameraPosition position) {
        if (position != null) {
            getAccountPreferences(context).edit().putString(KEY_ACCOUNT_MAP_CAMERA_POSITION, JSONUtils.toJSONString(GoogleMapUtils.toJSON(position))).apply();
        } else {
            getAccountPreferences(context).edit().remove(KEY_ACCOUNT_MAP_CAMERA_POSITION).apply();
        }
    }

    public static int getActiveViewMode(Context context, int fallback) {
        return getAccountPreferences(context).getInt(KEY_ACCOUNT_ACTIVE_VIEW_MODE, fallback);
    }

    public static void setActiveViewMode(Context context, int viewMode) {
        getAccountPreferences(context).edit().putInt(KEY_ACCOUNT_ACTIVE_VIEW_MODE, viewMode).apply();
    }

    public static long getActiveFormID(Context context) {
        return getAccountPreferences(context).getLong(KEY_ACCOUNT_ACTIVE_FORM, -1);
    }

    public static void setActiveFormID(Context context, long id) {
        getAccountPreferences(context).edit().putLong(KEY_ACCOUNT_ACTIVE_FORM, id).apply();
    }

    public static Date getLastRecordDownloadDate(Context context, Form form) {
        long ms = JSONUtils.getLong(getLastRecordDownloadDates(context, form.getAccount()), form.getRemoteID(), -1);
        return ms >= 0 ? new Date(ms) : null;
    }

    public static HashMap<String, Object> getLastRecordDownloadDates(Context context, Account account) {
        try {
            return JSONUtils.objectFromJSON(getAccountPreferences(context, account).getString(KEY_SYNC_LAST_RECORD_DOWNLOAD_DATES, "{}"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void setLastRecordDownloadDate(Context context, Form form, Date date) {
        Account account = form.getAccount();
        HashMap<String, Object> dates = getLastRecordDownloadDates(context, account);
        String formID = form.getRemoteID();
        if (date != null) {
            dates.put(formID, Long.valueOf(date.getTime()));
        } else {
            dates.remove(formID);
        }
        setLastRecordDownloadDates(context, account, dates);
    }

    public static void resetRecordDownloadDates(Context context) {
        getAccountPreferences(context).edit().putString(KEY_SYNC_LAST_RECORD_DOWNLOAD_DATES, JSONUtils.toJSONString(new HashMap())).apply();
    }

    public static Set<Long> getActiveMapLayers(Context context) {
        return getActiveMapLayers(getAccountPreferences(context) );
    }

    public static Set<Long> getActiveMapLayers(Context context, Account account) {
        return getActiveMapLayers(getAccountPreferences(context, account) );
    }

    private static Set<Long> getActiveMapLayers(SharedPreferences accountPreferences) {
        Throwable e;
        Set set = new HashSet();
        try {
            List array = JSONUtils.arrayFromJSON(accountPreferences.getString(KEY_MAP_LAYERS, "[]"));
            if (array != null) {
                for (int i = 0; i < array.size(); i++) {
                    long id = JSONUtils.getLong(array, i);
                    if (id > 0) {
                        MapLayer mapLayer = MapLayer.getMapLayer(id);
                        if (!(mapLayer instanceof MBTilesMapLayer) || ((MBTilesMapLayer) mapLayer).isAvailable()) {
                            set.add(Long.valueOf(id));
                        }
                    }
                }
                if (set.size() != array.size()) {
                    setActiveMapLayers(accountPreferences, set);
                }
            }
        } catch (IOException e2) {
            e = e2;
            GISLogger.log(e);
            return set;
        } catch (NullPointerException e3) {
            e = e3;
            GISLogger.log(e);
            return set;
        }
        return set;
    }


    public static void setActiveMapLayers(Context context, Set<Long> layers) {
        setActiveMapLayers(getAccountPreferences(context), (Set) layers);
    }

    public static void setActiveMapLayers(Context context, Account account, Set<Long> layers) {
        setActiveMapLayers(getAccountPreferences(context, account), (Set) layers);
    }

    public static int getBasemap(Context context) {
        return getAccountPreferences(context).getInt(KEY_MAP_BASEMAP, 0);
    }

    public static void setBasemap(Context context, int basemapPosition) {
        getAccountPreferences(context).edit().putInt(KEY_MAP_BASEMAP, basemapPosition).apply();
    }

    public static boolean isMapBoundsFilterEnabled(Context context) {
        return getAccountPreferences(context).getBoolean(KEY_LIST_MAP_FILTER, false);
    }

    public static int getSortingMode(Context context) {
        return getAccountPreferences(context).getInt(KEY_LIST_SORT_BY, 3);
    }

    public static boolean isSortAscendingEnabled(Context context) {
        return getAccountPreferences(context).getBoolean(KEY_LIST_ASCENDING, false);
    }

    public static boolean isAutoSyncPostLaunchEnabled(Context context) {
        return getAccountPreferences(context).getBoolean(KEY_SYNC_AUTO_POST_LAUNCH, false);
    }

    public static boolean isAutoSyncEnabled(Context context) {
        return getAccountPreferences(context).getBoolean(KEY_SYNC_AUTO_POST_RECORD_EDITS, false);
    }

    public static int getPhotosSyncMode(Context context) {
        return getAccountPreferences(context).getInt(KEY_SYNC_PHOTOS, 0);
    }

    public static int getVideosSyncMode(Context context) {
        return getAccountPreferences(context).getInt(KEY_SYNC_VIDEOS, 1);
    }

    public static int getAudioFilesSyncMode(Context context) {
        return getAccountPreferences(context).getInt(KEY_SYNC_AUDIO_FILES, 0);
    }

    public static int getSignaturesSyncMode(Context context) {
        return getAccountPreferences(context).getInt(KEY_SYNC_SIGNATURES, 0);
    }

    public static int getPhotoCaptureQuality(Context context) {
        return getAccountPreferences(context).getInt(KEY_PHOTOS_QUALITY, 1);
    }

    public static boolean isSavePhotosToGallery(Context context) {
        return getAccountPreferences(context).getBoolean(KEY_PHOTOS_SAVE_TO_GALLERY, false);
    }

    public static boolean isSaveVideosToGallery(Context context) {
        return getAccountPreferences(context).getBoolean(KEY_VIDEOS_SAVE_TO_GALLERY, false);
    }

    public static int getLocationDebuggerUnits(Context context) {
        return getAccountPreferences(context).getInt(KEY_LOCATION_DEBUGGER_UNITS, 0);
    }

    public static void setLocationDebuggerUnits(Context context, int unitsValue) {
        getAccountPreferences(context).edit().putInt(KEY_LOCATION_DEBUGGER_UNITS, unitsValue).apply();
    }

    private static SharedPreferences getGlobalPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
    }

    private static SharedPreferences getAccountPreferences(Context context) {
        return getAccountPreferences(context, Account.getActiveAccount(context));
    }

    private static void setActiveMapLayers(SharedPreferences accountPreferences, Set<Long> layers) {
        ArrayList<Long> array = new ArrayList();
        for (Long longValue : layers) {
            array.add(Long.valueOf(longValue.longValue()));
        }
        accountPreferences.edit().putString(KEY_MAP_LAYERS, JSONUtils.toJSONString(array)).apply();
    }

    private static void setLastRecordDownloadDates(Context context, Account account, Map dates) {
        getAccountPreferences(context, account).edit().putString(KEY_SYNC_LAST_RECORD_DOWNLOAD_DATES, JSONUtils.toJSONString(dates)).apply();
    }
}

