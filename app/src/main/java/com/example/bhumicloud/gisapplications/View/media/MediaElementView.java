package com.example.bhumicloud.gisapplications.View.media;

import android.app.AlertDialog.Builder;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.apiLayer.exception.ResourceNotFoundException;
import com.example.bhumicloud.gisapplications.Activity.AttachmentDownloadService;
import com.example.bhumicloud.gisapplications.Activity.AttachmentDownloadService.AttachmentDownloadServiceBinder;
import com.example.bhumicloud.gisapplications.Activity.AttachmentDownloadService.Observer;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment.Type;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaElement;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaValue;
import com.example.bhumicloud.gisapplications.model.validation.error.FieldLengthValidationError;
import com.example.bhumicloud.gisapplications.util.AlertUtils;
import com.example.bhumicloud.gisapplications.util.IntentUtils;
import com.example.bhumicloud.gisapplications.util.NetworkUtils;
import com.example.bhumicloud.gisapplications.View.ElementView;
import com.example.bhumicloud.gisapplications.View.FlowLayout;
import com.example.bhumicloud.gisapplications.View.FlowLayout.LayoutParams;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public abstract class MediaElementView extends ElementView<Element> implements ServiceConnection, Observer {
    private AttachmentDownloadService mDownloadService;
    private FlowLayout mFlowLayout;
    protected FormValue mFormValue;
    private MediaElementViewListener mListener;
    private final Map<String, ThumbnailView> mThumbnailIndex = new HashMap ();

    public interface MediaElementViewListener {
        void onEditCaptionButtonClicked(Element element, MediaItemValue mediaItemValue);

        void onNewMediaButtonClicked(Element element);

        void onRemoveMediaButtonClicked(Element element, MediaItemValue mediaItemValue);

        void onSelectMediaButtonClicked(Element element);

        void onShareMediaButtonClicked(MediaItemValue mediaItemValue, Type type);
    }

    private static class ThumbnailView extends FrameLayout {
        private final MediaImageView mImageView;
        private final MediaItemValue mMediaItemValue;
        private final ProgressBar mProgressBar;

        public ThumbnailView(Context context, int layout, MediaItemValue mediaItemValue) {
            super(context);
            this.mMediaItemValue = mediaItemValue;
            View v = LayoutInflater.from(context).inflate(layout, this, true);
            this.mImageView = (MediaImageView) v.findViewById(R.id.thumbnail_image);
            this.mImageView.setTag(this.mMediaItemValue.getMediaID());
            this.mProgressBar = (ProgressBar) v.findViewById(R.id.progress);
            reloadThumbnail();
        }

        public MediaItemValue getMediaItemValue() {
            return this.mMediaItemValue;
        }

        public void showDownloadProgress() {
            if (this.mProgressBar.getVisibility() != View.VISIBLE) {
                this.mProgressBar.setVisibility(View.VISIBLE);
            }
        }

        public void setDownloadProgress(float progress) {
            this.mProgressBar.setProgress((int) progress);
        }

        public void hideDownloadProgress() {
            if (this.mProgressBar.getVisibility() != View.INVISIBLE) {
                this.mProgressBar.setVisibility(View.INVISIBLE);
            }
        }

        public void reloadThumbnail() {
            if (this.mMediaItemValue != null) {
                Attachment attachment = this.mMediaItemValue.getAttachment();
                if (attachment == null) {
                    this.mImageView.setBackgroundResource(R.drawable.bg_media_download);
                } else if (attachment.isFileOneAvailable()) {
                    this.mImageView.setBackgroundResource(0);
                    this.mImageView.setImage(attachment);
                } else if (attachment.isUploaded()) {
                    this.mImageView.setBackgroundResource(R.drawable.bg_media_download);
                } else {
                    this.mImageView.setBackgroundResource(R.drawable.bg_media_missing);
                }
            }
        }
    }

    protected abstract int getNewMediaDrawable();

    protected abstract int getObserveActionResource();

    protected abstract int getSelectMediaDrawable();

    protected abstract int getThumbnailLayoutResource();

    protected abstract Type getType();

    public MediaElementView(Context context, Element element, boolean viewOnly) {
        super(context, element, viewOnly);
        AttachmentDownloadService.bind(context, this);
    }

    public void onServiceConnected(ComponentName name, IBinder service) {
        if (service instanceof AttachmentDownloadServiceBinder) {
            this.mDownloadService = ((AttachmentDownloadServiceBinder) service).getService();
            this.mDownloadService.registerObserver(this);
        }
    }

    public void onServiceDisconnected(ComponentName name) {
        this.mDownloadService = null;
    }

    public void onAttachmentDownloadStarted(String attachment) {
        ThumbnailView thumb = getThumbnail(attachment);
        if (thumb != null) {
            thumb.setClickable(false);
            thumb.showDownloadProgress();
        }
    }

    public void onAttachmentDownloadProgress(String attachment, int progress) {
        ThumbnailView thumb = getThumbnail(attachment);
        if (thumb != null) {
            thumb.setClickable(false);
            thumb.showDownloadProgress();
            thumb.setDownloadProgress((float) progress);
        }
    }

    public void onAttachmentDownloadFinished(String attachment) {
        ThumbnailView thumb = getThumbnail(attachment);
        if (thumb != null) {
            thumb.setClickable(true);
            thumb.hideDownloadProgress();
            thumb.setDownloadProgress(0.0f);
            thumb.reloadThumbnail();
        }
    }

    public void onAttachmentDownloadFailed(String attachment, Exception e) {
        ThumbnailView thumb = getThumbnail(attachment);
        if (thumb != null) {
            thumb.setClickable(true);
            thumb.hideDownloadProgress();
            thumb.setDownloadProgress(0.0f);
            if (e instanceof ResourceNotFoundException) {
                Builder b = new Builder (getContext());
                b.setTitle(R.string.unavailable_title);
                b.setMessage(R.string.unavailable_message);
                b.show();
                return;
            }
            AlertUtils.showExceptionAlert(getContext(), e);
        }
    }

    public void setListener(MediaElementViewListener listener) {
        this.mListener = listener;
    }

    public void setValue(FormValue value) {
        this.mFormValue = value;
        clearThumbnails();
        if (this.mFormValue != null) {
            for (MediaItemValue addMediaToView : ((MediaValue) this.mFormValue).getMediaItems()) {
                addMediaToView(addMediaToView);
            }
        }
        if (this.mFlowLayout.getChildCount() > 0) {
            this.mFlowLayout.setVisibility(View.VISIBLE);
        } else {
            this.mFlowLayout.setVisibility(View.GONE);
        }
    }

    public void onDestroy(Context context) {
        super.onDestroy(context);
        this.mListener = null;
        if (this.mDownloadService != null) {
            this.mDownloadService.unregisterObserver(this);
        }
        AttachmentDownloadService.unbind(getContext(), this);
        clearThumbnails();
    }

    public void update() {
        int i = 0;
        super.update();
        if (isReadOnly()) {
            findViewById(R.id.add_media_buttons).setVisibility(View.GONE);
        } else {
            findViewById(R.id.add_media_buttons).setVisibility(View.VISIBLE);
        }
        ImageButton addNewMediaButton = (ImageButton) findViewById(R.id.media_create_new_btn);
        if (addNewMediaButton != null) {
            int i2;
            if (isCaptureEnabled()) {
                i2 = 0;
            } else {
                i2 = 8;
            }
            addNewMediaButton.setVisibility(i2);
        }
        ImageButton addSelectMediaButton = (ImageButton) findViewById(R.id.media_select_existing_btn);
        if (addSelectMediaButton != null) {
            if (!isGalleryEnabled()) {
                i = 8;
            }
            addSelectMediaButton.setVisibility(i);
        }
    }

    protected void initialize() {
        super.initialize();
        this.mFlowLayout = (FlowLayout) findViewById(R.id.media_container);
        ImageButton addNewMediaButton = (ImageButton) findViewById(R.id.media_create_new_btn);
        addNewMediaButton.setTag(getElement().getDataName() + ":add");
        addNewMediaButton.setImageResource(getNewMediaDrawable());
        addNewMediaButton.setOnClickListener ( new OnClickListener () {
            @Override
            public void onClick(View v) {
                MediaElementView.this.onAddMediaButtonClicked(true);
            }
        } );
        ImageButton addSelectMediaButton = (ImageButton) findViewById(R.id.media_select_existing_btn);
        addSelectMediaButton.setTag(getElement().getDataName() + ":select");
        addSelectMediaButton.setImageResource(getSelectMediaDrawable());
        addSelectMediaButton.setOnClickListener ( new OnClickListener () {
            @Override
            public void onClick(View v) {
                MediaElementView.this.onAddMediaButtonClicked(false);
            }
        } );
    }

    protected int getElementViewLayout() {
        return R.layout.view_media_element;
    }

    private boolean isGalleryEnabled() {
        MediaElement mediaElement = (MediaElement) getElement();
        if (mediaElement.getOverrideMediaGalleryEnabled() != null) {
            return mediaElement.isMediaGalleryEnabled();
        }
        return getRecord() != null ? getRecord().getForm().isMediaGalleryEnabled().booleanValue() : true;
    }

    private boolean isCaptureEnabled() {
        MediaElement mediaElement = (MediaElement) getElement();
        if (mediaElement.getOverrideMediaCaptureEnabled() != null) {
            return mediaElement.isMediaCaptureEnabled();
        }
        return getRecord() != null ? getRecord().getForm().isMediaCaptureEnabled().booleanValue() : true;
    }

    private ThumbnailView getThumbnail(String attachment) {
        return (ThumbnailView) this.mThumbnailIndex.get(attachment);
    }

    protected void clearThumbnails() {
        this.mThumbnailIndex.clear();
        this.mFlowLayout.removeAllViews();
    }

    private void onAddMediaButtonClicked(boolean isNew) {
        onFieldViewClicked();
        Element element = getElement();
        if (element.hasMaxLength() && this.mFormValue != null && this.mFormValue.length() >= element.getMaxLength()) {
            Toast.makeText(getContext(), new FieldLengthValidationError(element).getMessage(), Toast.LENGTH_SHORT).show();
        } else if (this.mListener == null) {
        } else {
            if (isNew) {
                this.mListener.onNewMediaButtonClicked(element);
            } else {
                this.mListener.onSelectMediaButtonClicked(element);
            }
        }
    }

    protected void addMediaToView(MediaItemValue media) {
        ThumbnailView thumb = new ThumbnailView(getContext(), getThumbnailLayoutResource(), media);
        thumb.setOnClickListener ( new OnClickListener () {
            @Override
            public void onClick(View v) {
                MediaElementView.this.onThumbnailClicked((ThumbnailView) v);
            }
        } );
        String mediaID = media.getMediaID();
        this.mThumbnailIndex.put(mediaID, thumb);
        if (this.mDownloadService != null) {
            int p = this.mDownloadService.getDownloadProgress(mediaID);
            if (p >= 0) {
                thumb.showDownloadProgress();
                thumb.setDownloadProgress((float) p);
                thumb.setClickable(false);
            } else {
                thumb.hideDownloadProgress();
                thumb.setDownloadProgress(0.0f);
                thumb.setClickable(true);
            }
        }
        int size = getContext().getResources().getDimensionPixelSize(R.dimen.thumb_size);
        this.mFlowLayout.addView(thumb, new FrameLayout.LayoutParams (size, size));
    }

    private void onThumbnailClicked(ThumbnailView thumbnail) {
        final MediaItemValue media = thumbnail.getMediaItemValue();
        final ArrayList<String> optionsList = new ArrayList ();
        Attachment attachment = media.getAttachment();
        Resources res = getResources();
        final String play = res.getString(getObserveActionResource());
        final String share = res.getString(R.string.share);
        final String download = res.getString(R.string.media_download);
        final String edit_caption = res.getString(R.string.edit_caption);
        final String remove = res.getString(R.string.remove);
        if (attachment == null || !attachment.isFileOneAvailable()) {
            optionsList.add(download);
        } else {
            optionsList.add(play);
            optionsList.add(share);
        }
        if (!isReadOnly()) {
            optionsList.add(edit_caption);
            optionsList.add(remove);
        }
        String[] options = (String[]) optionsList.toArray(new String[optionsList.size()]);
        Builder b = new Builder (getContext());
        b.setTitle("Select Action");
        b.setItems(options, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String selection = (String) optionsList.get(which);
                if (selection.equals(download)) {
                    MediaElementView.this.onDownloadMediaItemClicked(media);
                } else if (selection.equals(edit_caption)) {
                    MediaElementView.this.onEditCaptionItemClicked(media);
                } else if (selection.equals(remove)) {
                    MediaElementView.this.onRemoveMediaItemClicked(media);
                } else if (selection.equals(play)) {
                    MediaElementView.this.onShowMediaItemClicked(media);
                } else if (selection.equals(share)) {
                    MediaElementView.this.onShareMediaItemClicked(media);
                }
            }
        });
        b.create().show();
    }

    private void onDownloadMediaItemClicked(MediaItemValue mediaItemValue) {
        Context context = getContext();
        if (NetworkUtils.isConnected(context)) {
            AttachmentDownloadService.startDownload(context, Account.getActiveAccount(), getRecord(), mediaItemValue.getMediaID(), getType());
            return;
        }
        AlertUtils.showNetworkUnavailableAlert(context);
    }

    private void onEditCaptionItemClicked(MediaItemValue media) {
        if (this.mListener != null) {
            this.mListener.onEditCaptionButtonClicked(getElement(), media);
        }
    }

    private void onRemoveMediaItemClicked(MediaItemValue media) {
        if (this.mListener != null) {
            this.mListener.onRemoveMediaButtonClicked(getElement(), media);
        }
    }

    private void onShowMediaItemClicked(MediaItemValue media) {
        Context context = getContext();
        Intent intent = IntentUtils.viewMedia(context, media.getAttachment().getFileOne(), getType());
        if (intent != null) {
            context.startActivity(intent);
        }
    }

    private void onShareMediaItemClicked(MediaItemValue media) {
        if (this.mListener != null) {
            this.mListener.onShareMediaButtonClicked(media, getType());
        }
    }
}
