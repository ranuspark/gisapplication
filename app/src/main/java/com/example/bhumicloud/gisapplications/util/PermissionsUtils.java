package com.example.bhumicloud.gisapplications.util;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;

public class PermissionsUtils {
    public static final int CAMERA = 16;
    public static final int LOCATION = 64;
    public static final int MICROPHONE = 32;
    public static final int REQUEST_01 = 1;
    public static final int REQUEST_02 = 2;
    public static final int REQUEST_04 = 4;
    public static final int STORAGE = 128;

    public static boolean checkPermissions(Context context, int permissionsGroups) {
        return checkAndConvert(context, permissionsGroups) == null;
    }

    public static boolean checkPermissionsAndRequest(Activity activity, int permissionsGroups) {
        String[] missingPermissions = checkAndConvert(activity, permissionsGroups);
        if (missingPermissions == null) {
            return true;
        }
        requestPermissions(activity, missingPermissions, permissionsGroups);
        return false;
    }

    private static String[] checkAndConvert(Context context, int permissionsGroups) {
        ArrayList<String> missingPermissions = new ArrayList();
        if ((permissionsGroups & 16) == 16 && !hasPermission(context, "android.permission.CAMERA")) {
            missingPermissions.add("android.permission.CAMERA");
        }
        if ((permissionsGroups & 32) == 32 && !hasPermission(context, "android.permission.RECORD_AUDIO")) {
            missingPermissions.add("android.permission.RECORD_AUDIO");
        }
        if ((permissionsGroups & 64) == 64) {
            if (!hasPermission(context, "android.permission.ACCESS_COARSE_LOCATION")) {
                missingPermissions.add("android.permission.ACCESS_COARSE_LOCATION");
            }
            if (!hasPermission(context, "android.permission.ACCESS_FINE_LOCATION")) {
                missingPermissions.add("android.permission.ACCESS_FINE_LOCATION");
            }
        }
        if ((permissionsGroups & 128) == 128 && !hasPermission(context, "android.permission.WRITE_EXTERNAL_STORAGE")) {
            missingPermissions.add("android.permission.WRITE_EXTERNAL_STORAGE");
        }
        if (missingPermissions.isEmpty()) {
            return null;
        }
        return (String[]) missingPermissions.toArray(new String[missingPermissions.size()]);
    }

    private static boolean hasPermission(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == 0;
    }

    private static void requestPermissions(Activity activity, String[] permissions, int requestCode) {
        ActivityCompat.requestPermissions(activity, permissions, requestCode);
    }
}
