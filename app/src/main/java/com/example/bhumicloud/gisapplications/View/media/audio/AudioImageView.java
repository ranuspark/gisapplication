package com.example.bhumicloud.gisapplications.View.media.audio;

import android.content.Context;
import android.util.AttributeSet;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.View.media.MediaImageView;

public class AudioImageView extends MediaImageView {
    public AudioImageView(Context context) {
        super(context);
    }

    public AudioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AudioImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setImage(Attachment anAttachment) {
        setBackgroundResource(R.drawable.bg_media_play);
    }
}
