package com.example.bhumicloud.gisapplications.Activity;

import android.os.Bundle;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;

public class EditCaptionDialog extends TextInputDialog {
    public static final String ARG_CAPTION = "ARG_CAPTION";
    public static final String ARG_ITEM_ID = "ARG_ITEM_ID";
    private String mItemID;
    private OnCaptionEditedListener mListener;

    public interface OnCaptionEditedListener {
        void onCaptionEdited(String str, String str2);
    }

    public static EditCaptionDialog getInstance(MediaItemValue media) {
        return getInstance(media.getMediaID(), media.getCaption());
    }

    public static EditCaptionDialog getInstance(String itemID, String caption) {
        Bundle args = new Bundle ();
        args.putString(ARG_ITEM_ID, itemID);
        args.putString(ARG_CAPTION, caption);
        EditCaptionDialog dialog = new EditCaptionDialog();
        dialog.setArguments(args);
        return dialog;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle((int) R.string.edit_caption);
        Bundle args = getArguments();
        if (args != null) {
            this.mItemID = args.getString(ARG_ITEM_ID);
            setTextValue(args.getString(ARG_CAPTION));
        }
    }

    public void onDestroy() {
        this.mListener = null;
        super.onDestroy();
    }

    public void setOnCaptionEditedListener(OnCaptionEditedListener listener) {
        this.mListener = listener;
    }

    protected void onTextValueEntered(String value) {
        super.onTextValueEntered(value);
        notifyOnCaptionEdited(value);
    }

    private void notifyOnCaptionEdited(String caption) {
        if (this.mListener != null) {
            this.mListener.onCaptionEdited(this.mItemID, caption);
        }
    }
}
