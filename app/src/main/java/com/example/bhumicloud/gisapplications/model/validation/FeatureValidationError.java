package com.example.bhumicloud.gisapplications.model.validation;

public abstract class FeatureValidationError {
    protected String mMessage = "";

    public String getMessage() {
        return this.mMessage;
    }
}
