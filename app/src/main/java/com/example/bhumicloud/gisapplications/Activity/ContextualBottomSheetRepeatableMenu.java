package com.example.bhumicloud.gisapplications.Activity;

import android.os.Bundle;

import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableItemValue;

public class ContextualBottomSheetRepeatableMenu extends ContextualBottomSheetMenu {
    private RepeatableItemValue mItemValue;
    private ContextualBottomSheetRepeatableMenuListener mListener;
    private String mTitle;

    public interface ContextualBottomSheetRepeatableMenuListener {
        void onDelete(RepeatableItemValue repeatableItemValue);

        void onDrivingDirections(RepeatableItemValue repeatableItemValue);

        void onDuplicate(RepeatableItemValue repeatableItemValue, boolean z, boolean z2);

        void onShowLocation(RepeatableItemValue repeatableItemValue);
    }

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        this.mTitle = getArguments().getString(ContextualBottomSheetMenu.ARG_TITLE);
    }

    public void onDestroy() {
        this.mListener = null;
        super.onDestroy();
    }

    protected String getTitleText() {
        return this.mTitle;
    }

    protected void duplicate(boolean location, boolean values) {
        this.mListener.onDuplicate(this.mItemValue, location, values);
    }

    protected void showOnMap() {
        this.mListener.onShowLocation(this.mItemValue);
    }

    protected void drivingDirections() {
        this.mListener.onDrivingDirections(this.mItemValue);
    }

    protected void deleteItem() {
        this.mListener.onDelete(this.mItemValue);
    }

    public void setListener(ContextualBottomSheetRepeatableMenuListener listener) {
        this.mListener = listener;
    }

    public void setRepeatableItemValue(RepeatableItemValue value) {
        this.mItemValue = value;
    }
}
