package com.example.bhumicloud.gisapplications.apiLayer;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.Base64;
import com.google.android.gms.maps.model.LatLngBounds;
import com.example.bhumicloud.gisapplications.apiLayer.CountingRequestBody.UploadObserver;
import com.example.bhumicloud.gisapplications.apiLayer.exception.BadRequestException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.InvalidCredentialsException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.InvalidResponseException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.ServerMaintenanceException;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment.Type;
import com.example.bhumicloud.gisapplications.model.field.attachment.photo.Photo;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.example.bhumicloud.gisapplications.util.ApplicationUtils;
import com.example.bhumicloud.gisapplications.util.DeviceInfo;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import com.example.bhumicloud.gisapplications.util.MimeUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import okhttp3.MediaType;
import okhttp3.MultipartBody.Part;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Response;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.jackson.JacksonConverterFactory;


public class Client {
    public static final String DEFAULT_HOSTNAME = "api.bhumicloud.com";
    private static final MediaType MULTIPART_FORM_DATA = MediaType.parse ( "multipart/form-data" );
    private GisService mAPI;
    private String mApplicationBuildNumber;
    private String mApplicationVersion;
    private String mDeviceID;
    private String mDeviceManufacturer;
    private String mDeviceModel;
    private OkHttpClient mGisServiceClient;
    private GISServiceInterceptor mGisServiceInterceptor;
    private OkHttpClient mGeneralServiceClient;
    private final String mHostname;
    private String mPlatformVersion;
    private final int mPort;
    private final String mProtocol;
    private String[] pieces;

    public Client(Context context) {
        this ( context, "" );
    }

    public Client(Context context, Account account) {
        this ( context, account.getToken () );
    }

    public Client(Context context, String accountToken) {
        this.mHostname = PatronSettings.getHostname ( context );
        boolean httpsEnabled = PatronSettings.isHttpsEnabled ( context );
        this.mDeviceID = DeviceInfo.getDeviceIdentifier ( context );
        this.mDeviceModel = DeviceInfo.getDeviceModel ();
        this.mDeviceManufacturer = DeviceInfo.getDeviceManufacturer ();
        this.mPlatformVersion = DeviceInfo.getSystemVersion ();
        this.mApplicationVersion = ApplicationUtils.getVersion ( context );
        this.mApplicationBuildNumber = "" + ApplicationUtils.getBuildNumber ( context );
        int portIndex = getHostname ().lastIndexOf ( ":" );
        if (httpsEnabled) {
            this.mPort = 443;
            this.mProtocol = "https://";
        } else if (portIndex > -1) {
            int port;
            try {
                port = Integer.parseInt ( getHostname ().substring ( portIndex + 1 ) );
            } catch (Throwable e) {
                GISLogger.log ( e );
                port = 80;
            }
            this.mPort = port;
            this.mProtocol = "http://";
        } else {
            this.mPort = 80;
            this.mProtocol = "http://";
        }
        this.mGisServiceInterceptor = new GISServiceInterceptor ( ApplicationUtils.getUserAgentString ( context ), accountToken, this.mApplicationVersion, this.mApplicationBuildNumber, this.mDeviceID, this.mDeviceModel, this.mDeviceManufacturer, this.mPlatformVersion );
        this.mAPI = createGisService ( true );
    }

    public String getProtocol() {
        return this.mProtocol;
    }

    public String getHostname() {
        return this.mHostname;
    }

    public int getPort() {
        return this.mPort;
    }

    public HashMap<String, Object> getMessages(Context context) throws IOException {
        PackageInfo info = ApplicationUtils.getGISPackageInfo ( context );
        String versionName = null;
        String buildNumber = null;
        if (info != null) {
            versionName = info.versionName;
            buildNumber = String.valueOf ( info.versionCode );
        }
        String androidVer = VERSION.RELEASE;
        String deviceName = Build.MODEL;
        return JSONUtils.getHashMap ( (Map) new CallWrapper ().execute ( this.mAPI.getMessages ( versionName, buildNumber, "GIS_android", androidVer, deviceName, deviceName + " - " + androidVer ) ).body (), "messages" );
    }

    public HashMap<String, Object> getAccount() throws IOException {
        return JSONUtils.getHashMap ( (Map) new CallWrapper ().execute ( this.mAPI.getAccount () ).body (), Account.TYPE_USER );
    }

    public HashMap<String, Object> getAccount(String username, String password) throws IOException {
        String authorization = null;
        if (!(TextUtils.isEmpty ( username ) || TextUtils.isEmpty ( password ))) {
            authorization = "Basic " + Base64.encodeToString ( (username + ":" + password).getBytes (), 2 );
        }

        return JSONUtils.getHashMap ( (Map) new CallWrapper ().execute ( this.mAPI.getAccount ( authorization ) ).body () , Account.TYPE_USER );
    }

    public HashMap<String, Object> getForms(int page) throws IOException {
        return (HashMap) new CallWrapper ().execute ( this.mAPI.getForms ( page ) ).body ();
    }

    public HashMap<String, Object> getProjects(int page) throws IOException {
        return (HashMap) new CallWrapper ().execute ( this.mAPI.getProjects ( page ) ).body ();
    }

    public HashMap<String, Object> getRecords(Form form, long sequence, LatLngBounds bounds, boolean useHistory) throws IOException {
        String boundingBox = null;
        if (bounds != null) {
            boundingBox = bounds.southwest.latitude + "," + bounds.southwest.longitude + "," + bounds.northeast.latitude + "," + bounds.northeast.longitude;
        }
        if (useHistory) {
            return (HashMap) new CallWrapper ().execute ( this.mAPI.getRecordHistory ( form.getRemoteID (), sequence, boundingBox ) ).body ();
        }
        return (HashMap) new CallWrapper ().execute ( this.mAPI.getRecords ( form.getRemoteID (), sequence, boundingBox ) ).body ();
    }

    public void uploadAttachment(Attachment attachment, UploadObserver observer) throws IOException {
        if (attachment instanceof Photo) {
            uploadPhoto ( (Photo) attachment, observer );
        }
    }

    public void uploadPhoto(Photo photo, UploadObserver observer) throws IOException {
        File file = photo.getFileOne ();
        Part part = Part.createFormData ( "photo[file]", file.getName (), new CountingRequestBody ( MimeUtils.getMimeType ( file, Type.PHOTO ), file, observer ) );
        HashMap<String, RequestBody> partMap = new HashMap ();
        addRequestBody ( partMap, "photo[access_key]", photo.getRemoteID () );
        new CallWrapper ().execute ( this.mAPI.uploadPhoto ( part, partMap ) );
    }


    public Response<HashMap<String, Object>> uploadRecord(Record record, String changeset) throws IOException {
        HashMap<String, Object> json = record.toJSON ();
        if (!TextUtils.isEmpty ( changeset )) {
            json.put ( "changeset_id", changeset );
        }
        HashMap<String, Object> container = new HashMap ();
        container.put ( "record", json );
        return new CallWrapper ().execute ( this.mAPI.syncrecord ( container ) );
    }

    public Response<ResponseBody> downloadGISitem(String aURL) throws IOException {
        URL url = new URL ( aURL );
        String[] pieces = processUrl ( url );
        if (pieces.length < 1) {
            throw new IOException ( "cannot process url: " + url.toString () );
        }
        return new CallWrapper ().execute ( createGisService ( pieces[0], false ).download ( null, pieces[1], processQuery ( url.getQuery () ) ) );
    }

    public Response<ResponseBody> downloadPublicItem(URL url) throws IOException {
        String[] pieces = processUrl ( url );
        if (pieces.length < 1) {
            throw new IOException ( "cannot process url: " + url.toString () );
        }
        Response response = createGeneralService ( pieces[0] ).download ( null, pieces[1], processQuery ( url.getQuery () ) ).execute ();
        if (response.isSuccessful ()) {
            return response;
        }
        switch (response.code ()) {
            case 401:
                throw new InvalidCredentialsException ();
            case 503:
                throw new ServerMaintenanceException ();
            default:
                throw new InvalidResponseException ( response );
        }
    }

    public Response<ResponseBody> downloadPublicItem(String url) throws IOException {
        return downloadPublicItem ( new URL ( url ) );
    }

    public HashMap<String, Object> getAttachmentJSON(Attachment attachment) throws IOException {
        return getAttachmentJSON ( attachment.getRemoteID (), attachment.getType () );
    }

    public HashMap<String, Object> getAttachmentJSON(String attachmentID, Type type) throws IOException {
        switch (type) {
            case PHOTO:
                return JSONUtils.getHashMap ( (Map) new CallWrapper ().execute ( this.mAPI.getPhotoJson ( attachmentID ) ).body (), "photo" );
               default:
                return null;
        }
    }

    public String downloadFormThumbnail(Context context, Form form) {
        URL remoteURL = form.getRemoteThumbnailPath ();
        if (form.getLocalThumbnailPath () != null) {
            form.deleteLocalThumbnail ();
        }
        String URLAsString = remoteURL.toString ();
        File file = form.generateStorageLocation ( context, URLAsString.substring ( URLAsString.lastIndexOf ( "/" ) + 1 ) );
        File temp = new File ( file.getParentFile (), file.getName () + ".tmp" );
        try {
            Response response = downloadPublicItem ( remoteURL );
            if (response.isSuccessful ()) {
                byte[] buffer = new byte[1024];
                InputStream in = ((ResponseBody) response.body ()).byteStream ();
                FileOutputStream out = new FileOutputStream ( temp );
                while (true) {
                    int length = in.read ( buffer );
                    if (length >= 0) {
                        out.write ( buffer, 0, length );
                    } else {
                        out.flush ();
                        out.close ();
                        temp.renameTo ( file );
                        return file.getAbsolutePath ();
                    }
                }
            }
            throw new InvalidResponseException ( response );
        } catch (Throwable e) {
            GISLogger.log ( e );
            temp.delete ();
            return null;
        }
    }

    public byte[] getSystemResource(String resource) {
        byte[] resourceBytes = new byte[0];
        try {
            byte[] buffer = new byte[4096];
            InputStream in = ((ResponseBody) new CallWrapper ().execute ( this.mAPI.getSystemResource ( null, resource ) ).body ()).byteStream ();
            ByteArrayOutputStream out = new ByteArrayOutputStream ();
            while (true) {
                int length = in.read ( buffer );
                if (length < 0) {
                    break;
                }
                out.write ( buffer, 0, length );
            }
            out.flush ();
            out.close ();
            resourceBytes = out.toByteArray ();
        } catch (Throwable e) {
            GISLogger.log ( e );
        }
        return resourceBytes;
    }

    public boolean checkEmail(String email) throws IOException {
        return JSONUtils.getBoolean ( (Map) new CallWrapper ().execute ( this.mAPI.checkEmail ( email ) ).body (), "available" );
    }

    public HashMap<String, Object> updateProfile(String firstName, String lastName, String email) throws IOException {
        HashMap<String, Object> user = new HashMap ();
        user.put ( Account.COLUMN_FIRST_NAME, firstName );
        user.put ( Account.COLUMN_LAST_NAME, lastName );
        user.put ( "email", email );
        HashMap<String, Object> userContainer = new HashMap ();
        userContainer.put ( Account.TYPE_USER, user );
        return JSONUtils.getHashMap ( (Map) new CallWrapper ().execute ( this.mAPI.updateProfile ( userContainer ) ).body (), Account.TYPE_USER );
    }

    public HashMap<String, Object> uploadProfileImage(File imageFile) throws IOException {
        return JSONUtils.getHashMap ( (Map) new CallWrapper ().execute ( this.mAPI.uploadProfileImage ( Part.createFormData ( "image", imageFile.getName (), RequestBody.create ( MediaType.parse ( MimeUtils.getMimeType ( imageFile, Type.PHOTO ) ), imageFile ) ) ) ).body (), Account.TYPE_USER );
    }

    public HashMap<String, Object> getAppGallery() throws IOException {
        return (HashMap) new CallWrapper ().execute ( this.mAPI.getAppGallery ( true ) ).body ();
    }

    public HashMap<String, Object> addGalleryApp(String appRemoteID) throws IOException {
        return (HashMap) new CallWrapper ().execute ( this.mAPI.addGalleryApp ( appRemoteID, "" ) ).body ();
    }

    public boolean resetPassword(String email) throws IOException {
        HashMap<String, Object> user = new HashMap ();
        user.put ( "email", email );
        HashMap<String, Object> userContainer = new HashMap ();
        userContainer.put ( Account.TYPE_USER, user );
        try {
            return JSONUtils.getBoolean ( (Map) new CallWrapper ().execute ( this.mAPI.resetPassword ( userContainer ) ).body (), "sent" );
        } catch (BadRequestException e) {
            return false;
        }
    }

    private void addRequestBody(HashMap<String, RequestBody> partMap, String key, String value) {
        if (!TextUtils.isEmpty ( value )) {
            partMap.put ( key, RequestBody.create ( MULTIPART_FORM_DATA, value ) );
        }
    }

    private String[] processUrl(URL url) {
        if (url == null) {
            return new String[0];
        }
        pieces = new String[3];
        String urlWithoutQuery = url.getProtocol () + "://" + url.getAuthority () + url.getPath ();
        int index = urlWithoutQuery.lastIndexOf ( "/" );
        pieces[0] = urlWithoutQuery.substring ( 0, index + 1 );
        pieces[1] = urlWithoutQuery.substring ( index + 1 );
        return pieces;
    }

    private HashMap<String, String> processQuery(String query) {
        HashMap<String, String> map = new HashMap ();
        if (query != null) {
            for (String kvp : query.split ( "&" )) {
                String[] kv = kvp.split ( "=" );
                map.put ( kv[0], kv[1] );
            }
        }
        return map;
    }

    private GeneralService createGeneralService(String endpoint) {
        if (this.mGeneralServiceClient == null) {
            this.mGeneralServiceClient = createOkHttpClientBuilder ().build ();
        }
        return (GeneralService) new Builder ().client ( this.mGeneralServiceClient ).baseUrl ( endpoint ).build ().create ( GeneralService.class );
    }

    private GisService createGisService(boolean convertResponseToJSON) {
        return createGisService ( getProtocol () + getHostname (), convertResponseToJSON );
    }

    private GisService createGisService(String endpoint, boolean convertResponseToJSON) {
        if (this.mGisServiceClient == null) {
            OkHttpClient.Builder okHttpClientBuilder = createOkHttpClientBuilder ();
            okHttpClientBuilder.addInterceptor ( this.mGisServiceInterceptor );
            this.mGisServiceClient = okHttpClientBuilder.build ();
        }
        Builder builder = new Builder ();
        builder.client ( this.mGisServiceClient ).baseUrl ( endpoint );
        if (convertResponseToJSON) {
            builder.addConverterFactory (JacksonConverterFactory.create () );
        }
        return (GisService) builder.build().create(GisService.class );
    }

    private OkHttpClient.Builder createOkHttpClientBuilder() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder ();
        builder.connectTimeout ( 15, TimeUnit.SECONDS );
        builder.readTimeout ( 60, TimeUnit.SECONDS );
        builder.writeTimeout ( 60, TimeUnit.SECONDS );
        builder.addInterceptor ( new HttpLoggingInterceptor ().setLevel ( Level.BASIC ) );
        return builder;
    }

    public HashMap<String, Object> openChangeset(Form form) throws IOException {
        HashMap<String, Object> metadata = new HashMap();
        HashMap<String, Object> changeset = new HashMap();
        HashMap<String, Object> payload = new HashMap();
        metadata.put("application", "GIS Android");
        metadata.put("application_version", this.mApplicationVersion);
        metadata.put("application_build", this.mApplicationBuildNumber);
        metadata.put("device_identifier", this.mDeviceID);
        metadata.put("device_manufacturer", this.mDeviceManufacturer);
        metadata.put("device_model", this.mDeviceModel);
        metadata.put("platform", "Android");
        metadata.put("platform_version", this.mPlatformVersion);
        changeset.put(Record.COLUMN_FORM_ID, form.getRemoteID());
        changeset.put("metadata", metadata);
        payload.put("changeset", changeset);
        return JSONUtils.getHashMap((Map) new CallWrapper().execute(this.mAPI.openChangeset(payload)).body(), "changeset");
    }

    public void closeChangeset(String changeset) throws IOException {
        new CallWrapper().execute(this.mAPI.closeChangeset(changeset, ""));
    }
}