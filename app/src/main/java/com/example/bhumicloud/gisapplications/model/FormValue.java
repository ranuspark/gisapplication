package com.example.bhumicloud.gisapplications.model;

import java.util.ArrayList;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;


public interface FormValue {
    boolean contains(String str);

    Object getColumnValue();

    String getDisplayValue();

    Element getElement();

    ArrayList<MultipleValueItem> getMultipleValues();

    String getSearchableValue();

    boolean isEmpty();

    boolean isEqualForConditions(String str);

    boolean isGreaterThan(String str);

    boolean isLessThan(String str);

    int length();

    boolean startsWith(String str);

    Object toJSON();
}
