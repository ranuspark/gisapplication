package com.example.bhumicloud.gisapplications.util;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.text.TextUtils;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.apiLayer.exception.GISServiceException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.InvalidCredentialsException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.NetworkUnavailableException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.ServerMaintenanceException;
import com.example.bhumicloud.gisapplications.model.Role;
import java.io.InterruptedIOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import javax.net.ssl.SSLException;

public class AlertUtils {
    public static void showExceptionAlert(Context context, Exception exception) {
        Builder b;
        if (exception instanceof InvalidCredentialsException) {
            showInvalidCredentialsAlert(context);
        } else if (exception instanceof ServerMaintenanceException) {
            showServerMaintenanceAlert(context);
        } else if (exception instanceof NetworkUnavailableException) {
            showNetworkUnavailableAlert(context);
        } else if (exception instanceof SSLException) {
            b = newInstance(context);
            b.setTitle(R.string.ssl_exception_title);
            b.setMessage(R.string.ssl_exception_message);
            b.show();
        } else if (exception instanceof SocketTimeoutException) {
            b = newInstance(context);
            b.setTitle(R.string.socket_timeout_exception_title);
            b.setMessage(R.string.socket_timeout_exception_message);
            b.show();
        } else if (exception instanceof InterruptedIOException) {
            b = newInstance(context);
            b.setTitle(R.string.connect_timeout_exception_title);
            b.setMessage(R.string.connect_timeout_exception_message);
            b.show();
        } else if (exception instanceof UnknownHostException) {
            b = newInstance(context);
            b.setTitle(R.string.unknown_host_exception_title);
            b.setMessage(R.string.unknown_host_exception_message);
            b.show();
        } else if (exception instanceof SocketException) {
            b = newInstance(context);
            b.setTitle(R.string.socket_exception_title);
            b.setMessage(R.string.socket_exception_message);
            b.show();
        } else {
            showGenericExceptionAlert(context, exception);
        }
    }

    public static void showNetworkUnavailableAlert(Context context) {
        Builder b = newInstance(context);
        b.setTitle(R.string.internet_unavailable_title);
        b.setMessage(R.string.internet_unavailable_message);
        b.show();
    }

    public static void showInvalidCredentialsAlert(Context context) {
        Builder b = newInstance(context);
        b.setTitle(R.string.invalid_credentials_title);
        b.setMessage(R.string.invalid_credentials_message);
        b.show();
    }

    public static void showServerMaintenanceAlert(Context context) {
        Builder b = newInstance(context);
        b.setTitle(R.string.server_maintenance_title);
        b.setMessage(R.string.server_maintenance_message);
        b.show();
    }

    public static void showGenericExceptionAlert(Context context, Exception exception) {
        Builder b = newInstance(context);
        b.setTitle(exception.getClass().getSimpleName());
        String message = getExceptionMessage(context, exception);
        if (TextUtils.isEmpty(message)) {
            message = "";
        }
        b.setMessage(message);
        b.show();
    }

    public static void showGenericAlert(Context context, int titleResource, int messageResource) {
        Builder b = newInstance(context);
        b.setTitle(titleResource);
        b.setMessage(messageResource);
        b.show();
    }

    public static void showPermissionsAlert(Context context, String permission) {
        Builder b = newInstance(context);
        if (Role.PERMISSION_CREATE_RECORDS.equalsIgnoreCase(permission)) {
            b.setMessage(R.string.cannot_create_records);
        } else if (Role.PERMISSION_UPDATE_RECORDS.equalsIgnoreCase(permission)) {
            b.setMessage(R.string.cannot_update_records);
        } else if (Role.PERMISSION_GENERATE_REPORTS.equalsIgnoreCase(permission)) {
            b.setMessage(R.string.cannot_generate_reports);
        } else if (Role.PERMISSION_CHANGE_PROJECT.equalsIgnoreCase(permission)) {
            b.setMessage(R.string.cannot_update_project);
        } else if (Role.PERMISSION_CHANGE_STATUS.equalsIgnoreCase(permission)) {
            b.setMessage(R.string.cannot_update_status);
        } else {
            b.setMessage(R.string.generic_permission_error_message);
        }
        b.setTitle(R.string.permission_error_title);
        b.show();
    }

    public static void showSynchronizingAlert(Context context) {
        Builder b = newInstance(context);
        b.setTitle(R.string.synchronizing);
        b.setMessage(R.string.concurrent_edit_message);
        b.show();
    }

    public static String getExceptionMessage(Context context, Exception e) {
        if (e instanceof GISServiceException) {
            return ((GISServiceException) e).getUserFriendlyMessage(context);
        }
        if (e instanceof SSLException) {
            return context.getString(R.string.ssl_exception_message);
        }
        if (e instanceof SocketTimeoutException) {
            return context.getString(R.string.socket_timeout_exception_message);
        }
        if (e instanceof InterruptedIOException) {
            return context.getString(R.string.connect_timeout_exception_message);
        }
        if (e instanceof UnknownHostException) {
            return context.getString(R.string.unknown_host_exception_message);
        }
        if (e instanceof SocketException) {
            return context.getString(R.string.socket_exception_message);
        }
        return e.getLocalizedMessage();
    }

    private static Builder newInstance(Context context) {
        return new Builder(context);
    }
}

