package com.example.bhumicloud.gisapplications.model.field.attachment.photo;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;

import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment.Type;
import com.example.bhumicloud.gisapplications.util.MimeUtils;

import java.io.File;
import java.util.List;

public class Photo extends Attachment {
    public static final Creator<Photo> CREATOR = new Creator<Photo> () {
        @Override
        public Photo createFromParcel(Parcel source) {
            return new Photo(source);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    public static Uri generatePhotoURI(Context context, String key, String contentType) {
        return Uri.fromFile(generatePhotoFile(context, key, contentType));
    }

    public static File generatePhotoFile(Context context, String key, String contentType) {
        return new File(Attachment.getStorageDirectory(context), "photo-" + key + "." + MimeUtils.getExtension(contentType));
    }

    public static List<Attachment> getPhotos(Account account, Record record, boolean includeUploaded) {
        return Attachment.getAttachments(account, record, includeUploaded, "photo");
    }

    public Photo(String key) {
        super(key);
    }

    public Photo(Cursor cursor) {
        super(cursor);
    }

    private Photo(Parcel parcel) {
        super(parcel);
    }

    public Photo(String key, Uri localUri, String thumbnailPath) {
        super(key, localUri, thumbnailPath);
    }

    public File generateFile(Context context, String contentType) {
        return generatePhotoFile(context, getRemoteID(), contentType);
    }

    public Type getType() {
        return Type.PHOTO;
    }

    public boolean canEqual(Object other) {
        return other instanceof Photo;
    }
}
