package com.example.bhumicloud.gisapplications.View.media.audio;

import android.content.Context;
import  com.example.bhumicloud.gisapplications.R;
import  com.example.bhumicloud.gisapplications.model.field.attachment.Attachment.Type;
import  com.example.bhumicloud.gisapplications.model.field.attachment.audio.AudioElement;
import  com.example.bhumicloud.gisapplications.View.media.MediaElementView;

public class AudioElementView extends MediaElementView {
    public AudioElementView(Context context, AudioElement element, boolean viewOnly) {
        super(context, element, viewOnly);
    }

    protected int getThumbnailLayoutResource() {
        return R.layout.view_audio_thumbnail;
    }

    protected int getObserveActionResource() {
        return R.string.media_play;
    }

    protected Type getType() {
        return Type.AUDIO;
    }

    protected int getNewMediaDrawable() {
        return R.drawable.ic_media_mic;
    }

    protected int getSelectMediaDrawable() {
        return R.drawable.ic_library_music;
    }
}
