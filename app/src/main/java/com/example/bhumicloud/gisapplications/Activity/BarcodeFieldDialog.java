package com.example.bhumicloud.gisapplications.Activity;

import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.field.textual.barcode.BarcodeValue;

public class BarcodeFieldDialog extends ThemedDialogFragment {
    private static final String STATE_TEXT_VALUE = "STATE_TEXT_VALUE";
    private BarcodeFieldDialogListener mListener;
    private EditText mTextField;
    private String mTextValue;

    public interface BarcodeFieldDialogListener {
        void onBarcodeValueSet(String str);

        void onScanForBarcodeButtonClicked();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNegativeButtonText((int) R.string.clear);
        setNeutralButtonText((int) R.string.scan);
        setPositiveButtonText((int) R.string.done);
        if (savedInstanceState != null) {
            this.mTextValue = savedInstanceState.getString(STATE_TEXT_VALUE);
        }
    }

    public View onCreateContentView(LayoutInflater inflater, ViewGroup root, Bundle savedState) {
        View view = inflater.inflate(R.layout.dialog_text_input, root, false);
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        this.mTextField = (EditText) view.findViewById(R.id.text_field);
        this.mTextField.setHint(R.string.barcode_hint);
        if (savedState == null) {
            this.mTextField.setText(this.mTextValue);
            if (this.mTextValue != null) {
                this.mTextField.setSelection(this.mTextValue.length());
            }
        }
        return view;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_TEXT_VALUE, this.mTextValue);
    }

    protected void onNegativeButtonClicked() {
        super.onNegativeButtonClicked();
        if (this.mTextField != null) {
            this.mTextField.setText(null);
        }
    }

    protected void onNeutralButtonClicked() {
        super.onNeutralButtonClicked();
        this.mListener.onScanForBarcodeButtonClicked();
    }

    protected void onPositiveButtonClicked() {
        super.onPositiveButtonClicked();
        if (this.mTextField != null) {
            Editable text = this.mTextField.getText();
            if (this.mListener != null) {
                this.mListener.onBarcodeValueSet(text.toString());
            }
        }
        dismissAllowingStateLoss();
    }

    public void onDestroy() {
        this.mListener = null;
        super.onDestroy();
    }

    public void setBarcodeValue(BarcodeValue barcodeValue) {
        setTitle(barcodeValue.getElement().getLabel());
        this.mTextValue = barcodeValue.getDisplayValue();
        if (this.mTextField != null) {
            this.mTextField.setText(this.mTextValue);
            if (this.mTextValue != null) {
                this.mTextField.setSelection(this.mTextValue.length());
            }
        }
    }

    public void setBarcodeFieldDialogListener(BarcodeFieldDialogListener listener) {
        this.mListener = listener;
    }
}
