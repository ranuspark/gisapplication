package com.example.bhumicloud.gisapplications.model;

import android.database.Cursor;

import java.io.Serializable;

public class Search implements Serializable {
//    private int id;
    private String field;
    private String operator;
    private String val;
    private String val_two;
    private String Id;
    private String status;
    private long ids;

   //private Cursor cursor;

    public String getId() {
        return Id;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getVal_two() {
        return val_two;
    }

    public void setVal_two(String val_two) {
        this.val_two = val_two;
    }

    public void setId(String id) {
        Id = id;
    }

    @Override
    public String toString() {
        return field;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getIds() {
        return ids;
    }

    public void setIds(long ids) {
        this.ids = ids;
    }


}
