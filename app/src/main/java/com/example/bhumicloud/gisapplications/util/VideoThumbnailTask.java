package com.example.bhumicloud.gisapplications.util;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

public class VideoThumbnailTask extends AsyncTask<Void, Void, Bitmap> {
    private int mHeight;
    private WeakReference<ImageView> mImageView;
    private final String mPath;
    private int mWidth;

    public VideoThumbnailTask(String path, int width, int height) {
        this.mWidth = -1;
        this.mHeight = -1;
        this.mWidth = width;
        this.mHeight = height;
        this.mPath = path;
    }

    public VideoThumbnailTask(String path, int width, int height, ImageView imageView) {
        this(path, width, height);
        setImageView(imageView);
    }

    public void setImageView(ImageView imageView) {
        if (imageView != null) {
            this.mImageView = new WeakReference (imageView);
        } else {
            this.mImageView = null;
        }
    }

    protected Bitmap doInBackground(Void... params) {
        if (this.mWidth <= 0 || this.mHeight <= 0 || TextUtils.isEmpty(this.mPath)) {
            return null;
        }
        MediaMetadataRetriever meta = new MediaMetadataRetriever ();
        meta.setDataSource(this.mPath);
        Bitmap frame = meta.getFrameAtTime(0);
        meta.release();
        if (frame == null) {
            frame = Bitmap.createBitmap(this.mWidth, this.mHeight, Config.ARGB_8888);
            frame.eraseColor( ViewCompat.MEASURED_STATE_MASK);
            return frame;
        }
        float scale;
        float w = (float) frame.getWidth();
        float h = (float) frame.getHeight();
        if (w > h) {
            scale = ((float) this.mWidth) / w;
        } else {
            scale = ((float) this.mHeight) / h;
        }
        return Bitmap.createScaledBitmap(frame, Math.round(w * scale), Math.round(h * scale), true);
    }

    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        if (!isCancelled() && result != null && this.mImageView != null) {
            ImageView view = (ImageView) this.mImageView.get();
            if (view != null) {
                view.setImageBitmap(result);
            }
        }
    }
}
