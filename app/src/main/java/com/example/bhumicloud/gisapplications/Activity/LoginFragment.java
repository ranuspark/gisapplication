package com.example.bhumicloud.gisapplications.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.util.AlertUtils;
import com.example.bhumicloud.gisapplications.util.NetworkUtils;

public class LoginFragment extends Fragment {
    private LoginFragmentListener mListener;
    private Button mLoginButton;
    private EditText mPassword;
    private EditText mUsername;

    public void onAttach(Context context) {
        super.onAttach ( context );
        if (context instanceof LoginFragmentListener) {
            this.mListener = (LoginFragmentListener) context;
            return;
        }
        throw new RuntimeException ( context.toString () + " must implement " + LoginFragmentListener.class.getSimpleName () );
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate ( R.layout.fragment_login, container, false );
        this.mUsername = (EditText) view.findViewById ( R.id.username_field );
        this.mPassword = (EditText) view.findViewById ( R.id.password_field );
        this.mLoginButton = (Button) view.findViewById ( R.id.login_btn );
        Button resetPasswordButton = (Button) view.findViewById ( R.id.forgot_password_btn );
        TextWatcher textWatcher = new TextWatcher () {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                LoginFragment.this.resetIsLoginFormValid ();
            }
        };
        this.mUsername.addTextChangedListener ( textWatcher );
        this.mPassword.addTextChangedListener ( textWatcher );
        this.mLoginButton.setEnabled ( false );
        this.mLoginButton.setOnClickListener ( new OnClickListener () {
            @Override
            public void onClick(View v) {
                if (LoginFragment.this.isLoginFormValid ()) {
                    Context context = LoginFragment.this.getContext ();
                    if (NetworkUtils.isConnected ( context )) {
                        LoginFragment.this.mLoginButton.setEnabled ( false );
                        LoginFragment.this.mListener.onLogin ( LoginFragment.this.mUsername.getText ().toString (), LoginFragment.this.mPassword.getText ().toString () );
                        return;
                    }
                    AlertUtils.showNetworkUnavailableAlert ( context );
                }
            }
        } );
        resetPasswordButton.setOnClickListener ( new OnClickListener () {
            public void onClick(View view) {
                //LoginFragment.this.mListener.onResetPasswordButtonClick();
            }
        } );
        return view;
    }

    public void resetIsLoginFormValid() {
        this.mLoginButton.setEnabled ( isLoginFormValid () );
    }

    private boolean isLoginFormValid() {
        return (TextUtils.isEmpty ( this.mUsername.getText ().toString () ) || TextUtils.isEmpty ( this.mPassword.getText ().toString () )) ? false : true;
    }

    public interface LoginButtonListener {
        void onLoginButtonClick();
    }

    public interface LoginFragmentListener {
        void onLogin(String str, String str2);
    }
}
