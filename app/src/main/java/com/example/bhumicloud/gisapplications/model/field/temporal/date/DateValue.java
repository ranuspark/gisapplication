package com.example.bhumicloud.gisapplications.model.field.temporal.date;

import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.example.bhumicloud.gisapplications.model.field.temporal.TemporalValue;
import com.example.bhumicloud.gisapplications.util.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

public class DateValue extends TemporalValue {
    public static final DateFormat DISPLAY_FORMAT = DateUtils.DISPLAY_FORMAT;
    public static final DateFormat VALUE_FORMAT = DateUtils.VALUE_FORMAT;
    private final DateElement mElement;

    public DateValue(DateElement element, Date date) {
        super(date);
        this.mElement = element;
    }

    public DateValue(DateElement element, String aValue) {
        super(aValue);
        this.mElement = element;
    }

    public DateElement getElement() {
        return this.mElement;
    }

    public DateFormat getValueFormat() {
        return VALUE_FORMAT;
    }

    public DateFormat getDisplayFormat() {
        return DISPLAY_FORMAT;
    }

    public Object getColumnValue() {
        try {
            Date date = DateUtils.COLUMN_VALUE_DATE_FORMAT.parse(this.mValue + " 00:00:00+0000");
            if (date != null) {
                return Long.valueOf(date.getTime() / 1000);
            }
        } catch (ParseException e) {
        }
        return null;
    }
}
