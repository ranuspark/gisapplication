package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;

import com.example.bhumicloud.gisapplications.R;

public class GISLogoImageView extends AppCompatImageView {
    private OnDoubleTapListener mListener;

    public interface OnDoubleTapListener {
        void onGisLogoDoubleTap();
    }

    public GISLogoImageView(Context context) {
        super(context);
        setup();
    }

    public GISLogoImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    public GISLogoImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup();
    }

    public void setOnDoubleTapListener(OnDoubleTapListener listener) {
        this.mListener = listener;
    }

    private void setup() {
        setImageResource(R.drawable.gis_logo_extended );
        setClickable(true);
        final GestureDetector gestureDetector = new GestureDetector (getContext(), new SimpleOnGestureListener (){
            @Override
            public boolean onDoubleTap(MotionEvent e) {
                if (GISLogoImageView.this.mListener != null) {
                    GISLogoImageView.this.mListener.onGisLogoDoubleTap();
                }
                return true;
            }
        } );
        setOnTouchListener(new OnTouchListener () {
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });
    }
}
