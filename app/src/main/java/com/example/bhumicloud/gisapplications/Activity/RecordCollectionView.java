package com.example.bhumicloud.gisapplications.Activity;

import java.util.ArrayList;
import com.example.bhumicloud.gisapplications.dossier.RecordPredicate;
import com.example.bhumicloud.gisapplications.util.RecordFilterOptions;


public interface RecordCollectionView {
    public static final String EXTRA_CONTEXT_MENU_ENABLED = "EXTRA_CONTEXT_MENU_ENABLED";
    public static final String EXTRA_CREATE_RECORDS_ENABLED = "EXTRA_CREATE_RECORDS_ENABLED";
    public static final String EXTRA_LIST_ROW_LAYOUT_ID = "EXTRA_LIST_ROW_LAYOUT_ID";
    void setFilter(RecordFilterOptions recordFilterOptions);

    void onSyncFinished();

    void reloadData();

    void setFormID(long j);

    void setSearchQuery(String str);

    void setSelectedRecordLinkIDs(ArrayList<String> arrayList);
    void setRecordConditions(RecordPredicate recordPredicate);

}

