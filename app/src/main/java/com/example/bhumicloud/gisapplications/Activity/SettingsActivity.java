package com.example.bhumicloud.gisapplications.Activity;

import android.app.FragmentManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.Activity.SettingsFragment.SettingsFragmentListener;
import com.example.bhumicloud.gisapplications.Activity.SyncService.Listener;
import com.example.bhumicloud.gisapplications.Activity.SyncService.SyncServiceBinder;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.util.PatronSettings;

import java.util.HashMap;

public class SettingsActivity extends GISActivity implements SettingsFragmentListener, Listener {
    private static final String TAG_SETTINGS_FRAGMENT = "TAG_SETTINGS_FRAGMENT";
    private final ServiceConnection mSyncConnection = new ServiceConnection () {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            if (service instanceof SyncServiceBinder) {
                SettingsActivity.this.mSyncService = ((SyncServiceBinder) service).getService();
                SettingsActivity.this.mSyncService.addListener(SettingsActivity.this);
                if (SettingsActivity.this.mSyncService.isSynchronizing()) {
                    SettingsActivity.this.toggleAccountSettings(false);
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            if (SettingsActivity.this.mSyncService != null) {
                SettingsActivity.this.mSyncService.removeListener ( SettingsActivity.this );
            }
            SettingsActivity.this.mSyncService = null;
        }
        };
    private SyncService mSyncService;

    public static void start(Context context) {
        context.startActivity(new Intent (context, SettingsActivity.class));
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_basic);
        ((Toolbar) findViewById(R.id.toolbar)).setVisibility( View.GONE);
        setTitle(R.string.menu_item_title_settings);
        Account currentAccount = Account.getActiveAccount(this);
        if (currentAccount == null) {
            finish();
            return;
        }
        FragmentManager fm = getFragmentManager();
        if (((SettingsFragment) fm.findFragmentByTag(TAG_SETTINGS_FRAGMENT)) == null) {
            fm.beginTransaction().replace(R.id.container, SettingsFragment.getInstance(currentAccount), TAG_SETTINGS_FRAGMENT).commit();
        }
    }

    protected void onStart() {
        super.onStart();
        SyncService.bind(this, this.mSyncConnection);
    }

    protected void onStop() {
        super.onStop();
        unbindService(this.mSyncConnection);
    }

    public void onAccountChanged(Account selectedAccount) {
        HashMap<String, Object> lastDownload = PatronSettings.getLastRecordDownloadDates(this, selectedAccount);
        if (lastDownload == null || lastDownload.size() < 1) {
            SyncService.synchronizeData(this, selectedAccount);
        }
        Intent dashboardActivityIntent = new Intent (this, DashboardActivity.class);
        dashboardActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(dashboardActivityIntent);
        finish();
    }

    public void onSyncFailed(String reason) {
        toggleAccountSettings(true);
    }

    public void onSyncFinished() {
        toggleAccountSettings(true);
    }

    private void toggleAccountSettings(boolean enabled) {
        SettingsFragment settingsFragment = (SettingsFragment) getFragmentManager().findFragmentByTag(TAG_SETTINGS_FRAGMENT);
        if (settingsFragment != null) {
            settingsFragment.toggleAccountSettings(enabled);
        }
    }
}
