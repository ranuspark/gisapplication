package com.example.bhumicloud.gisapplications.model.field.textual.barcode;

import android.os.Parcel;
import android.os.Parcelable.Creator;

import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.textual.TextualElement;

import java.util.Map;

public class BarcodeElement extends TextualElement {
    public static final Creator<BarcodeElement> CREATOR = new C12021();

    static class C12021 implements Creator<BarcodeElement> {
        C12021() {
        }

        public BarcodeElement createFromParcel(Parcel source) {
            return new BarcodeElement(source);
        }

        public BarcodeElement[] newArray(int size) {
            return new BarcodeElement[size];
        }
    }

    public BarcodeElement(Element parent, Map json) {
        super(parent, json);
    }

    private BarcodeElement(Parcel parcel) {
        super(parcel);
    }

    public String getType() {
        return Element.TYPE_BARCODE;
    }
}
