package com.example.bhumicloud.gisapplications;

import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableItemValue;

public interface RepeatableItemCollectionViewListener {
    void onAdded(RepeatableItemValue repeatableItemValue);

    void onDeleted(RepeatableItemValue repeatableItemValue);

    void onSelected(RepeatableItemValue repeatableItemValue);

    void onShowOnMap(RepeatableItemValue repeatableItemValue);
}
