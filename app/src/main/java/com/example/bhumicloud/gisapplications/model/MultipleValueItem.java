package com.example.bhumicloud.gisapplications.model;

import com.example.bhumicloud.gisapplications.model.field.Element;


public class MultipleValueItem {
    private final String mKey;
    private final String mTextValue;

    public MultipleValueItem(Element element, String textValue) {
        this.mKey = element.getKey();
        this.mTextValue = textValue;
    }

    public String getKey() {
        return this.mKey;
    }

    public String getTextValue() {
        return this.mTextValue;
    }
}

