package com.example.bhumicloud.gisapplications.model.validation.error;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.model.validation.FeatureValidationError;

public class GeometryRequiredValidationError extends FeatureValidationError {
    public GeometryRequiredValidationError() {
        this.mMessage = GIS.getInstance().getResources().getString(R.string.geometry_required_validation_error);
    }
}
