package com.example.bhumicloud.gisapplications.Activity;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.Toast;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.apiLayer.Client;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.appgallery.App;
import com.example.bhumicloud.gisapplications.util.GISLogger;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

public class AddAppService extends IntentService {
    private static final String EXTRA_GALLERY_APP = "GIS:extra:galleryApp";
    private static final String EXTRA_TOKEN = "GIS:extra:token";
    private final Collection<AddAppListener> mAddAppListeners = Collections.synchronizedCollection(new HashSet ());
    private final Handler mHandler = new Handler ();
    private volatile boolean mIsAdding;

    public interface AddAppListener {
        void onAddAppFailure(Exception exception);

        void onAddAppSuccess(App app);
    }

    public static class AppGalleryServiceBinder extends Binder {
        private final AddAppService mService;

        public AppGalleryServiceBinder(AddAppService service) {
            this.mService = service;
        }

        public AddAppService getService() {
            return this.mService;
        }
    }

    public static void bind(Context context, ServiceConnection connection) {
        context.bindService(new Intent (context, AddAppService.class), connection, 1);
    }

    public static void addAppToAccount(Context context, App app, Account account) {
        Intent intent = new Intent (context, AddAppService.class);
        intent.putExtra(EXTRA_GALLERY_APP, app);
        intent.putExtra("gis:extra:token", account.getToken());
        context.startService(intent);
    }

    public AddAppService() {
        super(AddAppService.class.getName());
    }

    public void addListener(AddAppListener listener) {
        this.mAddAppListeners.add(listener);
    }

    public void removeListener(AddAppListener listener) {
        this.mAddAppListeners.remove(listener);
    }

    public boolean isAdding() {
        return this.mIsAdding;
    }

    public AppGalleryServiceBinder onBind(Intent intent) {
        return new AppGalleryServiceBinder(this);
    }

    protected void onHandleIntent(Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            Bundle extras = intent.getExtras();
            this.mIsAdding = true;
            App app = (App) extras.getParcelable(EXTRA_GALLERY_APP);
            if (app == null || TextUtils.isEmpty(app.getRemoteId())) {
                GISLogger.log("trying to add app to account, was null or empty remote id" + app);
           //     Toast.makeText(this, R.string.failed_add_app, 0).show();
            } else {
                addAppToAccount(app, extras.getString("gis:extra:token"));
            }
            this.mIsAdding = false;
        }
    }

    private void addAppToAccount(App app, String token) {
        try {
            HashMap<String, Object> result = new Client((Context) this, token).addGalleryApp(app.getRemoteId());
            if (result == null || !result.containsKey("form")) {
                onAddAppFailure(new IOException ("failure adding: " + app.getRemoteId() + ": " + app.getName()));
            } else {
                onAddAppSuccess(app);
            }
        } catch (IOException e) {
            onAddAppFailure(e);
        }
    }

    private void onAddAppSuccess(final App app) {
        this.mHandler.post(new Runnable () {
            public void run() {
                synchronized (AddAppService.this.mAddAppListeners) {
                    for (AddAppListener observer : AddAppService.this.mAddAppListeners) {
                        observer.onAddAppSuccess(app);
                    }
                }
            }
        });
    }

    private void onAddAppFailure(final Exception e) {
        this.mHandler.post(new Runnable () {
            public void run() {
                synchronized (AddAppService.this.mAddAppListeners) {
                    for (AddAppListener observer : AddAppService.this.mAddAppListeners) {
                        observer.onAddAppFailure(e);
                    }
                }
            }
        });
    }
}
