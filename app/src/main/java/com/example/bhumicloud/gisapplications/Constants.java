package com.example.bhumicloud.gisapplications;

/**
 * Created by Himanshu Singhal on 07-01-2018.
 */

public class Constants {

    //BASE URL
    public static final String BASE_URL = "http://Bhumicloud.com/GISWebservice/";

    //API URL
    //--private String SERVER_URL = "http://Bhumicloud.com/GISWebservice/OQ_uploadtoserver.php";
    public static final String SERVER_URL = "uploadToServer.php";
    public static final String formURL = "getFormFields.php";
    //public static final String SaveDataURL = "SaveData.php";
    public static final String SaveDataURL = "formSaveData.php";
    public static final String DashboardURL = "getDashboardData.php";
    public static final String floorURL = "getSubFormFields.php";
    public static final String loginURL = "userAccess.php";
    //--String floorURL = "http://Bhumicloud.com/GISWebservice/Show_PartentForm.php";
    public static final String showformURL = "formEdit.php";
    // public static final  String editformURL = "http://zserver1.in/GISWebservice/UpdateForm.php";
    public static final String editformURL = "updateData.php";
    public static final String setImageURL = "getImage.php";
    public static final String setflooreditURL = "subFormEdit.php";
    // public static final String floorlistURL = "http://zserver1.in/GISWebservice/Show_AddedParentList.php";
    public static final String floorlistURL = "subFormList.php";
    public static final String updatefloorlistURL = "subFormUpdate.php";
    public static final String searchURL = "searchData.php";
    public static final String LoginURL = "checkUserLogin.php";
    public static final String projectURL = "FormProject.php";
    public static final String MultiprojectURL = "FetchMultipleProject.php";
    //--String SaveDataURL = "http://Bhumicloud.com/GISWebservice/SaveData.php";
    //--String editformURL = "http://Bhumicloud.com/GISWebservice/UpdateForm.php";
    public static final String AddparentURL = "http://Bhumicloud.com/GISWebservice/sqlite_insert_parentdata.php";
    public static final String AddparentURL1 = "http://Bhumicloud.com/GISWebservice/sqlite_Newinsert_parentdata.php";
    //--private String SERVER_URL = "http://Bhumicloud.com/GISWebservice/OQ_uploadtoserver.php";
    //--String updatefloorlistURL = "http://Bhumicloud.com/GISWebservice/Update_AddedParentForm.php";
    public static final String FormSyncUrl = "Autosync_server_sqlite.php";

    /////Image////
    public static final int CAMERA_REQUEST = 1888;
    public static final int PICK_FILE_REQUEST = 1;
    public static final int PERMISSION_REQUEST_CODE = 100;
    public static final int PERMISSION_REQUEST_CODE_LOCATION = 1;

    public static final int strokeColor = 0xffff0000; //Color Code you want
    public static final int shadeColor = 0x44ff0000; //opaque red fill

}
