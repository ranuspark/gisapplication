package com.example.bhumicloud.gisapplications.model.appgallery;

import android.os.Parcel;

import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.Map;

public class Category extends Item {
    public static final Creator<Category> CREATOR = new C11731();
    private String mBannerURL;
    private String mThumbnailURL;

    static class C11731 implements Creator<Category> {
        C11731() {
        }

        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        public Category[] newArray(int size) {
            return new Category[size];
        }
    }

    public Category(Map<String, Object> json) {
        super((Map) json);
        this.mBannerURL = JSONUtils.getString((Map) json, "banner");
        this.mThumbnailURL = JSONUtils.getString((Map) json, "thumbnail");
    }

    protected Category(Parcel parcel) {
        super(parcel);
        this.mBannerURL = parcel.readString();
        this.mThumbnailURL = parcel.readString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.mBannerURL);
        dest.writeString(this.mThumbnailURL);
    }

    public String getBannerURL() {
        return this.mBannerURL;
    }

    public String getThumbnailURL() {
        return this.mThumbnailURL;
    }
}
