package com.example.bhumicloud.gisapplications.widget;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.util.KeyboardUtils;
import com.example.bhumicloud.gisapplications.util.ViewUtils;

public class SearchRelativeLayout extends RelativeLayout implements OnActionExpandListener {
    private OnSearchQueryChangedListener mListener;
    private EditText mSearch;

  public interface OnSearchQueryChangedListener {
        void onSearchQueryChanged(String str);
    }

    public SearchRelativeLayout(Context context) {
        super(context);
        setup();
    }

    public SearchRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    private void setup() {
        ((LayoutInflater) getContext().getSystemService(getContext ().LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_search, this, true);
        LinearLayout l = (LinearLayout) getChildAt(0);
        this.mSearch = (EditText) l.getChildAt(0);
        this.mSearch.setId(ViewUtils.generateViewId());
        this.mSearch.addTextChangedListener ( new TextWatcher () {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (SearchRelativeLayout.this.mListener != null) {
                    SearchRelativeLayout.this.mListener.onSearchQueryChanged(s.toString());
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        } );
        this.mSearch.setOnFocusChangeListener ( new OnFocusChangeListener () {

            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    SearchRelativeLayout.this.mSearch.postDelayed ( new Runnable () {
                        @Override
                        public void run() {
                            ((InputMethodManager) SearchRelativeLayout.this.getContext().getSystemService(getContext().INPUT_METHOD_SERVICE)).showSoftInput(SearchRelativeLayout.this.mSearch, 0);
                        }
                    }, 50 );
                }
            }
        } );
    }

   public void setOnSearchQueryChangedListener(OnSearchQueryChangedListener listener) {
        this.mListener = listener;
    }

    public EditText getSearchEditText() {
        return this.mSearch;
    }

    public boolean onMenuItemActionExpand(MenuItem item) {
        this.mSearch.requestFocus();
        return true;
    }

    public boolean onMenuItemActionCollapse(MenuItem item) {
        KeyboardUtils.hideSoftKeyboard(getContext(), this);
        this.mSearch.setText(null);
        return true;
    }
}
