package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;


public class CanvasView extends View {
    private static final float STROKE_WIDTH = 5.0f;
    private boolean mBlankCanvas;
    private RectF mDirtyRect;
    private float mLastTouchX;
    private float mLastTouchY;
    private Paint mPaint;
    private Path mPath;

    public CanvasView(Context context) {
        super(context);
        initialize();
    }

    public CanvasView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialize();
    }

    public CanvasView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public void clear() {
        this.mBlankCanvas = true;
        this.mPath.reset();
        invalidate();
    }

    public Bitmap getBitmap() {
        if (this.mBlankCanvas) {
            return null;
        }
        clearFocus();
        setPressed(false);
        boolean willNotCache = willNotCacheDrawing();
        setWillNotCacheDrawing(false);
        int color = getDrawingCacheBackgroundColor();
        setDrawingCacheBackgroundColor(0);
        if (color != 0) {
            destroyDrawingCache();
        }
        buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(getDrawingCache());
        destroyDrawingCache();
        setWillNotCacheDrawing(willNotCache);
        setDrawingCacheBackgroundColor(color);
        return bitmap;
    }

    public boolean isBlank() {
        return this.mBlankCanvas;
    }

    protected void onDraw(Canvas canvas) {
        canvas.drawPath(this.mPath, this.mPaint);
    }

    public boolean onTouchEvent(@NonNull MotionEvent event) {
        float eventX = event.getX();
        float eventY = event.getY();
        switch (event.getAction()) {
            case 0:
                this.mLastTouchX = eventX;
                this.mLastTouchY = eventY;
                this.mPath.moveTo(eventX, eventY);
                return true;
            case 1:
            case 2:
                this.mBlankCanvas = false;
                resetDirtyRect(eventX, eventY);
                for (int i = 0; i < event.getHistorySize(); i++) {
                    float histX = event.getHistoricalX(i);
                    float histY = event.getHistoricalY(i);
                    expandDirtyRect(histX, histY);
                    this.mPath.lineTo(histX, histY);
                }
                this.mPath.lineTo(eventX, eventY);
                invalidate((int) (this.mDirtyRect.left - 2.5f), (int) (this.mDirtyRect.top - 2.5f), (int) (this.mDirtyRect.right + 2.5f), (int) (this.mDirtyRect.bottom + 2.5f));
                this.mLastTouchX = eventX;
                this.mLastTouchY = eventY;
                return true;
            default:
                return false;
        }
    }

    private void initialize() {
        this.mBlankCanvas = true;
        this.mPath = new Path();
        this.mPaint = new Paint();
        this.mPaint.setAntiAlias(true);
        this.mPaint.setColor(ViewCompat.MEASURED_STATE_MASK);
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setStrokeJoin(Join.ROUND);
        this.mPaint.setStrokeWidth(STROKE_WIDTH);
        this.mDirtyRect = new RectF();
    }

    private void resetDirtyRect(float x, float y) {
        this.mDirtyRect.left = Math.min(this.mLastTouchX, x);
        this.mDirtyRect.right = Math.max(this.mLastTouchX, x);
        this.mDirtyRect.top = Math.min(this.mLastTouchY, y);
        this.mDirtyRect.bottom = Math.max(this.mLastTouchY, y);
    }

    private void expandDirtyRect(float x, float y) {
        if (x < this.mDirtyRect.left) {
            this.mDirtyRect.left = x;
        } else if (x > this.mDirtyRect.right) {
            this.mDirtyRect.right = x;
        }
        if (y < this.mDirtyRect.top) {
            this.mDirtyRect.top = y;
        } else if (y > this.mDirtyRect.bottom) {
            this.mDirtyRect.bottom = y;
        }
    }
}

