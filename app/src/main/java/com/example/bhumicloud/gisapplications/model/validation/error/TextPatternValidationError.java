package com.example.bhumicloud.gisapplications.model.validation.error;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.textual.text.TextElement;
import com.example.bhumicloud.gisapplications.model.validation.FeatureValidationError;

public class TextPatternValidationError extends FeatureValidationError {
    public TextPatternValidationError(Record record, TextElement element) {
        String label = record == null ? element.getLabel() : element.getAbsoluteLabel(record);
        this.mMessage = String.format(GIS.getInstance().getResources().getString(R.string.text_pattern_validation_error), new Object[]{label, element.getPatternDescription()});
    }
}
