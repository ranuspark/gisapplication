package com.example.bhumicloud.gisapplications.Activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.validation.FeatureValidationError;

import java.util.ArrayList;
import java.util.List;

public class FeatureValidationErrorDialog extends ThemedDialogFragment {
    public static final String ARG_DRAFT_ENABLED = "ARG_DRAFT_ENABLED";
    public static final String ARG_ERRORS = "ARG_ERRORS";
    private boolean mDraftEnabled;
    private List<String> mErrors;
    private FeatureValidationErrorDialogListener mListener;

    public interface FeatureValidationErrorDialogListener {
        void onSaveAsDraftSelected();
    }

    public static FeatureValidationErrorDialog getInstance(List<FeatureValidationError> errors, boolean draftEnabled) {
        Bundle args = new Bundle ();
        ArrayList<String> messages = new ArrayList ();
        for (FeatureValidationError error : errors) {
            messages.add("-- " + error.getMessage());
        }
        args.putStringArrayList(ARG_ERRORS, messages);
        args.putBoolean(ARG_DRAFT_ENABLED, draftEnabled);
        FeatureValidationErrorDialog dialog = new FeatureValidationErrorDialog();
        dialog.setArguments(args);
        return dialog;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle((int) R.string.error_saving);
        setNegativeButtonText((int) R.string.okay);
        Bundle args = getArguments();
        if (args != null) {
            this.mErrors = args.getStringArrayList(ARG_ERRORS);
            this.mDraftEnabled = args.getBoolean(ARG_DRAFT_ENABLED, true);
        }
        if (this.mDraftEnabled) {
            setNeutralButtonText((int) R.string.save_as_draft);
        }
    }

    public View onCreateContentView(LayoutInflater inflater, ViewGroup root, Bundle savedState) {
        View view = inflater.inflate(R.layout.dialog_single_message, root, false);
        ((TextView) view.findViewById(R.id.dialog_message)).setText( TextUtils.join("\n", this.mErrors));
        return view;
    }

    protected void onNegativeButtonClicked() {
        super.onNegativeButtonClicked();
        dismissAllowingStateLoss();
    }

    protected void onNeutralButtonClicked() {
        super.onNeutralButtonClicked();
        if (this.mListener != null) {
            this.mListener.onSaveAsDraftSelected();
        }
        dismissAllowingStateLoss();
    }

    public void setListener(FeatureValidationErrorDialogListener listener) {
        this.mListener = listener;
    }
}
