package com.example.bhumicloud.gisapplications.Activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bhumicloud.gisapplications.R;

public class GenericDialog extends ThemedDialogFragment {
    private static final float PADDING_DP = 21.0f;
    private static final String STATE_MESSAGE = "STATE_MESSAGE";
    private String mMessage;
    private int mMessageId;
    private TextView mMessageView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.mMessageId != 0) {
            this.mMessage = getString(this.mMessageId);
        }
        if (savedInstanceState != null) {
            this.mMessage = savedInstanceState.getString(STATE_MESSAGE);
        }
    }

    public View onCreateContentView(LayoutInflater inflater, ViewGroup root, Bundle savedState) {
        int topPadding;
        int bottomPadding;
        View view = inflater.inflate(R.layout.dialog_single_message, root, false);
        this.mMessageView = (TextView) view.findViewById(R.id.dialog_message);
        this.mMessageView.setText(this.mMessage);
        int pixels = (int) ((PADDING_DP * getResources().getDisplayMetrics().density) + 0.5f);
        if (hasTitle()) {
            topPadding = 0;
        } else {
            topPadding = pixels;
        }
        if (hasButtons()) {
            bottomPadding = 0;
        } else {
            bottomPadding = pixels;
        }
        this.mMessageView.setPadding(pixels, topPadding, pixels, bottomPadding);
        return view;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_MESSAGE, this.mMessage);
    }

    public void setMessage(String message) {
        this.mMessageId = 0;
        this.mMessage = message;
        if (this.mMessageView != null) {
            this.mMessageView.setText(this.mMessage);
        }
    }

    public void setMessage(int message) {
        this.mMessageId = message;
        if (this.mMessageView != null) {
            this.mMessage = getString(message);
            this.mMessageView.setText(this.mMessage);
        }
    }

    protected void onPositiveButtonClicked() {
        super.onPositiveButtonClicked();
        dismiss();
    }
}
