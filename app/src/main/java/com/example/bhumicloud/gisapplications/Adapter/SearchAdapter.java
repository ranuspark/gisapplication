package com.example.bhumicloud.gisapplications.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.example.bhumicloud.gisapplications.Activity.GetValueListener;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.Search;

import java.util.ArrayList;
import java.util.List;

public class SearchAdapter extends  RecyclerView.Adapter<SearchAdapter.MyViewHolder>  {

    private List<Search> array_list;
    private Activity activity;
   // GetValueListener listener;
    String spi_field,spi_operator;

    private static String[] data = new String[] { "0", "1", "2", "3", "4" };

    public SearchAdapter(List<Search> array_list, Activity activity) {
        this.array_list = array_list;
        this.activity = activity;
       // this.listener = (GetValueListener) activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adap_search, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Search sdto = array_list.get(position);
        //  holder.courseName.setText(sdto.getCourseName());

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                array_list.remove(position);
                notifyDataSetChanged();
            }
        });

        List<String> list = new ArrayList<String>();
        list.add("Name");
        list.add("Address");
        list.add("Mobile Number");
        //  holder.ArrayAdapter_sp = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, list);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.Sp_field.setAdapter(dataAdapter);

        List<String> list1 = new ArrayList<String>();
        list1.add("<");
        list1.add(">");
        list1.add("=");
        list1.add("!=");
        list1.add("Between");
        list1.add("Like");
        //  holder.ArrayAdapter_sp = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, list);

        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, list1);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.Sp_operator.setAdapter(dataAdapter1);


        String ss=  holder.Et_val.getText().toString();
        Log.e("GetValue", "Value "+ss);
        holder.Sp_field.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            private int pos = position;
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int p, long arg3)
            {
                // TODO Auto-generated method stub
                  spi_field = (String) arg0.getItemAtPosition(position);

                Toast.makeText(activity, "select spinner " + spi_field+" with value ID "+p, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0)
            {
                // TODO Auto-generated method stub

            }
        });
        holder.Sp_operator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            private int pos = position;
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int p, long arg3)
            {
                // TODO Auto-generated method stub
                spi_operator = (String) arg0.getItemAtPosition(position);
                Toast.makeText(activity, "select spinner " + spi_operator+" with value ID "+p, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0)
            {
                // TODO Auto-generated method stub

            }
        });


      //  listener.getValue(spi_field, spi_operator, ss, null);



    }


    @Override
    public int getItemCount() {
        return array_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public Spinner Sp_field,Sp_operator;
        public ImageView imageView;
        EditText Et_val,Et_val1;
        ArrayAdapter ArrayAdapter_sp;


        public MyViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.iv);

            Sp_field=(Spinner) view. findViewById(R.id.sp_field);
            Sp_operator=(Spinner) view.findViewById(R.id.sp_operator);
            Et_val=(EditText) view.findViewById(R.id.et_val);
            Et_val1=(EditText) view.findViewById(R.id.et_val1);





        }

    }
}