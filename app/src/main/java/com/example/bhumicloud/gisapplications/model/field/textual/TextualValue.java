package com.example.bhumicloud.gisapplications.model.field.textual;

import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.MultipleValueItem;

import java.util.ArrayList;

public abstract class TextualValue implements FormValue {
    protected String mValue = null;

    protected TextualValue() {
    }

    public TextualValue(String aValue) {
        this.mValue = aValue;
    }

    public String getDisplayValue() {
        return this.mValue;
    }

    public String getSearchableValue() {
        return this.mValue;
    }

    public int length() {
        return this.mValue == null ? 0 : this.mValue.length();
    }

    public String toJSON() {
        return this.mValue;
    }

    public boolean isEmpty() {
        return TextUtils.isEmpty(this.mValue);
    }

    public Object getColumnValue() {
        return this.mValue;
    }

    public ArrayList<MultipleValueItem> getMultipleValues() {
        return null;
    }

    public boolean isEqualForConditions(String testValue) {
        return this.mValue == null ? TextUtils.isEmpty(testValue) : this.mValue.equalsIgnoreCase(testValue);
    }

    public boolean contains(String testValue) {
        return this.mValue == null ? TextUtils.isEmpty(testValue) : this.mValue.contains(testValue);
    }

    public boolean startsWith(String testValue) {
        return this.mValue == null ? TextUtils.isEmpty(testValue) : this.mValue.startsWith(testValue);
    }

    public boolean isLessThan(String testValue) {
        if (this.mValue == null) {
            return false;
        }
        try {
            if (Double.parseDouble(this.mValue) < Double.parseDouble(testValue)) {
                return true;
            }
            return false;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean isGreaterThan(String testValue) {
        if (this.mValue == null) {
            return false;
        }
        try {
            if (Double.parseDouble(this.mValue) > Double.parseDouble(testValue)) {
                return true;
            }
            return false;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public String getValue() {
        return this.mValue;
    }

    public void setValue(String value) {
        this.mValue = value;
    }

    public String toString() {
        return this.mValue;
    }
}
