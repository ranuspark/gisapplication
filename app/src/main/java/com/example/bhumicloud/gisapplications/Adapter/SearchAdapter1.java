package com.example.bhumicloud.gisapplications.Adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bhumicloud.gisapplications.Activity.GetValueListener;
import com.example.bhumicloud.gisapplications.Activity.Search_Activity;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.Search;

import java.util.ArrayList;
import java.util.List;

public class SearchAdapter1 extends  RecyclerView.Adapter<SearchAdapter1.MyViewHolder>  {

    private List<Search> array_list;
    private Activity activity;
    GetValueListener listener;
    String spi_field,spi_operator;


    public SearchAdapter1(List<Search> array_list, Activity activity) {
        this.array_list = array_list;
        this.activity = activity;
        this.listener=(GetValueListener)activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adap_search1, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Search sdto = array_list.get(position);
        holder.tv_name.setText(sdto.getField()+" "+sdto.getOperator()+" "+sdto.getVal());
        holder.EditimageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("Edit");

                LayoutInflater inflater = activity.getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.adap_search, null);

                final Spinner Sp_field=(Spinner) alertLayout. findViewById(R.id.sp_field);
                final  Spinner Sp_operator=(Spinner) alertLayout.findViewById(R.id.sp_operator);
                final EditText  Et_val=(EditText) alertLayout.findViewById(R.id.et_val);
                final EditText Et_val1=(EditText) alertLayout.findViewById(R.id.et_val1);



                Intent intent =activity.getIntent();
                String name= intent.getStringExtra("project_name");
                ArrayList<Search> list=Search_Activity.getValuefromdb(name);
                Et_val.setText(sdto.getVal() );


              //  ArrayAdapter<Search> dataAdapter = new ArrayAdapter<Search>(activity, android.R.layout.simple_spinner_item, list);

                final ArrayAdapter<Search> dataAdapter = new ArrayAdapter<Search>(activity,android.R.layout.simple_spinner_item,list){
                    @Override
                    public View getDropDownView(int position, View convertView,
                                                ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;


                        return view;
                    }
                };
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                Sp_field.setAdapter(dataAdapter);

//                if(Et_val1.getVisibility()==View.VISIBLE)
//                {
//                    Et_val1.setText();
//                }
                List<String> list1 = new ArrayList<String>();
                list1.add("Condition");
                list1.add("Less Than");
                list1.add("Greater Than");
                list1.add("Equals");
                list1.add("Not Equals");
                list1.add("Between");
                list1.add("Like");
                list1.add("Start With");
                list1.add("End With");

              //  ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, list1);

                final ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(activity,android.R.layout.simple_spinner_item,list1){
                    @Override
                    public View getDropDownView(int position, View convertView,
                                                ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;
                      //  tv.setText(""+sdto.getOperator());
                        return view;
                    }
                };

                dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                Sp_operator.setAdapter(dataAdapter1);



                Sp_field.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    //  private int pos = position;
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1,
                                               int p, long arg3)
                    {
                        // TODO Auto-generated method stub
                        Search search = (Search) arg0.getItemAtPosition(p);
                        spi_field=  search.getField();
                        Log.e("spi_field", " "+spi_field);


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0)
                    {
                        // TODO Auto-generated method stub

                    }
                });
                Sp_operator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1,
                                               int p, long arg3)
                    {
                        // TODO Auto-generated method stub
                        spi_operator = (String) arg0.getItemAtPosition(p);
                        if(spi_operator.equalsIgnoreCase("Between"))
                        {
                            Et_val1.setVisibility(View.VISIBLE);
                        }else {
                            Et_val1.setVisibility(View.INVISIBLE);
                        }


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0)
                    {
                        // TODO Auto-generated method stub

                    }
                });


                builder.setView(alertLayout);

                builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String val1 = null;
                       String val= Et_val.getText().toString();
                     //  Log.e("Done", val);
                       if(Et_val1.getVisibility()==View.VISIBLE)
                       {
                            val1= Et_val1.getText().toString();
                       }
                        Log.e("Done", val1 +"  "+val);
                        holder.tv_name.setText(spi_field+" "+spi_operator+" "+val);

                    }
                });

                builder.show();

            }
        });

        holder.CrossimageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                array_list.remove(position);
                notifyDataSetChanged();
                listener.getValue(""+array_list.size());

            }
        });



    }


    @Override
    public int getItemCount() {
        return array_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name;
        public ImageView EditimageView,CrossimageView1;




        public MyViewHolder(View view) {
            super(view);
            EditimageView = (ImageView) view.findViewById(R.id.edit_iv);
            CrossimageView1 = (ImageView) view.findViewById(R.id.cross_iv);

            tv_name=(TextView) view. findViewById(R.id.tv);






        }

    }
}