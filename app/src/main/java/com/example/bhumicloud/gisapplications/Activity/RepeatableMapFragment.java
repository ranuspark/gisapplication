package com.example.bhumicloud.gisapplications.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.example.bhumicloud.gisapplications.RepeatableItemCollectionViewListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.LatLngBounds.Builder;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.example.bhumicloud.gisapplications.maps.GISMarker;
import com.example.bhumicloud.gisapplications.maps.GISMarker.Color;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableItemValue;
import com.example.bhumicloud.gisapplications.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RepeatableMapFragment extends MapViewFragment implements RepeatableItemCollectionView {
    private long mFocusedId = -1;
    private List<RepeatableItemValue> mItems;
    private RepeatableItemCollectionViewListener mListener;
    private final Map<Marker, RepeatableItemValue> mMarkers = new HashMap ();
    private boolean mViewOnlyMode;

    class C11201 implements OnClickListener {
        C11201() {
        }

        public void onClick(View v) {
            ((RepeatableIndexFragment) RepeatableMapFragment.this.getParentFragment()).onAddNewItemOptionSelected();
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        Fragment fragment = getParentFragment();
        if (fragment instanceof RepeatableItemCollectionViewListener) {
            this.mListener = (RepeatableItemCollectionViewListener) fragment;
            return;
        }
        throw new RuntimeException (fragment + " must implement " + RepeatableItemCollectionViewListener.class.getSimpleName());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (!this.mViewOnlyMode) {
            this.mFloatingActionButton.setVisibility(View.VISIBLE);
            this.mFloatingActionButton.setOnClickListener(new C11201());
        }
        return view;
    }

    public List<RepeatableItemValue> getItems() {
        return this.mItems;
    }

    public void setRepeatableItems(List<RepeatableItemValue> items) {
        this.mItems = items;
        if (isResumed()) {
            reloadMapView();
        }
    }

    public void setViewOnlyMode(boolean viewOnlyMode) {
        this.mViewOnlyMode = viewOnlyMode;
    }

    public void setEditMode(boolean editMode) {
    }

    public void zoomToFeature(long recordID, LatLng location) {
        this.mFocusedId = recordID;
        super.zoomToFeature(recordID, location);
    }

    protected void onMapReady() {
        reloadMapView();
    }

    protected void onMarkerInfoWindowClicked(Marker marker) {
        super.onMarkerInfoWindowClicked(marker);
        RepeatableItemValue item = (RepeatableItemValue) this.mMarkers.get(marker);
        if (item != null) {
            this.mListener.onSelected(item);
        }
    }

    private void clearAllMarkers() {
        for (Marker marker : this.mMarkers.keySet()) {
            marker.remove();
        }
        this.mMarkers.clear();
    }

    private void reloadMapView() {
        clearAllMarkers();
        GoogleMap map = getMap();
        if (map != null && this.mItems != null && !this.mItems.isEmpty()) {
            Builder boundsBuilder = LatLngBounds.builder();
            boolean isBuilderPopulated = false;
            for (RepeatableItemValue item : this.mItems) {
                LatLng pos = item.getLatLng();
                if (pos != null) {
                    boundsBuilder.include(pos);
                    isBuilderPopulated = true;
                    MarkerOptions opts = new MarkerOptions ();
                    opts.position(pos);
                    opts.draggable(false);
                    GISMarker.setAnchors(opts);
                    opts.title(StringUtils.correctRightToLeft(getResources(), item.getDisplayValue()));
                    opts.icon(GISMarker.getMarkerBitmapDescriptor(Color.CB0D0C));
                    Marker marker = map.addMarker(opts);
                    this.mMarkers.put(marker, item);
                    if (this.mFocusedId == ((long) item.getIndex())) {
                        marker.showInfoWindow();
                    }
                }
            }
            if (isBuilderPopulated) {
                moveToLatLngBounds(boundsBuilder.build());
            }
        }
    }
}
