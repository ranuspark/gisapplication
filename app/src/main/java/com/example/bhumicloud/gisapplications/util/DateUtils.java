package com.example.bhumicloud.gisapplications.util;

import android.content.Context;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.R;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {
    public static final DateFormat COLUMN_VALUE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ", Locale.US);
    public static final DateFormat DISPLAY_FORMAT = SimpleDateFormat.getDateInstance(1);
    private static final double MILLISECONDS_TO_SECONDS = 1000.0d;
    public static final DateFormat VALUE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    private static final DecimalFormat sDecimalFormat = new DecimalFormat("#");
    private static final SimpleDateFormat sTimestampFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);

    static {
        DecimalFormatSymbols sym = new DecimalFormatSymbols(Locale.US);
        sDecimalFormat.setMinimumFractionDigits(3);
        sDecimalFormat.setMaximumFractionDigits(10);
        sDecimalFormat.setDecimalFormatSymbols(sym);
        sDecimalFormat.setGroupingUsed(false);
        sTimestampFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public static DateFormat getTimestampFormatter() {
        return sTimestampFormat;
    }

    public static Date parseTimestampString(String aDateStr) {
        try {
            return getTimestampFormatter().parse(aDateStr);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getTimestampString(Date aDate) {
        return getTimestampFormatter().format(aDate);
    }

    public static Date parseEpochTimestamp(String aTimestamp) {
        if (TextUtils.isEmpty(aTimestamp)) {
            return null;
        }
        double seconds;
        try {
            seconds = Double.parseDouble(aTimestamp);
        } catch (NumberFormatException formatException) {
            try {
                seconds = new DecimalFormat().parse(aTimestamp).doubleValue();
            } catch (ParseException e) {
                throw new RuntimeException(formatException);
            }
        }
        return new Date((long) (MILLISECONDS_TO_SECONDS * seconds));
    }

    public static String formatEpochTimestamp(Date aDate) {
        if (aDate != null) {
            return String.valueOf((int) (((double) aDate.getTime()) / MILLISECONDS_TO_SECONDS));
        }
        return null;
    }

    public static CharSequence getRelativeTimeSpanString(Context context, Date date) {
        return getRelativeTimeSpanString(context, date.getTime());
    }

    public static CharSequence getRelativeTimeSpanString(Context context) {
        return getRelativeTimeSpanString(context, -1);
    }

    public static CharSequence getRelativeTimeSpanString(Context context, long time) {
        if (time < 0) {
            return context.getString(R.string.updated_unknown_ago);
        }
        if (Math.abs(System.currentTimeMillis() - time) < 1000) {
            return context.getString(R.string.updated_just_now);
        }
        long j = time;
        return context.getString(R.string.updated_ago, new Object[]{android.text.format.DateUtils.getRelativeTimeSpanString(j, System.currentTimeMillis(), 1000, (0 | 16) | 4)});
    }

    public static String formatLocalizedStringFromMachineString(String value) {
        Date date = null;
        try {
            date = VALUE_FORMAT.parse(value);
        } catch (ParseException e) {
        }
        if (date != null) {
            return DISPLAY_FORMAT.format(date);
        }
        return value;
    }
}
