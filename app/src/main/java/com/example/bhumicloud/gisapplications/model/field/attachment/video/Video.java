package com.example.bhumicloud.gisapplications.model.field.attachment.video;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;

import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment.Type;
import com.example.bhumicloud.gisapplications.util.MimeUtils;

import java.io.File;
import java.util.List;

public class Video extends Attachment {
    public static final Creator<Video> CREATOR = new C11841();

    static class C11841 implements Creator<Video> {
        C11841() {
        }

        public Video createFromParcel(Parcel source) {
            return new Video(source);
        }

        public Video[] newArray(int size) {
            return new Video[size];
        }
    }

    public static Uri generateVideoURI(Context context, String key, String contentType) {
        return Uri.fromFile(generateVideoFile(context, key, contentType));
    }

    public static File generateVideoFile(Context context, String key, String contentType) {
        return new File(Attachment.getStorageDirectory(context), "video-" + key + "." + MimeUtils.getExtension(contentType));
    }

    public static List<Attachment> getVideos(Account account, Record record, boolean includeUploaded) {
        return Attachment.getAttachments(account, record, includeUploaded, "video");
    }

    public Video(String key) {
        super(key);
    }

    public Video(Cursor cursor) {
        super(cursor);
    }

    private Video(Parcel parcel) {
        super(parcel);
    }

    public File generateFile(Context context, String contentType) {
        return generateVideoFile(context, getRemoteID(), contentType);
    }

    public Type getType() {
        return Type.VIDEO;
    }

    public boolean canEqual(Object other) {
        return other instanceof Video;
    }
}
