package com.example.bhumicloud.gisapplications.dossier;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.field.choice.ChoiceList;
import com.example.bhumicloud.gisapplications.util.CursorUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class PersistentStore extends SQLiteOpenHelper {

    private static final String DEBUG_TAG = PersistentStore.class.getSimpleName();
    private static final String FILENAME = "BhumiGIS.db";
    private static final int SCHEMA_VERSION = 3;
    private static PersistentStore sSharedInstance;
    private static Context Maincontext;


    public static boolean needUpgrade(Context context) {
      File _dbname  = getDatabasePath ( context );
      boolean _exits = _dbname.exists ();
      boolean needUpgrade;
      if(_exits == false)
      {
          return true;
      }
      return false;
    }

    public static synchronized PersistentStore getInstance(Context context) {
        PersistentStore persistentStore;
        synchronized (PersistentStore.class) {
            if (sSharedInstance != null) {
                persistentStore = sSharedInstance;
            } else {
                sSharedInstance = new PersistentStore(context.getApplicationContext());
                Maincontext = context.getApplicationContext();
                sSharedInstance.getWritableDatabase();
                sSharedInstance.close();
                persistentStore = sSharedInstance;
            }
        }
        return persistentStore;
    }

    public static File getDatabasePath(Context context) {
        return context.getDatabasePath(FILENAME);
    }

    private PersistentStore(Context context) {
        super(context, FILENAME, null, 53);
    }

    public void onConfigure(SQLiteDatabase db) {
        db.enableWriteAheadLogging();
        db.execSQL("PRAGMA synchronous=NORMAL");
    }

    public void onCreate(SQLiteDatabase db) {
        GISLogger.log(DEBUG_TAG, "Creating new database...");
        Context context1 = GIS.getInstance ();
        runSQL(db, context1, R.raw.db_c_table_accounts);
        runSQL(db, context1, R.raw.db_c_table_projects);
        runSQL(db, context1, R.raw.db_c_table_forms);
        runSQL(db, context1, R.raw.db_c_table_choice_lists);
        runSQL(db, context1, R.raw.db_c_table_classification_sets);
        runSQL(db, context1, R.raw.db_c_table_records);
        runSQL(db, context1, R.raw.db_c_table_map_layers);
        runSQL(db, context1, R.raw.db_c_table_attachments);
        runSQL(db, context1, R.raw.db_c_table_trash_can);
        runSQL(db, context1, R.raw.db_c_table_storage);
        runSQL(db, context1, R.raw.db_c_table_signature);
        runSQL(db, context1, R.raw.db_c_table_sync_states);
        runSingleSQL(db, context1, R.raw.db_c_trigger_record_cascade);
        runSingleSQL(db, context1, R.raw.db_c_trigger_attachment_trash);
        runSingleSQL(db, context1, R.raw.db_c_trigger_map_layer_trash);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        GISLogger.log(DEBUG_TAG, "Upgrading database schema version from... " + oldVersion +" to " + newVersion);
        Context context1 = Maincontext;
        switch (oldVersion) {
            default: return;
        }
    }

    private void migrateFormJsonValuesToColumns(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE Forms ADD COLUMN report_templates TEXT");
        db.execSQL("ALTER TABLE Forms ADD COLUMN status_field TEXT");
        Cursor cursor = CursorUtils.queryTableWithLargeColumn(db, Form.TABLE_NAME, Collections.singletonList("_id"), "json", null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String formJSONAsString = CursorUtils.fetchLargeColumn(db, Form.TABLE_NAME, cursor, "json");
            if (TextUtils.isEmpty(formJSONAsString)) {
                cursor.moveToNext();
            } else {
                try {
                    Map formJSON = JSONUtils.objectFromJSON(formJSONAsString);
                    ArrayList<Object> elements = JSONUtils.getArrayList(formJSON, Form.COLUMN_ELEMENTS);
                    HashMap<String, Object> status_field = JSONUtils.getHashMap(formJSON, Form.COLUMN_STATUS_FIELD);
                    ArrayList<Object> report_templates = JSONUtils.getArrayList(formJSON, Form.COLUMN_REPORT_TEMPLATES);
                    ContentValues values = new ContentValues();
                    if (!(elements == null || elements.isEmpty())) {
                        values.put(Form.COLUMN_ELEMENTS, JSONUtils.toJSONString(elements));
                    }
                    if (!(status_field == null || status_field.isEmpty())) {
                        values.put(Form.COLUMN_STATUS_FIELD, JSONUtils.toJSONString(status_field));
                    }
                    if (!(report_templates == null || report_templates.isEmpty())) {
                        values.put(Form.COLUMN_REPORT_TEMPLATES, JSONUtils.toJSONString(report_templates));
                    }
                    if (values.size() > 0) {
                        SQLiteDatabase sQLiteDatabase = db;
                        sQLiteDatabase.update(Form.TABLE_NAME, values, "_id = ?", new String[]{CursorUtils.getString(cursor, "_id")});
                    }
                } catch (Throwable e) {
                    GISLogger.log(e);
                }
                cursor.moveToNext();
            }
        }
        cursor.close();
    }

    private void migrateChoiceListsJsonValuesToColumns(SQLiteDatabase db) {
        Cursor cursor = CursorUtils.queryTableWithLargeColumn(db, ChoiceList.TABLE_NAME, Collections.singletonList("_id"), "json", null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String choiceListJSONAsString = CursorUtils.fetchLargeColumn(db, ChoiceList.TABLE_NAME, cursor, "json");
            if (TextUtils.isEmpty(choiceListJSONAsString)) {
                cursor.moveToNext();
            } else {
                try {
                    ArrayList<Object> choices = JSONUtils.getArrayList(JSONUtils.objectFromJSON(choiceListJSONAsString), ChoiceList.COLUMN_CHOICES);
                    ContentValues values = new ContentValues();
                    if (!(choices == null || choices.isEmpty())) {
                        values.put(ChoiceList.COLUMN_CHOICES, JSONUtils.toJSONString(choices));
                    }
                    if (values.size() > 0) {
                        db.update(ChoiceList.TABLE_NAME, values, "_id = ?", new String[]{CursorUtils.getString(cursor, "_id")});
                    }
                } catch (Throwable e) {
                    GISLogger.log(e);
                }
                cursor.moveToNext();
            }
        }
        cursor.close();
    }

    private void runSQL(SQLiteDatabase db, Context context, int sqlFileID) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(sqlFileID)));
        try {
            ArrayList<String> statements = new ArrayList();
            StringBuilder builder = new StringBuilder();
            while (reader.ready()) {
                String line = reader.readLine();
                builder.append(line);
                if (line.endsWith(";")) {
                    statements.add(builder.toString());
                    builder = new StringBuilder();
                }
            }
            Iterator it = statements.iterator();
            while (it.hasNext()) {
                String statement = (String) it.next();
                GISLogger.log(DEBUG_TAG, "Executing: " + statement);
                db.execSQL(statement);
            }
        } catch (Throwable e) {
            GISLogger.log(e);
        }
    }

    private void runSingleSQL(SQLiteDatabase db, Context context, int sqlFileID) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(sqlFileID)));
        try {
            StringBuilder builder = new StringBuilder();
            while (reader.ready()) {
                builder.append(reader.readLine());
            }
            db.execSQL(builder.toString());
        } catch (Throwable e) {
            GISLogger.log(e);
        }
    }

    private ArrayList<String> getTableColumns(SQLiteDatabase db, String table) {
        ArrayList<String> columns = new ArrayList();
        Cursor c = db.rawQuery(String.format("PRAGMA table_info(%s)", new Object[]{table}), null);
        if (c != null) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                columns.add(CursorUtils.getString(c, "name"));
                c.moveToNext();
            }
            c.close();
        }
        return columns;
    }

    private boolean columnExists(SQLiteDatabase db, String table, String column) {
        return getTableColumns(db, table).contains(column);
    }

}