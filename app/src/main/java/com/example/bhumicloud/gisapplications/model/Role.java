package com.example.bhumicloud.gisapplications.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import java.util.HashMap;
import java.util.Map;


public class Role implements Parcelable {
    public static final Creator<Role> CREATOR = new Creator<Role> () {
        @Override
        public Role createFromParcel(Parcel source) {
            return new Role(source);
        }

        @Override
        public Role[] newArray(int size) {
            return new Role[size];
        }
    };

    public static final String PERMISSION_CHANGE_PROJECT = "can_change_project";
    public static final String PERMISSION_CHANGE_STATUS = "can_change_status";
    public static final String PERMISSION_CREATE_RECORDS = "can_create_records";
    public static final String PERMISSION_GENERATE_REPORTS = "can_run_reports";
    public static final String PERMISSION_UPDATE_RECORDS = "can_update_records";
    public static final String ROLE_NAME = "name";
    private final Map<String, Object> mRoleJSON;

    public Role(Map<String, Object> json) {
        this.mRoleJSON = json;
    }

    private Role(Parcel source) {
        boolean isRoleJSON = true;
        if (source.readInt() != 1) {
            isRoleJSON = false;
        }
        this.mRoleJSON = isRoleJSON ? new HashMap() : null;
        if (isRoleJSON) {
            try {
                this.mRoleJSON.putAll(JSONUtils.objectFromJSON(source.readString()));
            } catch (Throwable e) {
                GISLogger.log(e);
            }
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        if (this.mRoleJSON == null) {
            dest.writeInt(0);
            return;
        }
        dest.writeInt(1);
        dest.writeString(JSONUtils.toJSONString(this.mRoleJSON));
    }

    public boolean canChangeRecordStatus() {
        return can(PERMISSION_CHANGE_STATUS);
    }

    public boolean canChangeRecordProject() {
        return can(PERMISSION_CHANGE_PROJECT);
    }

    public boolean canCreateRecords() {
        return can(PERMISSION_CREATE_RECORDS);
    }

    public boolean canUpdateRecords() {
        return can(PERMISSION_UPDATE_RECORDS);
    }

    public boolean canGenerateReports() {
        return can(PERMISSION_GENERATE_REPORTS);
    }

    public boolean isOwner() {
        return "owner".equalsIgnoreCase(getName());
    }

    public String getName() {
        return JSONUtils.getString(this.mRoleJSON, "name");
    }

    public Map toJSON() {
        return this.mRoleJSON;
    }

    private boolean can(String permission) {
        return JSONUtils.getABoolean(this.mRoleJSON, permission, true);
    }
}

