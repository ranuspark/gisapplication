package com.example.bhumicloud.gisapplications.model.field.attachment;

import android.os.Parcel;

import com.example.bhumicloud.gisapplications.model.field.Element;

import java.util.Map;

public abstract class MediaElement extends Element {
    private Boolean mOverrideMediaCaptureEnabled;
    private Boolean mOverrideMediaGalleryEnabled;

    public MediaElement(Element parent, Map json) {
        super(parent, json);
    }

    public MediaElement(Parcel parcel) {
        super(parcel);
    }

    public void setOverrideMediaGalleryEnabled(Boolean override) {
        this.mOverrideMediaGalleryEnabled = override;
    }

    public void setOverrideMediaCaptureEnabled(Boolean override) {
        this.mOverrideMediaCaptureEnabled = override;
    }

    public Boolean getOverrideMediaGalleryEnabled() {
        return this.mOverrideMediaGalleryEnabled;
    }

    public Boolean getOverrideMediaCaptureEnabled() {
        return this.mOverrideMediaCaptureEnabled;
    }

    public boolean isMediaGalleryEnabled() {
        return this.mOverrideMediaGalleryEnabled != null ? this.mOverrideMediaGalleryEnabled.booleanValue() : true;
    }

    public boolean isMediaCaptureEnabled() {
        return this.mOverrideMediaCaptureEnabled != null ? this.mOverrideMediaCaptureEnabled.booleanValue() : true;
    }
}
