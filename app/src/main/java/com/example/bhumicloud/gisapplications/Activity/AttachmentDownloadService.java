package com.example.bhumicloud.gisapplications.Activity;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.apiLayer.Client;
import com.example.bhumicloud.gisapplications.apiLayer.exception.ResourceNotFoundException;
import  com.example.bhumicloud.gisapplications.model.Account;
import  com.example.bhumicloud.gisapplications.model.Record;
import  com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import  com.example.bhumicloud.gisapplications.model.field.attachment.Attachment.Type;
import  com.example.bhumicloud.gisapplications.util.FileUtils;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import  com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class AttachmentDownloadService extends Service {
    public static final String EXTRA_ACCOUNT_ID = "EXTRA_ACCOUNT_ID";
    public static final String EXTRA_RECORD_ID = "EXTRA_RECORD_ID";
    public static final String EXTRA_REMOTE_ID = "EXTRA_REMOTE_ID";
    public static final String EXTRA_TYPE = "EXTRA_TYPE";
    private Set<Observer> mObservers;
    private Map<String, Worker> mWorkers;

    public static class AttachmentDownloadServiceBinder extends Binder {
        private AttachmentDownloadService mService;

        public AttachmentDownloadServiceBinder(AttachmentDownloadService service) {
            this.mService = service;
        }

        public AttachmentDownloadService getService() {
            return this.mService;
        }
    }

    public interface Observer {
        void onAttachmentDownloadFailed(String str, Exception exception);

        void onAttachmentDownloadFinished(String str);

        void onAttachmentDownloadProgress(String str, int i);

        void onAttachmentDownloadStarted(String str);
    }

    private static class Worker extends AsyncTask<Void, Integer, Void> {
        private final String mAttachmentID;
        private final Client mClient;
        private final Context mContext = this.mService.getApplicationContext();
        private Exception mException;
        private int mProgress;
        private final String mRecordID;
        private AttachmentDownloadService mService = null;
        private final Type mType;

        public Worker(AttachmentDownloadService service, Account account, int type, String remoteID, String recordID) {
            this.mService = service;
            this.mType = Type.valueOf(type);
            this.mAttachmentID = remoteID;
            this.mRecordID = recordID;
            this.mClient = new Client(this.mContext, account);
        }

        public Worker start() {
            executeOnExecutor(THREAD_POOL_EXECUTOR, new Void[0]);
            return this;
        }

        public int getProgress() {
            return this.mProgress;
        }

        protected void onPreExecute() {
            this.mService.onAttachmentDownloadStarted(this.mAttachmentID);
        }

        protected Void doInBackground(Void... params) {
            try {
                Map json = this.mClient.getAttachmentJSON(this.mAttachmentID, this.mType);
                String originalURL = JSONUtils.getString(json, "original");
                String contentType = JSONUtils.getString(json, "content_type");
                Attachment attachment = Attachment.findByRemoteID(this.mAttachmentID);
                if (attachment == null) {
                    attachment = Attachment.getTypedInstance(this.mType, this.mAttachmentID);
                }
                if (attachment != null) {
                    downloadAttachment(originalURL, attachment, attachment.generateFile(this.mContext, contentType));
                }
            } catch (IOException e) {
                this.mException = e;
            }
            return null;
        }

        protected void onProgressUpdate(Integer... values) {
            this.mProgress = values[0].intValue();
            this.mService.onAttachmentDownloadProgress(this.mAttachmentID, this.mProgress);
        }

        protected void onPostExecute(Void result) {
            if (this.mException != null) {
                this.mService.onAttachmentDownloadException(this.mAttachmentID, this.mException);
            } else {
                this.mService.onAttachmentDownloadFinished(this.mAttachmentID);
            }
        }

        private void downloadAttachment(String url, Attachment attachment, File file) throws IOException {
            try {
                if (TextUtils.isEmpty ( url )) {
                    throw new ResourceNotFoundException ();
                }
                Response<ResponseBody> response = this.mClient.downloadPublicItem ( url );
                InputStream content = ((ResponseBody) response.body ()).byteStream ();
                OutputStream output = new FileOutputStream ( file );
                int progress = 0;
                float bytesReceived = 0.0f;
                long contentLength = ((ResponseBody) response.body ()).contentLength ();
                byte[] buffer = new byte[1024];
                while (true) {
                    int length = content.read ( buffer );
                    if (length < 0 || isCancelled ()) {
                        output.flush ();
                        output.close ();
                    } else {
                        bytesReceived += (float) length;
                        output.write ( buffer, 0, length );
                        if (contentLength > 0) {
                            int currentProgress = (int) ((bytesReceived / ((float) contentLength)) * 100.0f);
                            if (currentProgress > progress) {
                                publishProgress ( new Integer[]{Integer.valueOf ( progress )} );
                                progress = currentProgress;
                            }
                        }
                    }
                }
                //output.flush();
                //output.close();
               // if (isCancelled ()) {
                //    file.delete ();
                  //  return;
               // }
                //attachment.setPathOne ( file, FileUtils.generateThumbnailPath ( this.mService, file.getAbsolutePath () ) );
                //attachment.setRecordID ( this.mRecordID );
                //attachment.setUploaded ( true );
                //attachment.save ();
            } catch (Exception e)
            {
                GISLogger.log(e);
            }
        }
    }

    public static void bind(Context context, ServiceConnection connection) {
        context.bindService(new Intent (context, AttachmentDownloadService.class), connection, Context.BIND_AUTO_CREATE);
    }

    public static void unbind(Context context, ServiceConnection connection) {
        context.unbindService(connection);
    }

    public static void startDownload(Context context, Account account, Record record, String mediaID, Type type) {
        Intent intent = new Intent (context, AttachmentDownloadService.class);
        intent.putExtra(EXTRA_TYPE, type.ordinal());
        if (record != null) {
            intent.putExtra(EXTRA_RECORD_ID, record.getUniqueID());
        }
        if (account != null) {
            intent.putExtra(EXTRA_ACCOUNT_ID, account.getRowID());
        }
        if (mediaID != null) {
            intent.putExtra(EXTRA_REMOTE_ID, mediaID);
        }
        context.startService(intent);
    }

    public void onCreate() {
        super.onCreate();
        this.mObservers = Collections.synchronizedSet(new HashSet ());
        this.mWorkers = new HashMap ();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                Account account = Account.getAccount(extras.getLong(EXTRA_ACCOUNT_ID));
                if (account != null) {
                    int type = extras.getInt(EXTRA_TYPE);
                    String remote = extras.getString(EXTRA_REMOTE_ID);
                    String record = extras.getString(EXTRA_RECORD_ID);
                    if (!(TextUtils.isEmpty(remote) || TextUtils.isEmpty(record))) {
                        this.mWorkers.put(remote, new Worker(this, account, type, remote, record).start());
                    }
                }
            }
        }
        return Service.START_NOT_STICKY;
    }

    public IBinder onBind(Intent intent) {
        return new AttachmentDownloadServiceBinder(this);
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.mObservers != null) {
            this.mObservers.clear();
        }
        if (this.mWorkers != null) {
            for (Worker worker : this.mWorkers.values()) {
                worker.cancel(false);
            }
            this.mWorkers.clear();
        }
    }

    public void registerObserver(Observer observer) {
        if (observer != null) {
            this.mObservers.add(observer);
        }
    }

    public void unregisterObserver(Observer observer) {
        if (observer != null) {
            this.mObservers.remove(observer);
        }
    }

    public int getDownloadProgress(String attachmentID) {
        Worker worker = (Worker) this.mWorkers.get(attachmentID);
        if (worker != null) {
            return worker.getProgress();
        }
        return -1;
    }

    private void onAttachmentDownloadStarted(String attachment) {
        for (Observer ob : this.mObservers) {
            ob.onAttachmentDownloadStarted(attachment);
        }
    }

    private void onAttachmentDownloadProgress(String attachment, int progress) {
        for (Observer ob : this.mObservers) {
            ob.onAttachmentDownloadProgress(attachment, progress);
        }
    }

    private void onAttachmentDownloadFinished(String attachment) {
        for (Observer ob : this.mObservers) {
            ob.onAttachmentDownloadFinished(attachment);
        }
        this.mWorkers.remove(attachment);
    }

    private void onAttachmentDownloadException(String attachment, Exception exception) {
        for (Observer ob : this.mObservers) {
            ob.onAttachmentDownloadFailed(attachment, exception);
        }
        this.mWorkers.remove(attachment);
    }
}
