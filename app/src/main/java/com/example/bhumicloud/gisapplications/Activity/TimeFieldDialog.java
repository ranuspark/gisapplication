package com.example.bhumicloud.gisapplications.Activity;

import android.annotation.TargetApi;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TimePicker;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.field.temporal.time.TimeValue;
import com.example.bhumicloud.gisapplications.util.DeviceInfo;
import com.example.bhumicloud.gisapplications.util.GISLogger;

import java.util.Calendar;
import java.util.Date;

public class TimeFieldDialog extends ThemedDialogFragment {
    private static final String STATE_CALENDAR_TIME = "STATE_CALENDAR_TIME";
    private final Calendar mCalendar = Calendar.getInstance();
    private TimeFieldDialogListener mListener;
    private TimePicker mTimePicker;

    public interface TimeFieldDialogListener {
        void onTimeValueChanged(Date date);
    }

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        setNegativeButtonText((int) R.string.clear);
        setNeutralButtonText((int) R.string.now);
        setPositiveButtonText((int) R.string.done);
        if (savedState != null) {
            this.mCalendar.setTimeInMillis(savedState.getLong(STATE_CALENDAR_TIME));
        }
    }

    public View onCreateContentView(LayoutInflater inflater, ViewGroup root, Bundle savedState) {
        View view = inflater.inflate(R.layout.dialog_time_field, root, false);
        this.mTimePicker = (TimePicker) view.findViewById(R.id.time_picker);
        this.mTimePicker.setTag("time_picker");
        configureTimePicker();
        return view;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(STATE_CALENDAR_TIME, this.mCalendar.getTimeInMillis());
    }

    public void setTimeValue(TimeValue value) {
        Date time = null;
        if (value != null) {
            try {
                time = value.getTime();
            } catch (Throwable e) {
                GISLogger.log(e);
            }
        }
        if (time == null) {
            time = new Date ();
        }
        this.mCalendar.setTime(time);
    }

    public void setTimeFieldDialogListener(TimeFieldDialogListener listener) {
        this.mListener = listener;
    }

    protected void onNegativeButtonClicked() {
        super.onNegativeButtonClicked();
        notifyTimeValueChanged(null);
        dismissAllowingStateLoss();
    }

    protected void onNeutralButtonClicked() {
        super.onNeutralButtonClicked();
        this.mCalendar.setTime(new Date ());
        configureTimePicker();
        onPositiveButtonClicked();
    }

    protected void onPositiveButtonClicked() {
        super.onPositiveButtonClicked();
        commitTimeSelection();
        dismissAllowingStateLoss();
    }

    @TargetApi(23)
    private void configureTimePicker() {
        int h = this.mCalendar.get(Calendar.HOUR_OF_DAY);
        int m = this.mCalendar.get(Calendar.MINUTE);
        if (DeviceInfo.isMarshmallow()) {
            this.mTimePicker.setHour(h);
            this.mTimePicker.setMinute(m);
        } else {
            this.mTimePicker.setCurrentHour( Integer.valueOf(h));
            this.mTimePicker.setCurrentMinute( Integer.valueOf(m));
        }
        this.mTimePicker.setIs24HourView( Boolean.valueOf( DateFormat.is24HourFormat(getActivity())));
    }

    @TargetApi(23)
    private void commitTimeSelection() {
        int h = DeviceInfo.isMarshmallow() ? this.mTimePicker.getHour() : this.mTimePicker.getCurrentHour().intValue();
        int m = DeviceInfo.isMarshmallow() ? this.mTimePicker.getMinute() : this.mTimePicker.getCurrentMinute().intValue();
        this.mCalendar.set(Calendar.HOUR_OF_DAY, h);
        this.mCalendar.set(Calendar.MINUTE, m);
        notifyTimeValueChanged(this.mCalendar.getTime());
    }

    private void notifyTimeValueChanged(Date time) {
        if (this.mListener != null) {
            this.mListener.onTimeValueChanged(time);
        }
    }
}
