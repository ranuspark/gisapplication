package com.example.bhumicloud.gisapplications.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.util.CursorUtils;
import java.util.Collections;


public class Storage {
    public static final String COLUMN_ACCOUNT_ID = "account_id";
    public static final String COLUMN_KEY = "key";
    public static final String COLUMN_ROW_ID = "_id";
    public static final String COLUMN_SCOPE = "scope";
    public static final String COLUMN_VALUE = "value";
    public static final String TABLE_NAME = "storage";
    private long mAccountID;
    private String mScope = "default";

    public Storage(Account account) {
        this.mAccountID = account.getRowID();
    }

    public Number getLength() {
        return (Number) runQuery(new String[]{"COUNT(1)"}, "account_id = ? AND scope = ?", new String[]{String.valueOf(this.mAccountID), this.mScope}, null, null, null, null);
    }

    public String getKey(int index) {
        return (String) runQuery(new String[]{COLUMN_KEY}, "account_id = ? AND scope = ?", new String[]{String.valueOf(this.mAccountID), this.mScope}, null, null, COLUMN_KEY, index + ",1");
    }

    public String getItem(String key) {
        SQLiteDatabase db = GIS.getDatabase();
        Cursor c = CursorUtils.queryTableWithLargeColumn(db, TABLE_NAME, Collections.singletonList("_id"), "value", "account_id = ? AND scope = ? AND key = ?", new String[]{String.valueOf(this.mAccountID), this.mScope, key}, null);
        c.moveToFirst();
        String result = null;
        if (!c.isAfterLast()) {
            result = CursorUtils.fetchLargeColumn(db, TABLE_NAME, c, "value");
        }
        c.close();
        return result;
    }

    public void setItem(String key, String value) {
        removeItem(key);
        if (value != null) {
            SQLiteDatabase db = GIS.getDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("account_id", Long.valueOf(this.mAccountID));
            contentValues.put("scope", this.mScope);
            contentValues.put(COLUMN_KEY, key);
            contentValues.put("value", value);
            db.insert(TABLE_NAME, null, contentValues);
        }
    }

    public void removeItem(String key) {
        GIS.getDatabase().delete(TABLE_NAME, "account_id = ? AND scope = ? AND key = ?", new String[]{String.valueOf(this.mAccountID), this.mScope, key});
    }

    public void clear() {
        GIS.getDatabase().delete(TABLE_NAME, "account_id = ? AND scope = ?", new String[]{String.valueOf(this.mAccountID), this.mScope});
    }

    private Object runQuery(String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        Object obj = null;
        Cursor cursor = GIS.getDatabase().query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
        if (cursor != null) {
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                obj = CursorUtils.getObject(cursor, 0);
            }
            cursor.close();
        }
        return obj;
    }
}

