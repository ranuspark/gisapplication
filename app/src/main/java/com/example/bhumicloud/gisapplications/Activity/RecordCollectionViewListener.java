package com.example.bhumicloud.gisapplications.Activity;

public interface RecordCollectionViewListener {
    void onDeleteRecord(long j);

    void onDuplicateRecord(long j, boolean z, boolean z2);

    void onGenerateReport(long j);

    void onNewRecordOptionSelected();

    void onRecordSelected(long j);

    void onShowRecordLocation(long j, double d, double d2);
}
