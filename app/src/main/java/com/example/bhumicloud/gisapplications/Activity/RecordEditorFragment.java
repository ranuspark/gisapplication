package com.example.bhumicloud.gisapplications.Activity;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Toast;

import com.example.bhumicloud.gisapplications.Activity.AddressFieldDialog.AddressFieldDialogListener;
import com.example.bhumicloud.gisapplications.Activity.BarcodeFieldDialog.BarcodeFieldDialogListener;
import com.example.bhumicloud.gisapplications.Activity.ChoiceFieldDialog.OnChoiceFieldSelectionListener;
import com.example.bhumicloud.gisapplications.Activity.ClassificationFieldDialog.ClassificationFieldDialogListener;
import com.example.bhumicloud.gisapplications.Activity.DateFieldDialog.DateFieldDialogListener;
import com.example.bhumicloud.gisapplications.Activity.DiscardChangesDialog.DiscardChangesDialogListener;
import com.example.bhumicloud.gisapplications.Activity.EditCaptionDialog.OnCaptionEditedListener;
import com.example.bhumicloud.gisapplications.Activity.FeatureValidationErrorDialog.FeatureValidationErrorDialogListener;
import com.example.bhumicloud.gisapplications.Activity.ProjectPickerDialog.ProjectPickerDialogListener;
import com.example.bhumicloud.gisapplications.Activity.RecordStatusDialog.OnStatusOptionSelectedListener;
import com.example.bhumicloud.gisapplications.Activity.RemoveRecordLinkDialog.RemoveRecordLinkDialogListener;
import com.example.bhumicloud.gisapplications.Activity.RepeatableIndexFragment.RepeatableIndexFragmentListener;
import com.example.bhumicloud.gisapplications.Activity.TimeFieldDialog.TimeFieldDialogListener;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.View.AddressElementView;
import com.example.bhumicloud.gisapplications.View.AddressElementView.AddressElementViewListener;
import com.example.bhumicloud.gisapplications.View.BarcodeElementView;
import com.example.bhumicloud.gisapplications.View.BarcodeElementView.BarcodeElementViewListener;
import com.example.bhumicloud.gisapplications.View.CalculatedElementView;
import com.example.bhumicloud.gisapplications.View.ChoiceElementView;
import com.example.bhumicloud.gisapplications.View.ChoiceElementView.ChoiceElementViewListener;
import com.example.bhumicloud.gisapplications.View.ClassificationElementView;
import com.example.bhumicloud.gisapplications.View.ClassificationElementView.ClassificationElementViewListener;
import com.example.bhumicloud.gisapplications.View.DateElementView;
import com.example.bhumicloud.gisapplications.View.DateElementView.DateElementViewListener;
import com.example.bhumicloud.gisapplications.View.ElementView;
import com.example.bhumicloud.gisapplications.View.ElementView.ElementViewDataSource;
import com.example.bhumicloud.gisapplications.View.ElementView.ElementViewListener;
import com.example.bhumicloud.gisapplications.View.HyperlinkElementView;
import com.example.bhumicloud.gisapplications.View.HyperlinkElementView.HyperlinkElementViewListener;
import com.example.bhumicloud.gisapplications.View.InlineSectionElementView;
import com.example.bhumicloud.gisapplications.View.LabelElementView;
import com.example.bhumicloud.gisapplications.View.NestedSectionElementView;
import com.example.bhumicloud.gisapplications.View.NestedSectionElementView.NestedSectionElementViewListener;
import com.example.bhumicloud.gisapplications.View.RecordLinkElementView;
import com.example.bhumicloud.gisapplications.View.RecordLinkElementView.RecordLinkElementViewListener;
import com.example.bhumicloud.gisapplications.View.RepeatableElementView;
import com.example.bhumicloud.gisapplications.View.RepeatableElementView.RepeatableElementViewListener;
import com.example.bhumicloud.gisapplications.View.TextElementView;
import com.example.bhumicloud.gisapplications.View.TimeElementView;
import com.example.bhumicloud.gisapplications.View.TimeElementView.TimeElementViewListener;
import com.example.bhumicloud.gisapplications.View.YesNoElementView;
import com.example.bhumicloud.gisapplications.View.YesNoElementView.YesNoElementViewListener;
import com.example.bhumicloud.gisapplications.View.media.MediaElementView.MediaElementViewListener;
import com.example.bhumicloud.gisapplications.View.media.audio.AudioElementView;
import com.example.bhumicloud.gisapplications.View.media.photo.PhotoElementView;
import com.example.bhumicloud.gisapplications.View.media.signature.SignatureElementView;
import com.example.bhumicloud.gisapplications.View.media.signature.SignatureElementView.SignatureElementViewListener;
import com.example.bhumicloud.gisapplications.View.media.video.VideoElementView;
import com.example.bhumicloud.gisapplications.dossier.RecordPredicate;
import com.example.bhumicloud.gisapplications.geoJson.GeoJSONPoint;
import com.example.bhumicloud.gisapplications.location.Address;
import com.example.bhumicloud.gisapplications.location.ReverseGeocodeTask;
import com.example.bhumicloud.gisapplications.location.ReverseGeocodeTask.Callback;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.DefaultValues;
import com.example.bhumicloud.gisapplications.model.Feature;
import com.example.bhumicloud.gisapplications.model.FeatureEditState;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.FormValueContainer;
import com.example.bhumicloud.gisapplications.model.Project;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.Role;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.ElementContainer;
import com.example.bhumicloud.gisapplications.model.field.LabelElement;
import com.example.bhumicloud.gisapplications.model.field.SectionElement;
import com.example.bhumicloud.gisapplications.model.field.address.AddressElement;
import com.example.bhumicloud.gisapplications.model.field.address.AddressValue;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment.Type;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaValue;
import com.example.bhumicloud.gisapplications.model.field.attachment.audio.AudioElement;
import com.example.bhumicloud.gisapplications.model.field.attachment.audio.AudioItemValue;
import com.example.bhumicloud.gisapplications.model.field.attachment.photo.Photo;
import com.example.bhumicloud.gisapplications.model.field.attachment.photo.PhotoElement;
import com.example.bhumicloud.gisapplications.model.field.attachment.photo.PhotoItemValue;
import com.example.bhumicloud.gisapplications.model.field.attachment.photo.PhotoValue;
import com.example.bhumicloud.gisapplications.model.field.attachment.signature.Signature;
import com.example.bhumicloud.gisapplications.model.field.attachment.signature.SignatureElement;
import com.example.bhumicloud.gisapplications.model.field.attachment.signature.SignatureValue;
import com.example.bhumicloud.gisapplications.model.field.attachment.video.VideoElement;
import com.example.bhumicloud.gisapplications.model.field.attachment.video.VideoItemValue;
import com.example.bhumicloud.gisapplications.model.field.choice.ChoiceElement;
import com.example.bhumicloud.gisapplications.model.field.choice.ChoiceValue;
import com.example.bhumicloud.gisapplications.model.field.classification.ClassificationElement;
import com.example.bhumicloud.gisapplications.model.field.classification.ClassificationValue;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkDefault;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkElement;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkItemValue;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkValue;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableElement;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableItemValue;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableValue;
import com.example.bhumicloud.gisapplications.model.field.temporal.date.DateElement;
import com.example.bhumicloud.gisapplications.model.field.temporal.date.DateValue;
import com.example.bhumicloud.gisapplications.model.field.temporal.time.TimeElement;
import com.example.bhumicloud.gisapplications.model.field.temporal.time.TimeValue;
import com.example.bhumicloud.gisapplications.model.field.textual.barcode.BarcodeElement;
import com.example.bhumicloud.gisapplications.model.field.textual.barcode.BarcodeValue;
import com.example.bhumicloud.gisapplications.model.field.textual.calculated.CalculatedElement;
import com.example.bhumicloud.gisapplications.model.field.textual.calculated.CalculatedValue;
import com.example.bhumicloud.gisapplications.model.field.textual.hyperlink.HyperlinkElement;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement.StatusOption;
import com.example.bhumicloud.gisapplications.model.field.textual.text.TextElement;
import com.example.bhumicloud.gisapplications.model.field.textual.yesno.YesNoElement;
import com.example.bhumicloud.gisapplications.model.field.textual.yesno.YesNoValue;
import com.example.bhumicloud.gisapplications.model.javascript.expression.ExpressionEngine;
import com.example.bhumicloud.gisapplications.model.javascript.expression.ExpressionEngine.ExpressionEngineListener;
import com.example.bhumicloud.gisapplications.model.javascript.expression.ExpressionEvents;
import com.example.bhumicloud.gisapplications.model.javascript.expression.ExpressionResult;
import com.example.bhumicloud.gisapplications.model.validation.Condition;
import com.example.bhumicloud.gisapplications.model.validation.FeatureValidationError;
import com.example.bhumicloud.gisapplications.model.validation.FeatureValidator;
import com.example.bhumicloud.gisapplications.model.validation.error.CustomValidationError;
import com.example.bhumicloud.gisapplications.util.ActivityRequestCodes;
import com.example.bhumicloud.gisapplications.util.AlertUtils;
import com.example.bhumicloud.gisapplications.util.FileBasedStateStorage;
import com.example.bhumicloud.gisapplications.util.FileUtils;
import com.example.bhumicloud.gisapplications.util.FileUtils.CopyMediaTask;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.ImageFileResizeTask;
import com.example.bhumicloud.gisapplications.util.IntentUtils;
import com.example.bhumicloud.gisapplications.util.KeyboardUtils;
import com.example.bhumicloud.gisapplications.util.MimeUtils;
import com.example.bhumicloud.gisapplications.util.NetworkUtils;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.example.bhumicloud.gisapplications.util.PermissionsUtils;
import com.example.bhumicloud.gisapplications.util.StreamUtils;
import com.example.bhumicloud.gisapplications.widget.StatusFieldButton;
import com.google.android.gms.location.LocationListener;
import com.journeyapps.barcodescanner.CaptureActivity;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class RecordEditorFragment extends GISFragment implements GISActivity.OnBackPressedListener, LocationListener, ElementViewListener, ElementViewDataSource, RecordEditorFragmentListener, RepeatableIndexFragmentListener, ClassificationElementViewListener {
    public static final String EXTRA_FEATURE_IDENTIFIER = "EXTRA_FEATURE_IDENTIFIER";
    private static final String STATE_ADD_MODE = "STATE_ADD_MODE";
    private static final String STATE_ATTACHMENT_KEY = "STATE_ATTACHMENT_KEY";
    private static final String STATE_ATTACHMENT_URI = "STATE_ATTACHMENT_URI";
    private static final String STATE_CURRENT_ELEMENT = "STATE_CURRENT_ELEMENT";
    private static final String STATE_CURRENT_FEATURE = "STATE_CURRENT_FEATURE";
    private static final String STATE_ELEMENT_CONTAINER = "STATE_ELEMENT_CONTAINER";
    private static final String STATE_FEATURE_EDIT_STATE = "STATE_FEATURE_EDIT_STATE";
    private static final String STATE_FILE_SAVED_STORAGE = "STATE_FILE_SAVED_STORAGE";
    private static final String STATE_LOADED = "STATE_LOADED";
    private static final String STATE_PARENT_VALUES_FOR_CONDITIONS = "STATE_PARENT_VALUES_FOR_CONDITIONS";
    private static final String STATE_ROOT_EDITOR = "STATE_ROOT_EDITOR";
    private static final String STATE_START_TIME = "STATE_START_TIME";
    private static final String STATE_VIEW_ONLY_MODE = "STATE_VIEW_ONLY_MODE";
    private static final String TAG_ADDRESS_FIELD_DIALOG = "TAG_ADDRESS_FIELD_DIALOG";
    private static final String TAG_AUDIO_CAPTION_DIALOG = "TAG_AUDIO_CAPTION_DIALOG";
    private static final String TAG_BARCODE_FIELD_DIALOG = "TAG_BARCODE_FIELD_DIALOG";
    private static final String TAG_CHOICE_FIELD_DIALOG = "TAG_CHOICE_FIELD_DIALOG";
    private static final String TAG_CLASSIFICATION_SET_DIALOG = "TAG_CLASSIFICATION_SET_DIALOG";
    private static final String TAG_CUSTOM_DIALOG = "TAG_CUSTOM_DIALOG";
    private static final String TAG_DATE_FIELD_DIALOG = "TAG_DATE_FIELD_DIALOG";
    private static final String TAG_DISCARD_CHANGES_DIALOG = "TAG_DISCARD_CHANGES_DIALOG";
    private static final String TAG_FEATURE_VALIDATION_DIALOG = "TAG_FEATURE_VALIDATION_DIALOG";
    private static final String TAG_PHOTO_CAPTION_DIALOG = "TAG_PHOTO_CAPTION_DIALOG";
    private static final String TAG_PROJECT_PICKER_DIALOG = "TAG_PROJECT_PICKER_DIALOG";
    private static final String TAG_RECORD_STATUS_DIALOG = "TAG_RECORD_STATUS_DIALOG";
    private static final String TAG_REMOVE_RECORD_LINK_DIALOG = "TAG_REMOVE_RECORD_LINK_DIALOG";
    private static final String TAG_REPORT_GENERATION_DIALOG = "TAG_REPORT_GENERATION_DIALOG";
    private static final String TAG_TIME_FIELD_DIALOG = "TAG_TIME_FIELD_DIALOG";
    private static final String TAG_VIDEO_CAPTION_DIALOG = "TAG_VIDEO_CAPTION_DIALOG";
    private static int sDialogCount = 0;
    private boolean mAddMode;
    private final AddressFieldDialogListener mAddressFieldDialogListener = new AddressFieldDialogListener() {
        public void onAddressChanged(Address address) {
            RecordEditorFragment.this.onAddressChanged(address);
        }
    };
    private String mAttachmentKey;
    private Uri mAttachmentURI;
    private final OnCaptionEditedListener mAudioCaptionDialogListener = new OnCaptionEditedListener() {
        public void onCaptionEdited(String audioID, String caption) {
           // RecordEditorFragment.this.onAudioCaptionEdited(audioID, caption);
        }
    };
    private final BarcodeFieldDialogListener mBarcodeFieldDialogListener = new BarcodeFieldDialogListener() {
        public void onBarcodeValueSet(String value) {
            RecordEditorFragment.this.onBarcodeFieldSelection(value);
        }

        public void onScanForBarcodeButtonClicked() {
            if (PermissionsUtils.checkPermissionsAndRequest(RecordEditorFragment.this.getActivity(), 16 | 2)) {
                RecordEditorFragment.this.startBarcodeScanner();
            }
        }
    };
    private List<CalculatedElement> mCalculatedElements;
    private final OnChoiceFieldSelectionListener mChoiceFieldDialogListener = new OnChoiceFieldSelectionListener() {
        public void onChoiceFieldSelection(List<String> values, String other) {
            RecordEditorFragment.this.onChoiceFieldSelection(values, other);
        }
    };
    private final ClassificationFieldDialogListener mClassificationFieldDialogListener = new ClassificationFieldDialogListener() {
        public void onClassificationSelected(List<String> values, String other) {
            RecordEditorFragment.this.onClassificationFieldDialogFinished(values, other);
        }
    };
    private Element mCurrentElement;
    private Feature mCurrentFeature;
    private GenericDialog mCustomDialog;
    private final DateFieldDialogListener mDateFieldDialogListener = new DateFieldDialogListener() {
        public void onDateValueChanged(Date date) {
            RecordEditorFragment.this.onDateFieldValueChanged(date);
        }
    };
    private final DiscardChangesDialogListener mDiscardChangesDialogListener = new DiscardChangesDialogListener() {
        public void onPositiveButtonClicked() {
            RecordEditorFragment.this.onDiscardChangesOptionSelected();
        }
    };
    private ElementContainer mElementContainer;
    private final Map<String, ElementView<?>> mElementViewIndex = new HashMap();
    private final ExpressionEngineListener mExpressionEngineListener = new ExpressionEngineListener() {
        public void onExpressionResults(List<ExpressionResult> results) {
            if (RecordEditorFragment.this.getActivity() != null) {
                RecordEditorFragment.this.processExpressionResults(results);
                RecordEditorFragment.this.onUpdateCalculatedFields();
                RecordEditorFragment.this.onUpdateViews();
            }
        }

        public void onExpressionError(String error) {
            RecordEditorFragment.this.showErrorDialog(error);
        }
    };
    private ExpressionEvents mExpressionEvents;
    private FeatureEditState mFeatureEditState;
    private final FeatureValidationErrorDialogListener mFeatureValidationListener = new FeatureValidationErrorDialogListener() {
        public void onSaveAsDraftSelected() {
            RecordEditorFragment.this.onSaveDraft();
        }
    };
    private ViewGroup mFieldContainer;
    private boolean mLoaded;
/*    private final OnGenerateReportSaveRecordListener mOnGenerateReportSaveRecordListener = new OnGenerateReportSaveRecordListener() {
        public boolean onGenerateReportSaveRecord() {
            if (!RecordEditorFragment.this.onSaveRecord(false, false)) {
                return false;
            }
            RecordEditorFragment.this.mFeatureEditState = new FeatureEditState(RecordEditorFragment.this.getCurrentFeature());
            return true;
        }
    };*/
    private FormValueContainer mParentValuesForConditions;
    private final OnCaptionEditedListener mPhotoCaptionDialogListener = new OnCaptionEditedListener() {
        public void onCaptionEdited(String photoID, String caption) {
            RecordEditorFragment.this.onPhotoCaptionEdited(photoID, caption);
        }
    };
    private boolean mProcessLocationUpdates;
    private Button mProjectButton;
    private final ProjectPickerDialogListener mProjectPickerDialogListener = new ProjectPickerDialogListener() {
        public void onProjectSelected(Project project) {
            RecordEditorFragment.this.onProjectSelected(project);
        }
    };
    private final OnStatusOptionSelectedListener mRecordStatusDialogListener = new OnStatusOptionSelectedListener() {
        public void onStatusOptionSelected(StatusElement field, StatusOption option) {
            RecordEditorFragment.this.onStatusSelected(field, option);
        }
    };
    private final RemoveRecordLinkDialogListener mRemoveRecordLinkDialogListener = new RemoveRecordLinkDialogListener() {
        public void onPositiveButtonClicked(RecordLinkElement element, RecordLinkItemValue item) {
            RecordEditorFragment.this.onRemoveRecordLinkOptionSelected(element, item);
        }
    };
    private final List<ReverseGeocodeTask> mReverseGeocodeTasks = new ArrayList();
    private boolean mRootEditor;
    private ScrollView mScrollView;
    private MenuItem mSetLocationMenuItem;
    private long mStartTime;
    private StatusFieldButton mStatusButton;
    private final TimeFieldDialogListener mTimeFieldDialogListener = new TimeFieldDialogListener() {
        public void onTimeValueChanged(Date time) {
            RecordEditorFragment.this.onTimeFieldValueChanged(time);
        }
    };
    private FormValueContainer mValuesForConditions;
/*    private final OnCaptionEditedListener mVideoCaptionDialogListener = new OnCaptionEditedListener() {
        public void onCaptionEdited(String videoID, String caption) {
            RecordEditorFragment.this.onVideoCaptionEdited(videoID, caption);
        }
    };*/
    private boolean mViewOnlyMode;
    private HashMap<String,Boolean> cache2;

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        setHasOptionsMenu(true);
        getExpressionEngine();
        if (savedState != null) {
            this.mRootEditor = savedState.getBoolean(STATE_ROOT_EDITOR);
            this.mAddMode = savedState.getBoolean(STATE_ADD_MODE);
            this.mAttachmentKey = savedState.getString(STATE_ATTACHMENT_KEY);
            this.mAttachmentURI = (Uri) savedState.getParcelable(STATE_ATTACHMENT_URI);
            this.mViewOnlyMode = savedState.getBoolean(STATE_VIEW_ONLY_MODE);
            this.mLoaded = savedState.getBoolean(STATE_LOADED);
            this.mStartTime = savedState.getLong(STATE_START_TIME);
            FileBasedStateStorage stateStorage = (FileBasedStateStorage) savedState.getParcelable(STATE_FILE_SAVED_STORAGE);
            if (stateStorage != null) {
                this.mElementContainer = (ElementContainer) stateStorage.get(STATE_ELEMENT_CONTAINER);
                if (this.mCurrentFeature == null) {
                    this.mCurrentFeature = (Feature) stateStorage.get(STATE_CURRENT_FEATURE);
                }
                if (this.mParentValuesForConditions == null) {
                    this.mParentValuesForConditions = (FormValueContainer) stateStorage.get(STATE_PARENT_VALUES_FOR_CONDITIONS);
                }
                onUpdateValuesForConditions();
                this.mCurrentElement = (Element) stateStorage.get(STATE_CURRENT_ELEMENT);
                this.mFeatureEditState = (FeatureEditState) stateStorage.get(STATE_FEATURE_EDIT_STATE);
                return;
            }
            GISLogger.log("FileBasedStateStorage was null");
            return;
        }
        this.mStartTime = SystemClock.uptimeMillis();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_record_editor, container, false);
        this.mScrollView = (ScrollView) view.findViewById(R.id.scroll_view);
        this.mStatusButton = (StatusFieldButton) view.findViewById(R.id.select_status_btn);
        this.mProjectButton = (Button) view.findViewById(R.id.select_project_btn);
        this.mFieldContainer = (ViewGroup) view.findViewById(R.id.field_container);
        this.mStatusButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                RecordEditorFragment.this.onSelectStatusButtonClicked();
            }
        });
/*        this.mProjectButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                RecordEditorFragment.this.onSelectProjectButtonClicked();
            }
        });*/
        onCreateFormView();
        onUpdateButtons();
        onUpdateValuesForConditions();
        onUpdateCalculatedFields();
        onUpdateViews();
        onUpdateActivityTitle();
        onTriggerLoadEvent();
        return this.mScrollView;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onReconnectDialogFragments();
    }

    public void onStart() {
        super.onStart();
        startLocationUpdates();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (isAdded() && !isRemoving()) {
            inflater.inflate(R.menu.fragment_record_editor, menu);
            MenuItem saveChangesMenuItem = menu.findItem(R.id.menu_item_save_changes);
            MenuItem setLocationMenuItem = menu.findItem(R.id.menu_item_set_location);
        //    MenuItem printReportMenuItem = menu.findItem(R.id.menu_item_print_report);
            if (!isRootEditor()) {
                saveChangesMenuItem.setVisible(false);
                saveChangesMenuItem.setEnabled(false);
                setLocationMenuItem.setVisible(false);
                setLocationMenuItem.setEnabled(false);
          //      printReportMenuItem.setVisible(false);
          //      printReportMenuItem.setEnabled(false);
            }
            if (!isRecordEditor()) {
            //    printReportMenuItem.setVisible(false);
            //    printReportMenuItem.setEnabled(false);
            }
            if (!isGeometryEnabled()) {
                setLocationMenuItem.setVisible(false);
                setLocationMenuItem.setEnabled(false);
            }
            if (this.mViewOnlyMode) {
                saveChangesMenuItem.setVisible(false);
                saveChangesMenuItem.setEnabled(false);
                setLocationMenuItem.setVisible(false);
                setLocationMenuItem.setEnabled(false);
            }
            this.mSetLocationMenuItem = setLocationMenuItem;
            configureLocationMenuItem();
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_save_changes /*2131820942*/:
                onSaveChangesMenuItemSelected();
                return true;
            case R.id.menu_item_set_location /*2131820943*/:
                onSetLocationMenuItemSelected();
                return true;
          //  case R.id.menu_item_print_report /*2131820953*/:
          //      onPrintReportMenuItemSelected();
          //      return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_ROOT_EDITOR, this.mRootEditor);
        outState.putBoolean(STATE_ADD_MODE, this.mAddMode);
        outState.putString(STATE_ATTACHMENT_KEY, this.mAttachmentKey);
        outState.putParcelable(STATE_ATTACHMENT_URI, this.mAttachmentURI);
        outState.putBoolean(STATE_VIEW_ONLY_MODE, this.mViewOnlyMode);
        outState.putBoolean(STATE_LOADED, this.mLoaded);
        outState.putLong(STATE_START_TIME, this.mStartTime);
        FileBasedStateStorage stateStorage = new FileBasedStateStorage(getActivity());
        stateStorage.store(STATE_ELEMENT_CONTAINER, this.mElementContainer);
        stateStorage.store(STATE_CURRENT_FEATURE, this.mCurrentFeature);
        stateStorage.store(STATE_PARENT_VALUES_FOR_CONDITIONS, this.mParentValuesForConditions);
        stateStorage.store(STATE_CURRENT_ELEMENT, this.mCurrentElement);
        stateStorage.store(STATE_FEATURE_EDIT_STATE, this.mFeatureEditState);
        outState.putParcelable(STATE_FILE_SAVED_STORAGE, stateStorage);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case ActivityRequestCodes.REQUEST_SET_GEOMETRY /*100*/:
                    onSetGeometryActivityResult(data);
                    break;
                case ActivityRequestCodes.REQUEST_TAKE_PHOTO /*101*/:
                    onNativeCameraActivityResult();
                    break;
                case ActivityRequestCodes.REQUEST_PICK_PHOTO /*102*/:
                    onPhotoGalleryActivityResult(data);
                    break;
                case ActivityRequestCodes.REQUEST_EDIT_SIGNATURE /*103*/:
                    onSignatureActivityResult(data);
                    break;
                case ActivityRequestCodes.REQUEST_RECORD_VIDEO /*104*/:
                    //onRecordVideoActivityResult(data);
                    break;
                case ActivityRequestCodes.REQUEST_IMPORT_VIDEO /*105*/:
                    //onImportVideoActivityResult(data);
                    break;
                case ActivityRequestCodes.REQUEST_RECORD_BARCODE /*106*/:
                    //onRecordBarcodeResult(data);
                    break;
                case ActivityRequestCodes.REQUEST_RECORD_AUDIO /*107*/:
                    //onRecordAudioActivityResult(data);
                    break;
                case ActivityRequestCodes.REQUEST_IMPORT_AUDIO /*108*/:
                    //onImportAudioActivityResult(data);
                    break;
                case ActivityRequestCodes.REQUEST_NEW_RECORD /*109*/:
                    onAddRecordLink(data);
                    break;
                case ActivityRequestCodes.REQUEST_EDIT_RECORD /*110*/:
                    onUpdateFieldsFromRecordLink(data);
                    break;
                case ActivityRequestCodes.REQUEST_SELECT_RECORD /*111*/:
                    onAddRecordLink(data);
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onDestroyView() {
        Context context = getContext();
        for (ElementView<?> view : this.mElementViewIndex.values()) {
            view.onDestroy(context);
        }
        this.mElementViewIndex.clear();
        this.mFieldContainer.removeAllViews();
        this.mScrollView.removeAllViews();
        this.mStatusButton = null;
        this.mProjectButton = null;
        this.mFieldContainer = null;
        this.mScrollView = null;
        super.onDestroyView();
    }

    public void onDestroy() {
        for (ReverseGeocodeTask task : this.mReverseGeocodeTasks) {
            task.cancel(false);
            task.setCallback(null);
        }
        this.mReverseGeocodeTasks.clear();
        super.onDestroy();
    }

    public boolean onBackPressed() {
        if (this.mViewOnlyMode) {
            return true;
        }
        if (!isRootEditor()) {
            Fragment target = getTargetFragment();
            if (target == null || !(target instanceof RecordEditorFragmentListener)) {
                return true;
            }
            ((RecordEditorFragmentListener) target).onFeatureEdited(this.mCurrentFeature, false);
            return true;
        } else if (hasChanges() || this.mAddMode) {
            DiscardChangesDialog dialog = new DiscardChangesDialog();
            dialog.setDiscardChangesDialogListener(this.mDiscardChangesDialogListener);
            dialog.show(getChildFragmentManager(), TAG_DISCARD_CHANGES_DIALOG);
            return false;
        } else if (!isRecordEditor()) {
            return true;
        } else {
            stopLocationUpdates();
            releaseExpressionEngine();
            getEditorActivity().discardAttachmentChanges();
            return true;
        }
    }

    public void onLocationChanged(Location location) {
        if (this.mProcessLocationUpdates) {
            getExpressionEngine().setCurrentLocation(location);
            Feature feature = getCurrentFeature();
            boolean isAccurateEnough = ((double) location.getAccuracy()) <= getRecord().getForm().getAutoLocationMinimumAccuracy();
            if (getRecord().getForm().isAutoLocationEnabled() && isAccurateEnough && isGeometryEnabled() && feature != null && !feature.hasLocation()) {
                feature.setLocation(location);
                configureLocationMenuItem();
                onTriggerGeometryChange();
                onAutoPopulateAddressFields();
                onUpdateCalculatedFields();
                onUpdateViews();
            }
        }
    }

    public void onFormValueDidChange(Element element, FormValue value) {
        setFormValue(element, value);
        onUpdateCalculatedFields();
        onUpdateViews();
    }

    public void onShowElementDescriptionDialog(Element element) {
        GenericDialog dialog = new GenericDialog();
        dialog.setTitle(element.getLabel());
        dialog.setMessage(element.getDescription());
        dialog.show(getChildFragmentManager(), "element_description");
    }

    public Record getEditingRecord() {
        return getRecord();
    }

    public void onFeatureEdited(Feature feature, boolean autoSave) {
        logDebugEvent("onFeatureEdited");
        this.mCurrentFeature = feature;
        onUpdateValuesForConditions();
        if (autoSave) {
            autoSaveDraft();
        }
    }

    public void onFeatureDeleted(Feature feature) {
        logDebugEvent("onFeatureDeleted");
    }

    public void onRepeatableValueChanged(RepeatableValue value) {
        logDebugEvent("onRepeatableValueChanged");
        onFormValueDidChange(value.getElement(), value);
        autoSaveDraft();
    }

    public void onPresentClassificationDialog(ClassificationElement element) {
        this.mCurrentElement = element;
        ClassificationValue value = (ClassificationValue) getFormValue((Element) element);
        if (value == null) {
            value = new ClassificationValue(element);
        }
        ClassificationFieldDialog dialog = new ClassificationFieldDialog();
        dialog.setClassificationValue(value);
        dialog.setClassificationFieldDialogListener(this.mClassificationFieldDialogListener);
        dialog.show(getChildFragmentManager(), TAG_CLASSIFICATION_SET_DIALOG);
    }

    public Record getRecord() {
        return getEditorActivity().getRecord();
    }

    public RecordEditorActivity getEditorActivity() {
        return (RecordEditorActivity) getActivity();
    }

    public void setAddMode(boolean isAddMode) {
        this.mAddMode = isAddMode;
    }

    public void setViewOnlyMode(boolean viewOnlyMode) {
        this.mViewOnlyMode = viewOnlyMode;
    }

    public ElementContainer getElementContainer() {
        if (this.mElementContainer != null) {
            return this.mElementContainer;
        }
        return getRecord().getForm();
    }

    public void setElementContainer(ElementContainer container) {
        this.mElementContainer = container;
    }

    public Feature getCurrentFeature() {
        if (this.mCurrentFeature != null) {
            return this.mCurrentFeature;
        }
        return getRecord();
    }

    public void setCurrentFeature(Feature feature) {
        this.mCurrentFeature = feature;
    }

    public void setFeatureStateBeforeEdit(Feature feature) {
        this.mFeatureEditState = new FeatureEditState(feature);
    }

    public FormValueContainer getValuesForConditions() {
        return this.mValuesForConditions;
    }

    public boolean isRootEditor() {
        return this.mRootEditor;
    }

    public boolean isRootRecordEditor() {
        return isRootEditor() && isRecordEditor();
    }

    public boolean isRootRepeatableEditor() {
        return isRootEditor() && isRepeatableEditor();
    }

    public void setRootEditor(boolean isRootEditor) {
        this.mRootEditor = isRootEditor;
    }

    public boolean isRecordEditor() {
        return getCurrentFeature() instanceof Record;
    }

    public boolean isRepeatableEditor() {
        return getCurrentFeature() instanceof RepeatableItemValue;
    }

    public boolean isGeometryEnabled() {
        if (this.mElementContainer instanceof RepeatableElement) {
            return ((RepeatableElement) this.mElementContainer).isGeometryEnabled();
        }
        return getRecord().getForm().isGeometryEnabled();
    }

    public void setParentValuesForConditions(FormValueContainer values) {
        this.mParentValuesForConditions = values;
    }

    public void startNativeCameraActivity() {
        Context context = getActivity();
        this.mAttachmentKey = UUID.randomUUID().toString();
        File storageFile = Photo.generatePhotoFile(context, this.mAttachmentKey, "image/jpeg");
        this.mAttachmentURI = Uri.fromFile(storageFile);
        Intent intent = IntentUtils.captureImage(context, storageFile);
        if (intent != null) {
            startActivityForResult(intent, ActivityRequestCodes.REQUEST_TAKE_PHOTO);
        }
    }


    public void startBarcodeScanner() {
        startActivityForResult(new Intent(getActivity(), CaptureActivity.class), ActivityRequestCodes.REQUEST_RECORD_BARCODE);
    }

    public void startPhotoGalleryActivity() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.putExtra("android.intent.extra.LOCAL_ONLY", true);
        intent.setType("image/*");
        intent.addCategory("android.intent.category.OPENABLE");
        if (isAdded() && getActivity() != null && IntentUtils.showIntentSafe(getActivity(), intent)) {
            startActivityForResult(intent, ActivityRequestCodes.REQUEST_PICK_PHOTO);
        }
    }
/*
    public void startVideoImportActivity() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.putExtra("android.intent.extra.LOCAL_ONLY", true);
        intent.setType("video/mp4");
        intent.addCategory("android.intent.category.OPENABLE");
        if (isAdded() && getActivity() != null && IntentUtils.showIntentSafe(getActivity(), intent)) {
            startActivityForResult(intent, ActivityRequestCodes.REQUEST_IMPORT_VIDEO);
        }
    }

    public void startAudioImportActivity() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.putExtra("android.intent.extra.LOCAL_ONLY", true);
        intent.setType("audio*//*");
        intent.addCategory("android.intent.category.OPENABLE");
        if (isAdded() && getActivity() != null && IntentUtils.showIntentSafe(getActivity(), intent)) {
            startActivityForResult(intent, ActivityRequestCodes.REQUEST_IMPORT_AUDIO);
        }
    }*/

    private boolean hasChanges() {
        FeatureEditState currentState = new FeatureEditState(getCurrentFeature());
        currentState.getFormValues().clearInvisibleValues(this.mValuesForConditions, getRecord());
        return !this.mFeatureEditState.equals(currentState);
    }

    private ExpressionEvents getExpressionEvents() {
        if (this.mExpressionEvents == null) {
            this.mExpressionEvents = new ExpressionEvents(getRecord(), getCurrentFeature());
        }
        return this.mExpressionEvents;
    }

    private List<FeatureValidationError> processExpressionResults(List<ExpressionResult> results) {
        List<FeatureValidationError> validations = new ArrayList();
        for (ExpressionResult result : results) {
            if (result.isCalculationResult()) {
                processCalculationResult(result);
            } else if (result.isSetValueResult()) {
                processSetValueResult(result);
            } else if (result.isOpenResult()) {
                processOpenResult(result);
            } else if (result.isUpdateElementResult()) {
                processUpdateElementResult(result);
            } else if (result.isMessageResult()) {
                processMessageResult(result);
            } else if (result.isConfigureResult()) {
                processConfigureResult(result);
            } else if (result.isProgressResult()) {
                processProgressResult(result);
            } else if (result.isValidationResult()) {
                validations.add(processValidationResult(result));
            }
        }
        return validations;
    }

    private FeatureValidationError processValidationResult(ExpressionResult result) {
        return new CustomValidationError(result.getMessage());
    }

    private void processUpdateElementResult(ExpressionResult result) {
        Element element;
        if (result.getKey().equals(Condition.STATUS_ELEMENT_KEY)) {
            element = getRecord().getForm().getStatusElement();
        } else {
            element = getRecord().getForm().getElement(result.getKey());
        }
        result.process(getActivity(), getRecord().getForm(), element);
        if (!(element == null || (element instanceof StatusElement))) {
            setFormValue(element, getFormValue(element), true);
        }
        onUpdateButtons();
    }

    private void processOpenResult(ExpressionResult result) {
        result.process(getActivity(), getRecord().getForm(), null);
    }

    private void processMessageResult(ExpressionResult result) {
        GenericDialog dialog = new GenericDialog();
        dialog.setTitle(result.getTitle());
        dialog.setMessage(result.getMessage());
        dialog.setCancelable(false);
        dialog.setPositiveButtonText((int) R.string.okay);
        FragmentTransaction beginTransaction = getChildFragmentManager().beginTransaction();
        StringBuilder append = new StringBuilder().append("TAG_CUSTOM_DIALOG_");
        int i = sDialogCount + 1;
        sDialogCount = i;
        beginTransaction.add(dialog, append.append(i).toString()).show(dialog).commitAllowingStateLoss();
    }

    private void processConfigureResult(ExpressionResult result) {
    }

    private void processProgressResult(ExpressionResult result) {
        boolean show;
        GenericDialog dialog = this.mCustomDialog;
        if (result.getMessage() != null) {
            show = true;
        } else {
            show = false;
        }
        if (show) {
            if (dialog == null) {
                dialog = new GenericDialog();
            }
            dialog.setTitle(result.getTitle());
            dialog.setMessage(result.getMessage());
            dialog.setCancelable(false);
            dialog.setPositiveButtonText(null);
            dialog.setNegativeButtonText(null);
            dialog.setNeutralButtonText(null);
            if (this.mCustomDialog == null) {
                getChildFragmentManager().beginTransaction().add(dialog, TAG_CUSTOM_DIALOG).show(dialog).commitAllowingStateLoss();
            }
            this.mCustomDialog = dialog;
        } else if (this.mCustomDialog != null) {
            this.mCustomDialog.dismissAllowingStateLoss();
            this.mCustomDialog = null;
        }
    }

    private void processCalculationResult(ExpressionResult result) {
        CalculatedElement element = (CalculatedElement) getRecord().getForm().getElement(result.getKey());
        CalculatedValue calculatedValue = (CalculatedValue) getFormValue(element);
        if (calculatedValue == null) {
            calculatedValue = new CalculatedValue(element, null, null);
        }
        String oldValue = calculatedValue.getValue();
        String newValue = result.getValueAsString();
        if (oldValue == null || oldValue.isEmpty()) {
            oldValue = null;
        }
        if (newValue == null || newValue.isEmpty()) {
            newValue = null;
        }
        boolean changed = (oldValue == null && newValue != null) || ((oldValue != null && newValue == null) || !(oldValue == null || oldValue.equals(newValue)));
        calculatedValue.setValue(newValue);
        calculatedValue.setError(result.getError());
        if (changed) {
            setFormValue(element, calculatedValue);
        } else if (calculatedValue.hasError()) {
            ElementView<?> view = getElementView(element);
            if (view != null) {
                view.setValue(calculatedValue);
            }
        }
    }

    private void processSetValueResult(ExpressionResult result) {
        String key = result.getKey();
        int obj = -1;
        switch (key.hashCode()) {
            case -438598951:
                if (key.equals("@project")) {
                    obj = 1;
                    break;
                }
                break;
            case 73179186:
                if (key.equals(Condition.STATUS_ELEMENT_KEY)) {
                    obj = 0;
                    break;
                }
                break;
            case 2133051570:
                if (key.equals("@geometry")) {
                    obj = 2;
                    break;
                }
                break;
        }
        switch (obj) {
            case 0 /*0*/:
                updateStatusFromExpressionResult(result);
                return;
            case 1 /*1*/:
                updateProjectFromExpressionResult(result);
                return;
            case 2 /*2*/:
                updateGeometryFromExpressionResult(result);
                return;
            default:
                updateFieldFromExpressionResult(result);
                return;
        }
    }

    private void updateStatusFromExpressionResult(ExpressionResult result) {
        StatusOption option = getRecord().getForm().getStatusElement().getOption(result.getValueAsString());
        if (option != null) {
            getRecord().setRecordStatus(option.getValue());
        }
        onUpdateStatusButton();
    }

    private void updateProjectFromExpressionResult(ExpressionResult result) {
        String projectName = result.getValueAsString();
        Project project = null;
        if (projectName != null) {
            project = Project.getProject(Account.getActiveAccount(), projectName);
        }
        getRecord().setProject(project);
        onUpdateProjectButton();
    }

    private void updateGeometryFromExpressionResult(ExpressionResult result) {
        Map geometry = result.getValueAsMap();
        if (geometry != null) {
            getCurrentFeature().setLocation(new GeoJSONPoint(geometry).toLocation());
            return;
        }
        getCurrentFeature().setLocation(null);
    }

    private void updateFieldFromExpressionResult(ExpressionResult result) {
        Element element = getRecord().getForm().getElement(result.getKey());
        if (element != null) {
            FormValue newValue = FormValueContainer.inflateFormValue(element, result.getRawValue());
            setFormValue(element, newValue, true);
            if ((newValue instanceof RecordLinkValue) && !newValue.isEmpty()) {
                onUpdateFieldsFromRecordLink((RecordLinkElement) element, ((RecordLinkValue) newValue).getLastRecordLinkItemValue().getRecord());
            }
        }
    }

    private void onTriggerLoadEvent() {
        if (!this.mLoaded) {
            if (isRootRecordEditor()) {
                onTriggerEvent("load-record");
                if (this.mAddMode) {
                    onTriggerEvent("new-record");
                } else {
                    onTriggerEvent("edit-record");
                }
            }
            if (isRootRepeatableEditor()) {
                String dataName = ((RepeatableItemValue) getCurrentFeature()).getElement().getDataName();
                onTriggerEvent("load-repeatable", null, dataName);
                if (this.mAddMode) {
                    onTriggerEvent("new-repeatable", null, dataName);
                } else {
                    onTriggerEvent("edit-repeatable", null, dataName);
                }
            }
            onUpdateCalculatedFields();
            onUpdateViews();
        }
        this.mLoaded = true;
    }

    private List<FeatureValidationError> onTriggerEvent(String name) {
        return onTriggerEvent(name, null);
    }

    private List<FeatureValidationError> onTriggerEvent(String name, Map<String, Object> data) {
        onUpdateAuditData(getCurrentFeature());
        getExpressionEvents().setValues(this.mValuesForConditions);
        return processExpressionResults(getExpressionEvents().triggerEvent(getExpressionEngine(), name, data));
    }

    private void onTriggerClick(Element element) {
        HashMap<String, Object> params = new HashMap();
        params.put("field", element.getDataName());
        onTriggerEvent("click", params);
        onUpdateCalculatedFields();
        onUpdateViews();
    }

    private List<FeatureValidationError> onTriggerEvent(String eventName, Object value, String dataName) {
        onUpdateAuditData(getCurrentFeature());
        getExpressionEvents().setValues(this.mValuesForConditions);
        return processExpressionResults(getExpressionEvents().onValueChange(getExpressionEngine(), eventName, value, dataName));
    }

    private void onTriggerGeometryChange() {
        Map geometry = null;
        if (getCurrentFeature().hasLocation()) {
            geometry = new GeoJSONPoint(getCurrentFeature().getLocation()).toGeoJSON();
        }
        String dataName = null;
        if (isRepeatableEditor()) {
            dataName = ((RepeatableItemValue) getCurrentFeature()).getElement().getDataName();
        }
        onTriggerEvent("change-geometry", geometry, dataName);
    }

    private void onTriggerValueChange(Element element, FormValue value) {
        onUpdateAuditData(getCurrentFeature());
        getExpressionEvents().setValues(this.mValuesForConditions);
        processExpressionResults(getExpressionEvents().onValueChange(getExpressionEngine(), element, value));
    }

    private List<FeatureValidationError> onTriggerValidateRecord() {
        return onTriggerEvent("validate-record");
    }

    private List<FeatureValidationError> onTriggerValidateRepeatable() {
        return onTriggerEvent("validate-repeatable", null, ((RepeatableItemValue) getCurrentFeature()).getElement().getDataName());
    }

    private ExpressionEngine getExpressionEngine() {
        ExpressionEngine engine = getEditorActivity().getExpressionEngine();
        engine.setExpressionEngineListener(this.mExpressionEngineListener);
        engine.setIsNewFeature(this.mAddMode);
        return engine;
    }

    private void releaseExpressionEngine() {
        getEditorActivity().releaseExpressionEngine();
    }

    private List<CalculatedElement> getCalculatedElements() {
        if (this.mCalculatedElements != null) {
            return this.mCalculatedElements;
        }
        this.mCalculatedElements = CalculatedElement.getCalculatedElements(CalculatedElement.getCalculatedElementRoot(getRecord().getForm(), getElementContainer()));
        return this.mCalculatedElements;
    }

    private void onReconnectDialogFragments() {
        FragmentManager fm = getChildFragmentManager();
        DateFieldDialog dateFieldDialog = (DateFieldDialog) fm.findFragmentByTag(TAG_DATE_FIELD_DIALOG);
        if (dateFieldDialog != null) {
            dateFieldDialog.setDateFieldDialogListener(this.mDateFieldDialogListener);
        }
        TimeFieldDialog timeFieldDialog = (TimeFieldDialog) fm.findFragmentByTag(TAG_TIME_FIELD_DIALOG);
        if (timeFieldDialog != null) {
            timeFieldDialog.setTimeFieldDialogListener(this.mTimeFieldDialogListener);
        }
        ChoiceFieldDialog choiceFieldDialog = (ChoiceFieldDialog) fm.findFragmentByTag(TAG_CHOICE_FIELD_DIALOG);
        if (choiceFieldDialog != null) {
            choiceFieldDialog.setOnChoiceFieldSelectionListener(this.mChoiceFieldDialogListener);
        }
        EditCaptionDialog photoCaptionDialog = (EditCaptionDialog) fm.findFragmentByTag(TAG_PHOTO_CAPTION_DIALOG);
        if (photoCaptionDialog != null) {
            photoCaptionDialog.setOnCaptionEditedListener(this.mPhotoCaptionDialogListener);
        }
        EditCaptionDialog videoCaptionDialog = (EditCaptionDialog) fm.findFragmentByTag(TAG_VIDEO_CAPTION_DIALOG);
        if (videoCaptionDialog != null) {
            //videoCaptionDialog.setOnCaptionEditedListener(this.mVideoCaptionDialogListener);
        }
        EditCaptionDialog audioCaptionDialog = (EditCaptionDialog) fm.findFragmentByTag(TAG_AUDIO_CAPTION_DIALOG);
        if (audioCaptionDialog != null) {
            //audioCaptionDialog.setOnCaptionEditedListener(this.mAudioCaptionDialogListener);
        }
        AddressFieldDialog addressFieldDialog = (AddressFieldDialog) fm.findFragmentByTag(TAG_ADDRESS_FIELD_DIALOG);
        if (addressFieldDialog != null) {
            addressFieldDialog.setAddressFieldDialogListener(this.mAddressFieldDialogListener);
        }
        RecordStatusDialog recordStatusDialog = (RecordStatusDialog) fm.findFragmentByTag(TAG_RECORD_STATUS_DIALOG);
        if (recordStatusDialog != null) {
            recordStatusDialog.setOnStatusOptionSelectedListener(this.mRecordStatusDialogListener);
        }
        ProjectPickerDialog projectPickerDialog = (ProjectPickerDialog) fm.findFragmentByTag(TAG_PROJECT_PICKER_DIALOG);
        if (projectPickerDialog != null) {
            projectPickerDialog.setProjectPickerDialogListener(this.mProjectPickerDialogListener);
        }
        ClassificationFieldDialog classificationFieldDialog = (ClassificationFieldDialog) fm.findFragmentByTag(TAG_CLASSIFICATION_SET_DIALOG);
        if (classificationFieldDialog != null) {
            classificationFieldDialog.setClassificationFieldDialogListener(this.mClassificationFieldDialogListener);
        }
        FeatureValidationErrorDialog featureValidationDialog = (FeatureValidationErrorDialog) fm.findFragmentByTag(TAG_FEATURE_VALIDATION_DIALOG);
        if (featureValidationDialog != null) {
            featureValidationDialog.setListener(this.mFeatureValidationListener);
        }
        BarcodeFieldDialog barcodeFieldDialog = (BarcodeFieldDialog) fm.findFragmentByTag(TAG_BARCODE_FIELD_DIALOG);
        if (barcodeFieldDialog != null) {
            barcodeFieldDialog.setBarcodeFieldDialogListener(this.mBarcodeFieldDialogListener);
        }
        DiscardChangesDialog discardChangesDialog = (DiscardChangesDialog) fm.findFragmentByTag(TAG_DISCARD_CHANGES_DIALOG);
        if (discardChangesDialog != null) {
            discardChangesDialog.setDiscardChangesDialogListener(this.mDiscardChangesDialogListener);
        }
        RemoveRecordLinkDialog removeRecordLinkDialog = (RemoveRecordLinkDialog) fm.findFragmentByTag(TAG_REMOVE_RECORD_LINK_DIALOG);
        if (removeRecordLinkDialog != null) {
            removeRecordLinkDialog.setRemoveRecordLinkDialogListener(this.mRemoveRecordLinkDialogListener);
        }
/*        ReportGenerationDialog reportGenerationDialog = (ReportGenerationDialog) fm.findFragmentByTag ( TAG_REPORT_GENERATION_DIALOG );
        if (reportGenerationDialog != null) {
            reportGenerationDialog.setOnGenerateReportSaveRecordListener ( this.mOnGenerateReportSaveRecordListener );
        }*/
        this.mCustomDialog = (GenericDialog) fm.findFragmentByTag(TAG_CUSTOM_DIALOG);
    }

    private void onUpdateValuesForConditions() {
        this.mValuesForConditions = getCurrentFeature().getFormValues().copy();
        if (isRepeatableEditor() && this.mParentValuesForConditions != null) {
            this.mValuesForConditions.mergeValues(this.mParentValuesForConditions);
        }
    }

    private void onUpdateProjectButton() {
        if (this.mProjectButton != null) {
            Record record = getRecord();
            Form form = record.getForm();
            if (!isRootRecordEditor() || (form != null && !form.isProjectsEnabled())) {
                this.mProjectButton.setVisibility(View.GONE);
            } else if (Project.getProjects(record.getAccount()).isEmpty()) {
                this.mProjectButton.setVisibility(View.GONE);
            } else {
                Project project = record.getProject();
                if (project != null) {
                    this.mProjectButton.setText(project.getName());
                } else if (!this.mViewOnlyMode && record.getAccount().getRole().canChangeRecordProject()) {
                    this.mProjectButton.setText(R.string.select_project_btn);
                }
                if (this.mViewOnlyMode) {
                    this.mProjectButton.setClickable(false);
                }
            }
        }
    }

    private void onUpdateStatusButton() {
        boolean z = true;
        if (this.mStatusButton != null) {
            if (isRootRecordEditor()) {
                Record record = getRecord();
                StatusElement statusElement = record.getForm().getStatusElement();
                if (statusElement == null || statusElement.isHidden() || !statusElement.isEnabled()) {
                    this.mStatusButton.setVisibility(View.GONE);
                    return;
                }
                String currentStatusLabel;
                String btnText;
                this.mStatusButton.setVisibility(View.VISIBLE);
                String currentStatus = record.getRecordStatus();
                StatusOption currentOption = statusElement.getOption(currentStatus);
                this.mStatusButton.setStatusOption(currentOption);
                if (currentOption != null) {
                    currentStatusLabel = currentOption.getLabel();
                } else {
                    currentStatusLabel = currentStatus;
                }
                if (TextUtils.isEmpty(currentStatus)) {
                    btnText = getString(R.string.no_status);
                } else {
                    btnText = getString(R.string.status_placeholder, new Object[]{statusElement.getLabel(), currentStatusLabel});
                }
                this.mStatusButton.setText(btnText);
                StatusFieldButton statusFieldButton = this.mStatusButton;
                if (statusElement.isReadOnly()) {
                    z = false;
                }
                statusFieldButton.setEnabled(z);
                if (this.mViewOnlyMode) {
                    this.mStatusButton.setClickable(false);
                    return;
                }
                return;
            }
            this.mStatusButton.setVisibility(View.GONE);
        }
    }

    private void onCreateFormView() {
        onCreateFormView(getElementContainer(), this.mFieldContainer);
    }

    private void onCreateFormView(ElementContainer elementContainer, ViewGroup viewContainer) {
        Context context = getActivity();
        Iterator it = elementContainer.getElements().iterator();
        while (it.hasNext()) {
            Element element = (Element) it.next();
            if (element instanceof TextElement) {
                onCreateTextElementView(getActivity(), (TextElement) element, viewContainer);
            } else if (element instanceof DateElement) {
                onCreateDateElementView(context, (DateElement) element, viewContainer);
            } else if (element instanceof TimeElement) {
                onCreateTimeElementView(context, (TimeElement) element, viewContainer);
            } else if (element instanceof LabelElement) {
                onCreateLabelElementView(context, (LabelElement) element, viewContainer);
            } else if (element instanceof AudioElement) {
                //onCreateAudioElementView(context, (AudioElement) element, viewContainer);
            } else if (element instanceof PhotoElement) {
                onCreatePhotoElementView(context, (PhotoElement) element, viewContainer);
            } else if (element instanceof VideoElement) {
                //onCreateVideoElementView(context, (VideoElement) element, viewContainer);
            } else if (element instanceof ChoiceElement) {
                onCreateChoiceElementView(context, (ChoiceElement) element, viewContainer);
            } else if (element instanceof ClassificationElement) {
                onCreateClassificationElementView(context, (ClassificationElement) element, viewContainer);
            } else if (element instanceof AddressElement) {
                onCreateAddressElementView(context, (AddressElement) element, viewContainer);
            } else if (element instanceof SignatureElement) {
                onCreateSignatureElementView(context, (SignatureElement) element, viewContainer);
            } else if (element instanceof RepeatableElement) {
                onCreateRepeatableElementView(context, (RepeatableElement) element, viewContainer);
            } else if (element instanceof YesNoElement) {
                onCreateYesNoElementView(context, (YesNoElement) element, viewContainer);
            } else if (element instanceof HyperlinkElement) {
                onCreateHyperlinkElementView(context, (HyperlinkElement) element, viewContainer);
            } else if (element instanceof BarcodeElement) {
                onCreateBarcodeElementView(context, (BarcodeElement) element, viewContainer);
            } else if (element instanceof CalculatedElement) {
                onCreateCalculatedElementView(context, (CalculatedElement) element, viewContainer);
            } else if (element instanceof SectionElement) {
                SectionElement section = (SectionElement) element;
                if (section.isInline()) {
                    onCreateInlineSectionElementView(context, section, viewContainer);
                } else {
                    onCreateNestedSectionElementView(context, section, viewContainer);
                }
            } else if (element instanceof RecordLinkElement) {
                onCreateRecordLinkElementView(context, (RecordLinkElement) element, viewContainer);
            }
        }
    }

    private void onCreateTextElementView(Activity activity, TextElement element, ViewGroup container) {
        addElementView(element, new TextElementView(activity, element, this.mViewOnlyMode), container);
    }

    private void onCreateDateElementView(Context context, DateElement element, ViewGroup container) {
        DateElementView view = new DateElementView(context, element, this.mViewOnlyMode);
        view.setListener(new DateElementViewListener() {
            public void onShowDateFieldDialog(DateElement element) {
                RecordEditorFragment.this.onShowDateFieldDialog(element);
            }
        });
        addElementView(element, view, container);
    }

    private void onCreateTimeElementView(Context context, TimeElement element, ViewGroup container) {
        TimeElementView view = new TimeElementView(context, element, this.mViewOnlyMode);
        view.setListener(new TimeElementViewListener() {
            public void onShowTimeFieldDialog(TimeElement element) {
                RecordEditorFragment.this.onShowTimeFieldDialog(element);
            }
        });
        addElementView(element, view, container);
    }

    private void onCreateLabelElementView(Context context, LabelElement element, ViewGroup container) {
        addElementView(element, new LabelElementView(context, element, this.mViewOnlyMode), container);
    }

/*    private void onCreateAudioElementView(Context context, AudioElement element, ViewGroup container) {
        AudioElementView view = new AudioElementView(context, element, this.mViewOnlyMode);
        view.setListener(new MediaElementViewListener() {
            public void onNewMediaButtonClicked(Element element) {
                RecordEditorFragment.this.mCurrentElement = element;
                RecordEditorFragment.this.startAudioRecordActivity();
            }

            public void onSelectMediaButtonClicked(Element element) {
                RecordEditorFragment.this.mCurrentElement = element;
                if (PermissionsUtils.checkPermissionsAndRequest(RecordEditorFragment.this.getActivity(), PermissionsUtils.STORAGE | 4)) {
                    RecordEditorFragment.this.startAudioImportActivity();
                }
            }

            public void onEditCaptionButtonClicked(Element element, MediaItemValue item) {
                RecordEditorFragment.this.onEditAudioCaptionButtonClicked(element, item);
            }

            public void onRemoveMediaButtonClicked(Element element, MediaItemValue item) {
                RecordEditorFragment.this.onRemoveMediaButtonClicked(element, item);
            }

            public void onShareMediaButtonClicked(MediaItemValue mediaItemValue, Type mediaType) {
                RecordEditorFragment.this.onShareMediaButtonClicked(mediaItemValue, mediaType);
            }
        });
        addElementView(element, view, container);
    }*/

    private void onCreatePhotoElementView(final Context context, PhotoElement element, ViewGroup container) {
        PhotoElementView view = new PhotoElementView(context, element, this.mViewOnlyMode);
        view.setListener(new MediaElementViewListener() {
            public void onNewMediaButtonClicked(Element element) {
                RecordEditorFragment.this.mCurrentElement = element;
                int permissionsGroups = PermissionsUtils.CAMERA | 1;
                if (PatronSettings.isSavePhotosToGallery(context)) {
                    permissionsGroups |= PermissionsUtils.STORAGE;
                }
                if (PermissionsUtils.checkPermissionsAndRequest(RecordEditorFragment.this.getActivity(), permissionsGroups)) {
                    RecordEditorFragment.this.startNativeCameraActivity();
                }
            }

            public void onSelectMediaButtonClicked(Element element) {
                RecordEditorFragment.this.mCurrentElement = element;
                if (PermissionsUtils.checkPermissionsAndRequest(RecordEditorFragment.this.getActivity(), PermissionsUtils.STORAGE | 1)) {
                    RecordEditorFragment.this.startPhotoGalleryActivity();
                }
            }

            public void onEditCaptionButtonClicked(Element element, MediaItemValue item) {
                RecordEditorFragment.this.onPhotoEditCaptionButtonClicked(element, item);
            }

            public void onRemoveMediaButtonClicked(Element element, MediaItemValue item) {
                RecordEditorFragment.this.onRemoveMediaButtonClicked(element, item);
            }

            public void onShareMediaButtonClicked(MediaItemValue mediaItemValue, Type mediaType) {
                RecordEditorFragment.this.onShareMediaButtonClicked(mediaItemValue, mediaType);
            }
        });
        addElementView(element, view, container);
    }

/*
    private void onCreateVideoElementView(Context context, VideoElement element, ViewGroup container) {
        VideoElementView view = new VideoElementView(context, element, this.mViewOnlyMode);
        view.setListener(new MediaElementViewListener() {
            public void onNewMediaButtonClicked(Element element) {
                RecordEditorFragment.this.mCurrentElement = element;
                RecordEditorFragment.this.startVideoRecordActivity();
            }

            public void onSelectMediaButtonClicked(Element element) {
                RecordEditorFragment.this.mCurrentElement = element;
                if (PermissionsUtils.checkPermissionsAndRequest(RecordEditorFragment.this.getActivity(), PermissionsUtils.STORAGE | 2)) {
                    RecordEditorFragment.this.startVideoImportActivity();
                }
            }

            public void onEditCaptionButtonClicked(Element element, MediaItemValue item) {
                RecordEditorFragment.this.onEditVideoCaptionButtonClicked(element, item);
            }

            public void onRemoveMediaButtonClicked(Element element, MediaItemValue item) {
                RecordEditorFragment.this.onRemoveMediaButtonClicked(element, item);
            }

            public void onShareMediaButtonClicked(MediaItemValue mediaItemValue, Type mediaType) {
                RecordEditorFragment.this.onShareMediaButtonClicked(mediaItemValue, mediaType);
            }
        });
        addElementView(element, view, container);
    }
*/

    private void onCreateRecordLinkElementView(Context context, RecordLinkElement element, ViewGroup container) {
        RecordLinkElementView view = new RecordLinkElementView(context, element, this.mViewOnlyMode);
        view.setListener(new RecordLinkElementViewListener() {
            public void onCreateNewRecordLinkButtonClicked(RecordLinkElement element) {
                RecordEditorFragment.this.onCreateNewRecordLinkItem(element);
            }

            public void onSelectExistingRecordLinkButtonClicked(RecordLinkElement element) {
                RecordEditorFragment.this.onSelectExistingRecordLinkItem(element);
            }

            public void onRecordLinkItemClicked(RecordLinkElement element, RecordLinkItemValue recordLinkItemValue) {
                RecordEditorFragment.this.onRecordLinkItemClicked(element, recordLinkItemValue);
            }

            public void onDeleteRecordLinkItem(RecordLinkElement element, RecordLinkItemValue recordLinkItemValue) {
                RecordEditorFragment.this.showDeleteRecordLinkItemDialog(element, recordLinkItemValue);
            }
        });
        addElementView(element, view, container);
    }

    private void onCreateChoiceElementView(Context context, ChoiceElement element, ViewGroup container) {
        ChoiceElementView view = new ChoiceElementView(context, element, this.mViewOnlyMode);
        view.setListener(new ChoiceElementViewListener() {
            public void onShowChoiceDialog(ChoiceElement element) {
                RecordEditorFragment.this.onShowChoiceFieldDialog(element);
            }
        });
        addElementView(element, view, container);
    }

    private void onCreateInlineSectionElementView(Context context, SectionElement element, ViewGroup container) {
        InlineSectionElementView view = new InlineSectionElementView(context, element, this.mViewOnlyMode);
        addElementView(element, view, container);
        onCreateFormView(element, view.getContainerView());
    }

    private void onCreateNestedSectionElementView(Context context, SectionElement element, ViewGroup container) {
        NestedSectionElementView view = new NestedSectionElementView(context, element, this.mViewOnlyMode);
        view.setNestedSectionElementViewListener(new NestedSectionElementViewListener() {
            public void onNestedSectionElementViewSelected(SectionElement element) {
                RecordEditorFragment.this.onNestedSectionElementSelected(element);
            }
        });
        addElementView(element, view, container);
    }

    private void onCreateClassificationElementView(Context context, ClassificationElement element, ViewGroup container) {
        ClassificationElementView view = new ClassificationElementView(context, element, this.mViewOnlyMode);
        view.setListener(this);
        addElementView(element, view, container);
    }

    private void onCreateSignatureElementView(Context context, SignatureElement element, ViewGroup container) {
        SignatureElementView view = new SignatureElementView(context, element, this.mViewOnlyMode);
        view.setListener(new SignatureElementViewListener() {
            public void onPresentSignatureEditor(SignatureElement element) {
                RecordEditorFragment.this.onPresentSignatureActivity(element);
            }
        });
        addElementView(element, view, container);
    }

    private void onCreateAddressElementView(Context context, AddressElement element, ViewGroup container) {
        AddressElementView view = new AddressElementView(context, element, this.mViewOnlyMode);
        view.setListener(new AddressElementViewListener() {
            public void onShowAddressDialog(AddressElement element) {
                RecordEditorFragment.this.onShowAddressFieldDialog(element);
            }
        });
        addElementView(element, view, container);
    }

    private void onCreateRepeatableElementView(Context context, RepeatableElement element, ViewGroup container) {
        RepeatableElementView view = new RepeatableElementView(context, element, this.mViewOnlyMode);
        view.setRepeatableElementViewListener(new RepeatableElementViewListener() {
            public void onRepeatableElementViewSelected(RepeatableElement element) {
                RecordEditorFragment.this.onRepeatableElementSelected(element);
            }
        });
        addElementView(element, view, container);
    }

    private void onCreateYesNoElementView(Context context, YesNoElement element, ViewGroup container) {
        YesNoElementView view = new YesNoElementView(context, element, this.mViewOnlyMode);
        view.setListener(new YesNoElementViewListener() {
            public void onYesNoValueSelected(YesNoElement element, String value) {
                RecordEditorFragment.this.onYesNoFieldSelection(element, value);
            }
        });
        addElementView(element, view, container);
    }

    private void onCreateHyperlinkElementView(Context context, HyperlinkElement element, ViewGroup container) {
        HyperlinkElementView hyperlinkView = new HyperlinkElementView(context, element, this.mViewOnlyMode);
        hyperlinkView.setHyperlinkElementViewListener(new HyperlinkElementViewListener() {
            public void onClick(Element element) {
                RecordEditorFragment.this.onTriggerClick(element);
            }
        });
        addElementView(element, hyperlinkView, container);
    }

    private void onCreateBarcodeElementView(Context context, BarcodeElement element, ViewGroup container) {
        BarcodeElementView view = new BarcodeElementView(context, element, this.mViewOnlyMode);
        view.setListener(new BarcodeElementViewListener() {
            public void onShowBarcodeDialog(BarcodeElement element) {
                RecordEditorFragment.this.onShowBarcodeFieldDialog(element);
            }
        });
        addElementView(element, view, container);
    }

    private void onCreateCalculatedElementView(Context context, CalculatedElement element, ViewGroup container) {
        addElementView(element, new CalculatedElementView(context, element, this.mViewOnlyMode), container);
    }

    private void addElementView(Element element, ElementView<?> elementView, ViewGroup container) {
        elementView.setElementViewListener(this);
        elementView.setElementViewDataSource(this);
        elementView.setValue(getFormValue(elementView.getElement()));
        container.addView(elementView);
        this.mElementViewIndex.put(element.getKey(), elementView);
    }

    private void configureLocationMenuItem() {
        if (this.mSetLocationMenuItem != null) {
            Feature feature = getCurrentFeature();
            if (feature == null || !feature.hasLocation()) {
                this.mSetLocationMenuItem.setIcon(R.drawable.ic_action_gps_not_fixed);
            } else {
                this.mSetLocationMenuItem.setIcon(R.drawable.ic_action_gps_fixed);
            }
            this.mSetLocationMenuItem.setEnabled(getRecord().getForm().isManualLocationEnabled().booleanValue());
        }
    }

    private void onUpdateCalculatedFields() {
        if (!getCalculatedElements().isEmpty() && !this.mViewOnlyMode) {
            onUpdateAuditData(getCurrentFeature());
            ExpressionEngine engine = getExpressionEngine();
            engine.setRecord(getRecord());
            engine.setCalculatedElements(getCalculatedElements());
            engine.setIsNewFeature(this.mAddMode);
            engine.setFeature(getCurrentFeature());
            engine.setValues(this.mValuesForConditions);
            processExpressionResults(engine.evaluate());
        }
    }

    private void onUpdateViews() {
        onUpdateViews(getElementContainer().getElements(), null);
    }

    private void onUpdateViews(List<Element> elements, HashMap<String, Boolean> cache) {
        if (cache == null) {
            HashMap cache2 = new HashMap();
        }
        for (Element element : elements) {
            onUpdateViews(element, cache2);
            if (element instanceof SectionElement) {
                SectionElement section = (SectionElement) element;
                if (section.isInline()) {
                    onUpdateViews(section.getElements(), cache2);
                }
            }
        }
    }

    private void onUpdateViews(Element element, HashMap<String, Boolean> cache) {
        boolean isCurrentlyVisible;
        ElementView<?> view = getElementView(element);
        boolean shouldBeVisible = Condition.shouldElementBeVisible(element, getRecord(), this.mValuesForConditions, cache);
        if (view != null && shouldBeVisible) {
            if (Condition.shouldElementBeRequired(element, getRecord(), this.mValuesForConditions)) {
                view.setRequired(true);
            } else {
                view.setRequired(false);
            }
        }
        if (view == null || !view.isVisible()) {
            isCurrentlyVisible = false;
        } else {
            isCurrentlyVisible = true;
        }
        if (shouldBeVisible) {
            if (view != null) {
                view.update();
            }
            if (!isCurrentlyVisible && view != null) {
                view.setVisible(true);
            }
        } else if (isCurrentlyVisible) {
            view.setVisible(false);
        }
    }

    private void setFormValue(Element element, FormValue value) {
        setFormValue(element, value, false);
    }

    private void setFormValue(Element element, FormValue value, boolean silent) {
        if (this.mValuesForConditions != null) {
            this.mValuesForConditions.setFormValue(element, value);
        }
        getCurrentFeature().setFormValue(element, value);
        ElementView<?> view = getElementView(element);
        if (view != null) {
            view.setValue(value);
        }
        if (!silent) {
            onTriggerValueChange(element, value);
        }
    }

    private ElementView<?> getElementView(Element element) {
        return getElementView(element.getKey());
    }

    private ElementView<?> getElementView(String elementKey) {
        return (ElementView) this.mElementViewIndex.get(elementKey);
    }

    private FormValue getFormValue(Element element) {
        return getFormValue(element.getKey());
    }

    private FormValue getFormValue(String elementKey) {
        return getCurrentFeature().getFormValue(elementKey);
    }

    private void onShowDateFieldDialog(DateElement element) {
        this.mCurrentElement = element;
        DateValue value = (DateValue) getFormValue((Element) element);
        DateFieldDialog dialog = new DateFieldDialog();
        dialog.setTitle(element.getLabel());
        dialog.setDateValue(value);
        dialog.setDateFieldDialogListener(this.mDateFieldDialogListener);
        dialog.show(getChildFragmentManager(), TAG_DATE_FIELD_DIALOG);
    }

    private void onDateFieldValueChanged(Date date) {
        DateElement element = getCurrentDateElement();
        if (element == null) {
            return;
        }
        if (date != null) {
            onFormValueDidChange(element, new DateValue(element, date));
        } else {
            onFormValueDidChange(element, null);
        }
    }

    private void onShowTimeFieldDialog(TimeElement element) {
        this.mCurrentElement = element;
        TimeValue value = (TimeValue) getFormValue((Element) element);
        TimeFieldDialog dialog = new TimeFieldDialog();
        dialog.setTitle(element.getLabel());
        dialog.setTimeValue(value);
        dialog.setTimeFieldDialogListener(this.mTimeFieldDialogListener);
        dialog.show(getChildFragmentManager(), TAG_TIME_FIELD_DIALOG);
    }

    private void onTimeFieldValueChanged(Date time) {
        TimeElement element = getCurrentTimeElement();
        if (element == null) {
            return;
        }
        if (time != null) {
            onFormValueDidChange(element, new TimeValue(element, time));
        } else {
            onFormValueDidChange(element, null);
        }
    }

    private void onShowChoiceFieldDialog(ChoiceElement element) {
        this.mCurrentElement = element;
        ChoiceValue value = (ChoiceValue) getFormValue((Element) element);
        if (value == null) {
            value = new ChoiceValue(element);
        }
        ChoiceFieldDialog dialog = new ChoiceFieldDialog();
        dialog.setChoiceValue(value);
        dialog.setOnChoiceFieldSelectionListener(this.mChoiceFieldDialogListener);
        dialog.show(getChildFragmentManager(), TAG_CHOICE_FIELD_DIALOG);
    }

    private void onChoiceFieldSelection(List<String> values, String other) {
        ChoiceElement element = getCurrentChoiceElement();
        if (element != null) {
            onFormValueDidChange(element, new ChoiceValue(element, (Collection) values, other));
        }
    }

    private void onShowBarcodeFieldDialog(BarcodeElement element) {
        this.mCurrentElement = element;
        BarcodeValue value = (BarcodeValue) getFormValue((Element) element);
        if (value == null) {
            value = new BarcodeValue(element);
        }
        BarcodeFieldDialog dialog = new BarcodeFieldDialog();
        dialog.setBarcodeValue(value);
        dialog.setBarcodeFieldDialogListener(this.mBarcodeFieldDialogListener);
        dialog.show(getChildFragmentManager(), TAG_BARCODE_FIELD_DIALOG);
    }

    private void onBarcodeFieldSelection(String fieldValue) {
        BarcodeElement element = getCurrentBarcodeElement();
        if (element != null) {
            onFormValueDidChange(element, new BarcodeValue(element, fieldValue));
        }
    }

    private void onYesNoFieldSelection(YesNoElement element, String value) {
        if (TextUtils.isEmpty(value)) {
            onFormValueDidChange(element, null);
        } else {
            onFormValueDidChange(element, new YesNoValue(element, value));
        }
    }

    private void onShowAddressFieldDialog(AddressElement element) {
        this.mCurrentElement = element;
        AddressValue value = (AddressValue) getFormValue((Element) element);
        AddressFieldDialog dialog = new AddressFieldDialog();
        dialog.setTitle(element.getLabel());
        if (value != null) {
            dialog.setAddress(value.getAddress());
        }
        dialog.setLocation(getCurrentFeature().getLocation());
        dialog.setAddressFieldDialogListener(this.mAddressFieldDialogListener);
        dialog.show(getChildFragmentManager(), TAG_ADDRESS_FIELD_DIALOG);
    }

    private void onAddressChanged(Address address) {
        AddressElement element = getCurrentAddressElement();
        if (element != null) {
            onFormValueDidChange(element, new AddressValue(element, address));
        }
    }

    private void onPhotoEditCaptionButtonClicked(Element element, MediaItemValue item) {
        this.mCurrentElement = element;
        EditCaptionDialog dialog = EditCaptionDialog.getInstance(item);
        dialog.setOnCaptionEditedListener(this.mPhotoCaptionDialogListener);
        dialog.show(getChildFragmentManager(), TAG_PHOTO_CAPTION_DIALOG);
    }

    private void onPhotoCaptionEdited(String photoID, String caption) {
        Element element = getCurrentPhotoElement();
        if (element != null) {
            FormValue value = getFormValue(element);
            if (value instanceof PhotoValue) {
                PhotoValue photo = (PhotoValue) value;
                MediaItemValue item = photo.getMediaItemValue(photoID);
                if (item != null) {
                    item.setCaption(caption);
                    onFormValueDidChange(element, photo);
                }
            }
        }
    }

    private void onRemoveMediaButtonClicked(Element element, MediaItemValue item) {
        Attachment attachment = item.getAttachment();
        if (attachment != null) {
            getEditorActivity().removeAttachment(attachment);
        }
        MediaValue value = (MediaValue) getFormValue(element);
        if (value != null) {
            value.remove(item);
        }
        onFormValueDidChange(element, value);
        HashMap<String, Object> media = new HashMap();
        media.put("id", item.getMediaID());
        if (item instanceof PhotoItemValue) {
            onTriggerEvent("remove-photo", media, element.getDataName());
        } else if (item instanceof VideoItemValue) {
            onTriggerEvent("remove-video", media, element.getDataName());
        } else if (item instanceof AudioItemValue) {
            onTriggerEvent("remove-audio", media, element.getDataName());
        }
    }

    private void onShareMediaButtonClicked(MediaItemValue mediaItemValue, Type mediaType) {
        Activity activity = getActivity();
        Intent intent = IntentUtils.shareMedia(activity, mediaItemValue.getAttachment().getFileOne(), mediaType);
        if (intent != null) {
            activity.startActivity(intent);
        }
    }

    /*private void startVideoRecordActivity() {
        Context context = getActivity();
        this.mAttachmentKey = UUID.randomUUID().toString();
        VideoElement element = getCurrentVideoElement();
        if (element == null) {
            GISLogger.log(String.format("element %s was null", new Object[]{this.mCurrentElement}));
            Toast.makeText(getActivity(), R.string.cant_record_video, 0).show();
            return;
        }
        Intent intent = new Intent(context, VideoCaptureActivity.class);
        intent.putExtra(VideoCaptureActivity.EXTRA_ATTACHMENT_ID, this.mAttachmentKey);
        intent.putExtra(VideoCaptureActivity.EXTRA_TRACK_ENABLED, element.isTrackEnabled());
        intent.putExtra(VideoCaptureActivity.EXTRA_AUDIO_ENABLED, element.isAudioEnabled());
        intent.putExtra(VideoCaptureActivity.EXTRA_VIDEO_QUALITY, getRecord().getForm().getVideoQuality());
        startActivityForResult(intent, ActivityRequestCodes.REQUEST_RECORD_VIDEO);
    }

    private void onRecordVideoActivityResult(Intent data) {
        Bundle extras = data.getExtras();
        String video = extras.getString(VideoCaptureActivity.EXTRA_VIDEO_PATH);
        String track = extras.getString(VideoCaptureActivity.EXTRA_TRACK_PATH);
        long duration = extras.getLong(VideoCaptureActivity.EXTRA_VIDEO_DURATION);
        if (PatronSettings.isSaveVideosToGallery(getEditorActivity())) {
            new CopyMediaTask(getEditorActivity().getContentResolver(), Type.VIDEO, duration).execute(new String[]{video});
        }
        onAddNewVideo(this.mAttachmentKey, video, track);
    }

    private void onImportVideoActivityResult(Intent data) {
        Uri fileURI = null;
        Uri dataURI = data.getData();
        Context context = getActivity();
        String key = UUID.randomUUID().toString();
        if (dataURI != null) {
            InputStream videoContent = null;
            ContentResolver resolver = context.getContentResolver();
            try {
                String type = resolver.getType(dataURI);
                String extension = FileUtils.getExtension(dataURI.getPath());
                if (!TextUtils.isEmpty(extension) && MimeUtils.isAllowedExtension(extension, Type.VIDEO)) {
                    fileURI = Video.generateVideoURI(context, key, MimeUtils.getMimeType(extension, Type.VIDEO));
                } else if (!TextUtils.isEmpty(type) && MimeUtils.isAllowedMimeType(type, Type.VIDEO)) {
                    fileURI = Video.generateVideoURI(context, key, type);
                }
                if (fileURI != null) {
                    File destination = new File(fileURI.getPath());
                    videoContent = resolver.openInputStream(dataURI);
                    StreamUtils.writeToFile(videoContent, destination);
                }
                if (videoContent != null) {
                    try {
                        videoContent.close();
                    } catch (IOException e) {
                    }
                }
            } catch (Throwable e2) {
                GISLogger.log(e2);
                if (videoContent != null) {
                    try {
                        videoContent.close();
                    } catch (IOException e3) {
                    }
                }
            } catch (Throwable th) {
                if (videoContent != null) {
                    try {
                        videoContent.close();
                    } catch (IOException e4) {
                    }
                }
            }
        }
        if (fileURI != null) {
            onAddNewVideo(key, fileURI.getPath(), null);
        } else {
            Toast.makeText(context, R.string.import_video_failed, 1).show();
        }
    }

    private void onAddNewVideo(String key, String videoPath, String trackPath) {
        Element element = getCurrentVideoElement();
        if (element == null) {
            GISLogger.log(String.format("element %s was null", new Object[]{this.mCurrentElement}));
            Toast.makeText(getActivity(), R.string.cant_add_video, 0).show();
            return;
        }
        Video attachment = new Video(key);
        attachment.setPathOne(videoPath, FileUtils.generateThumbnailPath(getActivity(), videoPath));
        attachment.setPathTwo(trackPath);
        List<FeatureValidationError> errors = onTriggerEvent("add-video", ExpressionEvents.createVideoEvent(attachment, getContext()), element.getDataName());
        if (errors.isEmpty()) {
            insertAttachment(attachment);
            VideoValue value = (VideoValue) getFormValue(element);
            if (value == null) {
                value = new VideoValue(element);
            }
            value.addVideo(attachment);
            onFormValueDidChange(element, value);
            return;
        }
        showValidationErrorDialog(errors);
    }

    private void onEditVideoCaptionButtonClicked(Element element, MediaItemValue item) {
        this.mCurrentElement = element;
        EditCaptionDialog dialog = EditCaptionDialog.getInstance(item);
        dialog.setOnCaptionEditedListener(this.mVideoCaptionDialogListener);
        dialog.show(getChildFragmentManager(), TAG_VIDEO_CAPTION_DIALOG);
    }

    private void onVideoCaptionEdited(String videoID, String caption) {
        Element element = getCurrentVideoElement();
        if (element != null) {
            FormValue value = getFormValue(element);
            if (value instanceof VideoValue) {
                VideoValue video = (VideoValue) value;
                MediaItemValue item = video.getMediaItemValue(videoID);
                if (item != null) {
                    item.setCaption(caption);
                    onFormValueDidChange(element, video);
                }
            }
        }
    }

    private void onRecordBarcodeResult(Intent intent) {
        String contents = intent.getStringExtra("SCAN_RESULT");
        BarcodeElement element = getCurrentBarcodeElement();
        if (element != null) {
            BarcodeValue value = new BarcodeValue(element, contents);
            BarcodeFieldDialog barcodeFieldDialog = (BarcodeFieldDialog) getChildFragmentManager().findFragmentByTag(TAG_BARCODE_FIELD_DIALOG);
            if (barcodeFieldDialog != null) {
                barcodeFieldDialog.setBarcodeFieldDialogListener(this.mBarcodeFieldDialogListener);
                barcodeFieldDialog.setBarcodeValue(value);
            }
        }
    }

    private void startAudioRecordActivity() {
        Context context = getActivity();
        this.mAttachmentKey = UUID.randomUUID().toString();
        AudioElement element = getCurrentAudioElement();
        if (element != null) {
            Intent intent = new Intent(context, AudioRecordActivity.class);
            intent.putExtra(VideoCaptureActivity.EXTRA_ATTACHMENT_ID, this.mAttachmentKey);
            intent.putExtra(AudioRecordActivity.EXTRA_FIELD_LABEL, element.getLabel());
            intent.putExtra(VideoCaptureActivity.EXTRA_TRACK_ENABLED, element.isPositionTrackEnabled());
            startActivityForResult(intent, ActivityRequestCodes.REQUEST_RECORD_AUDIO);
        }
    }

    private void onRecordAudioActivityResult(Intent data) {
        Bundle extras = data.getExtras();
        onAddNewAudio(this.mAttachmentKey, extras.getString(AudioRecordActivity.EXTRA_AUDIO_PATH), extras.getString(VideoCaptureActivity.EXTRA_TRACK_PATH));
    }

    private void onImportAudioActivityResult(Intent data) {
        Uri fileURI = null;
        Uri dataURI = data.getData();
        Context context = getActivity();
        String key = UUID.randomUUID().toString();
        if (dataURI != null) {
            InputStream audioContent = null;
            ContentResolver resolver = context.getContentResolver();
            try {
                String type = resolver.getType(dataURI);
                String extension = FileUtils.getExtension(dataURI.getPath());
                if (!TextUtils.isEmpty(extension) && MimeUtils.isAllowedExtension(extension, Type.AUDIO)) {
                    fileURI = Audio.generateAudioURI(context, key, MimeUtils.getMimeType(extension, Type.AUDIO));
                } else if (!TextUtils.isEmpty(type) && MimeUtils.isAllowedMimeType(type, Type.AUDIO)) {
                    fileURI = Audio.generateAudioURI(context, key, type);
                }
                if (fileURI != null) {
                    File destination = new File(fileURI.getPath());
                    audioContent = resolver.openInputStream(dataURI);
                    StreamUtils.writeToFile(audioContent, destination);
                }
                if (audioContent != null) {
                    try {
                        audioContent.close();
                    } catch (IOException e) {
                    }
                }
            } catch (Throwable e2) {
                GISLogger.log(e2);
                if (audioContent != null) {
                    try {
                        audioContent.close();
                    } catch (IOException e3) {
                    }
                }
            } catch (Throwable th) {
                if (audioContent != null) {
                    try {
                        audioContent.close();
                    } catch (IOException e4) {
                    }
                }
            }
        }
        if (fileURI != null) {
            onAddNewAudio(key, fileURI.getPath(), null);
        } else {
            Toast.makeText(context, R.string.import_audio_failed, 1).show();
        }
    }

    private void onAddNewAudio(String key, String audioPath, String trackPath) {
        Element element = getCurrentAudioElement();
        if (element == null) {
            GISLogger.log(String.format("element %s was null", new Object[]{this.mCurrentElement}));
            Toast.makeText(getActivity(), R.string.cant_add_audio, 0).show();
            return;
        }
        Audio attachment = new Audio(key);
        attachment.setPathOne(audioPath, null);
        attachment.setPathTwo(trackPath);
        List<FeatureValidationError> errors = onTriggerEvent("add-audio", ExpressionEvents.createAudioEvent(attachment, getContext()), element.getDataName());
        if (errors.isEmpty()) {
            insertAttachment(attachment);
            AudioValue value = (AudioValue) getFormValue(element);
            if (value == null) {
                value = new AudioValue(element);
            }
            value.addAudio(attachment);
            onFormValueDidChange(element, value);
            return;
        }
        showValidationErrorDialog(errors);
    }

    private void onEditAudioCaptionButtonClicked(Element element, MediaItemValue item) {
        this.mCurrentElement = element;
        EditCaptionDialog dialog = EditCaptionDialog.getInstance(item);
        dialog.setOnCaptionEditedListener(this.mAudioCaptionDialogListener);
        dialog.show(getChildFragmentManager(), TAG_AUDIO_CAPTION_DIALOG);
    }

    private void onAudioCaptionEdited(String audioID, String caption) {
        Element element = getCurrentAudioElement();
        if (element != null) {
            FormValue value = getFormValue(element);
            if (value instanceof AudioValue) {
                AudioValue audio = (AudioValue) value;
                MediaItemValue item = audio.getMediaItemValue(audioID);
                if (item != null) {
                    item.setCaption(caption);
                    onFormValueDidChange(element, audio);
                }
            }
        }
    }
*/
    private void onClassificationFieldDialogFinished(List<String> values, String other) {
        ClassificationElement element = getCurrentClassificationElement();
        if (element == null) {
            return;
        }
        if (values == null && other == null) {
            onFormValueDidChange(element, null);
        } else {
            onFormValueDidChange(element, new ClassificationValue(element, (List) values, other));
        }
    }

    private void onNativeCameraActivityResult() {
        Throwable e;
        if (this.mAttachmentURI != null) {
            ImageFileResizeTask resizeTask = new ImageFileResizeTask(getActivity(), this.mAttachmentURI, getEditorActivity().getLastLocation(), this.mAttachmentKey, getRecord().getForm().getPhotoQuality());
            resizeTask.execute(new Void[0]);
            try {
                resizeTask.get();
            } catch (InterruptedException e2) {
                e = e2;
                GISLogger.log(e);
                if (PatronSettings.isSavePhotosToGallery(getEditorActivity())) {
                    new CopyMediaTask(getEditorActivity().getContentResolver(), Type.PHOTO).execute(new String[]{this.mAttachmentURI.getPath()});
                }
                onAddNewPhoto(this.mAttachmentKey, this.mAttachmentURI);
            } catch (ExecutionException e3) {
                e = e3;
                GISLogger.log(e);
                if (PatronSettings.isSavePhotosToGallery(getEditorActivity())) {
                    new CopyMediaTask(getEditorActivity().getContentResolver(), Type.PHOTO).execute(new String[]{this.mAttachmentURI.getPath()});
                }
                onAddNewPhoto(this.mAttachmentKey, this.mAttachmentURI);
            }
            if (PatronSettings.isSavePhotosToGallery(getEditorActivity())) {
                new CopyMediaTask(getEditorActivity().getContentResolver(), Type.PHOTO).execute(new String[]{this.mAttachmentURI.getPath()});
            }
            onAddNewPhoto(this.mAttachmentKey, this.mAttachmentURI);
        }
    }

    private void onPhotoGalleryActivityResult(Intent data) {
        Throwable e;
        Context context = getActivity();
        Uri destinationURI = null;
        String key = UUID.randomUUID().toString();
        Uri dataURI = data.getData();
        if (dataURI != null) {
            ContentResolver resolver = context.getContentResolver ();
            String type = resolver.getType ( dataURI );
            String extension = FileUtils.getExtension ( dataURI.getPath () );
            if (!TextUtils.isEmpty ( extension ) && MimeUtils.isAllowedExtension ( extension, Type.PHOTO )) {
                destinationURI = Photo.generatePhotoURI ( context, key, MimeUtils.getMimeType ( extension, Type.PHOTO ) );
            } else if (!TextUtils.isEmpty ( type ) && MimeUtils.isAllowedMimeType ( type, Type.PHOTO )) {
                destinationURI = Photo.generatePhotoURI ( context, key, type );
            }
            if (destinationURI != null) {
                File destination = new File ( destinationURI.getPath () );
                InputStream inputStream = null;
                try {
                    inputStream = resolver.openInputStream ( dataURI );
                    StreamUtils.writeToFile ( inputStream, destination );
                    ImageFileResizeTask resizeTask = new ImageFileResizeTask ( context, destinationURI, null, key, getRecord ().getForm ().getPhotoQuality () );
                    resizeTask.execute ( new Void[0] );
                    resizeTask.get ();
                    if (inputStream != null) {
                        try {
                            inputStream.close ();
                        } catch (IOException e2) {
                        }
                    }
                } catch (Throwable e3) {
                    destination.delete ();
                    destinationURI = null;
                    GISLogger.log ( e3 );
                    if (inputStream != null) {
                        try {
                            inputStream.close ();
                        } catch (IOException e4) {
                        }
                    }
                }
            }
        }

        if (destinationURI != null) {
            onAddNewPhoto(key, destinationURI);
        } else {
            Toast.makeText(context, R.string.import_photo_failed, Toast.LENGTH_LONG).show();
        }
    }

    private void onAddNewPhoto(String key, Uri photoURI) {
        Element element = getCurrentPhotoElement();
        if (element != null) {
            Photo attachment = new Photo(key, photoURI, FileUtils.generateThumbnailPath(getActivity(), photoURI.getPath()));
            List<FeatureValidationError> errors = onTriggerEvent("add-photo", ExpressionEvents.createPhotoEvent(attachment), element.getDataName());
            if (errors.isEmpty()) {
                insertAttachment(attachment);
                PhotoValue value = (PhotoValue) getFormValue(element);
                if (value == null) {
                    value = new PhotoValue( (PhotoElement) element );
                }
                value.addPhoto(attachment);
                onFormValueDidChange(element, value);
                return;
            }
            showValidationErrorDialog(errors);
            return;
        }
        GISLogger.log(String.format("element %s was null", new Object[]{this.mCurrentElement}));
        Toast.makeText(getActivity(), R.string.cant_add_photo, Toast.LENGTH_SHORT).show();
    }

    private void onPresentSignatureActivity(SignatureElement element) {
        Context context = getActivity();
        this.mCurrentElement = element;
        this.mAttachmentKey = UUID.randomUUID().toString();
        this.mAttachmentURI = Uri.fromFile(Signature.generateSignatureFile(context, this.mAttachmentKey, "image/png"));
        Intent intent = SignatureActivity.getIntent(context, element);
        intent.putExtra(SignatureActivity.EXTRA_URI, this.mAttachmentURI);
        startActivityForResult(intent, ActivityRequestCodes.REQUEST_EDIT_SIGNATURE);
    }

    private void onSignatureActivityResult(Intent data) {
        Bundle extras = data.getExtras();
        if (extras != null) {
            Element element = getCurrentSignatureElement();
            boolean cleared = extras.getBoolean(SignatureActivity.EXTRA_CLEARED, false);
            SignatureValue oldValue = (SignatureValue) getFormValue(element);
            if (oldValue != null) {
                getEditorActivity().removeAttachment(oldValue.getSignature());
            }
            if (cleared) {
                onFormValueDidChange(element, null);
                return;
            }
            Date timestamp = (Date) extras.getSerializable(SignatureActivity.EXTRA_TIMESTAMP);
            Signature signature = new Signature(this.mAttachmentKey, this.mAttachmentURI, FileUtils.generateThumbnailPath(getActivity(), this.mAttachmentURI.getPath()));
            SignatureValue value = new SignatureValue( (SignatureElement) element );
            value.setSignature(signature);
            value.setTimestamp(timestamp);
            insertAttachment(signature);
            onFormValueDidChange(element, value);
        }
    }

    private void onSetGeometryActivityResult(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            Location location = (Location) extras.getParcelable(RecordPlacementActivity.EXTRA_LOCATION);
            if (location != null) {
                getCurrentFeature().setLocation(location);
                onTriggerGeometryChange();
                onAutoPopulateAddressFields();
                onUpdateCalculatedFields();
                configureLocationMenuItem();
            }
        }
    }

    private void onCreateNewRecordLinkItem(RecordLinkElement element) {
        Form form = element.getForm();
        if (form == null) {
            AlertUtils.showGenericAlert(getActivity(), R.string.form_unavailable_title, R.string.form_unavailable_message);
            return;
        }
        this.mCurrentElement = element;
        startActivityForResult(RecordEditorActivity.getNewRecordIntent(getActivity(), form.getRowID()), ActivityRequestCodes.REQUEST_NEW_RECORD);
    }

    private void onSelectExistingRecordLinkItem(RecordLinkElement element) {
        Form form = element.getForm();
        if (form == null) {
            AlertUtils.showGenericAlert(getActivity(), R.string.form_unavailable_title, R.string.form_unavailable_message);
            return;
        }
        this.mCurrentElement = element;
        RecordPredicate predicate = new RecordPredicate(element);
        predicate.prepare(getRecord(), this.mValuesForConditions);
        ArrayList<String> selectedRecordIDs = null;
        RecordLinkValue value = (RecordLinkValue) getFormValue((Element) element);
        if (value != null) {
            selectedRecordIDs = value.getRecordLinkIdentifiers();
        }
        FragmentActivity activity = getActivity();
        long rowID = form.getRowID();
        boolean z = element.allowsCreating() && !this.mViewOnlyMode;
        startActivityForResult(SelectRecordLinkActivity.getSelectRecordIntent(activity, rowID, predicate, z, selectedRecordIDs), ActivityRequestCodes.REQUEST_SELECT_RECORD);
    }

    public void onRecordLinkItemClicked(RecordLinkElement element, RecordLinkItemValue recordLinkItemValue) {
        Record record = recordLinkItemValue.getRecord();
        if (record != null) {
            this.mCurrentElement = element;
            boolean allowUpdating = element.allowsUpdating();
            if (record.isNew()) {
                allowUpdating = true;
            }
            if (this.mViewOnlyMode) {
                allowUpdating = false;
            }
            startActivityForResult(RecordEditorActivity.getEditRecordIntent(getActivity(), record.getRowID(), !allowUpdating), ActivityRequestCodes.REQUEST_EDIT_RECORD);
        }
    }

    public void showDeleteRecordLinkItemDialog(RecordLinkElement element, RecordLinkItemValue recordLinkItemValue) {
        RemoveRecordLinkDialog dialog = new RemoveRecordLinkDialog();
        dialog.setRemoveRecordLinkDialogListener(this.mRemoveRecordLinkDialogListener);
        dialog.setElementAndItem(element, recordLinkItemValue);
        dialog.show(getChildFragmentManager(), TAG_REMOVE_RECORD_LINK_DIALOG);
    }

    private void onAddRecordLink(Intent data) {
        RecordLinkItemValue recordLink = new RecordLinkItemValue(data.getExtras().getString(EXTRA_FEATURE_IDENTIFIER));
        Element element = getCurrentRecordLinkElement();
        RecordLinkValue value = (RecordLinkValue) getFormValue(element);
        if (value == null) {
            value = new RecordLinkValue( (RecordLinkElement) element );
        }
        value.addRecordLink(recordLink);
        setFormValue(element, value);
        onUpdateFieldsFromRecordLink( (RecordLinkElement) element, recordLink.getRecord());
        onUpdateCalculatedFields();
        onUpdateViews();
        autoSaveDraft();
    }

    private void onUpdateFieldsFromRecordLink(Intent data) {
        RecordLinkItemValue recordLink = new RecordLinkItemValue(data.getExtras().getString(EXTRA_FEATURE_IDENTIFIER));
        Element element = getCurrentRecordLinkElement();
        onUpdateFieldsFromRecordLink( (RecordLinkElement) element, recordLink.getRecord());
        onUpdateCalculatedFields();
        onUpdateViews();
        autoSaveDraft();
        ((RecordLinkElementView) getElementView(element)).refreshRecordLinkItemView(recordLink);
    }

    private void onUpdateFieldsFromRecordLink(RecordLinkElement recordLinkElement, Record otherRecord) {
        DefaultValues.applyDefaultValuesForRecordLinkValue(getRecord().getForm(), recordLinkElement, otherRecord, getCurrentFeature().getFormValues());
        Iterator it = recordLinkElement.getRecordDefaults().iterator();
        while (it.hasNext()) {
            Element element = getRecord().getForm().getElement(((RecordLinkDefault) it.next()).getDestinationKey());
            if (element != null) {
                setFormValue(element, getFormValue(element));
            }
        }
    }

    private DateElement getCurrentDateElement() {
        if (this.mCurrentElement instanceof DateElement) {
            return (DateElement) this.mCurrentElement;
        }
        return null;
    }

    private TimeElement getCurrentTimeElement() {
        if (this.mCurrentElement instanceof TimeElement) {
            return (TimeElement) this.mCurrentElement;
        }
        return null;
    }

    private ChoiceElement getCurrentChoiceElement() {
        if (this.mCurrentElement instanceof ChoiceElement) {
            return (ChoiceElement) this.mCurrentElement;
        }
        return null;
    }

    private BarcodeElement getCurrentBarcodeElement() {
        if (this.mCurrentElement instanceof BarcodeElement) {
            return (BarcodeElement) this.mCurrentElement;
        }
        return null;
    }

    private AudioElement getCurrentAudioElement() {
        return this.mCurrentElement instanceof AudioElement ? (AudioElement) this.mCurrentElement : null;
    }

    private PhotoElement getCurrentPhotoElement() {
        if (this.mCurrentElement instanceof PhotoElement) {
            return (PhotoElement) this.mCurrentElement;
        }
        return null;
    }

    private VideoElement getCurrentVideoElement() {
        return this.mCurrentElement instanceof VideoElement ? (VideoElement) this.mCurrentElement : null;
    }

    private SignatureElement getCurrentSignatureElement() {
        if (this.mCurrentElement instanceof SignatureElement) {
            return (SignatureElement) this.mCurrentElement;
        }
        return null;
    }

    private ClassificationElement getCurrentClassificationElement() {
        if (this.mCurrentElement instanceof ClassificationElement) {
            return (ClassificationElement) this.mCurrentElement;
        }
        return null;
    }

    private AddressElement getCurrentAddressElement() {
        if (this.mCurrentElement instanceof AddressElement) {
            return (AddressElement) this.mCurrentElement;
        }
        return null;
    }

    private RecordLinkElement getCurrentRecordLinkElement() {
        return this.mCurrentElement instanceof RecordLinkElement ? (RecordLinkElement) this.mCurrentElement : null;
    }

    private void dismissKeyboard() {
        Activity activity = getActivity();
        if (activity != null) {
            KeyboardUtils.hideSoftKeyboard(activity);
        }
    }

    private void insertAttachment(Attachment attachment) {
        getEditorActivity().insertAttachment(attachment);
        attachment.setRecord(getRecord());
        attachment.save();
    }

    private void restoreFeature() {
        if (isRecordEditor()) {
            getEditorActivity().restoreAttachments(this.mFeatureEditState);
        }
        getCurrentFeature().loadFromFeatureState(this.mFeatureEditState);
    }

    private void onAutoPopulateAddressFields() {
        if (!this.mViewOnlyMode) {
            onAutoPopulateAddressFields(getElementContainer());
        }
    }

    private void onAutoPopulateAddressFields(ElementContainer container) {
        Iterator it = container.getElements().iterator();
        while (it.hasNext()) {
            Element element = (Element) it.next();
            if (element instanceof AddressElement) {
                AddressElement address = (AddressElement) element;
                if (address.isAutoPopulateEnabled()) {
                    onAutoPopulateAddress(address);
                }
            } else if (element instanceof SectionElement) {
                onAutoPopulateAddressFields((SectionElement) element);
            }
        }
    }

    private void onAutoPopulateAddress(AddressElement element) {
        if (getFormValue((Element) element) == null) {
            Location location = getCurrentFeature().getLocation();
            if (location != null) {
                reverseGeocodeLocation(element, location);
            }
        }
    }

    private void reverseGeocodeLocation(final AddressElement element, Location location) {
        ReverseGeocodeTask task = new ReverseGeocodeTask(getActivity(), location, new Callback() {
            public void onReverseGeocodeFinished(ReverseGeocodeTask task, android.location.Address result) {
                RecordEditorFragment.this.onReverseGeocodeFinished(task, element, result);
            }
        });
        this.mReverseGeocodeTasks.add(task);
        task.execute(new Void[0]);
    }

    private void onReverseGeocodeFinished(ReverseGeocodeTask task, AddressElement element, android.location.Address result) {
        if (!(element == null || result == null)) {
            onFormValueDidChange(element, new AddressValue(element, new Address(result)));
        }
        this.mReverseGeocodeTasks.remove(task);
    }

    private void onSelectStatusButtonClicked() {
        if (Account.getActiveAccount().getRole().canChangeRecordStatus()) {
            StatusElement statusElement = getRecord().getForm().getStatusElement();
            if (statusElement != null) {
                RecordStatusDialog dialog = new RecordStatusDialog();
                dialog.setStatusElement(statusElement);
                dialog.setOnStatusOptionSelectedListener(this.mRecordStatusDialogListener);
                dialog.show(getChildFragmentManager(), TAG_RECORD_STATUS_DIALOG);
                return;
            }
            return;
        }
        AlertUtils.showPermissionsAlert(getActivity(), Role.PERMISSION_CHANGE_STATUS);
    }

    private void onStatusSelected(StatusElement statusElement, StatusOption option) {
        this.mStatusButton.setStatusOption(option);
        this.mStatusButton.setText(getString(R.string.status_placeholder, new Object[]{statusElement.getLabel(), option.getLabel()}));
        getRecord().setRecordStatus(option.getValue());
        onTriggerEvent("change-status", option.getValue(), null);
        onUpdateCalculatedFields();
        onUpdateViews();
    }

    private void onSelectProjectButtonClicked() {
        if (Account.getActiveAccount().getRole().canChangeRecordProject()) {
            ProjectPickerDialog dialog = new ProjectPickerDialog();
            dialog.setSelectedProject(getRecord().getProject());
            dialog.setProjectPickerDialogListener(this.mProjectPickerDialogListener);
            dialog.show(getChildFragmentManager(), TAG_PROJECT_PICKER_DIALOG);
            return;
        }
        AlertUtils.showPermissionsAlert(getActivity(), Role.PERMISSION_CHANGE_PROJECT);
    }

    private void onProjectSelected(Project project) {
        Object name;
        getRecord().setProject(project);
        if (project != null) {
            this.mProjectButton.setText(project.getName());
            PatronSettings.setActiveProjectID(getActivity(), project.getRowID());
        } else {
            this.mProjectButton.setText(R.string.select_project_btn);
            PatronSettings.setActiveProjectID(getActivity(), -1);
        }
        String str = "change-project";
        if (project != null) {
            name = project.getName();
        } else {
            name = null;
        }
        onTriggerEvent(str, name, null);
        onUpdateCalculatedFields();
        onUpdateViews();
    }

    private void onNestedSectionElementSelected(SectionElement element) {
        RecordEditorFragment editor = new RecordEditorFragment();
        editor.mFeatureEditState = this.mFeatureEditState;
        editor.setElementContainer(element);
        if (this.mCurrentFeature != null) {
            editor.setCurrentFeature(this.mCurrentFeature);
        }
        editor.setAddMode(this.mAddMode);
        editor.setViewOnlyMode(this.mViewOnlyMode);
        editor.setTargetFragment(this, 0);
        editor.setParentValuesForConditions(this.mParentValuesForConditions);
        getEditorActivity().pushFragment(this, editor, element.getKey());
    }

    private void onRepeatableElementSelected(RepeatableElement element) {
        RepeatableValue value = (RepeatableValue) getFormValue((Element) element);
        if (value == null) {
            value = new RepeatableValue(element);
        }
        RepeatableIndexFragment index = new RepeatableIndexFragment();
        index.setRepeatableValue(value);
        index.setViewOnlyMode(this.mViewOnlyMode);
        index.setTargetFragment(this, 0);
        index.setParentValuesForConditions(this.mValuesForConditions);
        getEditorActivity().pushFragment(this, index, element.getKey() + "-index");
    }

    private void onSaveChangesMenuItemSelected() {
        dismissKeyboard();
        onSaveFeature();
    }

    private void onSetLocationMenuItemSelected() {
        startActivityForResult(RecordPlacementActivity.getIntent(getActivity(), getCurrentFeature().getLocation()), 100);
    }

    private void onPrintReportMenuItemSelected() {
        dismissKeyboard();
        Account account = Account.getActiveAccount();
        if (account == null || account.getRole().canGenerateReports()) {
            Record record = getRecord();
            boolean requireSynchronization = false;
            if (hasChanges() || !record.isSynchronized()) {
                if (hasPermissionToSave(record, account) && validateFeature(record)) {
                    requireSynchronization = true;
                } else {
                    return;
                }
            }
            setAddMode(false);
            FragmentManager fm = getChildFragmentManager();
            /*if (!ReportGenerationDialog.isShowing()) {
                ReportGenerationDialog.show(fm, TAG_REPORT_GENERATION_DIALOG, record, this.mOnGenerateReportSaveRecordListener, requireSynchronization);
                return;
            }*/
            return;
        }
        AlertUtils.showPermissionsAlert(getActivity(), Role.PERMISSION_GENERATE_REPORTS);
    }

    private void onDiscardChangesOptionSelected() {
        dismissKeyboard();
        stopLocationUpdates();
        restoreFeature();
        if (isRecordEditor()) {
            if (this.mAddMode) {
                getRecord().deleteWithTransaction();
            } else {
                getRecord().saveWithTransaction();
            }
            releaseExpressionEngine();
            RecordEditorActivity activity = getEditorActivity();
            activity.discardAttachmentChanges();
            activity.finish();
            return;
        }
        Fragment target = getTargetFragment();
        if (target != null && (target instanceof RecordEditorFragmentListener)) {
            RecordEditorFragmentListener listener = (RecordEditorFragmentListener) target;
            if (this.mAddMode) {
                listener.onFeatureDeleted(getCurrentFeature());
            } else {
                listener.onFeatureEdited(getCurrentFeature(), true);
            }
        }
        getFragmentManager().popBackStack();
    }

    private void onRemoveRecordLinkOptionSelected(RecordLinkElement element, RecordLinkItemValue recordLinkItemValue) {
        if (element != null && recordLinkItemValue != null) {
            RecordLinkValue value = (RecordLinkValue) getFormValue((Element) element);
            if (value != null) {
                value.remove(recordLinkItemValue);
                onFormValueDidChange(element, value);
            }
        }
    }

    private void onSaveFeature() {
        if (isRecordEditor()) {
            boolean sync = false;
            if (getRecord().getForm().isAutoSyncEnabled(getActivity()) && NetworkUtils.isConnected(getActivity())) {
                sync = true;
            }
            onSaveRecord(sync, true);
            return;
        }
        onSaveRepeatable(false);
    }

    private List<FeatureValidationError> onValidateFeature(Feature feature) {
        return FeatureValidator.validateFeature(feature, getRecord(), this.mValuesForConditions);
    }

    private void onUpdateAuditData(Feature feature) {
        Date now = new Date();
        if (feature.getCreatedAt() == null) {
            feature.setCreatedAt(now);
        }
        feature.setUpdatedAt(now);
        if (getRecord().getForm().isEditLocationsEnabled().booleanValue()) {
            Location location = getEditorActivity().getLastLocation();
            if (this.mAddMode && feature.getCreatedLocation() == null) {
                feature.setCreatedLocation(location);
            }
            feature.setUpdatedLocation(location);
        } else {
            feature.setCreatedLocation(this.mFeatureEditState.getCreatedLocation());
            feature.setUpdatedLocation(this.mFeatureEditState.getUpdatedLocation());
        }
        if (getRecord().getForm().isEditDurationsEnabled().booleanValue()) {
            int seconds = ((int) (SystemClock.uptimeMillis() - this.mStartTime)) / 1000;
            if (this.mAddMode && (this.mFeatureEditState.getCreatedDuration() == null || feature.getCreatedDuration() == null)) {
                feature.setCreatedDuration(Integer.valueOf(seconds));
            }
            feature.setUpdatedDuration(Integer.valueOf(seconds));
            if (this.mFeatureEditState.getEditedDuration() != null) {
                feature.setEditedDuration(Integer.valueOf(this.mFeatureEditState.getEditedDuration().intValue() + seconds));
                return;
            } else {
                feature.setEditedDuration(Integer.valueOf(seconds));
                return;
            }
        }
        feature.setCreatedDuration(this.mFeatureEditState.getCreatedDuration());
        feature.setUpdatedDuration(this.mFeatureEditState.getUpdatedDuration());
        feature.setEditedDuration(this.mFeatureEditState.getEditedDuration());
    }

    private void onSaveDraft() {
        if (isRecordEditor()) {
            onSaveRecordDraft(true);
        } else {
            onSaveRepeatable(true);
        }
    }

    private void onSaveRepeatable(boolean asDraft) {
        Record record = getRecord();
        Account account = Account.getActiveAccount();
        if (record.isNew()) {
            if (!account.getRole().canCreateRecords()) {
                AlertUtils.showPermissionsAlert(getActivity(), Role.PERMISSION_CREATE_RECORDS);
                return;
            }
        } else if (!(account.getRole().canUpdateRecords() || this.mViewOnlyMode)) {
            AlertUtils.showPermissionsAlert(getActivity(), Role.PERMISSION_UPDATE_RECORDS);
            return;
        }
        Feature feature = getCurrentFeature();
        if (!asDraft) {
            if (validateFeature(feature)) {
                onUpdateAuditData(feature);
            } else {
                return;
            }
        }
        feature.getFormValues().clearInvisibleValues(this.mValuesForConditions, getRecord());
        onTriggerEvent("save-repeatable", null, ((RepeatableItemValue) getCurrentFeature()).getElement().getDataName());
        record.getForm().updateStickyDefaults(feature.getFormValues(), record);
        Fragment target = getTargetFragment();
        if (target != null && (target instanceof RecordEditorFragmentListener)) {
            ((RecordEditorFragmentListener) target).onFeatureEdited(feature, true);
        }
        getFragmentManager().popBackStack();
    }

    private boolean onSaveRecord(boolean syncAfterSave, boolean finishAfterSave) {
        Record record = getRecord();
        Account account = Account.getActiveAccount();
        if (!hasPermissionToSave(record, account) || !validateFeature(record)) {
            return false;
        }
        onUpdateAuditData(record);
        record.getFormValues().clearInvisibleValues(this.mValuesForConditions, record);
        onTriggerEvent("save-record");
        Form form = record.getForm();
        form.updateStickyDefaults(record.getFormValues(), record);
        record.setDraft(false);
        record.setSynchronized(false);
        if (record.saveWithTransaction()) {
            if (this.mAddMode) {
                GISLogger.logNewRecord();
                form.incrementRecordCount();
            }
            getEditorActivity().persistAttachmentChanges();
            if (syncAfterSave && NetworkUtils.isConnected(getActivity())) {
                SyncService.synchronizeRecords(getActivity(), account);
            }
            if (finishAfterSave) {
                finishAfterSave();
            }
            return true;
        }
        Toast.makeText(getActivity(), "There was an unknown error while saving the record.", Toast.LENGTH_LONG).show();
        return false;
    }

    private void onSaveRecordDraft(boolean finishAfterSave) {
        Record record = getRecord();
        if (hasPermissionToSave(record, Account.getActiveAccount())) {
            record.getFormValues().clearInvisibleValues(this.mValuesForConditions, record);
            onTriggerEvent("save-record");
            record.setDraft(true);
            record.setSynchronized(false);
            onUpdateAuditData(record);
            record.saveWithTransaction();
            if (finishAfterSave) {
                finishAfterSave();
            }
        }
    }

    private boolean hasPermissionToSave(Record record, Account account) {
        if (record.isNew()) {
            if (!account.getRole().canCreateRecords()) {
                AlertUtils.showPermissionsAlert(getActivity(), Role.PERMISSION_CREATE_RECORDS);
                return false;
            }
        } else if (!(account.getRole().canUpdateRecords() || this.mViewOnlyMode)) {
            AlertUtils.showPermissionsAlert(getActivity(), Role.PERMISSION_UPDATE_RECORDS);
            return false;
        }
        return true;
    }

    private boolean validateFeature(Feature feature) {
        List<FeatureValidationError> errors = onValidateFeature(feature);
        List<FeatureValidationError> customErrors = new ArrayList();
        if (feature instanceof Record) {
            customErrors = onTriggerValidateRecord();
        } else if (feature instanceof RepeatableItemValue) {
            customErrors = onTriggerValidateRepeatable();
        }
        errors.addAll(customErrors);
        if (errors.isEmpty()) {
            return true;
        }
        FeatureValidationErrorDialog dialog = FeatureValidationErrorDialog.getInstance(errors, getRecord().getForm().isDraftsEnabled().booleanValue());
        dialog.setListener(this.mFeatureValidationListener);
        dialog.show(getChildFragmentManager(), TAG_FEATURE_VALIDATION_DIALOG);
        return false;
    }

    private void showValidationErrorDialog(List<FeatureValidationError> errors) {
        if (errors != null && !errors.isEmpty()) {
            ArrayList<String> messages = new ArrayList();
            for (FeatureValidationError error : errors) {
                messages.add(error.getMessage());
            }
            showErrorDialog(TextUtils.join("\n", messages));
        }
    }

    private void showErrorDialog(String message) {
        if (getActivity() != null) {
            GenericDialog dialog = new GenericDialog();
            dialog.setTitle((int) R.string.error);
            dialog.setMessage(message);
            dialog.setCancelable(false);
            dialog.setPositiveButtonText((int) R.string.okay);
            FragmentTransaction beginTransaction = getChildFragmentManager().beginTransaction();
            StringBuilder append = new StringBuilder().append("TAG_CUSTOM_DIALOG_");
            int i = sDialogCount + 1;
            sDialogCount = i;
            beginTransaction.add(dialog, append.append(i).toString()).show(dialog).commitAllowingStateLoss();
        }
    }

    private void finishAfterSave() {
        stopLocationUpdates();
        releaseExpressionEngine();
        Intent intent = new Intent();
        intent.putExtra(EXTRA_FEATURE_IDENTIFIER, getCurrentFeature().getIdentifier());
        getActivity().setResult(-1, intent);
        getActivity().finish();
    }

    private void autoSaveDraft() {
        if (isRootEditor() && isRecordEditor()) {
            Record record = getRecord();
            record.setDraft(true);
            record.setSynchronized(false);
            onUpdateAuditData(record);
            record.saveWithTransaction();
            getEditorActivity().persistAttachmentChanges();
            return;
        }
        Feature feature = getCurrentFeature();
        onUpdateAuditData(feature);
        Fragment target = getTargetFragment();
        if (target != null && (target instanceof RecordEditorFragmentListener)) {
            ((RecordEditorFragmentListener) target).onFeatureEdited(feature, true);
        }
    }

    private void startLocationUpdates() {
        this.mProcessLocationUpdates = true;
        if (PermissionsUtils.checkPermissionsAndRequest(getActivity(), 64)) {
            getEditorActivity().startLocationUpdates();
        }
    }

    private void stopLocationUpdates() {
        this.mProcessLocationUpdates = false;
    }

    private void onUpdateActivityTitle() {
        String title = null;
        ElementContainer elementContainer = getElementContainer();
        if (elementContainer instanceof Form) {
            title = ((Form) elementContainer).getName();
        } else if (elementContainer instanceof Element) {
            title = ((Element) elementContainer).getLabel();
        }
        getActivity().setTitle(title);
    }

    private void onUpdateButtons() {
        onUpdateProjectButton();
        onUpdateStatusButton();
        if (this.mProjectButton != null && this.mStatusButton != null && this.mProjectButton.getVisibility() == View.VISIBLE && this.mStatusButton.getVisibility() == View.VISIBLE) {
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int positiveBottomMargin = (int) TypedValue.applyDimension(1, 10.0f, metrics);
            int negativeSideMargin = (int) TypedValue.applyDimension(1, -15.0f, metrics);
            LayoutParams lp = new LayoutParams(-1, getResources().getDimensionPixelSize(R.dimen.project_button_height));
            lp.setMargins(negativeSideMargin, 0, negativeSideMargin, positiveBottomMargin);
            this.mProjectButton.setLayoutParams(lp);
        }
        configureLocationMenuItem();
    }
}
