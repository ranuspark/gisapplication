package com.example.bhumicloud.gisapplications.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.dossier.PersistentObject;
import com.example.bhumicloud.gisapplications.util.CursorUtils;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class Account extends PersistentObject {
    public static final String COLUMN_CONTEXT_ID = "context_id";
    public static final String COLUMN_CONTEXT_NAME = "context_name";
    public static final String COLUMN_CONTEXT_TYPE = "context_type";
    public static final String COLUMN_FIRST_NAME = "first_name";
    public static final String COLUMN_LAST_NAME = "last_name";
    public static final String COLUMN_TOKEN = "token";
    public static final String COLUMN_ROLE = "role";
    public static final String COLUMN_USER_EMAIL = "user_email";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String TABLE_NAME = "Accounts";
    public static final String TYPE_USER = "user";
    public static final Creator<Account> CREATOR = new Creator<Account> () {
        @Override
        public Account createFromParcel(Parcel source) {
            return new Account ( source );
        }

        @Override
        public Account[] newArray(int size) {
            return new Account[size];
        }
    };
    private static Account sActiveAccount;
    private String mAuthenticationToken;
    private String mContextID;
    private String mContextName;
    private String mContextType;
    private String mFirstName;
    private String mLastName;
    private String mUserEmail;
    private String mUserID;
    private Storage mStorage;
    private Role mRole;
    private Context context;

    public Account(){ }
    public Account(Context context) {
        this.context = context;
    }

    public Account(Cursor cursor) {
        super ( cursor );
        this.mUserID = cursor.getString ( cursor.getColumnIndex ( "user_id" ) );
        this.mFirstName = CursorUtils.getString ( cursor, COLUMN_FIRST_NAME );
        this.mLastName = CursorUtils.getString ( cursor, COLUMN_LAST_NAME );
        this.mUserEmail = cursor.getString ( cursor.getColumnIndex ( COLUMN_USER_EMAIL ) );
        this.mContextID = cursor.getString ( cursor.getColumnIndex ( COLUMN_CONTEXT_ID ) );
        this.mContextName = cursor.getString ( cursor.getColumnIndex ( COLUMN_CONTEXT_NAME ) );
        this.mContextType = cursor.getString ( cursor.getColumnIndex ( COLUMN_CONTEXT_TYPE ) );
        this.mAuthenticationToken = cursor.getString ( cursor.getColumnIndex ( COLUMN_TOKEN ) );
        Map roleJSON = null;
        String roleJSONStr = cursor.getString ( cursor.getColumnIndex ( COLUMN_ROLE ) );
        if (!TextUtils.isEmpty ( roleJSONStr )) {
            try {
                roleJSON = JSONUtils.objectFromJSON ( roleJSONStr );
            } catch (Throwable e) {
                GISLogger.log ( e );
            }
        }
        this.mRole = new Role ( roleJSON );
    }

    private Account(Parcel parcel) {
        super ( parcel );
        this.mUserID = parcel.readString ();
        this.mFirstName = parcel.readString ();
        this.mLastName = parcel.readString ();
        this.mUserEmail = parcel.readString ();
        this.mContextID = parcel.readString ();
        this.mContextName = parcel.readString ();
        this.mContextType = parcel.readString ();
        this.mAuthenticationToken = parcel.readString ();
        this.mRole = (Role) parcel.readParcelable ( getClass ().getClassLoader () );


    }

    public static Account getAccount(Map user, Map context) {
        String userId = JSONUtils.getString ( user, "id" );
        String contextId = JSONUtils.getString ( context, "id" );
        ArrayList<String> predicateList = new ArrayList ();
        ArrayList<String> paramsList = new ArrayList ();
        predicateList.add ( "user_id = ?" );
        paramsList.add ( userId );
        predicateList.add ( "context_id = ?" );
        paramsList.add ( contextId );
        String[] params = (String[]) paramsList.toArray ( new String[paramsList.size ()] );
        Cursor cursor = GIS.getDatabase ().query ( TABLE_NAME, null, TextUtils.join ( " AND ", predicateList ), params, null, null, null, null );
        Account account = null;
        cursor.moveToFirst ();
        if (!cursor.isAfterLast ()) {
            account = new Account ( cursor );
        }
        cursor.close ();
        if (account == null) {
            account = new  Account();
        }
        account.setAttributesFromJSON ( user, context );
        return account;
    }

    public static synchronized Account getActiveAccount() {
        Account activeAccount;
        synchronized (Account.class) {
            activeAccount = getActiveAccount ( GIS.getInstance () );
        }
        return activeAccount;
    }

    public static synchronized void setActiveAccount(Account account) {
        synchronized (Account.class) {
            setActiveAccount ( GIS.getInstance (), account );
        }
    }

    public static synchronized Account getActiveAccount(Context context) {
        Account account;
        synchronized (Account.class) {
            if (sActiveAccount != null) {
                account = sActiveAccount;
            } else {
                account = PatronSettings.getActiveAccount ( context );
                setActiveAccount ( context, account, false );
            }
        }
        return account;
    }

    public static synchronized Account setActiveAccount(Context context, long accountRowId) {
        Account account;
        synchronized (Account.class) {
            account = getAccount ( accountRowId );
            setActiveAccount ( context, account );
        }
        return account;
    }

    public static synchronized void setActiveAccount(Context context, Account account) {
        synchronized (Account.class) {
            setActiveAccount ( context, account, true );
        }
    }

    public static Account getAccount(long id) {
        return getAccount ( id, false );
    }

    public static Account getAccount(long id, boolean forceQuery) {
        if (sActiveAccount != null && sActiveAccount.getRowID () == id && !forceQuery) {
            return sActiveAccount;
        }
        String[] subArgs = new String[]{String.valueOf ( id )};
        Account account = null;
        Cursor cursor = GIS.getDatabase ().query ( TABLE_NAME, null, "_id = ?", subArgs, null, null, null );
        cursor.moveToFirst ();
        if (!cursor.isAfterLast ()) {
            account = new Account ( cursor );
        }
        cursor.close ();
        return account;
    }

    private static void setActiveAccount(Context context, Account account, boolean saveToPreferences) {
        sActiveAccount = account;
        GISLogger.setActiveAccount ( account );
        if (saveToPreferences) {
            PatronSettings.setActiveAccount ( context, account );
        }
    }

    public Storage getStorage() {
        if (this.mStorage == null) {
            this.mStorage = new Storage ( this );
        }
        return this.mStorage;
    }

    public Role getRole() {
        if (this.mRole == null) {
            this.mRole = new Role ( null );
        }
        return this.mRole;
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel ( dest, flags );
        dest.writeString ( getUserID () );
        dest.writeString ( getFirstName () );
        dest.writeString ( getLastName () );
        dest.writeString ( getUserEmailAddress () );
        dest.writeString ( getContextID () );
        dest.writeString ( getContextName () );
        dest.writeString ( this.mContextType );
        dest.writeString ( getToken () );
        dest.writeParcelable ( this.mRole, flags );

    }

    public String getUserID() {
        return this.mUserID;
    }

    public String getFirstName() {
        return this.mFirstName;
    }

    public String getLastName() {
        return this.mLastName;
    }

    public String getUserName() {
        if (!TextUtils.isEmpty ( this.mFirstName ) && !TextUtils.isEmpty ( this.mLastName )) {
            return this.mFirstName + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + this.mLastName;
        }
        if (!TextUtils.isEmpty ( this.mFirstName )) {
            return this.mFirstName;
        }
        if (TextUtils.isEmpty ( this.mLastName )) {
            return "";
        }
        return this.mLastName;
    }
    public String getUserEmailAddress() {
        return this.mUserEmail;
    }

    public String getContextID() {
        return this.mContextID;
    }

    public String getContextName() {
        return this.mContextName;
    }

    public String getToken() {
        return this.mAuthenticationToken;
    }


    public void setAttributesFromJSON(Map user, Map context) {
        this.mUserID = JSONUtils.getString ( user, "id" );
        this.mFirstName = JSONUtils.getString ( user, COLUMN_FIRST_NAME );
        this.mLastName = JSONUtils.getString ( user, COLUMN_LAST_NAME );
        this.mUserEmail = JSONUtils.getString ( user, "user_email" );
        this.mContextID = JSONUtils.getString ( context, "id" );
        this.mContextName = JSONUtils.getString ( context, "name" );
        this.mContextType = JSONUtils.getString ( context, "type", TYPE_USER );
        this.mAuthenticationToken = JSONUtils.getString ( context, "api_token" );
        this.mRole = new Role ( JSONUtils.getHashMap ( context, COLUMN_ROLE ) );
    }

    public void synchronizeAccounts(HashMap<String, Object> user) {
        clearContexts ();
        ArrayList<Object> contexts = JSONUtils.getArrayList ( user, "contexts" );
        if (contexts != null && contexts.size () > 0) {
            Iterator it = contexts.iterator ();
            while (it.hasNext ()) {
                upsert(user, JSONUtils.getHashMap(it.next()));
            }
        }
    }

    public static Account upsert(Map user, Map context) {
        Account account = getAccount(user, context);
        account.save();
        return account;
    }

    public void clearContexts() {
        String[] params = new String[]{getUserID (), String.valueOf ( getRowID () )};
        ContentValues values = new ContentValues ();
        values.putNull ( COLUMN_TOKEN );
        GIS.getDatabase ().update ( TABLE_NAME, values, "user_id = ? AND _id != ?", params );
    }

    public ArrayList<Account> getContexts() {
        String[] params = new String[]{getUserID ()};
        Cursor cursor = GIS.getDatabase ().query ( TABLE_NAME, null, "user_id = ? AND token IS NOT NULL", params, null, null, "_id ASC", null );
        ArrayList<Account> accounts = new ArrayList ();
        cursor.moveToFirst ();
        while (!cursor.isAfterLast ()) {
            accounts.add ( new Account ( cursor ) );
            cursor.moveToNext ();
        }
        cursor.close ();
        return accounts;
    }

    protected ContentValues getContentValues() {
        ContentValues values = super.getContentValues ();
        values.put ( "user_id", this.mUserID );
        values.put ( COLUMN_FIRST_NAME, this.mFirstName );
        values.put ( COLUMN_LAST_NAME, this.mLastName );
        values.put ( COLUMN_USER_EMAIL, this.mUserEmail );
        values.put ( COLUMN_CONTEXT_ID, this.mContextID );
        values.put ( COLUMN_CONTEXT_NAME, this.mContextName );
        values.put ( COLUMN_CONTEXT_TYPE, this.mContextType );
        values.put ( COLUMN_TOKEN, this.mAuthenticationToken );
        values.put ( COLUMN_ROLE, JSONUtils.toJSONString ( getRole ().toJSON () ) );

        return values;
    }

    protected String getTableName() {
        return TABLE_NAME;
    }
}

