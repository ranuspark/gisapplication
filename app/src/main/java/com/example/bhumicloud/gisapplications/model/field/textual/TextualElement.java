package com.example.bhumicloud.gisapplications.model.field.textual;

import android.os.Parcel;

import com.example.bhumicloud.gisapplications.model.field.Element;

import java.util.Map;

public abstract class TextualElement extends Element {
    public TextualElement(Element parent, Map json) {
        super(parent, json);
    }

    protected TextualElement(Parcel parcel) {
        super(parcel);
    }
}
