package com.example.bhumicloud.gisapplications.model.field.recordlink;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.Map;

public class RecordLinkDefault implements Parcelable {
    public static final Creator<RecordLinkDefault> CREATOR = new C11941();
    private static final String JSON_RECORD_LINK_DESTINATION_FIELD_KEY = "destination_field_key";
    private static final String JSON_RECORD_LINK_SOURCE_FIELD_KEY = "source_field_key";
    private String mDestinationKey;
    private String mSourceKey;

    static class C11941 implements Creator<RecordLinkDefault> {
        C11941() {
        }

        public RecordLinkDefault createFromParcel(Parcel source) {
            return new RecordLinkDefault(source);
        }

        public RecordLinkDefault[] newArray(int size) {
            return new RecordLinkDefault[size];
        }
    }

    public RecordLinkDefault(Map json) {
        this.mSourceKey = JSONUtils.getString(json, JSON_RECORD_LINK_SOURCE_FIELD_KEY);
        this.mDestinationKey = JSONUtils.getString(json, JSON_RECORD_LINK_DESTINATION_FIELD_KEY);
    }

    private RecordLinkDefault(Parcel parcel) {
        this.mSourceKey = parcel.readString();
        this.mDestinationKey = parcel.readString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mSourceKey);
        dest.writeString(this.mDestinationKey);
    }

    public int describeContents() {
        return 0;
    }

    public String getSourceKey() {
        return this.mSourceKey;
    }

    public String getDestinationKey() {
        return this.mDestinationKey;
    }
}
