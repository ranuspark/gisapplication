package com.example.bhumicloud.gisapplications.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.support.v4.internal.view.SupportMenu;
import android.util.AttributeSet;
import android.view.View;

import com.example.bhumicloud.gisapplications.R;

public class CircularProgressBar extends View {
    private final int mArcWidth = getResources().getDimensionPixelSize(R.dimen.circular_progress_arc_width);
    private RectF mBounds = new RectF ();
    private final Paint mPaint = new Paint ();
    private float mProgress;

    public CircularProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStyle( Style.STROKE);
        this.mPaint.setTextAlign( Align.CENTER);
        this.mPaint.setStrokeWidth((float) this.mArcWidth);
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        int arcW = this.mArcWidth;
        float diameter = (float) Math.min(w - ((getPaddingLeft() + getPaddingRight()) + arcW), h - ((getPaddingTop() + getPaddingBottom()) + arcW));
        RectF bounds = new RectF (0.0f, 0.0f, diameter, diameter);
        bounds.offsetTo((float) getPaddingLeft(), (float) getPaddingTop());
        bounds.offset(((float) arcW) / 2.0f, ((float) arcW) / 2.0f);
        this.mBounds = new RectF (bounds);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = this.mPaint;
        float cx = ((float) getLayoutParams().width) / 2.0f;
        float cy = ((float) getLayoutParams().height) / 2.0f;
        paint.setColor(-3355444);
        paint.setStrokeWidth(((float) this.mArcWidth) / 2.0f);
        canvas.rotate(-45.0f, cx, cy);
        for (int i = 0; i < 4; i++) {
            canvas.drawLine(cx, cy, cx, cy - (0.4f * cy), paint);
            canvas.rotate(-90.0f, cx, cy);
        }
        canvas.rotate(-45.0f, cx, cy);
        paint.setStrokeWidth((float) this.mArcWidth);
        canvas.drawArc(this.mBounds, 0.0f, 360.0f, false, paint);
        paint.setColor( SupportMenu.CATEGORY_MASK);
        Canvas canvas2 = canvas;
        canvas2.drawArc(this.mBounds, 0.0f, 360.0f * this.mProgress, false, paint);
    }

    public void setProgress(float progress) {
        this.mProgress = progress;
        postInvalidate();
    }
}
