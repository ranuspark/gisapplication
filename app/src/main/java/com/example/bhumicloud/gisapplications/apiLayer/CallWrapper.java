package com.example.bhumicloud.gisapplications.apiLayer;

import com.example.bhumicloud.gisapplications.apiLayer.exception.GISServiceException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.AudioFieldDisabledException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.BadRequestException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.InvalidCredentialsException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.InvalidResponseException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.ResourceNotFoundException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.ServerMaintenanceException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.UnprocessableEntityException;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

public class CallWrapper<T> {
    public Response<T> execute(Call<T> call) throws GISServiceException {
        Exception e;
        Response<T> response = null;
        try {
            response = call.execute();
        } catch (IOException e2) {
            e = e2;
            gisServiceException (e);
            if (!response.isSuccessful()) {
                handleUnsuccessful(response);
            }
            return response;
        } catch (RuntimeException e3) {
            e = e3;
            gisServiceException (e);
            if (response.isSuccessful()) {
                handleUnsuccessful(response);
            }
            return response;
        }
        if (response.isSuccessful()) {
            handleUnsuccessful(response);
        }
        return response;
    }

    private void handleUnsuccessful(retrofit2.Response r9) throws GISServiceException {

    }

    private void gisServiceException(Exception e) throws GISServiceException {
        throw new GISServiceException((Throwable) e);
    }

    private void badRequestException(String message) throws BadRequestException {
        throw new BadRequestException(message);
    }

    private void invalidCredentialsException() throws InvalidCredentialsException {
        throw new InvalidCredentialsException();
    }

    private void audioFieldDisabledException() throws AudioFieldDisabledException {
        throw new AudioFieldDisabledException();
    }

    private void invalidResponseException(int code, String message) throws InvalidResponseException {
        throw new InvalidResponseException(code, message);
    }

    private void resourceNotFoundException() throws ResourceNotFoundException {
        throw new ResourceNotFoundException();
    }

    private void unprocessableEntityException(String message) throws UnprocessableEntityException {
        throw new UnprocessableEntityException(message);
    }

    private void serverMaintenanceException() throws ServerMaintenanceException {
        throw new ServerMaintenanceException();
    }
}
