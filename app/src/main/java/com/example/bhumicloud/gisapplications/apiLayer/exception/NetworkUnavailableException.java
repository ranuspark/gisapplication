package com.example.bhumicloud.gisapplications.apiLayer.exception;

import android.content.Context;

import com.example.bhumicloud.gisapplications.R;

public class NetworkUnavailableException extends GISServiceException {
    public NetworkUnavailableException() {
        super("It appears that you are not connected to the Internet. Please check your network settings and try again.");
    }

    public String getUserFriendlyMessage(Context context) {
        return context.getString(R.string.internet_unavailable_message);
    }
}
