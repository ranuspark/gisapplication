package com.example.bhumicloud.gisapplications.Activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bhumicloud.gisapplications.R;

public class DiscardChangesDialog extends ThemedDialogFragment {
    private DiscardChangesDialogListener mListener;

    public interface DiscardChangesDialogListener {
        void onPositiveButtonClicked();
    }

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        setTitle((int) R.string.record_unsaved_changes_title);
        setPositiveButtonText((int) R.string.discard);
        setNegativeButtonText((int) R.string.cancel);
    }

    public View onCreateContentView(LayoutInflater inflater, ViewGroup root, Bundle savedState) {
        View view = inflater.inflate(R.layout.dialog_single_message, root, false);
        ((TextView) view.findViewById(R.id.dialog_message)).setText(R.string.record_unsaved_changes_message);
        return view;
    }

    public void setDiscardChangesDialogListener(DiscardChangesDialogListener listener) {
        this.mListener = listener;
    }

    protected void onPositiveButtonClicked() {
        super.onPositiveButtonClicked();
        this.mListener.onPositiveButtonClicked();
    }

    protected void onNegativeButtonClicked() {
        super.onNegativeButtonClicked();
        dismiss();
    }
}
