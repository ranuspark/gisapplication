package com.example.bhumicloud.gisapplications.dossier;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.example.bhumicloud.gisapplications.GIS;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;
import java.util.ArrayList;

public abstract class PersistentObject implements Parcelable {
    @QueryField("_id")
    public static final String COLUMN_ROW_ID = "_id";
    private long mRowID;

    @Retention(RetentionPolicy.RUNTIME)
    public @interface QueryField {
        String value();
    }

    public PersistentObject(){};
    protected abstract String getTableName();

    public PersistentObject(Cursor cursor) {
        if (cursor != null) {
            this.mRowID = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
        }
    }

    protected PersistentObject(Parcel parcel) {
        this.mRowID = parcel.readLong();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.mRowID);
    }

    public long getRowID() {
        return this.mRowID;
    }

    public boolean isPersisted() {
        return this.mRowID > 0;
    }

    public boolean save() {
        return save(null);
    }

    public void delete() {
        delete(null, null);
    }

    protected static ArrayList<String> buildColumns(Class clazz) {
        ArrayList<String> list = new ArrayList();
        for (Field field : clazz.getFields()) {
            if (field.isAnnotationPresent(QueryField.class)) {
                list.add(((QueryField) field.getAnnotation(QueryField.class)).value());
            }
        }
        return list;
    }

    protected ContentValues getContentValues() {
        return new ContentValues();
    }

    protected boolean save(SQLiteDatabase db) {
        if (db == null) {
            db = GIS.getDatabase();
        }
        String table = getTableName();
        ContentValues cv = getContentValues();
        if (isPersisted()) {
            if (db.update(table, cv, "_id = ?", new String[]{String.valueOf(this.mRowID)}) <= 0) {
                return false;
            }
            return true;
        }
        this.mRowID = db.insert(table, null, cv);
        return isPersisted();
    }

    protected boolean saveColumns(ContentValues contentValues) {
        return saveColumns(null, contentValues);
    }

    protected boolean saveColumns(SQLiteDatabase db, ContentValues contentValues) {
        if (db == null) {
            db = GIS.getDatabase();
        }
        String table = getTableName();
        if (isPersisted()) {
            if (db.update(table, contentValues, "_id = ?", new String[]{String.valueOf(this.mRowID)}) <= 0) {
                return false;
            }
            return true;
        }
        ContentValues cv = getContentValues();
        cv.putAll(contentValues);
        this.mRowID = db.insert(table, null, cv);
        return isPersisted();
    }

    protected void delete(SQLiteDatabase db, Bundle options) {
        if (isPersisted()) {
            if (db == null) {
                db = GIS.getDatabase();
            }
            db.delete(getTableName(), "_id = ?", new String[]{String.valueOf(this.mRowID)});
        }
    }
}

