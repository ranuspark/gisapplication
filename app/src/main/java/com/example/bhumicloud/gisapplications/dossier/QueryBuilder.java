package com.example.bhumicloud.gisapplications.dossier;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

public class QueryBuilder {
    private List<String> mArguments;
    private List<String> mPredicates;
    private String mTable;

    public QueryBuilder(String table) {
        this.mTable = table;
    }

    public QueryBuilder where(String predicate, int arg) {
        return where(predicate, String.valueOf(arg));
    }

    public QueryBuilder where(String predicate, long arg) {
        return where(predicate, String.valueOf(arg));
    }

    public QueryBuilder where(String predicate, boolean arg) {
        return where(predicate, arg ? 1 : 0);
    }

    public QueryBuilder where(String predicate, String arg) {
        if (!TextUtils.isEmpty(predicate)) {
            if (this.mPredicates == null) {
                this.mPredicates = new ArrayList ();
            }
            this.mPredicates.add(predicate);
        }
        if (!TextUtils.isEmpty(arg)) {
            if (this.mArguments == null) {
                this.mArguments = new ArrayList ();
            }
            this.mArguments.add(arg);
        }
        return this;
    }

    public Cursor cursor(SQLiteDatabase db) {
        String predicate = null;
        if (!(this.mPredicates == null || this.mPredicates.isEmpty())) {
            predicate = TextUtils.join(" AND ", this.mPredicates);
        }
        String[] subArgs = null;
        if (!(this.mArguments == null || this.mArguments.isEmpty())) {
            subArgs = (String[]) this.mArguments.toArray(new String[this.mArguments.size()]);
        }
        return db.query(this.mTable, null, predicate, subArgs, null, null, null, null);
    }
}
