package com.example.bhumicloud.gisapplications.geoJson;

import android.location.Location;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Coordinate {
    private static final int ALT_IDX = 2;
    private static final int LAT_IDX = 1;
    private static final int LON_IDX = 0;
    private final double[] mStorage;

    public Coordinate(List json) {
        this.mStorage = new double[3];
        this.mStorage[0] = JSONUtils.getDouble(json, 0);
        this.mStorage[1] = JSONUtils.getDouble(json, 1);
        this.mStorage[2] = JSONUtils.getDouble(json, 2);
    }

    public Coordinate(double latitude, double longitude) {
        this.mStorage = new double[3];
        this.mStorage[1] = latitude;
        this.mStorage[0] = longitude;
    }

    public Coordinate(Location location) {
        this.mStorage = new double[3];
        this.mStorage[1] = location.getLatitude();
        this.mStorage[0] = location.getLongitude();
        this.mStorage[2] = location.getAltitude();
    }

    public double getLatitude() {
        return this.mStorage[1];
    }

    public double getLongitude() {
        return this.mStorage[0];
    }

    public double getAltitude() {
        return this.mStorage[2];
    }

    public ArrayList toGeoJSON() {
        ArrayList<Object> coordinates = new ArrayList();
        coordinates.add(0, Double.valueOf(getLongitude()));
        coordinates.add(1, Double.valueOf(getLatitude()));
        coordinates.add(2, Double.valueOf(getAltitude()));
        return coordinates;
    }

    public Location toLocation() {
        Location location = new Location("GeoJSON");
        location.setLatitude(getLatitude());
        location.setLongitude(getLongitude());
        location.setAltitude(getAltitude());
        return location;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Coordinate)) {
            return false;
        }
        Coordinate aCoordinate = (Coordinate) object;
        if (aCoordinate.canEqual(this)) {
            return Arrays.equals(this.mStorage, aCoordinate.mStorage);
        }
        return false;
    }

    public boolean canEqual(Object other) {
        return other instanceof Coordinate;
    }

    public int hashCode() {
        return Arrays.hashCode(this.mStorage);
    }

    public String toString() {
        return Arrays.toString(this.mStorage);
    }
}

