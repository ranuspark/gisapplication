package com.example.bhumicloud.gisapplications.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bhumicloud.gisapplications.util.KeyboardUtils;
import com.google.zxing.client.android.Intents.Scan;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.dossier.RecordPredicate;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.Role;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.example.bhumicloud.gisapplications.util.AlertUtils;
import com.example.bhumicloud.gisapplications.util.PermissionsUtils;
import com.example.bhumicloud.gisapplications.widget.SearchRelativeLayout;
import com.example.bhumicloud.gisapplications.widget.SearchRelativeLayout.OnSearchQueryChangedListener;
import com.journeyapps.barcodescanner.CaptureActivity;

import java.util.ArrayList;

public class SelectRecordLinkActivity extends LocationListenerActivity implements RecordCollectionViewListener {
    private static final String EXTRA_ELEMENT_ALLOWS_CREATING = "SelectRecordActivity.EXTRA_ELEMENT_ALLOWS_CREATING";
    private static final String EXTRA_FORM_ID = "SelectRecordActivity.EXTRA_FORM_ID";
    private static final String EXTRA_RECORD_LINK_PREDICATE = "SelectRecordActivity.EXTRA_RECORD_LINK_PREDICATE";
    private static final String EXTRA_SELECTED_RECORD_LINKS = "SelectRecordActivity.EXTRA_SELECTED_RECORD_LINKS";
    private static final String STATE_SEARCH_QUERY = "STATE_SEARCH_QUERY";
    private static final String STATE_SEARCH_VISIBLE = "STATE_SEARCH_VISIBLE";
    private static final int VIEW_MODE_LIST = 2;
    private static final int VIEW_MODE_MAP = 1;
    private long mActiveFormID;
    private int mActiveViewMode;
    private boolean mAllowsCreating;
    private RecordCollectionView mCollectionView;
    private ArrayList<String> mLinkedRecordIDs;
    private RecordPredicate mRecordPredicate;
    private EditText mSearchActionView;
    private String mSearchQuery;
    private boolean mSearchVisible;
    private MenuItem mViewToggleItem;

    class C11281 implements OnSearchQueryChangedListener {
        C11281() {
        }

        public void onSearchQueryChanged(String query) {
            SelectRecordLinkActivity.this.mSearchQuery = query;
            if (SelectRecordLinkActivity.this.mCollectionView != null) {
                SelectRecordLinkActivity.this.mCollectionView.setSearchQuery(query);
            }
        }
    }

    public static Intent getSelectRecordIntent(Context context, long formRowID, RecordPredicate predicate, boolean allowsCreating, ArrayList<String> linkedRecordIdentifiers) {
        Intent i = new Intent (context, SelectRecordLinkActivity.class);
        i.putExtra(EXTRA_FORM_ID, formRowID);
        i.putExtra(EXTRA_RECORD_LINK_PREDICATE, predicate);
        i.putExtra(EXTRA_ELEMENT_ALLOWS_CREATING, allowsCreating);
        i.putStringArrayListExtra(EXTRA_SELECTED_RECORD_LINKS, linkedRecordIdentifiers);
        return i;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_select_record_link);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        setTitle(R.string.select_record);
        Bundle bundle = getIntent().getExtras();
        this.mActiveFormID = bundle.getLong(EXTRA_FORM_ID);
        this.mRecordPredicate = (RecordPredicate) bundle.getParcelable(EXTRA_RECORD_LINK_PREDICATE);
        this.mAllowsCreating = bundle.getBoolean(EXTRA_ELEMENT_ALLOWS_CREATING);
        this.mLinkedRecordIDs = bundle.getStringArrayList(EXTRA_SELECTED_RECORD_LINKS);
        this.mActiveViewMode = 2;
        FragmentManager fm = getSupportFragmentManager();
        if (savedInstanceState != null) {
            this.mSearchQuery = savedInstanceState.getString(STATE_SEARCH_QUERY);
            this.mSearchVisible = savedInstanceState.getBoolean(STATE_SEARCH_VISIBLE, false);
        }
        Fragment collectionView = fm.findFragmentById(R.id.container);
        if (collectionView instanceof RecordCollectionView) {
            this.mCollectionView = (RecordCollectionView) collectionView;
        }
        configureCollectionView();
    }

    @SuppressLint({"InflateParams"})
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.activity_select_record_link, menu);
        this.mViewToggleItem = menu.findItem(R.id.menu_item_view_toggle);
        if (this.mActiveViewMode == 1) {
            this.mViewToggleItem.setIcon(R.drawable.ic_action_list);
            this.mViewToggleItem.setTitle(R.string.list_view);
        } else {
            this.mViewToggleItem.setIcon(R.drawable.ic_action_map);
            this.mViewToggleItem.setTitle(R.string.map_view);
        }
        MenuItem searchRecordsItem = menu.findItem(R.id.menu_item_search);
        final SearchRelativeLayout searchView = (SearchRelativeLayout) MenuItemCompat.getActionView(searchRecordsItem);
        this.mSearchActionView = searchView.getSearchEditText();
        this.mSearchActionView.setContentDescription("search");
        searchView.setOnSearchQueryChangedListener(new C11281());
        MenuItemCompat.setOnActionExpandListener(searchRecordsItem, new OnActionExpandListener () {
            public boolean onMenuItemActionExpand(MenuItem item) {
                SelectRecordLinkActivity.this.mSearchVisible = true;
                searchView.onMenuItemActionExpand(item);
                return true;
            }

            public boolean onMenuItemActionCollapse(MenuItem item) {
                SelectRecordLinkActivity.this.mSearchVisible = false;
                searchView.onMenuItemActionCollapse(item);
                return true;
            }
        });
        this.mSearchActionView.setText(this.mSearchQuery);
        if (this.mSearchVisible) {
            searchRecordsItem.expandActionView();
            if (this.mSearchQuery != null) {
                this.mSearchActionView.setSelection(this.mSearchQuery.length());
            }
        }
        return true;
    }

    public void onResume() {
        super.onResume();
        setViewMode(this.mActiveViewMode);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_view_toggle:
                onToggleViewModeOptionSelected();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_SEARCH_QUERY, this.mSearchQuery);
        outState.putBoolean(STATE_SEARCH_VISIBLE, this.mSearchVisible);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case 106:
                    String contents = data.getStringExtra( Scan.RESULT);
                    if (contents != null) {
                        if (this.mSearchActionView == null) {
                            this.mSearchQuery = contents;
                            break;
                        }
                        this.mSearchActionView.setText(contents);
                        this.mSearchActionView.setSelection(contents.length());
                        break;
                    }
                    break;
                case 109:
                    setResult(resultCode, data);
                    finish();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onRecordSelected(long recordID) {
        KeyboardUtils.hideSoftKeyboard(this);
        Record record = Record.getRecord(recordID);
        Intent intent = new Intent ();
        intent.putExtra(RecordEditorFragment.EXTRA_FEATURE_IDENTIFIER, record.getIdentifier());
        setResult(-1, intent);
        finish();
    }

    public void onShowRecordLocation(long recordID, double latitude, double longitude) {
    }

    public void onDuplicateRecord(long recordID, boolean location, boolean values) {
    }

    public void onGenerateReport(long record) {
    }

    public void onDeleteRecord(long recordID) {
    }

    public void onNewRecordOptionSelected() {
        Account account = Account.getActiveAccount();
        if (account == null) {
            return;
        }
        if (account.getRole().canCreateRecords()) {
            startActivityForResult(RecordEditorActivity.getNewRecordIntent(this, getActiveFormID()), 109);
        } else {
            AlertUtils.showPermissionsAlert(this, Role.PERMISSION_CREATE_RECORDS);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        int i = 0;
        boolean granted;
        int length;
        if ((requestCode & 64) == 64) {
            if (grantResults.length > 0) {
                granted = true;
                length = grantResults.length;
                while (i < length) {
                    if (grantResults[i] != 0) {
                        granted = false;
                    }
                    i++;
                }
                if (granted) {
                    Fragment frag = getSupportFragmentManager().findFragmentById(R.id.container);
                    if (frag != null && (frag instanceof MapViewFragment)) {
                        ((MapViewFragment) frag).setMyLocationEnabled(null);
                    }
                }
            }
        } else if ((requestCode & 16) == 16) {
            if (grantResults.length > 0) {
                granted = true;
                for (int result : grantResults) {
                    if (result != 0) {
                        granted = false;
                    }
                }
                if (granted) {
                    startActivityForResult(new Intent (this, CaptureActivity.class), 106);
                    return;
                }
            }
            Toast.makeText(this, R.string.permissions_required, Toast.LENGTH_SHORT).show();
        }
    }

    private void onToggleViewModeOptionSelected() {
        switch (this.mActiveViewMode) {
            case 1:
                setViewMode(2);
                return;
            case 2:
                setViewMode(1);
                return;
            default:
                return;
        }
    }

    private long getActiveFormID() {
        return this.mActiveFormID;
    }

    private void setViewMode(int mode) {
        setViewMode(mode, true);
    }

    private void setViewMode(int mode, boolean async) {
        Bundle args = new Bundle ();
      //  args.putInt(RecordCollectionView.EXTRA_LIST_ROW_LAYOUT_ID, R.adap_search.list_item_select_record);
        args.putBoolean(RecordCollectionView.EXTRA_CONTEXT_MENU_ENABLED, false);
        args.putBoolean(RecordCollectionView.EXTRA_CREATE_RECORDS_ENABLED, this.mAllowsCreating);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        switch (mode) {
            case 1:
                if (this.mViewToggleItem != null) {
                    this.mViewToggleItem.setIcon(R.drawable.ic_action_list);
                    this.mViewToggleItem.setTitle(R.string.list_view);
                }
                if (!(this.mCollectionView instanceof RecordMapFragment)) {
                    RecordMapFragment map = new RecordMapFragment();
                    map.setArguments(args);
                    ft.replace(R.id.container, map);
                    this.mCollectionView = map;
                    break;
                }
                break;
            case 2:
                if (this.mViewToggleItem != null) {
                    this.mViewToggleItem.setIcon(R.drawable.ic_action_map);
                    this.mViewToggleItem.setTitle(R.string.map_view);
                }
                if (!(this.mCollectionView instanceof RecordListFragment)) {
                    RecordListFragment list = new RecordListFragment();
                    list.setArguments(args);
                    ft.replace(R.id.container, list);
                    this.mCollectionView = list;
                    break;
                }
                break;
        }
        try {
            ft.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
        }
        configureCollectionView();
        this.mActiveViewMode = mode;
        if (!async) {
            fm.executePendingTransactions();
        }
        PatronSettings.setActiveViewMode(this, mode);
    }

    private void configureCollectionView() {
        if (this.mCollectionView != null) {
            this.mCollectionView.setFormID(getActiveFormID());
            this.mCollectionView.setRecordConditions(this.mRecordPredicate);
            this.mCollectionView.setSelectedRecordLinkIDs(this.mLinkedRecordIDs);
        }
    }
}
