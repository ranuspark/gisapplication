package com.example.bhumicloud.gisapplications.model.javascript.expression;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.eclipsesource.v8.V8Object;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.Storage;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaElement;
import com.example.bhumicloud.gisapplications.model.field.choice.ChoiceElement;
import com.example.bhumicloud.gisapplications.model.field.classification.ClassificationElement;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement;
import com.example.bhumicloud.gisapplications.model.javascript.JavaScriptResource;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import com.example.bhumicloud.gisapplications.util.NumberUtils;

import java.util.List;
import java.util.Map;

public class ExpressionResult {
    private static final String ELEMENT_AUTO_LOCATION_ENABLED = "auto_location_enabled";
    private static final String ELEMENT_AUTO_LOCATION_MINIMUM_ACCURACY = "auto_location_minimum_accuracy";
    private static final String ELEMENT_AUTO_SYNC_ENABLED = "auto_sync_enabled";
    private static final String ELEMENT_CHOICES = "choices";
    private static final String ELEMENT_CHOICE_FILTER = "choice_filter";
    private static final String ELEMENT_DESCRIPTION = "description";
    private static final String ELEMENT_DISABLED = "disabled";
    private static final String ELEMENT_DRAFTS_ENABLED = "drafts_enabled";
    private static final String ELEMENT_EDIT_DURATIONS_ENABLED = "edit_durations_enabled";
    private static final String ELEMENT_EDIT_LOCATIONS_ENABLED = "edit_locations_enabled";
    private static final String ELEMENT_HIDDEN = "hidden";
    private static final String ELEMENT_LABEL = "label";
    private static final String ELEMENT_MANUAL_LOCATION_ENABLED = "manual_location_enabled";
    private static final String ELEMENT_MAX_LENGTH = "max_length";
    private static final String ELEMENT_MEDIA_CAPTURE_ENABLED = "media_capture_enabled";
    private static final String ELEMENT_MEDIA_GALLERY_ENABLED = "media_gallery_enabled";
    private static final String ELEMENT_MIN_LENGTH = "min_length";
    private static final String ELEMENT_PHOTO_QUALITY = "photo_quality";
    private static final String ELEMENT_REQUIRED = "required";
    private static final String ELEMENT_VIDEO_QUALITY = "video_quality";
    private static final String TYPE_CALCULATION = "calculation";
    private static final String TYPE_CONFIGURE = "configure";
    private static final String TYPE_MESSAGE = "message";
    private static final String TYPE_OPEN = "open";
    private static final String TYPE_PROGRESS = "progress";
    private static final String TYPE_SET_VALUE = "set-value";
    private static final String TYPE_UPDATE_ELEMENT = "update-element";
    private static final String TYPE_VALIDATION = "validation";
    private final String mAttribute;
    private final String mError;
    private final String mKey;
    private final String mMessage;
    private final String mTitle;
    private final String mType;
    private final Object mValue;

    public ExpressionResult(V8Object result) {
        this.mType = getStringValueForAttribute(result, "type");
        this.mKey = getStringValueForAttribute(result, Storage.COLUMN_KEY);
        this.mError = getStringValueForAttribute(result, "error");
        this.mTitle = getStringValueForAttribute(result, "title");
        this.mMessage = getStringValueForAttribute(result, "message");
        this.mAttribute = getStringValueForAttribute(result, "attribute");
        Object value = null;
        if (isCalculationResult()) {
            value = getStringValueForAttribute(result, "value");
        } else {
            Object rawValue = result.get("value");
            if (rawValue instanceof String) {
                try {
                    value = JSONUtils.fromJSON((String) rawValue);
                } catch (Throwable e) {
                    GISLogger.log(e);
                }
            }
            JavaScriptResource.release(rawValue);
        }
        this.mValue = value;
    }

    public void process(Context context, Form form, Element element) {
        if (isUpdateElementResult()) {
            processUpdateElement(form, element);
        } else if (isOpenResult()) {
            processOpenResult(context);
        }
    }

    public void processUpdateElement(Form form, Element element) {
        if (this.mAttribute != null) {
            String attribute = getAttribute();
            int obj = -1;
            switch (attribute.hashCode()) {
                case -1724546052:
                    if (attribute.equals("description")) {
                        obj = 1;
                        break;
                    }
                    break;
                case -1617047237:
                    if (attribute.equals(ELEMENT_VIDEO_QUALITY)) {
                        obj = 19;
                        break;
                    }
                    break;
                case -1486171923:
                    if (attribute.equals(ELEMENT_MEDIA_CAPTURE_ENABLED)) {
                        obj = 14;
                        break;
                    }
                    break;
                case -1225042958:
                    if (attribute.equals(ELEMENT_PHOTO_QUALITY)) {
                        obj = 18;
                        break;
                    }
                    break;
                case -1217487446:
                    if (attribute.equals(ELEMENT_HIDDEN)) {
                        obj = 3;
                        break;
                    }
                    break;
                case -974781482:
                    if (attribute.equals(ELEMENT_CHOICE_FILTER)) {
                        obj = 8;
                        break;
                    }
                    break;
                case -723481584:
                    if (attribute.equals(ELEMENT_MANUAL_LOCATION_ENABLED)) {
                        obj = 12;
                        break;
                    }
                    break;
                case -711577229:
                    if (attribute.equals(ELEMENT_MIN_LENGTH)) {
                        obj = 5;
                        break;
                    }
                    break;
                case -682101653:
                    if (attribute.equals(ELEMENT_EDIT_LOCATIONS_ENABLED)) {
                        obj = 16;
                        break;
                    }
                    break;
                case -548441748:
                    if (attribute.equals(ELEMENT_EDIT_DURATIONS_ENABLED)) {
                        obj = 17;
                        break;
                    }
                    break;
                case -393139297:
                    if (attribute.equals(ELEMENT_REQUIRED)) {
                        obj = 2;
                        break;
                    }
                    break;
                case 102727412:
                    if (attribute.equals(ELEMENT_LABEL)) {
                        obj = 99999;
                        break;
                    }
                    break;
                case 270940796:
                    if (attribute.equals(ELEMENT_DISABLED)) {
                        obj = 4;
                        break;
                    }
                    break;
                case 549978669:
                    if (attribute.equals(ELEMENT_AUTO_SYNC_ENABLED)) {
                        obj = 9;
                        break;
                    }
                    break;
                case 655284980:
                    if (attribute.equals(ELEMENT_DRAFTS_ENABLED)) {
                        obj = 15;
                        break;
                    }
                    break;
                case 751720178:
                    if (attribute.equals("choices")) {
                        obj = 7;
                        break;
                    }
                    break;
                case 850199879:
                    if (attribute.equals(ELEMENT_AUTO_LOCATION_ENABLED)) {
                        obj = 10;
                        break;
                    }
                    break;
                case 1111390753:
                    if (attribute.equals(ELEMENT_MAX_LENGTH)) {
                        obj = 6;
                        break;
                    }
                    break;
                case 1582085369:
                    if (attribute.equals(ELEMENT_MEDIA_GALLERY_ENABLED)) {
                        obj = 13;
                        break;
                    }
                    break;
                case 1804400324:
                    if (attribute.equals(ELEMENT_AUTO_LOCATION_MINIMUM_ACCURACY)) {
                        obj = 11;
                        break;
                    }
                    break;
            }
            switch (obj) {
                case 99999:
                    if (element != null) {
                        element.setOverrideLabel(getValueAsString());
                        return;
                    }
                    return;
                case 1:
                    if (element != null) {
                        element.setOverrideDescription(getValueAsString());
                        return;
                    }
                    return;
                case 2:
                    if (element != null) {
                        element.setOverrideIsRequired(getValueAsBoolean());
                        return;
                    }
                    return;
                case 3:
                    if (element != null) {
                        element.setOverrideIsHidden(getValueAsBoolean());
                        return;
                    }
                    return;
                case 4:
                    if (element != null) {
                        element.setOverrideIsDisabled(getValueAsBoolean());
                        return;
                    }
                    return;
                case 5:
                    if (element != null) {
                        element.setOverrideMinLength(getValueAsInteger());
                        return;
                    }
                    return;
                case 6:
                    if (element != null) {
                        element.setOverrideMaxLength(getValueAsInteger());
                        return;
                    }
                    return;
                case 7:
                    if (element instanceof ChoiceElement) {
                        ((ChoiceElement) element).setOverrideChoices(getValueAsList());
                        return;
                    } else if (element instanceof ClassificationElement) {
                        ((ClassificationElement) element).setOverrideClassificationItems(getValueAsList());
                        return;
                    } else {
                        return;
                    }
                case 8:
                    if (element instanceof ChoiceElement) {
                        ((ChoiceElement) element).setChoicesFilter(getValueAsList());
                        return;
                    } else if (element instanceof ClassificationElement) {
                        ((ClassificationElement) element).setClassificationFilter(getValueAsList());
                        return;
                    } else if (element instanceof StatusElement) {
                        ((StatusElement) element).setStatusFilter(getValueAsList());
                        return;
                    } else {
                        return;
                    }
                case 9:
                    form.setOverrideAutoSyncEnabled(getValueAsBoolean());
                    return;
                case 10:
                    form.setOverrideAutoLocationEnabled(getValueAsBoolean());
                    return;
                case 11:
                    form.setOverrideAutoLocationMinimumAccuracy(getValueAsDouble());
                    return;
                case 12:
                    form.setOverrideManualLocationEnabled(getValueAsBoolean());
                    return;
                case 13:
                    if (element instanceof MediaElement) {
                        ((MediaElement) element).setOverrideMediaGalleryEnabled(getValueAsBoolean());
                        return;
                    } else {
                        form.setOverrideMediaGalleryEnabled(getValueAsBoolean());
                        return;
                    }
                case 14:
                    if (element instanceof MediaElement) {
                        ((MediaElement) element).setOverrideMediaCaptureEnabled(getValueAsBoolean());
                        return;
                    } else {
                        form.setOverrideMediaCaptureEnabled(getValueAsBoolean());
                        return;
                    }
                case 15:
                    form.setOverrideDraftsEnabled(getValueAsBoolean());
                    return;
                case 16:
                    form.setOverrideEditLocationsEnabled(getValueAsBoolean());
                    return;
                case 17:
                    form.setOverrideEditDurationsEnabled(getValueAsBoolean());
                    return;
                case 18:
                    form.setOverridePhotoQuality(getValueAsString());
                    return;
                case 19:
                    form.setOverrideVideoQuality(getValueAsString());
                    return;
                default:
                    return;
            }
        }
    }

    void processOpenResult(Context context) {
        String url = getValueAsString();
        if (url != null) {
            Intent intent = new Intent ("android.intent.action.VIEW", Uri.parse(url));
            if (context.getPackageManager().queryIntentActivities(intent, 0).size() > 0) {
                context.startActivity(intent);
            }
        }
    }

    public boolean isCalculationResult() {
        return this.mType.equals(TYPE_CALCULATION);
    }

    public boolean isSetValueResult() {
        return this.mType.equals(TYPE_SET_VALUE);
    }

    public boolean isOpenResult() {
        return this.mType.equals(TYPE_OPEN);
    }

    public boolean isMessageResult() {
        return this.mType.equals("message");
    }

    public boolean isValidationResult() {
        return this.mType.equals(TYPE_VALIDATION);
    }

    public boolean isUpdateElementResult() {
        return this.mType.equals(TYPE_UPDATE_ELEMENT);
    }

    public boolean isConfigureResult() {
        return this.mType.equals(TYPE_CONFIGURE);
    }

    public boolean isProgressResult() {
        return this.mType.equals("progress");
    }

    public String getKey() {
        return this.mKey;
    }

    public String getError() {
        return this.mError;
    }

    public String getTitle() {
        return this.mTitle;
    }

    public String getMessage() {
        return this.mMessage;
    }

    public String getAttribute() {
        return this.mAttribute;
    }

    public Object getRawValue() {
        return this.mValue;
    }

    public String getValueAsString() {
        return this.mValue instanceof String ? (String) this.mValue : null;
    }

    public Integer getValueAsInteger() {
        return this.mValue instanceof Number ? Integer.valueOf(((Number) this.mValue).intValue()) : null;
    }

    public Double getValueAsDouble() {
        return this.mValue instanceof Number ? Double.valueOf(((Number) this.mValue).doubleValue()) : null;
    }

    public Boolean getValueAsBoolean() {
        return this.mValue instanceof Boolean ? (Boolean) this.mValue : null;
    }

    public List getValueAsList() {
        return this.mValue instanceof List ? (List) this.mValue : null;
    }

    public Map<String, Object> getValueAsMap() {
        return this.mValue instanceof Map ? (Map) this.mValue : null;
    }

    private String getStringValueForAttribute(V8Object result, String attribute) {
        Object value = result.get(attribute);
        String stringValue = null;
        if (value instanceof Number) {
            stringValue = NumberUtils.formatMachineStringFromNumber(((Number) value).doubleValue());
        } else if (value != null) {
            stringValue = value.toString();
        }
        JavaScriptResource.release(value);
        return stringValue;
    }
}
