package com.example.bhumicloud.gisapplications.model.javascript;

import android.content.Context;
import android.util.Log;


import com.eclipsesource.v8.Releasable;
import com.eclipsesource.v8.V8;
import com.eclipsesource.v8.V8Object;
import com.eclipsesource.v8.V8Value;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import com.example.bhumicloud.gisapplications.util.StreamUtils;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.Map;

public abstract class JavaScriptResource {
    protected static final String JS_LOG = "JsLog";
    private static final String VARIABLE_RUNTIME = "$$runtime";
    protected V8 mJavaScriptContext;
    private String mScript;

    protected abstract File getAutoUpdatingScriptPath(Context context);

    protected abstract String getEnvironmentName();

    protected abstract int getJavaScriptRawResourceID();

    public JavaScriptResource(Context context) {
        setup(context);
    }

    public void release() {
        if (this.mJavaScriptContext != null) {
            this.mJavaScriptContext.release(false);
        }
    }

    public static void logError(String msg) {
        Log.e(JS_LOG, msg);
    }

    public static void release(Object value) {
        if (value instanceof Releasable) {
            ((Releasable) value).release();
        }
    }

    private void setup(Context context) {
        this.mJavaScriptContext = V8.createV8Runtime();
        this.mJavaScriptContext.executeVoidScript(getRuntimeScript(context), getEnvironmentName(), 1);
        V8Object console = new V8Object(this.mJavaScriptContext);
        this.mJavaScriptContext.add("console", (V8Value) console);
        console.registerJavaMethod(this, "logError", "log", new Class[]{String.class});
        console.registerJavaMethod(this, "logError", "error", new Class[]{String.class});
        console.release();
        setupRuntimeVariables(context);
        setupRuntimeFunctions();
    }

    protected V8 getContext() {
        return this.mJavaScriptContext;
    }

    protected void setupRuntimeVariables(Context context) {
    }

    protected void setupRuntimeFunctions() {
    }

    protected V8Object getRuntimeVariable() {
        return getContext().getObject(VARIABLE_RUNTIME);
    }

    protected void assignNullRuntimeVariable(String variableName) {
        V8Object runtime = getRuntimeVariable();
        runtime.addNull(variableName);
        runtime.release();
    }

    protected void assignRuntimeVariable(String variableName, String value) {
        if (value != null) {
            V8Object runtime = getRuntimeVariable();
            runtime.add(variableName, value);
            runtime.release();
            return;
        }
        assignNullRuntimeVariable(variableName);
    }

    protected void assignRuntimeVariable(String variableName, Boolean value) {
        if (value != null) {
            V8Object runtime = getRuntimeVariable();
            runtime.add(variableName, value.booleanValue());
            runtime.release();
            return;
        }
        assignNullRuntimeVariable(variableName);
    }

    protected void assignRuntimeVariable(String variableName, Double value) {
        if (value != null) {
            V8Object runtime = getRuntimeVariable();
            runtime.add(variableName, value.doubleValue());
            runtime.release();
            return;
        }
        assignNullRuntimeVariable(variableName);
    }

    protected void assignRuntimeVariable(String variableName, Float value) {
        if (value != null) {
            V8Object runtime = getRuntimeVariable();
            runtime.add(variableName, (double) value.floatValue());
            runtime.release();
            return;
        }
        assignNullRuntimeVariable(variableName);
    }

    protected void assignRuntimeVariable(String variableName, Integer value) {
        if (value != null) {
            V8Object runtime = getRuntimeVariable();
            runtime.add(variableName, value.intValue());
            runtime.release();
            return;
        }
        assignNullRuntimeVariable(variableName);
    }

    protected void assignGlobalVariable(String variableName, String value) {
        getContext().add(variableName, value);
    }

    protected void evaluateGlobalVariableAssignment(String variableName, String rawSource) {
        getContext().executeVoidScript(String.format("%s = %s;", new Object[]{variableName, rawSource}), variableName, 1);
    }

    protected void evaluateRuntimeVariableAssignment(String variableName, Map object) {
        evaluateRuntimeVariableAssignment(variableName, JSONUtils.toJSONString(object));
    }

    protected void evaluateRuntimeVariableAssignmentFromArray(String variableName, List object) {
        evaluateRuntimeVariableAssignment(variableName, JSONUtils.toJSONString(object));
    }

    protected void evaluateRuntimeVariableAssignment(String variableName, String rawSource) {
        getContext().executeVoidScript(String.format("%s.%s = %s;", new Object[]{VARIABLE_RUNTIME, variableName, rawSource}), variableName, 1);
    }

    protected Object evaluateFunctionCall(String functionName) {
        return getContext().executeObjectScript(String.format("%s();", new Object[]{functionName}), functionName, 1);
    }

    protected void evaluateVoidFunctionCall(String functionName) {
        getContext().executeVoidScript(String.format("%s();", new Object[]{functionName}), functionName, 1);
    }

    protected String getRuntimeScript(Context context) {
        return getScript(context);
    }

    private String getScript(Context context) {
        if (this.mScript != null) {
            return this.mScript;
        }
        String script = "%s";
        try {
            script = StreamUtils.toString(context.getResources().openRawResource(getJavaScriptRawResourceID()));
        } catch (Throwable e) {
            GISLogger.log(e);
        }
        this.mScript = script;
        return this.mScript;
    }

    private String getAutoUpdatingScript(Context context) {
        File path = getAutoUpdatingScriptPath(context);
        if (!path.exists()) {
            return null;
        }
        StringBuffer fileContent = new StringBuffer();
        try {
            FileInputStream input = new FileInputStream(path);
            byte[] buffer = new byte[4096];
            while (true) {
                int count = input.read(buffer);
                if (count == -1) {
                    break;
                }
                fileContent.append(new String(buffer, 0, count));
            }
            input.close();
        } catch (Throwable e) {
            fileContent = null;
            GISLogger.log(e);
        }
        if (fileContent != null) {
            return fileContent.toString();
        }
        return null;
    }
}
