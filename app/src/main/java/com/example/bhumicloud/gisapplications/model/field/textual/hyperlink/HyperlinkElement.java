package com.example.bhumicloud.gisapplications.model.field.textual.hyperlink;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.textual.TextualElement;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.HashMap;
import java.util.Map;

public class HyperlinkElement extends TextualElement {
    public static final Creator<HyperlinkElement> CREATOR = new C12041();
    private static final String JSON_DEFAULT_URL = "default_url";
    private String mDefaultURL;

    static class C12041 implements Creator<HyperlinkElement> {
        C12041() {
        }

        public HyperlinkElement createFromParcel(Parcel source) {
            return new HyperlinkElement(source);
        }

        public HyperlinkElement[] newArray(int size) {
            return new HyperlinkElement[size];
        }
    }

    public HyperlinkElement(Element parent, Map json) {
        super(parent, json);
    }

    private HyperlinkElement(Parcel parcel) {
        super(parcel);
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = super.toJSON();
        json.put(JSON_DEFAULT_URL, this.mDefaultURL);
        return json;
    }

    public Uri getDefaultURL() {
        if (TextUtils.isEmpty(this.mDefaultURL)) {
            return null;
        }
        return Uri.parse(this.mDefaultURL);
    }

    public boolean hasDefaultURL() {
        return !TextUtils.isEmpty(this.mDefaultURL);
    }

    public String getType() {
        return Element.TYPE_HYPERLINK;
    }

    public void setJSONAttributes(Map json) {
        super.setJSONAttributes(json);
        this.mDefaultURL = JSONUtils.getString(json, JSON_DEFAULT_URL);
    }
}
