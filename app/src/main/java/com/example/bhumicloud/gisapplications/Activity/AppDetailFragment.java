package com.example.bhumicloud.gisapplications.Activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.appgallery.App;

public class AppDetailFragment extends GISFragment {
    private static final String ARG_APP = "gis:arg:app";
    private AppDetailFragmentListener mListener;

    public interface AppDetailFragmentListener {
        void onAddAppClick(App app);
    }

    public static AppDetailFragment getInstance(App app) {
        Bundle args = new Bundle ();
        AppDetailFragment fragment = new AppDetailFragment();
        args.putParcelable(ARG_APP, app);
        fragment.setArguments(args);
        return fragment;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AppDetailFragmentListener) {
            this.mListener = (AppDetailFragmentListener) context;
            return;
        }
        throw new RuntimeException (context.toString() + " must implement " + AppDetailFragmentListener.class.getSimpleName());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_app_detail, container, false);
        final App app = (App) getArguments().getParcelable(ARG_APP);
        if (app == null) {
            Toast.makeText(getActivity(), R.string.app_not_on_device, 0).show();
        } else {
            TextView titleView = (TextView) view.findViewById(R.id.title_view);
            TextView descriptionView = (TextView) view.findViewById(R.id.description_view);
            Button addAppBtn = (Button) view.findViewById(R.id.add_app_btn);
            titleView.setText(app.getName());
            descriptionView.setText(app.getDescription());
            addAppBtn.setOnClickListener(new OnClickListener () {
                public void onClick(View v) {
                    AppDetailFragment.this.mListener.onAddAppClick(app);
                }
            });
        }
        return view;
    }

    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }
}
