package com.example.bhumicloud.gisapplications.model.field.temporal;

import android.os.Parcel;

import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.textual.TextualElement;

import java.util.Map;

public abstract class TemporalElement extends TextualElement {
    public TemporalElement(Element parent, Map json) {
        super(parent, json);
    }

    protected TemporalElement(Parcel parcel) {
        super(parcel);
    }
}
