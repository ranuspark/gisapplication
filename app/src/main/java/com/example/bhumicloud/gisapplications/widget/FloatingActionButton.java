package com.example.bhumicloud.gisapplications.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

public class FloatingActionButton extends android.support.design.widget.FloatingActionButton implements OnScrollListener {
    private int mLastScrollY;
    private AbsListView mListView;
    private int mPreviousFirstVisibleItem;

    public FloatingActionButton(Context context) {
        super(context);
    }

    public FloatingActionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (totalItemCount != 0) {
            if (isSameRow(firstVisibleItem)) {
                int newScrollY = getTopItemScrollY();
                if (Math.abs(this.mLastScrollY - newScrollY) > 0) {
                    if (this.mLastScrollY > newScrollY) {
                        hide();
                    } else {
                        show();
                    }
                }
                this.mLastScrollY = newScrollY;
                return;
            }
            if (firstVisibleItem > this.mPreviousFirstVisibleItem) {
                hide();
            } else {
                show();
            }
            this.mLastScrollY = getTopItemScrollY();
            this.mPreviousFirstVisibleItem = firstVisibleItem;
        }
    }

    public void attachToListView(@NonNull AbsListView listView) {
        this.mListView = listView;
        listView.setOnScrollListener(this);
    }

    private boolean isSameRow(int firstVisibleItem) {
        return firstVisibleItem == this.mPreviousFirstVisibleItem;
    }

    private int getTopItemScrollY() {
        if (this.mListView == null || this.mListView.getChildAt(0) == null) {
            return 0;
        }
        return this.mListView.getChildAt(0).getTop();
    }
}
