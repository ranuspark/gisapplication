package com.example.bhumicloud.gisapplications.util;

import android.os.Parcel;
import java.util.Date;

public class ParcelUtils {
    public static void writeDate(Parcel parcel, Date date) {
        if (date != null) {
            parcel.writeInt(1);
            parcel.writeLong(date.getTime());
            return;
        }
        parcel.writeInt(0);
        parcel.writeLong(0);
    }

    public static Date readDate(Parcel parcel) {
        int exists = parcel.readInt();
        long epoch = parcel.readLong();
        if (exists == 1) {
            return new Date(epoch);
        }
        return null;
    }

    public static Boolean readBoolean(Parcel parcel) {
        return (Boolean) parcel.readValue(null);
    }

    public static Integer readInteger(Parcel parcel) {
        return (Integer) parcel.readValue(null);
    }
}

