package com.example.bhumicloud.gisapplications;

public class FormSaveDetails {


    int datid,user_id,projectid,formid,fieldid,pid,saveid,groupid,sid,eleId,Order_ID,Drop_value_Order,show_in_search,show_parent,condition_logic,imgid,p_id,field_action,flag_mode,Save_formid;
    String value,flag,image,latlong,imagep,Element_Name,Drop_value_Name,input_type,Value,curr_loc,datefield,name,description,uuserid,dummy_img;

    int status;

    // Empty constructor
    public FormSaveDetails(){

    }

    // constructor
    public FormSaveDetails(int dataid, int User_id, int Project_Id,int Form_Id, int fieldId, String Value, String Flag, String imageName, String latLong, String imagepath,int s_id,int p_id,int save_id,int group_id){
        this.datid = dataid;
        this.user_id = User_id;
        this.projectid = Project_Id;
        this.formid = Form_Id;
        this.fieldid = fieldId;
        this.value = Value;
        this.flag = Flag;
        this.image = imageName;
        this.latlong = latLong;
        this.imagep = imagepath;
        this.sid = s_id;
        this.pid = p_id;
        this.saveid = save_id;
        this.groupid = group_id;

    }

    // constructor
    public FormSaveDetails(int User_id,int p_id,String Value,int Project_Id,int Form_Id,int save_id,int group_id,int Save_formid,String Flag){

        this.user_id = User_id;
        this.pid = p_id;
        this.value = Value;
        this.projectid = Project_Id;
        this.formid = Form_Id;
        this.saveid = save_id;
        this.groupid = group_id;
        this.Save_formid = Save_formid;
        this.flag = Flag;

    }

    // constructor
    public FormSaveDetails(int User_id, int Project_Id, int Form_Id, int fieldId,String Value,String Flag, String imageName, String imagepath, int Save_formid, String latLong){
        this.user_id = User_id;
        this.projectid = Project_Id;
        this.formid = Form_Id;
        this.fieldid = fieldId;
        this.value = Value;
        this.flag = Flag;
        this.image = imageName;
        this.imagep = imagepath;
        this.Save_formid = Save_formid;
        this.latlong = latLong;



    }



    //////////////// formdummydata /////////////////



    public FormSaveDetails(int fieldId,int Project_Id, int Form_Id, int eleId,int Order_ID,String Element_Name, int Group_id, int Drop_value_Order, String Drop_value_Name, int show_in_search, int show_parent, int condition_logic){
        this.fieldid = fieldId;
        this.projectid = Project_Id;
        this.formid = Form_Id;
        this.eleId = eleId;
        this.Order_ID = Order_ID;
        this.Element_Name = Element_Name;
        this.groupid = Group_id;
        this.Drop_value_Order = Drop_value_Order;
        this.Drop_value_Name = Drop_value_Name;
        this.show_in_search = show_in_search;
        this.show_parent = show_parent;
        this.condition_logic = condition_logic;


    }

    //////////////// formdummydata /////////////////




    //////////////// formELEMENT /////////////////



    public FormSaveDetails(int eleId,String input_type){
        this.eleId = eleId;
        this.input_type = input_type;



    }

    //////////////// formELEMENT /////////////////





    //////////////// formimages /////////////////



    public FormSaveDetails(int Project_Id, int Form_Id, int User_id,String Value,String dummy_img,String curr_loc){
        this.projectid = Project_Id;
        this.formid = Form_Id;
        this.user_id = User_id;
        this.Value = Value;
        this.dummy_img = dummy_img;
        this.curr_loc = curr_loc;


    }

    //////////////// formimages /////////////////






    //////////////// formParentdata /////////////////



    public FormSaveDetails(int p_id,int Project_Id, int eleId,int Order_ID,String Element_Name, int Group_id, int Drop_value_Order, String Drop_value_Name,int condition_logic){
        this.p_id = p_id;
        this.projectid = Project_Id;
        this.eleId = eleId;
        this.Order_ID = Order_ID;
        this.Element_Name = Element_Name;
        this.groupid = Group_id;
        this.Drop_value_Order = Drop_value_Order;
        this.Drop_value_Name = Drop_value_Name;
        this.condition_logic = condition_logic;


    }

    //////////////// formParentdata /////////////////




    //////////////// formParentSavedata /////////////////



    public FormSaveDetails(int User_id, int p_id,String Value,int Project_Id, int Form_Id, int save_id, int group_id){
        this.user_id = User_id;
        this.p_id = p_id;
        this.Value = Value;
        this.projectid = Project_Id;
        this.formid = Form_Id;
        this.saveid = save_id;
        this.groupid = group_id;

    }

    //////////////// formParentSavedata /////////////////




    //////////////// formParentConditional /////////////////



    public FormSaveDetails(int project_id, int Group_id,int fieldid,String values1, int field_action, int flag){
        this.projectid = project_id;
        this.groupid = Group_id;
        this.fieldid = fieldid;
        this.Value = values1;
        this.field_action = field_action;
        this.flag_mode = flag;

    }

    //////////////// formParentConditional /////////////////



    //////////////// formConditional /////////////////



    public FormSaveDetails(int project_id,int fieldid,String values1, int field_action, int flag){
        this.projectid = project_id;
        this.fieldid = fieldid;
        this.Value = values1;
        this.field_action = field_action;
        this.flag_mode = flag;

    }

    //////////////// formConditional /////////////////



    //////////////// formdummySavedata /////////////////



    public FormSaveDetails(int User_id, int fieldId,String Value,int Project_Id, int Form_Id,String date){
        this.user_id = User_id;
        this.fieldid = fieldId;
        this.Value = Value;
        this.projectid = Project_Id;
        this.formid = Form_Id;
        this.datefield = date;




    }

    //////////////// formdummySavedata /////////////////






    //////////////// formproject /////////////////



    public FormSaveDetails(int id, String userid,String name,String description, String date1,int status){
        this.p_id = id;
        this.uuserid = userid;
        this.name = name;
        this.description = description;
        this.datefield = date1;
        this.status = status;




    }

    //////////////// formproject /////////////////


    public String getdummyimg(){
        return this.dummy_img;
    }
    public void setdummyimg(String dummy_img){
        this.dummy_img = dummy_img;
    }



    public int getProjectstatus(){
        return this.status;
    }
    public void setProjectstatus(int status){
        this.status = status;
    }

    public String getProjectdate(){
        return this.datefield;
    }
    public void setProjectdate(String date1){
        this.datefield = date1;
    }

    public String getProjectdesc(){
        return this.description;
    }
    public void setProjectdesc(String description){
        this.description = description;
    }


    public String getProjectname(){
        return this.name;
    }
    public void setProjectname(String name){
        this.name = name;
    }

    public String getProjectuserid(){
        return this.uuserid;
    }
    public void setProjectuserid(String userid){
        this.uuserid = userid;
    }

    public int getuserprojectId(){
        return this.p_id;
    }
    public void setuserprojectId(int id){
        this.p_id = id;
    }




    public int getsavformid(){
        return this.Save_formid;
    }
    public void setsavformid(int Save_formid){
        this.Save_formid = Save_formid;
    }



    public String getdate(){
        return this.datefield;
    }
    public void setdate(String date){
        this.datefield = date;
    }





    public int getflagmode(){
        return this.flag_mode;
    }
    public void setflagmode(int flag){
        this.flag_mode = flag;
    }


    public int getfieldaction(){
        return this.field_action;
    }
    public void setfieldaction(int field_action){
        this.field_action = field_action;
    }


    public String getParentconvalues(){
        return this.Value;
    }
    public void setParentconvalues(String values1){
        this.Value = values1;
    }



    public int getParentfieldid(){
        return this.fieldid;
    }
    public void setParentfieldid(int fieldid){
        this.fieldid = fieldid;
    }


    public int getParentprojectid(){
        return this.projectid;
    }
    public void setParentprojectid(int project_id){
        this.projectid = project_id;
    }




    public int getParentCondid(){
        return this.p_id;
    }
    public void setParentCondid(int id){
        this.p_id = id;
    }







    public int getParentid(){
        return this.p_id;
    }
    public void setParentid(int p_id){
        this.p_id = p_id;
    }





    public String getcurrloc(){
        return this.curr_loc;
    }
    public void setcurrloc(String curr_loc){
        this.curr_loc = curr_loc;
    }





    public String getFormimgval(){
        return this.Value;
    }
    public void setFormimgval(String Value){
        this.Value = Value;
    }




    public int getimgid(){
        return this.imgid;
    }
    public void setimgid(int imgid){
        this.imgid = imgid;
    }





    public String getFormElement(){
        return this.input_type;
    }
    public void setFormElement(String input_type){
        this.input_type = input_type;
    }






    public int getconlogic(){
        return this.condition_logic;
    }
    public void setconlogic(int condition_logic){
        this.condition_logic = condition_logic;
    }





    public int getshowparent(){
        return this.show_parent;
    }
    public void setshowparent(int show_parent){
        this.show_parent = show_parent;
    }




    public int getshowinsearch(){
        return this.show_in_search;
    }
    public void setshowinsearch(int show_in_search){
        this.show_in_search = show_in_search;
    }


    public String getDropname(){
        return this.Drop_value_Name;
    }
    public void setDropname(String Drop_value_Name){
        this.Drop_value_Name = Drop_value_Name;
    }


    public int getDroporder(){
        return this.Drop_value_Order;
    }
    public void setDroporder(int Drop_value_Order){
        this.Drop_value_Order = Drop_value_Order;
    }


    public int getgrpid(){
        return this.groupid;
    }
    public void setgrpid(int Group_id){
        this.groupid = Group_id;
    }


    public String getelename(){
        return this.Element_Name;
    }
    public void setelename(String Element_Name){
        this.Element_Name = Element_Name;
    }


    public int getorderid(){
        return this.Order_ID;
    }
    public void setorderid(int Order_ID){
        this.Order_ID = Order_ID;
    }


    public int geteleId(){
        return this.eleId;
    }
    public void seteleId(int eleId){
        this.eleId = eleId;
    }




    public int getID(){
        return this.datid;
    }
    public void setID(int dataid){
        this.datid = dataid;
    }


    public int getuserID(){
        return this.user_id;
    }
    public void setuserID(int User_id){
        this.user_id = User_id;

    }


    public int getProjectId(){
        return this.projectid;
    }
    public void setprojectID(int Project_Id) {
        this.projectid = Project_Id;
    }


    public int getFormId(){
        return this.formid;
    }
    public void setFormID(int Form_Id) {
        this.formid = Form_Id;
    }


    public int getfiledID(){
        return this.fieldid;
    }
    public void setfieldID(int fieldId){
        this.fieldid = fieldId;
    }


    public String getValue(){
        return this.value;
    }
    public void setValue(String Value){
        this.value = Value;
    }


    public String getFlag(){
        return this.flag;
    }
    public void setFlag(String Flag){
        this.flag = Flag;
    }


    public String getImageName(){
        return this.image;
    }
    public void setImageName(String imageName){
        this.image = imageName;
    }


    public String getLatLong(){
        return this.latlong;
    }
    public void setLatLong(String latLong){
        this.latlong = latLong;
    }


    public String getImagepath(){
        return this.imagep;
    }
    public void setImagepath(String Imagepath){
        this.imagep = Imagepath;
    }


    public int getSID(){
        return this.pid;
    }
    public void setSID(int s_id ){
        this.pid = s_id;
    }



    public int getPID(){
        return this.pid;
    }
    public void setPID(int p_id ){
        this.pid = p_id;
    }


    public int getSaveID(){
        return this.saveid;
    }
    public void setSaveID(int save_id){
        this.saveid = save_id;

    }

    public int getGroupID(){
        return this.groupid;
    }
    public void setGroupID(int group_id){
        this.groupid = group_id;

    }




}



