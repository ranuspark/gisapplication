package com.example.bhumicloud.gisapplications.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.google.android.gms.location.LocationRequest;
import com.example.bhumicloud.gisapplications.R;

public class LocationDebuggerActivity extends LocationListenerActivity {
    public static void start(Context context) {
        context.startActivity(new Intent (context, LocationDebuggerActivity.class));
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_basic);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        setTitle(R.string.location_debug_title);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new LocationDebuggerFragment()).commit();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if ((requestCode & 64) != 64) {
            return;
        }
        if (grantResults.length <= 0 || grantResults[0] != 0) {
            finish();
            Toast.makeText(this, R.string.permissions_location, Toast.LENGTH_SHORT).show();
            return;
        }
        startLocationUpdates();
    }

    protected LocationRequest getLocationRequest() {
        return null;
    }
}
