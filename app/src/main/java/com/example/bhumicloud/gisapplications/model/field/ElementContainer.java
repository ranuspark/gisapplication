package com.example.bhumicloud.gisapplications.model.field;

import android.os.Parcelable;

import java.util.ArrayList;

public interface ElementContainer extends Parcelable {
    Element getElement(String str);

    ArrayList<Element> getElements();
}
