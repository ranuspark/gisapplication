package com.example.bhumicloud.gisapplications.model.field.temporal.time;

import android.os.Parcel;
import android.os.Parcelable.Creator;

import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.temporal.TemporalElement;

import java.util.Map;

public class TimeElement extends TemporalElement {
    public static final Creator<TimeElement> CREATOR = new C12011();

    static class C12011 implements Creator<TimeElement> {
        C12011() {
        }

        public TimeElement createFromParcel(Parcel source) {
            return new TimeElement(source);
        }

        public TimeElement[] newArray(int size) {
            return new TimeElement[size];
        }
    }

    public TimeElement(Element parent, Map json) {
        super(parent, json);
    }

    private TimeElement(Parcel parcel) {
        super(parcel);
    }

    public String getType() {
        return Element.TYPE_TIME;
    }
}
