package com.example.bhumicloud.gisapplications.util;

import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import java.util.HashMap;
import java.util.Map;

public class GoogleMapUtils {
    private static final String JSON_CAMERA_BEARING = "bearing";
    private static final String JSON_CAMERA_LATITUDE = "latitude";
    private static final String JSON_CAMERA_LONGITUDE = "longitude";
    private static final String JSON_CAMERA_TILT = "tilt";
    private static final String JSON_CAMERA_ZOOM = "zoom";

    public static HashMap<String, Object> toJSON(CameraPosition camera) {
        HashMap<String, Object> json = new HashMap();
        if (!Float.valueOf(camera.zoom).isNaN()) {
            json.put(JSON_CAMERA_ZOOM, Float.valueOf(camera.zoom));
        }
        json.put(JSON_CAMERA_TILT, Float.valueOf(camera.tilt));
        json.put ( JSON_CAMERA_BEARING, Float.valueOf ( camera.bearing ) );
        json.put ( JSON_CAMERA_LATITUDE, Double.valueOf ( camera.target.latitude ) );
        json.put ( JSON_CAMERA_LONGITUDE, Double.valueOf ( camera.target.longitude ) );
        return json;
    }

    public static CameraPosition parseCameraPosition(Map json) {
        return new CameraPosition ( new LatLng ( JSONUtils.getDouble ( json, JSON_CAMERA_LATITUDE ), JSONUtils.getDouble ( json, JSON_CAMERA_LONGITUDE ) ), (float) JSONUtils.getDouble ( json, JSON_CAMERA_ZOOM ), (float) JSONUtils.getDouble ( json, JSON_CAMERA_TILT ), (float) JSONUtils.getDouble ( json, JSON_CAMERA_BEARING ) );
    }
}
