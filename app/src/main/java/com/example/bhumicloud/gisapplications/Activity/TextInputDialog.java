package com.example.bhumicloud.gisapplications.Activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.bhumicloud.gisapplications.R;

public class TextInputDialog extends ThemedDialogFragment {
    private static final String STATE_PLACEHOLDER = "STATE_PLACEHOLDER";
    private static final String STATE_TEXT_VALUE = "STATE_TEXT_VALUE";
    private TextInputDialogListener mListener;
    private String mPlaceholder;
    private EditText mTextField;
    private String mTextValue;

    public interface TextInputDialogListener {
        void onTextValueEntered(String str);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNegativeButtonText((int) R.string.clear);
        setPositiveButtonText((int) R.string.done);
        if (savedInstanceState != null) {
            this.mTextValue = savedInstanceState.getString(STATE_TEXT_VALUE);
            this.mPlaceholder = savedInstanceState.getString(STATE_PLACEHOLDER);
        }
    }

    public View onCreateContentView(LayoutInflater inflater, ViewGroup root, Bundle savedState) {
        View view = inflater.inflate(R.layout.dialog_text_input, root, false);
        this.mTextField = (EditText) view.findViewById(R.id.text_field);
        this.mTextField.setHint(this.mPlaceholder);
        if (savedState == null) {
            this.mTextField.setText(this.mTextValue);
        }
        return view;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_PLACEHOLDER, this.mPlaceholder);
        outState.putString(STATE_TEXT_VALUE, this.mTextValue);
    }

    protected void onNegativeButtonClicked() {
        super.onNegativeButtonClicked();
        if (this.mTextField != null) {
            this.mTextField.setText(null);
        }
    }

    protected void onPositiveButtonClicked() {
        super.onPositiveButtonClicked();
        if (this.mTextField != null) {
            onTextValueEntered(this.mTextField.getText().toString());
        }
        dismissAllowingStateLoss();
    }

    public void onDestroy() {
        this.mListener = null;
        super.onDestroy();
    }

    public void setTextValue(String textValue) {
        this.mTextValue = textValue;
        if (this.mTextField != null) {
            this.mTextField.setText(textValue);
        }
    }

    public void setTextInputDialogListener(TextInputDialogListener listener) {
        this.mListener = listener;
    }

    protected void onTextValueEntered(String value) {
        notifyOnTextValueEntered(value);
    }

    private void notifyOnTextValueEntered(String value) {
        if (this.mListener != null) {
            this.mListener.onTextValueEntered(value);
        }
    }
}
