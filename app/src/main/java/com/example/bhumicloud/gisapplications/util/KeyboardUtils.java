package com.example.bhumicloud.gisapplications.util;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class KeyboardUtils {
    public static void hideSoftKeyboard(Activity activity) {
        View focus = activity.getCurrentFocus();
        if (focus != null) {
            InputMethodManager svc = (InputMethodManager) activity.getSystemService(activity.INPUT_METHOD_SERVICE);
            if (svc instanceof InputMethodManager) {
                svc.hideSoftInputFromWindow(focus.getWindowToken(), 0);
            }
        }
    }

    public static void hideSoftKeyboard(Context context, View view) {
        if (view != null) {
            InputMethodManager svc = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
            if (svc instanceof InputMethodManager) {
                svc.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public static void hideDialogSoftKeyboard(Activity activity, View view) {
        if (view != null) {
            InputMethodManager svc = (InputMethodManager) activity.getSystemService(activity.INPUT_METHOD_SERVICE);
            if (svc instanceof InputMethodManager) {
                svc.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }
}
