package com.example.bhumicloud.gisapplications.location;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Address implements Parcelable {
    public static final Creator<Address> CREATOR = new C11621();
    private static final String JSON_ADMIN_AREA = "admin_area";
    private static final String JSON_COUNTRY = "country";
    private static final String JSON_LOCALITY = "locality";
    private static final String JSON_POSTAL_CODE = "postal_code";
    private static final String JSON_SUB_ADMIN_AREA = "sub_admin_area";
    private static final String JSON_SUB_THOROUGHFARE = "sub_thoroughfare";
    private static final String JSON_SUITE = "suite";
    private static final String JSON_THOROUGHFARE = "thoroughfare";
    private final String mCity;
    private final String mCountry;
    private final String mCounty;
    private final Locale mLocale;
    private final String mNumber;
    private final String mPostal;
    private final String mState;
    private final String mStreet;
    private final String mSuite;

    static class C11621 implements Creator<Address> {
        C11621() {
        }

        public Address createFromParcel(Parcel source) {
            return new Address(source);
        }

        public Address[] newArray(int size) {
            return new Address[size];
        }
    }

    public Address(android.location.Address address) {
        this.mLocale = address.getLocale();
        this.mNumber = address.getSubThoroughfare();
        this.mStreet = address.getThoroughfare();
        this.mSuite = null;
        this.mCity = address.getLocality();
        this.mCounty = address.getSubAdminArea();
        this.mState = address.getAdminArea();
        this.mPostal = address.getPostalCode();
        this.mCountry = address.getCountryName();
    }

    public Address(Map json) {
        this.mLocale = Locale.getDefault();
        this.mNumber = JSONUtils.getString(json, JSON_SUB_THOROUGHFARE);
        this.mStreet = JSONUtils.getString(json, JSON_THOROUGHFARE);
        this.mSuite = JSONUtils.getString(json, JSON_SUITE);
        this.mCity = JSONUtils.getString(json, JSON_LOCALITY);
        this.mCounty = JSONUtils.getString(json, JSON_SUB_ADMIN_AREA);
        this.mState = JSONUtils.getString(json, JSON_ADMIN_AREA);
        this.mPostal = JSONUtils.getString(json, JSON_POSTAL_CODE);
        this.mCountry = JSONUtils.getString(json, JSON_COUNTRY);
    }

    public Address(Locale locale, String number, String street, String suite, String city, String county, String state, String postalCode, String country) {
        this.mLocale = locale;
        this.mNumber = number;
        this.mStreet = street;
        this.mSuite = suite;
        this.mCity = city;
        this.mCounty = county;
        this.mState = state;
        this.mPostal = postalCode;
        this.mCountry = country;
    }

    private Address(Parcel source) {
        String language = source.readString();
        String country = source.readString();
        this.mLocale = country.length() > 0 ? new Locale(language, country) : new Locale(language);
        this.mNumber = source.readString();
        this.mStreet = source.readString();
        this.mSuite = source.readString();
        this.mCity = source.readString();
        this.mCounty = source.readString();
        this.mState = source.readString();
        this.mPostal = source.readString();
        this.mCountry = source.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getLocale().getLanguage());
        dest.writeString(getLocale().getCountry());
        dest.writeString(getStreetNumber());
        dest.writeString(getStreetName());
        dest.writeString(getSuite());
        dest.writeString(getCity());
        dest.writeString(getCounty());
        dest.writeString(getState());
        dest.writeString(getPostalCode());
        dest.writeString(getCountryName());
    }

    public Locale getLocale() {
        return this.mLocale;
    }

    public String getStreetNumber() {
        return this.mNumber;
    }

    public String getStreetName() {
        return this.mStreet;
    }

    public String getSuite() {
        return this.mSuite;
    }

    public String getCity() {
        return this.mCity;
    }

    public String getCounty() {
        return this.mCounty;
    }

    public String getState() {
        return this.mState;
    }

    public String getPostalCode() {
        return this.mPostal;
    }

    public String getCountryName() {
        return this.mCountry;
    }

    public String getAddressLine1() {
        ArrayList<String> parts = new ArrayList(2);
        if (!TextUtils.isEmpty(this.mNumber)) {
            parts.add(this.mNumber);
        }
        if (!TextUtils.isEmpty(this.mStreet)) {
            parts.add(this.mStreet);
        }
        return TextUtils.join(" ", parts);
    }

    public String getAddressLine2() {
        ArrayList<String> parts = new ArrayList(3);
        if (!TextUtils.isEmpty(this.mCity)) {
            parts.add(this.mCity);
        }
        if (!TextUtils.isEmpty(this.mState)) {
            parts.add(this.mState);
        }
        if (!TextUtils.isEmpty(this.mPostal)) {
            parts.add(this.mPostal);
        }
        return TextUtils.join((CharSequence) MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, parts);
    }

    public String getAddressLine3() {
        return this.mCountry;
    }

    public boolean isEmpty() {
        if (TextUtils.isEmpty(getAddressLine1()) && TextUtils.isEmpty(getAddressLine2()) && TextUtils.isEmpty(getAddressLine3())) {
            return true;
        }
        return false;
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = new HashMap();
        json.put(JSON_SUB_THOROUGHFARE, getStreetNumber());
        json.put(JSON_THOROUGHFARE, getStreetName());
        json.put(JSON_SUITE, getSuite());
        json.put(JSON_LOCALITY, getCity());
        json.put(JSON_SUB_ADMIN_AREA, getCounty());
        json.put(JSON_ADMIN_AREA, getState());
        json.put(JSON_POSTAL_CODE, getPostalCode());
        json.put(JSON_COUNTRY, getCountryName());
        return json;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Address)) {
            return false;
        }
        Address anAddress = (Address) object;
        if (!anAddress.canEqual(this)) {
            return false;
        }
        boolean isLocaleEqual = getLocale() == null ? anAddress.getLocale() == null : getLocale().equals(anAddress.getLocale());
        if (isLocaleEqual && TextUtils.equals(getStreetNumber(), anAddress.getStreetNumber()) && TextUtils.equals(getStreetName(), anAddress.getStreetName()) && TextUtils.equals(getSuite(), anAddress.getSuite()) && TextUtils.equals(getCity(), anAddress.getCity()) && TextUtils.equals(getCounty(), anAddress.getCounty()) && TextUtils.equals(getState(), anAddress.getState()) && TextUtils.equals(getPostalCode(), anAddress.getPostalCode()) && TextUtils.equals(getCountryName(), anAddress.getCountryName())) {
            return true;
        }
        return false;
    }

    public boolean canEqual(Object other) {
        return other instanceof Address;
    }

    public int hashCode() {
        int result;
        int hashCode;
        int i = 0;
        if (this.mLocale != null) {
            result = this.mLocale.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.mNumber != null) {
            hashCode = this.mNumber.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mStreet != null) {
            hashCode = this.mStreet.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mSuite != null) {
            hashCode = this.mSuite.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mCity != null) {
            hashCode = this.mCity.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mCounty != null) {
            hashCode = this.mCounty.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mState != null) {
            hashCode = this.mState.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.mPostal != null) {
            hashCode = this.mPostal.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (i2 + hashCode) * 31;
        if (this.mCountry != null) {
            i = this.mCountry.hashCode();
        }
        return hashCode + i;
    }
}

