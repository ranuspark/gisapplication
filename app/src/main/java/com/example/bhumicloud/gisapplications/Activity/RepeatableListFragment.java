package com.example.bhumicloud.gisapplications.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.Activity.ContextualBottomSheetRepeatableMenu.ContextualBottomSheetRepeatableMenuListener;
import com.example.bhumicloud.gisapplications.RepeatableItemCollectionViewListener;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableItemValue;
import com.example.bhumicloud.gisapplications.util.DateUtils;
import com.example.bhumicloud.gisapplications.util.IntentUtils;
import com.example.bhumicloud.gisapplications.widget.DynamicListView;
import com.example.bhumicloud.gisapplications.widget.FloatingActionButton;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RepeatableListFragment extends GISListFragment implements RepeatableItemCollectionView, ContextualBottomSheetRepeatableMenuListener {
    private static final String TAG_CONTEXTUAL_MENU_DIALOG = "TAG_CONTEXTUAL_MENU_DIALOG";
    private FloatingActionButton mFloatingActionButton;
    private RepeatableItemCollectionViewListener mListener;
    private boolean mViewOnlyMode;

    class C11181 implements OnClickListener {
        C11181() {
        }

        public void onClick(View v) {
            ((RepeatableIndexFragment) RepeatableListFragment.this.getParentFragment()).onAddNewItemOptionSelected();
        }
    }

    class C11192 implements OnItemLongClickListener {
        C11192() {
        }

        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
            Bundle args = new Bundle ();
            CharSequence titleSeq = ((TextView) view.findViewById(R.id.item_title)).getText();
            args.putString(ContextualBottomSheetMenu.ARG_TITLE, titleSeq == null ? null : titleSeq.toString());
            ContextualBottomSheetRepeatableMenu menuDialog = new ContextualBottomSheetRepeatableMenu();
            menuDialog.setArguments(args);
            menuDialog.setRepeatableItemValue((RepeatableItemValue) RepeatableListFragment.this.getAdapter().getItem(position));
            menuDialog.setListener(RepeatableListFragment.this);
            menuDialog.show(RepeatableListFragment.this.getChildFragmentManager(), RepeatableListFragment.TAG_CONTEXTUAL_MENU_DIALOG);
            return true;
        }
    }

    private static class RepeatableItemAdapter extends ArrayAdapter<RepeatableItemValue> {
        private boolean mEditMode;
        private LayoutInflater mInflater;
        private final List<RepeatableItemValue> mItems;

        public RepeatableItemAdapter(Context context, List<RepeatableItemValue> items) {
            super(context, R.layout.list_item_repeatable, items);
            this.mItems = items;
            Object service = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (service != null) {
                this.mInflater = (LayoutInflater) service;
            }
        }

        @NonNull
        public View getView(int position, View reusable, @NonNull ViewGroup parent) {
            Object identifier;
            Date updatedAt;
            View view = reusable;
            if (view == null) {
                view = this.mInflater.inflate(R.layout.list_item_repeatable, parent, false);
            }
            RepeatableItemValue item = (RepeatableItemValue) getItem(position);
            if (item != null) {
                identifier = item.getIdentifier();
            } else {
                identifier = null;
            }
            view.setTag(identifier);
            TextView timestampLabel = (TextView) view.findViewById(R.id.item_timestamp);
            ((TextView) view.findViewById(R.id.item_title)).setText(item != null ? item.getDisplayValue() : "");
            if (item != null) {
                updatedAt = item.getUpdatedAt();
            } else {
                updatedAt = null;
            }
            if (updatedAt != null) {
                timestampLabel.setText(DateUtils.getRelativeTimeSpanString(getContext(), updatedAt));
            } else {
                timestampLabel.setText(DateUtils.getRelativeTimeSpanString(getContext()));
            }
            ImageView repeatableEditView = (ImageView) view.findViewById(R.id.item_chevron_edit);
            ImageView reorderView = (ImageView) view.findViewById(R.id.item_recorder_icon);
            if (this.mEditMode) {
                repeatableEditView.setVisibility(View.GONE);
                reorderView.setVisibility(View.VISIBLE);
            } else {
                repeatableEditView.setVisibility(View.VISIBLE);
                reorderView.setVisibility(View.GONE);
            }
            return view;
        }

        public long getItemId(int position) {
            if (position < 0 || position >= getCount()) {
                return -1;
            }
            RepeatableItemValue riv = (RepeatableItemValue) getItem(position);
            if (riv != null) {
                return (long) riv.getIndex();
            }
            return -1;
        }

        public void setEditMode(boolean editMode) {
            this.mEditMode = editMode;
        }

        public List<RepeatableItemValue> getItems() {
            return this.mItems;
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        Fragment fragment = getParentFragment();
        if (fragment instanceof RepeatableItemCollectionViewListener) {
            this.mListener = (RepeatableItemCollectionViewListener) fragment;
            return;
        }
        throw new RuntimeException (fragment + " must implement " + RepeatableItemCollectionViewListener.class.getSimpleName());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repeatable_list, container, false);
        this.mFloatingActionButton = (FloatingActionButton) view.findViewById(R.id.floating_action_button);
        if (this.mViewOnlyMode) {
            this.mFloatingActionButton.setVisibility(View.GONE);
        } else {
            this.mFloatingActionButton.setOnClickListener(new C11181());
        }
        return view;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DynamicListView dynamicListView = getDynamicListView();
        setListAdapter(new RepeatableItemAdapter(getActivity(), new ArrayList ()));
        dynamicListView.setDragViewResId(R.id.item_recorder_icon);
        if (this.mFloatingActionButton.getVisibility() == View.VISIBLE) {
            this.mFloatingActionButton.attachToListView(dynamicListView);
        }
        dynamicListView.setOnItemLongClickListener(new C11192());
        ContextualBottomSheetRepeatableMenu contextMenuDialog = (ContextualBottomSheetRepeatableMenu) getChildFragmentManager().findFragmentByTag(TAG_CONTEXTUAL_MENU_DIALOG);
        if (contextMenuDialog != null) {
            contextMenuDialog.setListener(this);
        }
    }

    public void onListItemClick(ListView listView, View view, int position, long itemID) {
        this.mListener.onSelected((RepeatableItemValue) getAdapter().getItem(position));
    }

    public void onDuplicate(RepeatableItemValue sourceItem, boolean location, boolean values) {
        this.mListener.onAdded(new RepeatableItemValue(sourceItem, location, values, getAdapter().getCount()));
    }

    public void onShowLocation(RepeatableItemValue value) {
        this.mListener.onShowOnMap(value);
    }

    public void onDrivingDirections(RepeatableItemValue value) {
        startActivity(IntentUtils.getDrivingDirections(getActivity(), value.getLatLng()));
    }

    public void onDelete(RepeatableItemValue value) {
        this.mListener.onDeleted(value);
    }

    public List<RepeatableItemValue> getItems() {
        return getAdapter().getItems();
    }

    public void setRepeatableItems(List<RepeatableItemValue> items) {
        if (isResumed()) {
            RepeatableItemAdapter adapter = getAdapter();
            adapter.clear();
            adapter.addAll(items);
        }
    }

    public void setViewOnlyMode(boolean viewOnlyMode) {
        this.mViewOnlyMode = viewOnlyMode;
    }

    public void setEditMode(boolean editMode) {
        RepeatableItemAdapter adapter = (RepeatableItemAdapter) getListAdapter();
        adapter.setEditMode(editMode);
        getDynamicListView().setDragAndDropEnabled(editMode);
        if (editMode) {
            adapter.notifyDataSetChanged();
            this.mFloatingActionButton.setVisibility(View.GONE);
            return;
        }
        if (this.mFloatingActionButton != null) {
            this.mFloatingActionButton.setVisibility(View.VISIBLE);
        }
        adapter.notifyDataSetChanged();
    }

    private DynamicListView getDynamicListView() {
        return (DynamicListView) getListView();
    }

    private RepeatableItemAdapter getAdapter() {
        return (RepeatableItemAdapter) getListAdapter();
    }
}
