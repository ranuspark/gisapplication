package com.example.bhumicloud.gisapplications.dossier;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Form;


public class FormLoader extends CursorLoader {
    private String[] mQueryColumns;
    private String mSearchText;

    public FormLoader(Context context, String... columns) {
        super(context);
        this.mQueryColumns = columns;
    }

    public Cursor loadInBackground() {
        return Form.getFormsCursor(Account.getActiveAccount(), GIS.getDatabase(), this.mQueryColumns, this.mSearchText);
    }

    public FormLoader setSearchFilter(String searchText) {
        this.mSearchText = searchText;
        return this;
    }
}
