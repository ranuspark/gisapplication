package com.example.bhumicloud.gisapplications.Activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.Account;

public class ContextualBottomSheetRecordMenu extends ContextualBottomSheetMenu {
    public static final String ARG_ID = "GIS:arg:contextual_menu_id";
    public static final String ARG_POSITION = "GIS:arg:contextual_menu_position";
    private int mAdapterPosition;
    private long mItemId;
    private ContextualBottomSheetRecordMenuListener mListener;
    private String mTitle;

    class C10551 implements OnClickListener {
        C10551() {
        }

        public void onClick(View v) {
            ContextualBottomSheetRecordMenu.this.mListener.onGenerate(ContextualBottomSheetRecordMenu.this.mItemId);
            ContextualBottomSheetRecordMenu.this.dismiss();
        }
    }

    public interface ContextualBottomSheetRecordMenuListener {
        void onDelete(long j);

        void onDrivingDirections(int i);

        void onDuplicate(long j, boolean z, boolean z2);

        void onGenerate(long j);

        void onShowLocation(int i);
    }

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        Bundle arguments = getArguments();
        this.mTitle = arguments.getString(ContextualBottomSheetMenu.ARG_TITLE);
        this.mAdapterPosition = arguments.getInt(ARG_POSITION);
        this.mItemId = arguments.getLong(ARG_ID);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState) {
        View view = super.onCreateView(inflater, container, savedState);
        if (view != null) {
/*
            LinearLayout generateReport = (LinearLayout) view.findViewById(R.id.layout_generate_report);
            if (Account.getActiveAccount(getContext()).getRole().canGenerateReports()) {
                generateReport.setVisibility(View.VISIBLE);
                generateReport.setOnClickListener(new C10551());
            } else {
                generateReport.setVisibility(View.GONE);
            }
*/
        }
        return view;
    }

    public void onDestroy() {
        this.mListener = null;
        super.onDestroy();
    }

    protected String getTitleText() {
        return this.mTitle;
    }

    protected void duplicate(boolean location, boolean values) {
        this.mListener.onDuplicate(this.mItemId, location, values);
    }

    protected void showOnMap() {
        this.mListener.onShowLocation(this.mAdapterPosition);
    }

    protected void drivingDirections() {
        this.mListener.onDrivingDirections(this.mAdapterPosition);
    }

    protected void deleteItem() {
        this.mListener.onDelete(this.mItemId);
    }

    public void setListener(ContextualBottomSheetRecordMenuListener listener) {
        this.mListener = listener;
    }
}
