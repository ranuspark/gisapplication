package com.example.bhumicloud.gisapplications.model.field.classification;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClassificationItem implements Parcelable {
    public static final Creator<ClassificationItem> CREATOR = new C11901();
    private static final String JSON_CHILD_ITEMS = "child_classifications";
    private static final String JSON_LABEL = "label";
    private static final String JSON_VALUE = "value";
    private final List<ClassificationItem> mChildItems;
    private final String mLabel;
    private ClassificationItem mParent;
    private String mSearchLabel;
    private final String mValue;

    static class C11901 implements Creator<ClassificationItem> {
        C11901() {
        }

        public ClassificationItem createFromParcel(Parcel in) {
            return new ClassificationItem(in);
        }

        public ClassificationItem[] newArray(int size) {
            return new ClassificationItem[size];
        }
    }

    public ClassificationItem(ClassificationItem parent, Map json) {
        this.mChildItems = new ArrayList();
        this.mLabel = JSONUtils.getString(json, JSON_LABEL);
        this.mValue = JSONUtils.getString(json, "value");
        List children = JSONUtils.getArrayList(json, JSON_CHILD_ITEMS);
        if (children != null) {
            for (int i = 0; i < children.size(); i++) {
                Map child = JSONUtils.getHashMap(children, i);
                if (child != null) {
                    this.mChildItems.add(new ClassificationItem(this, child));
                }
            }
        }
        this.mParent = parent;
    }

    public ClassificationItem(ClassificationItem parent, String label, String value) {
        this.mChildItems = new ArrayList();
        this.mLabel = label;
        this.mValue = value;
        this.mParent = parent;
    }

    private ClassificationItem(Parcel parcel) {
        this.mChildItems = new ArrayList();
        this.mLabel = parcel.readString();
        this.mValue = parcel.readString();
        parcel.readTypedList(this.mChildItems, CREATOR);
        for (ClassificationItem item : this.mChildItems) {
            item.mParent = this;
        }
        this.mSearchLabel = parcel.readString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mLabel);
        dest.writeString(this.mValue);
        dest.writeTypedList(getChildItems());
        dest.writeString(getSearchLabel());
    }

    public int describeContents() {
        return 0;
    }

    public String getLabel() {
        if (TextUtils.isEmpty(this.mLabel)) {
            return this.mValue;
        }
        return this.mLabel;
    }

    public String getValue() {
        if (TextUtils.isEmpty(this.mValue)) {
            return this.mLabel;
        }
        return this.mValue;
    }

    public ClassificationItem getParent() {
        return this.mParent;
    }

    public List<ClassificationItem> getHierarchicalPath() {
        List<ClassificationItem> path = new ArrayList();
        ClassificationItem pointer = this;
        do {
            path.add(pointer);
            pointer = pointer.getParent();
        } while (pointer != null);
        Collections.reverse(path);
        return path;
    }

    public boolean hasChildItems() {
        return !this.mChildItems.isEmpty();
    }

    public List<ClassificationItem> getChildItems() {
        return this.mChildItems;
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = new HashMap();
        json.put(JSON_LABEL, this.mLabel);
        json.put("value", this.mValue);
        ArrayList<Object> childItems = new ArrayList();
        for (ClassificationItem item : this.mChildItems) {
            childItems.add(item.toJSON());
        }
        json.put(JSON_CHILD_ITEMS, childItems);
        return json;
    }

    public String getSearchLabel() {
        return this.mSearchLabel;
    }

    public void setSearchLabel(ClassificationItem matchingAncestor) {
        matchingAncestor = matchingAncestor.getParent();
        StringBuilder sb = new StringBuilder(getLabel());
        ClassificationItem parent = getParent();
        while (parent != null && parent != matchingAncestor) {
            sb.insert(0, parent.getLabel() + " > ");
            parent = parent.getParent();
        }
        this.mSearchLabel = sb.toString();
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof ClassificationItem)) {
            return false;
        }
        ClassificationItem aClassificationItem = (ClassificationItem) object;
        if (aClassificationItem.canEqual(this)) {
            return toJSON().equals(aClassificationItem.toJSON());
        }
        return false;
    }

    public boolean canEqual(Object other) {
        return other instanceof ClassificationItem;
    }

    public int hashCode() {
        int result;
        int i = 0;
        if (this.mLabel != null) {
            result = this.mLabel.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.mValue != null) {
            i = this.mValue.hashCode();
        }
        return ((i2 + i) * 31) + this.mChildItems.hashCode();
    }

    public String toString() {
        return this.mLabel;
    }
}
