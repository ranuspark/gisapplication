CREATE TABLE IF NOT EXISTS attachments (
    _id INTEGER PRIMARY KEY AUTOINCREMENT,
    remote_id TEXT,
    account_id INTEGER,
    record_id TEXT,
    local_uri TEXT,
    path_one TEXT,
    path_two TEXT,
    type TEXT,
    uploaded INTEGER,
    thumbnail_path TEXT
);
CREATE INDEX IF NOT EXISTS attachments_remote_id_idx ON attachments (remote_id);
CREATE INDEX IF NOT EXISTS attachments_account_id_idx ON attachments (account_id);
CREATE INDEX IF NOT EXISTS attachments_record_id_idx ON attachments (record_id);