package com.example.bhumicloud.gisapplications.Activity;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.ListView;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.Activity.TextInputDialog.TextInputDialogListener;
import com.example.bhumicloud.gisapplications.model.field.classification.ClassificationElement;
import com.example.bhumicloud.gisapplications.model.field.classification.ClassificationItem;
import com.example.bhumicloud.gisapplications.model.field.classification.ClassificationValue;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class ClassificationFieldDialog extends ChoicePickerDialog {
    private static final String STATE_ITEMS = "STATE_ITEMS";
    private static final String STATE_OTHER_ENABLED = "STATE_OTHER_ENABLED";
    private static final String STATE_OTHER_VALUE = "STATE_OTHER_VALUE";
    private static final String STATE_SELECTED_ITEMS = "STATE_SELECTED_ITEMS";
    private static final String STATE_SELECTED_VALUES = "STATE_SELECTED_VALUES";
    private static final String TAG_OTHER_FIELD_DIALOG = "TAG_OTHER_FIELD_DIALOG";
    private List<ClassificationItem> mFilteredItems;
    private List<ClassificationItem> mItems;
    private ClassificationFieldDialogListener mListener;
    private boolean mOtherEnabled;
    private final TextInputDialogListener mOtherFieldDialogListener = new C10491();
    private String mOtherValue;
    private int mRadioButtonResource;
    private final Stack<ClassificationItem> mSelectedItems = new Stack ();
    private List<String> mSelectedValues;
    private Iterator<ClassificationItem> r4;
    private ArrayList values;

    class C10491 implements TextInputDialogListener {
        C10491() {
        }

        public void onTextValueEntered(String value) {
            ClassificationFieldDialog.this.onOtherFieldDialogFinished(value);
        }
    }

    public interface ClassificationFieldDialogListener {
        void onClassificationSelected(List<String> list, String str);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSearchEnabled(true);
        setNeutralButtonText((int) R.string.clear);
        setPositiveButtonText((int) R.string.done);
        if (savedInstanceState != null) {
            this.mOtherValue = savedInstanceState.getString(STATE_OTHER_VALUE);
            this.mOtherEnabled = savedInstanceState.getBoolean(STATE_OTHER_ENABLED);
            this.mItems = savedInstanceState.getParcelableArrayList(STATE_ITEMS);
            this.mSelectedValues = savedInstanceState.getStringArrayList(STATE_SELECTED_VALUES);
            String selectedString = savedInstanceState.getString(STATE_SELECTED_ITEMS);
            try {
                this.mSelectedItems.clear();
                ClassificationItem parent = null;
                List selected = JSONUtils.arrayFromJSON(selectedString);
                if (!(selected == null || selected.isEmpty())) {
                    for (int i = 0; i < selected.size(); i++) {
                        if (!this.mSelectedItems.empty()) {
                            parent = (ClassificationItem) this.mSelectedItems.peek();
                        }
                        Map itemJSON = JSONUtils.getHashMap(selected, i);
                        if (itemJSON != null) {
                            this.mSelectedItems.push(new ClassificationItem(parent, itemJSON));
                        }
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException (e);
            }
        }
        TypedArray ta = getActivity().obtainStyledAttributes(new int[]{16843289});
        this.mRadioButtonResource = ta.getResourceId(0, 17301513);
        ta.recycle();
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextInputDialog dialog = (TextInputDialog) getChildFragmentManager().findFragmentByTag(TAG_OTHER_FIELD_DIALOG);
        if (dialog != null) {
            dialog.setTextInputDialogListener(this.mOtherFieldDialogListener);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_OTHER_VALUE, this.mOtherValue);
        outState.putBoolean(STATE_OTHER_ENABLED, this.mOtherEnabled);
        outState.putParcelableArrayList(STATE_ITEMS, new ArrayList (this.mItems));
        outState.putStringArrayList(STATE_SELECTED_VALUES, new ArrayList (this.mSelectedValues));
        ArrayList<Object> selected = new ArrayList ();
        Iterator it = this.mSelectedItems.iterator();
        while (it.hasNext()) {
            selected.add(((ClassificationItem) it.next()).toJSON());
        }
        outState.putString(STATE_SELECTED_ITEMS, JSONUtils.toJSONString(selected));
    }

    public boolean isEmpty() {
        if (this.mOtherEnabled && !isSearchActive()) {
            return false;
        }
        List<?> items = getDisplayItems();
        if (items == null || items.isEmpty()) {
            return true;
        }
        return false;
    }

    public int getCount() {
        int count = 0;
        if (this.mOtherEnabled && !isSearchActive()) {
            count = 0 + 1;
        }
        List<?> items = getDisplayItems();
        if (items != null) {
            return count + items.size();
        }
        return count;
    }

    public ClassificationItem getItem(int position) {
        if (!isOtherItemPosition(position)) {
            return (ClassificationItem) getDisplayItems().get(position);
        }
        String label = getString(R.string.other_choice_button);
        if (this.mSelectedItems.size() == this.mSelectedValues.size() && !TextUtils.isEmpty(this.mOtherValue)) {
            label = this.mOtherValue;
        }
        return new ClassificationItem(null, label, label);
    }

    public void setClassificationValue(ClassificationValue value) {
        ClassificationElement element = value.getElement();
        setTitle(element.getLabel());
        this.mOtherEnabled = element.isOtherAllowed();
        this.mItems = element.getItems();
        this.mOtherValue = value.getOtherValue();
        this.mSelectedValues = value.getSelectedValues();
    }

    public void setClassificationFieldDialogListener(ClassificationFieldDialogListener listener) {
        this.mListener = listener;
    }

    protected int getItemViewLayout() {
        return R.layout.list_item_classification;
    }

    protected void onBindItemView(View view, int position) {
        CheckedTextView label = (CheckedTextView) view.findViewById(R.id.text1);
        ClassificationItem item = getItem(position);
        if (isSearchActive()) {
            label.setText(item.getSearchLabel());
        } else {
            label.setText(item.getLabel());
        }
        boolean selected = false;
        if (this.mSelectedItems.size() == this.mSelectedValues.size() && !TextUtils.isEmpty(this.mOtherValue)) {
            selected = isOtherItemPosition(position);
        } else if (this.mSelectedItems.size() < this.mSelectedValues.size() && this.mSelectedValues.size() > 0) {
            selected = ClassificationValue.search(this.mSelectedValues, this.mSelectedItems.size(), item) != null;
        }
        label.setChecked(selected);
        if (item.hasChildItems()) {
            label.setCheckMarkDrawable(R.drawable.chevron_right);
        } else {
            label.setCheckMarkDrawable(this.mRadioButtonResource);
        }
    }

    protected void onSearchQueryDidChange(String query) {
        String trimmedAndLowered = query.trim().toLowerCase();
        if (TextUtils.isEmpty(trimmedAndLowered)) {
            this.mFilteredItems = null;
        } else {
            this.mFilteredItems = new ArrayList ();
            searchItems(getCurrentItems(), trimmedAndLowered, this.mFilteredItems);
        }
        notifyDataSetChanged();
    }

    protected boolean onChoiceItemClicked(ListView listView, int position, boolean selected) {
        ClassificationItem item = getItem(position);
        if (isOtherItemPosition(position)) {
            showOtherFieldDialog();
            return false;
        } else if (isSearchActive()) {
            if (this.mListener != null) {
                values = new ArrayList ();
                for (ClassificationItem part : item.getHierarchicalPath()) {
                    values.add(part.getValue());
                }
                this.mListener.onClassificationSelected(values, null);
            }
            return true;
        } else {
            this.mSelectedItems.push(item);
            if (item.hasChildItems()) {
                notifyDataSetChanged();
                return false;
            }
            if (this.mListener != null) {
                values = new ArrayList ();
                r4 = this.mSelectedItems.iterator();
                while (r4.hasNext()) {
                    values.add(((ClassificationItem) r4.next()).getValue());
                }
                this.mListener.onClassificationSelected(values, null);
            }
            return true;
        }
    }

    protected void onNeutralButtonClicked() {
        super.onNeutralButtonClicked();
        if (this.mListener != null) {
            this.mListener.onClassificationSelected(null, null);
        }
        dismissAllowingStateLoss();
    }

    protected void onPositiveButtonClicked() {
        super.onPositiveButtonClicked();
        dismissAllowingStateLoss();
    }

    protected boolean onBackButtonClicked() {
        super.onBackButtonClicked();
        if (this.mSelectedItems.empty() || isSearchActive()) {
            dismissAllowingStateLoss();
        } else {
            this.mSelectedItems.pop();
            notifyDataSetChanged();
        }
        return true;
    }

    private List<ClassificationItem> getCurrentItems() {
        if (this.mSelectedItems.empty()) {
            return this.mItems;
        }
        return ((ClassificationItem) this.mSelectedItems.peek()).getChildItems();
    }

    private List<ClassificationItem> getDisplayItems() {
        if (this.mFilteredItems != null) {
            return this.mFilteredItems;
        }
        return getCurrentItems();
    }

    private boolean isOtherItemPosition(int position) {
        return this.mFilteredItems == null && this.mOtherEnabled && position == getCount() - 1;
    }

    private boolean isSearchActive() {
        return this.mFilteredItems != null;
    }

    private void searchItems(List<ClassificationItem> items, String trimmedAndLowered, List<ClassificationItem> matching) {
        for (ClassificationItem item : items) {
            if (!item.getLabel().toLowerCase().contains(trimmedAndLowered)) {
                searchItems(item.getChildItems(), trimmedAndLowered, matching);
            } else if (item.hasChildItems()) {
                flattenChildren(item.getChildItems(), item, matching);
            } else {
                addSearchResult(item, item, matching);
            }
        }
    }

    private void flattenChildren(List<ClassificationItem> items, ClassificationItem ancestor, List<ClassificationItem> list) {
        for (ClassificationItem item : items) {
            if (item.hasChildItems()) {
                flattenChildren(item.getChildItems(), ancestor, list);
            } else {
                addSearchResult(item, ancestor, list);
            }
        }
    }

    private void addSearchResult(ClassificationItem item, ClassificationItem ancestor, List<ClassificationItem> matching) {
        item.setSearchLabel(ancestor);
        matching.add(item);
    }

    private void showOtherFieldDialog() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle(getString(R.string.other_field_title));
        dialog.setTextValue(this.mOtherValue);
        dialog.setTextInputDialogListener(this.mOtherFieldDialogListener);
        dialog.show(getChildFragmentManager(), TAG_OTHER_FIELD_DIALOG);
    }

    private void onOtherFieldDialogFinished(String otherValue) {
        if (this.mListener != null) {
            List<String> values = new ArrayList ();
            Iterator it = this.mSelectedItems.iterator();
            while (it.hasNext()) {
                values.add(((ClassificationItem) it.next()).getValue());
            }
            this.mListener.onClassificationSelected(values, otherValue);
        }
        dismissAllowingStateLoss();
    }
}
