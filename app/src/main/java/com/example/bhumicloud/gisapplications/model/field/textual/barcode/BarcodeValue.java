package com.example.bhumicloud.gisapplications.model.field.textual.barcode;

import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.example.bhumicloud.gisapplications.model.field.textual.TextualValue;

public class BarcodeValue extends TextualValue {
    private final BarcodeElement mElement;

    public BarcodeValue(BarcodeElement element) {
        this.mElement = element;
    }

    public BarcodeValue(BarcodeElement element, String aValue) {
        super(aValue);
        this.mElement = element;
    }

    public BarcodeElement getElement() {
        return this.mElement;
    }
}
