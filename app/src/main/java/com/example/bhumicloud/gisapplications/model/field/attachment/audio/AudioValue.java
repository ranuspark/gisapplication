package com.example.bhumicloud.gisapplications.model.field.attachment.audio;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaValue;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.List;
import java.util.Map;

public class AudioValue extends MediaValue {
    private final AudioElement mElement;

    public AudioValue(AudioElement element) {
        this.mElement = element;
    }

    public AudioValue(AudioElement element, List json) {
        this(element);
        if (json != null && !json.isEmpty()) {
            for (int i = 0; i < json.size(); i++) {
                Map audioJSON = JSONUtils.getHashMap(json, i);
                if (!(audioJSON == null || audioJSON.isEmpty())) {
                    this.mMediaItems.add(new AudioItemValue(audioJSON));
                }
            }
        }
    }

    protected int getDisplayValueResource() {
        return R.plurals.audio_file;
    }

    public AudioElement getElement() {
        return this.mElement;
    }

    public void addAudio(Audio audio) {
        if (audio != null) {
            this.mMediaItems.add(new AudioItemValue((Attachment) audio));
        }
    }
}
