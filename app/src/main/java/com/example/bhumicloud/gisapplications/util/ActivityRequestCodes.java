package com.example.bhumicloud.gisapplications.util;

public abstract class ActivityRequestCodes {
    public static final int REQUEST_APP_GALLERY = 112;
    public static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    public static final int REQUEST_EDIT_RECORD = 110;
    public static final int REQUEST_EDIT_SIGNATURE = 103;
    public static final int REQUEST_IMPORT_AUDIO = 108;
    public static final int REQUEST_IMPORT_VIDEO = 105;
    public static final int REQUEST_NEW_RECORD = 109;
    public static final int REQUEST_PICK_PHOTO = 102;
    public static final int REQUEST_PROFILE_EDITOR = 113;
    public static final int REQUEST_RECORD_AUDIO = 107;
    public static final int REQUEST_RECORD_BARCODE = 106;
    public static final int REQUEST_RECORD_VIDEO = 104;
    public static final int REQUEST_SELECT_RECORD = 111;
    public static final int REQUEST_SET_GEOMETRY = 100;
    public static final int REQUEST_TAKE_PHOTO = 101;
}
