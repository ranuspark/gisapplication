package com.example.bhumicloud.gisapplications.model.validation.error;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.model.field.textual.text.TextElement;
import com.example.bhumicloud.gisapplications.model.validation.FeatureValidationError;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class NumericRangeValidationError extends FeatureValidationError {
    public NumericRangeValidationError(TextElement element) {
        NumberFormat formatter = DecimalFormat.getInstance();
        formatter.setMinimumFractionDigits(0);
        String min = formatter.format(element.getMinValue());
        String max = formatter.format(element.getMaxValue());
        this.mMessage = String.format(GIS.getInstance().getResources().getString(R.string.numeric_range_validation_error), new Object[]{element.getLabel(), min, max});
    }
}
