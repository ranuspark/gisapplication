package com.example.bhumicloud.gisapplications.location;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.example.bhumicloud.gisapplications.util.GISLogger;


public class SimpleLocationClient implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
    private static final String DEBUG_TAG = SimpleLocationClient.class.getSimpleName();
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocationListener mLocationListener;
    private final LocationRequest mLocationRequest;
    private boolean mUpdatesEnabled;

    public SimpleLocationClient(Context context, LocationRequest locationRequest) {
        this.mGoogleApiClient = new Builder(context, this, this).addApi(LocationServices.API).build();
        if (locationRequest == null) {
            this.mLocationRequest = LocationRequest.create();
            this.mLocationRequest.setPriority(100);
            this.mLocationRequest.setInterval(0);
            return;
        }
        this.mLocationRequest = locationRequest;
    }

    public SimpleLocationClient(Context context) {
        this(context, null);
    }

    public void onConnected(Bundle extras) {
        if (this.mUpdatesEnabled) {
            LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient, this.mLocationRequest, (LocationListener) this);
        }
    }

    public void onConnectionSuspended(int cause) {
        String causeString;
        if (cause == 2) {
            causeString = "CAUSE_NETWORK_LOST";
        } else if (cause == 1) {
            causeString = "CAUSE_SERVICE_DISCONNECTED";
        } else {
            causeString = String.valueOf(cause);
        }
        Log.d(DEBUG_TAG, "onConnectionSuspended: " + causeString);
    }

    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Log.d(DEBUG_TAG, "onConnectionFailed: " + result.getErrorCode());
    }

    public void onLocationChanged(Location location) {
        this.mLastLocation = location;
        if (this.mLocationListener != null) {
            this.mLocationListener.onLocationChanged(location);
        }
    }

    public void connect() {
        this.mGoogleApiClient.connect();
    }

    public void disconnect() {
        ceaseUpdates();
        this.mGoogleApiClient.disconnect();
    }

    public void beginUpdates() {
        this.mUpdatesEnabled = true;
        if (this.mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient, this.mLocationRequest, (LocationListener) this);
        } else {
            this.mGoogleApiClient.connect();
        }
    }

    public void ceaseUpdates() {
        this.mUpdatesEnabled = false;
        if (this.mGoogleApiClient.isConnected()) {
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(this.mGoogleApiClient, (LocationListener) this);
            } catch (Throwable ex) {
                GISLogger.log(ex);
            }
        }
    }

    public Location getLastLocation() {
        Location location = LocationServices.FusedLocationApi.getLastLocation(this.mGoogleApiClient);
        if (location == null) {
            return this.mLastLocation;
        }
        return location;
    }

    public void setListener(LocationListener listener) {
        this.mLocationListener = listener;
    }
}

