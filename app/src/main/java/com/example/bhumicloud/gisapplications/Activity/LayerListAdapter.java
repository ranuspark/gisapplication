package com.example.bhumicloud.gisapplications.Activity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.bhumicloud.gisapplications.model.layer.MapLayer;
import com.example.bhumicloud.gisapplications.View.MapLayerListItemView;

public class LayerListAdapter extends ArrayAdapter<MapLayer> {
    private boolean mEditMode = false;
    private LayerListAdapterListener mListener;

    public interface LayerListAdapterListener {
        boolean isDownloading(long j);

        boolean isEnabled(long j);

        void onDelete(MapLayer mapLayer);
    }

    public LayerListAdapter(Context context, int layoutID, int textID, LayerListAdapterListener listener) {
        super(context, layoutID, textID);
        this.mListener = listener;
    }

    public MapLayer getItemById(long id) {
        for (int i = 0; i < getCount(); i++) {
            MapLayer ml = (MapLayer) getItem(i);
            if (ml != null && ml.getRowID() == id) {
                return ml;
            }
        }
        return null;
    }

    public void setEditMode(boolean editMode) {
        this.mEditMode = editMode;
    }

    public boolean isEditMode() {
        return this.mEditMode;
    }

    public long getItemId(int position) {
        if (position < 0 || position >= getCount()) {
            return -1;
        }
        MapLayer ml = (MapLayer) getItem(position);
        if (ml != null) {
            return ml.getRowID();
        }
        return -1;
    }

    @NonNull
    public MapLayerListItemView getView(int position, View reuse, @NonNull ViewGroup parent) {
        MapLayerListItemView view;
        if (reuse instanceof MapLayerListItemView) {
            view = (MapLayerListItemView) reuse;
        } else {
            view = new MapLayerListItemView(getContext());
        }
        view.configure((MapLayer) getItem(position), this.mListener, isEditMode());
        return view;
    }
}
