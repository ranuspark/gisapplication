package com.example.bhumicloud.gisapplications.model.field.temporal.time;

import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.example.bhumicloud.gisapplications.model.field.temporal.TemporalValue;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeValue extends TemporalValue {
    public static final DateFormat DISPLAY_FORMAT = SimpleDateFormat.getTimeInstance(3);
    public static final DateFormat VALUE_FORMAT = new SimpleDateFormat("HH:mm", Locale.US);
    private final TimeElement mElement;

    public TimeValue(TimeElement element, Date time) {
        super(time);
        this.mElement = element;
    }

    public TimeValue(TimeElement element, String value) {
        super(value);
        this.mElement = element;
    }

    public TimeElement getElement() {
        return this.mElement;
    }

    public DateFormat getValueFormat() {
        return VALUE_FORMAT;
    }

    public DateFormat getDisplayFormat() {
        return DISPLAY_FORMAT;
    }

    public Object getColumnValue() {
        try {
            Date date = getTime();
            if (date != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                return Integer.valueOf((calendar.get(11) * 60) + calendar.get(12));
            }
        } catch (ParseException e) {
        }
        return null;
    }
}
