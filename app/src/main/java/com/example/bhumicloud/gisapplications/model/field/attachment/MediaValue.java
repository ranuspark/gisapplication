package com.example.bhumicloud.gisapplications.model.field.attachment;

import android.text.TextUtils;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.MultipleValueItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public abstract class MediaValue implements FormValue {
    protected final ArrayList<MediaItemValue> mMediaItems = new ArrayList();

    protected abstract int getDisplayValueResource();

    protected MediaValue() {
    }

    public String getDisplayValue() {
        return GIS.getInstance().getResources().getQuantityString(getDisplayValueResource(), this.mMediaItems.size(), new Object[]{Integer.valueOf(this.mMediaItems.size())});
    }

    public String getSearchableValue() {
        if (isEmpty()) {
            return null;
        }
        List<String> tokens = new ArrayList();
        Iterator it = this.mMediaItems.iterator();
        while (it.hasNext()) {
            String caption = ((MediaItemValue) it.next()).getCaption();
            if (!TextUtils.isEmpty(caption)) {
                tokens.add(caption);
            }
        }
        return TextUtils.join(" ", tokens);
    }

    public int length() {
        return this.mMediaItems.size();
    }

    public ArrayList<HashMap<String, Object>> toJSON() {
        if (this.mMediaItems.isEmpty()) {
            return null;
        }
        ArrayList<HashMap<String, Object>> json = new ArrayList();
        Iterator it = this.mMediaItems.iterator();
        while (it.hasNext()) {
            json.add(((MediaItemValue) it.next()).toJSON());
        }
        return json;
    }

    public boolean isEmpty() {
        return this.mMediaItems.isEmpty();
    }

    public String getColumnValue() {
        ArrayList<String> identifiers = new ArrayList();
        for (MediaItemValue value : getMediaItems()) {
            identifiers.add(value.getMediaID());
        }
        return TextUtils.join(",", identifiers);
    }

    public ArrayList<MultipleValueItem> getMultipleValues() {
        ArrayList<MultipleValueItem> values = new ArrayList();
        for (MediaItemValue mediaItemValue : getMediaItems()) {
            values.add(new MultipleValueItem(getElement(), mediaItemValue.getMediaID()));
        }
        return values;
    }

    public boolean isEqualForConditions(String testValue) {
        return false;
    }

    public boolean contains(String testValue) {
        return false;
    }

    public boolean startsWith(String testValue) {
        return false;
    }

    public boolean isLessThan(String testValue) {
        try {
            return ((double) this.mMediaItems.size()) < Double.parseDouble(testValue);
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean isGreaterThan(String testValue) {
        try {
            return ((double) this.mMediaItems.size()) > Double.parseDouble(testValue);
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public List<MediaItemValue> getMediaItems() {
        return this.mMediaItems;
    }
    public MediaItemValue getMediaItemValue(String mediaID) {
        Iterator it = this.mMediaItems.iterator();
        while (it.hasNext()) {
            MediaItemValue value = (MediaItemValue) it.next();
            if (TextUtils.equals(value.getMediaID(), mediaID)) {
                return value;
            }
        }
        return null;
    }

    public void remove(MediaItemValue mediaValue) {
        this.mMediaItems.remove(mediaValue);
    }
}
