package com.example.bhumicloud.gisapplications.dossier;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.FormValueContainer;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkCondition;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkCondition.Operator;
import com.example.bhumicloud.gisapplications.model.field.recordlink.RecordLinkElement;
import java.util.ArrayList;
import java.util.Iterator;

public class RecordPredicate implements Parcelable {
    private static final String ALL = "all";
    private static final String ANY = "any";
    public static final Creator<RecordPredicate> CREATOR = new C11601();
    private static final String RECORD_LINK_FIELD_CONDITION_ROLE_KEY = "@role";
    private static final String RECORD_LINK_FIELD_CONDITION_STATUS_KEY = "@status";
    private String mFieldKey;
    private Operator mOperator;
    private ArrayList<RecordPredicate> mSubPredicates;
    private String mValue;
    private String mValueFieldKey;

    static class C11601 implements Creator<RecordPredicate> {
        C11601() {
        }

        public RecordPredicate createFromParcel(Parcel source) {
            return new RecordPredicate(source);
        }

        public RecordPredicate[] newArray(int size) {
            return new RecordPredicate[size];
        }
    }

    public RecordPredicate(RecordLinkElement recordLinkElement) {
        this(recordLinkElement.getRecordConditions(), recordLinkElement.getRecordConditionsType());
    }

    public RecordPredicate(ArrayList<RecordLinkCondition> conditions, String type) {
        this.mSubPredicates = new ArrayList();
        if (type != null && conditions != null && !conditions.isEmpty()) {
            if (type.equals("all")) {
                this.mOperator = Operator.and;
            } else if (type.equals("any")) {
                this.mOperator = Operator.or;
            }
            Iterator it = conditions.iterator();
            while (it.hasNext()) {
                this.mSubPredicates.add(new RecordPredicate((RecordLinkCondition) it.next()));
            }
        }
    }

    public RecordPredicate(RecordLinkCondition condition) {
        this.mSubPredicates = new ArrayList();
        this.mOperator = condition.getOperator();
        this.mFieldKey = condition.getLinkedFormFieldKey();
        this.mValue = condition.getValue();
        this.mValueFieldKey = condition.getValueFieldKey();
    }

    private RecordPredicate(Parcel parcel) {
        this.mSubPredicates = new ArrayList();
        this.mFieldKey = parcel.readString();
        String value = parcel.readString();
        if (value != null) {
            this.mOperator = Operator.valueOf(value);
        }
        this.mValue = parcel.readString();
        this.mValueFieldKey = parcel.readString();
        parcel.readTypedList(this.mSubPredicates, CREATOR);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mFieldKey);
        if (this.mOperator == null) {
            dest.writeString(null);
        } else {
            dest.writeString(this.mOperator.name());
        }
        dest.writeString(this.mValue);
        dest.writeString(this.mValueFieldKey);
        dest.writeTypedList(this.mSubPredicates);
    }

    public int describeContents() {
        return 0;
    }

    public void prepare(Record record, FormValueContainer conditionValues) {
        prepare(this, record, conditionValues);
    }

    public void buildParts(ArrayList<String> predicates, ArrayList<String> paramsList) {
        if (this.mOperator != null) {
            buildParts(this, predicates, paramsList);
        }
    }

    private void prepare(RecordPredicate condition, Record record, FormValueContainer conditionValues) {
        if (condition.mValueFieldKey != null) {
            String str = condition.mValueFieldKey;
            int obj = -1;
            switch (str.hashCode()) {
                case 62611638:
                    if (str.equals(RECORD_LINK_FIELD_CONDITION_ROLE_KEY)) {
                        obj = 1;
                        break;
                    }
                    break;
                case 73179186:
                    if (str.equals("@status")) {
                        obj = 0;
                        break;
                    }
                    break;
            }
            switch (obj) {
                case 0:
                    condition.mValue = record.getRecordStatus();
                    break;
                case 1:
                    condition.mValue = record.getAccount().getRole().getName();
                    break;
                default:
                    FormValue fv = conditionValues.getFormValue(condition.mValueFieldKey);
                    if (fv != null) {
                        Object obj2 = fv.getColumnValue();
                        condition.mValue = obj2 != null ? String.valueOf(obj2) : null;
                        break;
                    }
                    break;
            }
        }
        Iterator it = condition.mSubPredicates.iterator();
        while (it.hasNext()) {
            prepare((RecordPredicate) it.next(), record, conditionValues);
        }
    }

    private void buildParts(RecordPredicate condition, ArrayList<String> predicates, ArrayList<String> paramsList) {
        if (condition.mSubPredicates.size() > 0) {
            String subQuery;
            ArrayList<String> subPredicates = new ArrayList();
            ArrayList<String> subParams = new ArrayList();
            Iterator it = condition.mSubPredicates.iterator();
            while (it.hasNext()) {
                buildParts((RecordPredicate) it.next(), subPredicates, subParams);
            }
            if (condition.mOperator == Operator.and) {
                subQuery = TextUtils.join(" AND ", subPredicates);
            } else {
                subQuery = TextUtils.join(" OR ", subPredicates);
            }
            predicates.add("( " + subQuery + " )");
            paramsList.addAll(subParams);
        } else if (!TextUtils.isEmpty(condition.mValue) || condition.mOperator == Operator.is_empty || condition.mOperator == Operator.is_not_empty) {
            String predicate = null;
            String param = null;
            String columnName = columnNameForFieldKey(condition.mFieldKey);
            switch (condition.mOperator) {
                case equal_to:
                    predicate = "LOWER(" + columnName + ") = LOWER(?)";
                    param = condition.mValue;
                    break;
                case not_equal_to:
                    predicate = String.format("( %s IS NULL OR LOWER(%s) != LOWER(?) )", new Object[]{columnName, columnName});
                    param = condition.mValue;
                    break;
                case contains:
                    predicate = columnName + " LIKE ?";
                    param = "%" + condition.mValue + "%";
                    break;
                case starts_with:
                    predicate = columnName + " LIKE ?";
                    param = condition.mValue + "%";
                    break;
                case greater_than:
                    predicate = columnName + " > ?";
                    param = condition.mValue;
                    break;
                case less_than:
                    predicate = columnName + " < ?";
                    param = condition.mValue;
                    break;
                case is_empty:
                    predicate = columnName + " IS NULL";
                    break;
                case is_not_empty:
                    predicate = columnName + " IS NOT NULL";
                    break;
            }
            if (predicate != null) {
                predicates.add(predicate);
                if (param != null) {
                    paramsList.add(param);
                }
            }
        }
    }

    private String columnNameForFieldKey(String fieldKey) {
        if (fieldKey.equals("@status")) {
            return "query.status";
        }
        return "`f" + fieldKey + "`";
    }
}

