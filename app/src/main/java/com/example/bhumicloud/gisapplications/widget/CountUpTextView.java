package com.example.bhumicloud.gisapplications.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.util.GISLogger;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

public class CountUpTextView extends AppCompatTextView {
    private static final String DEFAULT_PATTERN = "H:mm:ss.S";
    private SimpleDateFormat mFormat;
    private Handler mHandler = new Handler();
    private int mInterval;
    private AtomicBoolean mRunning = new AtomicBoolean(false);
    private long mRuntime;
    private Timer mTimer;

    class C00061 extends TimerTask {

        class C00051 implements Runnable {
            C00051() {
            }

            public void run() {
                CountUpTextView.this.mRuntime = CountUpTextView.this.mRuntime + ((long) CountUpTextView.this.mInterval);
                CountUpTextView.this.setText(CountUpTextView.this.mFormat.format(Long.valueOf(CountUpTextView.this.mRuntime)));
            }
        }

        C00061() {
        }

        public void run() {
            CountUpTextView.this.mHandler.post(new C00051());
        }
    }



    @SuppressWarnings("ResourceType")
    public CountUpTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CardView, 0, 0);
        try {
            this.mInterval = a.getInt(0, 1000);
            String format = a.getString(1);
            if (format == null) {
                this.mFormat = new SimpleDateFormat(DEFAULT_PATTERN, Locale.getDefault());
            } else {
                this.mFormat = new SimpleDateFormat(format, Locale.getDefault());
            }
        } catch (IllegalArgumentException e) {
            this.mFormat = new SimpleDateFormat(DEFAULT_PATTERN, Locale.getDefault());
        } catch (Throwable th) {
            a.recycle();
        }
        a.recycle();
        this.mFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public void setRuntime(long runtime) {
        this.mRuntime = runtime;
        setText(this.mFormat.format(Long.valueOf(this.mRuntime)));
    }

    public void start() {
        if (this.mRunning.compareAndSet(false, true)) {
            this.mTimer = new Timer();
            try {
                this.mTimer.scheduleAtFixedRate(new C00061(), (long) this.mInterval, (long) this.mInterval);
            } catch (IllegalArgumentException e) {
                GISLogger.log(e);
            }
        }
    }

    public void stop() {
        if (this.mRunning.compareAndSet(true, false)) {
            this.mTimer.cancel();
        }
    }
}