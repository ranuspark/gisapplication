package com.example.bhumicloud.gisapplications.util;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.bhumicloud.gisapplications.model.Project;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement.StatusOption;
import java.util.ArrayList;
import java.util.List;

public class RecordFilterOptions implements Parcelable {
    public static final Creator<RecordFilterOptions> CREATOR = new C12291();
    private List<Long> mProjects;
    private List<String> mStatuses;
    private List<Long> mTempProjects;
    private List<String> mTempStatuses;

    static class C12291 implements Creator<RecordFilterOptions> {
        C12291() {
        }

        public RecordFilterOptions createFromParcel(Parcel source) {
            return new RecordFilterOptions(source);
        }

        public RecordFilterOptions[] newArray(int size) {
            return new RecordFilterOptions[size];
        }
    }

    public RecordFilterOptions() {
        this.mProjects = new ArrayList();
        this.mStatuses = new ArrayList();
        this.mTempProjects = new ArrayList();
        this.mTempStatuses = new ArrayList();
    }

    public RecordFilterOptions(Parcel parcel) {
        int i = 0;
        for (long project : parcel.createLongArray()) {
            this.mProjects.add(Long.valueOf(project));
        }
        long[] tempProjects = parcel.createLongArray();
        int length = tempProjects.length;
        while (i < length) {
            this.mTempProjects.add(Long.valueOf(tempProjects[i]));
            i++;
        }
        parcel.readStringList(this.mStatuses);
        parcel.readStringList(this.mTempStatuses);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        int i;
        long[] projects = new long[this.mProjects.size()];
        for (i = 0; i < projects.length; i++) {
            projects[i] = ((Long) this.mProjects.get(i)).longValue();
        }
        dest.writeLongArray(projects);
        long[] tempProjects = new long[this.mTempProjects.size()];
        for (i = 0; i < tempProjects.length; i++) {
            tempProjects[i] = ((Long) this.mTempProjects.get(i)).longValue();
        }
        dest.writeLongArray(tempProjects);
        dest.writeStringList(this.mStatuses);
        dest.writeStringList(this.mTempStatuses);
    }

    public List<Long> getSelectedProjects() {
        return this.mProjects;
    }

    public List<String> getSelectedStatusOptions() {
        return this.mStatuses;
    }

    public void addProject(Project project) {
        this.mTempProjects.add(Long.valueOf(project.getRowID()));
    }

    public void removeProject(Project project) {
        this.mTempProjects.remove(Long.valueOf(project.getRowID()));
    }

    public void addStatusOption(StatusOption option) {
        this.mTempStatuses.add(option.getValue());
    }

    public void removeStatusOption(StatusOption option) {
        this.mTempStatuses.remove(option.getValue());
    }

    public void commitChanges() {
        this.mProjects.clear();
        for (Long longValue : this.mTempProjects) {
            this.mProjects.add(Long.valueOf(longValue.longValue()));
        }
        this.mStatuses.clear();
        for (String status : this.mTempStatuses) {
            this.mStatuses.add(status);
        }
    }

    public boolean isEnabled(Project project) {
        return this.mTempProjects.contains(Long.valueOf(project.getRowID()));
    }

    public boolean isEnabled(StatusOption option) {
        return this.mTempStatuses.contains(option.getValue());
    }

    public void resetTemp() {
        this.mTempProjects.clear();
        for (Long longValue : this.mProjects) {
            this.mTempProjects.add(Long.valueOf(longValue.longValue()));
        }
        this.mTempStatuses.clear();
        for (String tempStatus : this.mStatuses) {
            this.mTempStatuses.add(tempStatus);
        }
    }

    public void resetFilter() {
        this.mProjects.clear();
        this.mTempProjects.clear();
        this.mStatuses.clear();
        this.mTempStatuses.clear();
    }
}

