package com.example.bhumicloud.gisapplications.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Build.VERSION;
import java.util.Locale;

public class ApplicationUtils {
    public static PackageInfo getApplicationPackageInfo(Context context, String application) {
        try {
            return context.getPackageManager().getPackageInfo(application, 0);
        } catch (NameNotFoundException e) {
            return null;
        }
    }

    public static PackageInfo getGISPackageInfo(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (NameNotFoundException e) {
            return null;
        }
    }

    public static String getVersion(Context context) {
        PackageInfo info = getGISPackageInfo(context);
        if (info != null) {
            return info.versionName;
        }
        return null;
    }

    public static int getBuildNumber(Context context) {
        PackageInfo info = getGISPackageInfo(context);
        if (info != null) {
            return info.versionCode;
        }
        return 0;
    }

    public static String getFormattedVersionString(Context context) {
        PackageInfo info = getGISPackageInfo(context);
        if (info == null) {
            return null;
        }
        String versionName = info.versionName;
        String buildNumber = String.valueOf(info.versionCode);
        return String.format("%s (%s)", new Object[]{versionName, buildNumber});
    }

    public static String getUserAgentString(Context context) {
        String format = "GIS Android %s, Android %s, %s, %s";
        String versionStr = getFormattedVersionString(context);
        String androidVer = VERSION.RELEASE;
        String manufacturer = Build.MANUFACTURER;
        String deviceName = Build.MODEL;
        return String.format(Locale.US, "GIS Android %s, Android %s, %s, %s", new Object[]{versionStr, androidVer, manufacturer, deviceName});
    }
}
