package com.example.bhumicloud.gisapplications.util;

import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory.Options;

public class BitmapUtils {
    public static CompressFormat getCompressFormat(String filepath, Options options) {
        String extension = FileUtils.getExtension(filepath);
        if ((options.outMimeType != null && options.outMimeType.matches("i\\w*/j\\w*g")) || extension.equalsIgnoreCase("jpeg") || extension.equalsIgnoreCase("jpg")) {
            return CompressFormat.JPEG;
        }
        return CompressFormat.PNG;
    }

    public static CompressFormat getCompressFormat(String filepath, String mimeType) {
        String extension = FileUtils.getExtension(filepath);
        if ((mimeType != null && mimeType.matches("i\\w*/j\\w*g")) || extension.equalsIgnoreCase("jpeg") || extension.equalsIgnoreCase("jpg")) {
            return CompressFormat.JPEG;
        }
        return CompressFormat.PNG;
    }
}
