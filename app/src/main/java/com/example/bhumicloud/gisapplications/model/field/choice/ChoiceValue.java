package com.example.bhumicloud.gisapplications.model.field.choice;

import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.MultipleValueItem;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ChoiceValue implements FormValue {
    private final ChoiceElement mElement;
    private HashMap<String, Object> mJSONValue;
    private final List<String> mOtherValues;
    private String mSearchableValue;
    private final ArrayList<String> mSelectedValues;

    public ChoiceValue(ChoiceElement element) {
        this.mSelectedValues = new ArrayList();
        this.mOtherValues = new ArrayList();
        this.mElement = element;
        String defaultValue = element.getDefaultValue();
        if (!TextUtils.isEmpty(defaultValue)) {
            this.mSelectedValues.add(defaultValue);
        }
    }

    private ChoiceValue(ChoiceElement element, String otherValue) {
        this.mSelectedValues = new ArrayList();
        this.mOtherValues = new ArrayList();
        this.mElement = element;
        if (!TextUtils.isEmpty(otherValue)) {
            this.mOtherValues.add(otherValue);
        }
    }

    public ChoiceValue(ChoiceElement element, Collection<String> selected, String otherValue) {
        this(element, otherValue);
        if (selected != null) {
            this.mSelectedValues.addAll(selected);
        }
    }

    public ChoiceValue(ChoiceElement element, String selected, String otherValue) {
        this(element, otherValue);
        if (selected != null) {
            this.mSelectedValues.add(selected);
        }
    }

    public ChoiceValue(ChoiceElement element, Map json) {
        this.mSelectedValues = new ArrayList();
        this.mOtherValues = new ArrayList();
        this.mElement = element;
        if (json != null && !json.isEmpty()) {
            int i;
            String value;
            List selectedJSON = JSONUtils.getArrayList(json, "choice_values");
            if (!(selectedJSON == null || selectedJSON.isEmpty())) {
                for (i = 0; i < selectedJSON.size(); i++) {
                    value = JSONUtils.getString(selectedJSON, i);
                    if (!TextUtils.isEmpty(value)) {
                        this.mSelectedValues.add(value);
                    }
                }
            }
            List otherJSON = JSONUtils.getArrayList(json, "other_values");
            if (otherJSON != null && !otherJSON.isEmpty()) {
                for (i = 0; i < otherJSON.size(); i++) {
                    value = JSONUtils.getString(otherJSON, i);
                    if (!TextUtils.isEmpty(value)) {
                        this.mOtherValues.add(value);
                    }
                }
            }
        }
    }

    public ChoiceElement getElement() {
        return this.mElement;
    }

    public String getDisplayValue() {
        return TextUtils.join(", ", toArrayList());
    }

    public String getSearchableValue() {
        if (this.mSearchableValue != null) {
            return this.mSearchableValue;
        }
        List<String> values = new ArrayList();
        Iterator it = this.mSelectedValues.iterator();
        while (it.hasNext()) {
            String value = (String) it.next();
            ChoiceItem item = this.mElement.getChoice(value);
            if (item != null) {
                values.add(item.getLabel());
                values.add(item.getValue());
            } else {
                values.add(value);
            }
        }
        values.addAll(this.mOtherValues);
        this.mSearchableValue = TextUtils.join(" ", values);
        return this.mSearchableValue;
    }

    public int length() {
        return toArrayList().size();
    }

    public HashMap<String, Object> toJSON() {
        if (isEmpty()) {
            return null;
        }
        if (this.mJSONValue != null) {
            return this.mJSONValue;
        }
        ArrayList<String> sJSON = new ArrayList();
        ArrayList<String> oJSON = new ArrayList();
        Iterator it = this.mSelectedValues.iterator();
        while (it.hasNext()) {
            sJSON.add((String) it.next());
        }
        for (String other : this.mOtherValues) {
            oJSON.add(other);
        }
        this.mJSONValue = new HashMap();
        this.mJSONValue.put("choice_values", sJSON);
        this.mJSONValue.put("other_values", oJSON);
        return this.mJSONValue;
    }

    public boolean isEmpty() {
        return this.mSelectedValues.isEmpty() && this.mOtherValues.isEmpty();
    }

    public String getColumnValue() {
        ArrayList<String> values = new ArrayList();
        values.addAll(this.mSelectedValues);
        values.addAll(this.mOtherValues);
        if (values.size() == 0) {
            return null;
        }
        if (getElement().isMultipleChoice()) {
            return "\t" + TextUtils.join("\t", values) + "\t";
        }
        return (String) values.get(0);
    }

    public ArrayList<MultipleValueItem> getMultipleValues() {
        return null;
    }

    public boolean isEqualForConditions(String value) {
        String otherValue = getOtherValue();
        List<String> selected = this.mSelectedValues;
        if (selected != null) {
            for (String item : selected) {
                if (item.equals(value)) {
                    return true;
                }
            }
        }
        if (otherValue == null || !otherValue.equalsIgnoreCase(value)) {
            return false;
        }
        return true;
    }

    public boolean contains(String value) {
        return isEqualForConditions(value);
    }

    public boolean startsWith(String value) {
        return contains(value);
    }

    public boolean isLessThan(String testValue) {
        if (isEmpty() && !TextUtils.isEmpty(testValue)) {
            return false;
        }
        try {
            double thatValue = Double.parseDouble(testValue);
            Iterator it = getAllValues().iterator();
            while (it.hasNext()) {
                if (Double.parseDouble((String) it.next()) >= thatValue) {
                    return false;
                }
            }
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean isGreaterThan(String testValue) {
        if (isEmpty() && !TextUtils.isEmpty(testValue)) {
            return false;
        }
        try {
            double thatValue = Double.parseDouble(testValue);
            Iterator it = getAllValues().iterator();
            while (it.hasNext()) {
                if (Double.parseDouble((String) it.next()) <= thatValue) {
                    return false;
                }
            }
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public ArrayList<String> getAllValues() {
        ArrayList<String> values = new ArrayList();
        values.addAll(this.mSelectedValues);
        values.addAll(this.mOtherValues);
        return values;
    }

    public ArrayList<String> getSelectedValues() {
        return this.mSelectedValues;
    }

    public String getOtherValue() {
        if (this.mOtherValues.size() > 0) {
            return (String) this.mOtherValues.get(0);
        }
        return null;
    }

    private ArrayList<String> toArrayList() {
        ArrayList<String> values = new ArrayList();
        Iterator it = this.mSelectedValues.iterator();
        while (it.hasNext()) {
            String value = (String) it.next();
            ChoiceItem item = this.mElement.getChoice(value);
            if (item != null) {
                values.add(item.getLabel());
            } else {
                values.add(value);
            }
        }
        values.addAll(this.mOtherValues);
        return values;
    }


}
