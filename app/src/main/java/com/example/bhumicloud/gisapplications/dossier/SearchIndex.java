package com.example.bhumicloud.gisapplications.dossier;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.util.GISLogger;

public class SearchIndex extends SQLiteOpenHelper {
    private static final String DEBUG_TAG = SearchIndex.class.getSimpleName();
    private static final String FILENAME = "BhumiGIS.search.db";
    private static final int SCHEMA_VERSION = 1;
    private static SearchIndex sSharedInstance;

    public static synchronized SearchIndex getInstance(Context context) {
        SearchIndex searchIndex;
        synchronized (SearchIndex.class) {
            if (sSharedInstance != null) {
                searchIndex = sSharedInstance;
            } else {
                sSharedInstance = new SearchIndex(context.getApplicationContext());
                sSharedInstance.getWritableDatabase();
                sSharedInstance.close();
                searchIndex = sSharedInstance;
            }
        }
        return searchIndex;
    }

    public SearchIndex(Context context) {
        super(context, FILENAME, null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        createRecordValuesIndexTable(db);
    }

    public void onConfigure(SQLiteDatabase db) {
        db.enableWriteAheadLogging();
        db.execSQL("PRAGMA synchronous=NORMAL");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        GISLogger.log(DEBUG_TAG, "Upgrading Search Index Database To Version " + newVersion + " From " + oldVersion);
    }

    public long[] searchRecordValues(String query) {
        query = query.trim();
        if (TextUtils.isEmpty(query)) {
            return null;
        }
        long[] results = null;
        SQLiteDatabase db = getReadableDatabase();
        String flanked = "\"*" + query + "*\"";
        String str = "record_values_index";
        Cursor c = db.query(str, new String[]{"rowid"}, "record_values_index MATCH ?", new String[]{flanked}, null, null, null);
        if (c != null) {
            results = new long[c.getCount()];
            c.moveToFirst();
            while (!c.isAfterLast()) {
                results[c.getPosition()] = c.getLong(0);
                c.moveToNext();
            }
            c.close();
        }
        return results;
    }

    public long updateRecordValuesIndex(Record record) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("content", record.getSearchableValue());
        long rowID = record.getValuesIndexRowID();
        if (rowID > 0) {
            db.update("record_values_index", values, "rowid = ?", new String[]{String.valueOf(rowID)});
        } else {
            db.insert("record_values_index", null, values);
            Cursor c = db.query("record_values_index", new String[]{"max(rowid)"}, null, null, null, null, null);
            if (c != null) {
                c.moveToFirst();
                if (!c.isAfterLast()) {
                    rowID = c.getLong(0);
                }
                c.close();
            }
        }
        return rowID;
    }

    public void deleteRecordValuesIndex(Record record) {
        SQLiteDatabase db = getWritableDatabase();
        if (record.getValuesIndexRowID() > 0) {
            db.delete("record_values_index", "rowid = ?", new String[]{String.valueOf(record.getValuesIndexRowID())});
        }
    }

    private void createRecordValuesIndexTable(SQLiteDatabase db) {
        GISLogger.log(DEBUG_TAG, "Creating Record Search Index Table...");
        db.execSQL("CREATE VIRTUAL TABLE record_values_index USING fts3");
    }
}

