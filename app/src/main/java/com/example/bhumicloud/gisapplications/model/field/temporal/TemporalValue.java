package com.example.bhumicloud.gisapplications.model.field.temporal;

import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.model.field.textual.TextualValue;
import com.example.bhumicloud.gisapplications.util.GISLogger;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

public abstract class TemporalValue extends TextualValue {
    private static final String NOW = "now";

    public abstract DateFormat getDisplayFormat();

    public abstract DateFormat getValueFormat();

    public TemporalValue(Date time) {
        this.mValue = getValueFormat().format(time);
    }

    public TemporalValue(String aValue) {
        if (aValue.equalsIgnoreCase(NOW)) {
            this.mValue = getValueFormat().format(new Date());
            return;
        }
        boolean parseable;
        try {
            getValueFormat().parse(aValue);
            parseable = true;
        } catch (ParseException e) {
            parseable = false;
        }
        if (!parseable) {
            aValue = null;
        }
        this.mValue = aValue;
    }

    public String getDisplayValue() {
        if (isEmpty()) {
            return null;
        }
        try {
            return getDisplayFormat().format(getTime());
        } catch (Throwable e) {
            GISLogger.log(e);
            return this.mValue;
        }
    }

    public boolean isLessThan(String testValue) {
        if (this.mValue == null) {
            return TextUtils.isEmpty(testValue);
        }
        try {
            Date thisDate = getTime();
            Date thatDate = getValueFormat().parse(testValue);
            if (thisDate == null || thatDate == null || thisDate.getTime() >= thatDate.getTime()) {
                return false;
            }
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public boolean isGreaterThan(String testValue) {
        if (this.mValue == null) {
            return TextUtils.isEmpty(testValue);
        }
        try {
            Date thisDate = getTime();
            Date thatDate = getValueFormat().parse(testValue);
            if (thisDate == null || thatDate == null || thisDate.getTime() <= thatDate.getTime()) {
                return false;
            }
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public Date getTime() throws ParseException {
        return !isEmpty() ? getValueFormat().parse(this.mValue) : null;
    }
}
