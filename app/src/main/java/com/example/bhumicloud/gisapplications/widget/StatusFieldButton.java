package com.example.bhumicloud.gisapplications.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.util.StateSet;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.field.textual.status.StatusElement.StatusOption;
import com.example.bhumicloud.gisapplications.util.ColorUtils;
import com.example.bhumicloud.gisapplications.util.DeviceInfo;

public class StatusFieldButton extends AppCompatButton {

    private static class BackgroundFactory {
        private BackgroundFactory() {
        }

        public static Drawable createDrawable(Context context, int color) {
            Resources res = context.getResources();
            int radius = res.getDimensionPixelSize(R.dimen.btn_radius);
            int stroke = res.getDimensionPixelSize(R.dimen.btn_stroke);
            StateListDrawable bg = new StateListDrawable ();
            GradientDrawable pressed = new GradientDrawable ();
            pressed.setCornerRadius((float) radius);
            pressed.setColor(ColorUtils.darken(color, 0.15f));
            bg.addState(new int[]{16842919}, pressed);
            GradientDrawable normal = new GradientDrawable ();
            normal.setColor(color);
            normal.setCornerRadius((float) radius);
            if (ColorUtils.getContentColor(color, 0, 1) == 1) {
                normal.setStroke(stroke, -4802890);
            }
            bg.addState( StateSet.WILD_CARD, normal);
            return bg;
        }
    }

    public StatusFieldButton(Context context) {
        super(context);
    }

    public StatusFieldButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StatusFieldButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @TargetApi(16)
    public void setStatusOption(StatusOption status) {
        int statusColor;
        Context context = getContext();
        if (status != null) {
            statusColor = status.getColor();
        } else {
            statusColor = -1;
        }
        setTextColor(ColorUtils.getContentColor(statusColor));
        Drawable background = BackgroundFactory.createDrawable(context, statusColor);
        if (DeviceInfo.isJellyBean()) {
            setBackground(background);
        } else {
            setBackgroundDrawable(background);
        }
    }
}
