package com.example.bhumicloud.gisapplications.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.model.appgallery.App;
import com.example.bhumicloud.gisapplications.util.GISLogger;

public class AddAppDialog extends ThemedDialogFragment {
    private static final String ARG_APP = "GIS:arg:app";
    private static final String STATE_CURRENT_STEP = "GIS:state:current_step";
    private static final int STEP_ADDING = 1;
    private static final int STEP_CONFIRM = 0;
    private static final int STEP_DONE = 2;
    private static final String TAG = AddAppDialog.class.getCanonicalName();
    private App mApp;
    private int mCurrentStep;
    private AddAppDialogListener mListener;
    private TextView mMessageView;
    private TextView mProgressLabel;
    private View mProgressView;

    public interface AddAppDialogListener {
        void onAddAppConfirmed(App app);
    }

    private static AddAppDialog getDialog(App app) {
        AddAppDialog dialog = new AddAppDialog();
        Bundle arguments = new Bundle ();
        arguments.putParcelable(ARG_APP, app);
        dialog.setArguments(arguments);
        dialog.setCancelable(false);
        return dialog;
    }

    public static void show(FragmentManager manager, String tag, App app) {
        getDialog(app).show(manager, tag);
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AddAppDialogListener) {
            this.mListener = (AddAppDialogListener) context;
            return;
        }
        throw new RuntimeException (context.toString() + " must implement " + AddAppDialogListener.class.getSimpleName());
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mApp = (App) getArguments().getParcelable(ARG_APP);
        if (this.mApp == null) {
            Toast.makeText(getActivity(), R.string.failed_add_app, 1).show();
            GISLogger.log(TAG, "mApp is null");
            dismissAllowingStateLoss();
            return;
        }
        setTitle((int) R.string.add_app);
        if (savedInstanceState != null) {
            this.mCurrentStep = savedInstanceState.getInt(STATE_CURRENT_STEP);
        } else {
            this.mCurrentStep = 0;
        }
    }

    public View onCreateContentView(LayoutInflater inflater, ViewGroup root, Bundle savedState) {
        View view = inflater.inflate(R.layout.dialog_add_app, root, false);
        if (this.mApp != null) {
            this.mProgressView = view.findViewById(R.id.progress);
            this.mMessageView = (TextView) view.findViewById(R.id.message);
            this.mProgressLabel = (TextView) view.findViewById(R.id.progress_label);
            reloadView();
        }
        return view;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_CURRENT_STEP, this.mCurrentStep);
    }

    public void onDestroyView() {
        this.mMessageView = null;
        this.mProgressView = null;
        this.mProgressLabel = null;
        super.onDestroyView();
    }

    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }

    public void onAddAppSuccess() {
        this.mCurrentStep = 2;
        reloadView();
    }

    protected void onNegativeButtonClicked() {
        super.onNegativeButtonClicked();
        if (this.mCurrentStep == 0) {
            dismissAllowingStateLoss();
        }
    }

    protected void onPositiveButtonClicked() {
        super.onPositiveButtonClicked();
        if (this.mCurrentStep == 0) {
            this.mCurrentStep = 1;
            this.mListener.onAddAppConfirmed(this.mApp);
            reloadView();
        } else if (this.mCurrentStep == 2) {
            dismissAllowingStateLoss();
        }
    }

    private void reloadView() {
        this.mMessageView.setVisibility(8);
        this.mProgressView.setVisibility(8);
        setNegativeButtonText(null);
        setNeutralButtonText(null);
        setPositiveButtonText(null);
        switch (this.mCurrentStep) {
            case 0:
                this.mMessageView.setVisibility(View.VISIBLE);
                this.mMessageView.setText(getString(R.string.add_app_confirmation, this.mApp.getName()));
                setNegativeButtonText((int) R.string.cancel);
                setPositiveButtonText((int) R.string.okay);
                return;
            case 1:
                this.mProgressView.setVisibility(View.VISIBLE);
                this.mProgressLabel.setText(R.string.adding_app);
                return;
            case 2:
                this.mMessageView.setVisibility(View.VISIBLE);
                this.mMessageView.setText(R.string.added_app_successfully);
                setPositiveButtonText((int) R.string.okay);
                return;
            default:
                dismissAllowingStateLoss();
                return;
        }
    }
}
