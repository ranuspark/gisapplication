CREATE TRIGGER IF NOT EXISTS delete_record_attachments DELETE ON Records
    BEGIN
        DELETE FROM attachments WHERE record_id = OLD.unique_id;
    END;