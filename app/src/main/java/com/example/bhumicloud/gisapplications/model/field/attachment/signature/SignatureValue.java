package com.example.bhumicloud.gisapplications.model.field.attachment.signature;

import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.MultipleValueItem;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.field.attachment.MediaItemValue;
import com.example.bhumicloud.gisapplications.util.DateUtils;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SignatureValue implements FormValue {
    private static final String JSON_SIGNATURE_ID = "signature_id";
    private static final String JSON_TIMESTAMP = "timestamp";
    private final SignatureElement mElement;
    private String mSignatureID;
    private String mTimestamp;

    public SignatureValue(SignatureElement element) {
        this.mElement = element;
    }

    public SignatureValue(SignatureElement element, Map attributes) {
        this.mElement = element;
        this.mSignatureID = JSONUtils.getString(attributes, JSON_SIGNATURE_ID);
        this.mTimestamp = JSONUtils.getString(attributes, JSON_TIMESTAMP);
    }

    public SignatureElement getElement() {
        return this.mElement;
    }

    public String getDisplayValue() {
        return "1 Signature";
    }

    public String getSearchableValue() {
        return null;
    }

    public int length() {
        if (this.mSignatureID == null) {
            return 0;
        }
        return 1;
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = new HashMap();
        json.put(JSON_TIMESTAMP, this.mTimestamp);
        json.put(JSON_SIGNATURE_ID, getSignatureID());
        return json;
    }

    public boolean isEmpty() {
        return false;
    }

    public String getColumnValue() {
        return this.mSignatureID;
    }

    public ArrayList<MultipleValueItem> getMultipleValues() {
        return new ArrayList();
    }

    public boolean isEqualForConditions(String testValue) {
        return false;
    }

    public boolean contains(String testValue) {
        return false;
    }

    public boolean startsWith(String testValue) {
        return false;
    }

    public boolean isLessThan(String testValue) {
        return false;
    }

    public boolean isGreaterThan(String testValue) {
        return false;
    }

    public String getSignatureID() {
        return this.mSignatureID;
    }

    public void setSignatureID(String signatureID) {
        this.mSignatureID = signatureID;
    }

    public void setSignature(Signature signature) {
        setSignatureID(signature.getRemoteID());
    }

    public Signature getSignature() {
        if (TextUtils.isEmpty(this.mSignatureID)) {
            return null;
        }
        return (Signature) Attachment.findByRemoteID(this.mSignatureID);
    }

    public void setTimestamp(Date timestamp) {
        if (timestamp != null) {
            this.mTimestamp = DateUtils.getTimestampString(timestamp);
        } else {
            this.mTimestamp = null;
        }
    }
}
