package com.example.bhumicloud.gisapplications.util;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Video.Media;
import android.support.v4.content.FileProvider;

import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment.Type;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;

public class FileUtils {

    public static class CopyMediaTask extends AsyncTask<String, Void, File> {
        private ContentResolver mContentResolver;
        private long mDuration;
        private Type mType;

        public CopyMediaTask(ContentResolver contentResolver, Type type) {
            this.mContentResolver = contentResolver;
            this.mType = type;
        }

        public CopyMediaTask(ContentResolver contentResolver, Type type, long duration) {
            this(contentResolver, type);
            this.mDuration = duration;
        }

        protected File doInBackground(String... params) {
            File mediaDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getPath() + "/Camera");
            if (!mediaDirectory.exists()) {
                mediaDirectory.mkdirs();
            }
            if (!mediaDirectory.exists()) {
                mediaDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                if (!mediaDirectory.exists()) {
                    mediaDirectory.mkdirs();
                }
                if (!mediaDirectory.exists()) {
                    GISLogger.log(CopyMediaTask.class.getSimpleName(), "could not find a gallery directory to use");
                    return null;
                }
            }
            try {
                File originalMedia = new File(params[0]);
                File copiedMedia = new File(mediaDirectory + "/" + originalMedia.getName());
                InputStream in = new FileInputStream(originalMedia);
                OutputStream out = new FileOutputStream(copiedMedia);
                byte[] buffer = new byte[1024];
                while (true) {
                    int read = in.read(buffer);
                    if (read != -1) {
                        out.write(buffer, 0, read);
                    } else {
                        in.close();
                        out.flush();
                        out.close();
                        return copiedMedia;
                    }
                }
            } catch (Throwable e) {
                GISLogger.log(e);
                return null;
            }
        }

        protected void onPostExecute(File result) {
            if (result != null) {
                ContentValues values = new ContentValues();
                values.put("_data", result.getPath());
                values.put("date_added", Long.valueOf(System.currentTimeMillis()));
                values.put("_display_name", result.getName());
                values.put("mime_type", MimeUtils.getMimeType(result, this.mType));
                values.put("title", result.getName());
                if (this.mType == Type.VIDEO) {
                    values.put("duration", Long.valueOf(this.mDuration));
                    this.mContentResolver.insert(Media.EXTERNAL_CONTENT_URI, values);
                } else if (this.mType == Type.PHOTO) {
                    this.mContentResolver.insert(Images.Media.EXTERNAL_CONTENT_URI, values);
                }
            }
        }
    }

    public enum GISDirectoryType {
        reports,
        backups,
        profiles,
        attachments,
        maps,
        thumbnails
    }

    public static boolean isExternalStorageAvailable() {
        return "mounted".equals(Environment.getExternalStorageState());
    }

    public static String toHumanReadableBytes(double bytes) {
        if (bytes < ((double) 1024)) {
            return bytes + " B";
        }
        String pre = "KMGTPE".charAt(((int) (Math.log(bytes) / Math.log((double) 1024))) - 1) + "";
        return String.format(Locale.US, "%.1f %sB", new Object[]{Double.valueOf(bytes / Math.pow((double) 1024, (double) 1024)), pre});
    }

    public static boolean isReadable(File aFile) {
        return aFile != null && aFile.exists() && aFile.canRead();
    }

    public static boolean isWritable(File aFile) {
        return aFile != null && aFile.exists() && aFile.canWrite();
    }

    public static void delete(File file) {
        if (file != null && file.exists()) {
            if (file.isDirectory()) {
                for (File child : file.listFiles()) {
                    delete(child);
                }
                return;
            }
            file.delete();
        }
    }

    public static String getExtension(String filepath) {
        int dot = filepath.lastIndexOf(".");
        return dot < 0 ? filepath : filepath.substring(dot + 1);
    }

    public static String getFilenameWithExtensionFromPath(String path) {
        int slash = path.lastIndexOf("/");
        return slash < 0 ? path : path.substring(slash);
    }

    public static String getFilenameFromPath(String path) {
        int slash = path.lastIndexOf("/");
        if (slash < 0) {
            return path;
        }
        path = path.substring(slash);
        int dot = path.indexOf(".");
        return dot >= 0 ? path.substring(0, dot) : path;
    }

    public static File getGisDataDir(Context context, GISDirectoryType type) {
        File directory = new File(context.getExternalFilesDir(null), type.name());
        if (!directory.exists()) {
            directory.mkdirs();
        }
        return directory;
    }

    public static Uri getShareableUriForFile(Context context, File file) {
        if (DeviceInfo.isNougat()) {
            return FileProvider.getUriForFile(context, "com.example.bhumicloud.gisapplications.fileprovider", file);
        }
        return Uri.fromFile(file);
    }

    public static String generateThumbnailPath(Context context, String originalImagePath) {
        return getGisDataDir(context, GISDirectoryType.thumbnails).getAbsolutePath() + getFilenameWithExtensionFromPath(originalImagePath);
    }

    public static boolean isJpeg(File file) {
        byte[] buffer = new byte[2];
        try {
            if (new FileInputStream(file).read(buffer) == buffer.length) {
                if (buffer[0] == (byte) -1 && buffer[1] == (byte) -40) {
                    return true;
                }
                return false;
            }
        } catch (IOException e) {
        }
        return false;
    }
}

