CREATE TABLE IF NOT EXISTS Projects (
    _id INTEGER PRIMARY KEY AUTOINCREMENT,
    remote_id TEXT,
    account_id INTEGER,
    name TEXT,
    description TEXT,
    deleted_at REAL
);
CREATE INDEX IF NOT EXISTS projects_account_id_idx ON Projects (account_id);
CREATE INDEX IF NOT EXISTS projects_account_id_name_idx ON Projects (account_id, name);
CREATE INDEX IF NOT EXISTS projects_account_id_remote_id_idx ON Projects (account_id, remote_id);