package com.example.bhumicloud.gisapplications.util;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class StreamUtils {
    public static String toString(File file) throws IOException {
        return toString(file, HttpRequest.CHARSET_UTF8);
    }

    public static String toString(File file, String encoding) throws IOException {
        return file == null ? null : toString(new FileInputStream(file), encoding);
    }

    public static String toString(InputStream byteStream) throws IOException {
        return toString(byteStream, HttpRequest.CHARSET_UTF8);
    }

    public static String toString(InputStream byteStream, String encoding) throws IOException {
        char[] buffer = new char[1024];
        StringBuilder builder = new StringBuilder();
        InputStreamReader reader = new InputStreamReader(byteStream, encoding);
        while (true) {
            int length = reader.read(buffer);
            if (length >= 0) {
                builder.append(buffer, 0, length);
            } else {
                reader.close();
                return builder.toString();
            }
        }
    }

    public static void writeToFile(InputStream stream, File file) throws IOException {
        writeToFile(stream, file, 1024);
    }

    public static void writeToFile(InputStream stream, File file, int bufferSize) throws IOException {
        byte[] buffer = new byte[bufferSize];
        OutputStream out = new BufferedOutputStream(new FileOutputStream(file), bufferSize);
        while (true) {
            int length = stream.read(buffer);
            if (length >= 0) {
                out.write(buffer, 0, length);
            } else {
                out.flush();
                out.close();
                return;
            }
        }
    }
}

