package com.example.bhumicloud.gisapplications.View;

import android.content.Context;
import com.example.bhumicloud.gisapplications.model.field.temporal.date.DateElement;

public class DateElementView extends ElementView<DateElement> {
    private DateElementViewListener mListener;

    public interface DateElementViewListener {
        void onShowDateFieldDialog(DateElement dateElement);
    }

    public DateElementView(Context context, DateElement element, boolean viewOnly) {
        super(context, element, viewOnly);
    }

    public void setListener(DateElementViewListener listener) {
        this.mListener = listener;
    }

    protected void onFieldViewClicked() {
        super.onFieldViewClicked();
        if (this.mListener != null) {
            this.mListener.onShowDateFieldDialog((DateElement) getElement());
        }
    }
}
