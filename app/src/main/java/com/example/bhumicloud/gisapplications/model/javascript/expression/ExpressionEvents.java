package com.example.bhumicloud.gisapplications.model.javascript.expression;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.MediaMetadataRetriever;

import com.example.bhumicloud.gisapplications.model.Feature;
import com.example.bhumicloud.gisapplications.model.FormValue;
import com.example.bhumicloud.gisapplications.model.FormValueContainer;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.attachment.audio.Audio;
import com.example.bhumicloud.gisapplications.model.field.attachment.photo.Photo;
import com.example.bhumicloud.gisapplications.model.field.attachment.video.Video;
import com.example.bhumicloud.gisapplications.util.DeviceInfo;
import com.example.bhumicloud.gisapplications.util.EXIFUtils;
import com.example.bhumicloud.gisapplications.util.FileUtils;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import io.fabric.sdk.android.services.settings.SettingsJsonConstants;

public class ExpressionEvents {
    private static SimpleDateFormat parser;
    private Feature mFeature;
    private Record mRecord;
    private FormValueContainer mValues;

    public ExpressionEvents(Record record, Feature feature) {
        this.mRecord = record;
        this.mFeature = feature;
    }

    public void setValues(FormValueContainer values) {
        this.mValues = values;
    }

    public List<ExpressionResult> onValueChange(ExpressionEngine engine, Element element, FormValue value) {
        HashMap<String, Object> data = new HashMap ();
        data.put("field", element.getDataName());
        data.put("value", value == null ? null : value.toJSON());
        return triggerEvent(engine, "change", data);
    }

    public List<ExpressionResult> onValueChange(ExpressionEngine engine, String eventName, Object value, String dataName) {
        HashMap<String, Object> data = new HashMap ();
        if (dataName != null) {
            data.put("field", dataName);
        }
        data.put("value", value);
        return triggerEvent(engine, eventName, data);
    }

    public List<ExpressionResult> triggerEvent(ExpressionEngine engine, String eventName, Map<String, Object> data) {
        if (engine == null) {
            return null;
        }
        if (data == null) {
            data = new HashMap ();
        }
        data.put("name", eventName);
        engine.setRecord(this.mRecord);
        engine.setFeature(this.mFeature);
        engine.setValues(this.mValues);
        return engine.triggerEvent(data);
    }

    public static HashMap<String, Object> createPhotoEvent(Photo photo) {
        HashMap<String, Object> event = new HashMap ();
        File fileOne = photo.getFileOne();
        event.put("id", photo.getRemoteID());
        event.put("uri", photo.getUriOne().toString());
        event.put("size", Long.valueOf(fileOne.length()));
        if (FileUtils.isJpeg(fileOne)) {
            try {
                event.putAll(EXIFUtils.getEXIF(fileOne));
            } catch (Throwable e) {
                GISLogger.log(e);
            }
        }
        return event;
    }

    @TargetApi(17)
    public static HashMap<String, Object> createVideoEvent(Video video, Context context) {
        HashMap<String, Object> event = new HashMap ();
        event.put("id", video.getRemoteID());
        event.put("uri", video.getUriOne().toString());
        event.put("size", Long.valueOf(video.getFileOne().length()));
        try {
            if (video.getFileTwo() != null) {
                event.put("track", JSONUtils.toJSONObject(video.getFileTwo()));
            }
            MediaMetadataRetriever retriever = new MediaMetadataRetriever ();
            retriever.setDataSource(context, video.getUriOne());
            String duration = retriever.extractMetadata(9);
            String width = retriever.extractMetadata(18);
            String height = retriever.extractMetadata(19);
            String date = retriever.extractMetadata(5);
            String orientation = DeviceInfo.isJellyBeanMR1() ? retriever.extractMetadata(24) : null;
            event.put("duration", Double.valueOf(((double) Long.parseLong(duration)) / 1000.0d));
            event.put( SettingsJsonConstants.ICON_WIDTH_KEY, Long.valueOf( Long.parseLong(width)));
            event.put( SettingsJsonConstants.ICON_HEIGHT_KEY, Long.valueOf( Long.parseLong(height)));
            event.put("orientation", Long.valueOf( Long.parseLong(orientation)));
            event.put("timestamp", convertDateStringToJavaScriptDateString(date));
        } catch (Throwable e) {
            GISLogger.log(e);
        }
        return event;
    }

    public static HashMap<String, Object> createAudioEvent(Audio audio, Context context) {
        HashMap<String, Object> event = new HashMap ();
        event.put("id", audio.getRemoteID());
        event.put("uri", audio.getUriOne().toString());
        event.put("size", Long.valueOf(audio.getFileOne().length()));
        try {
            if (audio.getFileTwo() != null) {
                event.put("track", JSONUtils.toJSONObject(audio.getFileTwo()));
            }
            MediaMetadataRetriever retriever = new MediaMetadataRetriever ();
            retriever.setDataSource(context, audio.getUriOne());
            String duration = retriever.extractMetadata(9);
            String date = retriever.extractMetadata(5);
            event.put("duration", Double.valueOf(((double) Long.parseLong(duration)) / 1000.0d));
            event.put("timestamp", convertDateStringToJavaScriptDateString(date));
        } catch (Throwable e) {
            GISLogger.log(e);
        }
        return event;
    }

    private static String convertDateStringToJavaScriptDateString(String utcDateString) {
        Date date = null;
        try {
            SimpleDateFormat parser = new SimpleDateFormat ("yyyyMMdd'T'HHmmss.SSS'Z'", Locale.US);
            parser.setTimeZone( TimeZone.getTimeZone("UTC"));
            date = parser.parse(utcDateString);
        } catch (ParseException e) {
        }
        if (date == null) {
            try {
                parser = new SimpleDateFormat ("yyyy MM dd", Locale.US);
                parser.setTimeZone( TimeZone.getTimeZone("UTC"));
                date = parser.parse(utcDateString);
            } catch (ParseException e2) {
            }
        }
        if (date == null) {
            GISLogger.log("Unparseable date: " + utcDateString);
            return null;
        }
        SimpleDateFormat formatter = new SimpleDateFormat ("yyyy/MM/dd HH:mm:ss", Locale.US);
        formatter.setTimeZone( TimeZone.getDefault());
        return formatter.format(date);
    }
}
