package com.example.bhumicloud.gisapplications.Activity;

import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.widget.Toast;

import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.RecordValues;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.util.ActivityRequestCodes;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.example.bhumicloud.gisapplications.util.ApplicationUtils;
import com.example.bhumicloud.gisapplications.util.GISCamcorderProfile;
import com.example.bhumicloud.gisapplications.util.RoundedTransformation;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

public class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener, Target {
    public static final String ARG_ACCOUNT = "GIS:arg:account";
    private static final String KEY_ABOUT_VERSION = "about:version";
    private static final String KEY_ACCOUNT_LOG_OUT = "account:logout";
    private static final String KEY_ACCOUNT_USERNAME = "account:username";
    private Account mAccount;
    private SettingsFragmentListener mListener;
    private ProgressDialog mProgressDialog;

    public interface SettingsFragmentListener {
        void onAccountChanged(Account account);
    }

    public static SettingsFragment getInstance(Account account) {
        Bundle args = new Bundle ();
        SettingsFragment fragment = new SettingsFragment();
        args.putParcelable(ARG_ACCOUNT, account);
        fragment.setArguments(args);
        fragment.setRetainInstance(true);
        return fragment;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SettingsFragmentListener) {
            this.mListener = (SettingsFragmentListener) context;
            return;
            }
        throw new RuntimeException (context.toString() + " must implement " + SettingsFragmentListener.class.getSimpleName());
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mAccount = (Account) getArguments().getParcelable(ARG_ACCOUNT);
        PreferenceManager prefMgr = getPreferenceManager();
        prefMgr.setSharedPreferencesName(PatronSettings.getAccountPreferencesName(this.mAccount));
        prefMgr.setSharedPreferencesMode(0);
        addPreferencesFromResource(R.xml.settings);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case ActivityRequestCodes.REQUEST_PROFILE_EDITOR:
                    this.mAccount = (Account) data.getParcelableExtra(ARG_ACCOUNT);
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        reloadSummaries();

    }

    public void onPause() {
        super.onPause();
        if (this.mProgressDialog != null) {
            hideProgressDialog();
        }
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    public boolean onPreferenceTreeClick(PreferenceScreen screen, @NonNull Preference preference) {
        String key = preference.getKey();
        if (KEY_ACCOUNT_USERNAME.equals(key)) {
           // startActivityForResult(new Intent(getActivity(), ProfileEditorActivity.class), ActivityRequestCodes.REQUEST_PROFILE_EDITOR);
            return true;
        }  else if (KEY_ACCOUNT_LOG_OUT.equals(key)) {
            onLogOutSelected();
            return true;
        } else {
          //  OnboardingActivity.start(getActivity());
            return true;
        }
    }

    private void reloadSummaries() {
        Resources res = getResources ();
        Context context = getActivity ();
        if (this.mAccount != null) {
            Preference username = findPreference(KEY_ACCOUNT_USERNAME);
            username.setIcon(R.drawable.ic_person_dark);
            username.setTitle(this.mAccount.getUserName());
        }

    }
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        reloadSummaries();

    }

    public void onBitmapLoaded(Bitmap bitmap, LoadedFrom from) {
        if (isAdded()) {
            findPreference(KEY_ACCOUNT_USERNAME).setIcon(new BitmapDrawable (getResources(), bitmap));
        }
    }

    public void onBitmapFailed(Drawable errorDrawable) {
    }

    public void onPrepareLoad(Drawable placeHolderDrawable) {
    }

    public void toggleAccountSettings(boolean enabled) {
        findPreference(KEY_ACCOUNT_LOG_OUT).setEnabled(enabled);
    }

    private void showProgressDialog(int title) {
        this.mProgressDialog = new ProgressDialog (getActivity());
        this.mProgressDialog.setTitle(title);
        this.mProgressDialog.setIndeterminate(true);
        this.mProgressDialog.setCancelable(false);
        this.mProgressDialog.show();
    }

    private void hideProgressDialog() {
        this.mProgressDialog.dismiss();
        this.mProgressDialog = null;
    }


    private void onLogOutSelected() {
        Builder b = new Builder (getActivity());
        b.setTitle(R.string.logout_confirmation_title);
        b.setMessage(R.string.logout_confirmation_message);
        b.setPositiveButton ( R.string.logout_title, new OnClickListener () {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    SettingsFragment.this.onLogOutConfirmed();
            }
        } );
        b.show();
    }

    private void onLogOutConfirmed() {
        Context context = getActivity();
        Account.setActiveAccount(context, null);
        Intent intent = new Intent (context, LaunchActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        getActivity().finish();
    }
}
