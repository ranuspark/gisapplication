package com.example.bhumicloud.gisapplications.util;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class FileBasedStateStorage implements Parcelable {
    public static final String CACHE_FILE_DIRECTORY = "fileBasedStateStorage";
    public static final Creator<FileBasedStateStorage> CREATOR = new C12271();
    private Context mContext;
    private HashMap<String, Parcelable> mKeysValues = new HashMap();

    static class C12271 implements Creator<FileBasedStateStorage> {
        C12271() {
        }

        public FileBasedStateStorage createFromParcel(Parcel source) {
            return new FileBasedStateStorage(source);
        }

        public FileBasedStateStorage[] newArray(int size) {
            return new FileBasedStateStorage[size];
        }
    }

    public FileBasedStateStorage(Context context) {
        this.mContext = context;
    }

    public FileBasedStateStorage(Parcel in) {
        int success = in.readInt();
        if (success != 1) {
            GISLogger.log("success is: " + success + ". Probably unable to write to file");
            return;
        }
        int bytesLength = in.readInt();
        byte[] dataAsBytes = new byte[bytesLength];
        URI savedStateFileURI = (URI) in.readSerializable();
        File savedStateFile = new File(savedStateFileURI);
        Parcel parcel = Parcel.obtain();
        try {
            FileInputStream fis = new FileInputStream(savedStateFile);
            fis.read(dataAsBytes, 0, bytesLength);
            fis.close();
            savedStateFile.delete();
            parcel.unmarshall(dataAsBytes, 0, bytesLength);
            parcel.setDataPosition(0);
            this.mKeysValues = parcel.readHashMap(getClass().getClassLoader());
        } catch (IOException e) {
            GISLogger.log("Wasn't able to read bytes from file.");
        } catch (Throwable e2) {
            GISLogger.log("caught runtime exception");
            Map meta = new HashMap();
            meta.put("success", String.valueOf(success));
            meta.put("bytesLength", String.valueOf(bytesLength));
            meta.put("savedStateFileUri", savedStateFileURI.getPath());
            meta.put("dataAsBytes", new String(dataAsBytes));
            GISLogger.log(e2, meta);
        }
        parcel.recycle();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        Parcel parcel = Parcel.obtain();
        parcel.writeMap(this.mKeysValues);
        byte[] dataAsBytes = parcel.marshall();
        File cacheFile = new File(this.mContext.getDir(CACHE_FILE_DIRECTORY, 0), UUID.randomUUID() + ".cache");
        try {
            FileOutputStream fos = new FileOutputStream(cacheFile);
            fos.write(dataAsBytes);
            fos.flush();
            fos.close();
            URI savedStateFileURI = cacheFile.toURI();
            dest.writeInt(1);
            dest.writeInt(dataAsBytes.length);
            dest.writeSerializable(savedStateFileURI);
        } catch (IOException e) {
            dest.writeInt(0);
        }
        parcel.recycle();
    }

    public void store(String key, Parcelable value) {
        this.mKeysValues.put(key, value);
    }

    public Parcelable get(String key) {
        return (Parcelable) this.mKeysValues.get(key);
    }
}

