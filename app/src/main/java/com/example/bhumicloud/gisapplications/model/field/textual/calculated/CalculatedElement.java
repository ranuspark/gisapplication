package com.example.bhumicloud.gisapplications.model.field.textual.calculated;

import android.os.Parcel;
import android.os.Parcelable.Creator;

import com.example.bhumicloud.gisapplications.model.DisplayOptions;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.model.field.ElementContainer;
import com.example.bhumicloud.gisapplications.model.field.SectionElement;
import com.example.bhumicloud.gisapplications.model.field.repeatable.RepeatableElement;
import com.example.bhumicloud.gisapplications.model.field.textual.TextualElement;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CalculatedElement extends TextualElement {
    public static final Creator<CalculatedElement> CREATOR = new Creator<CalculatedElement>() {
        public CalculatedElement createFromParcel(Parcel source) {
            return new CalculatedElement(source);
        }

        public CalculatedElement[] newArray(int size) {
            return new CalculatedElement[size];
        }
    };
    private static final String JSON_DISPLAY = "display";
    private static final String JSON_EXPRESSION = "expression";
    private DisplayOptions mDisplay;
    private String mExpression;

    public CalculatedElement(Element parent, Map json) {
        super(parent, json);
    }

    private CalculatedElement(Parcel parcel) {
        super(parcel);
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = super.toJSON();
        json.put(JSON_EXPRESSION, this.mExpression);
        json.put(JSON_DISPLAY, this.mDisplay.toJSON());
        return json;
    }

    public String getType() {
        return Element.TYPE_CALCULATION;
    }

    public String getExpression() {
        return this.mExpression;
    }

    public DisplayOptions getDisplay() {
        return this.mDisplay;
    }

    public void setJSONAttributes(Map json) {
        super.setJSONAttributes(json);
        this.mExpression = JSONUtils.getString(json, JSON_EXPRESSION);
        this.mDisplay = new DisplayOptions(JSONUtils.getHashMap(json, JSON_DISPLAY));
    }

    public static ElementContainer getCalculatedElementRoot(Form form, ElementContainer container) {
        if (container instanceof SectionElement) {
            return getCalculatedElementRoot(form, (ElementContainer) form.getParentElement((Element) container));
        }
        if (container instanceof RepeatableElement) {
            return container;
        }
        return form;
    }

    public static List<CalculatedElement> getCalculatedElements(ElementContainer container) {
        ArrayList<CalculatedElement> elements = new ArrayList();
        Iterator it = container.getElements().iterator();
        while (it.hasNext()) {
            Element element = (Element) it.next();
            if (element instanceof CalculatedElement) {
                elements.add((CalculatedElement) element);
            } else if (element instanceof SectionElement) {
                elements.addAll(getCalculatedElements((SectionElement) element));
            }
        }
        return elements;
    }
}

