package com.example.bhumicloud.gisapplications.View.media.video;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import  com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import  com.example.bhumicloud.gisapplications.util.VideoThumbnailTask;
import  com.example.bhumicloud.gisapplications.View.media.MediaImageView;

public class VideoImageView extends MediaImageView {
    private VideoThumbnailTask mThumbnailTask;
    private String mVideoPath;

    public VideoImageView(Context context) {
        super(context);
    }

    public VideoImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VideoImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setImage(Attachment attachment) {
        this.mVideoPath = attachment.getFileOne().getAbsolutePath();
        onRenderBackgroundImage();
    }

    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);
        onRenderBackgroundImage();
    }

    private void onRenderBackgroundImage() {
        if (this.mThumbnailTask != null) {
            this.mThumbnailTask.cancel(true);
            this.mThumbnailTask = null;
        }
        if (!TextUtils.isEmpty(this.mVideoPath)) {
            this.mThumbnailTask = new VideoThumbnailTask(this.mVideoPath, getWidth(), getHeight(), this);
            this.mThumbnailTask.execute(new Void[0]);
        }
    }
}
