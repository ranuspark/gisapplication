package com.example.bhumicloud.gisapplications.model.field.attachment;

import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.HashMap;
import java.util.Map;

public abstract class MediaItemValue {
    private static final String JSON_CAPTION = "caption";
    private String mCaption;
    protected final String mMediaID;

    protected abstract boolean canEqual(Object obj);

    protected abstract String getIDFieldTitle();

    public MediaItemValue(Attachment attachment) {
        this.mMediaID = attachment.getRemoteID();
    }

    public MediaItemValue(Map attributes) {
        this.mMediaID = JSONUtils.getString(attributes, getIDFieldTitle());
        this.mCaption = JSONUtils.getString(attributes, "caption");
    }

    public String getMediaID() {
        return this.mMediaID;
    }

    public String getCaption() {
        return this.mCaption;
    }

    public void setCaption(String caption) {
        this.mCaption = caption;
    }

    public Attachment getAttachment() {
        return !TextUtils.isEmpty(this.mMediaID) ? Attachment.findByRemoteID(this.mMediaID) : null;
    }

    public HashMap<String, Object> toJSON() {
        HashMap<String, Object> json = new HashMap();
        json.put(getIDFieldTitle(), this.mMediaID);
        json.put("caption", this.mCaption);
        return json;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof MediaItemValue)) {
            return false;
        }
        MediaItemValue aMediaItemValue = (MediaItemValue) object;
        if (aMediaItemValue.canEqual(this)) {
            return TextUtils.equals(getMediaID(), aMediaItemValue.getMediaID());
        }
        return false;
    }

    public int hashCode() {
        return this.mMediaID != null ? this.mMediaID.hashCode() : 0;
    }
}
