CREATE TABLE IF NOT EXISTS ClassificationSets (
    _id INTEGER PRIMARY KEY AUTOINCREMENT,
    remote_id TEXT,
    account_id INTEGER,
    name TEXT,
    description TEXT,
    items TEXT
);
CREATE INDEX IF NOT EXISTS classification_sets_account_id_idx ON ClassificationSets (account_id);
CREATE INDEX IF NOT EXISTS classification_sets_account_id_remote_id_idx ON ClassificationSets (account_id, remote_id);