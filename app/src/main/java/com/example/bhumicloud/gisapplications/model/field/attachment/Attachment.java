package com.example.bhumicloud.gisapplications.model.field.attachment;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.dossier.PersistentObject;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.field.attachment.audio.Audio;
import com.example.bhumicloud.gisapplications.model.field.attachment.photo.Photo;
import com.example.bhumicloud.gisapplications.model.field.attachment.signature.Signature;
import com.example.bhumicloud.gisapplications.model.field.attachment.video.Video;
import com.example.bhumicloud.gisapplications.util.CursorUtils;
import com.example.bhumicloud.gisapplications.util.FileUtils;
import com.example.bhumicloud.gisapplications.util.FileUtils.GISDirectoryType;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public abstract class Attachment extends PersistentObject {
    public static final String COLUMN_ACCOUNT_ID = "account_id";
    public static final String COLUMN_LOCAL_URI = "local_uri";
    public static final String COLUMN_PATH_ONE = "path_one";
    public static final String COLUMN_PATH_TWO = "path_two";
    public static final String COLUMN_RECORD_ID = "record_id";
    public static final String COLUMN_REMOTE_ID = "remote_id";
    public static final String COLUMN_THUMBNAIL_PATH = "thumbnail_path";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_UPLOADED = "uploaded";
    public static final String TABLE_NAME = "attachments";
    public static final String TYPE_AUDIO = "audio";
    public static final String TYPE_PHOTO = "photo";
    public static final String TYPE_SIGNATURE = "signature";
    public static final String TYPE_VIDEO = "video";
    private final long mAccountID;
    private String mPathOne;
    private String mPathTwo;
    private String mRecordID;
    private final String mRemoteID;
    private String mThumbnailPath;
    private boolean mUploaded;

    public enum Type {
        PHOTO,
        VIDEO,
        SIGNATURE,
        AUDIO;

        public static Type valueOf(int type) {
            switch (type) {
                case 1:
                    return VIDEO;
                case 2:
                    return SIGNATURE;
                case 3:
                    return AUDIO;
                default:
                    return PHOTO;
            }
        }
    }

    public abstract boolean canEqual(Object obj);

    public abstract File generateFile(Context context, String str);

    public abstract Type getType();

    public static File getStorageDirectory(Context context) {
        return FileUtils.getGisDataDir(context, GISDirectoryType.attachments);
    }

    public static Attachment findByRemoteID(String remoteID) {
        String[] subArgs = new String[]{remoteID};
        Attachment result = null;
        Cursor c = GIS.getDatabase().query(TABLE_NAME, null, "remote_id = ?", subArgs, null, null, null);
        if (c != null) {
            c.moveToFirst();
            if (!c.isAfterLast()) {
                result = getTypedInstance(c);
            }
            c.close();
        }
        return result;
    }

    public static List<Attachment> getAttachments(Account account, Record record, boolean includeUploaded, String type) {
        List<Attachment> attachments = new ArrayList();
        List<String> predicates = new ArrayList();
        List<String> paramsList = new ArrayList();
        if (account != null) {
            predicates.add("account_id = ?");
            paramsList.add(String.valueOf(account.getRowID()));
        }
        if (record != null) {
            predicates.add("record_id = ?");
            paramsList.add(record.getUniqueID());
        } else {
            predicates.add("record_id IS NOT NULL");
        }
        if (!TextUtils.isEmpty(type)) {
            predicates.add("type = ?");
            paramsList.add(type);
        }
        if (!includeUploaded) {
            predicates.add("uploaded = 0");
        }
        Cursor c = GIS.getDatabase().query(TABLE_NAME, null, TextUtils.join(" AND ", predicates), (String[]) paramsList.toArray(new String[paramsList.size()]), null, null, null);
        if (c != null) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                attachments.add(getTypedInstance(c));
                c.moveToNext();
            }
            c.close();
        }
        return attachments;
    }

    public static Attachment getTypedInstance(Cursor cursor) {
        String type = CursorUtils.getString(cursor, "type");
        if ("photo".equalsIgnoreCase(type)) {
            return new Photo(cursor);
        }
        if ("video".equalsIgnoreCase(type)) {
            return new Video(cursor);
        }
        if (TYPE_SIGNATURE.equalsIgnoreCase(type)) {
            return new Signature(cursor);
        }
        if (TYPE_AUDIO.equalsIgnoreCase(type)) {
            return new Audio(cursor);
        }
        return null;
    }

    public static Attachment getTypedInstance(Type type, String remoteAttachmentID) {
        switch (type) {
            case PHOTO:
                return new Photo(remoteAttachmentID);
            case VIDEO:
                return new Video(remoteAttachmentID);
            case SIGNATURE:
                return new Signature(remoteAttachmentID);
            case AUDIO:
                return new Audio(remoteAttachmentID);
            default:
                return null;
        }
    }

    public static File generateTrackFile(Context context, String key) {
        return new File(getStorageDirectory(context), "track-" + key + ".json");
    }

    public Attachment(String key) {
        this.mRemoteID = key;
        this.mAccountID = Account.getActiveAccount().getRowID();
    }

    public Attachment(Cursor cursor) {
        super(cursor);
        this.mAccountID = CursorUtils.getLong(cursor, "account_id");
        this.mRemoteID = CursorUtils.getString(cursor, "remote_id");
        this.mRecordID = CursorUtils.getString(cursor, COLUMN_RECORD_ID);
        this.mPathOne = CursorUtils.getString(cursor, COLUMN_PATH_ONE);
        this.mPathTwo = CursorUtils.getString(cursor, COLUMN_PATH_TWO);
        this.mUploaded = CursorUtils.getBoolean(cursor, COLUMN_UPLOADED);
        this.mThumbnailPath = CursorUtils.getString(cursor, COLUMN_THUMBNAIL_PATH);
    }

    protected Attachment(Parcel parcel) {
        super(parcel);
        boolean z = true;
        this.mAccountID = parcel.readLong();
        this.mRemoteID = parcel.readString();
        this.mRecordID = parcel.readString();
        this.mPathOne = parcel.readString();
        this.mPathTwo = parcel.readString();
        if (parcel.readInt() != 1) {
            z = false;
        }
        this.mUploaded = z;
        this.mThumbnailPath = parcel.readString();
    }

    public Attachment(String key, Uri localUri, String thumbnailPath) {
        this(key);
        setPathOne(localUri, thumbnailPath);
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeLong(this.mAccountID);
        dest.writeString(getRemoteID());
        dest.writeString(this.mRecordID);
        dest.writeString(this.mPathOne);
        dest.writeString(this.mPathTwo);
        dest.writeInt(this.mUploaded ? 1 : 0);
        dest.writeString(this.mThumbnailPath);
    }

    public String getRemoteID() {
        return this.mRemoteID;
    }

    public String getRecordID() {
        return this.mRecordID;
    }

    public void setRecordID(String recordID) {
        this.mRecordID = recordID;
    }

    public void setRecord(Record record) {
        setRecordID(record.getUniqueID());
    }

    public Uri getUriOne() {
        File file = getFileOne();
        if (file != null) {
            return Uri.fromFile(file);
        }
        return null;
    }

    public File getFileOne() {
        if (TextUtils.isEmpty(this.mPathOne)) {
            return null;
        }
        return new File(this.mPathOne);
    }

    public void setPathOne(File file, String thumbnailPath) {
        if (file != null) {
            this.mPathOne = file.getAbsolutePath();
            this.mThumbnailPath = thumbnailPath;
            return;
        }
        this.mPathOne = null;
        this.mThumbnailPath = null;
    }

    public void setPathOne(Uri uri, String thumbnailPath) {
        if (uri != null) {
            this.mPathOne = uri.getPath();
            this.mThumbnailPath = thumbnailPath;
            return;
        }
        this.mPathOne = null;
        this.mThumbnailPath = null;
    }

    public void setPathOne(String path, String thumbnailPath) {
        this.mPathOne = path;
        this.mThumbnailPath = thumbnailPath;
    }

    public File getFileTwo() {
        if (TextUtils.isEmpty(this.mPathTwo)) {
            return null;
        }
        return new File(this.mPathTwo);
    }

    public void setPathTwo(String path) {
        this.mPathTwo = path;
    }

    public File getThumbnail() {
        return TextUtils.isEmpty(this.mThumbnailPath) ? null : new File(this.mThumbnailPath);
    }

    public boolean isUploaded() {
        return this.mUploaded;
    }

    public void setUploaded(boolean uploaded) {
        this.mUploaded = uploaded;
    }

    public boolean isFileOneAvailable() {
        File file = getFileOne();
        return file != null && file.exists();
    }

    public String toString() {
        String type = getClass().getSimpleName();
        return String.format("%s<%s>", new Object[]{type, getRemoteID()});
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Attachment)) {
            return false;
        }
        Attachment anAttachment = (Attachment) object;
        if (!anAttachment.canEqual(this)) {
            return false;
        }
        if (this.mAccountID == anAttachment.mAccountID && TextUtils.equals(getRemoteID(), anAttachment.getRemoteID())) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (((int) (this.mAccountID ^ (this.mAccountID >>> 32))) * 31) + (this.mRemoteID != null ? this.mRemoteID.hashCode() : 0);
    }

    protected String getTableName() {
        return TABLE_NAME;
    }

    protected ContentValues getContentValues() {
        ContentValues cv = super.getContentValues();
        cv.put("account_id", Long.valueOf(this.mAccountID));
        cv.put("remote_id", getRemoteID());
        cv.put(COLUMN_RECORD_ID, this.mRecordID);
        cv.put(COLUMN_PATH_ONE, this.mPathOne);
        cv.put(COLUMN_PATH_TWO, this.mPathTwo);
        cv.put(COLUMN_UPLOADED, Boolean.valueOf(this.mUploaded));
        cv.put("type", getType().name().toLowerCase());
        cv.put(COLUMN_THUMBNAIL_PATH, this.mThumbnailPath);
        return cv;
    }

    protected void delete(SQLiteDatabase db, Bundle options) {
        FileUtils.delete(getFileOne());
        FileUtils.delete(getFileTwo());
        FileUtils.delete(getThumbnail());
        super.delete(db, options);
    }
}
