package com.example.bhumicloud.gisapplications.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;

import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.squareup.picasso.Picasso;

import org.apache.sanselan.ImageReadException;
import org.apache.sanselan.ImageWriteException;
import org.apache.sanselan.formats.jpeg.exifRewrite.ExifRewriter;
import org.apache.sanselan.formats.tiff.TiffImageMetadata;
import org.apache.sanselan.formats.tiff.constants.ExifTagConstants;
import org.apache.sanselan.formats.tiff.constants.TagInfo;
import org.apache.sanselan.formats.tiff.constants.TiffConstants;
import org.apache.sanselan.formats.tiff.constants.TiffDirectoryConstants;
import org.apache.sanselan.formats.tiff.constants.TiffFieldTypeConstants;
import org.apache.sanselan.formats.tiff.write.TiffOutputDirectory;
import org.apache.sanselan.formats.tiff.write.TiffOutputField;
import org.apache.sanselan.formats.tiff.write.TiffOutputSet;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class ImageFileResizeTask extends AsyncTask<Void, Void, Void> {
    private static final String TAG = ImageFileResizeTask.class.getSimpleName();
    private final CompressFormat mCompressFormat;
    private final Context mContext;
    private final String mFilePath;
    private final Uri mFileUri;
    private final Location mLocation;
    private int mNewHeight;
    private int mNewWidth;
    private final int mOriginalHeight;
    private final int mOriginalWidth;
    private final String mPhotoID;

    public ImageFileResizeTask(Context context, Uri sourceUri, Location location, String photoID, String overrideQuality) {
        float largestDimension;
        float scaleFactor;
        this.mContext = context;
        this.mFileUri = sourceUri;
        this.mFilePath = sourceUri.getPath();
        this.mLocation = location;
        this.mPhotoID = photoID;
        Integer overrideDimension = null;
        if (overrideQuality != null) {
            try {
                overrideDimension = overrideQuality.equals("native") ? Integer.valueOf(0) : Integer.valueOf( Integer.parseInt(overrideQuality));
            } catch (NumberFormatException e) {
            }
        }
        Options options = new Options ();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(this.mFilePath, options);
        this.mOriginalWidth = options.outWidth;
        this.mOriginalHeight = options.outHeight;
        if (overrideDimension == null) {
            switch (PatronSettings.getPhotoCaptureQuality(this.mContext)) {
                case 1:
                    largestDimension = 1080.0f;
                    break;
                case 2:
                    largestDimension = 720.0f;
                    break;
                case 3:
                    largestDimension = 480.0f;
                    break;
                default:
                    largestDimension = 0.0f;
                    break;
            }
        }
        overrideDimension = Integer.valueOf(0);
        largestDimension =  (float) overrideDimension.intValue();
        if (largestDimension == 0.0f) {
            scaleFactor = 1.0f;
        } else if (this.mOriginalHeight >= this.mOriginalWidth) {
            scaleFactor = largestDimension / ((float) this.mOriginalHeight);
        } else {
            scaleFactor = largestDimension / ((float) this.mOriginalWidth);
        }
        this.mNewHeight = Math.round(((float) this.mOriginalHeight) * scaleFactor);
        this.mNewWidth = Math.round(((float) this.mOriginalWidth) * scaleFactor);
        this.mCompressFormat = BitmapUtils.getCompressFormat(this.mFilePath, options);
    }

    protected Void doInBackground(Void... args) {
        Throwable e;
        File sourceFile = new File(this.mFilePath);
        TiffOutputSet exif = null;
        if (FileUtils.isJpeg(sourceFile)) {
            exif = getSanselanOutputSet(sourceFile, 73);
        }
        int orientation = EXIFUtils.getOrientation(this.mFilePath);
        if (!(this.mNewHeight == this.mOriginalHeight && this.mNewWidth == this.mOriginalWidth && orientation == 0)) {
            if (orientation == 6 || orientation == 8) {
                int tmpNewHeight = this.mNewHeight;
                this.mNewHeight = this.mNewWidth;
                this.mNewWidth = tmpNewHeight;
            }
            Bitmap bitmap = null;
            try {
                bitmap = Picasso.with(this.mContext).load(this.mFileUri).resize(this.mNewWidth, this.mNewHeight).onlyScaleDown().get();
            } catch (IOException e1) {
                e1.printStackTrace ();
            }
            this.mNewHeight = bitmap.getHeight();
            this.mNewWidth = bitmap.getWidth();
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream (this.mFilePath);
                bitmap.compress(this.mCompressFormat, 100, fos);
                fos.close();
            } catch (FileNotFoundException e1) {
                e1.printStackTrace ();
            } catch (IOException e1) {
                e1.printStackTrace ();
            }

            if (exif != null) {
                exif.removeField(TiffConstants.EXIF_TAG_ORIENTATION);
                exif.removeField(TiffConstants.TIFF_TAG_ORIENTATION);
            }
        }
        if (exif != null) {
            try {
                writeExifInformation(exif);
                writeExifLocation(exif, this.mLocation);
                saveExifToFile(sourceFile, exif);
            } catch (IOException e3) {
                e = e3;
                GISLogger.log(e);
                return null;
            } catch (ImageWriteException e4) {
                e = e4;
                GISLogger.log(e);
                return null;
            } catch (ImageReadException e5) {
                e = e5;
                GISLogger.log(e);
                return null;
            }
        }
        return null;
    }


    private TiffOutputSet getSanselanOutputSet(File jpegImageFile, int defaultByteOrder) {
        Throwable e;
        try {
            TiffImageMetadata metadata = EXIFUtils.getImageMetadata(jpegImageFile);
            TiffOutputSet outputSet = metadata == null ? null : metadata.getOutputSet();
            if (outputSet != null) {
                return outputSet;
            }
            if (metadata != null) {
                defaultByteOrder = metadata.contents.header.byteOrder;
            }
            return new TiffOutputSet (defaultByteOrder);
        } catch (IOException e2) {
            e = e2;
            GISLogger.log(e);
            return null;
        } catch (ImageWriteException e3) {
            e = e3;
            GISLogger.log(e);
            return null;
        } catch (ImageReadException e4) {
            e = e4;
            GISLogger.log(e);
            return null;
        }
    }

    private void writeExifInformation(TiffOutputSet exif) throws ImageWriteException {
        TiffOutputField field;
        TiffOutputDirectory exifDirectory = exif.getOrCreateExifDirectory();
        if (exifDirectory != null) {
            SimpleDateFormat formatter;
            field = TiffOutputField.create( ExifTagConstants.EXIF_TAG_EXIF_IMAGE_WIDTH, exif.byteOrder, Integer.valueOf(this.mNewWidth));
            exifDirectory.removeField( ExifTagConstants.EXIF_TAG_EXIF_IMAGE_WIDTH);
            exifDirectory.add(field);
            field = TiffOutputField.create( ExifTagConstants.EXIF_TAG_EXIF_IMAGE_LENGTH, exif.byteOrder, Integer.valueOf(this.mNewHeight));
            exifDirectory.removeField( ExifTagConstants.EXIF_TAG_EXIF_IMAGE_LENGTH);
            exifDirectory.add(field);
            if (exifDirectory.findField( ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL) == null) {
                formatter = new SimpleDateFormat ("yyyy:MM:dd HH:mm:ss", Locale.US);
                formatter.setTimeZone( TimeZone.getDefault());
                field = TiffOutputField.create( ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL, exif.byteOrder, formatter.format(new Date ()));
                exifDirectory.removeField( ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL);
                exifDirectory.add(field);
            }
            if (exifDirectory.findField( ExifTagConstants.EXIF_TAG_CREATE_DATE) == null) {
                formatter = new SimpleDateFormat ("yyyy:MM:dd HH:mm:ss", Locale.US);
                formatter.setTimeZone( TimeZone.getDefault());
                field = TiffOutputField.create( ExifTagConstants.EXIF_TAG_CREATE_DATE, exif.byteOrder, formatter.format(new Date ()));
                exifDirectory.removeField( ExifTagConstants.EXIF_TAG_CREATE_DATE);
                exifDirectory.add(field);
            }
        }
        TiffOutputDirectory rootDirectory = exif.getOrCreateRootDirectory();
        if (rootDirectory != null) {
            field = TiffOutputField.create( ExifTagConstants.EXIF_TAG_IMAGE_WIDTH_IFD0, exif.byteOrder, Integer.valueOf(this.mNewWidth));
            rootDirectory.removeField( ExifTagConstants.EXIF_TAG_IMAGE_WIDTH_IFD0);
            rootDirectory.add(field);
            field = TiffOutputField.create( ExifTagConstants.EXIF_TAG_IMAGE_HEIGHT_IFD0, exif.byteOrder, Integer.valueOf(this.mNewHeight));
            rootDirectory.removeField( ExifTagConstants.EXIF_TAG_IMAGE_HEIGHT_IFD0);
            rootDirectory.add(field);
            field = TiffOutputField.create( ExifTagConstants.EXIF_TAG_ARTIST, exif.byteOrder, Account.getActiveAccount().getUserID());
            rootDirectory.removeField( ExifTagConstants.EXIF_TAG_ARTIST);
            rootDirectory.add(field);
            field = TiffOutputField.create( ExifTagConstants.EXIF_TAG_SOFTWARE, exif.byteOrder, ApplicationUtils.getUserAgentString(this.mContext));
            rootDirectory.removeField( ExifTagConstants.EXIF_TAG_SOFTWARE);
            rootDirectory.add(field);
            field = TiffOutputField.create( ExifTagConstants.EXIF_TAG_IMAGE_DESCRIPTION, exif.byteOrder, this.mPhotoID);
            rootDirectory.removeField( ExifTagConstants.EXIF_TAG_IMAGE_DESCRIPTION);
            rootDirectory.add(field);
            field = TiffOutputField.create( ExifTagConstants.EXIF_TAG_IMAGE_UNIQUE_ID, exif.byteOrder, this.mPhotoID);
            rootDirectory.removeField( ExifTagConstants.EXIF_TAG_IMAGE_UNIQUE_ID);
            rootDirectory.add(field);
            if (rootDirectory.findField( ExifTagConstants.EXIF_TAG_MAKE) == null) {
                field = TiffOutputField.create( ExifTagConstants.EXIF_TAG_MAKE, exif.byteOrder, Build.MANUFACTURER);
                rootDirectory.removeField( ExifTagConstants.EXIF_TAG_MAKE);
                rootDirectory.add(field);
            }
            if (rootDirectory.findField( ExifTagConstants.EXIF_TAG_MODEL) == null) {
                field = TiffOutputField.create( ExifTagConstants.EXIF_TAG_MODEL, exif.byteOrder, Build.MODEL);
                rootDirectory.removeField( ExifTagConstants.EXIF_TAG_MODEL);
                rootDirectory.add(field);
            }
            if (rootDirectory.findField( ExifTagConstants.EXIF_TAG_DOCUMENT_NAME) == null) {
                field = TiffOutputField.create( ExifTagConstants.EXIF_TAG_DOCUMENT_NAME, exif.byteOrder, DateUtils.getTimestampFormatter().format(new Date ()));
                rootDirectory.removeField( ExifTagConstants.EXIF_TAG_DOCUMENT_NAME);
                rootDirectory.add(field);
            }
        }
    }

    private void writeExifLocation(TiffOutputSet exif, Location location) {
        if (location != null) {
            try {
                boolean hasLocation;
                double longitude;
                double latitude;
                String longitudeRef = null;
                String latitudeRef= null;
                TiffOutputField field;
                double altitude;
                int altitudeRef = 0;
                TagInfo altitudeRefTag;
                TagInfo altitudeTag;
                double accuracy;
                TagInfo accuracyTag;
                TagInfo dopTag;
                TiffOutputDirectory gps = exif.getOrCreateGPSDirectory();
                if (gps.findField( TiffConstants.GPS_TAG_GPS_LATITUDE_REF) != null) {
                    if (gps.findField( TiffConstants.GPS_TAG_GPS_LATITUDE) != null) {
                        if (gps.findField( TiffConstants.GPS_TAG_GPS_LONGITUDE_REF) != null) {
                            if (gps.findField( TiffConstants.GPS_TAG_GPS_LONGITUDE) != null) {
                                hasLocation = true;
                                if (!hasLocation) {
                                    longitude = location.getLongitude();
                                    latitude = location.getLatitude();
                                    longitudeRef = longitude >= 0.0d ? "W" : "E";
                                    longitude = Math.abs(longitude);
                                    latitudeRef = latitude >= 0.0d ? "S" : "N";
                                    latitude = Math.abs(latitude);
                                    field = TiffOutputField.create( TiffConstants.GPS_TAG_GPS_LONGITUDE_REF, exif.byteOrder, longitudeRef);
                                    gps.removeField( TiffConstants.GPS_TAG_GPS_LONGITUDE_REF);
                                    gps.add(field);
                                    field = TiffOutputField.create( TiffConstants.GPS_TAG_GPS_LATITUDE_REF, exif.byteOrder, latitudeRef);
                                    gps.removeField( TiffConstants.GPS_TAG_GPS_LATITUDE_REF);
                                    gps.add(field);
                                    field = TiffOutputField.create( TiffConstants.GPS_TAG_GPS_LONGITUDE, exif.byteOrder, toDMS(longitude));
                                    gps.removeField( TiffConstants.GPS_TAG_GPS_LONGITUDE);
                                    gps.add(field);
                                    field = TiffOutputField.create( TiffConstants.GPS_TAG_GPS_LATITUDE, exif.byteOrder, toDMS(latitude));
                                    gps.removeField( TiffConstants.GPS_TAG_GPS_LATITUDE);
                                    gps.add(field);
                                }
                                if (location.hasAltitude()) {
                                    altitude = location.getAltitude();
                                    altitudeRef = altitude >= 0.0d ? 1 : 0;
                                    altitude = Math.abs(altitude);
                                    altitudeRefTag = new TagInfo ("GPS Altitude Ref", 5, TiffFieldTypeConstants.FIELD_TYPE_DESCRIPTION_BYTE, 1, TiffDirectoryConstants.EXIF_DIRECTORY_GPS);
                                    field = TiffOutputField.create(altitudeRefTag, exif.byteOrder, Byte.valueOf((byte) altitudeRef));
                                    gps.removeField(altitudeRefTag);
                                    gps.add(field);
                                    altitudeTag = new TagInfo ("GPS Altitude", 6, TiffFieldTypeConstants.FIELD_TYPE_DESCRIPTION_RATIONAL, 1, TiffDirectoryConstants.EXIF_DIRECTORY_GPS);
                                    field = TiffOutputField.create(altitudeTag, exif.byteOrder, new Double[]{Double.valueOf(altitude)});
                                    gps.removeField(altitudeTag);
                                    gps.add(field);
                                }
                                if (location.hasAccuracy()) {
                                    accuracy = (double) location.getAccuracy();
                                    accuracyTag = new TagInfo ("GPS H Positioning Error", 31, TiffFieldTypeConstants.FIELD_TYPE_DESCRIPTION_RATIONAL, 1, TiffDirectoryConstants.EXIF_DIRECTORY_GPS);
                                    field = TiffOutputField.create(accuracyTag, exif.byteOrder, new Double[]{Double.valueOf(accuracy)});
                                    gps.removeField(accuracyTag);
                                    gps.add(field);
                                    dopTag = new TagInfo ("GPS DOP", 11, TiffFieldTypeConstants.FIELD_TYPE_DESCRIPTION_RATIONAL, 1, TiffDirectoryConstants.EXIF_DIRECTORY_GPS);
                                    field = TiffOutputField.create(dopTag, exif.byteOrder, new Double[]{Double.valueOf(accuracy)});
                                    gps.removeField(dopTag);
                                    gps.add(field);
                                }
                            }
                        }
                    }
                }
                hasLocation = false;
                if (hasLocation) {
                    longitude = location.getLongitude();
                    latitude = location.getLatitude();
                    if (longitude >= 0.0d) {
                    }
                    longitude = Math.abs(longitude);
                    if (latitude >= 0.0d) {
                    }
                    latitude = Math.abs(latitude);
                    field = TiffOutputField.create( TiffConstants.GPS_TAG_GPS_LONGITUDE_REF, exif.byteOrder, longitudeRef);
                    gps.removeField( TiffConstants.GPS_TAG_GPS_LONGITUDE_REF);
                    gps.add(field);
                    field = TiffOutputField.create( TiffConstants.GPS_TAG_GPS_LATITUDE_REF, exif.byteOrder, latitudeRef);
                    gps.removeField( TiffConstants.GPS_TAG_GPS_LATITUDE_REF);
                    gps.add(field);
                    field = TiffOutputField.create( TiffConstants.GPS_TAG_GPS_LONGITUDE, exif.byteOrder, toDMS(longitude));
                    gps.removeField( TiffConstants.GPS_TAG_GPS_LONGITUDE);
                    gps.add(field);
                    field = TiffOutputField.create( TiffConstants.GPS_TAG_GPS_LATITUDE, exif.byteOrder, toDMS(latitude));
                    gps.removeField( TiffConstants.GPS_TAG_GPS_LATITUDE);
                    gps.add(field);
                }
                if (location.hasAltitude()) {
                    altitude = location.getAltitude();
                    if (altitude >= 0.0d) {
                    }
                    altitude = Math.abs(altitude);
                    altitudeRefTag = new TagInfo ("GPS Altitude Ref", 5, TiffFieldTypeConstants.FIELD_TYPE_DESCRIPTION_BYTE, 1, TiffDirectoryConstants.EXIF_DIRECTORY_GPS);
                    field = TiffOutputField.create(altitudeRefTag, exif.byteOrder, Byte.valueOf((byte) altitudeRef));
                    gps.removeField(altitudeRefTag);
                    gps.add(field);
                    altitudeTag = new TagInfo ("GPS Altitude", 6, TiffFieldTypeConstants.FIELD_TYPE_DESCRIPTION_RATIONAL, 1, TiffDirectoryConstants.EXIF_DIRECTORY_GPS);
                    field = TiffOutputField.create(altitudeTag, exif.byteOrder, new Double[]{Double.valueOf(altitude)});
                    gps.removeField(altitudeTag);
                    gps.add(field);
                }
                if (location.hasAccuracy()) {
                    accuracy = (double) location.getAccuracy();
                    accuracyTag = new TagInfo ("GPS H Positioning Error", 31, TiffFieldTypeConstants.FIELD_TYPE_DESCRIPTION_RATIONAL, 1, TiffDirectoryConstants.EXIF_DIRECTORY_GPS);
                    field = TiffOutputField.create(accuracyTag, exif.byteOrder, new Double[]{Double.valueOf(accuracy)});
                    gps.removeField(accuracyTag);
                    gps.add(field);
                    dopTag = new TagInfo ("GPS DOP", 11, TiffFieldTypeConstants.FIELD_TYPE_DESCRIPTION_RATIONAL, 1, TiffDirectoryConstants.EXIF_DIRECTORY_GPS);
                    field = TiffOutputField.create(dopTag, exif.byteOrder, new Double[]{Double.valueOf(accuracy)});
                    gps.removeField(dopTag);
                    gps.add(field);
                }
            } catch (Throwable e) {
                GISLogger.log(e);
            }
        }
    }

    private Double[] toDMS(double input) {
        double degrees = (double) ((long) input);
        double remainder = (input % 1.0d) * 60.0d;
        double minutes = (double) ((long) remainder);
        double seconds = (remainder % 1.0d) * 60.0d;
        return new Double[]{Double.valueOf(degrees), Double.valueOf(minutes), Double.valueOf(seconds)};
    }

    private void saveExifToFile(File imageFile, TiffOutputSet exif) throws IOException, ImageWriteException, ImageReadException {
        File tempFile = new File (imageFile.getAbsolutePath() + ".tmp");
        BufferedOutputStream tempStream = new BufferedOutputStream (new FileOutputStream (tempFile));
        new ExifRewriter ().updateExifMetadataLossless(imageFile, tempStream, exif);
        tempStream.close();
        if (imageFile.delete()) {
            tempFile.renameTo(imageFile);
        }
    }
}
