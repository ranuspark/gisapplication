package com.example.bhumicloud.gisapplications.util;

import android.media.ExifInterface;
import android.text.TextUtils;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.example.bhumicloud.gisapplications.model.Record;

import org.apache.sanselan.ImageReadException;
import org.apache.sanselan.Sanselan;
import org.apache.sanselan.common.RationalNumber;
import org.apache.sanselan.formats.jpeg.JpegImageMetadata;
import org.apache.sanselan.formats.tiff.TiffDirectory;
import org.apache.sanselan.formats.tiff.TiffField;
import org.apache.sanselan.formats.tiff.TiffImageMetadata;
import org.apache.sanselan.formats.tiff.TiffImageMetadata.GPSInfo;
import org.apache.sanselan.formats.tiff.constants.ExifTagConstants;
import org.apache.sanselan.formats.tiff.constants.GPSTagConstants;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.fabric.sdk.android.services.settings.SettingsJsonConstants;

public class EXIFUtils {
    private static IOException e2;
    private static ImageReadException e22;

    public static TiffImageMetadata getImageMetadata(File jpegImageFile) throws IOException, ImageReadException {
        JpegImageMetadata jpegMetadata = (JpegImageMetadata) Sanselan.getMetadata(jpegImageFile);
        if (jpegMetadata != null) {
            return jpegMetadata.getExif();
        }
        return null;
    }

    public static HashMap<String, Object> getRawEXIF(File image) {
        Throwable e;
        HashMap<String, Object> exif = new HashMap ();
        try {
            TiffImageMetadata sourceExif = getImageMetadata(image);
            if (sourceExif != null) {
                int i;
                TiffField field;
                String tagName;
                Object value;
                try {
                    List fields = sourceExif.getAllFields();
                    for (i = 0; i < fields.size(); i++) {
                        field = (TiffField) fields.get(i);
                        tagName = field.getTagName();
                        value = convertExifField(field);
                        if (value != null) {
                            exif.put(tagName, value);
                        }
                    }
                } catch (Throwable e2) {
                    GISLogger.log(e2);
                }
                TiffDirectory gpsDirectory = sourceExif.findDirectory(-3);
                if (gpsDirectory != null) {
                    try {
                        GPSInfo gps = sourceExif.getGPS();
                        if (gps != null) {
                            exif.put("GPS Latitude Value", Double.valueOf(gps.getLatitudeAsDegreesNorth()));
                            exif.put("GPS Longitude Value", Double.valueOf(gps.getLongitudeAsDegreesEast()));
                        }
                    } catch (Throwable e22) {
                        GISLogger.log(e22);
                    }
                    for (i = 0; i < gpsDirectory.entries.size(); i++) {
                        field = (TiffField) gpsDirectory.entries.get(i);
                        if (field.tag < GPSTagConstants.ALL_GPS_TAGS.length) {
                            tagName = GPSTagConstants.ALL_GPS_TAGS[field.tag].name;
                            value = convertExifField(field);
                            if (value != null) {
                                exif.put(tagName, value);
                            }
                        } else if (field.tag == 31) {
                            value = convertExifField(field);
                            if (value != null) {
                                exif.put("GPS H Positioning Error", value);
                            }
                        }
                    }
                }
            }
        } catch (IOException e322) {
            e2 = e322;
        } catch (ImageReadException e4) {
            e22=e4;
        }
        return exif;
       // GISLogger.log(e2);
       // return exif;
    }

    public static HashMap<String, Object> getEXIF(File image) {
        HashMap<String, Object> raw = getRawEXIF(image);
        HashMap<String, Object> exif = new HashMap ();
        if (raw != null) {
            if (raw.containsKey("GPS Latitude Value")) {
                exif.put(Record.COLUMN_LATITUDE, raw.get("GPS Latitude Value"));
            }
            if (raw.containsKey("GPS Longitude Value")) {
                exif.put(Record.COLUMN_LONGITUDE, raw.get("GPS Longitude Value"));
            }
            if (raw.containsKey("GPS H Positioning Error")) {
                exif.put(Record.COLUMN_ACCURACY, raw.get("GPS H Positioning Error"));
            }
            if (raw.containsKey(GPSTagConstants.GPS_TAG_GPS_ALTITUDE.name)) {
                Double altitude = (Double) raw.get(GPSTagConstants.GPS_TAG_GPS_ALTITUDE.name);
                if (((Number) raw.get(GPSTagConstants.GPS_TAG_GPS_ALTITUDE_REF.name)).intValue() == 1) {
                    altitude = Double.valueOf(-altitude.doubleValue());
                }
                exif.put(Record.COLUMN_ALTITUDE, altitude);
            }
            if (raw.containsKey(GPSTagConstants.GPS_TAG_GPS_IMG_DIRECTION.name)) {
                exif.put("direction", raw.get(GPSTagConstants.GPS_TAG_GPS_IMG_DIRECTION.name));
            }
            if (raw.containsKey(ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL.name)) {
                String[] parts = TextUtils.split((String) raw.get( ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL.name), String.valueOf ( MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR ) );
                if (parts.length > 1) {
                    exif.put("timestamp", parts[0].replace(":", "/") + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + parts[1]);
                }
            }
            if (raw.containsKey(ExifTagConstants.EXIF_TAG_ORIENTATION.name)) {
                exif.put("orientation", raw.get(ExifTagConstants.EXIF_TAG_ORIENTATION.name));
            }
            if (raw.containsKey(ExifTagConstants.EXIF_TAG_EXIF_IMAGE_WIDTH.name)) {
                exif.put( SettingsJsonConstants.ICON_WIDTH_KEY, raw.get(ExifTagConstants.EXIF_TAG_EXIF_IMAGE_WIDTH.name));
            }
            if (raw.containsKey(ExifTagConstants.EXIF_TAG_EXIF_IMAGE_LENGTH.name)) {
                exif.put( SettingsJsonConstants.ICON_HEIGHT_KEY, raw.get(ExifTagConstants.EXIF_TAG_EXIF_IMAGE_LENGTH.name));
            }
        }
        return exif;
    }

    public static int getOrientation(String filePath) {
        int i = 1;
        try {
            i = new ExifInterface (filePath).getAttributeInt(android.support.media.ExifInterface.TAG_ORIENTATION, 1);
        } catch (IOException e) {
        }
        return i;
    }

    private static Object convertExifField(TiffField field) {
        int i = 0;
        try {
            Object value = field.getValue();
            if (value instanceof String) {
                return value;
            }
            if (value instanceof RationalNumber) {
                return Double.valueOf(((RationalNumber) value).doubleValue());
            }
            if (value instanceof Integer) {
                return value;
            }
            if (value instanceof Byte) {
                return Integer.valueOf(((Byte) value).intValue());
            }
            int length;
            if (value instanceof byte[]) {
                ArrayList<Integer> bytes = new ArrayList ();
                byte[] bArr = (byte[]) value;
                length = bArr.length;
                while (i < length) {
                    bytes.add( Integer.valueOf(bArr[i]));
                    i++;
                }
                return bytes;
            } else if (value instanceof int[]) {
                ArrayList<Integer> numbers = new ArrayList ();
                int[] iArr = (int[]) value;
                length = iArr.length;
                while (i < length) {
                    numbers.add( Integer.valueOf(iArr[i]));
                    i++;
                }
                return numbers;
            } else if (!(value instanceof RationalNumber[])) {
                return null;
            } else {
                ArrayList<Double> numbers2 = new ArrayList ();
                RationalNumber[] rationalNumberArr = (RationalNumber[]) value;
                length = rationalNumberArr.length;
                while (i < length) {
                    numbers2.add( Double.valueOf(rationalNumberArr[i].doubleValue()));
                    i++;
                }
                return numbers2;
            }
        } catch (Throwable e) {
            GISLogger.log(e);
            return null;
        }
    }
}
