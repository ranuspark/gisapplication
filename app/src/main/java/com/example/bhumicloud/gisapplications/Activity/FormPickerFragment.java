package com.example.bhumicloud.gisapplications.Activity;

import org.droidparts.util.*;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.dossier.FormLoader;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.util.CursorUtils;
import com.example.bhumicloud.gisapplications.util.KeyboardUtils;

import java.io.File;

public class FormPickerFragment extends GISListFragment implements TextWatcher, LoaderCallbacks<Cursor> {
    private static final int LOADER_ID = 100;
    private FormPickerAdapter mAdapter;
    private String mCurrentFilter = "";
    private TextView mEmptyView;
    private FormPickerFragmentListener mListener;
    private EditText mSearchView;

    public interface FormPickerFragmentListener {
        void onFormSelected(long j);
    }

    private static class FormPickerAdapter extends CursorAdapter {
        private LayoutInflater inflater;

        public FormPickerAdapter(Context context) {
            super(context, null, 0);
            this.inflater = LayoutInflater.from(context);
        }
        @Override
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            return this.inflater.inflate(R.layout.list_item_form_picker, viewGroup, false);
        }
        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView recordCountTextView = (TextView) view.findViewById(R.id.form_picker_record_count);
            TextView descriptionTextView = (TextView) view.findViewById(R.id.form_picker_description);
            ((TextView) view.findViewById(R.id.form_picker_title)).setText(CursorUtils.getString(cursor, "name"));
            int recordCount = CursorUtils.getInt(cursor, Form.COLUMN_RECORD_COUNT);
            recordCountTextView.setText(context.getResources().getQuantityString(R.plurals.record_count, recordCount, new Object[]{Integer.valueOf(recordCount)}));
            String description = CursorUtils.getString(cursor, "description");
            if (TextUtils.isEmpty(description)) {
                descriptionTextView.setVisibility(View.GONE);
            } else {
                descriptionTextView.setText(description);
                descriptionTextView.setVisibility(View.VISIBLE);
            }
            String localThumbnailPath = CursorUtils.getString(cursor, Form.COLUMN_LOCAL_THUMBNAIL_PATH);
            ImageView image = (ImageView) view.findViewById(R.id.imageView);
            if (localThumbnailPath != null) {
                File thumbnail = new File(localThumbnailPath);
                if (thumbnail.exists()) {
                    image.setVisibility(View.VISIBLE);
                    return;
                }
                image.setVisibility(View.GONE);
                return;
            }
            image.setVisibility(View.GONE);
        }

        public long getFormID(int position) {
            Cursor c = getCursor();
            c.moveToPosition(position);
            return CursorUtils.getLong(c, "_id");
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FormPickerFragmentListener) {
            this.mListener = (FormPickerFragmentListener) context;
            return;
        }
        throw new RuntimeException(context.toString() + " must implement " + FormPickerFragmentListener.class.getSimpleName());
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mAdapter = new FormPickerAdapter(getActivity());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState) {
        View view = inflater.inflate(R.layout.drawer_form_picker, container, false);
        ((TextView) view.findViewById(R.id.title_view)).setText(R.string.assigned_form);
        this.mSearchView = (EditText) view.findViewById(R.id.search_view);
        this.mSearchView.addTextChangedListener(this);
        this.mSearchView.setVisibility(View.GONE);
        getLoaderManager().initLoader(100, null, this);
        return view;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView listView = getListView();
        listView.setChoiceMode(1);
        listView.setAdapter(this.mAdapter);
        this.mEmptyView = (TextView) listView.getEmptyView();
    }

    public void onResume() {
        super.onResume();
        Account activeAccount = Account.getActiveAccount();
        if (activeAccount == null || !activeAccount.getRole().isOwner()) {
            return;
        }
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    public void afterTextChanged(Editable s) {
        this.mCurrentFilter = s != null ? s.toString().toLowerCase() : "";
        getLoaderManager().restartLoader(100, null, this);
    }

    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);
        KeyboardUtils.hideDialogSoftKeyboard(getActivity(), getView());
        this.mListener.onFormSelected(this.mAdapter.getFormID(position));
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new FormLoader(getActivity(), "_id", "name", Form.COLUMN_LOCAL_THUMBNAIL_PATH, Form.COLUMN_RECORD_COUNT, "description").setSearchFilter(this.mCurrentFilter);
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        this.mAdapter.swapCursor(data);
    }

    public void onLoaderReset(Loader<Cursor> loader) {
        this.mAdapter.swapCursor(null);
    }

    public void clearSearch() {
        this.mSearchView.setText(null);
        reloadForms();
    }

    public void reloadForms() {
        if (getActivity() != null && isAdded()) {
            getLoaderManager().restartLoader(100, null, this);
        }
    }

    public void setSynchronizing(boolean isSynchronizing) {
        if (this.mEmptyView != null) {
            this.mEmptyView.setText(isSynchronizing ? R.string.synchronizing : R.string.no_form_message);
        }
    }
}
