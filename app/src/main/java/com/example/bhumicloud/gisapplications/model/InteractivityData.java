package com.example.bhumicloud.gisapplications.model;

import android.os.Parcel;
import android.os.Parcelable;

public class InteractivityData implements Parcelable {
    public static final Creator<InteractivityData> CREATOR = new C11671();
    private String mContent;
    private String mSnippet;

    public InteractivityData() {

    }

    static class C11671 implements Creator<InteractivityData> {
        C11671() {
        }

        public InteractivityData createFromParcel(Parcel source) {
            return new InteractivityData(source);
        }

        public InteractivityData[] newArray(int size) {
            return new InteractivityData[size];
        }
    }

    public InteractivityData(Parcel in) {
        this.mSnippet = in.readString();
        this.mContent = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mSnippet);
        dest.writeString(this.mContent);
    }

    public String getSnippet() {
        return this.mSnippet;
    }

    public void setSnippet(String snippet) {
        this.mSnippet = snippet;
    }

    public String getContent() {
        return this.mContent;
    }

    public void setContent(String content) {
        this.mContent = content;
    }
}
