package com.example.bhumicloud.gisapplications.apiLayer.exception;

public class IncompatibleClientException extends GISServiceException {
    public IncompatibleClientException(String message) {
        super(message);
    }
}
