package com.example.bhumicloud.gisapplications.Activity;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.sqlite.SQLiteDatabase;
import android.os.Binder;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat.BigTextStyle;
import android.support.v4.app.NotificationCompat.Builder;
import android.text.TextUtils;

import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.apiLayer.Client;
import com.example.bhumicloud.gisapplications.apiLayer.CountingRequestBody.UploadObserver;
import com.example.bhumicloud.gisapplications.apiLayer.exception.IncompatibleClientException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.InvalidResponseException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.NetworkUnavailableException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.ResourceNotFoundException;
import com.example.bhumicloud.gisapplications.apiLayer.exception.UnprocessableEntityException;
import com.example.bhumicloud.gisapplications.dossier.RecordQueryBuilder;
import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.Project;
import com.example.bhumicloud.gisapplications.model.Record;
import com.example.bhumicloud.gisapplications.model.TrashCan;
import com.example.bhumicloud.gisapplications.model.field.attachment.Attachment;
import com.example.bhumicloud.gisapplications.model.field.attachment.photo.Photo;
import com.example.bhumicloud.gisapplications.model.javascript.FormSchema;
import com.example.bhumicloud.gisapplications.util.AlertUtils;
import com.example.bhumicloud.gisapplications.util.DateUtils;
import com.example.bhumicloud.gisapplications.util.GISLogger;
import com.example.bhumicloud.gisapplications.util.JSONUtils;
import com.example.bhumicloud.gisapplications.util.NetworkUtils;
import com.example.bhumicloud.gisapplications.util.PatronSettings;
import com.example.bhumicloud.gisapplications.util.SQLiteUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class SyncService extends IntentService {
    public static final String EXTRA_ACCOUNT = "GIS:extra:account";
    public static final String EXTRA_TASK = "GIS:extra:task";
    private static final String DEBUG_TAG = SyncService.class.getSimpleName ();
    private static final String HISTORY_CHANGE_TYPE_CREATE = "c";
    private static final String HISTORY_CHANGE_TYPE_DELETE = "d";
    private static final String NOTIFICATION_CHANNEL_ID = "GIS_ID";
    private static final int NOTIFICATION_SYNC_STATUS = 100;
    private static final int TASK_SYNC_ALL = 0;
    private static final int TASK_SYNC_RECORDS = 1;
    private final Handler mObserverHandler = new Handler ();
    private final HashSet<Listener> mObservers = new HashSet ();
    private Client mClient;
    private NotificationManager mNotificationManager;
    private int mProgress;
    private ArrayList<Object> mSyncStates;
    private Builder mSyncStatusBuilder;
    private volatile boolean mSynchronizing = false;
    private Object formName;

    public SyncService() {
        super ( SyncService.class.getName () );
    }

    public static boolean bind(Context context, ServiceConnection connection) {
        return context.bindService ( new Intent ( context, SyncService.class ), connection, TASK_SYNC_RECORDS );
    }

    public static void synchronizeData(Context context, Account account) {
        Intent intent = new Intent ( context, SyncService.class );
        if (account != null) {
            intent.putExtra ( EXTRA_ACCOUNT, account.getRowID () );
        }
        intent.putExtra ( EXTRA_TASK, TASK_SYNC_ALL );
        context.startService ( intent );
    }

    public static void synchronizeRecords(Context context, Account account) {
        Intent intent = new Intent ( context, SyncService.class );
        if (account != null) {
            intent.putExtra ( EXTRA_ACCOUNT, account.getRowID () );
        }
        intent.putExtra ( EXTRA_TASK, TASK_SYNC_RECORDS );
        context.startService ( intent );
    }

    private static boolean shouldSync(Context context, int syncMode) {
        if (2 == syncMode) {
            return false;
        }
        if (1 != syncMode || NetworkUtils.isUsingWiFi ( context )) {
            return true;
        }
        return false;
    }

    private static void log(String message) {
        GISLogger.log ( DEBUG_TAG, message );
    }

    private static void log(String format, Object... args) {
        GISLogger.log ( DEBUG_TAG, format, args );
    }

    public void onCreate() {
        super.onCreate ();
        this.mSyncStatusBuilder = new Builder ( this, NOTIFICATION_CHANNEL_ID );
        Object service = getSystemService ( Context.NOTIFICATION_SERVICE );
        if (service instanceof NotificationManager) {
            this.mNotificationManager = (NotificationManager) service;
            if (VERSION.SDK_INT >= 26) {
                this.mNotificationManager.createNotificationChannel(new NotificationChannel(NOTIFICATION_CHANNEL_ID, getString(R.string.GIS), NotificationManager.IMPORTANCE_DEFAULT));
            }
        }
    }

    @Override
    public SyncServiceBinder onBind(Intent intent) {
        return new SyncServiceBinder ( this );
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        this.mSynchronizing = true;
        if (!(intent == null || intent.getExtras () == null)) {
            Bundle extras = intent.getExtras ();
            switch (extras.getInt ( EXTRA_TASK, TASK_SYNC_ALL )) {
                case TASK_SYNC_ALL:
                    onSynchronizeData ( extras.getLong ( EXTRA_ACCOUNT ) );
                    break;
                case TASK_SYNC_RECORDS:
                    // onSynchronizeRecords ( extras.getLong ( EXTRA_ACCOUNT ) );
                    break;
            }
        }
        TrashCan.empty ( this );
        this.mSynchronizing = false;
        notifySyncFinished ();
        dismissSyncStatus ();
    }

    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved ( rootIntent );
        dismissSyncStatus ();
    }

    public void addListener(Listener listener) {
        this.mObservers.add ( listener );
    }

    public void removeListener(Listener listener) {
        this.mObservers.remove ( listener );
    }

    public boolean isSynchronizing() {
        return this.mSynchronizing;
    }

    private void onSynchronizeData(long accountID) {
        Account account = Account.getAccount ( accountID );
        if (account != null) {
            this.mClient = new Client ( (Context) this, account );
            try {
                onCheckReachability ();
                //onCheckMessages();
               // HashMap onAuthenticateAccount = onAuthenticateAccount();
                onSynchronizeAccounts ( account, onAuthenticateAccount () );
                onSynchronizeForm ( account );
                onSynchronizeProjects ( account );
                onUploadAttachments ( account, null );
                onUploadRecords(account);
                onDownloadRecords(account);
            } catch (IOException e) {
                handleException ( e );
            }
        }
    }

    private void onSynchronizeRecords(long accountID) {
        Account account = Account.getAccount ( accountID );
        if (account != null) {
            this.mClient = new Client ( (Context) this, account );
            try {
                onCheckReachability ();
                //onCheckMessages ();
                onAuthenticateAccount ();
                //onUploadAttachments ( account, null );
                //onUploadRecords ( account );
            } catch (IOException e) {
                handleException ( e );
            }
        }
    }

    private void onCheckReachability() throws IOException {
        log ( "Checking Reachability..." );
        if (NetworkUtils.isConnected ( this )) {
            String hostname;
            Socket socket = new Socket ();
            String fullHostname = this.mClient.getHostname ();
            int portIndex = fullHostname.lastIndexOf ( ":" );
            if (portIndex > -1) {
                hostname = fullHostname.substring ( 0, portIndex );
            } else {
                hostname = fullHostname;
            }
            InetAddress address = InetAddress.getByName ( hostname );
            log ( "Hostname Resolved Successfully!" );
            socket.connect ( new InetSocketAddress ( address, this.mClient.getPort () ), 5000 );
            log ( "Socket Connected Successfully!" );
            if (socket.isConnected ()) {
                socket.close ();
                return;
            }
            return;
        }
        throw new NetworkUnavailableException ();
    }

    private void onCheckMessages() throws IOException {
        log ( "Checking Messages..." );
        if (NetworkUtils.isConnected ( this )) {
            Map messages = this.mClient.getMessages ( this );
            String errorMessage = JSONUtils.getString ( messages, "error_message" );
            String infoMessage = JSONUtils.getString ( messages, "info_message" );
            if (!TextUtils.isEmpty ( errorMessage )) {
                throw new IncompatibleClientException ( errorMessage );
            } else if (!TextUtils.isEmpty ( infoMessage )) {
                notifySyncFailed ( infoMessage );
                return;
            } else {
                return;
            }
        }
        throw new NetworkUnavailableException ();
    }

    private HashMap<String, Object> onAuthenticateAccount() throws IOException {
        log ( "Authenticating Account..." );
        updateSyncStatus ( (int) R.string.sync_status_authenticating );
        if (NetworkUtils.isConnected ( this )) {
            Map user = this.mClient.getAccount ();
            Map access = JSONUtils.getHashMap ( user, "access" );
            if (JSONUtils.getBoolean ( access, "allowed", true )) {
                return (HashMap<String, Object>) user;
            }
            throw new IncompatibleClientException ( JSONUtils.getString ( access, "reason" ) );
        }
        throw new NetworkUnavailableException ();
    }

    private void onSynchronizeAccounts(Account activeAccount, HashMap<String, Object> user) {
        activeAccount.synchronizeAccounts ( user );
    }

    private void onSynchronizeForm(Account account) throws IOException {
        onSynchronizeForms ( account );
    }

    private void onSynchronizeProjects(Account anAccount) throws IOException {
        log ( "Synchronizing Projects..." );
        updateSyncStatus ( (int) R.string.projects );
        if (NetworkUtils.isConnected ( this )) {
            ArrayList<String> projectIDs = new ArrayList ();
            int currentPage = TASK_SYNC_RECORDS;
            int currentProject = TASK_SYNC_ALL;
            while (true) {
                HashMap<String, Object> response = this.mClient.getProjects ( currentPage );
                int totalPages = JSONUtils.getInt ( response, "total_pages" );
                int totalCount = JSONUtils.getInt ( response, "total_count", Integer.valueOf ( -1 ) ).intValue ();
                List projects = JSONUtils.getArrayList ( response, "projects" );
                if (!(projects == null || projects.isEmpty ())) {
                    SQLiteDatabase db = GIS.getDatabase ();
                    db.beginTransactionNonExclusive ();
                    for (int i = TASK_SYNC_ALL; i < projects.size (); i += TASK_SYNC_RECORDS) {
                        updateSyncStatus ( (int) R.string.projects, currentProject, totalCount );
                        Map projectJSON = JSONUtils.getHashMap ( projects, i );
                        String projectID = JSONUtils.getString ( projectJSON, "id" );
                        if (!(projectJSON == null || projectID == null)) {
                            projectIDs.add ( projectID );
                            Project project = Project.find ( null, projectID, anAccount );
                            if (project == null) {
                                project = new Project ( projectJSON );
                                project.setAccount ( anAccount );
                            } else {
                                project.setAttributesFromJSON ( projectJSON );
                                project.clearDeletedAt ();
                            }
                            project.save ();
                        }
                        currentProject += TASK_SYNC_RECORDS;
                    }
                    try {
                        db.setTransactionSuccessful ();
                    } finally {
                        db.endTransaction ();
                    }
                }
                currentPage += TASK_SYNC_RECORDS;
                if (currentPage > totalPages) {
                    Project.truncate ( projectIDs, anAccount );
                    return;
                }
            }
        }
        throw new NetworkUnavailableException ();
    }

    private void onSynchronizeForms(Account anAccount) throws IOException {
        log ( "Syncing Forms..." );
        updateSyncStatus ( (int) R.string.sync_status_forms );
        if (NetworkUtils.isConnected ( this )) {
            ArrayList<String> formIDs = new ArrayList ();
            int currentPage = TASK_SYNC_RECORDS;
            int currentForms = TASK_SYNC_ALL;
            while (true) {
                HashMap<String, Object> response = this.mClient.getForms ( currentPage );
                int totalPages = JSONUtils.getInt ( response, "total_pages" );
                int totalCount = JSONUtils.getInt ( response, "total_count", Integer.valueOf ( -1 ) ).intValue ();
                List froms = JSONUtils.getArrayList ( response, "forms" );
                if (!(froms == null || froms.isEmpty ())) {
                    SQLiteDatabase db = GIS.getDatabase ();
                    FormSchema _schema = new FormSchema ( this, db );
                    db.beginTransactionNonExclusive ();
                    for (int i = TASK_SYNC_ALL; i < froms.size (); i++) {
                        updateSyncStatus ( (int) R.string.sync_status_forms, currentForms, totalCount );
                        Map formJSON = JSONUtils.getHashMap ( froms, i );
                        String formID = JSONUtils.getString ( formJSON, "id" );

                        if (!(formJSON == null || formID == null)) {
                            formIDs.add ( formID );
                            Form form = Form.getForm ( anAccount, formID, false );
                            Map<String, Object> oldFormJSON = null;

                            if (form == null) {
                                form = new Form ( formJSON );
                                form.setAccount ( anAccount );
                                Date updatedAt = null;
                                PatronSettings.setLastRecordDownloadDate ( this, form, updatedAt );
                            } else {
                                oldFormJSON = form.toJSON ();
                                form.setAttributesFromJSON ( formJSON );
                                form.clearDeletedAt ();
                            }
                            form.save ();
                            form.getRowID ();
                            _schema.updateSchemaWithOldForm ( oldFormJSON, formJSON, form.getRowID (), anAccount );

                        }
                        currentForms += TASK_SYNC_RECORDS;
                    }
                    try {
                        db.setTransactionSuccessful ();
                    } finally {
                        db.endTransaction ();
                        _schema.release ();
                    }
                }
                currentPage += TASK_SYNC_RECORDS;
                if (currentPage > totalPages) {
                     Form.truncate ( formIDs, anAccount );
                    return;
                }
            }
        }
        else
        {throw new NetworkUnavailableException ();}

    }

    private void onUploadRecords(Account account) throws IOException {
        log ( "Uploading Records..." );
        updateSyncStatus ( (int) R.string.sync_status_records );
        if (NetworkUtils.isConnected ( this )) {
            SQLiteDatabase db = GIS.getDatabase ();
            for (Form form : Form.getFormsSortedByDependencies ( account )) {
                log ( "Uploading " + form.getName () + "'s Records..." );
                List<Record> records = new RecordQueryBuilder ().account ( account ).form ( form ).isSynchronized ( false ).isDraft ( false ).list ( db );
                int currentRecord = 0;
                int totalRecordsCount = records.size ();
                if (totalRecordsCount > 0) {
                    String changeset = onOpenChangeset ( form );
                    for (Record record : records) {
                        updateSyncStatus ( (int) R.string.sync_status_records, currentRecord, totalRecordsCount );
                        onUploadRecord ( record, account, changeset );
                        currentRecord++;
                    }
                    //onCloseChangeset ( changeset );
                }
            }
            return;
        }
        throw new NetworkUnavailableException ();
    }

    private String onOpenChangeset(Form form) throws IOException {
        return JSONUtils.getString ( this.mClient.openChangeset ( form ), "id" );
    }

    private void onCloseChangeset(String changeset) throws IOException {
        this.mClient.closeChangeset ( changeset );
    }

    private void onUploadRecord(Record record, Account account, String changeset) throws IOException {
        log ( "Uploading Record " + record.getUniqueID () );
        try {
            HashMap<String, Object> jsonRecord = JSONUtils.getHashMap ( (Map) this.mClient.uploadRecord ( record, changeset ).body (), "record" );
            if (jsonRecord != null) {
                record.setJSONAttributes ( account, jsonRecord );
                record.setSynchronized ( true );
                record.setNew ( false );
                record.save ();
            }
        } catch (InvalidResponseException _expection) {
            if (!record.isNew ()) {
                log ( "Deleting Record " + record.getRemoteID () );
                record.deleteWithTransaction ();
                return;
            }
        } catch (UnprocessableEntityException _accountException) {
            GISLogger.log ( _accountException );
            notifySyncFailed ( getResources ().getString ( R.string.failed_record_upload_unprocessable, new Object[]{record.getTitle ()} ) );
            return;
        } catch (ResourceNotFoundException _exception) {
            GISLogger.log ( _exception );
            notifySyncFailed ( getResources ().getString ( R.string.failed_record_upload_unprocessable, new Object[]{record.getTitle ()} ) );

        }
    }

    private void onUploadAttachments(Account account, Record record) throws IOException {
        try {
            onUploadPhotos ( account, record );
        } catch (Exception e) {
            handleException ( e );
        }
    }

    private void onUploadPhotos(Account account, Record record) throws IOException {
        log ( "Uploading Photos..." );
        if (!NetworkUtils.isConnected ( this )) {
            throw new NetworkUnavailableException ();
        } else if (shouldSync ( this, PatronSettings.getPhotosSyncMode ( this ) )) {
            List<Attachment> photos = Photo.getPhotos ( account, record, false );
            int size = photos.size ();
            int count = 1;
            updateSyncStatus ( getString ( R.string.sync_status_photos, new Object[]{Integer.valueOf ( 0 ), Integer.valueOf ( size )} ) );
            for (Attachment photo : photos) {
                onUploadAttachment ( photo, getString ( R.string.sync_status_photos, new Object[]{Integer.valueOf ( count ), Integer.valueOf ( size )} ) );
                count++;
            }
        }
    }

    private void onUploadAttachment(Attachment attachment, final String statusMsg) throws IOException {
        Client client = this.mClient;
        updateSyncStatus ( statusMsg, 0, 100 );
        if (SQLiteUtils.getCount ( GIS.getDatabase (), Record.TABLE_NAME, "unique_id = ?", new String[]{attachment.getRecordID ()} ) <= 0) {
            GISLogger.log ( "SyncService", "Deleting Orphaned %s", attachment );
            attachment.delete ();
            return;
        }
        boolean uploaded;
        try {
            HashMap<String, Object> json = client.getAttachmentJSON ( attachment );
            uploaded = json != null && JSONUtils.getBoolean ( json, Attachment.COLUMN_UPLOADED );
        } catch (ResourceNotFoundException e) {
            uploaded = false;
        }
        if (uploaded) {
            attachment.setUploaded ( true );
            attachment.save ();
            return;
        }
        try {
            if (attachment.getFileOne ().length () < 1) {
                throw new UnprocessableEntityException ( "\"" + attachment.getFileOne ().getPath () + "\" Invalid file size: " + attachment.getFileOne ().length () );
            }
            client.uploadAttachment ( attachment, new UploadObserver () {
                public void onUploadProgress(int progress) {
                    SyncService.this.updateSyncStatus ( statusMsg, progress, 100 );
                }
            } );
            attachment.setUploaded ( true );
            attachment.save ();
        } catch (Throwable e2) {
            GISLogger.log ( e2 );
            notifySyncFailed ( getResources ().getString ( R.string.failed_attachment_upload_file_not_found, new Object[]{attachment.getFileOne ()} ) );
        }
    }

    private void onDownloadRecords(Account account) throws IOException {
        log ( "Downloading Records..." );
        updateSyncStatus ( (int) R.string.sync_status_records );
        if (NetworkUtils.isConnected ( this )) {
            ArrayList<Form> forms = Form.getFormsSortedByDependencies ( account );
            SQLiteDatabase db = GIS.getDatabase ();
            Iterator it = forms.iterator ();
            while (it.hasNext ()) {
                Form form = (Form) it.next ();
                String formName = form.getName();
                Object[] objArr = new Object[TASK_SYNC_RECORDS];
                objArr[TASK_SYNC_ALL] = formName;
                log("Downloading Records for %s...", objArr);
                Date syncTimestamp = PatronSettings.getLastRecordDownloadDate(this, form);
                boolean useHistory = syncTimestamp != null;
                long sequence = useHistory ? syncTimestamp.getTime() : 0;
                long alphaMillis = SystemClock.elapsedRealtime();
                int currentPage = TASK_SYNC_ALL;
                int downloads = TASK_SYNC_ALL;
                do {
                    objArr = new Object[TASK_SYNC_RECORDS];
                    currentPage += TASK_SYNC_RECORDS;
                    objArr[TASK_SYNC_ALL] = Integer.valueOf(currentPage);
                    log("Fetching Page %d", objArr);
                    objArr = new Object[2];
                    downloads += TASK_SYNC_RECORDS;
                    objArr[TASK_SYNC_ALL] = Integer.valueOf(downloads);
                    objArr[TASK_SYNC_RECORDS] = formName;
                    updateSyncStatus(getString(R.string.sync_status_downloading, objArr));
                    HashMap<String, Object> responseJSON = this.mClient.getRecords(form, sequence, null, useHistory);
                    String processingMessage = getString(R.string.sync_status_processing, new Object[]{Integer.valueOf(downloads), formName});
                    updateSyncStatus(processingMessage);
                    ArrayList<Object> recordsJSON = JSONUtils.getArrayList(responseJSON, "records");
                    if (!(recordsJSON == null || recordsJSON.isEmpty())) {
                        int currentRecord = TASK_SYNC_ALL;
                        int length = recordsJSON.size();
                        updateSyncStatus(processingMessage, (int) TASK_SYNC_ALL, length);
                      //  db.beginTransactionNonExclusive();
                        int i = TASK_SYNC_ALL;
                        while (i < recordsJSON.size()) {
                            try {
                                currentRecord += TASK_SYNC_RECORDS;
                                if (currentRecord % 20 == 0) {
                                    updateSyncStatus(processingMessage, currentRecord, length);
                                }
                                HashMap<String, Object> recordJSON = JSONUtils.getHashMap((List) recordsJSON, i);
                                if (recordJSON != null) {
                                    onDownloadRecord(account, form, recordJSON);
                                }
                                i += TASK_SYNC_RECORDS;
                            }
                            catch (Exception e)
                            {
                                GISLogger.log ( e.getMessage () );
                            }
                            finally {
                               // db.endTransaction();
                            }
                        }
                    //    db.endTransaction ();
                    //    db.setTransactionSuccessful();
                    }
                    sequence = JSONUtils.getLong(responseJSON, "next_sequence", -1);
                } while (sequence >= 0);
                form.updateRecordCount(db);
                objArr = new Object[TASK_SYNC_RECORDS];
                objArr[TASK_SYNC_ALL] = Double.valueOf(((double) (SystemClock.elapsedRealtime() - alphaMillis)) / 1000.0d);
                log("Record Download Complete! (%f sec)", objArr);
            }
            return;
        }
        throw new NetworkUnavailableException ();
    }

    private void onDownloadRecord(Account account, Form form, Map recordJSON) {
        String recordID = JSONUtils.getString ( recordJSON, "id" );
        Date updatedAt = DateUtils.parseTimestampString ( JSONUtils.getString ( recordJSON, Record.COLUMN_UPDATED_AT ) );
        long history = JSONUtils.getLong ( recordJSON, "sequence", -1 );
        if (history >= 0) {
            updatedAt = new Date ( history );
        }
        PatronSettings.setLastRecordDownloadDate ( this, form, updatedAt );
        String changeType = JSONUtils.getString ( recordJSON, "history_change_type" );
        if (TextUtils.isEmpty ( changeType )) {
            changeType = HISTORY_CHANGE_TYPE_CREATE;
        }
        Record record = Record.getRecordByRemoteID ( account, recordID );
        if (!HISTORY_CHANGE_TYPE_DELETE.equals ( changeType )) {
            if (record == null) {
                record = new Record ( account, form, recordJSON );
            } else if (!record.isDraft () && record.isSynchronized ()) {
                record.setJSONAttributes ( account, form, recordJSON );
            } else {
                return;
            }
            record.setSynchronized ( true );
            record.save ();
        } else if (record != null) {
            record.deleteWithTransaction ();
        }
    }

    private void onDownloadSystemResources() {
        ArrayList<String> resources = new ArrayList ();
        resources.add ( "expressions.js" );
        Iterator it = resources.iterator ();
        while (it.hasNext ()) {
            String resource = (String) it.next ();
            byte[] content = this.mClient.getSystemResource ( resource );
            if (content.length > 0) {
                writeSystemResourceFile ( resource, content );
            }
        }
    }

    private void writeSystemResourceFile(String resource, byte[] content) {
        File resourceRoot = new File ( getApplicationContext ().getFilesDir (), "resources" );
        resourceRoot.mkdirs ();
        try {
            BufferedOutputStream output = new BufferedOutputStream ( new FileOutputStream ( new File ( resourceRoot, resource ) ) );
            output.write ( content );
            output.flush ();
            output.close ();
        } catch (Throwable e) {
            GISLogger.log ( e );
        }
    }

    private void handleException(Exception e) {
        GISLogger.log ( (Throwable) e );
        notifySyncFailed ( e );
    }

    private void updateSyncStatus(int messageID) {
        updateSyncStatus ( messageID, -1, -1 );
    }

    private void updateSyncStatus(String message) {
        updateSyncStatus ( message, -1, -1 );
    }

    private void updateSyncStatus(int messageID, int progress, int maxProgress) {
        updateSyncStatus ( getString ( messageID ), progress, maxProgress );
    }

    private void updateSyncStatus(String message, int progress, int maxProgress) {
        boolean indeterminate;
        if (progress < 0) {
            indeterminate = true;
        } else if (progress != this.mProgress) {
            indeterminate = false;
        } else {
            return;
        }
        this.mProgress = progress;
        Builder b = this.mSyncStatusBuilder;
        b.setTicker ( "Synchronizing..." );
        b.setOngoing ( true );
        b.setSmallIcon ( R.drawable.ic_stat_sync );
        b.setProgress ( maxProgress, progress, indeterminate );
        b.setContentTitle ( "Synchronizing" );
        b.setContentText ( message );
        b.setOnlyAlertOnce ( true );
        b.setContentIntent ( PendingIntent.getActivity ( this, 0, new Intent (), 0 ) );
        this.mNotificationManager.notify ( 100, b.build () );
    }

    private void dismissSyncStatus() {
        this.mNotificationManager.cancel ( 100 );
    }

    private void notifySyncFailed(Exception exception) {
        notifySyncFailed ( AlertUtils.getExceptionMessage ( this, exception ) );
    }

    private void notifySyncFailed(final String message) {
        Builder b = new Builder ( this );
        b.setTicker ( "GIS: Sync Failed" );
        b.setSmallIcon ( R.drawable.ic_stat_error );
        b.setContentTitle ( "GIS: Sync Failed" );
        b.setContentText ( message );
        b.setStyle ( new BigTextStyle ().bigText ( message ) );
        b.setContentIntent ( PendingIntent.getActivity ( this, 0, new Intent (), 0 ) );
        this.mNotificationManager.notify ( new Random ().nextInt ( ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED ), b.build () );
        this.mObserverHandler.post ( new Runnable () {
            public void run() {
                Iterator it = SyncService.this.mObservers.iterator ();
                while (it.hasNext ()) {
                    ((Listener) it.next ()).onSyncFailed ( message );
                }
            }
        } );
    }

    private void notifySyncFinished() {
        this.mObserverHandler.post ( new Runnable () {
            @Override
            public void run() {
                Iterator it = SyncService.this.mObservers.iterator ();
                while (it.hasNext ()) {
                    ((Listener) it.next ()).onSyncFinished ();
                }
            }
        } );
    }

    public interface Listener {
        void onSyncFailed(String str);

        void onSyncFinished();
    }

    public static class SyncServiceBinder extends Binder {
        private final SyncService mService;

        public SyncServiceBinder(SyncService service) {
            this.mService = service;
        }

        public SyncService getService() {
            return this.mService;
        }
    }
}
