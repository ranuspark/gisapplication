package com.example.bhumicloud.gisapplications.apiLayer.exception;

import android.content.Context;

import com.example.bhumicloud.gisapplications.R;

public class ResourceNotFoundException extends GISServiceException {
    public ResourceNotFoundException() {
        super("The requested resource could not be found on the server.");
    }

    public String getUserFriendlyMessage(Context context) {
        return context.getString(R.string.resource_not_found_message);
    }
}
