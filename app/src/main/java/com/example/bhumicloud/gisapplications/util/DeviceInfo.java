package com.example.bhumicloud.gisapplications.util;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.util.DisplayMetrics;
import android.view.Display;

import com.example.bhumicloud.gisapplications.R;

import java.util.Locale;

import io.fabric.sdk.android.services.common.CommonUtils;

public class DeviceInfo {

    private static int mEmulatorRating = -1;

    private static boolean isVersionOrHigher(int version) {
        return VERSION.SDK_INT >= version;
    }

    public static boolean isNougat() {
        return isVersionOrHigher(24);
    }

    public static boolean isMarshmallow() {
        return isVersionOrHigher(23);
    }

    public static boolean isLollipop() {
        return isVersionOrHigher(21);
    }

    public static boolean isJellyBeanMR2() {
        return isVersionOrHigher(18);
    }

    public static boolean isJellyBeanMR1() {
        return isVersionOrHigher(17);
    }

    public static boolean isJellyBean() {
        return isVersionOrHigher(16);
    }

    public static boolean isRightToLeft(Resources resources) {
        return resources.getBoolean( R.bool.is_right_to_left);
    }

    public static boolean isRightToLeft() {
        return isRightToLeft(Locale.getDefault());
    }

    public static boolean isRightToLeft(Locale locale) {
        int directionality = Character.getDirectionality(locale.getDisplayName().charAt(0));
        if (directionality == 1 || directionality == 2) {
            return true;
        }
        return false;
    }

    public static String getSystemVersion() {
        return VERSION.RELEASE;
    }

    public static String getDeviceManufacturer() {
        return Build.MANUFACTURER;
    }

    public static String getDeviceModel() {
        return Build.MODEL;
    }

    @SuppressLint({"HardwareIds"})
    public static String getDeviceIdentifier(Context context) {
        return Secure.getString(context.getContentResolver(), "android_id");
    }

    @TargetApi(17)
    public static double getAspectRatio(Display display) {
        double width;
        double height;
        if (isJellyBeanMR1()) {
            DisplayMetrics realMetrics = new DisplayMetrics();
            display.getRealMetrics(realMetrics);
            width = (double) realMetrics.widthPixels;
            height = (double) realMetrics.heightPixels;
        } else {
            try {
                width = (double) ((Integer) Display.class.getMethod("getRawWidth", new Class[0]).invoke(display, new Object[0])).intValue();
                height = (double) ((Integer) Display.class.getMethod("getRawHeight", new Class[0]).invoke(display, new Object[0])).intValue();
            } catch (Exception e) {
                width = (double) display.getWidth();
                height = (double) display.getHeight();
            }
        }
        if (width > height) {
            return width / height;
        }
        return height / width;
    }

    public static boolean isEmulator() {
        if (mEmulatorRating < 0) {
            int newRating = 0;
            if (Build.PRODUCT.equals("sdk") || Build.PRODUCT.equals( CommonUtils.GOOGLE_SDK) || Build.PRODUCT.equals("sdk_x86") || Build.PRODUCT.equals("vbox86p")) {
                newRating = 0 + 1;
            }
            if (Build.MANUFACTURER.equals("unknown") || Build.MANUFACTURER.equals("Genymotion")) {
                newRating++;
            }
            if (Build.BRAND.equals("generic") || Build.BRAND.equals("generic_x86")) {
                newRating++;
            }
            if (Build.DEVICE.equals("generic") || Build.DEVICE.equals("generic_x86") || Build.DEVICE.equals("vbox86p")) {
                newRating++;
            }
            if (Build.MODEL.equals("sdk") || Build.MODEL.equals(CommonUtils.GOOGLE_SDK) || Build.MODEL.equals("Android SDK built for x86")) {
                newRating++;
            }
            if (Build.HARDWARE.equals("goldfish") || Build.HARDWARE.equals("vbox86")) {
                newRating++;
            }
            if (Build.FINGERPRINT.contains("generic/sdk/generic") || Build.FINGERPRINT.contains("generic_x86/sdk_x86/generic_x86") || Build.FINGERPRINT.contains("generic/google_sdk/generic") || Build.FINGERPRINT.contains("generic/vbox86p/vbox86p")) {
                newRating++;
            }
            mEmulatorRating = newRating;
        }
        return mEmulatorRating > 4;
    }
}