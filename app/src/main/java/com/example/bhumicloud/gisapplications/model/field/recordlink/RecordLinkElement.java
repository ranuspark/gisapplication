package com.example.bhumicloud.gisapplications.model.field.recordlink;

import android.os.Parcel;
import android.os.Parcelable.Creator;

import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.field.Element;
import com.example.bhumicloud.gisapplications.util.JSONUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RecordLinkElement extends Element {
    public static final Creator<RecordLinkElement> CREATOR = new C11951();
    private static final String JSON_RECORD_LINK_ALLOW_CREATING = "allow_creating_records";
    private static final String JSON_RECORD_LINK_ALLOW_EXISTING = "allow_existing_records";
    private static final String JSON_RECORD_LINK_ALLOW_MULTIPLE = "allow_multiple_records";
    private static final String JSON_RECORD_LINK_ALLOW_UPDATING = "allow_updating_records";
    private static final String JSON_RECORD_LINK_CONDITIONS = "record_conditions";
    private static final String JSON_RECORD_LINK_CONDITIONS_TYPE = "record_conditions_type";
    private static final String JSON_RECORD_LINK_FORM_ID = "form_id";
    private static final String JSON_RECORD_LINK_RECORD_DEFAULTS = "record_defaults";
    private boolean mAllowCreating;
    private boolean mAllowExisting;
    private boolean mAllowMultiple;
    private boolean mAllowUpdating;
    private Form mForm;
    private String mFormID;
    private ArrayList<RecordLinkCondition> mRecordConditions;
    private String mRecordConditionsType;
    private ArrayList<RecordLinkDefault> mRecordDefaults;

    static class C11951 implements Creator<RecordLinkElement> {
        C11951() {
        }

        public RecordLinkElement createFromParcel(Parcel source) {
            return new RecordLinkElement(source);
        }

        public RecordLinkElement[] newArray(int size) {
            return new RecordLinkElement[size];
        }
    }

    public RecordLinkElement(Element parent, Map json) {
        super(parent, json);
        int i;
        this.mRecordConditions = new ArrayList();
        this.mRecordDefaults = new ArrayList();
        this.mFormID = JSONUtils.getString(json, "form_id");
        this.mRecordConditionsType = JSONUtils.getString(json, JSON_RECORD_LINK_CONDITIONS_TYPE);
        List recordConditionsAttributes = JSONUtils.getArrayList(json, JSON_RECORD_LINK_CONDITIONS);
        if (!(recordConditionsAttributes == null || recordConditionsAttributes.isEmpty())) {
            for (i = 0; i < recordConditionsAttributes.size(); i++) {
                this.mRecordConditions.add(new RecordLinkCondition(JSONUtils.getHashMap(recordConditionsAttributes, i)));
            }
        }
        List recordDefaultsAttributes = JSONUtils.getArrayList(json, JSON_RECORD_LINK_RECORD_DEFAULTS);
        if (!(recordDefaultsAttributes == null || recordDefaultsAttributes.isEmpty())) {
            for (i = 0; i < recordDefaultsAttributes.size(); i++) {
                this.mRecordDefaults.add(new RecordLinkDefault(JSONUtils.getHashMap(recordDefaultsAttributes, i)));
            }
        }
        this.mAllowMultiple = JSONUtils.getBoolean(json, JSON_RECORD_LINK_ALLOW_MULTIPLE, true);
        this.mAllowExisting = JSONUtils.getBoolean(json, JSON_RECORD_LINK_ALLOW_EXISTING, true);
        this.mAllowCreating = JSONUtils.getBoolean(json, JSON_RECORD_LINK_ALLOW_CREATING, true);
        this.mAllowUpdating = JSONUtils.getBoolean(json, JSON_RECORD_LINK_ALLOW_UPDATING, true);
    }

    private RecordLinkElement(Parcel parcel) {
        super(parcel);
        boolean z;
        boolean z2 = true;
        this.mRecordConditions = new ArrayList();
        this.mRecordDefaults = new ArrayList();
        this.mFormID = parcel.readString();
        this.mRecordConditionsType = parcel.readString();
        parcel.readTypedList(this.mRecordConditions, RecordLinkCondition.CREATOR);
        parcel.readTypedList(this.mRecordDefaults, RecordLinkDefault.CREATOR);
        if (parcel.readInt() == 1) {
            z = true;
        } else {
            z = false;
        }
        this.mAllowMultiple = z;
        if (parcel.readInt() == 1) {
            z = true;
        } else {
            z = false;
        }
        this.mAllowExisting = z;
        if (parcel.readInt() == 1) {
            z = true;
        } else {
            z = false;
        }
        this.mAllowCreating = z;
        if (parcel.readInt() != 1) {
            z2 = false;
        }
        this.mAllowUpdating = z2;
    }

    public void writeToParcel(Parcel dest, int flags) {
        int i;
        int i2 = 1;
        super.writeToParcel(dest, flags);
        dest.writeString(this.mFormID);
        dest.writeString(this.mRecordConditionsType);
        dest.writeTypedList(this.mRecordConditions);
        dest.writeTypedList(this.mRecordDefaults);
        if (this.mAllowMultiple) {
            i = 1;
        } else {
            i = 0;
        }
        dest.writeInt(i);
        if (this.mAllowExisting) {
            i = 1;
        } else {
            i = 0;
        }
        dest.writeInt(i);
        if (this.mAllowCreating) {
            i = 1;
        } else {
            i = 0;
        }
        dest.writeInt(i);
        if (!this.mAllowUpdating) {
            i2 = 0;
        }
        dest.writeInt(i2);
    }

    public String getType() {
        return Element.TYPE_RECORD_LINK;
    }

    public Form getForm() {
        if (this.mForm == null) {
            this.mForm = Form.getForm(Account.getActiveAccount(), this.mFormID, false);
        }
        return this.mForm;
    }

    public String getFormID() {
        return this.mFormID;
    }

    public boolean allowsMultiple() {
        return this.mAllowMultiple;
    }

    public boolean allowsExisting() {
        return this.mAllowExisting;
    }

    public boolean allowsCreating() {
        return this.mAllowCreating;
    }

    public boolean allowsUpdating() {
        return this.mAllowUpdating;
    }

    public ArrayList<RecordLinkDefault> getRecordDefaults() {
        return this.mRecordDefaults;
    }

    public String getRecordConditionsType() {
        return this.mRecordConditionsType;
    }

    public ArrayList<RecordLinkCondition> getRecordConditions() {
        return this.mRecordConditions;
    }
}
