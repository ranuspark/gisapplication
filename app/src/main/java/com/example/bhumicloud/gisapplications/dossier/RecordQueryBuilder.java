package com.example.bhumicloud.gisapplications.dossier;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.bhumicloud.gisapplications.model.Account;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.Record;

import java.util.ArrayList;
import java.util.List;

public class RecordQueryBuilder extends QueryBuilder {
    public RecordQueryBuilder() {
        super(Record.TABLE_NAME);
    }

    public RecordQueryBuilder form(Form form) {
        return (RecordQueryBuilder) where("form_id = ?", form.getRowID());
    }

    public RecordQueryBuilder account(Account account) {
        return (RecordQueryBuilder) where("account_id = ?", account.getRowID());
    }

    public RecordQueryBuilder isSynchronized(boolean isSynchronized) {
        return (RecordQueryBuilder) where("is_synchronized = ?", isSynchronized);
    }

    public RecordQueryBuilder isDraft(boolean isDraft) {
        return (RecordQueryBuilder) where("is_draft = ?", isDraft);
    }

    public List<Record> list(SQLiteDatabase db) {
        Cursor cursor = cursor(db);
        List<Record> records = new ArrayList ();
        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                records.add(new Record(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return records;
    }
}
