package com.example.bhumicloud.gisapplications.Activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bhumicloud.gisapplications.Adapter.SearchAdapter1;
import com.example.bhumicloud.gisapplications.GIS;
import com.example.bhumicloud.gisapplications.R;
import com.example.bhumicloud.gisapplications.dossier.RecordPredicate;
import com.example.bhumicloud.gisapplications.model.Form;
import com.example.bhumicloud.gisapplications.model.RecordValues;
import com.example.bhumicloud.gisapplications.model.Search;
import com.example.bhumicloud.gisapplications.util.CursorUtils;
import com.example.bhumicloud.gisapplications.util.RecordFilterOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Search_Activity extends AppCompatActivity implements GetValueListener {
    private Toolbar mToolbar;

    private RecyclerView recyclerView;
    private Button Btn_add,Btn_search;
    private SeekBar seekBar;
    private TextView tv_seek;
    SearchAdapter1 searchAdapter;
    ArrayList<Search> search_list=new ArrayList<>();
    ArrayList<Search> add_list=new ArrayList<>();
    public Spinner Sp_field,Sp_operator;
    public ImageView imageView;
    EditText Et_val,Et_val1;
    TextView Tv_count;
    String spi_field,spi_operator;
    static ArrayList<Search> list = new ArrayList<>();
    Search search;
    RecordCollectionView recordCollectionView;
    Context context;

    int i=1;
    String sp_key;
    ArrayList<String> strings_List=new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_search1);
        this.mToolbar = (Toolbar) findViewById(R.id.toolbar);
        this.mToolbar.setTitle("Search");
        this.mToolbar.setNavigationIcon((int) R.drawable.arrow2);
     //   this.mToolbar.setNavigationContentDescription((int) R.string.assigned_form);
        this.mToolbar.setNavigationOnClickListener ( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Navigation", "Navigation");
            startActivity(new Intent(Search_Activity.this,DashboardActivity.class));
          //  finish();

            }
        } );
        setSupportActionBar(this.mToolbar);

        recyclerView=(RecyclerView) findViewById(R.id.recyclerView);
        Tv_count=(TextView) findViewById(R.id.tv_count);
        Sp_field=(Spinner)  findViewById(R.id.sp_field);
        Sp_operator=(Spinner) findViewById(R.id.sp_operator);
        Et_val=(EditText) findViewById(R.id.et_val);
        Et_val1=(EditText)findViewById(R.id.et_val1);

        Et_val1.setVisibility(View.INVISIBLE);
        seekBar=(SeekBar) findViewById(R.id.seek_bar);
        tv_seek=(TextView)  findViewById(R.id.tv);
        tv_seek.setText(""+0+"Km");
        Btn_add=(Button)findViewById(R.id.btn_add);
        Btn_search=(Button)findViewById(R.id.btn_search);
        this.recordCollectionView = (RecordCollectionView) context;

            if(search_list.size()>-1) {
                LinearLayoutManager liLayoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(liLayoutManager);
                searchAdapter = new SearchAdapter1(search_list, Search_Activity.this);
                recyclerView.setAdapter(searchAdapter);

            }
        Intent intent =getIntent();
        String name= intent.getStringExtra("project_name");
        long form_id= intent.getLongExtra("project_Id",-1);
        long account_id= intent.getLongExtra("account_Id",-1);

//            Intent intent =getIntent();
//            String name= intent.getStringExtra("project_name");
//            Log.e("Name", name);
//            SQLiteDatabase db = GIS.getDatabase();
//           Cursor cursor= CursorUtils.queryTableWithCol(db,"Forms","elements",name);
//
//           String vaalue = cursor.getString(cursor.getColumnIndex("elements"));
//        Log.e("Value", vaalue);
//      ParseString(vaalue);
        getValuefromdb(name);

        Form form = Form.getForm(form_id);
        final String table_Name= RecordValues.tableNameWithForm(form, null);

        Log.e("Run TimeTable", table_Name);
        ArrayAdapter<Search> dataAdapter = new ArrayAdapter<Search>(this, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Sp_field.setAdapter(dataAdapter);

        List<String> list1 = new ArrayList<String>();
        list1.add("Condition");
        list1.add("Contain");
        list1.add("Less Than");
        list1.add("Greater Than");
        list1.add("Equals");
        list1.add("Not Equals");
        list1.add("Between");
        list1.add("Start With");
        list1.add("End With");

        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list1);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Sp_operator.setAdapter(dataAdapter1);



       Sp_field.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
          //  private int pos = position;
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int p, long arg3)
            {
                // TODO Auto-generated method stub
                Search search = (Search) arg0.getItemAtPosition(p);
                spi_field=  search.getField();
                 sp_key=search.getId();
                Log.e("spi_field", " "+spi_field+" "+sp_key);


            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0)
            {
                // TODO Auto-generated method stub

            }
        });
       Sp_operator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int p, long arg3)
            {
                // TODO Auto-generated method stub
                spi_operator = (String) arg0.getItemAtPosition(p);
                if(spi_operator.equalsIgnoreCase("Between"))
                {
                    Et_val1.setVisibility(View.VISIBLE);
                }else {
                    Et_val1.setVisibility(View.INVISIBLE);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0)
            {
                // TODO Auto-generated method stub

            }
        });


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tv_seek.setText(""+progress +" "+"Km");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        Btn_add.setOnClickListener(new View.OnClickListener() {
            String ss1;
            @Override
            public void onClick(View view) {
                String ss=  Et_val.getText().toString();
                 if(Et_val1.getVisibility()==View.VISIBLE)
                 {
                    ss1=  Et_val1.getText().toString();
                 }


               Boolean val=Validation(ss);
               if(val){
                    search=new Search();
                    search.setId(sp_key);
                   search.setField(spi_field);
                   search.setOperator(spi_operator);
                   search.setVal(ss);
                   search.setVal_two(ss1);
                   search_list.add(search);
                   searchAdapter.notifyDataSetChanged();

                   Log.e("GetValue", "Value "+ss);
                   Tv_count.setText("Count :- "+search_list.size());
               }else {

               }





            }
        });

        Btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            String radius=tv_seek.getText().toString();
                if(!radius.equals("0")) {
                    String ss1 = "";
                    Boolean first = false;
                    String query1 = "SELECT " + "*" + " FROM '" + table_Name + "'";

                    for (int i = 0; i < search_list.size(); i++) {
                        search_list.get(i).getField();
                        String con = search_list.get(i).getOperator();
                        String val = search_list.get(i).getVal();
                        // search_list.get(i).getId();
                        Log.e("Loop", search_list.get(i).getField() + " " + search_list.get(i).getId() + " " + search_list.get(i).getOperator() + " " + search_list.get(i).getVal() + " " + tv_seek.getText().toString());
                        String ss = "f" + search_list.get(i).getId();

                        if (first == false) {
                            ss1 = ss1.concat(" WHERE ");

                            first = true;
                        } else {
                            ss1 = ss1.concat(" AND ");
                        }
                        if (con.equalsIgnoreCase("Contain")) {
                            ss1 = ss1.concat("instr(" + ss + ",'" + val + "')");
                        }

                        if (con.equalsIgnoreCase("Equals")) {
                            ss1 = ss1.concat("`" + ss + "`" + "='" + val + "'"); // AND lower(IFNULL(`fdOzI`,0))=lower('') -- equals
                        }
                        if (con.equalsIgnoreCase("Not Equals")) {
                            ss1 = ss1.concat("lower(IFNULL(`" + ss + "`,0))" + "IS NOT lower('" + val + "')");   //lower(IFNULL(`fdOzI`,0)) IS NOT lower('') -- not equals
                        }
                        if (con.equalsIgnoreCase("Greater Than")) {
                            ss1 = ss1.concat("lower(IFNULL(`" + ss + "`,0))" + "< lower('" + val + "')");   //lower(IFNULL(`fdOzI`,0)) < lower('') -- greater than
                        }
                        if (con.equalsIgnoreCase("Less Than")) {
                            ss1 = ss1.concat("lower(IFNULL(`" + ss + "`,0))" + "> lower('" + val + "')");   //lower(IFNULL(`fdOzI`,0)) > lower('')
                        }
                        if (con.equalsIgnoreCase("Start With")) {
                            ss1 = ss1.concat("lower(IFNULL(`" + ss + "`,0))" + "Like lower('%" + val + "')"); // lower(IFNULL(`fdOzI`,0)) Like lower('%abc') -- start with
                        }
                        if (con.equalsIgnoreCase("End With")) {
                            ss1 = ss1.concat("lower(IFNULL(`" + ss + "`,0))" + "Like lower('" + val + "%')"); //  lower(IFNULL(`fdOzI`,0)) Like lower('abc%') -- end with
                        }
                        if (con.equalsIgnoreCase("Between")) {
                            ss1 = ss1.concat("lower(IFNULL(`" + ss + "`,0))" + "BETWEEN lower ('" + val + "')" + "AND lower ('" + val + "')"); // lower(IFNULL(`fdOzI`,0)) BETWEEN lower('') AND lower('') -- between  */
                        }


                    }
                    double[] latiLong= getLatiLong();
                    double maxLat=latiLong[0];
                    double minLat=latiLong[1];
                    double maxLon=latiLong[2];
                    double minLon=latiLong[3];

//  AND (
//		`latitude`  BETWEEN least('28.0','28.0') AND greatest('28.0','28.0') AND
//		`longitude` BETWEEN least('77.0','77.0') AND greatest('77.0','77.0')
                    String add_radius=" AND (`latitude`" +" BETWEEN min('"+minLat+"','"+maxLat+"') AND max('"+minLat+"','"+maxLat+"') AND"+
                           " `longitude`" +" BETWEEN min('"+minLon+"','"+maxLon+"') AND max('"+minLon+"','"+maxLon+"'))";


                    String query=query1.concat(ss1);
                    String finalQuery=query.concat(add_radius);
                            Log.e("Queryyyyy",finalQuery);
                    SQLiteDatabase db = GIS.getDatabase();

                    RecordListFragment1 recordListFragment1=new RecordListFragment1();
                  ArrayList<Search> record_list =  CursorUtils.querySearch(db,finalQuery);
                    ArrayList<Search> record_list1=new ArrayList<>();
                    Cursor cursor1 = null;
                    for (int i=0;i<record_list.size();i++) {
                        String id = record_list.get(i).getId();

                        String query11 = "SELECT " + "*" + " FROM `" + "Records" + "` WHERE _id = '" + id + "';";
                         cursor1 = db.rawQuery(query11, null);

                        if (cursor1 != null && cursor1.getCount()>0) {
                            while (cursor1.moveToNext()){
                           //     long long_id = cursor1.getLong(cursor1.getColumnIndex("_id"));
                                String long_id = cursor1.getString(cursor1.getColumnIndex("_id"));
                             String status = cursor1.getString(cursor1.getColumnIndex("status"));
                            Log.e("Value1", long_id + "  " + " ");

//                                Search ss=new Search();
//                                ss.setIds(long_id);
//                                ss.setStatus(status);
//
//                                record_list1.add(ss);

                                strings_List.add("5");
                              //  strings_List.add("10");
                                recordListFragment1.setSelectedRecordLinkIDs(strings_List);


                        }
                        }

                    }
//                  String vaalue = cursor.getString(cursor.getColumnIndex("id"));
//                    String record_id = cursor.getString(cursor.getColumnIndex("record_id")); //
//                    String vaalue2 = cursor.getString(cursor.getColumnIndex("record_resource_id"));
//                    Log.e("Value", vaalue+"  "+record_id +" "+vaalue2);
//                    ArrayList<Search> record_list=new ArrayList<>();
//                    Search ss=new Search();
//                    ss.setId(record_id);
//                    record_list.add(ss);
                   // Log.e("GetList", String.valueOf(record_list.get(0).getId()));
//
//                    Bundle bundle=new Bundle();
//                    bundle.putSerializable("record_list", record_list);

                    Intent intent =  new Intent(Search_Activity.this,SearchActivity.class);
                    //  intent.putExtras(bundle);
                    startActivity(intent);


                }

            }
        });


      /*  SELECT * FROM `account_1_form_1`
        WHERE
        instr(lower(IFNULL(`fdOzI`,0)),lower('')) --contain
        AND lower(IFNULL(`fdOzI`,0))=lower('') -- equals
        AND lower(IFNULL(`fdOzI`,0)) IS NOT lower('') -- not equals
        AND lower(IFNULL(`fdOzI`,0)) < lower('') -- greater than
        AND lower(IFNULL(`fdOzI`,0)) > lower('') -- less than
        AND lower(IFNULL(`fdOzI`,0)) Like lower('%abc') -- start with
        AND lower(IFNULL(`fdOzI`,0)) Like lower('abc%') -- end with
        --AND lower(IFNULL(`fdOzI`,0)) Like lower('%abc%') --contain
        AND lower(IFNULL(`fdOzI`,0)) BETWEEN lower('') AND lower('') -- between  */

//        Form form = Form.getForm(form_id);
//       String table_Name= RecordValues.tableNameWithForm(form, null);
//
//       Log.e("Run TimeTable", table_Name);



    }


            public Boolean Validation(String ss)
            {
                boolean val;
                if(spi_field.equalsIgnoreCase("Field"))
                {
                    val=false;
                    Toast.makeText(this, "Please Select Field value", Toast.LENGTH_LONG).show();
                }else if(spi_operator.equalsIgnoreCase("condition")){
                    Toast.makeText(this, "Please Select Condition ", Toast.LENGTH_LONG).show();
                    val=false;
                }else if(ss.equals("")||TextUtils.isEmpty(ss)||ss==null)
                {
                    Toast.makeText(this, "Please Fill Text Field ", Toast.LENGTH_LONG).show();
                    val=false;
                }else {
                    val=true;
                }
                return val;
            }

    @Override
    public void getValue(String count) {

        Tv_count.setText("Count :-"+count);

    }

   public static ArrayList<Search> ParseString(String value)
    {
        list.clear();
        try {
            JSONArray jsonArray=new JSONArray(value);
            for(int i = 0;i<jsonArray.length();i++)
            {
                JSONObject jsonObject=jsonArray.getJSONObject(i);
               String key= jsonObject.getString("key");
                String label =  jsonObject.getString("label");
            Log.e("Key_Label", key+" "+label);
                Search search=new Search();
                search.setId(key);
                search.setField(label);
                list.add(search);



            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    return list;

    }


    public static ArrayList<Search> getValuefromdb(String name)
    {

        Log.e("Name", name);
        SQLiteDatabase db = GIS.getDatabase();
        Cursor cursor= CursorUtils.queryTableWithCol(db,"Forms","elements",name);

        String vaalue = cursor.getString(cursor.getColumnIndex("elements"));
        Log.e("Value", vaalue);
       ArrayList<Search> alist= ParseString(vaalue);
        return alist;
    }


    public double[] getLatiLong ()
    {
        double[] lat_array;
        double presentLatitude=28.5931745;
        double presentLongitude=77.3136617;
      String dis=tv_seek.getText().toString();
        String[] splitStr = dis.split("\\s+");
       String a1= splitStr[0];
       Log.e("Al", a1);
        double searchRadiusKM= Integer.parseInt(a1);
        double earthRadiusKM=6371;

        Log.e("Foating", String.valueOf(Math.toDegrees(searchRadiusKM/earthRadiusKM)));
        double maxLat = presentLatitude + Math.toDegrees(searchRadiusKM/earthRadiusKM);
        double minLat = presentLatitude - Math.toDegrees(searchRadiusKM/earthRadiusKM);
        double maxLon = presentLongitude + Math.toDegrees( Math.sin(searchRadiusKM/earthRadiusKM) / Math.cos(Math.toRadians(presentLatitude)));
        double minLon = presentLongitude - Math.toDegrees( Math.sin(searchRadiusKM/earthRadiusKM) / Math.cos(Math.toRadians(presentLatitude)));

        Log.e("Latitute Longitude", maxLat +" "+minLat+" "+maxLon+" "+minLon);
        lat_array= new double[]{maxLat, minLat, maxLon, minLon};
        return lat_array;
    }


}
