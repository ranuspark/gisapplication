package com.example.bhumicloud.gisapplications.model.field.textual.calculated;

import android.text.TextUtils;
import com.example.bhumicloud.gisapplications.model.field.textual.TextualValue;

public class CalculatedValue extends TextualValue {
    private final CalculatedElement mElement;
    private String mError;

    public CalculatedValue(CalculatedElement calculatedElement, String str, String str2) {
        super(str);
        this.mElement = calculatedElement;
        this.mError = str2;
    }

    public String getDisplayValue() {
        if (TextUtils.isEmpty(this.mError)) {
            return getElement().getDisplay().format(this.mValue);
        }
        return this.mError;
    }

    public CalculatedElement getElement() {
        return this.mElement;
    }

    public boolean hasError() {
        return TextUtils.isEmpty(this.mError);
    }

    public void setError(String str) {
        this.mError = str;
    }
}
